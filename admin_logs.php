<? 
	$DEBUG=FALSE;
	include("./includes/db_config.inc.php");
	include("./functions/client.func.php");
?>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<?	include("./javascript.php");?>
<!--script type="text/javascript" src="./js/admin_log.js"></script-->
<link rel="stylesheet" href="css/custom-theme/jquery-ui-1.8.23.custom.css">
<link rel="stylesheet" href="css/StyleSheet.css">
<script type="text/javascript" src="./js/jquery-ui-timepicker-addon.js"></script>
<script type="text/javascript">
	function genLog(Dialog) {
		var clientID=$('#selClientID').val();
		var cmd=$('#selStatus').val();
		var dStart=$('#txtDayStart').val();
		var dStop=$('#txtDayStop').val();
		var tStart=$('#txtTimeStart').val();
		var tStop=$('#txtTimeStop').val();
		var tAddress=$('#txtAddress').val();
		var clientID=$('#selClientID').val();
		var URL="./ajax/admin_logs.php";
		var tdata='#LogLiveALL_Body';
		var Params={'cmd':cmd,'client':clientID,'dstart':dStart,'dstop':dStop,'tstart':tStart,'tstop':tStop,'ip':tAddress};
		console.log('URL => '+URL);
		console.log(Params);
		$(tdata).empty();
		$.post(URL,Params,function(data) {
			if (data != null) {
				console.log(data);
				if (data.length > 0) {
					html="<tr class='header'>";
					html+="<td class='center w-15'>date time</td>";
					if (cmd=='live') {
						html+="<td class='center w-10'>client</td>";
						html+="<td class='center w-15'>from</td>";
						html+="<td class='center'>request</td>";
					} 
					if ( (cmd=='all')||(cmd=='feed') ) {
						html+="<td class='center w-5'>client</td>";
						html+="<td class='center w-15'>command</td>";
						html+="<td class='center w-5' nowrap>feed id</td>";
						html+="<td class='center w-10'>from</td>";
						html+="<td class='center'>request</td>";
					}
					if (cmd=='usage') {
						html+="<td class='center w-10'>user</td>";
						html+="<td class='center w-10'>from</td>";
						html+="<td class='center w-15'>menu</td>";
						html+="<td class='center'>request</td>";
					}
					html+="</tr>";
					$(tdata).append(html);

					$.each(data,function(entryIndex,entry) {
						html="<tr valign='top'>";
						html+="<td>"+entry['log_dtime']+"</td>";

						if (cmd=='live') {
							html+="<td class='right'>"+entry['log_client']+"</td>";
							html+="<td>"+entry['log_ip']+"</td>";
							html+="<td>"+entry['log_request'].replace('>','')+"</td>";
						} else if ((cmd=='all') || (cmd=='feed') ){
							html+="<td class='right'>"+entry['log_client']+"</td>";
							html+="<td nowrap>"+entry['log_cmd']+"</td>";
							html+="<td class='right'>"+entry['log_feed']+"</td>";
							html+="<td>"+entry['log_ip']+"</td>";
							html+="<td>"+strShow(entry['log_request'].replace('>',''),50)+"</td>";
						}	else if (cmd=='usage') {
							html+="<td>"+entry['usr_name']+"</td>";
							html+="<td>"+entry['log_ip']+"</td>";
							html+="<td>"+entry['log_request'].replace('menu=>','')+"</td>";
							html+="<td>"+entry['log_session'].replace('>','').replace('\n','<br>')+"</td>";
						}
						html+="</tr>";
						$(tdata).append(html);
					});
					$('#'+Dialog).dialog('close');
					$('#dvLog').show();
				}
			} else {
				$('#'+Dialog).dialog('close');
			}
		},"json");
		
	}

	$('document').ready(function() {
		$( "#txtDayStart,#txtDayStop" ).datepicker({
			showOn: "button",
			buttonImage: "images/calendar.gif",
			buttonImageOnly: true,
			dateFormat:"yy-mm-dd"
/*			dateFormat: "dd/mm/yy" */
		});

		$( "#txtTimeStart,#txtTimeStop" ).timepicker({
			hourGrid:4, 
			minuteGrid:10,
			showSecond: true,
			secondGrid:10,
			timeFormat: 'hh:mm:ss'
		});

		$.extend($.datepicker,{_checkOffset:function(inst,offset,isFixed){
			console.log(inst);
			console.log(offset);
			console.log(isFixed);
//			offset.top -= 350;
			return offset
			}});

		$( "#txtDayStart,#txtDayStop" ).css('font-size','12px');
		$( "#txtTimeStart,#txtTimeStop" ).css('font-size','12px');
	});
</script>
</head>
<body>
<div class="w-100 boxin">
    <div class="header"><span>Admin Logs</span></div>
    <table>
     <tr>
      <td>
      	<div class="dvtr">
          <div id="dvHead" style="float:left; padding-top:5px;">
          Log Type: 
          <select id="selStatus">
            <option value="all" selected>ALL</option>
            <option value="live">Live</option>
            <option value="feed">Feed</option>
            <option value="usage">Usage</option>
          </select>
          Client ID: 
          <select id="selClientID">
            <option value="-99" selected>ALL</option>
  <?
    $arrClient=json_decode(client_list(),true);
    for ($iCount=0; $iCount<count($arrClient); $iCount++) {
  ?>
            <option value="<?=$arrClient[$iCount]['cli_id']?>"><?=$arrClient[$iCount]['cli_name']?></option>
  <?
    }
  ?>
          </select>
          </div>
          <div id="dvOnlyIP" style="float:left;padding-left:10px; padding-top:5px;">
          Check Only IP :<input type="text" id="txtAddress" size="16">
          </div>
          <div id="dvRun" style="float:left; padding-left:10px;">
          <input class="btnAction" type="button" name="btnRun" onClick="$('#dvRunning').dialog('open');genLog('dvRunning');" value="Run">
          <input class="btnAction" type="button" name="btnClear" onClick="$('#dvLog').hide(); $('#LogLiveALL_Body').empty();" value="Clear">
          </div>
        </div>
        <div id="dvPeriod" style="padding-top:40px;">
        <strong>Period &nbsp;</strong> Start:<input type="text" id="txtDayStart" size="20" value="<?=date('Y-m-d');?>">
        <input type="text" id="txtTimeStart" size="20" value="00:00:00">
        Stop:<input type="text" id="txtDayStop" size="8" value="<?=date('Y-m-d');?>">
        <input type="text" id="txtTimeStop" size="8" value="<?=date('H:i:s');?>">
        </div>
      </td>
     </tr>
    </table>
</div>
<script type="text/javascript">
	$(function() {
		$('#dvRunning').dialog({ autoOpen: false, modal: true, draggable: false });
		$('#dvRunning').dialog("option","width",450);
		$('#dvRunning').dialog("option","height",180);
		$('#dvRunning').dialog("option","resizable",false);
	});
</script>

<div class="demo">
<div id="dvRunning" title="Query information please waiting.">
<table>
  <tr>
  	<th>
    <div class="center"><img src="./images/loading.gif" width="125" height="125"></div>
		</th>
 </tr>
</table>
</div>
</div>

<div id="dvLog" class="w-100 boxin" style="display:none;">
  <div class="header"><span>Live</span></div>
  <div style="max-height:275px; min-height:50px; overflow:auto">
  <table>
		<tbody id="LogLiveALL_Body" >
		</tbody>
  </table>
 </div>
</div>
</body>
</html>