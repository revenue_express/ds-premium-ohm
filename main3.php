<!DOCTYPE html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" href="css/style.css" type="text/css" />
<link rel="shortcut icon" href="images/RE%20icon_24x24.PNG" type="image/png" />
<title>Revenue Express | บริษัท เรเวนิว เอ็กซ์เพรส จำกัด</title>
<style type="text/css">
body{ 	
	margin:auto; 
	padding:0px; 
	background: #FFF url(images/bgfoot.png) repeat-x; background-position:bottom;
	/* padding: 0 0 180px 0; */
	overflow:hidden; 
}
</style>
<script type="text/javascript" src="js/jquery-1.7.2.min.js"></script>
<script type="text/javascript" src="js/jquery-ui-1.8.21.custom.min.js"></script>
<script type="text/javascript" src="./js/checkValid.js"></script>
</head>
<?	$menu=$_REQUEST['menu'];?>
<body>
<script>
	$(document).ready(function(){
		var allheight = window.innerHeight;// document.body.offsetHeight;
		var div_head = 80;
		var div_menu = 35;
		var div_foot = 30;
		var div_main = allheight-div_head-div_menu-div_foot;
		$("div#div_main").height(div_main);
	})
</script>
<center>
	<div class="index">
    	<div id="header">
       	  <div class="header_left">
          	<div style="float:left; margin:25px 0 0 20px;">	
				<img src="images/info-express_Header1.png">
           	</div>
            <div style="float:left; margin-top:35px; font-size:28px; text-shadow: 0.1em 0.1em #7BB1F4; font-weight:bold; color:#06C;">
            	Designer & Schedule Management System
            </div>
          </div>
<!--      <div class="header_center">
            <div style="float:left; margin-top:20px; margin-left:50px;">	
            	<center></center>
           	</div>
          </div> -->
          <div class="header_right">
          		<div style="float:right; margin:5px 10px 0 0;">
                <div style="float:left"><img src="images/logo1.png" width="45" height="70"></div>
                <div style="float:left; margin:20px 0 0 10px; font-family:'Angsana New'; font-size:25px; color:#F00">
                	สำนักราชเลขาธิการ
                </div>  
                </div>
          </div>
        </div>
        <div class="menu">
        	<div style="float:left; text-align:left; padding:0px; margin:0px; background-color:#B6D9F3">	
        	<? include "menu3.php"; ?>
            </div>
        </div>
        <div id="div_main" class="main">
        	<iframe name="frmDisplay" id="frmDisplay" src="<? if($_GET['menu']!=""){ echo $_GET['menu'].".php"; }else{ echo "home.php"; }?>" frameborder="0" cellspacing="0" style="border-style:none; width:100%; height:100%; background-color:#FFF;"></iframe>
        </div>
        <div class="footer">
      		<div style="padding-top:5px;">Copyright 2012 Revenue Express Co., Ltd. All rights reserved.</div>
        </div>
    </div>
</center>
</body>
</html>