<? 
	$DEBUG=FALSE;
	include("./includes/db_config.inc.php");
?>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<? include ("./javascript.php"); ?>
<!--
<script type="text/javascript" src="js/jquery-1.8.0.min.js"></script>
<script type="text/javascript" src="js/jquery-ui-1.8.23.custom.min.js"></script>
<script type="text/javascript" src="./js/checkValid.js"></script>
<script type="text/javascript" src="./js/signage.js"></script>
-->
<script type="text/javascript" src="./js/admin_client.js"></script>
<script>
	$(function() {
		$( "input:submit, input:button,  input:reset, a, button", ".demo" ).button();
		$( "a", ".demo" ).click(function() { return false; });
		$('tr:even').addClass('even');
	});
</script>
<link rel="stylesheet" href="css/custom-theme/jquery-ui-1.8.23.custom.css">
<link rel="stylesheet" href="css/StyleSheet.css">
<body onLoad="client_refresh();">
<? if ($DEBUG) print_r($USER);?>
<div id="dvClientResult">
    <div class="w-80 boxin"><!-- .altbox for alternative box's color -->
        <div class="header">
            <h3>
            <span>Client Management</span>
            <span style="padding-left:20px;">
                        <input type="button" class="btnTh" id="btnRefresh" value="Refresh" onClick="client_refresh();">
<?	if (strtolower($USER['usr_level'])==$_LevelSystem) {?>
                        &nbsp; &nbsp; 
                        <input type="button" class="btnTh" id="btnAddNew" value=" Add " onClick="client_show_form();" title="Add Client">
<?	}	?>
						</span>
            </h3>
        </div>
        <table cellspacing="0">
            <thead>
                <tr>
                    <th class="w-5">ID</th>
                    <th class="w-15">GROUP</th>
                    <th>CLIENT NAME</th>
                    <th class="w-20">ADDRESS</th>
                    <th colspan="2" class="w-10">Operation</th>
                </tr>
             </thead>
             <tbody id="tblResult_Body">
             	<tr>
                	<td>Login</td>
                	<td>User</td>
                	<td>Level</td>
                  	<td>
                    	<input type="button" id="btnEdit_XXX" value="Edit" onClick="client_show_edit(1234);"></td>
                  	<td>
                    	<input type="button" id="btnDel_XXX" value="Delete" onClick="client_delete(1234);"></td>
              	</tr>
             </tbody>
        </table>
	</div>
</div>
<div id="dvClientForm" class="w-50">
	<input type="button" class="btnBack" id="btnBack" value=" " onClick="client_refresh();">
    <div class="well">
        <div class="left head">Client Information</div>
    		<div class="dvtr top15">
            <div class="dvtd_left w-35 top5">Client ID : </div>
            <div class="dvtd_right">
                <input id="ClientID" class="txt" type="text" size="8" maxlength="32" readonly>
            </div>
        </div>
    		<div class="dvtr">
            <div class="dvtd_left w-35 top5">Client Code : </div>
            <div class="dvtd_right">
                <input id="txtCode" class="txt" type="text" size="16" maxlength="32" onKeyUp="client_input_check();">
            </div>
        </div>
        <div class="dvtr">
            <div class="dvtd_left w-35 top5">Client Name : </div>
            <div class="dvtd_right">
                <input id="txtName" class="txt" type="text" size="16" maxlength="32" onKeyUp="client_input_check();">
            </div>
        </div>
        <div class="dvtr">
            <div class="dvtd_left w-35 top5">Description : </div>
            <div class="dvtd_right">
                <input id="txtDesc" class="txt" type="text" size="40" maxlength="127" onKeyUp="client_input_check();">
            </div>
        </div>
        <div class="dvtr">
            <div class="dvtd_left w-35 top5">Group Name : </div>
            <div class="dvtd_right">
                <select id="selGroup" name="selGroup">
                </select>
            </div>
        </div>
        <div class="dvtr">
            <div class="dvtd_left w-35 top5">Password : </div>
            <div class="dvtd_right"><input id="txtPassword" class="txt" type="password" size="20" maxlength="127" onKeyUp="client_input_check();"><input type="checkbox" id="chkShowPass" onClick="client_click_pass();"><span id="spShowPass">show Password</span>
            </div>
        </div>
        <div class="dvtr">
            <div class="dvtd_left w-35 top5">IP Address : </div>
            <div class="dvtd_right"><input id="txtAddress" class="txt" type="text" size="40" maxlength="127" onKeyUp="client_input_check();";></div>
        </div>
        <div class="dvtr" style="display:none">
            <div class="dvtd_left w-35 top5">URL : </div>
            <div class="dvtd_right"><input id="txtURL" type="text" class="txt" size="40" maxlength="127"></div>
        </div>
        <div class="dvtr top10">
            <div class="dvtd_left w-50">
                <input type="button" class="btnSky" id="btnSubmitInfo" value="Save" onClick="client_submit()">
            </div>
            <div class="dvtd_right">
                <input type="reset" class="btnSky" id="btnAddCancel" value="Reset" onClick="">
            </div>
        </div>
    </div>
</div>
</body>