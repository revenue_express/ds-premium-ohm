<? 
	include("./includes/db_config.inc.php");
	include("./includes/sys_config.inc.php");
	include("./functions/library.func.new.php");
	$DEBUG=TRUE;
	$TypeID=2;
//	$ComID=1;
//	foreach ($_REQUEST as $Key => $Value) { $$Key=addslashes(urldecode($Value)); }
?>
<?	include("./javascript.php");?>
</head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<body>
<?	
	$FileList=json_decode(library_list($ComID,$TypeID,1),true);
	if ($DEBUG) {
//	echo "Count(Data) =".count($data);
//	$FileList=json_decode($data);
//		echo "Count(FileList) =".count($FileList);
//		echo "<pre>";
//		print_r($FileList);
//		echo "</pre>";
	}
?>
      <table cellspacing="0" border="1">
        <thead>
          <tr>
              <th>Display Name</th>
              <th>Original Name</th>
              <th>Sys Name</th>
              <th>Size(MB)</th>
              <th>Time(sec)</th>
              <th>W X H</th>
              <th>In Used</th>
          </tr>
      </thead>
		<?
			if (count($FileList) > 0) {
				for ($iCount=0;$iCount<count($FileList);$iCount++) { 
					$sFileName=sprintf("./data/videos/%s",$FileList[$iCount]['file_sname']);
					$onViewClick=sprintf("window.open('%s','_view');",$sFileName);
					$formDel=sprintf("formDelete_%d",$FileList[$iCount]['file_id']);  
					$onDelete=sprintf("if (confirm('Do you confirm to delete')) {document.getElementById('%s').submit(); }",$formDel);
					$formRestore=sprintf("formRestore_%d",$FileList[$iCount]['file_id']);  
					$onRestore=sprintf("if (confirm('Do you confirm to restore data')) {document.getElementById('%s').submit(); }",$formRestore);
					$FileInUse=FALSE;
					//$clsTR=($FileInUse)?"data_used":"data_free";
					$clsTR=($FileInUse)?"":"alt";
					if ($FileInUse) {
							$onDelete=sprintf("alert('%s')","File in used. Cannot Delete.");
					}	
					$movie = new ffmpeg_movie($sFileName);
					//$mtime=$movie->getDuration();
					//$movie_duration=sprintf("%s",gmdate('H:i:s',$mtime));
					//$msize=array("height"=>$movie->getFrameHeight(),"width"=>$movie->getFrameWidth());
					//$movie_size=sprintf("%d X %d",$msize['width'],$msize['height']);
		?>
      <tr>
          <td>[<?=$iCount;?>]<?=$FileList[$iCount]['file_dname'];?></td>
          <td><?=$FileList[$iCount]['file_oname'];?></td>
          <td><?=$FileList[$iCount]['file_sname'];?></td>
          <td class="right"><?=number_format($FileList[$iCount]['file_size']/(1024*1024),3);?></td>
          <td class="right"><?=$movie_duration;?></td>
          <td class="right"><?=$movie_size;?></td>
          <td class="center"><?=($FileInUse)?"Yes":"No";?></td>
       </tr>
    <? 		} ?>
    <? }?>
    </table>
</body>
</html>