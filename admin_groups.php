<? 
	$DEBUG=FALSE;
	include("./includes/db_config.inc.php");
	include("./functions/group.func.php");
?>
<? include("./javascript.php");?>
<script>
	$(function() {
		$( "input:submit, input:button, input:reset, a, button", ".demo" ).button();
		$( "a", ".demo" ).click(function() { return false; });		
	});

</script>
<link rel="stylesheet" href="css/StyleSheet.css">
<div id="dvLibUploadVideos">
<?
		if ($DEBUG) { echo "Request<pre>";		print_r($_REQUEST);		echo "</pre>"; }

		if (isset($btnDelete)) {
			if ($DEBUG) echo "<br> Delete File => $btnDelete";
			$RowAffect=group_active($btnDelete);
			if ($RowAffect == 1) {
				if ($DEBUG) echo ".....Delete Completed";
				unset($GroupID);
			} else {
				if ($DEBUG) echo ".....Delete Failed";
			}
		} elseif (isset($btnRestore)) {
			if ($DEBUG) echo "<br> Restore File => $btnRestore";
			$RowAffect=group_active($btnRestore,1);
			if ($RowAffect == 1) {
				if ($DEBUG) echo ".....Restore Completed";
				unset($GroupID);
			} else {
				if ($DEBUG) echo ".....Restore Failed";
			}
		} elseif (isset($btnAdd)) {
			if ($DEBUG) echo "Add<br>";
			$GroupID=group_add($ComID);
		} elseif (isset($btnEdit)) {
			if ($DEBUG) echo "Edit<br>";
			$RowAffect=group_edit($ComID,$GroupID,$txtCode,$txtName,$txtDesc);
			if ($RowAffect == 1) {
				if ($DEBUG) echo ".....Edit Completed";
				unset($GroupID);
			} else {
				if ($DEBUG) echo ".....Edit Failed";
			}
		} else {
			if (isset($_SERVER['HTTP_REFERER']))	{
				if ($_SERVER['HTTP_URI'] = $PHP_SELF) {
					echo ("No File Upload");
				}
			}
		}	
?>
</div>
<?	if (! isset($GroupID)) {?>
<?	
	$ItemList=json_decode(group_list($ComID),true);
	if ($DEBUG) {
//	echo "Count(Data) =".count($data);
//	$FileList=json_decode($data);
		echo "Count(ItemList) =".count($ItemList);
		echo "<pre>";
		print_r($ItemList);
		echo "</pre>";
//	for ($iCount=0; $iCount < count($FileList); $iCount++) {
//		$arrData=array();
//		$arrData=$FileList[$iCount];
//		echo "<pre>";
//		print_r($arrData);
//		echo "</pre>";
//		echo $arrData['file_id'];
	}
?>
<div class="w-50 boxin">   
    <div class="header">
        <h3>
        <span>Group Management</span>
        <span>&nbsp; 
        <button class="btnTh" onClick="document.getElementById('GroupAdd').submit();" title="Add Group"> &nbsp; Add &nbsp; </button>
        </span>
        </h3>
    </div>
    <table id="tblGroupResult" cellspacing="0">
        <thead>
          <tr>
            <th class="w-90 center">Group Name</th>
            <? if ($USER['usr_level'] <> 'Administrator') { 
                    $ColSize=2; $ColWidth=32;
                } else { $ColSize=3; $ColWidth=48;} ?>
            <form action="<?=$PHP_SELF;?>" method="post" id="GroupAdd" name="GroupAdd">
            <input type="hidden" id="btnAdd" name="btnAdd" value="Insert">
            <th colspan="<?=$ColSize;?>">Operations</th>
            </form>
          </tr>
        </thead>
        <?
        if (count($ItemList) > 0) {
            for ($iCount=0;$iCount<count($ItemList);$iCount++) {
                $formEdit=sprintf("formEdit_%d",$ItemList[$iCount]['grp_id']);  
                $onEdit=sprintf("document.getElementById('%s').submit();",$formEdit);
    
                $formDel=sprintf("formDelete_%d",$ItemList[$iCount]['grp_id']);  
                $onDelete=sprintf("if (confirm('Do you confirm to delete')) {document.getElementById('%s').submit(); }",$formDel);
    
                $formRestore=sprintf("formRestore_%d",$ItemList[$iCount]['grp_id']);  
                $onRestore=sprintf("if (confirm('Do you confirm to restore data')) {document.getElementById('%s').submit(); }",$formRestore);
                $NameClass = "";
                if($iCount==0){ $NameClass = "frist "; }
                if($iCount%2==0){ $NameClass .= "even"; }
        ?>
        <tbody>
          <tr class="<? echo $NameClass;?>">
            <td><?=$ItemList[$iCount]['grp_name'];?></td>
                <form action="<?=$PHP_SELF;?>" method="post" id="<?=$formEdit;?>" name="<?=$formEdit;?>">
            <input type="hidden" id="btnView" name="btnView" value="<?=$ItemList[$iCount]['grp_id'];?>">
            <input type="hidden" id="GroupID" name="GroupID" value="<?=$ItemList[$iCount]['grp_id'];?>">
            </td>
            <td class="w-10"><div class="btnEdit" onclick="<?=$onEdit;?>" title="Edit"></div></td>
                </form>
        <?	if ($USER['usr_level'] == 'Administrator') { ?>
                <form action="<?=$PHP_SELF;?>" method="post" id="<?=$formRestore;?>" name="<?=$formRestore;?>">
            <input type="hidden" id="btnRestore" name="btnRestore" value="<?=$ItemList[$iCount]['grp_id'];?>">
            <td class="w-10">
            <? if ($ItemList[$iCount]['grp_active']==0) { ?>
            <div class="btnRes" onclick="<?=$onRestore;?>" title="restore"></div><? }?>
            </td>
            </form>
        <?	} ?>
                <form action="<?=$PHP_SELF;?>" method="post" id="<?=$formDel;?>" name="<?=$formDel;?>">
            <input type="hidden" id="btnDelete" name="btnDelete" value="<?=$ItemList[$iCount]['grp_id'];?>">
            <td class="left">
            <? if ($ItemList[$iCount]['grp_active']==1) { ?>
            <div class="btnDel" onclick="<?=$onDelete;?>" title="delete"></div><? } ?>
            </td>
            </form>
          </tr>
        <? } }  else { ?>
          <tr>
        <?	$ColSize=($USER['usr_level'] == 'Administrator')?4:5; ?>
            <td align="center" colspan="<?=$ColSize;?>">NO FOUND</td>
         </tr>
        <?	} ?>
        </tbody>
    </table>
</div>
<?	}  else { ?> 
<? 
$ItemData=array();
if (isset($GroupID)) {
	$ItemData=json_decode(group_view($GroupID),true);
	$sDisplay="Do you confirm to change group information?";
	$onSubmitClick=sprintf("showConfirm('%s','formGroup','btnSaveSubmit','submit','Save');",$sDisplay);
}
?>
<button onClick="window.open('./admin_groups.php','_self')" class="btnBack"> </button>
<div class="w-50">
  	<div class="well">
    <form method="post" id="formGroup" name="formGroup">
    <input type="hidden" name="btnEdit" value="<?=$GroupID;?>">
    <input type="hidden" name="GroupID" value="<?=$GroupID;?>">
   		<div class="left head">Add Group</div>
        <div class="dvtr" style="display:none;">
            <div class="dvtd_left w-35 top5">Group Code :</div>
            <div class="dvtd_right top5">
            	<input type="hidden"id="txtCode" name="txtCode" value="<?=$ItemData[0]['grp_code'];?>" /><?=$ItemData[0]['grp_code'];?></div>
        </div>
        <div class="dvtr top15">
            <div class="dvtd_left w-35 top5">Group Name : </div>
            <div style="float:left; padding-left:5px;">
            	<input class="txt" type="text" id="txtName" name="txtName" value="<?=$ItemData[0]['grp_name'];?>"></div>
        </div>
        <div class="dvtr">
            <div class="dvtd_left w-35 top5">Group Desc : </div>
            <div style="float:left; padding-left:5px; padding-top:3px;">
                <textarea id="txtDesc" name="txtDesc" cols="40" rows="3" wrap="physical"><?=$ItemData[0]['grp_desc'];?></textarea>
            </div>
        </div>
        <div class="dvtr top10">
            <div class="dvtd_left w-50"><input type="button" class="btnSky" id="btnSaveSubmit" name="btnSaveSubmit" value="Save" onClick="<?=$onSubmitClick;?>"></div>
            <div class="dvtd_right"><input type="Reset" class="btnSky" id="btnReset" name="btnReset" value="Reset"></div>
        </div>
    </form>
	</div>
</div>
<?	} ?>