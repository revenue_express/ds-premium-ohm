<?
	function playlist_head_option_edit($OptID,$Type,$Category,$Value) {
		global $DEBUG,$USER;
		if (strlen($Value) > 0) {
			$sSQL="UPDATE playlist_head_option SET pho_active=1 ";
			$sSQL.=sprintf(", pho_type ='%s' ",$Type);
			$sSQL.=sprintf(", pho_name ='%s' ",$Category);
			$sSQL.=sprintf(", pho_value='%s' ",$Value);
			$sSQL.=sprintf("WHERE pho_id=%d",$OptID);
		} else {
			$sSQL=sprintf("DELETE FROM playlist_head_option WHERE pho_id=%d AND pho_name='%s' ",$OptID,$Category);
		}
		if ($DEBUG) echo "\n<br>[".__FUNCTION__."] sSQL -> '$sSQL'";
		$DBReturn=AffectedSQL($sSQL);
//		echo $DBReturn;
		return($DBReturn);
	}
	
	function playlist_head_option_add($PlayID,$Type,$Category,$Value) {
		global $DEBUG,$USER;

		$sSQL="INSERT INTO playlist_head_option SET pho_active=1 ";
		$sSQL.=sprintf(", pho_plh_id =%d ",$PlayID);
		$sSQL.=sprintf(", pho_type ='%s' ",$Type);
		$sSQL.=sprintf(", pho_name ='%s' ",$Category);
		$sSQL.=sprintf(", pho_value='%s' ",$Value);
		if ($DEBUG) echo "\n<br>[".__FUNCTION__."] sSQL -> '$sSQL'";
		$InsertID=InsertSQL($sSQL);
		return ($InsertID);
	}

	function playlist_head_option_delete($PlayID,$Type,$Category,$Value) {
		global $DEBUG,$USER;
		$sSQL=sprintf("DELETE FROM playlist_head_option WHERE pho_plh_id=%d ",$PlayID);
		$sSQL.=sprintf("AND pho_name='%s' ",$Category);
		if ($Type == 'Check') $sSQL.=sprintf("AND pho_value='%s' ",$Value);
		if ($DEBUG) echo "\n<br>[".__FUNCTION__."] sSQL -> '$sSQL'";
		$DBReturn=AffectedSQL($sSQL);
//		echo $DBReturn;
		return($DBReturn);
	}
	
	function playlist_head_option_list($PlayID) {
		global $DEBUG,$USER;
		
		$DBReturn=0;
		$sSQL=sprintf("SELECT * FROM playlist_head_option ");
		$sSQL.=sprintf("WHERE pho_plh_id=%d ",$PlayID);
		if ($USER['usr_level'] <> 'Administrator') $sSQL.="AND (pho_active = 1) "; 
		if ($DEBUG) echo "\n<br>[".__FUNCTION__."] sSQL -> '$sSQL'";
		$DBReturn=JSonSQL($sSQL);
//		echo $DBReturn;
		return($DBReturn);
	}
?>