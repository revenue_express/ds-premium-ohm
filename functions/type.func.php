<?
	function type_list() {
		global $DEBUG,$USER;
		$sSQL=sprintf("SELECT * FROM type_info WHERE true ");
//		if ($USER['usr_level'] <> 'Administrator') $sSQL.="AND (type_active=1)";
		$sSQL.="ORDER BY type_id ";
		if ($DEBUG) echo "<br>[".__FUNCTION__."] sSQL -> $sSQL";
		$Data=JSonSQL($sSQL);
		return($Data);
	}

	function type_info($TypeID) {
		global $DEBUG,$USER;
		$sSQL=sprintf("SELECT * FROM type_info WHERE type_id=%d ",$TypeID);
//		if ($USER['usr_level']<> 'Administrator') $sSQL.="AND (type_active=1)";
		$sSQL.="ORDER BY type_id ";
		if ($DEBUG) echo "<br>[".__FUNCTION__."] sSQL -> $sSQL";
		$Data=JSonSQL($sSQL);
		return($Data);
	}

	function type_option_list($TypeID) {
		global $DEBUG,$USER;
		$sSQL=sprintf("SELECT * FROM type_info,type_option WHERE (type_id = typo_type_id) AND (typo_type_id=%d) ",$TypeID);
//		if ($USER['usr_level'] <> 'Administrator') $sSQL.="AND (typo_active=1)";
		$sSQL.="AND (typo_active=1)";
		$sSQL.="ORDER BY typo_category,typo_id ";
		if ($DEBUG) echo "<br>[".__FUNCTION__."] sSQL -> $sSQL";
		$Data=JSonSQL($sSQL);
		return($Data);
	}

?>