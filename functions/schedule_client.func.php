<?

	function schedule_client_info($ClientID,$ScheduleID) {
		global $DEBUG,$USER;
		$sSQL=sprintf("SELECT * FROM schedule_client WHERE schc_cli_id=%d AND schc_schh_id =%d ",$ClientID,$ScheduleID);
		if ($DEBUG) echo "\n<br>[".__FUNCTION__."] sSQL -> $sSQL";
		$InsertID=JSonSQL($sSQL);
		return ($InsertID);
	}

	function schedule_client_add($ScheduleID,$ClientID) {
		global $DEBUG,$USER;
		$sSQL="INSERT INTO schedule_client SET ";
		$sSQL.=sprintf("schc_schh_id = %d",$ScheduleID);
		$sSQL.=sprintf(",schc_cli_id = %d",$ClientID);
		$sSQL.=sprintf(",schc_active = 0");
		if ($DEBUG) echo "\n<br>[".__FUNCTION__."] sSQL -> $sSQL";
		$InsertID=InsertSQL($sSQL);
		return ($InsertID);
	}
	
	function schedule_client_list_from($Mode="client",$DataID,$All=1) {
		global $DEBUG,$USER;
		
		$sSQL=sprintf("SELECT * FROM schedule_client,schedule_head WHERE (schc_cli_id = %d) ",$DataID);
		if (strtolower($Mode) <> 'client') {
			$sSQL=sprintf("SELECT * FROM schedule_client,schedule_head WHERE (schc_schh_id = %d) ",$DataID);
		}
		if (strtolower($Mode) == 'clientip') {
			$sSQL="SELECT * FROM client_info,schedule_client,schedule_head ";
			$sSQL.=sprintf("WHERE (cli_id = schc_cli_id) AND (cli_address='%s') ",$DataID);
		}
		$sSQL.=sprintf("AND (schc_schh_id = schh_id) ");

		if ($All <> 1) {
			$sSQL.="AND (schc_active = 1) ";
		}
//		if ($USER['usr_level'] <> 'Administrator') $sSQL.="AND (schc_active = 1) ";
		$sSQL.="ORDER BY schc_id";
		
		if ($DEBUG) echo "<br>[".__FUNCTION__."] sSQL -> $sSQL<br>";
		$JSonData=JSonSQL($sSQL);
//		echo $DBReturn;
		return($JSonData);
		
	}

	function schedule_client_count_from($Mode="client",$DataID) {
		global $DEBUG,$USER;
		
		$sSQL=sprintf("SELECT count(*) as counter FROM schedule_client WHERE schc_cli_id = %d ",$Data);
		if (strtolower($Mode) <> 'client') {
			$sSQL=sprintf("SELECT count(*) as counter FROM schedule_client WHERE schc_schh_id = %d ",$Data);
		}
		
		if ($USER['usr_level'] <> 'Administrator') $sSQL.="AND (schc_active = 1) ";
		$sSQL.="ORDER BY schc_id";
		
		if ($DEBUG) echo "<br>[".__FUNCTION__."] sSQL -> $sSQL<br>";
		$Result=ExecuteReader($sSQL);
		$Counter=0;
		if ($Data=mysql_fetch_row($Result)) {
			$Counter=$Data['counter'];
		}
//		echo $DBReturn;
		return($Counter);
		
	}

	function schedule_client_active($ClientID,$SchID,$Active=0) {
		global $DEBUG,$USER;
		$sSQL=sprintf("UPDATE schedule_client SET schc_active=%d WHERE schc_cli_id =%d AND schc_schh_id=%d ",$Active,$ClientID,$SchID);
		if ($DEBUG) echo "\n<br>[".__FUNCTION__."] sSQL -> $sSQL";
		$InsertID=AffectedSQL($sSQL);
		return ($InsertID);
	}

	function schedule_client_delete($ClientID,$SchID) {
		global $DEBUG,$USER;
		$sSQL=sprintf("DELETE FROM schedule_client WHERE schc_cli_id =%d AND schc_schh_id=%d ",$ClientID,$SchID);
		if ($DEBUG) echo "\n<br>[".__FUNCTION__."] sSQL -> $sSQL";
		$InsertID=AffectedSQL($sSQL);
		return ($InsertID);
	}
	
	function schedule_client_activate($ClientID,$SchID) {
		global $DEBUG,$USER;
		$SchInfo=json_decode(schedule_client_info($ClientID,$SchID),true)	;
		$Data=0;
		if (count($SchInfo) > 0) {
			$ClientID=$SchInfo[0]['schc_cli_id'];
			$List=json_decode(schedule_client_list_from('client',$ClientID),true);
			for ($iCount=0; $iCount<count($List); $iCount++) {
				$Data=schedule_client_active($ClientID,$List[$iCount]['schc_schh_id'],0);
			}
			$Data=schedule_client_active($ClientID,$SchID,1);
		}
		return ($Data);
	}
	
	
?>