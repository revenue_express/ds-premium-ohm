<?
	function layout_cover_info($HeadID) {
		global $DEBUG,$USER;
		
		$sSQL=sprintf("SELECT * FROM template_cover,file_info WHERE tmpc_tmph_id = %d ",$HeadID);
		$sSQL.=sprintf("AND (tmpc_file_id = file_id) ");
		$sSQL.=sprintf("AND (tmpc_active = 1) ");
//		if ($USER['usr_level'] <> 'Administrator') $sSQL.="AND (schp_active = 1) ";
//		$sSQL.="ORDER BY schp_start,schp_stop";
		
		if ($DEBUG) echo "<br>[".__FUNCTION__."] sSQL -> $sSQL<br>";
		$JSonData=JSonSQL($sSQL);
//		echo $DBReturn;
		return($JSonData);
	
	}
	
	function layout_cover_list($ComID=1) {
		global $DEBUG,$USER;

		$sSQL=sprintf("SELECT * FROM file_info WHERE file_type_id=11 ");
//		$sSQL.=sprintf("AND (tmpc_file_id = file_id) ");
		$sSQL.=sprintf("AND (file_active = 1) ");
		$sSQL.=sprintf("AND (file_com_id = %d) ",$ComID);
//		$sSQL.=sprintf(" GROUP BY tmpc_file_id");
//		if ($USER['usr_level'] <> 'Administrator') $sSQL.="AND (schp_active = 1) ";
//		$sSQL.="ORDER BY schp_start,schp_stop";
		
		if ($DEBUG) echo "<br>[".__FUNCTION__."] sSQL -> $sSQL<br>";
		$JSonData=JSonSQL($sSQL);
//		echo $DBReturn;
		return($JSonData);
	}
	
	function layout_cover_add($HeadID,$FileID) {
		global $DEBUG,$Config,$ErrorMsg,$DBReturn,$USER;

		$sSQL="INSERT INTO template_cover SET tmpc_active = 1 ";
		$sSQL.=sprintf(",tmpc_tmph_id=%d ",$HeadID);
		$sSQL.=sprintf(",tmpc_file_id=%d ",$FileID);
		if ($DEBUG) echo "<br>[".__FUNCTION__."] sSQL -> $sSQL";
		$InsertID=InsertSQL($sSQL);
		if ($DEBUG) echo "<li>[".__FUNCTION__."] Return -> $InsertID<br>";
		return ($InsertID);
	}
	
	function layout_cover_update($LineID,$HeadID,$FileID) {
		global $DEBUG,$Config,$ErrorMsg,$DBReturn,$USER;

		$sSQL="UPDATE template_cover SET tmpc_active = 1 ";
		$sSQL.=sprintf(",tmpc_tmph_id=%d ",$HeadID);
		$sSQL.=sprintf(",tmpc_file_id=%d ",$FileID);
		$sSQL.=sprintf("WHERE tmpc_id=%d",$LineID);
		if ($DEBUG) echo "<br>[".__FUNCTION__."] sSQL -> $sSQL";
		$rCount=AffectedSQL($sSQL);
		return($rCount);
	}

	function layout_cover_delete($LineID) {
		global $DEBUG,$Config,$ErrorMsg,$DBReturn,$USER;

		$sSQL.=sprintf("DELETE FROM template_cover WHERE tmpc_id=%d",$LineID);
		if ($DEBUG) echo "<br>[".__FUNCTION__."] sSQL -> $sSQL";
		$rCount=AffectedSQL($sSQL);
		return($rCount);
	}

?>