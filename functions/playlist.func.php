<?
	function playlist_head_list($comID=1,$typeID=1,$login=1, $ppage=0, $limit=0, $spage=0) {
		global $DEBUG,$USER;

		if ($typeID == 5) {
			$sTable=sprintf("SELECT PL.*,count(plf_id) as counter ");
			$sTable.=sprintf("FROM playlist_feed PL, feed_info FI WHERE feed_id=plf_feed_id "); 
//		f (strtolower($USER['usr_level']) == 'operator') $sTable.="AND (pll_active = 1) "; 
			$sTable.="GROUP BY plf_plh_id";

			$sSQL=sprintf("SELECT * FROM  playlist_head LEFT JOIN (%s) as T1 ",$sTable);
			$sSQL.=sprintf("on T1.plf_plh_id = plh_id WHERE (plh_com_id = %d) ",$comID);
		} else if ($typeID == 6) {
			$sTable=sprintf("SELECT PL.*,count(plrss_id) as counter ");
			$sTable.=sprintf("FROM playlist_rss PL, rss_info_head FI WHERE rinh_id=plrss_rinh_id "); 
//		f (strtolower($USER['usr_level']) == 'operator') $sTable.="AND (pll_active = 1) "; 
			$sTable.="GROUP BY plrss_plh_id";

			$sSQL=sprintf("SELECT * FROM  playlist_head LEFT JOIN (%s) as T1 ",$sTable);
			$sSQL.=sprintf("on T1.plf_plh_id = plh_id WHERE (plh_com_id = %d) ",$comID);
		} else if ($typeID == 8) {
			$sTable=sprintf("SELECT PL.*,count(plstm_id) as counter ");
			$sTable.=sprintf("FROM playlist_stream PL, stream_info FI WHERE stm_id=plstm_stm_id "); 
//		f (strtolower($USER['usr_level']) == 'operator') $sTable.="AND (pll_active = 1) "; 
			$sTable.="GROUP BY plstm_plh_id";

			$sSQL=sprintf("SELECT * FROM  playlist_head LEFT JOIN (%s) as T1 ",$sTable);
			$sSQL.=sprintf("on T1.plstm_plh_id = plh_id WHERE (plh_com_id = %d) ",$comID);
		} else {
			$sTable=sprintf("SELECT PL.*,count(pll_id) as counter, sum(file_size) as summary ");
			$sTable.=sprintf("FROM playlist_line PL, file_info FI WHERE file_id=pll_file_id "); 
//		f (strtolower($USER['usr_level']) == 'operator') $sTable.="AND (pll_active = 1) "; 
			$sTable.="GROUP BY pll_plh_id";
			$sSQL=sprintf("SELECT * FROM  playlist_head LEFT JOIN (%s) as T1 ",$sTable);
			$sSQL.=sprintf("on T1.pll_plh_id = plh_id WHERE (plh_com_id = %d) ",$comID);
		}

//		f (strtolower($USER['usr_level']) == 'operator') $sSQL.="AND (plh_active = 1) "; 
		if ($typeID <> 0)  $sSQL.=sprintf("AND plh_type_id=%d ",$typeID);
		$sSQL.="ORDER BY plh_active DESC,plh_id";
		if ( ($ppage <> 0) && ($limit<> 0) && ($spage <> 0) ) {
			$sSQL.=sprintf(" LIMIT %d,%d",$spage*$limit,$limit);
		}
		if ($DEBUG) echo "<br>[".__FUNCTION__."] sSQL -> '$sSQL'";
		$DBReturn=JSonSQL($sSQL);
//		echo $DBReturn;
		return($DBReturn);
	}

	function playlist_head_dummyID($comID=1,$TypeID=1) {
		global $DEBUG,$USER;
		$sHead=sprintf("%s",date('ymd'));
		$sSQL=sprintf("SELECT * FROM  playlist_head WHERE (plh_code like '%s%%') ",$sHead);
		$sSQL.=sprintf("AND (plh_com_id=%d) ",$comID);
		$sSQL.=sprintf("AND (plh_type_id=%d) ",$TypeID);
		$sSQL.=sprintf("ORDER BY plh_code DESC LIMIT 1 ");
		if ($DEBUG) echo "<br>[".__FUNCTION__."] sSQL -> '$sSQL'";
		$iCount=1;
		$Result=ExecuteReader($sSQL);
		if ($Data=mysql_fetch_array($Result)) {
			$Num=$Data['plh_code'];
//			if ($DEBUG) echo "[".__FUNCTION__."] plh_code -> '$Num'<br>";
			$iCount=intval(substr($Num,strlen($sHead)));
			$iCount++;
//			if ($DEBUG) echo "[".__FUNCTION__."] iCount -> '$iCount'<br>";
		}
		$Code=sprintf("%s%03d",$sHead,$iCount);
//			if ($DEBUG) echo "[".__FUNCTION__."] Return -> '$Code'<br>";
		return($Code);		
	}
	
	function playlist_head_view($PlaylistID) {
		global $DBReturn,$DEBUG;
		$sSQL=sprintf("SELECT * FROM playlist_head,type_info WHERE plh_type_id = type_id AND plh_id=%d",$PlaylistID);
		if ($DEBUG) echo "<br>[".__FUNCTION__."] sSQL -> '$sSQL'";
		$DBReturn=JSonSQL($sSQL);
//		echo $DBReturn;
		return($DBReturn);
	}

	function playlist_head_info($PlaylistID) {
		global $DBReturn,$DEBUG;
		$sSQL=sprintf("SELECT * FROM playlist_head,type_info WHERE plh_type_id = type_id AND plh_id=%d",$PlaylistID);
		if ($DEBUG) echo "<br>[".__FUNCTION__."] sSQL -> '$sSQL'";
		$DBReturn=JSonSQL($sSQL);
//		echo $DBReturn;
		return($DBReturn);
	}

	function playlist_head_add($ComID=1,$TypeID=1,$PLName=NULL) {
		global $DBReturn,$DEBUG;
		$DBReturn=0;
		$InsertID=0;
		$sCode=playlist_head_dummyID($ComID,$TypeID);
		if (! is_null($PLName)) {
			$sCode=$PLName;
		}
		$sSQL="INSERT INTO playlist_head SET ";
		$sSQL.="plh_active=1 ";
		$sSQL.=", plh_com_id=$ComID ";
		$sSQL.=sprintf(", plh_type_id=%d ",$TypeID);
		$sSQL.=sprintf(", plh_code='%s'",$sCode);
		$sSQL.=sprintf(", plh_name='%s'",$sCode);
		if ($DEBUG) echo "<br>[".__FUNCTION__."] sSQL -> '$sSQL'";
		$InsertID=InsertSQL($sSQL);
		return ($InsertID);
	}
	
	function playlist_head_edit($HeadID,$Name) {
		global $DEBUG;
		$Name=addslashes($Name);
		$sSQL=sprintf("UPDATE playlist_head SET plh_name = '%s' ",$Name);
		$sSQL.=sprintf("WHERE plh_id=%d",$HeadID);
		if ($DEBUG) echo "<br>[".__FUNCTION__."] sSQL -> '$sSQL'";
		$DBReturn=AffectedSQL($sSQL);
//		echo $DBReturn;
		return($DBReturn);
	}
	
	function playlist_head_active($HeadID,$active=0) {
		global $DEBUG;
		
		$sSQL=sprintf("UPDATE playlist_head SET plh_active = %d ",$active);
		$sSQL.=sprintf("WHERE plh_id=%d",$HeadID);
		if ($DEBUG) echo "<br>[".__FUNCTION__."] sSQL -> '$sSQL'";
		$DBReturn=AffectedSQL($sSQL);
//		echo $DBReturn;
		return($DBReturn);
	}
	

	function playlist_line_lastorder($HeadID) {
		global $DEBUG;
		$DBReturn=0;
			
		$sSQL=sprintf("SELECT pll_order FROM playlist_line WHERE pll_plh_id = %d ORDER BY pll_order DESC LIMIT 1",$HeadID);
		if ($DEBUG) echo "<br>[".__FUNCTION__."] sSQL -> '$sSQL'";
		$Result=ExecuteReader($sSQL);
		if ($Data=mysql_fetch_array($Result)) {
			$DBReturn=$Data['pll_order'];
		}
		return ($DBReturn);
	}
	
	function playlist_line_add($HeadID,$FileID,$Para=NULL) {
		global $DEBUG;
		$OrderID=playlist_line_lastorder($HeadID);
				
		$sSQL="INSERT INTO playlist_line SET pll_active=1 ";
		$sSQL.=sprintf(", pll_plh_id=%d ",$HeadID);
		$sSQL.=sprintf(", pll_file_id=%d ",$FileID);
		$sSQL.=sprintf(", pll_order=%d ",$OrderID+1);
		if (! is_null($Para)) $sSQL.=sprintf(", pll_para='%s' ",$Para);
		if ($DEBUG) echo "<br>[".__FUNCTION__."] sSQL -> '$sSQL'";
		$InsertID=InsertSQL($sSQL);
		return ($InsertID);
	}
	
	function playlist_line_list($ComID=1,$TypeID=1,$HeadID=1,$login=1, $ppage=0, $limit=0, $spage=0) {
		global $DEBUG,$USER;
		if ($TypeID == 8) {
			$sSQL="SELECT * FROM playlist_head ph ,playlist_stream ps ,stream_info si ";
			$sSQL.="WHERE (ph.plh_id = ps.plstm_plh_id) AND (ps.plstm_stm_id = si.stm_id) ";
			$sSQL.="AND (ph.plh_active=1) AND (ps.plstm_active=1) AND (si.stm_active=1) ";
			$sSQL.=sprintf("AND (ph.plh_id = %d) ",$HeadID);
			$sSQL.="LIMIT 1 ";
		} else {
			$sTable=sprintf("SELECT * FROM file_info WHERE (file_type_id = %d) AND (file_com_id=%d) ",$TypeID,$ComID);
			if (strtolower($USER['usr_level']) == 'operator') $sTable.="AND (file_active = 1) "; 
	
			$sTab=sprintf("SELECT * FROM playlist_line,playlist_head WHERE (pll_plh_id = plh_id)");
			if (strtolower($USER['usr_level']) == 'operator') $sTab.="AND (plh_active = 1) "; 
	
	//		$sSQL=sprintf("SELECT * FROM  (%s) as T0 LEFT JOIN (%s) as T1 on T1.file_id = T0.pll_file_id WHERE (T0.pll_plh_id = %d) ",$sTab,$sTable,$HeadID);
	//		$sSQL=sprintf("SELCT * FROM (%s) as T0, (%s) as T1 WHERE T1.file_id=T0.pll_file_id WHERE (T0.pll_plh_id=%d) ",$sTab,$sTable,$HeadID);

			$sSQL=sprintf("SELECT * FROM type_info,file_info,playlist_head,playlist_line WHERE (pll_plh_id=%d) ",$HeadID);
			$sSQL.=sprintf("AND (file_type_id=%d) AND (file_com_id=%d) ",$TypeID,$ComID);
			$sSQL.=sprintf("AND (pll_plh_id = plh_id) AND (pll_file_id = file_id) AND (type_id = plh_type_id) ");
	
			if ($USER['usr_level'] <> 'Administrator') $sSQL.="AND (pll_active = 1) "; 
			if ($USER['usr_level'] <> 'Administrator') $sSQL.="AND (plh_active = 1) "; 
	
			$sSQL.=sprintf("ORDER BY pll_active DESC, pll_order ");		
//		$sSQL.="Order by T0.pll_order, T0.pll_active DESC,T0.pll_id";
			if ( ($ppage <> 0) && ($limite<> 0) && ($spage <> 0) ) {
				$sSQL.=sprintf(" LIMIT %d,%d",$spage*$limit,$limit);
			}
		}

		if ($DEBUG) echo "<br>[".__FUNCTION__."] sSQL -> '$sSQL'";
		$DBReturn=JSonSQL($sSQL);
//		echo $DBReturn;
		return($DBReturn);
	}
	
	function playlist_line_count($HeadID) {
		global $DEBUG,$USER;
		
		$DBReturn=0;
		$sSQL=sprintf("SELECT count(*) as counter FROM playlist_line ");
		$sSQL.=sprintf("WHERE pll_plh_id=%d ",$HeadID);
		if (strtolower($USER['usr_level']) == 'operator') $sSQL.="AND (pll_active = 1) "; 
		if ($DEBUG) echo "<br>[".__FUNCTION__."] sSQL -> '$sSQL'";
		$Result=ExecuteReader($sSQL);
		if ($Data = mysql_fetch_array($Result)) {
				$DBReturn=$Data['counter'];
		}
		return($DBReturn);
	}

	function playlist_line_active($LineID,$active=0) {
		global $DEBUG,$USER;
		
		$sSQL=sprintf("UPDATE playlist_line SET pll_active = %d ",$active);
		$sSQL.=sprintf("WHERE pll_id=%d",$LineID);
		if ($DEBUG) echo "<br>[".__FUNCTION__."] sSQL -> '$sSQL'";
		$DBReturn=AffectedSQL($sSQL);
//		echo $DBReturn;
		return($DBReturn);
	}


	function playlist_line_order($LineID,$OrderID) {
		global $DEBUG,$USER;
		
		$sSQL=sprintf("UPDATE playlist_line SET pll_order = %d ",$OrderID);
		$sSQL.=sprintf("WHERE pll_id=%d",$LineID);
		if ($DEBUG) echo "<br>[".__FUNCTION__."] sSQL -> '$sSQL'";
		$DBReturn=AffectedSQL($sSQL);
//		echo $DBReturn;
		return($DBReturn);
	}
	
	function playlist_head_clone($ComID=1,$HeadID) {
		global $DEBUG,$USER;

		$sCode=playlist_head_dummyID($ComID);
		$sSQL="INSERT INTO playlist_head (plh_type_id,plh_code,plh_com_id,plh_name) ";
		$sSQL.=sprintf("SELECT T1.plh_type_id, '%s' , T1.plh_com_id, concat(T1.plh_name,'_copy') ",$sCode);
		$sSQL.=sprintf("FROM playlist_head as T1 WHERE T1.plh_id =%d LIMIT 1",$HeadID);
		if ($DEBUG) echo "<br>[".__FUNCTION__."] sSQL -> '$sSQL'";
		$InsertID=InsertSQL($sSQL);
		return ($InsertID);
	}
	
	function playlist_line_clone($NewID,$OldID) {
		global $DEBUG,$USER;
		
		$sSQL="INSERT INTO playlist_line (pll_plh_id,pll_order,pll_file_id) ";
		$sSQL.=sprintf("SELECT %d, T1.pll_order, T1.pll_file_id ",$NewID);
		$sSQL.=sprintf("FROM playlist_line as T1 WHERE T1.pll_active =1 AND T1.pll_plh_id =%d",$OldID);
		if ($DEBUG) echo "<br>[".__FUNCTION__."] sSQL -> '$sSQL'";
		$DBReturn=AffectedSQL($sSQL);
//		echo $DBReturn;
		return($DBReturn);
	}
	
	
	function playlist_head_option_edit($OptID,$Type,$Category,$Value) {
		global $DEBUG,$USER;
		if (strlen($Value) > 0) {
			$sSQL="UPDATE playlist_head_option SET pho_active=1 ";
			$sSQL.=sprintf(", pho_type ='%s' ",$Type);
			$sSQL.=sprintf(", pho_name ='%s' ",$Category);
			$sSQL.=sprintf(", pho_value='%s' ",$Value);
			$sSQL.=sprintf("WHERE pho_id=%d",$OptID);
		} else {
			$sSQL=sprintf("DELETE FROM playlist_head_option WHERE pho_id=%d AND pho_name='%s' ",$OptID,$Category);
		}
		if ($DEBUG) echo "\n<br>[".__FUNCTION__."] sSQL -> '$sSQL'";
		$DBReturn=AffectedSQL($sSQL);
//		echo $DBReturn;
		return($DBReturn);
	}
	
	function playlist_head_option_add($PlayID,$Type,$Category,$Value) {
		global $DEBUG,$USER;

		$sSQL="INSERT INTO playlist_head_option SET pho_active=1 ";
		$sSQL.=sprintf(", pho_plh_id =%d ",$PlayID);
		$sSQL.=sprintf(", pho_type ='%s' ",$Type);
		$sSQL.=sprintf(", pho_name ='%s' ",$Category);
		$sSQL.=sprintf(", pho_value='%s' ",$Value);
		if ($DEBUG) echo "\n<br>[".__FUNCTION__."] sSQL -> '$sSQL'";
		$InsertID=InsertSQL($sSQL);
		return ($InsertID);
	}

	function playlist_head_option_delete($PlayID,$Type,$Category,$Value) {
		global $DEBUG,$USER;
		$sSQL=sprintf("DELETE FROM playlist_head_option WHERE pho_plh_id=%d ",$PlayID);
		$sSQL.=sprintf("AND pho_name='%s' ",$Category);
		if ($Type == 'Check') $sSQL.=sprintf("AND pho_value='%s' ",$Value);
		if ($DEBUG) echo "\n<br>[".__FUNCTION__."] sSQL -> '$sSQL'";
		$DBReturn=AffectedSQL($sSQL);
//		echo $DBReturn;
		return($DBReturn);
	}
	
	function playlist_head_option_list($PlayID) {
		global $DEBUG,$USER;
		
		$DBReturn=0;
		$sSQL=sprintf("SELECT * FROM playlist_head_option ");
		$sSQL.=sprintf("WHERE pho_plh_id=%d ",$PlayID);
		if (strtolower($USER['usr_level']) == 'operator') $sSQL.="AND (pho_active = 1) "; 
		if ($DEBUG) echo "\n<br>[".__FUNCTION__."] sSQL -> '$sSQL'";
		$DBReturn=JSonSQL($sSQL);
//		echo $DBReturn;
		return($DBReturn);
	}
	
	function playlist_used($PlayID) {
		global $DEBUG,$USER;
		$sSQL="SELECT count(*) as Counter FROM playlist_head,layout_head,layout_line ";
		$sSQL.="WHERE (layl_plh_id= plh_id) AND (layh_id = layl_layh_id) AND (layl_layh_id = layh_id)  ";
//		$sSQL.="AND (layh_active = 1) AND (layl_active =1) ";
		$sSQL.=sprintf("AND (plh_id = %d) ",$PlayID);
		if ($DEBUG) echo "[".__FUNCTION__."] sSQL -> '$sSQL'<br>";
		$DBReturn=ExecuteReader($sSQL);
		if ($Data=mysql_fetch_array($DBReturn)) {
//		echo $DBReturn;
			return($Data[0]['Counter']);
		} else {
			return 0;
		}
	}
	
	function playlist_line_option_list($LineID) {
		global $DEBUG,$USER;
		
		$DBReturn=0;
		$sSQL=sprintf("SELECT * FROM playlist_line_option ");
		$sSQL.=sprintf("WHERE plo_pll_id=%d ",$LineID);
		if (strtolower($USER['usr_level']) == 'operator') $sSQL.="AND (plo_active = 1) "; 
		if ($DEBUG) echo "<br>[".__FUNCTION__."] sSQL -> '$sSQL'";
		$DBReturn=JSonSQL($sSQL);
//		echo $DBReturn;
		return($DBReturn);
	}

	function playlist_line_option_edit($OptID,$Type,$Category,$Value) {
		global $DEBUG,$USER;
		if (strlen($Value) > 0) {
			$sSQL="UPDATE playlist_line_option SET plo_active=1 ";
			$sSQL.=sprintf(", plo_type ='%s' ",$Type);
			$sSQL.=sprintf(", plo_name ='%s' ",$Category);
			$sSQL.=sprintf(", plo_value='%s' ",$Value);
			$sSQL.=sprintf("WHERE plo_id=%d",$OptID);
		} else {
			$sSQL=sprintf("DELETE FROM playlist_line_option WHERE plo_id=%d AND plo_name='%s' ",$OptID,$Category);
		}
		if ($DEBUG) echo "\n<br>[".__FUNCTION__."] sSQL -> '$sSQL'";
		$DBReturn=AffectedSQL($sSQL);
//		echo $DBReturn;
		return($DBReturn);
	}
	
	function playlist_line_option_add($PlayID,$Type,$Category,$Value) {
		global $DEBUG,$USER;

		$sSQL="INSERT INTO playlist_line_option SET plo_active=1 ";
		$sSQL.=sprintf(", plo_pll_id =%d ",$PlayID);
		$sSQL.=sprintf(", plo_type ='%s' ",$Type);
		$sSQL.=sprintf(", plo_name ='%s' ",$Category);
		$sSQL.=sprintf(", plo_value='%s' ",$Value);
		if ($DEBUG) echo "\n<br>[".__FUNCTION__."] sSQL -> '$sSQL'";
		$InsertID=InsertSQL($sSQL);
		return ($InsertID);
	}

	function playlist_line_option_delete($PlayID,$Type,$Category,$Value) {
		global $DEBUG,$USER;
		$sSQL=sprintf("DELETE FROM playlist_line_option WHERE plo_pll_id=%d ",$PlayID);
		$sSQL.=sprintf("AND plo_name='%s' ",$Category);
		if (strtolower($Type) == 'check') $sSQL.=sprintf("AND plo_value='%s' ",$Value);
		if ($DEBUG) echo "\n<br>[".__FUNCTION__."] sSQL -> '$sSQL'";
		$DBReturn=AffectedSQL($sSQL);
//		echo $DBReturn;
		return($DBReturn);
	}


//	================================
//	Remove Playlist Section
// ================================
	
	function playlist_head_remove($HeadID) {
		global $DEBUG,$USER;
		
		$sSQL=sprintf("DELETE FROM playlist_head");
		$sSQL.=sprintf(" WHERE plh_id=%d",$HeadID);
		if ($DEBUG) echo "<br>[".__FUNCTION__."] sSQL -> '$sSQL'";
		$DBReturn=AffectedSQL($sSQL);
//		echo $DBReturn;
		return($DBReturn);
	}

	function playlist_head_option_remove($HeadID,$optALL=NULL) {
		global $DEBUG,$USER;
		
		$sSQL=sprintf("DELETE FROM playlist_head_option");
		if (is_null($optALL)) {
			$sSQL.=sprintf(" WHERE pho_id=%d",$HeadID);
		} else {
			$sSQL.=sprintf(" WHERE pho_plh_id=%d",$HeadID);
		}
		if ($DEBUG) echo "<br>[".__FUNCTION__."] sSQL -> '$sSQL'";
		$DBReturn=AffectedSQL($sSQL);
//		echo $DBReturn;
		return($DBReturn);
	}

	function playlist_line_remove($LineID,$optALL=NULL) {
		global $DEBUG,$USER;
		
		$sSQL=sprintf("DELETE FROM playlist_line");
		if (is_null($optALL)) {
			$sSQL.=sprintf(" WHERE pll_id=%d",$LineID);
		} else {
			$sSQL.=sprintf(" WHERE pll_plh_id=%d",$LineID);
		}
		if ($DEBUG) echo "<br>[".__FUNCTION__."] sSQL -> '$sSQL'";
		$DBReturn=AffectedSQL($sSQL);
//		echo $DBReturn;
		return($DBReturn);
	}
	
	function playlist_line_option_remove($LineID,$optALL=NULL) {
		global $DEBUG,$USER;
		
		$sSQL=sprintf("DELETE FROM playlist_line_option");
		if (is_null($optALL)) {
			$sSQL.=sprintf(" WHERE plo_id=%d",$LineID);
		} else {
			$sSQL.=sprintf(" WHERE plo_pll_id=%d",$LineID);
		}
		if ($DEBUG) echo "<br>[".__FUNCTION__."] sSQL -> '$sSQL'";
		$DBReturn=AffectedSQL($sSQL);
//		echo $DBReturn;
		return($DBReturn);
	}

?>