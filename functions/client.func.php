<?
	function client_list($ComID=1) {
		global $DEBUG,$USER;
		$sSQL=sprintf("SELECT * FROM client_info,group_info WHERE (cli_grp_id=grp_id) AND (cli_com_id=%d) ",$ComID);
		if (strtolower($USER['usr_level']) == 'operator') $sSQL.="AND (cli_active = 1) "; 
		$sSQL.="ORDER BY cli_grp_id,cli_id";
		if ($DEBUG) echo "[".__FUNCTION__."] sSQL -> '$sSQL'<br>";
		$DBReturn=JSonSQL($sSQL);
//		echo $DBReturn;
		return($DBReturn);
	}

	function client_view($ClientID) {
		global $DEBUG,$USER;
		$sSQL=sprintf("SELECT * FROM client_info WHERE (cli_id=%d) ",$ClientID);
		if (strtolower($USER['usr_level']) == 'operator') $sSQL.="AND (cli_active = 1) "; 
		$sSQL.="LIMIT 0,1";
		if ($DEBUG) echo "[".__FUNCTION__."] sSQL -> '$sSQL'<br>";
		$DBReturn=JSonSQL($sSQL);
//		echo $DBReturn;
		return($DBReturn);
	}

	function client_info($ClientID) {
		global $DEBUG,$USER;
		$sSQL=sprintf("SELECT * FROM client_info WHERE (cli_id=%d) ",$ClientID);
		if (strtolower($USER['usr_level']) == 'operator') $sSQL.="AND (cli_active = 1) "; 
		$sSQL.="LIMIT 0,1";
		if ($DEBUG) echo "[".__FUNCTION__."] sSQL -> '$sSQL'<br>";
		$DBReturn=JSonSQL($sSQL);
//		echo $DBReturn;
		return($DBReturn);
	}
	
	function client_add($ComID=1,$Code=NULL,$Name=NULL,$Desc=NULL,$GroupID=NULL,$Pass=NULL,$Address=NULL,$URL=NULL) {
		global $DEBUG,$USER;
		$sSQL=sprintf("INSERT INTO client_info SET cli_active=1, cli_com_id=%d ",$ComID);
		$sSQL.=sprintf(", cli_grp_id=%d ",$GroupID);
		$sSQL.=sprintf(", cli_code='%s' ",$Code);
		$sSQL.=sprintf(", cli_name='%s' ",$Name);
		if (! is_null($Desc)) $sSQL.=sprintf(", cli_desc='%s' ",$Desc);
		$sSQL.=sprintf(", cli_address='%s' ",$Address);
		$sSQL.=sprintf(", cli_syscode='%s' ",$Pass);
		$sSQL.=sprintf(", cli_url='%s' ",$URL);
		if ($DEBUG) echo "[".__FUNCTION__."] sSQL -> '$sSQL'<br>";
		$DBReturn=InsertSQL($sSQL);
		if ($DEBUG) echo "[".__FUNCTION__."] Retrun -> '$DBReturn'<br>";
		return ($DBReturn);
	}

	function client_edit($ClientID,$Code=NULL,$Name=NULL,$Desc=NULL,$GroupID=NULL,$Pass=NULL,$Address=NULL,$URL=NULL) {
		global $DEBUG,$USER;
		$sSQL="UPDATE client_info SET cli_active=1 ";
		if (! is_null($Code)) $sSQL.=sprintf(",cli_code = '%s' ",$Code);
		if (! is_null($Name)) $sSQL.=sprintf(",cli_name = '%s' ",$Name);
		if (! is_null($Desc)) $sSQL.=sprintf(",cli_desc = '%s' ",$Desc);
		if (! is_null($GroupID)) $sSQL.=sprintf(",cli_grp_id = %d ",$GroupID);
		if (! is_null($Pass)) $sSQL.=sprintf(",cli_syscode = '%s' ",$Pass);
		if (! is_null($Address)) $sSQL.=sprintf(",cli_address = '%s' ",$Address);
		if (! is_null($URL)) $sSQL.=sprintf(",cli_url = '%s' ",$URL);
		$sSQL.=sprintf("WHERE cli_id = %d",$ClientID);
		if ($DEBUG) echo "[".__FUNCTION__."] sSQL -> '$sSQL'<br>";
		$DBReturn=AffectedSQL($sSQL);
		if ($DEBUG) echo "[".__FUNCTION__."] Retrun -> '$DBReturn'<br>";
		return ($DBReturn);
	}

	function client_active($ClientID,$Active=0) {
		global $DEBUG,$USER;
		$sSQL=sprintf("UPDATE client_info SET cli_active=%d WHERE cli_id=%d",$Active,$ClientID);
		if ($DEBUG) echo "[".__FUNCTION__."] sSQL -> '$sSQL'<br>";
		$DBReturn=AffectedSQL($sSQL);
		if ($DEBUG) echo "[".__FUNCTION__."] Retrun -> '$DBReturn'<br>";
		return ($DBReturn);
	}

/******/
	
	function user_check_login($ComID=1,$usrLogin) {
		global $DEBUG,$USER;
		$sSQL=sprintf("SELECT * FROM user_info WHERE (usr_com_id=%d ) AND (usr_login='%s') ",$ComID,$usrLogin);
		if ($USER['usr_level'] <> 'Administrator') $sSQL.="AND (usr_active = 1) "; 
		if ($DEBUG) echo "[".__FUNCTION__."] sSQL -> '$sSQL'<br>";
		$DBReturn=ExecuteReader($sSQL);
		$iCount=mysql_num_rows($DBReturn);
		return ($iCount>0);
	}


	function user_change_pass($usrID,$usrOldPass,$usrNewPass) {
		global $DEBUG,$USER;
		$sSQL=sprintf("UPDATE user_info SET usr_passwd=MD5('%s') WHERE (usr_id=%d) AND (usr_passwd=MD5('%s'))",$usrNewPass,$usrID,$usrOldPass);
		if ($DEBUG) echo "[".__FUNCTION__."] sSQL -> '$sSQL'<br>";
		$DBReturn=AffectedSQL($sSQL);
		if ($DEBUG) echo "[".__FUNCTION__."] Retrun -> '$DBReturn'<br>";
		return ($DBReturn);
	}
	
	function user_check_password($usrID,$usrPass) {
		global $DEBUG,$USER;
		$sSQL=sprintf("SELECT * FROM user_info WHERE (usr_id=%d) AND (usr_passwd=MD5('%s')) ",$usrID,$usrPass);
		if ($USER['usr_level'] <> 'Administrator') $sSQL.="AND (usr_active = 1) "; 
		if ($DEBUG) echo "[".__FUNCTION__."] sSQL -> '$sSQL'<br>";
		$DBReturn=ExecuteReader($sSQL);
		$iCount=mysql_num_rows($DBReturn);
		return ($iCount>0);
	}

	function user_reset_pass($usrID,$usrNewPass) {
		global $DEBUG,$USER;
		$sSQL=sprintf("UPDATE user_info SET usr_passwd=MD5('%s') WHERE (usr_id=%d)",$usrNewPass,$usrID);
		if ($DEBUG) echo "[".__FUNCTION__."] sSQL -> '$sSQL'<br>";
		$DBReturn=AffectedSQL($sSQL);
		if ($DEBUG) echo "[".__FUNCTION__."] Retrun -> '$DBReturn'<br>";
		return ($DBReturn);
	}

?>