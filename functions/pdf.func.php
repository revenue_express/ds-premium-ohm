<?
	function pdf_list($Start=0,$PerPage=0,$ComID=1) {
		global $DEBUG,$USER;
		$sSQL="SELECT * FROM file_info WHERE (file_type_id = 10) ";
		if ($USER['usr_level'] =='Operator') $sSQL.="AND (file_active=1)";
		if (($Start >=0) && ($PerPage >0)) {
			$sSQL.=sprintf("LIMIT %d,%d",$Start*$PerPage,$PerPage);
		}
		if ($DEBUG) echo "<br>\n[".__FUNCTION__."] sSQL -> $sSQL";
		$Data=JSonSQL($sSQL);
		if ($DEBUG) { echo "<br>\nData<pre>"; print_r(json_decode($Data,true)); echo "</pre>"; }
		return($Data);
	}

	function pdf_info($FileID) {
		global $DEBUG,$USER;
		$sSQL=sprintf("SELECT * FROM file_info WHERE (file_id=%d) ",$FileID);
		if ($USER['usr_level'] =='Operator') $sSQL.="AND (file_active=1)";
		if ($DEBUG) echo "<br>\n[".__FUNCTION__."] sSQL -> $sSQL";
		$Data=JSonSQL($sSQL);
		if ($DEBUG) { echo "<br>\nData<pre>"; print_r(json_decode($Data,true)); echo "</pre>"; }
		return($Data);
	}

	function playlist_pdf_head_info($HeadID) {
		global $DEBUG,$USER;
		$sSQL=sprintf("SELECT * FROM playlist_head WHERE plh_id=%d ",$HeadID);
		if ($USER['usr_level'] =='Operator') $sSQL.="AND (plh_active=1)";
		if ($DEBUG) echo "<br>\n[".__FUNCTION__."] sSQL -> $sSQL";
		$Data=JSonSQL($sSQL);
		if ($DEBUG) { echo "<br>\nData<pre>"; print_r(json_decode($Data,true)); echo "</pre>"; }
		return($Data);
	}
	
	function playlist_pdf_head_list() {
		global $DEBUG,$USER;
		$sTable="SELECT * , count(pll_id) as counter FROM playlist_line WHERE true ";
		if ($USER['usr_level'] =='Operator') $sTable.="AND (pll_active=1) ";
		$sTable.="GROUP BY pll_plh_id ORDER BY pll_plh_id";
		
		$sSQL=sprintf("SELECT * FROM playlist_head as T1 LEFT JOIN (%s) as T2 ON (T1.plh_id = T2.pll_plh_id) ",$sTable);
		$sSQL.=sprintf("WHERE (T1.plh_type_id = 10) ");
//		if ($USER['usr_level']<> 'Administrator') $sSQL.="AND (T1.plh_active=1)";
		$sSQL.="ORDER BY T1.plh_active DESC,T1.plh_id";
		if ($DEBUG) echo "<br>\n[".__FUNCTION__."] sSQL -> $sSQL";
		$Data=JSonSQL($sSQL);
		if ($DEBUG) { echo "<br>\nData<pre>"; print_r(json_decode($Data,true)); echo "</pre>"; }
		return($Data);
	}

	function playlist_pdf_line_list($PlayID) {
		global $DEBUG,$USER;
		$sSQL=sprintf("SELECT * FROM playlist_line,file_info WHERE (pll_file_id = file_id) ");
		$sSQL.=sprintf("AND (pll_plh_id=%d) ",$PlayID);
		if ($USER['usr_level'] =='Operator') $sSQL.="AND (pll_active=1) AND (file_active=1)";
		$sSQL.="ORDER BY pll_active DESC,pll_order,pll_id";
		if ($DEBUG) echo "<br>\n[".__FUNCTION__."] sSQL -> $sSQL";
		$Data=JSonSQL($sSQL);
		if ($DEBUG) { echo "<br>\nData<pre>"; print_r(json_decode($Data,true)); echo "</pre>"; }
		return($Data);
	}
	
	function playlist_used($PlayID) {
		global $DEBUG,$USER;
		$sSQL="SELECT count(*) as Counter FROM playlist_head,layout_head,layout_line ";
		$sSQL.="WHERE (layl_plh_id= plh_id) AND (layh_id = layl_layh_id) AND (layl_layh_id = layh_id)  ";
//		$sSQL.="AND (layh_active = 1) AND (layl_active =1) ";
		$sSQL.=sprintf("AND (plh_id = %d) ",$PlayID);
		if ($DEBUG) echo "[".__FUNCTION__."] sSQL -> '$sSQL'<br>";
		$DBReturn=ExecuteReader($sSQL);
		if ($Data=mysql_fetch_array($DBReturn)) {
//		echo $DBReturn;
			return($Data[0]['Counter']);
		} else {
			return 0;
		}
	}

	function playlist_pdf_delete($PlaylistID) {
		global $DEBUG,$USER;
		$Return="";
//	list playlist line Option
		$PLLInfo=json_decode(playlist_pdf_line_list($PlaylistID),true);
		for ($iCount=0; $iCount<count($PLLInfo); $iCount++) {
//		Delete Playlist Line Option
			$sSQL=sprintf("DELETE FROM playlist_line_option WHERE plo_pll_id=%d ",$PLLInfo[$iCount]['pll_id']);
			if ($DEBUG) echo "[".__FUNCTION__."] sSQL -> '$sSQL'<br>";
			$DBCount=AffectedSQL($sSQL);
			if ($DEBUG)	$Return.=sprintf("\nDelete %d Option(s) in this Playlist Item.",$DBCount);
		}
// Delete Playlist Line
		$sSQL=sprintf("DELETE FROM playlist_line WHERE pll_plh_id=%d ",$PlaylistID);
		if ($DEBUG) echo "[".__FUNCTION__."] sSQL -> '$sSQL'<br>";
		$DBCount=AffectedSQL($sSQL);
		if ($DEBUG)	$Return.=sprintf("\nDelete %d Item(s) in this playlist.",$DBCount);

//	Delete Playlist Head Option
		$sSQL=sprintf("DELETE FROM playlist_head_option WHERE pho_plh_id=%d ",$PlaylistID);
		if ($DEBUG) echo "[".__FUNCTION__."] sSQL -> '$sSQL'<br>";
		$DBCount=AffectedSQL($sSQL);
		if ($DEBUG)	$Return.=sprintf("\nDelete %d Option(s) in this Playlist.",$DBCount);

//	Delete Schedule Head
		$sSQL=sprintf("DELETE FROM playlist_head WHERE plh_id=%d ",$PlaylistID);
		if ($DEBUG) echo "[".__FUNCTION__."] sSQL -> '$sSQL'<br>";
		$DBCount=AffectedSQL($sSQL);
		if ($DBCount ==1) {
			$Return.=sprintf("\n\nDelete this playlist successful.",$DBCount);
		} else {
			$Return.=sprintf("\n\nDelete this playlist failed.",$DBCount);
		}
		return $Return;
	}

	

?>