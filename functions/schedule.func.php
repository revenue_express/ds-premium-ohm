<?
	function schedule_list($ComID=1,$login=1, $limit=0, $spage=0, $order=NULL) {
		global $DEBUG,$USER;

		$sItem="SELECT *,COUNT(*) as sum_items FROM schedule_line WHERE (schl_id >0) ";
		if ($USER['usr_level'] == 'operator') $sItem.="AND (schl_active = 1)";
		$sItem.="GROUP BY schl_schh_id";

		$sHead="SELECT *,concat(disp_width,' X ',disp_height) as disp_size FROM schedule_head,display_info WHERE (schh_disp_id = disp_id) ";
//		if ($USER['usr_level'] <> 'Administrator') $sHead.="AND (schh_active = 1) AND (disp_active =1) ";

		$sSQL=sprintf("SELECT * FROM  (%s) AS T1 LEFT JOIN (%s) AS T2 ON T1.schh_id = T2.schl_schh_id  WHERE schh_com_id=%d ",$sHead,$sItem,$ComID);
		
		if (! is_null($order)) {
			$sSQL.=sprintf(" ORDER BY %s",$order);
		} else {
			$sSQL.=sprintf(" ORDER BY schh_name");
		}
		
		if ( ($limit == 0) && ($spage == 0) ) {
			$sSQL.="";
		} else {
			$sSQL.=sprintf(" LIMIT %d,%d",$spage*$limit,$limit);
		}
		if ($DEBUG) echo "<br>[".__FUNCTION__."] sSQL -> $sSQL<br>";
		$JSonData=JSonSQL($sSQL);
//		echo $DBReturn;
		return($JSonData);
	}
	
	function schedule_period_list($schID) {
		global $DEBUG,$USER;
		
		$sSQL=sprintf("SELECT * FROM schedule_period WHERE schp_schh_id = %d ",$schID);
		if ($USER['usr_level'] <> 'Administrator') $sSQL.="AND (schp_active = 1) ";
		$sSQL.="ORDER BY schp_active DESC,schp_start,schp_stop";
		
		if ($DEBUG) echo "<br>[".__FUNCTION__."] sSQL -> $sSQL<br>";
		$JSonData=JSonSQL($sSQL);
//		echo $DBReturn;
		return($JSonData);
	}

	
	function schedule_dummyID($ComID=1) {
		global $DEBUG,$USER;
		$sHead=sprintf("%s",date('ymd'));
		$sSQL=sprintf("SELECT * FROM  schedule_head WHERE (schh_name like '%s%%') ",$sHead);
		$sSQL.=sprintf("AND (schh_com_id=%d) ",$ComID);
		$sSQL.=sprintf("ORDER BY schh_name DESC LIMIT 1 ");
		if ($DEBUG) echo "<br>[".__FUNCTION__."] sSQL -> $sSQL";
		$iCount=1;
		$Result=ExecuteReader($sSQL);
		if ($Data=mysql_fetch_array($Result)) {
			$Num=$Data['schh_name'];
			if ($DEBUG) echo "<br>[".__FUNCTION__."] schh_name -> '$Num'<br>";
			$iCount=intval(substr($Num,strlen($sHead)));
			$iCount++;
			if ($DEBUG) echo "<br>[".__FUNCTION__."] iCount -> '$iCount'<br>";
		}
		$Code=sprintf("%s%03d",$sHead,$iCount);
			if ($DEBUG) echo "<br>[".__FUNCTION__."] Return -> '$Code'<br>";
		return($Code);		
	}
	
	function schedule_head_add($ComID=1,$HeadName,$DispID=1) {
		global $DEBUG,$USER;
		$sSQL="INSERT INTO schedule_head SET ";
		$sSQL.=sprintf("schh_name = '%s'",$HeadName);
		$sSQL.=sprintf(",schh_disp_id = %d",$DispID);
		$sSQL.=sprintf(",schh_com_id = %d",$ComID);
		if ($DEBUG) echo "<br>[".__FUNCTION__."] sSQL -> $sSQL";
		$InsertID=InsertSQL($sSQL);
		return ($InsertID);
	}
	
	function schedule_head_view($schID) {
		global $DEBUG,$USER;
		$sSQL=sprintf("SELECT * FROM schedule_head,display_info WHERE (schh_disp_id = disp_id ) AND (schh_id = %d) ",$schID);
		if ($USER['usr_level'] <> 'Administrator') $sSQL.="AND (schh_active = 1) ";
		
		if ($DEBUG) echo "<br>[".__FUNCTION__."] sSQL -> $sSQL<br>";
		$JSonData=JSonSQL($sSQL);
//		echo $DBReturn;
		return($JSonData);
	}

	function schedule_head_edit($schID,$schName=NULL,$DispID=NULL) {
		global $DEBUG,$USER;
		$sSQL="UPDATE schedule_head SET schh_active = 1 ";
		if (! is_null($schName)) $sSQL.=sprintf(", schh_name = \"%s\" ",$schName);
		if (! is_null($DispID)) $sSQL.=sprintf(", schh_disp_id = %d ",$DispID);
		$sSQL.=sprintf("WHERE (schh_id = %d) ",$schID);
		if ($USER['usr_level'] <> 'Administrator') $sSQL.="AND (schh_active = 1)";
		
		if ($DEBUG) echo "<br>[".__FUNCTION__."] sSQL -> $sSQL<br>";
		$Count=AffectedSQL($sSQL);
//		echo $DBReturn;
		return($Count);
	}
	function schedule_period_add($schID,$schStart=NULL,$schStop=NULL) {
		global $DEBUG,$USER;
		list($myDate,$myMonth,$myYear)=split("/",$schStart);
		$Start=sprintf("%04d-%02d-%02d",$myYear,$myMonth,$myDate);
		list($myDate,$myMonth,$myYear)=split("/",$schStop);
		$Stop=sprintf("%04d-%02d-%02d",$myYear,$myMonth,$myDate);
		$sSQL="INSERT INTO schedule_period SET ";
		$sSQL.=sprintf("schp_schh_id = %d",$schID);
		$sSQL.=sprintf(",schp_start = '%s'",$Start);
		$sSQL.=sprintf(",schp_stop = '%s'",$Stop);
		if ($DEBUG) echo "<br>[".__FUNCTION__."] sSQL -> $sSQL";
		$InsertID=InsertSQL($sSQL);
		return ($InsertID);
	}

	function schedule_period_edit($PeriodID,$schStart=NULL,$schStop=NULL) {
		global $DEBUG,$USER;
		list($myDate,$myMonth,$myYear)=split("/",$schStart);
		$Start=sprintf("%04d-%02d-%02d",$myYear,$myMonth,$myDate);
		list($myDate,$myMonth,$myYear)=split("/",$schStop);
		$Stop=sprintf("%04d-%02d-%02d",$myYear,$myMonth,$myDate);

		$sSQL="UPDATE schedule_period SET schp_active = 1 ";
		if (! is_null($schStart)) $sSQL.=sprintf(", schp_start = '%s' ",$Start);
		if (! is_null($schStop)) $sSQL.=sprintf(", schp_stop = '%s' ",$Stop);
		$sSQL.=sprintf("WHERE (schp_id = %d) ",$PeriodID);
		if ($USER['usr_level'] <> 'Administrator') $sSQL.="AND (schp_active = 1)";
		
		if ($DEBUG) echo "<br>[".__FUNCTION__."] sSQL -> $sSQL<br>";
		$Count=AffectedSQL($sSQL);
//		echo $DBReturn;
		return($Count);
	}

	function schedule_period_active($PeriodID,$Active=0) {
		global $DEBUG,$USER;
		
		$sSQL=sprintf("UPDATE schedule_period SET schp_active = %d ",$Active);
		$sSQL.=sprintf("WHERE (schp_id = %d) ",$PeriodID);
		
		if ($DEBUG) echo "<br>[".__FUNCTION__."] sSQL -> $sSQL<br>";
		$Count=AffectedSQL($sSQL);
//		echo $DBReturn;
		return($Count);
	}

	function schedule_layout_list($schID,$Group=0) {
		global $DEBUG,$USER;
		
		$sSQL="SELECT * FROM schedule_line,layout_head,template_head ";
		$sSQL.="WHERE (schl_layh_id = layh_id)  AND (layh_tmph_id = tmph_id) ";
		$sSQL.=sprintf("AND (schl_schh_id=%d) ",$schID);
//		if ($USER['usr_level'] <> 'Administrator') $sSQL.="AND (schl_active=1) AND (layh_active=1) AND (tmph_active=1) ";
//		$sSQL.="ORDER BY schl_active DESC,schl_start,schl_stop";
		if ($Group == 1) {
// User for generate file list
			$sSQL.="GROUP BY layh_id ";
		}
		$sSQL.="ORDER BY schl_start,schl_stop";
		
		if ($DEBUG) echo "<br>[".__FUNCTION__."] sSQL -> $sSQL<br>";
		$JSonData=JSonSQL($sSQL);
//		echo $DBReturn;
		return($JSonData);
	}

	function schedule_layout_edit($LineID,$LayoutID,$Sun=0,$Mon=0,$Tue=0,$Wed=0,$Thu=0,$Fri=0,$Sat=0,$Start='00:00:00',$Stop='23:59:59') {
		global $DEBUG,$USER;
		
		$sSQL=sprintf("UPDATE schedule_line SET schl_active = 1 ");
		$sSQL.=sprintf(", schl_layh_id= %d",$LayoutID);
		$sSQL.=sprintf(", schl_start= '%s'",$Start);
		$sSQL.=sprintf(", schl_stop= '%s'",$Stop);
		$sSQL.=sprintf(", schl_sunday= %d",$Sun);
		$sSQL.=sprintf(", schl_monday= %d",$Mon);
		$sSQL.=sprintf(", schl_tuesday= %d",$Tue);
		$sSQL.=sprintf(", schl_wednesday= %d",$Wed);
		$sSQL.=sprintf(", schl_thursday= %d",$Thu);
		$sSQL.=sprintf(", schl_friday= %d",$Fri);
		$sSQL.=sprintf(", schl_saturday= %d",$Sat);
		$sSQL.=sprintf(" WHERE (schl_id = %d) ",$LineID);
		
		if ($DEBUG) echo "<br>[".__FUNCTION__."] sSQL -> $sSQL<br>";
		$Count=AffectedSQL($sSQL);
//		echo $DBReturn;
		return($Count);
	}

	function schedule_layout_add($SchID,$LayoutID,$Sun=0,$Mon=0,$Tue=0,$Wed=0,$Thu=0,$Fri=0,$Sat=0,$Start='00:00:00',$Stop='23:59:59') {
		global $DEBUG,$USER;
		
		$sSQL=sprintf("INSERT INTO schedule_line SET schl_active = 1 ");
		$sSQL.=sprintf(", schl_schh_id= %d",$SchID);
		$sSQL.=sprintf(", schl_layh_id= %d",$LayoutID);
		$sSQL.=sprintf(", schl_start= '%s'",$Start);
		$sSQL.=sprintf(", schl_stop= '%s'",$Stop);
		$sSQL.=sprintf(", schl_sunday= %d",$Sun);
		$sSQL.=sprintf(", schl_monday= %d",$Mon);
		$sSQL.=sprintf(", schl_tuesday= %d",$Tue);
		$sSQL.=sprintf(", schl_wednesday= %d",$Wed);
		$sSQL.=sprintf(", schl_thursday= %d",$Thu);
		$sSQL.=sprintf(", schl_friday= %d",$Fri);
		$sSQL.=sprintf(", schl_saturday= %d",$Sat);
		
		if ($DEBUG) echo "<br>[".__FUNCTION__."] sSQL -> $sSQL<br>";
		$InsertID=InsertSQL($sSQL);
		return ($InsertID);
	}
	
	function schedule_layout_active($LineID,$Active=0) {
		global $DEBUG,$USER;
		$sSQL=sprintf("UPDATE schedule_line SET schl_active = %d ",$Active);
		$sSQL.=sprintf(" WHERE (schl_id = %d) ",$LineID);
		
		if ($DEBUG) echo "<br>[".__FUNCTION__."] sSQL -> $sSQL<br>";
		$Count=AffectedSQL($sSQL);
//		echo $DBReturn;
		return($Count);
	}

	function schedule_layout_delete($LineID) {
		global $DEBUG,$USER;
		$sSQL=sprintf("DELETE FROM schedule_line WHERE (schl_id = %d) ",$LineID);
		if ($DEBUG) echo "<br>[".__FUNCTION__."] sSQL -> $sSQL<br>";
		$Count=AffectedSQL($sSQL);
//		echo $DBReturn;
		return($Count);
	}

	function schedule_active($schID,$Active=0) {
		global $DEBUG,$USER;
		$sSQL=sprintf("UPDATE schedule_head SET schh_active = %d ",$Active);
		$sSQL.=sprintf(" WHERE (schh_id = %d) ",$schID);
		
		if ($DEBUG) echo "<br>[".__FUNCTION__."] sSQL -> $sSQL<br>";
		$Count=AffectedSQL($sSQL);
//		echo $DBReturn;
		return($Count);
	}

	function schedule_used($ScheduleID) {
		global $DEBUG,$USER;
		$sSQL=sprintf("SELECT count(*) as Counter FROM schedule_client WHERE (schc_schh_id = %d) ",$ScheduleID);
		if ($DEBUG) echo "[".__FUNCTION__."] sSQL -> '$sSQL'<br>";
		$DBReturn=ExecuteReader($sSQL);
		if ($Data=mysql_fetch_array($DBReturn)) {
//		echo $DBReturn;
			return($Data[0]['Counter']);
		} else {
			return 0;
		}
	}

	function schedule_delete($ScheduleID) {
		global $DEBUG,$USER;
		$Return="";
//	Delete Schedule Line
		$sSQL=sprintf("DELETE FROM schedule_line WHERE schl_schh_id=%d ",$ScheduleID);
		if ($DEBUG) echo "[".__FUNCTION__."] sSQL -> '$sSQL'<br>";
		$DBCount=AffectedSQL($sSQL);
		if ($DEBUG)	$Return.=sprintf("\nDelete %d template in this schedule.",$DBCount);
//	Delete Schedule Period
		$sSQL=sprintf("DELETE FROM schedule_period WHERE schp_schh_id=%d ",$ScheduleID);
		if ($DEBUG) echo "[".__FUNCTION__."] sSQL -> '$sSQL'<br>";
		$DBCount=AffectedSQL($sSQL);
		if ($DEBUG) $Return.=sprintf("\nDelete %d period in this schedule.",$DBCount);
//	Delete Schedule Head
		$sSQL=sprintf("DELETE FROM schedule_head WHERE schh_id=%d ",$ScheduleID);
		if ($DEBUG) echo "[".__FUNCTION__."] sSQL -> '$sSQL'<br>";
		$DBCount=AffectedSQL($sSQL);
		if ($DBCount ==1) {
			$Return.=sprintf("\n\nDelete this schedule successful.",$DBCount);
		} else {
			$Return.=sprintf("\n\nDelete this schedule failed.",$DBCount);
		}
		return $Return;
	}
?>