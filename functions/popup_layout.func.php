<?
	function popup_layout_list($schID,$ComID=1) {
		global $DEBUG,$USER;

		$sSQL="SELECT *,concat(disp_width,' X ',disp_height) as DispSize ";
		$sSQL.="FROM layout_head,template_head,display_info ";
		$sSQL.="WHERE (layh_tmph_id = tmph_id) AND (tmph_disp_id = disp_id) ";
		$sSQL.="AND (layh_active =1) AND (tmph_active=1) AND (disp_active =1) ";
		$sSQL.=sprintf("AND (layh_com_id =%d) AND (tmph_com_id=%d) ",$ComID,$ComID);
		$sSQL.="AND disp_id = (SELECT schh_disp_id FROM schedule_head ";
		$sSQL.=sprintf("WHERE schh_id=%d)",$schID); 		
		if ($DEBUG) echo "<br>[".__FUNCTION__."] sSQL -> $sSQL";
		$Data=JSonSQL($sSQL);
		return($Data);
	}
?>