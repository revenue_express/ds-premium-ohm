<?

	function template_head_list($comID=1,$login=1, $limit=0, $spage=0, $order='tmph_active DESC,tmph_name') {
		global $DEBUG,$Config,$ErrorMsg,$DBReturn,$USER;
		$sSQL=sprintf("SELECT * FROM  template_head LEFT JOIN display_info ON disp_id = tmph_disp_id WHERE (tmph_com_id = %d) ",$comID);
//		if ($USER['usr_level'] <> 'Administrator') $sSQL.="AND (tmph_active = 1) "; 
		$sSQL.=sprintf(" ORDER BY %s",$order);

		if ( ($limit == 0) && ($spage == 0) ) {
			$sSQL.="";
		} else {
			$sSQL.=sprintf(" LIMIT %d,%d",$spage*$limit,$limit);
		}
		if ($DEBUG) echo "<br>[".__FUNCTION__."] sSQL -> $sSQL";
		$JSonData=JSonSQL($sSQL);
//		echo $DBReturn;
		return($JSonData);
	}

	function template_head_dummyID($comID=1) {
		global $DEBUG,$USER;
		$sHead=sprintf("%s",date('ymd'));
		$sSQL=sprintf("SELECT * FROM  template_head WHERE (tmph_name like '%s%%') ",$sHead);
		$sSQL.=sprintf("AND (tmph_com_id=%d) ",$comID);
		$sSQL.=sprintf("ORDER BY tmph_name DESC LIMIT 1 ");
		if ($DEBUG) echo "<br>[".__FUNCTION__."] sSQL -> $sSQL";
		$iCount=1;
		$Result=ExecuteReader($sSQL);
		if ($Data=mysql_fetch_array($Result)) {
			$Num=$Data['tmph_name'];
			if ($DEBUG) echo "[".__FUNCTION__."] tmph_name -> '$Num'<br>";
			$iCount=intval(substr($Num,strlen($sHead)));
			$iCount++;
			if ($DEBUG) echo "[".__FUNCTION__."] iCount -> '$iCount'<br>";
		}
		$Code=sprintf("%s%03d",$sHead,$iCount);
			if ($DEBUG) echo "[".__FUNCTION__."] Return -> '$Code'<br>";
		return($Code);		
	}

	function template_head_add($ComID=1,$DispID=1,$Name=NULL,$Desc=NULL) {
		global $DEBUG,$Config,$ErrorMsg,$DBReturn,$USER;

		$sSQL="INSERT INTO template_head SET ";
		$sSQL.=" tmph_active = 1";
		if (isset($ComID)) $sSQL.=sprintf(",tmph_com_id=%d ",$ComID);
		if (isset($DispID)) $sSQL.=sprintf(",tmph_disp_id=%d ",$DispID);
		if (is_null($Name)) $Name=template_head_dummyID($ComID);
		if (isset($Name)) $sSQL.=sprintf(",tmph_name='%s' ",$Name);
		if (! is_null($Desc)) $sSQL.=sprintf(",tmph_desc='%s' ",$Desc);
		if ($DEBUG) echo "<br>[".__FUNCTION__."] sSQL -> $sSQL";
		$InsertID=InsertSQL($sSQL);
		if ($DEBUG) echo "<li>[".__FUNCTION__."] Return -> $InsertID<br>";
		return ($InsertID);
	}
	
	function template_head_edit($ComID,$LastID,$DispID,$Name,$Desc) {
		global $DEBUG,$Config,$ErrorMsg,$DBReturn,$USER;

		$sSQL="UPDATE template_head SET tmph_active = 1";
		if (isset($ComID)) $sSQL.=sprintf(",tmph_com_id=%d ",$ComID);
		if (isset($DispID)) $sSQL.=sprintf(",tmph_disp_id=%d ",$DispID);
		if (isset($Name)) $sSQL.=sprintf(",tmph_name='%s' ",$Name);
		if (isset($Desc)) $sSQL.=sprintf(",tmph_desc='%s' ",$Desc);
		$sSQL.=sprintf("WHERE tmph_id=%d",$LastID);
		if ($DEBUG) echo "<br>[".__FUNCTION__."] sSQL -> $sSQL";
		$rCount=AffectedSQL($sSQL);
		if ($rCount = 1) {
			return(TRUE);
		}
		return(FALSE);
	}
	
	function template_head_active($tmph_id,$active=0) {
		global $DEBUG,$Config,$ErrorMsg,$DBReturn,$USER;

		$sSQL=sprintf("UPDATE template_head SET tmph_active = %d WHERE tmph_id = %d",$active,$tmph_id);
		if ($DEBUG) echo "<br>[".__FUNCTION__."] sSQL -> $sSQL";
		$rCount=AffectedSQL($sSQL);
		if ($rCount = 1) {
			return(TRUE);
		}
		return(FALSE);
	}
	
	function template_line_list($comID=1,$HeadID=1,$login=1, $ppage=0, $limit=0, $spage=0) {
		global $DEBUG,$USER;
		$sSQL=sprintf("SELECT * FROM template_line,type_info WHERE (tmpl_type_id=type_id) AND (tmpl_tmph_id = %d) ",$HeadID);
		if ($USER['usr_level'] == 'Operator') $sSQL.="AND (tmpl_active = 1) "; 
		$sSQL.="ORDER by tmpl_active DESC, tmpl_order";
		if ( ($ppage <> 0) && ($limite<> 0) && ($spage <> 0) ) {
			$sSQL.=sprintf(" LIMIT %d,%d",$spage*$limit,$limit);
		}
		if ($DEBUG) echo "<br>[".__FUNCTION__."] sSQL -> $sSQL";
		$DBReturn=JSonSQL($sSQL);
//		echo $DBReturn;
		return($DBReturn);
	}

/*
	function template_line_list($comID=1,$login=1,$tmph_ID, $limit=0, $spage=0, $order='tmpl_order') {
		global $DEBUG,$Config,$ErrorMsg,$DBReturn,$USER;
		$sSQL=sprintf("SELECT * FROM  template_line WHERE (tmph_active = 1) AND (tmph_com_id = %d) ",$comID);
		$sSQL.=sprintf("AND (tmpl_tmph_id=%d) ",$tmph_ID);
		$sSQL.=sprintf("ORDER BY %s",$order);

		if ( ($limit == 0) && ($spage == 0) ) {
			$sSQL.="";
		} else {
			$sSQL.=sprintf(" LIMIT %d,%d",$spage*$limit,$limit);
		}
		if ($DEBUG) echo "<li>[".__FUNCTION__."] sSQL -> $sSQL<br>";
		$JSonData=JSonSQL($sSQL);
//		echo $DBReturn;
		return($JSonData);
	}
*/
	function template_line_add($HeadID,$TypeID,$Name,$Order,$Left=0,$Top=0,$Width=0,$Height=0) {
		global $DEBUG,$Config,$ErrorMsg,$DBReturn,$USER;

		$sSQL="INSERT INTO template_line SET ";
		$sSQL.=" tmpl_active = 1";
		if (isset($HeadID)) $sSQL.=sprintf(",tmpl_tmph_id=%d ",$HeadID);
		if (isset($TypeID)) $sSQL.=sprintf(",tmpl_type_id=%d ",$TypeID);
		if (isset($Name)) $sSQL.=sprintf(",tmpl_name='%s' ",$Name);
//		if (isset($Desc)) $sSQL.=sprintf(",tmpl_desc='%s' ",$Desc);
		if (isset($Order)) $sSQL.=sprintf(",tmpl_order=%d ",$Order);
		if (isset($Left)) $sSQL.=sprintf(",tmpl_left=%d ",$Left);
		if (isset($Top)) $sSQL.=sprintf(",tmpl_top=%s ",$Top);
		if (isset($Width)) $sSQL.=sprintf(",tmpl_width=%d ",$Width);
		if (isset($Height)) $sSQL.=sprintf(",tmpl_height=%d ",$Height);
		if ($DEBUG) echo "<br>[".__FUNCTION__."] sSQL -> $sSQL";
		$InsertID=InsertSQL($sSQL);
		return ($InsertID);
	}

	function template_line_edit($LastID,$HeadID=0,$TypeID=0,$Name=NULL,$Order=-9999,$Left=-9999,$Top=-9999,$Width=-9999,$Height=-9999) {
		global $DEBUG,$Config,$ErrorMsg,$DBReturn,$USER;

		$sSQL="UPDATE template_line SET tmpl_active = 1";
		if ($HeadID > 0) $sSQL.=sprintf(",tmpl_tmph_id=%d ",$HeadID);
		if ($TypeID > 0) $sSQL.=sprintf(",tmpl_type_id=%d ",$TypeID);
		if (! is_null($Name)) $sSQL.=sprintf(",tmpl_name='%s' ",$Name);
//		if (isset($Desc)) $sSQL.=sprintf(",tmpl_desc='%s' ",$Desc);
		if ($Order > -9999) $sSQL.=sprintf(",tmpl_order=%d ",$Order);
		if ($Left > -9999) $sSQL.=sprintf(",tmpl_left=%d ",$Left);
		if ($Top > -9999) $sSQL.=sprintf(",tmpl_top=%s ",$Top);
		if ($Width > -9999) $sSQL.=sprintf(",tmpl_width=%d ",$Width);
		if ($Height > -9999) $sSQL.=sprintf(",tmpl_height=%d ",$Height);

		$sSQL.=sprintf("WHERE tmpl_id=%d",$LastID);
		if ($DEBUG) echo "<br>[".__FUNCTION__."] sSQL -> $sSQL";
		$rCount=AffectedSQL($sSQL);
		if ($rCount = 1) {
			return(TRUE);
		}
		return(FALSE);
	}

	function template_line_active($LineID,$active=0) {
		global $DEBUG,$Config,$ErrorMsg,$DBReturn,$USER;

		$sSQL=sprintf("UPDATE template_line SET tmpl_active = %d WHERE tmpl_id = %d",$active,$LineID);
		if ($DEBUG) echo "<br>[".__FUNCTION__."] sSQL -> $sSQL";
		$rCount=AffectedSQL($sSQL);
		if ($rCount = 1) {
			return(TRUE);
		}
		return(FALSE);
	}

	function template_line_delete($LineID) {
		global $DEBUG,$Config,$ErrorMsg,$DBReturn,$USER;

		$sSQL=sprintf("DELETE FROM template_line WHERE tmpl_id=%d ",$LineID);
		if ($DEBUG) echo "<br>[".__FUNCTION__."] sSQL -> $sSQL";
		$rCount=AffectedSQL($sSQL);
		if ($rCount = 1) {
			return(TRUE);
		}
		return(FALSE);
	}

	function template_head_view($TemplateID) {
		global $DEBUG,$Config,$ErrorMsg,$DBReturn,$USER;
		$sSQL=sprintf("SELECT * FROM  template_head LEFT JOIN display_info ON disp_id = tmph_disp_id WHERE tmph_id=%d ",$TemplateID);
		if ($DEBUG) echo "<br>[".__FUNCTION__."] sSQL -> $sSQL";
		$DBReturn=JSonSQL($sSQL);
//		echo $DBReturn;
		return($DBReturn);
	}

	function template_head_info($TemplateID) {
		global $DEBUG,$Config,$ErrorMsg,$DBReturn,$USER;
		$sSQL=sprintf("SELECT * FROM  template_head LEFT JOIN display_info ON disp_id = tmph_disp_id WHERE tmph_id=%d ",$TemplateID);
		if ($DEBUG) echo "<br>[".__FUNCTION__."] sSQL -> $sSQL";
		$DBReturn=JSonSQL($sSQL);
//		echo $DBReturn;
		return($DBReturn);
	}

	function template_line_count($HeadID) {
		global $DEBUG,$USER;
		
		$DBReturn=0;
		$sSQL=sprintf("SELECT count(*) as counter FROM template_line ");
		$sSQL.=sprintf("WHERE tmpl_tmph_id=%d ",$HeadID);
		$sSQL.="AND (tmpl_active = 1) ";
//		if ($USER['usr_level'] == 'Operator') $sSQL.="AND (tmpl_active = 1) "; 
		if ($DEBUG) echo "<br>[".__FUNCTION__."] sSQL -> $sSQL";
		$Result=ExecuteReader($sSQL);
		if ($Data = mysql_fetch_array($Result)) {
				$DBReturn=$Data['counter'];
		}
		return($DBReturn);
	}

	function template_show_list($ComID=1) {
		global $DEBUG,$USER;
		$s1=sprintf("SELECT * FROM  template_head ,display_info WHERE (tmph_disp_id = disp_id) and (tmph_com_id=%d) ",$ComID);
		$s2="SELECT tmpl_tmph_id, COUNT( tmpl_tmph_id ) AS counter FROM  `template_line` GROUP BY tmpl_tmph_id ";
		$sSQL=sprintf("SELECT * FROM (%s) AS T1,(%s) AS T2  WHERE T1.tmph_id = T2.tmpl_tmph_id ",$s1,$s2);
		if ($USER['usr_level'] == 'Operator') $sSQL.="AND (tmph_active = 1) "; 
		$sSQL.="ORDER BY tmph_id ";
		if ($DEBUG) echo "\n<br>[".__FUNCTION__."] sSQL -> $sSQL";
		$Data=JSonSQL($sSQL);
//		echo $DBReturn;
		return($Data);
		
	}
	
	function template_used($TID) {
		global $DEBUG,$USER;
		$sSQL="SELECT count(*) as counter FROM template_head,layout_head WHERE (tmph_id = layh_tmph_id) ";
		$sSQL.=sprintf("AND (tmph_id=%d)",$TID);
		if ($DEBUG) echo "<br>[".__FUNCTION__."] sSQL -> $sSQL";
		$Result=ExecuteReader($sSQL);
		if ($Data = mysql_fetch_array($Result)) {
				$DBReturn=$Data['counter'];
		}
		return($DBReturn);
	}

	function template_delete($TID) {
		global $DEBUG,$USER;
		$Return="";
// Delete Layout Line
		$sSQL=sprintf("DELETE FROM template_line WHERE tmpl_tmph_id=%d ",$TID);
		if ($DEBUG) echo "[".__FUNCTION__."] sSQL -> '$sSQL'<br>";
		$DBCount=AffectedSQL($sSQL);
		if ($DEBUG)	$Return.=sprintf("\nDelete %d object(s) in this layout.",$DBCount);

// Delete Layout Cover 
		$sSQL=sprintf("DELETE FROM template_cover WHERE tmpc_tmph_id=%d ",$TID);
		if ($DEBUG) echo "[".__FUNCTION__."] sSQL -> '$sSQL'<br>";
		$DBCount=AffectedSQL($sSQL);
		if ($DEBUG)	$Return.=sprintf("\nDelete %d cover in this layout.",$DBCount);

//	Delete Schedule Head
		$sSQL=sprintf("DELETE FROM template_head WHERE tmph_id=%d ",$TID);
		if ($DEBUG) echo "[".__FUNCTION__."] sSQL -> '$sSQL'<br>";
		$DBCount=AffectedSQL($sSQL);
		if ($DBCount ==1) {
			$Return.=sprintf("\n\nDelete this playlist successful.",$DBCount);
		} else {
			$Return.=sprintf("\n\nDelete this playlist failed.",$DBCount);
		}
		return $Return;
	}

	function template_option_add($LineID,$Name="Header",$Layer=100,$Lt=0,$Tp=0,$Wt=0,$Ht=0,$Op=1,$Co='#000000',$Bg='#FFFFFF',$Bd=0) {
		global $DEBUG,$Config,$ErrorMsg,$DBReturn,$USER;

		$sSQL="INSERT INTO template_line_option SET ";
		$sSQL.=" tplo_active = 1";
		if (isset($LineID)) $sSQL.=sprintf(",tplo_tmpl_id=%d ",$LineID);
		if (isset($Name)) $sSQL.=sprintf(",tplo_name='%s' ",$Name);
		if (isset($Layer)) $sSQL.=sprintf(",tplo_order=%d ",$Layer);
		if (isset($Lt)) $sSQL.=sprintf(",tplo_left=%d ",$Lt);
		if (isset($Tp)) $sSQL.=sprintf(",tplo_top=%s ",$Tp);
		if (isset($Wt)) $sSQL.=sprintf(",tplo_width=%d ",$Wt);
		if (isset($Ht)) $sSQL.=sprintf(",tplo_height=%d ",$Ht);
		if (isset($Op)) $sSQL.=sprintf(",tplo_opacity=%d ",$Op);
		if (isset($Co)) $sSQL.=sprintf(",tplo_color='%s' ",$Co);
		if (isset($Bg)) $sSQL.=sprintf(",tplo_bgcolor='%s' ",$Bg);
		if (isset($Bd)) $sSQL.=sprintf(",tplo_border='%s' ",$Bd);
		if ($DEBUG) echo "<br>[".__FUNCTION__."] sSQL -> $sSQL";
		$InsertID=InsertSQL($sSQL);
		return ($InsertID);
	}

	function template_option_update($OptID,$Name="Header",$Layer=100,$Lt=0,$Tp=0,$Wt=0,$Ht=0,$Op=1,$Co='#000000',$Bg='#FFFFFF',$Bd=0) {
		global $DEBUG,$Config,$ErrorMsg,$DBReturn,$USER;

		$sSQL="UPDATE template_line_option SET ";
		$sSQL.=" tplo_active = 1";
		if (isset($Name)) $sSQL.=sprintf(",tplo_name='%s' ",$Name);
		if (isset($Layer)) $sSQL.=sprintf(",tplo_order=%d ",$Layer);
		if (isset($Lt)) $sSQL.=sprintf(",tplo_left=%d ",$Lt);
		if (isset($Tp)) $sSQL.=sprintf(",tplo_top=%s ",$Tp);
		if (isset($Wt)) $sSQL.=sprintf(",tplo_width=%d ",$Wt);
		if (isset($Ht)) $sSQL.=sprintf(",tplo_height=%d ",$Ht);
		if (isset($Op)) $sSQL.=sprintf(",tplo_opacity=%d ",$Op);
		if (isset($Co)) $sSQL.=sprintf(",tplo_color='%s' ",$Co);
		if (isset($Bg)) $sSQL.=sprintf(",tplo_bgcolor='%s' ",$Bg);
		if (isset($Bd)) $sSQL.=sprintf(",tplo_border='%s' ",$Bd);
		$sSQL.=sprintf(" WHERE tplo_id = %d ",$OptID);
		if ($DEBUG) echo "<br>[".__FUNCTION__."] sSQL -> $sSQL";
		$Return=AffectedSQL($sSQL);
		return ($Return == 1);
	}

	function template_option_delete($OptionID,$Type=NULL) {
		global $DEBUG,$Config,$ErrorMsg,$DBReturn,$USER;
		if ($Type == NULL) {
			$sSQL=sprintf("DELETE FROM template_line_option WHERE tplo_id = %d ",$OptionID);
			if ($DEBUG) echo "<br>[".__FUNCTION__."] sSQL -> $sSQL";
			$Return=AffectedSQL($sSQL);
			return ($Return == 1);
		} elseif ($Type != "ALL") {
			$sSQL=sprintf("DELETE FROM template_line_option WHERE tplo_tmpl_id = %d ",$OptionID);
			$sSQL.=sprintf(" AND tplo_name = '%s' ",$Type);
			if ($DEBUG) echo "<br>[".__FUNCTION__."] sSQL -> $sSQL";
			$Return=AffectedSQL($sSQL);
			return ($Return == 1);
		} else {
			$sSQL=sprintf("DELETE FROM template_line_option WHERE tplo_tmpl_id = %d ",$OptionID);
			if ($DEBUG) echo "<br>[".__FUNCTION__."] sSQL -> $sSQL";
			$Return=AffectedSQL($sSQL);
			return ($Return > 0);
		}
	}
	
	function template_option_list($LineID) {
		global $DEBUG,$USER;
		$sSQL=sprintf("SELECT * FROM  template_line_option WHERE (tplo_tmpl_id = %d) ORDER BY tplo_id",$LineID);
		if ($DEBUG) echo "<br>[".__FUNCTION__."] sSQL -> $sSQL";
		$Return=JSonSQL($sSQL);
		return ($Return);
	}

	function template_option_info($LineID,$Type=NULL) {
		global $DEBUG,$USER;
		$sSQL=sprintf("SELECT * FROM  template_line_option WHERE (tplo_tmpl_id = %d) ",$LineID);
		if ($Type != NULL) $sSQL.=sprintf("AND tplo_name='%s' ",$Type);
		$sSQL.="ORDER BY tplo_id";
		if ($DEBUG) echo "<br>[".__FUNCTION__."] sSQL -> $sSQL";
		$Return=JSonSQL($sSQL);
		return ($Return);
	}

// Clone Data

	function template_head_clone($ComID,$OldID) {
		global $DEBUG,$Config,$ErrorMsg,$DBReturn,$USER;

			$sSQL="INSERT INTO template_head (tmph_com_id,tmph_disp_id,tmph_name) ";
			$sSQL.=sprintf("(SELECT tmph_com_id,tmph_disp_id,concat (tmph_name,'_copy') FROM template_head ");
			$sSQL.=sprintf("WHERE (tmph_id = %d))",$OldID);
		if ($DEBUG) echo "<br>[".__FUNCTION__."] sSQL -> $sSQL";
			$InsertID=InsertSQL($sSQL);
			return ($InsertID);
	}

	function template_line_clone($ComID,$OldID,$NewID) {
		global $DEBUG,$USER;
		
		$iRun=0;
		$sSQL=sprintf("SELECT * FROM template_line WHERE (tmpl_tmph_id=%d) AND (tmpl_active=1) ",$OldID);
		if ($DEBUG) echo "<br>[".__FUNCTION__."][SELECT] sSQL -> $sSQL";
		$OldData=json_decode(JSonSQL($sSQL),TRUE);
		for ($iCount=0; $iCount<count($OldData); $iCount++) {
			$Fields=sprintf("tmpl_tmph_id=%d ",$NewID);
			$Fields.=sprintf(",tmpl_type_id='%s' ",$OldData[$iCount]['tmpl_type_id']);
//			$Fields.=sprintf(",tmpl_name='%s_copy' ",$OldData[$iCount]['tmpl_name']);
			$Fields.=sprintf(",tmpl_name='%s' ",$OldData[$iCount]['tmpl_name']);
			$Fields.=sprintf(",tmpl_order=%d ",$OldData[$iCount]['tmpl_order']);
			$Fields.=sprintf(",tmpl_left=%d ",$OldData[$iCount]['tmpl_left']);
			$Fields.=sprintf(",tmpl_top=%d ",$OldData[$iCount]['tmpl_top']);
			$Fields.=sprintf(",tmpl_width=%d ",$OldData[$iCount]['tmpl_width']);
			$Fields.=sprintf(",tmpl_height=%d ",$OldData[$iCount]['tmpl_height']);
			$Fields.=sprintf(",tmpl_opacity=%d ",$OldData[$iCount]['tmpl_opacity']);
			$Fields.=sprintf(",tmpl_color='%s' ",$OldData[$iCount]['tmpl_color']);
			$Fields.=sprintf(",tmpl_bgcolor='%s' ",$OldData[$iCount]['tmpl_bgcolor']);
			$Fields.=sprintf(",tmpl_border=%d ",$OldData[$iCount]['tmpl_border']);
			$Fields.=sprintf(",tmpl_header=%d ",$OldData[$iCount]['tmpl_header']);
			$Fields.=sprintf(",tmpl_footer=%d ",$OldData[$iCount]['tmpl_footer']);
			$sSQL=sprintf("INSERT INTO template_line SET %s ",$Fields);
			$InsertID=InsertSQL($sSQL);
			if ($DEBUG) echo "<br>[".__FUNCTION__."][Insert] sSQL -> $sSQL ==> $InsertID";
			if ($InsertID > 0) {
				if (template_option_clone($OldData[$iCount]['tmpl_id'],$InsertID)) $iRun++;
			}
		}
		return($iCount==$iRun);
		
//		$sSQL="INSERT INTO template_line (tmpl_tmph_id,tmpl_type_id,tmpl_order,tmpl_left,tmpl_top,tmpl_width,tmpl_height,tmpl_name) ";
//		$sSQL.=sprintf("( SELECT %d, T1.tmpl_type_id,T1.tmpl_order,T1.tmpl_left,T1.tmpl_top,T1.tmpl_width,T1.tmpl_height",$NewID);
//		$sSQL.=sprintf(",concat(T1.tmpl_name,'_copy')");
//		$sSQL.=sprintf("FROM template_line as T1 WHERE T1.tmpl_tmph_id =%d )",$OldID);
//		if ($DEBUG) echo "<br>[".__FUNCTION__."] sSQL -> $sSQL";
//		$DBReturn=AffectedSQL($sSQL);
//		echo $DBReturn;
//		return($DBReturn);
	}

	function template_option_clone($OldID,$NewID) {
		global $DEBUG,$USER;

		$iRun=0;
		$sSQL=sprintf("SELECT * FROM template_line_option WHERE (tplo_tmpl_id=%d) AND (tplo_active=1) ",$OldID);
		if ($DEBUG) echo "<br>[".__FUNCTION__."][SELECT] sSQL -> $sSQL";
		$OldData=json_decode(JSonSQL($sSQL),TRUE);
		for ($iCount=0; $iCount<count($OldData); $iCount++) {
			$Fields=sprintf("tplo_tmpl_id=%d ",$NewID);
			$Fields.=sprintf(",tplo_name='%s' ",$OldData[$iCount]['tplo_name']);
			$Fields.=sprintf(",tplo_order=%d ",$OldData[$iCount]['tplo_order']);
			$Fields.=sprintf(",tplo_left=%d ",$OldData[$iCount]['tplo_left']);
			$Fields.=sprintf(",tplo_top=%d ",$OldData[$iCount]['tplo_top']);
			$Fields.=sprintf(",tplo_width=%d ",$OldData[$iCount]['tplo_width']);
			$Fields.=sprintf(",tplo_height=%d ",$OldData[$iCount]['tplo_height']);
			$Fields.=sprintf(",tplo_opacity=%d ",$OldData[$iCount]['tplo_opacity']);
			$Fields.=sprintf(",tplo_color='%s' ",$OldData[$iCount]['tplo_color']);
			$Fields.=sprintf(",tplo_bgcolor='%s' ",$OldData[$iCount]['tplo_bgcolor']);
			$Fields.=sprintf(",tplo_border=%d ",$OldData[$iCount]['tplo_border']);
			$sSQL=sprintf("INSERT INTO template_line_option SET %s ",$Fields);
			$InsertID=InsertSQL($sSQL);
			if ($DEBUG) echo "<br>[".__FUNCTION__."][Insert] sSQL -> $sSQL ==> $InsertID";
			if ($InsertID > 0) $iRun++;
		}
//		echo $DBReturn;
		return($iCount==$iRun);
	}

?>