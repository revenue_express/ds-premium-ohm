<?
	function command_list($ComID=1,$ClientID=0,$Command=NULL,$Completed=-1,$sTime=NULL,$rTime=NULL) {
		global $DEBUG,$USER;
		$sSQL=sprintf("SELECT * FROM command_info WHERE (cmd_com_id=%d) ",$ComID);
		if ($ClientID <> 0)  $sSQL.=sprintf("AND (cmd_cli_id=%d) ",$ClientID);
		if (! is_null($Command))  $sSQL.=sprintf("AND (cmd_cmd='%s') ",$Command);

		if ($Completed <> -1) {  
			$sSQL.=sprintf("AND (cmd_status=%d) ",$Completed);
		}
		$sSQL.=sprintf("ORDER BY cmd_status DESC,cmd_id ");

		if ($DEBUG) echo "<br>[".__FUNCTION__."] sSQL -> '$sSQL'";
		$DBReturn=JSonSQL($sSQL);
//		echo $DBReturn;
		return($DBReturn);
	}
	
	function command_add_schedule($ComID=1,$ClientID,$ScheduleID) {
		global $DEBUG,$USER;
		$CurrTime=date('Y-m-d h:i:s');
		$sSQL=sprintf("INSERT INTO command_info SET cmd_com_id=%d ",$ComID);
		$sSQL.=sprintf(", cmd_cmd='Send Schedule'");
		$sSQL.=sprintf(", cmd_cli_id=%d ",$ClientID);
		$sSQL.=sprintf(", cmd_para1='%d' ",$ScheduleID);
		$sSQL.=sprintf(", cmd_stime='%s' ",$CurrTime);
		$sSQL.=sprintf(", cmd_rtime='%s' ",$CurrTime);

		if ($DEBUG) echo "<br>[".__FUNCTION__."] sSQL -> '$sSQL'";
		$DBReturn=InsertSQL($sSQL);
//		echo $DBReturn;
		return($DBReturn);
	}

	function command_add_download($ComID=1,$ClientID,$Type,$Name,$URL,$Hash,$Group='0') {
		global $DEBUG,$USER;
		$CurrTime=date('Y-m-d h:i:s');
		$sSQL=sprintf("INSERT INTO command_info SET cmd_com_id=%d ",$ComID);
		$sSQL.=sprintf(", cmd_cmd='Download File'");
		$sSQL.=sprintf(", cmd_cli_id=%d ",$ClientID);
		$sSQL.=sprintf(", cmd_para1='%s' ",$Type);
		$sSQL.=sprintf(", cmd_para2='%s' ",$Name);
		$sSQL.=sprintf(", cmd_para3='%s' ",$Hash);
		$sSQL.=sprintf(", cmd_para4='%s' ",$Group);
		$sSQL.=sprintf(", cmd_stime='%s' ",$CurrTime);
		$sSQL.=sprintf(", cmd_rtime='%s' ",$CurrTime);

		if ($DEBUG) echo "<br>[".__FUNCTION__."] sSQL -> '$sSQL'";
		$DBReturn=InsertSQL($sSQL);
//		echo $DBReturn;
		return($DBReturn);
	}

	function command_active($cmdID,$Active=0) {
		global $DEBUG,$USER;
		$sSQL=sprintf("UPDATE command_info SET cmd_active=%d WHERE cmd_id=%d",$Active,$cmdID);
		if ($DEBUG) echo "<br>[".__FUNCTION__."] sSQL -> '$sSQL'";
		$DBReturn=AffectedSQL($sSQL);
//		echo $DBReturn;
		return($DBReturn);
	}
	
	function command_response($cmdID,$Status=1) {
		global $DEBUG,$USER;
		$CurrTime=date('Y-m-d h:i:s');
		$sSQL=sprintf("UPDATE command_info SET cmd_status=%d, cmd_rtime='%s' ",$Status,$CurrTime);
		$sSQL.=sprintf("WHERE cmd_id=%d",$cmdID);
		if ($DEBUG) echo "<br>[".__FUNCTION__."] sSQL -> '$sSQL'";
		$DBReturn=AffectedSQL($sSQL);
//		echo $DBReturn;
		return($DBReturn);
	}
?>