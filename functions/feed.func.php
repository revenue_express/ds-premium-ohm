<?
	function feed_list($Start=0,$PerPage=0,$ComID=1) {
		global $DEBUG,$USER;
		$sSQL=sprintf("SELECT * FROM feed_info ");
		$sSQL.=sprintf("WHERE feed_com_id=%d ",$ComID);
		if (strtolower($USER['usr_level']) == 'operator') $sSQL.="AND (feed_active=1)";
		$sSQL.="ORDER BY feed_active DESC,feed_id";
		if (($Start >=0) && ($PerPage >0)) {
			$sSQL.=sprintf("LIMIT %d,%d",$Start*$PerPage,$PerPage);
		}
		if ($DEBUG) echo "<br>\n[".__FUNCTION__."] sSQL -> $sSQL";
		$Data=JSonSQL($sSQL);
		if ($DEBUG) { echo "<br>\nData<pre>"; print_r(json_decode($Data,true)); echo "</pre>"; }
		return($Data);
	}

	function feed_info($FeedID) {
		global $DEBUG,$USER;
		$sSQL=sprintf("SELECT * FROM feed_info WHERE (feed_id=%d) ",$FeedID);
		if (strtolower($USER['usr_level']) == 'operator') $sSQL.="AND (feed_active=1)";
		if ($DEBUG) echo "<br>\n[".__FUNCTION__."] sSQL -> $sSQL";
		$Data=JSonSQL($sSQL);
		if ($DEBUG) { echo "<br>\nData<pre>"; print_r(json_decode($Data,true)); echo "</pre>"; }
		return($Data);
	}

	function feed_add($ComID=1,$Data,$Charset="UTF-8",$Lang="Thai/English") {
		global $DEBUG,$USER;
		$sSQL=sprintf("INSERT INTO feed_info SET feed_com_id=%d,feed_data='%s'",$ComID,$Data);
		$sSQL.=sprintf(", feed_charset='%s', feed_lang='%s' ",$Charset,$Lang);
		if (strtolower($USER['usr_level']) == 'operator') $sSQL.=", feed_active=1 ";
		if ($DEBUG) echo "<br>\n[".__FUNCTION__."] sSQL -> $sSQL";
		$Data=InsertSQL($sSQL);
		if ($DEBUG) { echo "<br>\nData=> $Data"; }
		return($Data);
	}

	function feed_edit($FeedID,$Data,$Charset="UTF-8",$Lang="Thai/English") {
		global $DEBUG,$USER;
		$sSQL=sprintf("UPDATE feed_info SET feed_id=%d ",$FeedID);
		$sSQL.=sprintf(", feed_data='%s' ",$Data);
		$sSQL.=sprintf(", feed_charset='%s' ",$Charset);
		$sSQL.=sprintf(", feed_lang='%s' ",$Lang);
		$sSQL.=sprintf("WHERE feed_id=%d ",$FeedID);
		if ($DEBUG) echo "<br>\n[".__FUNCTION__."] sSQL -> $sSQL";
		$Data=AffectedSQL($sSQL);
		if ($DEBUG) { echo "<br>\nData=> $Data"; }
		return($Data);
	}

	function feed_active($FeedID,$Active=0) {
		global $DEBUG,$USER;
		$sSQL=sprintf("UPDATE feed_info SET feed_active=%d ",$Active);
		$sSQL.=sprintf("WHERE feed_id=%d ",$FeedID);
		if ($DEBUG) echo "<br>\n[".__FUNCTION__."] sSQL -> $sSQL";
		$Data=AffectedSQL($sSQL);
		if ($DEBUG) { echo "<br>\nData=> $Data"; }
		return($Data);
	}
	/* playlist head for feed*/
	function playlist_feed_head_info($HeadID) {
		global $DEBUG,$USER;
		$sSQL=sprintf("SELECT * FROM playlist_head WHERE plh_id=%d ",$HeadID);
		if (strtolower($USER['usr_level']) == 'operator') $sSQL.="AND (plh_active=1)";
		if ($DEBUG) echo "<br>\n[".__FUNCTION__."] sSQL -> $sSQL";
		$Data=JSonSQL($sSQL);
		if ($DEBUG) { echo "<br>\nData<pre>"; print_r(json_decode($Data,true)); echo "</pre>"; }
		return($Data);
	}

	function playlist_feed_head_list($ComID=1) {
		global $DEBUG,$USER;
		$sTable="SELECT * , count(plf_id) as counter FROM playlist_feed,feed_info";
		$sTable.=sprintf(" WHERE (plf_feed_id = feed_id) AND (feed_active=1) AND (plf_com_id=%d) ",$ComID);
		if (strtolower($USER['usr_level']) == 'operator') $sTable.="AND (plf_active=1) ";
		$sTable.="GROUP BY plf_plh_id ORDER BY plf_plh_id";
		
		$sSQL=sprintf("SELECT * FROM playlist_head as T1 LEFT JOIN (%s) as T2 ON (T1.plh_id = T2.plf_plh_id) ",$sTable);
		$sSQL.=sprintf("WHERE (T1.plh_type_id = 5) ");
		if (strtolower($USER['usr_level']) == 'operator') $sSQL.="AND (T1.plh_active=1)";
		$sSQL.="ORDER BY T1.plh_active DESC,T1.plh_id";
		if ($DEBUG) echo "<br>\n[".__FUNCTION__."] sSQL -> $sSQL";
		$Data=JSonSQL($sSQL);
		if ($DEBUG) { echo "<br>\nData<pre>"; print_r(json_decode($Data,true)); echo "</pre>"; }
		return($Data);
	}

	/* playlist feed*/
	function playlist_feed_line_list($PlayID) {
		global $DEBUG,$USER;
		$sSQL=sprintf("SELECT * FROM playlist_feed,feed_info WHERE (plf_feed_id = feed_id) ");
		$sSQL.=sprintf("AND (plf_plh_id=%d) ",$PlayID);
		$sSQL.=sprintf("AND (feed_active=1) ");
		if (strtolower($USER['usr_level']) == 'operator') $sSQL.="AND (plf_active=1) AND (feed_active=1)";
		$sSQL.="ORDER BY plf_active DESC,plf_order,plf_id";
		if ($DEBUG) echo "<br>\n[".__FUNCTION__."] sSQL -> $sSQL";
		$Data=JSonSQL($sSQL);
		if ($DEBUG) { echo "<br>\nData<pre>"; print_r(json_decode($Data,true)); echo "</pre>"; }
		return($Data);
	}

	function playlist_feed_info($FeedID) {
		global $DEBUG,$USER;
		$sSQL=sprintf("SELECT * FROM playlist_feed,feed_info WHERE (plf_id=%d) ",$FeedID);
		$sSQL.=sprintf("AND (plf_feed_id = feed_id) ");
//		if (strtolower($USER['usr_level']) == 'operator') $sSQL.="AND (plf_active=1) AND (feed_active=1)";
		if ($DEBUG) echo "<br>\n[".__FUNCTION__."] sSQL -> $sSQL";
		$Data=JSonSQL($sSQL);
		if ($DEBUG) { echo "<br>\nData<pre>"; print_r(json_decode($Data,true)); echo "</pre>"; }
		return($Data);
	}

	function playlist_feed_add($HeadID,$FeedID,$Order=0,$ComID=1) {
		global $DEBUG,$USER;
		$sSQL=sprintf("INSERT INTO playlist_feed SET plf_active=1 ");
		$sSQL.=sprintf(", plf_plh_id=%d ",$HeadID);
		$sSQL.=sprintf(", plf_com_id=%d ",$ComID);
		$sSQL.=sprintf(", plf_feed_id=%d ",$FeedID);
		$sSQL.=sprintf(", plf_order=%d ",$Order);
		if ($DEBUG) echo "<br>\n[".__FUNCTION__."] sSQL -> $sSQL";
		$Data=InsertSQL($sSQL);
		if ($DEBUG) { echo "<br>\nData=> $Data"; }
		return($Data);
	}
	
	function playlist_feed_edit($LineID,$HeadID,$FeedID,$Order=0) {
		global $DEBUG,$USER;
		$sSQL=sprintf("UPDATE playlist_feed SET plf_id = %d",$LineID);
		if (! is_null($HeadID)) $sSQL.=sprintf(", plf_plh_id=%d ",$HeadID);
		if (! is_null($FeedID)) $sSQL.=sprintf(", plf_feed_id=%d ",$FeedID);
		if (! is_null($Order)) $sSQL.=sprintf(", plf_order=%d ",$Order);
		$sSQL.=sprintf("WHERE plf_id=%d ",$LineID);
		if ($DEBUG) echo "<br>\n[".__FUNCTION__."] sSQL -> $sSQL";
		$Data=AffectedSQL($sSQL);
		if ($DEBUG) { echo "<br>\nData=> $Data"; }
		return($Data);
	}

	function playlist_feed_active($LineID,$Active=0) {
		global $DEBUG,$USER;
		$sSQL=sprintf("UPDATE playlist_feed SET plf_active = %d WHERE plf_id=%d ",$Active,$LineID);
		if ($DEBUG) echo "<br>\n[".__FUNCTION__."] sSQL -> $sSQL";
		$Data=AffectedSQL($sSQL);
		if ($DEBUG) { echo "<br>\nData=> $Data"; }
		return($Data);
	}
	function playlist_feed_order($LineID,$Order=0) {
		global $DEBUG,$USER;
		$sSQL=sprintf("UPDATE playlist_feed SET plf_id = %d",$LineID);
		$sSQL.=sprintf(", plf_order=%d ",$Order);
		$sSQL.=sprintf("WHERE plf_id=%d ",$LineID);
		if ($DEBUG) echo "<br>\n[".__FUNCTION__."] sSQL -> $sSQL";
		$Data=AffectedSQL($sSQL);
		if ($DEBUG) { echo "<br>\nData=> $Data"; }
		return($Data);
	}

	function playlist_feed_last_order($PlayID) {
		global $DEBUG,$USER;
		$sSQL=sprintf("SELECT * FROM playlist_feed WHERE (plf_plh_id=%d) ",$PlayID);
		if (strtolower($USER['usr_level']) == 'operator') $sSQL.="AND (plf_active=1) ";
		$sSQL.="ORDER BY plf_order DESC LIMIT 1";
		if ($DEBUG) echo "<br>\n[".__FUNCTION__."] sSQL -> $sSQL";
		$Result=ExecuteReader($sSQL);
		$Return=1;
		if ($Data=mysql_fetch_array($Result)) {
			$Return=$Data['plf_order'];
		}
		return($Return);
	}

	function playlist_feed_count($HeadID) {
		global $DEBUG,$USER;
		
		$DBReturn=0;
		$sSQL=sprintf("SELECT count(*) as counter FROM playlist_feed ");
		$sSQL.=sprintf("WHERE plf_plh_id=%d ",$HeadID);
		if (strtolower($USER['usr_level']) == 'operator') $sSQL.="AND (plf_active = 1) "; 
		if ($DEBUG) echo "<br>[".__FUNCTION__."] sSQL -> '$sSQL'";
		$Result=ExecuteReader($sSQL);
		if ($Data = mysql_fetch_array($Result)) {
				$DBReturn=$Data['counter'];
		}
		return($DBReturn);
	}

	function playlist_feed_clone($NewID,$OldID) {
		global $DEBUG,$USER;
		
		$sSQL="INSERT INTO playlist_feed (plf_plh_id,plf_com_id,plf_order,plf_feed_id) ";
		$sSQL.=sprintf("SELECT %d, T1.plf_com_id ,T1.plf_order, T1.plf_feed_id ",$NewID);
		$sSQL.=sprintf("FROM playlist_feed as T1 WHERE T1.plf_active =1 AND T1.plf_plh_id =%d",$OldID);
		if ($DEBUG) echo "<br>[".__FUNCTION__."] sSQL -> '$sSQL'";
		$DBReturn=AffectedSQL($sSQL);
//		echo $DBReturn;
		return($DBReturn);
	}

/*
	function playlist_used($PlayID) {
		global $DEBUG,$USER;
		$sSQL="SELECT count(*) as Counter FROM playlist_head,layout_head,layout_line ";
		$sSQL.="WHERE (layl_plh_id= plh_id) AND (layh_id = layl_layh_id) AND (layl_layh_id = layh_id)  ";
//		$sSQL.="AND (layh_active = 1) AND (layl_active =1) ";
		$sSQL.=sprintf("AND (plh_id = %d) ",$PlayID);
		if ($DEBUG) echo "[".__FUNCTION__."] sSQL -> '$sSQL'<br>";
		$DBReturn=ExecuteReader($sSQL);
		if ($Data=mysql_fetch_array($DBReturn)) {
//		echo $DBReturn;
			return($Data[0]['Counter']);
		} else {
			return 0;
		}
	}
*/

	function feed_used($FeedID) {
		global $DEBUG,$USER;
		$sSQL="SELECT count(*) as counter FROM playlist_head,playlist_feed WHERE (plh_id=plf_plh_id) ";
		if (strtolower($USER['usr_level']) == 'operator')	$sSQL.="AND (plh_active=1) AND (plf_active=1) ";
		$sSQL.=sprintf("AND (plf_feed_id=%d) ",$FeedID);
		if ($DEBUG) echo "[".__FUNCTION__."] sSQL -> '$sSQL'<br>";
		$Result=ExecuteReader($sSQL);
		if ($Data = mysql_fetch_array($Result)) {
			return $Data['counter'];
		} else {
			return 0;
		}
	}
	
	function playlist_feed_delete($PlaylistID) {
		global $DEBUG,$USER;
		$Return="";

// Delete Playlist Line
		$sSQL=sprintf("DELETE FROM playlist_feed WHERE plf_plh_id=%d ",$PlaylistID);
		if ($DEBUG) echo "[".__FUNCTION__."] sSQL -> '$sSQL'<br>";
		$DBCount=AffectedSQL($sSQL);
		if ($DEBUG)	$Return.=sprintf("\nDelete %d Item(s) in this playlist.",$DBCount);

//	Delete Playlist Head Option
		$sSQL=sprintf("DELETE FROM playlist_head_option WHERE pho_plh_id=%d ",$PlaylistID);
		if ($DEBUG) echo "[".__FUNCTION__."] sSQL -> '$sSQL'<br>";
		$DBCount=AffectedSQL($sSQL);
		if ($DEBUG)	$Return.=sprintf("\nDelete %d Option(s) in this Playlist.",$DBCount);

//	Delete Feed Head
		$sSQL=sprintf("DELETE FROM playlist_head WHERE plh_id=%d ",$PlaylistID);
		if ($DEBUG) echo "[".__FUNCTION__."] sSQL -> '$sSQL'<br>";
		$DBCount=AffectedSQL($sSQL);
		if ($DBCount ==1) {
			$Return.=sprintf("\n\nDelete this playlist successful.",$DBCount);
		} else {
			$Return.=sprintf("\n\nDelete this playlist failed.",$DBCount);
		}
		return $Return;
	}

	function feed_delete($FeedID) {
		global $DEBUG,$USER;
		$sSQL=sprintf("DELETE FROM feed_info WHERE feed_id=%d ",$FeedID);
		if ($DEBUG) echo "<br>\n[".__FUNCTION__."] sSQL -> $sSQL";
		$DBReturn=AffectedSQL($sSQL);
		$Return="Delete failed";
		if ($DBReturn > 0) $Return="Delete successful";
//		echo $DBReturn;
		return($Return);
	}
	
//	================================
//	Remove Playlist Section
// ================================

	function playlist_feed_remove($LineID,$optALL=NULL) {
		global $DEBUG,$USER;
		$sSQL=sprintf("DELETE FROM playlist_feed");
		if (is_null($optALL)) {
			$sSQL.=sprintf(" WHERE plf_id=%d ",$LineID);
		} else {
			$sSQL.=sprintf(" WHERE plf_plh_id=%d ",$LineID);
		}
		if ($DEBUG) echo "<br>\n[".__FUNCTION__."] sSQL -> $sSQL";
		$DBReturn=AffectedSQL($sSQL);
		return($DBReturn);
	}

?>