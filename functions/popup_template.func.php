<?

	function template_head_list($comID=1,$login=1, $limit=0, $spage=0, $order='tmph_name') {
		global $DEBUG,$Config,$ErrorMsg,$DBReturn,$USER;
		$sSQL=sprintf("SELECT * FROM  template_head LEFT JOIN display_info ON disp_id = tmph_disp_id WHERE (tmph_com_id = %d) ",$comID);
		if ($USER['usr_level'] <> 'Administrator') $sSQL.="AND (tmph_active = 1) "; 
		$sSQL.=sprintf(" order by %s",$order);

		if ( ($limit == 0) && ($spage == 0) ) {
			$sSQL.="";
		} else {
			$sSQL.=sprintf(" LIMIT %d,%d",$spage*$limit,$limit);
		}
		if ($DEBUG) echo "<li>[".__FUNCTION__."] sSQL -> $sSQL<br>";
		$JSonData=JSonSQL($sSQL);
//		echo $DBReturn;
		return($JSonData);
	}

	function template_head_dummyID($comID=1) {
		global $DEBUG,$USER;
		$sHead=sprintf("%s",date('ymd'));
		$sSQL=sprintf("SELECT * FROM  template_head WHERE (tmph_name like '%s%%') ",$sHead);
		$sSQL.=sprintf("AND (tmph_com_id=%d) ",$comID);
		$sSQL.=sprintf("ORDER BY tmph_name DESC LIMIT 1 ");
		if ($DEBUG) echo "[".__FUNCTION__."] sSQL -> '$sSQL'<br>";
		$iCount=1;
		$Result=ExecuteReader($sSQL);
		if ($Data=mysql_fetch_array($Result)) {
			$Num=$Data['tmph_name'];
			if ($DEBUG) echo "[".__FUNCTION__."] tmph_name -> '$Num'<br>";
			$iCount=intval(substr($Num,strlen($sHead)));
			$iCount++;
			if ($DEBUG) echo "[".__FUNCTION__."] iCount -> '$iCount'<br>";
		}
		$Code=sprintf("%s%03d",$sHead,$iCount);
			if ($DEBUG) echo "[".__FUNCTION__."] Return -> '$Code'<br>";
		return($Code);		
	}

	function template_head_add($ComID=1,$DispID=1,$Name,$Desc=NULL) {
		global $DEBUG,$Config,$ErrorMsg,$DBReturn,$USER;

		$sSQL="INSERT INTO template_head SET ";
		$sSQL.=" tmph_active = 1";
		if (isset($ComID)) $sSQL.=sprintf(",tmph_com_id=%d ",$ComID);
		if (isset($DispID)) $sSQL.=sprintf(",tmph_disp_id=%d ",$DispID);
		if (isset($Name)) $sSQL.=sprintf(",tmph_name='%s' ",$Name);
		if (isset($Desc)) $sSQL.=sprintf(",tmph_desc='%s' ",$Desc);
		if ($DEBUG) echo "<li>[".__FUNCTION__."] sSQL -> $sSQL<br>";
		$InsertID=InsertSQL($sSQL);
		if ($DEBUG) echo "<li>[".__FUNCTION__."] Return -> $InsertID<br>";
		return ($InsertID);
	}
	
	function template_head_edit($ComID,$LastID,$DispID,$Name,$Desc) {
		global $DEBUG,$Config,$ErrorMsg,$DBReturn,$USER;

		$sSQL="UPDATE template_head SET tmph_active = 1";
		if (isset($ComID)) $sSQL.=sprintf(",tmph_com_id=%d ",$ComID);
		if (isset($DispID)) $sSQL.=sprintf(",tmph_disp_id=%d ",$DispID);
		if (isset($Name)) $sSQL.=sprintf(",tmph_name='%s' ",$Name);
		if (isset($Desc)) $sSQL.=sprintf(",tmph_desc='%s' ",$Desc);
		$sSQL.=sprintf("WHERE tmph_id=%d",$LastID);
		if ($DEBUG) echo "<li>[".__FUNCTION__."] sSQL -> $sSQL<br>";
		$rCount=AffectedSQL($sSQL);
		if ($rCount = 1) {
			return(TRUE);
		}
		return(FALSE);
	}
	
	function template_head_active($tmph_id,$active=0) {
		global $DEBUG,$Config,$ErrorMsg,$DBReturn,$USER;

		$sSQL=sprintf("UPDATE template_head SET tmph_active = %d WHERE tmph_id = %d",$active,$tmph_id);
		if ($DEBUG) echo "<li>[".__FUNCTION__."] sSQL -> $sSQL<br>";
		$rCount=AffectedSQL($sSQL);
		if ($rCount = 1) {
			return(TRUE);
		}
		return(FALSE);
	}
	
	function template_head_clone($ComID,$OldID) {
		global $DEBUG,$Config,$ErrorMsg,$DBReturn,$USER;

			$sSQL="INSERT INTO template_head (tmph_com_id,tmph_disp_id,tmph_name) ";
			$sSQL.=sprintf("(SELECT tmph_com_id,tmph_disp_id,concat (tmph_name,'_copy') FROM template_head ");
			$sSQL.=sprintf("WHERE (tmph_id = %d))",$OldID);
			if ($DEBUG) echo "[".__FUNCTION__."] sSQL -> '$sSQL'<br>";
			$InsertID=InsertSQL($sSQL);
			return ($InsertID);
	}

	function template_line_clone($ComID,$OldID,$NewID) {
		global $DEBUG,$USER;
		
		$sSQL="INSERT INTO template_line (tmpl_tmph_id,tmpl_type_id,tmpl_order,tmpl_left,tmpl_top,tmpl_width,tmpl_height,tmpl_name) ";
		$sSQL.=sprintf("( SELECT %d, T1.tmpl_type_id,T1.tmpl_order,T1.tmpl_left,T1.tmpl_top,T1.tmpl_width,T1.tmpl_height",$NewID);
		$sSQL.=sprintf(",concat(T1.tmpl_name,'_copy')");
		$sSQL.=sprintf("FROM template_line as T1 WHERE T1.tmpl_tmph_id =%d )",$OldID);
		if ($DEBUG) echo "[".__FUNCTION__."] sSQL -> '$sSQL'<br>";
		$DBReturn=AffectedSQL($sSQL);
//		echo $DBReturn;
		return($DBReturn);
	}

	function template_line_list($comID=1,$HeadID=1,$login=1, $ppage=0, $limit=0, $spage=0) {
		global $DEBUG,$USER;
		$sSQL=sprintf("SELECT * FROM template_line WHERE (tmpl_tmph_id = %d) ",$HeadID);
		if ($USER['usr_level'] <> 'Administrator') $sSQL.="AND (tmpl_active = 1) "; 
		$sSQL.="ORDER by tmpl_active DESC, tmpl_order";
		if ( ($ppage <> 0) && ($limite<> 0) && ($spage <> 0) ) {
			$sSQL.=sprintf(" LIMIT %d,%d",$spage*$limit,$limit);
		}
		if ($DEBUG) echo "[".__FUNCTION__."] sSQL -> '$sSQL'<br>";
		$DBReturn=JSonSQL($sSQL);
//		echo $DBReturn;
		return($DBReturn);
	}

/*
	function template_line_list($comID=1,$login=1,$tmph_ID, $limit=0, $spage=0, $order='tmpl_order') {
		global $DEBUG,$Config,$ErrorMsg,$DBReturn,$USER;
		$sSQL=sprintf("SELECT * FROM  template_line WHERE (tmph_active = 1) AND (tmph_com_id = %d) ",$comID);
		$sSQL.=sprintf("AND (tmpl_tmph_id=%d) ",$tmph_ID);
		$sSQL.=sprintf("ORDER BY %s",$order);

		if ( ($limit == 0) && ($spage == 0) ) {
			$sSQL.="";
		} else {
			$sSQL.=sprintf(" LIMIT %d,%d",$spage*$limit,$limit);
		}
		if ($DEBUG) echo "<li>[".__FUNCTION__."] sSQL -> $sSQL<br>";
		$JSonData=JSonSQL($sSQL);
//		echo $DBReturn;
		return($JSonData);
	}
*/
	function template_line_add($HeadID,$TypeID,$Name,$Order,$Left=0,$Top=0,$Width=0,$Height=0) {
		global $DEBUG,$Config,$ErrorMsg,$DBReturn,$USER;

		$sSQL="INSERT INTO template_line SET ";
		$sSQL.=" tmpl_active = 1";
		if (isset($HeadID)) $sSQL.=sprintf(",tmpl_tmph_id=%d ",$HeadID);
		if (isset($TypeID)) $sSQL.=sprintf(",tmpl_type_id=%d ",$TypeID);
		if (isset($Name)) $sSQL.=sprintf(",tmpl_name='%s' ",$Name);
//		if (isset($Desc)) $sSQL.=sprintf(",tmpl_desc='%s' ",$Desc);
		if (isset($Order)) $sSQL.=sprintf(",tmpl_order=%d ",$Order);
		if (isset($Left)) $sSQL.=sprintf(",tmpl_left=%d ",$Left);
		if (isset($Right)) $sSQL.=sprintf(",tmpl_top=%s ",$Top);
		if (isset($Width)) $sSQL.=sprintf(",tmpl_width=%d ",$Width);
		if (isset($Height)) $sSQL.=sprintf(",tmpl_height=%d ",$Height);
		if ($DEBUG) echo "<li>[".__FUNCTION__."] sSQL -> '$sSQL'<br>";
		$InsertID=InsertSQL($sSQL);
		return ($InsertID);
	}

	function template_line_edit($LastID,$HeadID,$TypeID,$Name,$Order,$Left=0,$Top=0,$Width=0,$Height=0) {
		global $DEBUG,$Config,$ErrorMsg,$DBReturn,$USER;

		$sSQL="UPDATE template_line SET tmpl_active = 1";
		if (isset($HeadID)) $sSQL.=sprintf(",tmpl_tmph_id=%d ",$HeadID);
		if (isset($TypeID)) $sSQL.=sprintf(",tmpl_type_id=%d ",$TypeID);
		if (isset($Name)) $sSQL.=sprintf(",tmpl_name='%s' ",$Name);
//		if (isset($Desc)) $sSQL.=sprintf(",tmpl_desc='%s' ",$Desc);
		if (isset($Order)) $sSQL.=sprintf(",tmpl_order=%d ",$Order);
		if (isset($Left)) $sSQL.=sprintf(",tmpl_left=%d ",$Left);
		if (isset($Right)) $sSQL.=sprintf(",tmpl_top=%s ",$Top);
		if (isset($Width)) $sSQL.=sprintf(",tmpl_width=%d ",$Width);
		if (isset($Height)) $sSQL.=sprintf(",tmpl_height=%d ",$Height);
		$sSQL.=sprintf("WHERE tmpl_id=%d",$LastID);
		if ($DEBUG) echo "<li>[".__FUNCTION__."] sSQL -> '$sSQL'<br>";
		$rCount=AffectedSQL($sSQL);
		if ($rCount = 1) {
			return(TRUE);
		}
		return(FALSE);
	}

	function template_line_active($tmpl_id,$active=0) {
		global $DEBUG,$Config,$ErrorMsg,$DBReturn,$USER;

		$sSQL=sprintf("UPDATE template_line SET tmpl_active = %d WHERE tmpl_id = %d",$active,$tmpl_id);
		if ($DEBUG) echo "<li>[".__FUNCTION__."] sSQL -> $sSQL<br>";
		$rCount=AffectedSQL($sSQL);
		if ($rCount = 1) {
			return(TRUE);
		}
		return(FALSE);
	}

	function template_head_view($TemplateID) {
		global $DEBUG,$Config,$ErrorMsg,$DBReturn,$USER;
		$sSQL=sprintf("SELECT * FROM  template_head LEFT JOIN display_info ON disp_id = tmph_disp_id WHERE tmph_id=%d ",$TemplateID);
		if ($DEBUG) echo "[".__FUNCTION__."] sSQL -> '$sSQL'<br>";
		$DBReturn=JSonSQL($sSQL);
//		echo $DBReturn;
		return($DBReturn);
	}

	function template_line_count($HeadID) {
		global $DEBUG,$USER;
		
		$DBReturn=0;
		$sSQL=sprintf("SELECT count(*) as counter FROM template_line ");
		$sSQL.=sprintf("WHERE tmpl_tmph_id=%d ",$HeadID);
		if ($USER['usr_level'] <> 'Administrator') $sSQL.="AND (tmpl_active = 1) "; 
		if ($DEBUG) echo "[".__FUNCTION__."] sSQL -> '$sSQL'<br>";
		$Result=ExecuteReader($sSQL);
		if ($Data = mysql_fetch_array($Result)) {
				$DBReturn=$Data['counter'];
		}
		return($DBReturn);
	}

?>