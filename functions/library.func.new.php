<?php
	function file_type_list($TypeID=1) {
		global $DEBUG,$USER;
		$sSQL=sprintf("SELECT * FROM type_info WHERE (type_id = %d) LIMIT 1",$TypeID);
		if ($DEBUG) echo "[".__FUNCTION__."] sSQL -> '$sSQL'<br>";
		$DBReturn=JSonSQL($sSQL);
//		echo $DBReturn;
		return($DBReturn);
	}

	function library_list($comID=1,$typeID=1,$login=1, $Page=0, $PerPage=0) {
		global $DEBUG,$USER;
		$sSQL=sprintf("SELECT * FROM  file_info WHERE (file_com_id = %d) ",$comID);
		if ($USER['usr_level']=='Operator') $sSQL.="AND (file_active = 1) "; 
		if ($typeID <> 0)  $sSQL.=sprintf("AND file_type_id=%d ",$typeID);
		$sSQL.="ORDER BY file_active DESC,file_id";
		if (($PerPage <> 0) ) {
			$sSQL.=sprintf(" LIMIT %d,%d",$PerPage*$Page,$PerPage);
		}
		if ($DEBUG) echo "[".__FUNCTION__."] sSQL -> '$sSQL'<br>";
		$DBReturn=JSonSQL($sSQL);
//		echo $DBReturn;
		return($DBReturn);
	}

	function library_add($type,$OldName,$NewName,$SysName,$FileSize,$ComID='1') {
		global $DBReturn,$DEBUG;
		switch(strtolower($type)) {
			case 'images':	$typeID=1; break;
			case "videos" : $typeID=2; break; 
			case "flashs" : $typeID=3; break;
			case "audios" : $typeID=4; break;
			case "docs" : $typeID=9; break;
			case "pdfs" : $typeID=10; break;
			case 'covers':	$typeID=11; break;
			case 'ppts':	$typeID=12; break;
		}
		$DBReturn=0;
		$InsertID=0;
		$NewPath="../data/".$type;
		if (! is_dir($NewPath))	$NewPath="./data/".$type;

		$sNewFile=$NewName;

		$mTime=microtime();
		if ($DEBUG) echo "mTime -> '$mTime'";
		list($sTime)=explode(" ",$mTime);

		$sSysName=sprintf("%s%03d",date('YmdHis'),$sTime*1000);
		$exten=strtolower(end(explode(".", $OldName)));
		$newFile=sprintf("%s.%s",$sSysName,$exten);
		$newFileName=sprintf("%s/%s.%s",$NewPath,$sSysName,$exten);
		rename($SysName,$newFileName);

		$CRC=hash_file('md5',$newFileName);
		$sSQL="INSERT INTO file_info SET ";
		$sSQL.=sprintf("file_com_id=%d ",$ComID);
		$sSQL.=sprintf(",file_type_id=%d ",$typeID);
		$sSQL.=sprintf(",file_sname='%s' ",$newFile);
		$sSQL.=sprintf(",file_oname='%s' ",$OldName);
//		$sSQL.=sprintf(",file_dname='%s' ",$sSysName);
		$sSQL.=sprintf(",file_dname='คำบรรยาย' ",$sSysName);
		$sSQL.=sprintf(",file_path='%s' ",$type);
		$sSQL.=sprintf(",file_size=%d ",filesize($newFileName));
		$sSQL.=sprintf(",file_crc='%s' ",$CRC);
		if ($DEBUG) echo "[".__FUNCTION__."] sSQL -> '$sSQL'<br>";

		$InsertID=InsertSQL($sSQL);
		return $InsertID;
	}
	
	function library_convert($fileID) {
		global $DBReturn,$DEBUG;
// Convert File 
		$sSQL=sprintf("SELECT * FROM  file_info WHERE (file_id = %d) LIMIT 1 ",$fileID);
		$arrData=json_decode(JSonSQL($sSQL),true);
		$arrFile=$arrData[0];
		$NewPath="../data/".$arrFile['file_path'];
		if (! is_dir($NewPath))	$NewPath="./data/".$arrFile['file_path'];
		$parts=pathinfo($arrFile['file_sname']);
		$orgName=$parts['filename'];
		$orgFileName=sprintf('%s/%s',$NewPath,$arrFile['file_sname']);
		$newFileName=$orgFileName;
		$newFile=$arrFile['file_sname'];
		switch($arrFile['file_type_id']) {
//			case "images" : $typeID=1; 
			case 1 : // IMAGES
				$newFile=sprintf("%s.jpg",$orgName); 
				$newFileName=sprintf("%s/%s",$NewPath,$newFile);
				if ($orgFileName != $newFileName) {
					$cmd=sprintf("convert %s %s",$orgFileName,$newFileName);
					exec($cmd,$output,$return);	
					if ($DEBUG) echo sprintf("<br>command => %s",$cmd); 
					if ($DEBUG) echo sprintf("<br>output => %s",$output); 
					if ($DEBUG) echo sprintf("<br>return => %s",$return); 
					unlink($orgFileName);
				}
				// Resize Image by insert "_s" after System Name
				$oldImgSize=getimagesize($newFileName);
				$resizeName1=sprintf("%s/%s_s.jpg",$NewPath,$orgName);
				$resizeName2=sprintf("%s/%s_g.jpg",$NewPath,$orgName);
				if ($oldImgSize[1] > 40) {
					// FOR SMALL PREVIEW HEIGHT 40 PX
					$imgHeight=40;
					$imgWidth=round($imgHeight*$oldImgSize[0]/$oldImgSize[1]);
					$images_orig = ImageCreateFromJPEG($newFileName);
					$photoX = ImagesX($images_orig);
					$photoY = ImagesY($images_orig);
					$images_fin = ImageCreateTrueColor($imgWidth, $imgHeight);
					ImageCopyResampled($images_fin, $images_orig, 0, 0, 0, 0, $imgWidth+1, $imgHeight+1, $photoX, $photoY);
					ImageJPEG($images_fin,$resizeName1);
					ImageDestroy($images_orig);
					ImageDestroy($images_fin);				
					if ($oldImgSize[1] > 100) {
						// FOR GALLERY PREVIEW HEIGHT 100 PX
						$imgHeight=100;
						$imgWidth=round($imgHeight*$oldImgSize[0]/$oldImgSize[1]);
						$images_orig = ImageCreateFromJPEG($newFileName);
						$photoX = ImagesX($images_orig);
						$photoY = ImagesY($images_orig);
						$images_fin = ImageCreateTrueColor($imgWidth, $imgHeight);
						ImageCopyResampled($images_fin, $images_orig, 0, 0, 0, 0, $imgWidth+1, $imgHeight+1, $photoX, $photoY);
						ImageJPEG($images_fin,$resizeName2);
						ImageDestroy($images_orig);
						ImageDestroy($images_fin);				
					} else {
						copy($newFileName,$reSizeName2);
					}
				} else {
					copy($newFileName,$reSizeName1);
				}
				break;
			case 2: //VIDEOS 
				$newFile=sprintf("%s.webm",$orgName);
				$newFileName=sprintf("%s/%s",$NewPath,$newFile);
				if ($orgFileName != $newFileName) {
					$cmd=sprintf("ffmpeg -i %s -f webm %s",$orgFileName,$newFileName);
					exec($cmd,$output,$return);	
					if ($DEBUG) echo sprintf("<br>command => %s",$cmd); 
					if ($DEBUG) echo sprintf("<br>output => %s",$output); 
					if ($DEBUG) echo sprintf("<br>return => %s",$return); 
					unlink($orgFileName);
					$exten="webm";
				}
				break;
			case 3 : //shockwave flash
				break;
			case 4 : // Audio 
				break;
			case 9 : //Document;
				// Convert DOCS to PDF
				$newFile=sprintf("%s.pdf",$orgName);
				$pdfFileName=sprintf("%s/%s",$NewPath,$newFile);
//				$cmd=sprintf("soffice --headless --convert-to pdf --outdir %s %s",$pdfFileName,$orgFileName);
				$cmd=sprintf("soffice --headless --convert-to pdf --outdir %s %s",$NewPath,$orgFileName);
				exec($cmd,$output,$return);	
				if ($DEBUG) echo sprintf("<br>command => %s",$cmd); 
				if ($DEBUG) echo sprintf("<br>output => %s",$output); 
				if ($DEBUG) echo sprintf("<br>return => %s",$return); 
				unlink($orgFileName);

				// Convert PDF to SWF
				$newFile=sprintf("%s.swf",$orgName);
				$newFileName=sprintf("%s/%s",$NewPath,$newFile);
				$cmd=sprintf("pdf2swf %s -o %s",$pdfFileName,$newFileName);
				exec($cmd,$output,$return);	
				if ($DEBUG) echo sprintf("<br>command => %s",$cmd); 
				if ($DEBUG) echo sprintf("<br>output => %s",$output); 
				if ($DEBUG) echo sprintf("<br>return => %s",$return); 
				unlink($pdfFileName);
				$exten="swf";
				break;
			case 10 : //PDF;
				// Convert PDF to SWF
				$newFile=sprintf("%s.swf",$orgName);
				$newFileName=sprintf("%s/%s",$NewPath,$newFile);
				$cmd=sprintf("pdf2swf %s -o %s",$orgFileName,$newFileName);
				exec($cmd,$output,$return);	
				if ($DEBUG) echo sprintf("<br>command => %s",$cmd); 
				if ($DEBUG) echo sprintf("<br>output => %s",$output); 
				if ($DEBUG) echo sprintf("<br>return => %s",$return); 
				unlink($orgFileName);

				$exten="swf";
				break;
			case 11 : // COVERS
				$newFile=sprintf("%s.png",$orgName);
				$newFileName=sprintf("%s/%s",$NewPath,$newFile);
				$oldImgSize=getimagesize($newFileName);
				$resizeName1=sprintf("%s/%s_s.png",$NewPath,$orgName);
				if ($oldImgSize[1] > 40) {
					// FOR SMALL PREVIEW HEIGHT 40 PX
					$imgHeight=40;
					$imgWidth=round($imgHeight*$oldImgSize[0]/$oldImgSize[1]);
					$images_orig = ImageCreateFromPng($newFileName);
					$photoX = ImagesX($images_orig);
					$photoY = ImagesY($images_orig);
					$images_fin = ImageCreateTrueColor($imgWidth, $imgHeight);
					imagealphablending($images_fin, true); 
					$transparent = imagecolorallocatealpha( $images_fin, 0, 0, 0, 127 ); 
					imagefill( $images_fin, 0, 0, $transparent ); 

					ImageCopyResampled($images_fin, $images_orig, 0, 0, 0, 0, $imgWidth+1, $imgHeight+1, $photoX, $photoY);
					imagealphablending($images_fin, false);
					imagesavealpha($images_fin,true);  
					ImagePng($images_fin,$resizeName1);
					ImageDestroy($images_orig);
					ImageDestroy($images_fin);				
				} else {
					copy($newFileName,$reSizeName1);
				}
				break;
			case 12 : //PPT; 
				// Convert PPT to PDF
				$newFile=sprintf("%s.pdf",$orgName);
				$pdfFileName=sprintf("%s/%s",$NewPath,$newFile);
				$cmd=sprintf("soffice --headless --convert-to pdf --outdir %s %s",$NewPath,$orgFileName);
				exec($cmd,$output,$return);	
				if ($DEBUG) echo sprintf("<br>command => %s",$cmd); 
				if ($DEBUG) echo sprintf("<br>output => %s",$output); 
				if ($DEBUG) echo sprintf("<br>return => %s",$return); 
				unlink($orgFileName);

				// Convert PDF to SWF
				$newFile=sprintf("%s.swf",$orgName);
				$newFileName=sprintf("%s/%s",$NewPath,$newFile);
				$cmd=sprintf("pdf2swf %s -o %s",$pdfFileName,$newFileName);
				exec($cmd,$output,$return);	
				if ($DEBUG) echo sprintf("<br>command => %s",$cmd); 
				if ($DEBUG) echo sprintf("<br>output => %s",$output); 
				if ($DEBUG) echo sprintf("<br>return => %s",$return); 
				unlink($pdfFileName);

				$exten="swf";
				break;
		}

		$CRC=hash_file('md5',$newFileName);
		$sSQL=sprintf("UPDATE file_info SET file_success = 1 ");
//		$sSQL.=sprintf(",file_com_id=%d ",$arrFile['']);
//		$sSQL.=sprintf(",file_type_id=%d ",$typeID);
		$sSQL.=sprintf(",file_sname='%s' ",$newFile);
//		$sSQL.=sprintf(",file_oname='%s' ",$OldName);
//		$sSQL.=sprintf(",file_dname='%s' ",$sNewFile);
//		$sSQL.=sprintf(",file_path='%s' ",$type);
		$sSQL.=sprintf(",file_size=%d ",filesize($newFileName));
		$sSQL.=sprintf(",file_crc='%s' ",$CRC);
		$sSQL.=sprintf("WHERE file_id=%d ",$arrFile['file_id']);
		if ($DEBUG) echo "[".__FUNCTION__."] sSQL -> '$sSQL'<br>";
		$Result=AffectedSQL($sSQL);
		return (($Result==1)?$fileID:0);
	}
	
	function library_edit($FileID,$FileName) {
		global $DEBUG;
		
		$sSQL=sprintf("UPDATE file_info SET file_dname = '%s' ",$FileName);
		$sSQL.=sprintf("WHERE file_id=%d",$FileID);
		if ($DEBUG) echo "[".__FUNCTION__."] sSQL -> '$sSQL'<br>";
		$DBReturn=AffectedSQL($sSQL);
//		echo $DBReturn;
		return($DBReturn);
	}
	
	function library_active($FileID,$active=0) {
		global $DEBUG;
		
		$sSQL=sprintf("UPDATE file_info SET file_active = %d ",$active);
		$sSQL.=sprintf("WHERE file_id=%d",$FileID);
		if ($DEBUG) echo "[".__FUNCTION__."] sSQL -> '$sSQL'<br>";
		$DBReturn=AffectedSQL($sSQL);
//		echo $DBReturn;
		return($DBReturn);
	}

	function file_used($FileID) {
		global $DEBUG;
		
		$sSQL=sprintf("SELECT count(*) as counter FROM playlist_head,playlist_line WHERE (plh_id = pll_plh_id) ");
		$sSQL.=sprintf("AND (plh_active=1) AND (pll_active=1) AND (pll_file_id=%d) ",$FileID);
		if ($DEBUG) echo "[".__FUNCTION__."] sSQL -> '$sSQL'<br>";
		$DBReturn=ExecuteReader($sSQL);
		if ($Data=mysql_fetch_array($DBReturn)) {
//		echo $DBReturn;
			return($Data[0]['Counter']);
		} else {
			return 0;
		}
	}
	
	function video_convert($sPath,$sSrc,$sDest,$sSize) {
	}

?>