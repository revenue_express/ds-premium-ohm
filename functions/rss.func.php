<?

	function rss_info($Start=0,$PerPage=0) {
		global $DEBUG,$USER;
		$sSQL=sprintf("SELECT * FROM playlist_rss,rss_info_head WHERE (plrss_rinh_id = rinh_id) ");
		if ($USER['usr_level']<> 'Administrator') $sSQL.="AND (plrss_active=1) AND (rinh_active=1)";
		$sSQL.="ORDER BY plf_order,plf_id";
		if ($DEBUG) echo "<br>\n[".__FUNCTION__."] sSQL -> $sSQL";
		$Data=JSonSQL($sSQL);
		if ($DEBUG) { echo "<br>\nData<pre>"; print_r(json_decode($Data,true)); echo "</pre>"; }
		return($Data);
	}

	function rss_load($rssID,$expired=30) {
	}
	
	function playlist_rss_head_list($Start=0,$PerPage=0) {
		global $DEBUG,$USER;
		$sT1=sprintf("SELECT * FROM playlist_head,playlist_rss,rss_info_head WHERE (plrss_rinh_id = rinh_id) ");
		if ($USER['usr_level'] <> 'Administrator') $sT1.="AND (plrss_active=1) AND (rinh_active=1) ";
		$sT1.="AND (plh_id = plrss_plh_id) ";
		$sT1.="ORDER BY plrss_id";
		
		$sT2="SELECT *,count(*) as counter FROM rss_info_line WHERE (rinl_edate > NOW()) ";
		if ($USER['usr_level'] <> 'Administrator') $sT2.="AND (rinl_active=1) ";
		$sT2.="GROUP BY rinl_rinh_id";

		$sSQL=sprintf("SELECT * FROM (%s) as T1 LEFT JOIN (%s) as T2 ON T1.rinh_id = T2.rinl_rinh_id",$sT1,$sT2);
		if ($PerPage>0) {
			$sSQL.=sprintf("LIMIT %d,%d",$Start*$PerPage,$PerPage);
		}
		if ($DEBUG) echo "\n<br>\n[".__FUNCTION__."] sSQL -> $sSQL";
		$Data=JSonSQL($sSQL);
		if ($DEBUG) { echo "\n<br>\nData<pre>"; print_r(json_decode($Data,true)); echo "</pre>"; }
		return($Data);
	}

	function rss_line_list($HeadID,$Start=0,$PerPage=0) {
		global $DEBUG,$USER;
		$sSQL=sprintf("SELECT *,(rinl_edate < NOW()) as Expired FROM rss_info_line WHERE (rinl_rinh_id = %d) ",$HeadID);
		if ($USER['usr_level']<> 'Administrator') $sSQL.="AND (rinl_active=1)";
//		$sSQL.="AND (Expired=0) ";
		$sSQL.="ORDER BY rinl_order,rinl_id";
		if ($DEBUG) echo "\n<br>\n[".__FUNCTION__."] sSQL -> $sSQL";
		$Data=JSonSQL($sSQL);
		if ($DEBUG) { echo "<br>\nData<pre>"; print_r(json_decode($Data,true)); echo "</pre>"; }
		return($Data);
	}
	
	function rss_line_lastorder($HeadID) {
		global $DEBUG;
		$DBReturn=0;
			
		$sSQL=sprintf("SELECT rinl_order FROM rss_info_line WHERE rinl_rinh_id = %d ORDER BY rinl_order DESC LIMIT 1",$HeadID);
		if ($DEBUG) echo "\n<br>[".__FUNCTION__."] sSQL -> '$sSQL'";
		$Result=ExecuteReader($sSQL);
		if ($Data=mysql_fetch_array($Result)) {
			$DBReturn=$Data['rinl_order'];
		}
		return ($DBReturn);
	}
	
	function rss_line_add($RssID,$Title,$Desc,$Expire) {
		global $DEBUG;
		$OrderID=rss_line_lastorder($RssID);

		if ($DEBUG) echo sprintf("<br>date('Y-m-d') => '%s'",date('Y-m-d'));
		list($nYear,$nMonth,$nDay)=split("-",date('Y-m-d'));
		if ($DEBUG) echo sprintf("<br>nYear=>%d,nMonth=>%d,nDay=>%d",$nYear,$nMonth,$nDay);
		$ExDate=mktime(0,0,0,$nMonth,$nDay+$Expire,$nYear);
		if ($DEBUG) echo sprintf("<br>timestamp = '%s'",$ExDate);
		$sDate=date('Y-m-d H:i:s',$ExDate);
		if ($DEBUG) echo sprintf("<br>Date = '%s'",$sDate);

		$sSQL="INSERT INTO rss_info_line SET rinl_active=1 ";
		$sSQL.=sprintf(", rinl_rinh_id=%d ",$RssID);
		$sSQL.=sprintf(", rinl_title='%s' ",addslashes($Title));
		$sSQL.=sprintf(", rinl_data='%s' ",addslashes($Desc));
		$sSQL.=sprintf(", rinl_order=%d ",$OrderID+1);
		$sSQL.=sprintf(", rinl_edate='%s' ",$sDate);
		if ($DEBUG) echo "\n<br>[".__FUNCTION__."] sSQL -> '$sSQL'";
		$InsertID=InsertSQL($sSQL);
		return ($InsertID);
	}


	function rss_line_active($LineID,$active=0) {
		global $DEBUG,$USER;
		
		$sSQL=sprintf("UPDATE rss_info_line SET rinl_active = %d ",$active);
		$sSQL.=sprintf("WHERE rinl_id=%d",$LineID);
		if ($DEBUG) echo "\n<br>[".__FUNCTION__."] sSQL -> '$sSQL'";
		$DBReturn=AffectedSQL($sSQL);
//		echo $DBReturn;
		return($DBReturn);
	}

	function rss_line_order($LineID,$OrderID) {
		global $DEBUG,$USER;
		
		$sSQL=sprintf("UPDATE rss_info_line SET rinl_order = %d ",$OrderID);
		$sSQL.=sprintf("WHERE rinl_id=%d",$LineID);
		if ($DEBUG) echo "\n<br>[".__FUNCTION__."] sSQL -> '$sSQL'";
		$DBReturn=AffectedSQL($sSQL);
//		echo $DBReturn;
		return($DBReturn);
	}

	function rss_head_info($HeadID) {
		global $DEBUG,$USER;
		$sSQL=sprintf("SELECT * FROM playlist_head, playlist_rss, rss_info_head ");
		$sSQL.=sprintf("WHERE (plrss_rinh_id = rinh_id) AND (plrss_plh_id=plh_id) ");
		$sSQL.=sprintf("AND (plh_id=%d) ",$HeadID);
		if ($USER['usr_level']<> 'Administrator') $sSQL.="AND (plrss_active=1) AND (rinh_active=1)";
//		$sSQL.="ORDER BY plf_order,plf_id";
		if ($DEBUG) echo "<br>\n[".__FUNCTION__."] sSQL -> $sSQL";
		$Data=JSonSQL($sSQL);
		if ($DEBUG) { echo "<br>\nData<pre>"; print_r(json_decode($Data,true)); echo "</pre>"; }
		return($Data);
	}
	
	function rss_delete_old($RssID) {
		global $DEBUG,$USER;
		$sSQL=sprintf("DELETE FROM rss_info_line WHERE rinl_rinh_id=%d",$RssID);
		if ($DEBUG) echo "\n<br>[".__FUNCTION__."] sSQL -> '$sSQL'";
		$DBReturn=AffectedSQL($sSQL);
//		echo $DBReturn;
		return($DBReturn);
	}
	
	function rss_head_update($RssID,$URL) {
		global $DEBUG,$USER;
		
		$sSQL=sprintf("UPDATE rss_info_head SET rinh_adate = '%s' ",date('Y-m-d H:i:s'));
		$sSQL.=sprintf(", rinh_url='%s' ",addslashes($URL));
		$sSQL.=sprintf("WHERE rinh_id=%d ",$RssID);
		if ($DEBUG) echo "\n<br>[".__FUNCTION__."] sSQL -> '$sSQL'";
		$DBReturn=AffectedSQL($sSQL);
//		echo $DBReturn;
		return($DBReturn);
	}
	
	function rss_head_add($ComID,$Link) {
		global $DEBUG,$USER;

		$sSQL="INSERT INTO rss_info_head SET rinh_active=1 ";
		$sSQL.=sprintf(", rinh_com_id=%d ",$ComID);
		$sSQL.=sprintf(", rinh_url='%s' ",addslashes($Link));
		$sSQL.=sprintf(", rinh_adate='2012-01-01 00:00:00' ");
		if ($DEBUG) echo "\n<br>[".__FUNCTION__."] sSQL -> '$sSQL'";
		$InsertID=InsertSQL($sSQL);
		return ($InsertID);
	}

	function playlist_rss_add($HeadID,$RssID) {
		global $DEBUG,$USER;

		$sSQL="INSERT INTO playlist_rss SET plrss_active=1 ";
		$sSQL.=sprintf(", plrss_plh_id=%d ",$HeadID);
		$sSQL.=sprintf(", plrss_rinh_id=%d ",$RssID);
		if ($DEBUG) echo "\n<br>[".__FUNCTION__."] sSQL -> '$sSQL'";
		$InsertID=InsertSQL($sSQL);
		return ($InsertID);
	}

?>