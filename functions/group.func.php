<?
	function group_list($ComID=1) {
		global $DEBUG,$USER;
		$sSQL=sprintf("SELECT * FROM group_info WHERE grp_com_id=%d ORDER BY grp_id",$ComID);
		if ($DEBUG) echo "[".__FUNCTION__."] sSQL -> '$sSQL'<br>";
		$DBReturn=JSonSQL($sSQL);
//		echo $DBReturn;
		return($DBReturn);
	}

	function group_add($ComID=1) {
		global $DEBUG,$USER;
		$sCode=group_dummyID($ComID);

		$sSQL=sprintf("INSERT INTO group_info SET grp_com_id=%d, grp_code='%s', grp_name='%s'",$ComID,$sCode,$sCode);
		if ($DEBUG) echo "[".__FUNCTION__."] sSQL -> '$sSQL'<br>";
		$DBReturn=InsertSQL($sSQL);
//		echo $DBReturn;
		return($DBReturn);
	}
	
	function group_active($GroupID,$Active=0) {
		global $DEBUG,$USER;
		$sSQL=sprintf("UPDATE group_info SET grp_active=%d WHERE grp_id=%d",$Active,$GroupID);
		if ($DEBUG) echo "[".__FUNCTION__."] sSQL -> '$sSQL'<br>";
		$DBReturn=AffectedSQL($sSQL);
//		echo $DBReturn;
		return($DBReturn);
	}
	
	function group_edit($ComID=1,$GroupID,$Code,$Name,$Desc=NULL) {
		global $DEBUG,$USER;
		$sSQL=sprintf("UPDATE group_info SET grp_com_id=%d, grp_code='%s', grp_name='%s', grp_desc='%s'",$ComID,$Code,$Name,$Desc);
		$sSQL.=sprintf(" WHERE grp_id=%d",$GroupID);
		if ($DEBUG) echo "[".__FUNCTION__."] sSQL -> '$sSQL'<br>";
		$DBReturn=AffectedSQL($sSQL);
//		echo $DBReturn;
		return($DBReturn);
	}

	function group_dummyID($ComID=1) {
		global $DEBUG,$USER;
		$sHead=sprintf("%s",date('ymd'));
		$sSQL=sprintf("SELECT * FROM  group_info WHERE (grp_code like '%s%%') ",$sHead);
		$sSQL.=sprintf("AND (grp_com_id=%d) ",$ComID);
		$sSQL.=sprintf("ORDER BY grp_code DESC LIMIT 1 ");
		if ($DEBUG) echo "[".__FUNCTION__."] sSQL -> '$sSQL'<br>";
		$iCount=1;
		$Result=ExecuteReader($sSQL);
		if ($Data=mysql_fetch_array($Result)) {
			$Num=$Data['grp_code'];
			if ($DEBUG) echo "[".__FUNCTION__."] grp_code -> '$Num'<br>";
			$iCount=intval(substr($Num,strlen($sHead)));
			$iCount++;
			if ($DEBUG) echo "[".__FUNCTION__."] iCount -> '$iCount'<br>";
		}
		$Code=sprintf("%s%03d",$sHead,$iCount);
			if ($DEBUG) echo "[".__FUNCTION__."] Return -> '$Code'<br>";
		return($Code);		
	}

	function group_view($GroupID) {
		global $DBReturn,$DEBUG;
		$sSQL=sprintf("SELECT * FROM group_info WHERE grp_id=%d LIMIT 1",$GroupID);
		if ($DEBUG) echo "[".__FUNCTION__."] sSQL -> '$sSQL'<br>";
		$DBReturn=JSonSQL($sSQL);
//		echo $DBReturn;
		return($DBReturn);
	}

	function home_group_list($ComID=1) {
		global $DEBUG,$USER;
		$sSQL=sprintf("SELECT * FROM group_info, ");
		$sSQL.=sprintf(" (SELECT cli_grp_id,cli_code,cli_name , count(*) as items FROM client_info GROUP BY cli_grp_id ) AS T1 ");
		$sSQL.=sprintf(" WHERE (grp_id = cli_grp_id) AND (grp_com_id=%d)",$ComID);
		if ($DEBUG) echo "[".__FUNCTION__."] sSQL -> '$sSQL'<br>";
		$DBReturn=JSonSQL($sSQL);
//		echo $DBReturn;
		return($DBReturn);
	}
?>