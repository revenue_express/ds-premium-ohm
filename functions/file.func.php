<?
	function file_list($TypeID,$Page=0,$PerPage=0,$ComID=1)	{
		global $DEBUG,$USER;
		$sSQL=sprintf("SELECT * FROM file_info WHERE file_com_id = %d ",$ComID);
		$sSQL.=sprintf("AND (file_type_id = %d) ",$TypeID);
		if ($USER['usr_level'] <> 'Administrator') $sTable.="AND (file_active = 1) "; 
		if ( ($PerPage <> 0) && ($Page <> 0) ) {
			$sSQL.=sprintf(" LIMIT %d,%d",$Page*$PerPage,$PerPage);
		}
		if ($DEBUG) echo "<br>[".__FUNCTION__."] sSQL -> '$sSQL'";
		$DBReturn=JSonSQL($sSQL);
//		echo $DBReturn;
		return($DBReturn);
	}
	
	function file_info($FileID) {
		global $DEBUG,$USER;
		$sSQL=sprintf("SELECT * FROM file_info WHERE (file_id = %d) ",$FileID);
		if ($DEBUG) echo "<br>[".__FUNCTION__."] sSQL -> '$sSQL'";
		$DBReturn=JSonSQL($sSQL);
//		echo $DBReturn;
		return($DBReturn);
	}
	
	function file_update($FileID,$FileName) {
		global $DEBUG,$USER;
		$sSQL=sprintf("UPDATE file_info SET file_dname = '%s' WHERE file_id=%d",$FileName,$FileID);
		if ($DEBUG) echo "<br>[".__FUNCTION__."] sSQL -> '$sSQL'";
		$DBReturn=AffectedSQL($sSQL);
//		echo $DBReturn;
		return($DBReturn);
	}

?>