<?
	function user_list($ComID=1) {
		global $DEBUG,$USER;
		$sSQL=sprintf("SELECT * FROM user_info,level_info WHERE (usr_com_id=%d) ",$ComID);
		if (strtolower($USER['usr_level']) == 'operator') $sSQL.="AND (usr_active = 1) "; 
		$sSQL.="AND (usr_lvl_id =lvl_id) ORDER BY usr_lvl_id,usr_active DESC";
		if ($DEBUG) echo "[".__FUNCTION__."] sSQL -> '$sSQL'<br>";
		$DBReturn=JSonSQL($sSQL);
//		echo $DBReturn;
		return($DBReturn);
	}

	function user_view($usrID) {
		global $DEBUG,$USER;
		$sSQL=sprintf("SELECT * FROM user_info,level_info WHERE (usr_id=%d) ",$usrID);
		if ($USER['usr_level'] <> 'Administrator') $sSQL.="AND (usr_active = 1) "; 
		$sSQL.="AND (usr_lvl_id =lvl_id) ORDER BY usr_lvl_id LIMIT 0,1";
		if ($DEBUG) echo "[".__FUNCTION__."] sSQL -> '$sSQL'<br>";
		$DBReturn=JSonSQL($sSQL);
//		echo $DBReturn;
		return($DBReturn);
	}

	function user_info($usrID) {
		global $DEBUG,$USER;
		$sSQL=sprintf("SELECT * FROM user_info,level_info WHERE (usr_id=%d) ",$usrID);
		if ($USER['usr_level'] <> 'Administrator') $sSQL.="AND (usr_active = 1) "; 
		$sSQL.="AND (usr_lvl_id =lvl_id) ORDER BY usr_lvl_id LIMIT 0,1";
		if ($DEBUG) echo "[".__FUNCTION__."] sSQL -> '$sSQL'<br>";
		$DBReturn=JSonSQL($sSQL);
//		echo $DBReturn;
		return($DBReturn);
	}
	
	function user_edit($usrID,$usrName=NULL,$usrLevel=NULL,$usrQuest=NULL,$usrAns=NULL,$usrEmail=NULL) {
		global $DEBUG,$USER;
		$sSQL="UPDATE user_info SET usr_active=1 ";
		if (! is_null($usrName)) $sSQL.=sprintf(",usr_name = '%s' ",$usrName);
		if (! is_null($usrLevel)) $sSQL.=sprintf(",usr_lvl_id = %d ",$usrLevel);
		if (! is_null($usrQuest)) $sSQL.=sprintf(",usr_hint = '%s' ",$usrQuest);
		if (! is_null($usrAns)) $sSQL.=sprintf(",usr_answer = '%s' ",$usrAns);
		if (! is_null($usrEmail)) $sSQL.=sprintf(",usr_email = '%s' ",$usrEmail);
		$sSQL.=sprintf("WHERE usr_id = %d",$usrID);
		if ($DEBUG) echo "[".__FUNCTION__."] sSQL -> '$sSQL'<br>";
		$DBReturn=AffectedSQL($sSQL);
		if ($DEBUG) echo "[".__FUNCTION__."] Retrun -> '$DBReturn'<br>";
		return ($DBReturn);
	}

	function user_add($ComID=1,$usrLogin=NULL,$usrName=NULL,$UsrPass=NULL,$usrLevel=NULL,$usrQuest=NULL,$usrAns=NULL,$usrEmail=NULL) {
		global $DEBUG,$USER;
		$sSQL=sprintf("INSERT INTO user_info SET usr_active=1, usr_com_id=%d ",$ComID);
		$sSQL.=sprintf(", usr_login='%s' ",$usrLogin);
		$sSQL.=sprintf(", usr_name='%s' ",$usrName);
		$sSQL.=sprintf(", usr_passwd=MD5('%s') ",$usrPass);
		$sSQL.=sprintf(", usr_hint='%s' ",$usrQuest);
		$sSQL.=sprintf(", usr_answer='%s' ",$usrAns);
		$sSQL.=sprintf(", usr_lvl_id=%d ",$usrLevel);
		$sSQL.=sprintf(", usr_email='%s' ",$usrEmail);
		if ($DEBUG) echo "[".__FUNCTION__."] sSQL -> '$sSQL'<br>";
		$DBReturn=InsertSQL($sSQL);
		if ($DEBUG) echo "[".__FUNCTION__."] Retrun -> '$DBReturn'<br>";
		return ($DBReturn);
	}
	
	function user_check_login($ComID=1,$usrLogin) {
		global $DEBUG,$USER;
		$sSQL=sprintf("SELECT * FROM user_info WHERE (usr_com_id=%d ) AND (usr_login='%s') ",$ComID,$usrLogin);
		if ($USER['usr_level'] <> 'Administrator') $sSQL.="AND (usr_active = 1) "; 
		if ($DEBUG) echo "[".__FUNCTION__."] sSQL -> '$sSQL'<br>";
		$DBReturn=ExecuteReader($sSQL);
		$iCount=mysql_num_rows($DBReturn);
		return ($iCount>0);
	}

	function user_active($usrID,$usrActive=0) {
		global $DEBUG,$USER;
		$sSQL=sprintf("UPDATE user_info SET usr_active=%d WHERE usr_id=%d",$usrActive,$usrID);
		if ($DEBUG) echo "[".__FUNCTION__."] sSQL -> '$sSQL'<br>";
		$DBReturn=AffectedSQL($sSQL);
		if ($DEBUG) echo "[".__FUNCTION__."] Retrun -> '$DBReturn'<br>";
		return ($DBReturn);
	}

	function user_change_pass($usrID,$usrOldPass,$usrNewPass) {
		global $DEBUG,$USER;
		$sSQL=sprintf("UPDATE user_info SET usr_passwd=MD5('%s') WHERE (usr_id=%d) AND (usr_passwd=MD5('%s'))",$usrNewPass,$usrID,$usrOldPass);
		if ($DEBUG) echo "[".__FUNCTION__."] sSQL -> '$sSQL'<br>";
		$DBReturn=AffectedSQL($sSQL);
		if ($DEBUG) echo "[".__FUNCTION__."] Retrun -> '$DBReturn'<br>";
		return ($DBReturn);
	}
	
	function user_check_password($usrID,$usrPass) {
		global $DEBUG,$USER;
		$sSQL=sprintf("SELECT * FROM user_info WHERE (usr_id=%d) AND (usr_passwd=MD5('%s')) ",$usrID,$usrPass);
		if ($USER['usr_level'] <> 'Administrator') $sSQL.="AND (usr_active = 1) "; 
		if ($DEBUG) echo "[".__FUNCTION__."] sSQL -> '$sSQL'<br>";
		$DBReturn=ExecuteReader($sSQL);
		$iCount=mysql_num_rows($DBReturn);
		return ($iCount>0);
	}

	function user_reset_pass($usrID,$usrNewPass) {
		global $DEBUG,$USER;
		$sSQL=sprintf("UPDATE user_info SET usr_passwd=MD5('%s') WHERE (usr_id=%d)",$usrNewPass,$usrID);
		if ($DEBUG) echo "[".__FUNCTION__."] sSQL -> '$sSQL'<br>";
		$DBReturn=AffectedSQL($sSQL);
		if ($DEBUG) echo "[".__FUNCTION__."] Retrun -> '$DBReturn'<br>";
		return ($DBReturn);
	}

?>