<?
	function stream_list($Start=0,$PerPage=0) {
		global $DEBUG,$USER;
		$sSQL="SELECT * FROM stream_info WHERE true ";
		if (strtolower($USER['usr_level']) == 'operator') $sSQL.="AND (stm_active=1)";
		if (($Start >=0) && ($PerPage >0)) {
			$sSQL.=sprintf("LIMIT %d,%d",$Start*$PerPage,$PerPage);
		}
		if ($DEBUG) echo "<br>\n[".__FUNCTION__."] sSQL -> $sSQL";
		$Data=JSonSQL($sSQL);
		if ($DEBUG) { echo "<br>\nData<pre>"; print_r(json_decode($Data,true)); echo "</pre>"; }
		return($Data);
	}

	function stream_info($StreamID) {
		global $DEBUG,$USER;
		$sSQL=sprintf("SELECT * FROM stream_info WHERE (stm_id=%d) ",$StreamID);
		if (strtolower($USER['usr_level']) == 'operator') $sSQL.="AND (stm_active=1)";
		if ($DEBUG) echo "<br>\n[".__FUNCTION__."] sSQL -> $sSQL";
		$Data=JSonSQL($sSQL);
		if ($DEBUG) { echo "<br>\nData<pre>"; print_r(json_decode($Data,true)); echo "</pre>"; }
		return($Data);
	}

	function stream_add($ComID=1,$Protocol='http',$Address='localhost',$Port=8080,$Para01=NULL,$Para02=NULL) {
		global $DEBUG,$USER;
		$sSQL=sprintf("INSERT INTO stream_info SET stm_active=1, stm_com_id=%d ",$ComID);
		if (! is_null($Protocol)) $sSQL.=sprintf(", stm_protocol='%s' ",$Protocol);
		if (! is_null($Address)) $sSQL.=sprintf(", stm_address='%s' ",$Address);
		if (! is_null($Address)) $sSQL.=sprintf(", stm_port=%d ",$Port);
		if (! is_null($Para01)) $sSQL.=sprintf(", stm_para1='%s' ",$Para01);
		if (! is_null($Para02)) $sSQL.=sprintf(", stm_para2='%s' ",$Para02);
//		if (strtolower($USER['usr_level']) == 'operator') $sSQL.=", feed_active=1 ";
		if ($DEBUG) echo "<br>\n[".__FUNCTION__."] sSQL -> $sSQL";
		$Data=InsertSQL($sSQL);
		if ($DEBUG) { echo "<br>\nData=> $Data"; }
		return($Data);
	}

	function stream_edit($StreamID,$Protocol='http',$Address='localhost',$Port=8080,$Para01=NULL,$Para02=NULL) {
		global $DEBUG,$USER;
		$sSQL=sprintf("UPDATE stream_info SET stm_id=%d ",$StreamID);
		
		$sSQL.=sprintf(", stm_protocol='%s' ",$Protocol);
		$sSQL.=sprintf(", stm_address='%s' ",$Address);
		$sSQL.=sprintf(", stm_port=%d ",$Port);
		$sSQL.=sprintf(", stm_para1='%s' ",$Para01);
		$sSQL.=sprintf(", stm_para2='%s' ",$Para02);
		$sSQL.=sprintf("WHERE stm_id=%d",$StreamID);
		if ($DEBUG) echo "<br>\n[".__FUNCTION__."] sSQL -> $sSQL";
		$Data=AffectedSQL($sSQL);
		if ($DEBUG) { echo "<br>\nData=> $Data"; }
		return($Data);
	}

	function stream_active($StreamID,$Active=0) {
		global $DEBUG,$USER;
		$sSQL=sprintf("UPDATE stream_info SET stm_active=%d WHERE stm_id=%d",$Active,$StreamID);
		if ($DEBUG) echo "<br>\n[".__FUNCTION__."] sSQL -> $sSQL";
		$Data=AffectedSQL($sSQL);
		if ($DEBUG) { echo "<br>\nData=> $Data"; }
		return($Data);
	}

	function stream_delete($LineID) {
		global $DEBUG,$USER;
		$sSQL=sprintf("DELETE FROM stream_info WHERE stm_id=%d ",$LineID);
		if ($DEBUG) echo "<br>\n[".__FUNCTION__."] sSQL -> $sSQL";
		$Data=AffectedSQL($sSQL);
		if ($DEBUG) { echo "<br>\nData=> $Data"; }
		return($Data);
	}

	function playlist_stream_dummyID() {
		global $DEBUG,$USER;
		$sHead=sprintf("%s",date('ymd'));
		$sSQL=sprintf("SELECT * FROM  playlist_stream WHERE (plstm_code like '%s%%') ",$sHead);
		$sSQL.=sprintf("ORDER BY plstm_code DESC LIMIT 1 ");
		if ($DEBUG) echo "<br>\n[".__FUNCTION__."] sSQL -> '$sSQL'";
		$iCount=1;
		$Result=ExecuteReader($sSQL);
		if ($Data=mysql_fetch_array($Result)) {
			$Num=$Data['plstm_code'];
//			if ($DEBUG) echo "[".__FUNCTION__."] plh_code -> '$Num'<br>";
			$iCount=intval(substr($Num,strlen($sHead)));
			$iCount++;
//			if ($DEBUG) echo "[".__FUNCTION__."] iCount -> '$iCount'<br>";
		}
		$Code=sprintf("%s%03d",$sHead,$iCount);
//			if ($DEBUG) echo "[".__FUNCTION__."] Return -> '$Code'<br>";
		return($Code);		
	}

	function stream_used($StreamID) {
		global $DEBUG,$USER;
		
		$sSQL=sprintf("SELECT count(*) as counter FROM playlist_head,playlist_stream WHERE (plh_id = plstm_plh_id) ");
//		$sSQL.=sprintf("AND (tmph_active=1) AND (tmpc_active=1)");
		if (strtolower($USER['usr_level']) == 'operator')	$sSQL.="AND (plh_active=1) AND (plstm_active=1) ";
		$sSQL.=sprintf("AND (plstm_stm_id=%d) ",$StreamID);
		if ($DEBUG) echo "[".__FUNCTION__."] sSQL -> '$sSQL'<br>";
		$DBReturn=ExecuteReader($sSQL);
		if ($Data=mysql_fetch_array($DBReturn)) {
//		echo $DBReturn;
			return($Data[0]['Counter']);
		} else {
			return 0;
		}
	}

	function playlist_stream_list() {
		global $DEBUG,$USER;
		$sSQL=sprintf("SELECT * FROM playlist_stream,stream_info WHERE (plstm_stm_id = stm_id) ");
		if (strtolower($USER['usr_level']) == 'operator') $sSQL.="AND (plstm_active=1) AND (stm_active=1)";
		$sSQL.="ORDER BY plstm_id";
		if ($DEBUG) echo "<br>\n[".__FUNCTION__."] sSQL -> $sSQL";
		$Data=JSonSQL($sSQL);
		if ($DEBUG) { echo "<br>\nData<pre>"; print_r(json_decode($Data,true)); echo "</pre>"; }
		return($Data);
	}

	function playlist_stream_info($HeadID) {
		global $DEBUG,$USER;
		$sSQL=sprintf("SELECT * FROM playlist_stream,stream_info WHERE (plstm_stm_id = stm_id) ");
		if (strtolower($USER['usr_level']) == 'operator') $sSQL.="AND (plstm_active=1) AND (stm_active=1) ";
		$sSQL.=sprintf("AND plstm_plh_id=%d ",$HeadID);
		$sSQL.="ORDER BY plstm_id";
		if ($DEBUG) echo "<br>\n[".__FUNCTION__."] sSQL -> $sSQL";
		$Data=JSonSQL($sSQL);
		if ($DEBUG) { echo "<br>\nData<pre>"; print_r(json_decode($Data,true)); echo "</pre>"; }
		return($Data);
	}

	function playlist_stream_add($HeadID,$StreamID,$Name=NULL) {
		global $DEBUG,$USER;
		$Code=playlist_stream_dummyID();
		if (is_null($Name)) $Name=$Code;
		
		$sSQL=sprintf("INSERT INTO playlist_stream SET plstm_active=1 ");
		$sSQL.=sprintf(", plstm_plh_id=%d ",$HeadID);
		$sSQL.=sprintf(", plstm_stm_id=%d ",$StreamID);
		$sSQL.=sprintf(", plstm_code='%s' ",$Code);
		$sSQL.=sprintf(", plstm_name='%s' ",$Name);
		if ($DEBUG) echo "<br>\n[".__FUNCTION__."] sSQL -> $sSQL";
		$Data=InsertSQL($sSQL);
		if ($DEBUG) { echo "<br>\nData=> $Data"; }
		return($Data);
	}
	
	function playlist_stream_edit($LineID,$StreamID,$Name=NULL) {
		global $DEBUG,$USER;
		$sSQL=sprintf("UPDATE playlist_stream SET plstm_id = %d",$LineID);
		$sSQL.=sprintf(", plstm_stm_id=%d ",$StreamID);
		$sSQL.=sprintf(", plstm_name='%s' ",$Name);
		$sSQL.=sprintf("WHERE plstm_id=%d ",$LineID);
		if ($DEBUG) echo "<br>\n[".__FUNCTION__."] sSQL -> $sSQL";
		$Data=AffectedSQL($sSQL);
		if ($DEBUG) { echo "<br>\nData=> $Data"; }
		return($Data);
	}

	function playlist_stream_active($LineID,$Active=0) {
		global $DEBUG,$USER;
		$sSQL=sprintf("UPDATE playlist_stream SET plstm_active = %d WHERE plstm_id=%d ",$Active,$LineID);
		if ($DEBUG) echo "<br>\n[".__FUNCTION__."] sSQL -> $sSQL";
		$Data=AffectedSQL($sSQL);
		if ($DEBUG) { echo "<br>\nData=> $Data"; }
		return($Data);
	}

	function playlist_stream_delete($LineID) {
		global $DEBUG,$USER;
		$sSQL=sprintf("DELETE FROM playlist_stream WHERE plstm_id=%d ",$LineID);
		if ($DEBUG) echo "<br>\n[".__FUNCTION__."] sSQL -> $sSQL";
		$Data=AffectedSQL($sSQL);
		if ($DEBUG) { echo "<br>\nData=> $Data"; }
		return($Data);
	}

//	================================
//	Remove Playlist Section
// ================================

	function playlist_stream_remove($LineID,$optALL=NULL) {
		global $DEBUG,$USER;
		$sSQL=sprintf("DELETE FROM playlist_stream");
		if (is_null($optALL)) {
			$sSQL.=sprintf(" WHERE plstm_id=%d ",$LineID);
		} else {
			$sSQL.=sprintf(" WHERE plstm_plh_id=%d ",$LineID);
		}
		if ($DEBUG) echo "<br>\n[".__FUNCTION__."] sSQL -> $sSQL";
		$DBReturn=AffectedSQL($sSQL);
		return($DBReturn);
	}

?>