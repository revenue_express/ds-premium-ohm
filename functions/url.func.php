<?

	function playlist_url_dummyID() {
		global $DEBUG,$USER;
		$sHead=sprintf("%s",date('ymd'));
		$sSQL=sprintf("SELECT * FROM  playlist_url WHERE (plurl_code like '%s%%') ",$sHead);
		$sSQL.=sprintf("ORDER BY plurl_code DESC LIMIT 1 ");
		if ($DEBUG) echo "<br>\n[".__FUNCTION__."] sSQL -> '$sSQL'";
		$iCount=1;
		$Result=ExecuteReader($sSQL);
		if ($Data=mysql_fetch_array($Result)) {
			$Num=$Data['plurl_code'];
//			if ($DEBUG) echo "[".__FUNCTION__."] plh_code -> '$Num'<br>";
			$iCount=intval(substr($Num,strlen($sHead)));
			$iCount++;
//			if ($DEBUG) echo "[".__FUNCTION__."] iCount -> '$iCount'<br>";
		}
		$Code=sprintf("%s%03d",$sHead,$iCount);
//			if ($DEBUG) echo "[".__FUNCTION__."] Return -> '$Code'<br>";
		return($Code);		
	}


	function playlist_url_add($HeadID,$Name=NULL,$Address=NULL) {
		global $DEBUG,$USER;

		$Code=playlist_url_dummyID($ComID);
		if (is_null($Name)) $Name=$Code;
		$sSQL="INSERT INTO playlist_url SET plurl_active=1 ";
		$sSQL.=sprintf(", plurl_plh_id=%d ",$HeadID);
		$sSQL.=sprintf(", plurl_code='%s' ",$Code);
		$sSQL.=sprintf(", plurl_name='%s' ",$Name);
		if (! is_null($Addres)) $sSQL.=sprintf(", plurl_address='%s' ",$Address);

		if ($DEBUG) echo "<br>\n[".__FUNCTION__."] sSQL -> $sSQL";
		$Data=InsertSQL($sSQL);
		if ($DEBUG) { echo sprintf("<br>\nData => %d<br>\n",$Data); } 
		return($Data);
	}

	function playlist_url_edit($HeadID,$Name=NULL,$Address=NULL) {
		global $DEBUG,$USER;

		$sSQL="UPDATE playlist_url SET plurl_active=1 ";
		$sSQL.=sprintf(", plurl_name='%s' ",$Name);
		$sSQL.=sprintf(", plurl_address='%s' ",$Address);
		$sSQL.=sprintf("WHERE plurl_id=%d ",$HeadID);
		
		if ($DEBUG) echo "<br>\n[".__FUNCTION__."] sSQL -> $sSQL";
		$Data=AffectedSQL($sSQL);
		if ($DEBUG) { echo sprintf("<br>\nData => %d<br>\n",$Data); } 
		return($Data);
	}

	function playlist_url_active($PlaylistID,$Active=0) {
		global $DEBUG,$USER;
		$sSQL=sprintf("UPDATE playlist_url SET plurl_active=%d WHERE plurl_id=%d ",$Active,$PlaylistID);

		if ($DEBUG) echo "<br>\n[".__FUNCTION__."] sSQL -> $sSQL";
		$Data=AffectedSQL($sSQL);
		if ($DEBUG) { echo sprintf("<br>\nData => %d<br>\n",$Data); } 
		return($Data);
	}

	function playlist_url_list($Start=0,$PerPage=0) {
		global $DEBUG,$USER;

		$sSQL=sprintf("SELECT * FROM playlist_url WHERE true ");
		if ($USER['usr_level']<> 'Administrator') $sSQL.="AND (plurl_active=1) ";
		if (($Start >=0) && ($PerPage >0)) {
			$sSQL.=sprintf("LIMIT %d,%d",$Start*$PerPage,$PerPage);
		}
		if ($DEBUG) echo "<br>\n[".__FUNCTION__."] sSQL -> $sSQL";
		$Data=JSonSQL($sSQL);
		if ($DEBUG) { echo "<br>\nData\n<pre>"; print_r(json_decode($Data,true)); echo "</pre>\n"; } 
		return($Data);
	}

	function playlist_url_info($UrlID) {
		global $DEBUG,$USER;

		$sSQL=sprintf("SELECT * FROM playlist_url WHERE plurl_id =%d ",$UrlID);
		if ($USER['usr_level']<> 'Administrator') $sSQL.="AND (plurl_active=1) ";

		if ($DEBUG) echo "<br>\n[".__FUNCTION__."] sSQL -> $sSQL";
		$Data=JSonSQL($sSQL);
		if ($DEBUG) { echo "<br>\nData\n<pre>"; print_r(json_decode($Data,true)); echo "</pre>\n"; } 
		return($Data);
	}

?>