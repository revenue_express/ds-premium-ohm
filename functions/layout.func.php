<?
	function layout_head_list($ComID=1,$login=1, $ppage=0, $limit=0, $spage=0) {
		global $DEBUG,$USER;

		$sSQL=sprintf("SELECT * FROM  layout_head,template_head WHERE (layh_com_id = %d) ",$ComID);
		$sSQL.="AND (tmph_id = layh_tmph_id) ";
//		if ($USER['usr_level'] <> 'Administrator') $sSQL.="AND (layh_active = 1) "; 
		$sSQL.="ORDER BY layh_active DESC,layh_id";
		if ( ($ppage <> 0) && ($limite<> 0) && ($spage <> 0) ) {
			$sSQL.=sprintf(" LIMIT %d,%d",$spage*$limit,$limit);
		}
		if ($DEBUG) echo "<br>[".__FUNCTION__."] sSQL -> '$sSQL'";
		$DBReturn=JSonSQL($sSQL);
//		echo $DBReturn;
		return($DBReturn);
	}
	
	function layout_head_info($HeadID) {
		global $DEBUG,$USER;

		$sSQL="SELECT * FROM  layout_head,template_head WHERE  (tmph_id = layh_tmph_id) ";
//		$sSQL.=sprintf("AND (tmph_id = %d) ",$HeadID);
		$sSQL.=sprintf("AND (layh_id = %d) ",$HeadID);
//		if ($USER['usr_level'] <> 'Administrator') $sSQL.="AND (layh_active = 1) ";
		$sSQL.="LIMIT 0,1"; 
		if ($DEBUG) echo "<br>[".__FUNCTION__."] sSQL -> '$sSQL'";
		$DBReturn=JSonSQL($sSQL);
//		echo $DBReturn;
		return($DBReturn);
	}

	function layout_head_add($ComID=1,$Name,$TemplateID) {
		global $DEBUG,$USER;
		$sSQL="INSERT INTO layout_head SET layh_active=1 ";
		$sSQL.=sprintf(",layh_com_id=%d",$ComID);
		$sSQL.=sprintf(",layh_name='%s'",$Name);
		$sSQL.=sprintf(",layh_tmph_id='%d'",$TemplateID);
		if ($DEBUG) echo "<br>[".__FUNCTION__."] sSQL -> '$sSQL'";
		$DBReturn=InsertSQL($sSQL);
		if ($DBReturn > 0) {
			return ($DBReturn);
		}
	}

	function layout_head_active($TID,$Active=1) {
		global $DEBUG,$USER;
		$sSQL=sprintf("UPDATE layout_head SET layh_active=%d WHERE layh_id=%d",$Active,$TID);
		if ($DEBUG) echo "<br>[".__FUNCTION__."] sSQL -> '$sSQL'";
		$DBReturn=AffectedSQL($sSQL);
		return ($DBReturn == 1);
	}

	function layout_head_edit($TID,$Name) {
		global $DEBUG,$USER;
		$sSQL=sprintf("UPDATE layout_head SET layh_name='%s' WHERE layh_id=%d",$Name,$TID);
		if ($DEBUG) echo "<br>[".__FUNCTION__."] sSQL -> '$sSQL'";
		$DBReturn=AffectedSQL($sSQL);
		return ($DBReturn == 1);
	}

	function layout_line_add($HeadID,$TemplateID,$PlayListID) {
		global $DEBUG,$USER;
		$sSQL="INSERT INTO layout_line SET layl_active=1 ";
		$sSQL.=sprintf(",layl_layh_id=%d",$HeadID);
		$sSQL.=sprintf(",layl_tmpl_id='%d'",$TemplateID);
		$sSQL.=sprintf(",layl_plh_id='%d'",$PlayListID);
		if ($DEBUG) echo "<br>[".__FUNCTION__."] sSQL -> '$sSQL'";
		$DBReturn=InsertSQL($sSQL);
		if ($DBReturn > 0) {
			return ($DBReturn);
		}
	}
	
	function layout_head_clone($ComID,$OldID) {
		global $DEBUG,$USER;
		$sSQL=sprintf("INSERT INTO layout_head (layh_com_id,layh_tmph_id,layh_name) ");
		$sSQL.=sprintf("(SELECT %d,layh_tmph_id,concat(layh_name,'_copy') FROM layout_head WHERE (layh_id = %d) )",$ComID,$OldID); 
		if ($DEBUG) echo "<br>[".__FUNCTION__."] sSQL -> '$sSQL'";
		$DBReturn=InsertSQL($sSQL);
		if ($DBReturn > 0) {
			return ($DBReturn);
		}
	}
	
	function layout_line_clone($OldID,$NewID) {
		global $DEBUG,$USER;
		$sSQL=sprintf("INSERT INTO layout_line (layl_layh_id,layl_plh_id,layl_active) ");
		$sSQL.=sprintf("(SELECT %d,layl_plh_id,layl_active FROM layout_head WHERE (layl_layh_id = %d) )",$NewID,$OldID); 
		if ($DEBUG) echo "<br>[".__FUNCTION__."] sSQL -> '$sSQL'";
		$DBReturn=AffectedSQL($sSQL);
		if ($DBReturn > 0) {
			return ($DBReturn);
		}
	}
	
	function layout_line_list($HeadID) {
		global $DEBUG,$USER;
		$sSQL=sprintf("SELECT * FROM  layout_line,template_head,template_line,type_info ");
		$sSQL.=sprintf("WHERE (tmph_id=tmpl_tmph_id) AND (type_id=tmpl_type_id) AND (layl_tmpl_id = tmpl_id) ");
		$sSQL.=sprintf("AND (layl_layh_id = %d) ",$HeadID);
		if ($DEBUG) echo "<br>[".__FUNCTION__."] sSQL -> '$sSQL'";
		$DBReturn=JSonSQL($sSQL);
//		echo $DBReturn;
		return($DBReturn);
	}
	
	function layout_used($HeadID) {
		global $DEBUG,$USER;
		$sSQL="SELECT count(*) as Counter FROM layout_head,schedule_head,schedule_line ";
		$sSQL.="WHERE (schh_id= schl_schh_id) AND (layh_id = schl_layh_id) ";
//		$sSQL.="AND (layh_active = 1) AND (layl_active =1) ";
		$sSQL.=sprintf("AND (layh_id = %d) ",$HeadID);
		if ($DEBUG) echo "[".__FUNCTION__."] sSQL -> '$sSQL'<br>";
		$DBReturn=ExecuteReader($sSQL);
		if ($Data=mysql_fetch_array($DBReturn)) {
//		echo $DBReturn;
			return($Data[0]['Counter']);
		} else {
			return 0;
		}
	}
	
	function layout_delete($LayoutID) {
		global $DEBUG,$USER;
		$Return="";
//	Delete Layout Line
		$sSQL=sprintf("DELETE FROM layout_line WHERE layl_layh_id=%d ",$LayoutID);
		if ($DEBUG) echo "[".__FUNCTION__."] sSQL -> '$sSQL'<br>";
		$DBCount=AffectedSQL($sSQL);
		if ($DEBUG)	$Return.=sprintf("\nDelete %d Item(s) in this Layout.",$DBCount);
//	Delete Schedule Head
		$sSQL=sprintf("DELETE FROM layout_head WHERE layh_id=%d ",$LayoutID);
		if ($DEBUG) echo "[".__FUNCTION__."] sSQL -> '$sSQL'<br>";
		$DBCount=AffectedSQL($sSQL);
		if ($DBCount ==1) {
			$Return.=sprintf("\n\nDelete this template successful.",$DBCount);
		} else {
			$Return.=sprintf("\n\nDelete this template failed.",$DBCount);
		}
		return $Return;
	}

	function layout_head_remove($HeadID) {
		global $DEBUG,$USER;
		$sSQL=sprintf("DELETE FROM layout_head WHERE layh_id=%d",$HeadID);
		if ($DEBUG) echo "<br>[".__FUNCTION__."] sSQL -> '$sSQL'";
		$DBReturn=AffectedSQL($sSQL);
		return ($DBReturn);
	}

	function layout_line_remove($LineID,$optALL=NULL) {
		global $DEBUG,$USER;
		$sSQL=sprintf("DELETE FROM layout_head");
		if (is_null($optALL)) {
			$sSQL.=sprintf(" WHERE layl_id=%d",$LineID);
		} else {
			$sSQL.=sprintf(" WHERE layl_layh_id=%d",$LineID);
		}
		if ($DEBUG) echo "<br>[".__FUNCTION__."] sSQL -> '$sSQL'";
		$DBReturn=AffectedSQL($sSQL);
		return ($DBReturn);
	}

?>