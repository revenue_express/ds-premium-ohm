<head>
<?php 
/*
 <script src="http://hayageek.github.io/jQuery-Upload-File/jquery.uploadfile.min.js"></script>
*/
	include("./includes/db_config.inc.php");
	include("./functions/library.func.php");
	include("./javascript.php");
?>
<script src="js/jquery.uploadfile.min.js"></script>
<link href="css/uploadfile.min.css" rel="stylesheet">
<script language="javascript">
	function library_convert(typeID,fileID,filename) {
		var html='';
		html='<div id="file_'+fileID+'">Convert File '+filename+'.... Please wait</div>';
		$('#convert').append(html);
		var URL='ajax/library_convert.php';
		var Params={'fileID':fileID};

		$.post(URL,Params,function(data) {
			if (data != null) {
				console.log('data :' +data);
				if (data = fileID) {
					$('#file_'+fileID).append(' Completed');
					sleep(1000);
					$('#file_'+fileID).remove();
				} else {
					$('#file_'+fileID).html('Convert Fail :: '+entry['Result']);
				}
			} else {
				$('#file_'+fileID).html('Cannot Convert');
			}
		});
	}
</script>
</head>
<body>
<?php if(isset($typeID)) { ?>
<?php
	switch($typeID) {
		case 1: // Images
			$sExtension='jpg,jpeg,bmp';
			$iMaxSize=5*1024*1024;
			$sPath='images';
			$sType='images';
			$sName="Image";
			break;
		case 11: // Cover
			$sExtension='png';
			$iMaxSize=10*1024*1024;
			$sPath='covers';
			$sType='covers';
			$sName='Frame';
			break;
		case 2: // Video
			$sExtension='webm,mp4,avi,mov,flv';
			$iMaxSize=1024*1024*1024;
			$sPath='videos';
			$sType='videos';
			$sName='Video';
			break;
		case 3: // Flash
			$sExtension='swf';
			$iMaxSize=10*1024*1024;
			$sPath='flashs';
			$sType='flashs';
			$sName='Flash';
			break;
		case 4: // Audio
			$sExtension='mp3,wav,mid';
			$iMaxSize=20*1024*1024;
			$sPath='audios';
			$sType='audios';
			$sName='Audio';
			break;
		case 9: // Documents
			$sExtension='doc,docx,rtf';
			$iMaxSize=20*1024*1024;
			$sPath='docs';
			$sType='docs';
			$sName='Document';
			break;
		case 10: // Acrobat
			$sExtension='pdf';
			$iMaxSize=20*1024*1024;
			$sPath='pdfs';
			$sType='pdfs';
			$sName='Acrobat';
			break;
		case 12: // PowerPoint
			$sExtension='ppt,pptx,pps';
			$iMaxSize=20*1024*1024;
			$sPath='ppts';
			$sType='ppts';
			$sName='Powerpoint';
			break;
	}
?>
<div class="demo">
	<div id="dvOption" title="Upload Limitation">
  <br>
  <div class="boxin">
  <table>
  <thead>
  <tr><th>Upload Type</th><th>Support File</th><th>Maxiumu Upload</th><th>Recommend</th></tr>
  </thead>
  <tbody>
  <tr><td>Image</td><td>jpeg,jpg,bmp</td><td class="right">5 MB</td><td></td></tr>
  <tr><td>Video</td><td>avi,mp4,flv,mov,mkv,webm</td><td class="right">1 GB</td><td>(webm) all file will convert to webm (drop quality)</td></tr>
  <tr><td>Background</td><td>png</td><td class="right">10 MB</td><td></td></tr>
  <tr><td>Audio</td><td>mp3,wave,midi</td><td class="right">10 MB</td><td></td></tr>
  <tr><td>Flash</td><td>swf</td><td class="right">10 MB</td><td></td></tr>
  <tr><td>Document</td><td>doc,docx,rtf</td><td class="right">20 MB</td><td>all file will convert to flash</td></tr>
  <tr><td>Presentation</td><td>ppt,pptx,pps</td><td class="right">20 MB</td><td>all file will convert to flash</td></tr>
  <tr><td>Acrobat</td><td>pdf</td><td class="right">20 MB</td><td>all file will convert to flash</td></tr>
  </tbody>
  </table>
	</div>
	</div>
</div>

<div class="boxin">
	<div class="header">
    <button onClick="history.go(-1);"><b> Back to <?php echo $sName;?> Library </b></button>
		<span style="float:right; margin-top:0px; margin-right:15px;">
    <input type="button" onClick="$('#dvOption').dialog('open');" value="Upload Limitation"></span>
  </div>
  <br>
	<div id="multipleupload">Browse</div>
	<br>
</div>
<script>
$(document).ready(function() {
	$('#dvOption').dialog({ autoOpen: false, modal: true, draggable: false });
	$('#dvOption').dialog("option","width",900);
	$('#dvOption').dialog("option","height",380);
	$('#dvOption').dialog("option","resizable",false);

	$("#multipleupload").uploadFile({
		url:"upload_library.php",
		multiple:true,
		fileName:"myfile",
		showStatusAfterSuccess:false,
		showAbort:true,
		showDone:true,
		showProgress:true,
		allowedTypes:'<?php echo $sExtension;?>',
		maxFileSize:<?php echo $iMaxSize;?>,
		formData: { 'path':'<?php echo $sPath;?>','extension':'<?php echo $sExtension;?>','stype':'<?php echo $sType;?>'},
		dragDropStr: "<span> <b> Drag & Drop Here</b> </span>",
		abortStr:" ยกเลิก ",
		cancelStr:"résilier",
		doneStr:" เสร็จสิ้นเรียบร้อย ",
		multiDragErrorStr: "Plusieurs Drag & Drop de fichiers ne sont pas autorisés.",
		extErrorStr:" ประเภทไฟล์ไม่ถูกต้องรับเฉพาะ <?php echo $sExtension;?>",
		sizeErrorStr:" ขนาดไฟล์ที่อัพโหลดมีขนาดใหญ่เกินที่กำหนด (ไม่ควรเกิน <?php echo ($iMaxSize/1024);?> KB) ",
		uploadErrorStr:" อัพโหลดไฟล์ไม่สำเร็จ",
		onSuccess:function(files,data,xhr) {
			console.log('onSuccess');
			console.log(files);
			console.log(data);
			console.log(xhr);
			var ret=JSON.parse(data);
			console.log(ret);
			library_convert(<?php echo $typeID;?>,ret[0]['InsertID'],files);			
		},
	});
});
</script>
<p></p>
<?php
} else {
		echo "<h1>Wrong Format Type</h1>";
}
/*
	echo sprintf("<br> session_save_path=> '%s'",session_save_path());
	echo sprintf("<br> session_id=> '%s'",session_id());
	echo "<br> Before <br>".$_RETURN;
	echo "<br> After";
	foreach ($_SESSION as $Key => $Value) { echo sprintf("<br>session [%s] => '%s'",$Key,addslashes(urldecode($Value))); }
	foreach ($_REQUEST as $Key => $Value) { echo sprintf("<br>request [%s] => '%s'",$Key,addslashes(urldecode($Value))); }
	foreach ($_COOKIE as $Key => $Value) { echo sprintf("<br>cookies [%s] => '%s'",$Key,addslashes(urldecode($Value))); }
*/
?>
<div id="convert">
</div>
</body>
