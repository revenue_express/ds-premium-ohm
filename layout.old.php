<? 
	include("./includes/db_config.inc.php");
	include("./includes/sys_config.inc.php");
	include("./functions/layout.func.php");
	$DEBUG=TRUE;
	$USER['usr_level']="Administrator";
?>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<script type="text/javascript" src="./js/jquery-1.7.2.min.js"></script>
<script type="text/javascript" src="./js/jquery-ui-1.8.21.custom.min.js"></script>
<script type="text/javascript" src="./js/checkValid.js"></script>
<script type="text/javascript" src="./js/signage.js"></script>
</script>
<!--link rel="stylesheet" href="./css/display.css"-->
<script type="text/javascript" src="./js/layout.js"></script>
<script type="text/javascript">
	
	function add_new() {
		ShowObject('dvHeadResult',false,'');
		ShowObject('dvHeadAddForm',true,'');
	}
	

</script>
</head>
<body>
<div>
<?
	foreach ($_REQUEST as $Key => $Val) { 
		if ($DEBUG) echo sprintf("<li>%s => %s",$Key,$Val); 
	}
	if (isset($btnLayoutAdd)) {
		include("./functions/template.func.php");
		include("./functions/playlist.func.php");
		/*Add Head Submit*/
		echo "Add Head<br>";
		$LayoutID=layout_head_add($ComID,$txtName,$TemplateID);
		echo "Add ID => $LayoutID<br>";
		$TempList=json_decode(template_line_list($ComID,$TemplateID),TRUE);
		$iCount=0;
		while ($iCount<count($TempList)) {
			if ($DEBUG) echo sprintf("<li>Loop[%d]</li>",$iCount); 
			$PlayListID=playlist_head_add($ComID,$TempList[$iCount]['tmpl_type_id']);
			if ($PlayListID > 0) {
				if ($DEBUG) echo sprintf("<li>Create New Playlist %d</li>",$PlayListID); 
				$PlayListName=sprintf("%s_copy",$TempList[$iCount]['tmpl_name']);
				$AffectedRow=playlist_head_edit($PlayListID,$PlayListName);
				if ($AffectedRow > 0) {
					if ($DEBUG) echo sprintf("<li>Edit Playlist Name ='%s'</li>",$PlayListName);
					$LayoutLineID=layout_line_add($LayoutID,$TempList[$iCount]['tmpl_id'],$PlayListID); 
				}
			}
			$iCount++;
		}
	} elseif (isset($btnRestore)) {
		$AlertMsg="Restore Failed.";
		echo "Restore<br>";
		if (layout_head_active($btnRestore)) {
			$AlertMsg="Restore Successful.";
		}
		echo sprintf("<script language='javascript'>alert('%s');</script>",$AlertMsg);
	} elseif (isset($btnDelete)) {
		echo "Delete<br>";
		$AlertMsg="Delete Failed.";
		if (layout_head_active($btnDelete,0)) {
			$AlertMsg="Delete Successful.";
		}
		echo sprintf("<script language='javascript'>alert('%s');</script>",$AlertMsg);
	} elseif (isset($btnClone)) {
		include("./functions/template.func.php");
		include("./functions/playlist.func.php");
		/*Add Head Submit*/
		echo "Clone Head<br>";
		$LayoutID=layout_head_clone($ComID,$btnClone);
		if ($LayoutID > 0) {
			$LayoutList=json_decode(layout_line_list($btnClone),true);
			$iCount=0;
			while($iCount<count($LayoutList)) {
				if ($DEBUG) echo "<h3>Playlist Clone</h3><br>";
				if ($DEBUG) echo "Playlist Head Clone<br>";
				$PlayListID=playlist_head_clone($ComID,$LayoutList[$iCount]['layl_plh_id']);
				if ($DEBUG) echo "Playlist Line Clone<br>";
				$LineCount=playlist_line_clone($PlayListID,$LayoutList[$iCount]['layl_plh_id']);
				if ($DEBUG) echo "Layout Line Clone<br>";
				$LineLayoutID=layout_line_add($LayoutID,$LayoutList[$iCount]['layl_tmpl_id'],$PlayListID);
				$iCount++;
			}
		}
	}
?>
</div>
<div><h3>Layout Management</h3></div>
<?	if (! isset($LayoutID)) { ?>
<script language="javascript">
	load_headlist();
</script>
<div id="dvHeadAddForm" style="display:none;">
<button onClick="window.open('./layout.php','_self');">Back</button>
<? 
	$Url=sprintf("%s?FName=%s&TID=%s&TName=%s&BName=%s",'popup_template.php','formLayoutAdd','TemplateID','txtTemplate','btnSubmit');
	$Option=sprintf("width=%d,height=%d,location=no,menubar=no,resizeable=no,scrollbars=no,status=no,toolbar=no",710,280);
	$onAddLayout=sprintf("window.open('%s','_template','%s');",$Url,$Option);
	
	$sSubmit="You can not change template Layout. Do you confirm to add this layout ?";
	$onSubmit=sprintf("if (confirm('%s')) {document.getElementById('%s').submit(); }",$sSubmit,'formLayoutAdd');
	$onReset="document.forms['formLayoutAdd'].reset();EnableObject('btnSubmit',false)";
	?>
<table id="tblFormAdd">
<form id="formLayoutAdd" name="formLayoutAdd" method="post">
<input type="hidden" id="btnLayoutAdd" name="btnLayoutAdd" value="LayoutAdd"/>
<input type="hidden" id="TemplateID" name="TemplateID" value=""/>
<tr>
	<th colspan="2">Layout Information</th>
</tr>
<tr>
  <td>Layout Name</td>
  <td><input type="text" id="txtName" name="txtName" size="40" maxlength="64"/></td>
</tr>
<tr>
  <td>Template Name</td>
  <td>
  <input type="text" id="txtTemplate" name="txtTemplate" size="40" maxlength="64" readonly/>
  <input type="button" id="btnSelTemplate" value="Select Template" onClick="<?=$onAddLayout;?>"/>
  </td>
</tr>
<tr>
	<td colspan="2" align="right">
  <input type="submit" id="btnSubmit" name="btnSubmit" value="Add Layout" onClick="<?=$onSubmit;?>" disabled/>
&nbsp;  <input type="button" id="btnReset" value="Reset" onClick="<?=$onReset;?>"/>
  </td>
 </tr>
</form>
</table>
</div>
<div id="dvHeadResult">
<table width="100%" border="1">
	<thead>
  <tr>
  	<th>Layout Name / Template</th>
    <th>Status</th>
    <th colspan="4" nowrap>
    	<input type="button" id="btnHeadRefresh" value="Refresh" onClick="load_headlist();"/>
			<input type="button" id="btnHeadAdd" value="Add New" onClick="add_new();"/>
    </th>
  </tr>
  </thead>
  <tbody id="showHead_Data">
  </tbody>
</table>
</div>
<? } else {?>
<div id="dvHeadInfo">
<? 
	$Items=json_decode(layout_line_list($LayoutID),true);	
	echo "<pre>";	print_r($Items); echo "</pre>";
?>
<table border="1">
  <thead>
  <tr>
    <th>Object Type</th>
    <th>Object Name </th>
    <th>Size(L,T) [WxH]</th>
  </tr>
  </thead>
	<tbody id='tbody_result'>
<?
	if (count($Items)) {
		for($iCount=0; $iCount<count($Items); $iCount++) { 
?>
	<tr>
    <td><?=sprintf("%s",$Items[$iCount]['type_name']);?></th>
    <td><?=sprintf("%s",$Items[$iCount]['tmpl_name']);?></td>
    <td>
		<?=sprintf("(%d,%d)",$Items[$iCount]['tmpl_left'],$Items[$iCount]['tmpl_left']);?>
		<?=sprintf("[%d,%d]",$Items[$iCount]['tmpl_width'],$Items[$iCount]['tmpl_height']);?>
    </td>
  </tr>
<? } 	} else { ?>
<? } ?>
</tbody>
</table>
</div>
<div id="dvLineResult">
<div id="dvLayoutShow">
<table>
	<tr><td align="right">Layout Preview<input type="button" id="LayoutShowToggle" value="Show" onClick="ButtonToggles('','dvLayout','LayoutShowToggle','Show','Hide');"/></td></tr>
  <tr>
  	<td>
    <div id="dvLayout" style="display:none;">
    <div id="dv001" style="display:block;">div001</div>
    <div id="dv002" style="display:block;">div002</div>
    <div id="dv003" style="display:block;">div003</div>
    <div id="dv004" style="display:block;">div004</div>
    <div id="dv005" style="display:block;">div005</div>
    <div id="dv006" style="display:block;">div006</div>
    </div>
    </td>
   </tr>
</table>
</div id="dvLayoutShow">
</div id="dvLineForm">

</div id="dvLineResult">
<? } ?>
</body>
</html>