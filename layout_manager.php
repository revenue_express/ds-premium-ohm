<?
	$ComID=1;
	$DEBUG=FALSE;
	include("./includes/db_config.inc.php");
	
	if ($TID) {
		$URL=sprintf("%s/ajax/lm_list.php?TID=%s",$LocalHost,$TID);
		$dataModel=sprintf("'%s'",file_get_contents($URL,true));
?>
<!doctype html>
<html lang="us">
<head>
	<meta charset="utf-8">
	<title>Layout Manager</title>
	
	<link href="css/fixcustom-theme/jquery-ui-1.9.1.custom.css" rel="stylesheet">
	<script src="js/jquery-1.8.2.js"></script>
	<script src="js/jquery-ui-1.9.1.custom.js"></script>
	<script src="js/ldz.min.js"></script>
    <script type="text/javascript">
        /*

            Use PHP echo This Script 
        */
				var dataModel=jQuery.parseJSON(<?=$dataModel;?>);
				console.log("dataModel"+dataModel);
				console.log(dataModel);
<?
/*
        var dataModel={
                    LayoutName :"dev",
                    tbody:{
                        width:800, 
                        height:600
                    },
                    coverimg:{
                        src:"tv.png"
                    },  
                    PanelObject:{
                        video1:{
                                id: 0,
                                ttype : "media",
                                title : "YYYY",
                                position:"fixed", 
                                left:0, 
                                top:0, 
                                width:150, 
                                height:150,  
                                zindex:0,
                                display:"block",  
                                opacity:1,
                                bgcolor:"#ffffff",
                                border:0.1
                            },
                        video2:{
                                id: 0,
                                ttype : "media",
                                title : "YYYY",
                                position:"fixed", 
                                left:0, 
                                top:0, 
                                width:150, 
                                height:150,  
                                zindex:0,
                                display:"block",  
                                opacity:1,
                                bgcolor:"#ffffff",
                                border:0.1
                            },
                        video3:{
                                id: 0,
                                ttype : "media",
                                title : "YYYY",
                                position:"fixed", 
                                left:0, 
                                top:0, 
                                width:150, 
                                height:150,  
                                zindex:0,
                                display:"block",  
                                opacity:1,
                                bgcolor:"#ffffff",
                                border:0.1
                            },
                    
                        image1:{
                                id: 0,
                                ttype : "header",
                                title : "YYYY",
                                position:"fixed", 
                                left:0, 
                                top:0, 
                                width:150, 
                                height:150,  
                                zindex:0,
                                display:"block",  
                                opacity:1,
                                bgcolor:"#ffffff",
                                border:0.1
                            }
                    }
                };
*/
?>
    </script>
    <script type="text/javascript">
        function saveLayout(){

            var saveData = JSON.stringify(dataModel);
            var sendData =encodeURIComponent(saveData);

            console.log(sendData);
//						alert(saveData);
            /*

                Call Ajax TO save 

            */
							var Params={"Params":saveData};
//							alert(saveData);
							var URL="./ajax/lm_update.php";
							$.post(URL,Params,function(data) {
								if (data != null) {
//								alert("Return \n"+data);
										window.close();
								} else {
									alert("Error Update layout manager");
								}
							});

        }
    </script>


	<style type="text/css">

        #body{
            position:fixed;
            top:0; right:0; bottom:0; left:0;
            margin:0;
            box-sizing:border-box;
            width:100%;
            height:100%;
            object-fit:contain;
        }
        
      
        #testpanel{
            position:fixed;
            height:150px;
            width:150px;
            top:100px;
            left:100px;
            z-index: 88;
            
         }
        
        #t-body{
            position:fixed;
        }
        #toolbar{
            z-index:99;
        }
      	#menu{
            position:fixed;
            height:300px;
            width:150px;
          
            
         }
         #toolbar{
            position:fixed;
            
            width:350px;
            font-size: 14px;
          
            
         }
         #EditPanel{
            position:fixed;
            height:600px;
            width:800px;
            top:0px;
            left:0px;
            border-style:dotted;
            border-width:1px;
         }
         #imgcover{
           
/*            height:100%;
            width:100%;
*/
            top:0px;
            left:0px;
            z-index: -9;
        }
         .opt1{
            
            height:250px;
            width:250px;
            
         }
         .inputBox{
            width:60px;
         }

    </style>

      

    
</head>
<body>
<div id = "toolbar">
    <ul>
        <li><a href="#tabs-main">Main</a></li>
        <li><a href="#tabs-media">Media</a></li>
<!--        <li><a href="#tabs-etc">Etc.</a></li> -->
<!--        <li><a href="#tabs-hide">hide</a></li> -->
    </ul>
    
    <div id="tabs-main">
      <div id ="main-menulist">      
      </div>
    </div>
    <div id="tabs-media" >
    	<div style="overflow:none;max-height:600px; width:315px;">
      	<div id ="main-medialist">
      </div>
    </div>
       
    </div>

 <!--
    <div id="tabs-etc">
    </div>
    <div id="tabs-hide">
    </div>
-->
</div>

<div id = "EditPanel">
	<img id="imgcover"  />
</div>


<script type="text/javascript">
	buildPanel(dataModel.PanelObject);
	buildMainPanel(dataModel);
	$(document).ready(function(){
		$("#toolbar").tabs().draggable();
		$("#main-menulist").accordion();
		$("#main-medialist").accordion({collapsible: true});
		$("#toolbar").css({'left':document.width-$('#toolbar').width()-10});
	});
</script>
</body>
</html>
<?
	}
?>