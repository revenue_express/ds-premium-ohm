<? 
	$DEBUG=FALSE;
	include("./includes/db_config.inc.php");
?>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<script type="text/javascript" src="js/jquery-1.8.0.min.js"></script>
<script type="text/javascript" src="js/jquery-ui-1.8.23.custom.min.js"></script>
<script type="text/javascript" src="./js/checkValid.js"></script>
<script type="text/javascript" src="./js/signage.js"></script>
<script type="text/javascript" src="./js/admin_user.js"></script>
<script>
	$(function() {
		$( "input:submit, input:button, input:reset, a, button", ".demo" ).button();
		$( "a", ".demo" ).click(function() { return false; });
	});
</script>
<link rel="stylesheet" href="css/StyleSheet.css">
<link rel="stylesheet" href="css/custom-theme/jquery-ui-1.8.23.custom.css">
<body onLoad="user_refresh();">
<div id="dvUserResult" class="w-80 boxin">   
    <div class="header">
    	<h3>
      <span>User Management</span>
      <span>&nbsp;
      <input type="button" class="btnTh" id="btnRefresh" value="Refresh" onClick="user_refresh();">
      &nbsp; &nbsp; 
      <input type="button" class="btnTh" id="btnAddNew" value="Add" onClick="user_show_add();" title="Add User">
			</span>
      </h3>
   	</div>
    <table cellspacing="0">
        <thead>
            <tr>
                <th class="w-30">Login</th>
                <th class="w-30">User Name</th>
                <th>Level</th>
                <th colspan="3" class="w-17">Operation</th>
            </tr>
        </thead>
        <tbody id="tblResult_Body">
            <tr>
                <td>Login</td>
                <td>User</td>
                <td>Level</td>
                <td><input type="button" class="btnRes" id="btnPass_XXX" value="Pass" onClick="user_show_changepass(1234);" title="Change password"></td>
                <td><input type="button" class="btnEdit" id="btnEdit_XXX" value="Edit" onClick="user_show_edit(1234);" title="Edit user information"></td>
                <td><input type="button" class="btnDel" id="btnDel_XXX" value="Delete" onClick="user_delete(1234);" title="Delete this user"></td>
            </tr>
        </tbody>
    </table>
</div>
<div id="dvUserAdd">
    <input type="button" id="btnBack" class="btnBack" value="" onClick="user_refresh();">
    <div class="w-50">
        <div class="well">
            <div class="left head">User Information</div>
        	<div class="dvtr top15">
            	<div class="dvtd_left w-35 top5">Name : </div>
            	<div class="dvtd_right">
                	<input id="txtName" class="txt" type="text" size="40" maxlength="127" onKeyUp="user_add_check();"></div>
            </div>
          	<div class="dvtr">
            	<div class="dvtd_left w-35 top5">Login : </div>
                <div class="dvtd_right">
                	<span>
                    	<input id="txtLogin" class="txt" type="text" size="16" maxlength="32" onKeyUp="user_add_check();">		
                    	<input style="margin-left:2px;" type="button" class="btnTh" onClick="user_check_login();" value="check">
                    </span>
                	<span id="spAddLogin" style="margin-left:10px;"></span>
       			</div>
           	</div>
          	<div class="dvtr">
            	<div class="dvtd_left w-35 top5">Password : </div>
           		<div class="dvtd_right">
                	<input id="txtPass1" class="txt" type="password" size="20" maxlength="127" onKeyUp="user_add_strength();user_add_check();"><span id='spAddStrength'></span></div>
           	</div>
          	<div class="dvtr">
            	<div class="dvtd_left w-35 top5">Confirm Password : </div>
            	<div class="dvtd_right">
                	<input id="txtPass2" class="txt" type="password" size="20" maxlength="127" onKeyUp="user_add_samepass();">
        			<span id='spAddSame'></span></div>
           	</div>
          	<div class="dvtr">
            	<div class="dvtd_left w-35 top5">User Level : </div>
            	<div class="dvtd_right">
                <select id="selLevel" name="selLevel" onChange="user_add_level();">
<? if (strtolower($_SESSION['usr_level'])=='administrator') {?>
                    <option value="1">Administrator</option>
<?	} ?>
<? if (strtolower($_SESSION['usr_level'])!='operator') {?>
                    <option value="2">Site Administrator</option>
<?	} ?>
                    <option value="3">Operator</option>
              	</select>
                    <input type="hidden" id="LevelID">
       			</div>
            </div>
            <div>&nbsp;</div>
            <div class="left head">Forget Password</div>
          	<div class="dvtr top15">
            	<div class="dvtd_left w-35 top5">Question : </div>
            	<div class="dvtd_right">
                    <select id="selQuest" name="selQuest" onChange="user_add_quest(); user_add_check();">
                            <option value="Question 1">Question 1</option>
                            <option value="Question 2">Question 2</option>
                            <option value="Question 3">Question 3</option>
                    </select>
       			<span>
                	<input id="txtQuest" class="txt" type="text" size="30" maxlength="127" onKeyUp="user_add_check();";>
                </span>
                </div>
           	</div>
          	<div class="dvtr">
            	<div class="dvtd_left w-35 top5">Answer : </div>
            	<div class="dvtd_right">
                	<input id="txtAnswer" class="txt" type="text" size="40" maxlength="127" onKeyUp="user_add_check();"></div>
			</div>
          	<div class="dvtr">
            	<div class="dvtd_left w-35 top5">Email : </div>
            	<div class="dvtd_right">
                	<input id="txtEmail" class="txt" type="text" size="40" maxlength="127" onKeyUp="user_add_check();"></div>
           	</div>
          	<div class="dvtr">
            	<div class="dvtd_left w-35 top5">
           		 	<input type="hidden" id="UserID">
            		<input type="button" class="btnSky" id="btnAddInfo" value="Save" onClick="user_add();">
                </div>
            	<div class="dvtd_right top5">
            		<input type="reset" class="btnSky" id="btnAddCancel" value="Reset" onClick="user_show_check();">
            	</div>
            </div>
    	</div>
    </div>
</div>
<div id="dvUserEdit">
	<input type="button" class="btnBack" id="btnBack" value="" onClick="user_refresh();">
    <div class="w-50">
        <div class="well">
            <div class="left head">User Information</div>
            <div class="dvtr top15">
                <div class="dvtd_left w-35 top5">Name : </div>
                <div class="dvtd_right">
                    <input id="txtEditName" class="txt" type="text" size="40" maxlength="127" onKeyUp="user_edit_check();"></div>
            </div>
            <div class="dvtr">
                <div class="dvtd_left w-35 top5">Login : </div>
                <div class="dvtd_right top5"><span id="txtEditLogin"></span></div>
            </div>
            <div class="dvtr">
                <div class="dvtd_left w-35 top5">User Level : </div>
                <div class="dvtd_right top5">
                <select id="selEditLevel" name="selLevel" onChange="user_edit_level();user_edit_check();">
<? if (strtolower($_SESSION['usr_level'])=='administrator') {?>
                    <option value="1">Administrator</option>
<?	} ?>
<? if (strtolower($_SESSION['usr_level'])!='operator') {?>
                    <option value="2">Site Administrator</option>
<?	} ?>
                    <option value="3">Operator</option>
                </select>
                </div>
            </div>
            <div>&nbsp;</div>
            <div class="left head">Forget Password</div>
            <div class="dvtr top15">
                <div class="dvtd_left w-35 top5">Question : </div>
                <div class="dvtd_right">
                    <select id="selEditQuest" name="selEditQuest" onChange="user_edit_quest();user_edit_check();">
                            <option value="Question 1">Question 1</option>
                            <option value="Question 2">Question 2</option>
                            <option value="Question 3">Question 3</option>
                    </select>
                    <span>
                        <input id="txtEditQuest" class="txt" type="text" size="30" maxlength="127" onKeyUp="user_edit_check();">
                    </span>
                </div>
            </div>
            <div class="dvtr">
                <div class="dvtd_left w-35 top5">Answer : </div>
                <div class="dvtd_right"><input id="txtEditAnswer" class="txt" type="text" size="40" maxlength="127" onKeyUp="user_edit_check();"></div>
            </div>
            <div class="dvtr">
                <div class="dvtd_left w-35 top5">Email : </div>
                <div class="dvtd_right"><input id="txtEditEmail" class="txt" type="text" size="40" maxlength="127" onKeyUp="user_edit_check();"></div>
            </div>
            <div class="dvtr">
                <div class="dvtd_left w-35 top5">
                    <input type="button" id="btnEditInfo" class="btnSky" value="Save" onClick="user_edit();" disabled>
                </div>
                <div class="dvtd_right top5">
                    <input type="reset" id="btnEditCancel" class="btnSky" value="Reset">
                </div>
            </div>
        </div>
    </div>
</div>
<div id="dvChangePassword">
<input type="button" class="btnBack" id="btnBack" value=" " onClick="user_refresh();">
    <div class="w-50">
      <div class="well">
            <table border="1" width="500">
                <tr><th colspan="2">Change Password</th></tr>
                <tr>	<td class="right" width="120">Name : </td><td><span id="spChangeName"></span></td></tr>
              <tr>	<td class="right">Login : </td>	<td><span id="spChangeLogin"></span></td></tr>
                <tr>
                <td class="right">Old Password : </td>
                    <td><input id="txtOldPass" type="password" size="20" maxlength="127" onKeyUp="user_change_check();"></td>
              </tr>
                <tr>
                <td class="right">New Password : </td>
                    <td><input id="txtNewPass1" type="password" size="20" maxlength="127" onKeyUp="user_change_strength();user_change_check();">
                <span id='spChangeStrength'>&nbsp; &nbsp; &nbsp;</span></td>
              </tr>
                <tr>
                <td class="right">Confirm New : </td>
                    <td><input id="txtNewPass2" type="password" size="20" maxlength="127" onKeyUp="user_change_samepass();user_change_check();">
                <span id='spChangeSame'>&nbsp; &nbsp; &nbsp;</span></td>
              </tr>
                <tr>
                <td colspan="2" class="right">
                    <input type="button" id="btnChangeInfo" value="Save" onClick="user_change_password();" disabled>
                    <input type="button" id="btnChangeCancel" value="Reset">
                </td>
              </tr>
            </table>
		</div>
    </div>
</div>
<div id="dvResetPassword">
<input type="button" class="btnBack" id="btnBack" value=" " onClick="user_refresh();">
    <div class="w-50">
      <div class="well">
			<div class="left head">Reset Password</div>
            <div class="dvtr top15">
            	<div class="dvtd_left w-45">Name : </div>
                <div class="dvtd_right"><span id="spResetName"></span></div>
           	</div>
          	<div class="dvtr">
            	<div class="dvtd_left w-45 top5">Login : </div>
                <div class="dvtd_right top5"><span id="spResetLogin"></span></div>
            </div>
            <div class="dvtr top5">
            	<div class="dvtd_left w-45 top5">New Password : </div>
                <div class="dvtd_right"><input id="txtResetPass1" class="txt" type="password" size="20" maxlength="127" onKeyUp="user_reset_strength();user_reset_check();">
            		<span id='spResetStrength'></span>
               	</div>
           	</div>
          	<div class="dvtr">
            	<div class="dvtd_left w-45 top5">Confirm New : </div>
                <div class="dvtd_right"><input id="txtResetPass2" class="txt" type="password" size="20" maxlength="127" onKeyUp="user_reset_samepass();user_reset_check();">
            	<span id='spResetSame'></span></div>
           	</div>
          	<div class="dvtr">
            	<div class="dvtd_left w-45 top5">
                	<input type="button" class="btnSky" id="btnResetInfo" value="Save" onClick="user_reset_password();" disabled>
                </div> 
                <div class="dvtd_right top5">
                	<input type="button" class="btnSky" id="btnResetCancel" value="Reset">
                </div>
			</div>
        </div>
	</div>
</div>
<?  // echo "session<pre>"; print_r($_SESSION); echo "</pre>";?>
</body>