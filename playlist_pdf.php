<? 
	include("./includes/db_config.inc.php");
	include("./includes/sys_config.inc.php");
	$DEBUG=FALSE;
?>
<html>
<head>
<?	include("./javascript.php");?>
<script type="text/javascript" src="./js/playlist_pdf.js"></script>
<script type="text/javascript" src="./js/playlist_head_option.js"></script>
<link rel="stylesheet" href="css/StyleSheet.css">
<script type="text/javascript">
	$('document').ready(function() {
		pdf_head_list();
		
	});
</script>
</head>
<body>
<div id="dvHeadResult" class="w-80 boxin">
	<div class="header">
  	<h3>Acrobat Playlist</h3>
  </div>
  <table cellspacing="0">
  <thead>
  	<tr>
      <th class="w-10">Code</th>
      <th>Playlist Name</th>
      <th class="w-5">Item(s)</th>
      <th class="w-15">
      <input class="btnTH" type="button" value="Refresh" onClick="pdf_head_list();"/>
      &nbsp; &nbsp;
      <input class="btnTH" type="button" value=" &nbsp;Add &nbsp;" onClick="playlist_head_add();"/>
      </th>
  	</tr>
  </thead>
  <tbody id="tblHead_Result">
    <tr>
      <td class="center">1</td>
      <td>2</td>
      <td>Item</td>
      <td class="center"><img class="preview" src="images/icons/edit.png" title="preview"></td>
      <td class="center"><img class="preview" src="images/icons/delete.png" title="preview"></td>
    </tr>
  </tbody>
  </table>
</div>
<div id="dvLineResult" class="boxin w-80">
	<div class="header">
  	<h3>Acrobat Playlist</h3><input class="btnHeader" type="button" value="Back" onClick="pdf_head_list();"/>
  </div>
  <div>
<?
$onChange="EnableObject('btnHeadSave',false);";
$sDisplay="Do you Comfirm to Change Playlist Name?";
$onChange.=sprintf("changeInputValue('txtHeadName','%s',document.getElementById('txtHeadName').value);",$sDisplay);
$onChange.="EnableObject('btnHeadSave',! checkEqual('txtHeadName','txtOldName'));";

$onReset="EnableObject('btnHeadSave',false);";
$onReset.="setObjValue('txtOldName','txtHeadName');";
?>
    &nbsp; &nbsp;  Playlist Name : 
    <input class="txt" size="40" type="text" id="txtHeadName" name="txtHeadName" readonly/>
    <input class="txt" size="40" type="hidden" id="txtOldName" name="txtOldName" />
    <input class="ui-button-text" type="button" id="btnHeadChange" value="Change" onClick="<?=$onChange;?>">
    <input class="ui-button-text" type="button" id="btnHeadSave" value="Save" onClick="playlist_head_save('HeadID','txtHeadName');" disabled>
    <input class="ui-button-text" type="button" id="btnHeadReset" value="Reset" onClick="<?=$onReset;?>">
    <input class="ui-button-text" type="button" value="Playlist Option" onClick="playlist_head_option('HeadID','TypeID');"/>
  </div>
  <div class="boxin w-100">
    <div class="header left">
      <h3><span>Acrobat Playlist Items</span>
      <span style="margin-left:20">
      <input class="ui-button-text" type="button" value="Refresh" onClick="pdf_refresh(document.getElementById('HeadID').value);"/>
      <input class="ui-button-text" type="button" value="Acrobat Library" onClick="show_file_exists();"/>
      <input class="ui-button-text" type="button" value="Acrobat Playlist Exists" onClick="show_playlist_exists();"/>
      <input id="HeadID" type="textbox" size="2"/> <input id="TypeID" type="textbox" value="10" size="2"/>
      </span></h3>
    </div>
    <table cellspacing="0" class="w-100">
    <tbody id="tblLine_Result">
      <tr>
        <td class="w-80 center"></td>
        <td class="w-20 center">
          <div class="dvtr top5 w-100">
             <div class="dvtd_left w-20 center"><img class="preview" src="images/icons/preview.png" title="preview"></div>
             <div class="dvtd_left w-20 center"><img class="preview" src="images/icons/down.png" title="down"></div>
             <div class="dvtd_left w-20 center"><img class="preview" src="images/icons/up.png" title="up"></div>
             <div class="dvtd_left w-20 center"><img class="preview" src="images/icons/edit.png" title="edit"></div>
             <div class="dvtd_left w-20 center"><img class="preview" src="images/icons/delete.png" title="delete"></div>
          </div>
        </td>
      </tr>
    </tbody>
    </table>
  </div>
</div>
<script type="text/javascript">
	$(function() {
		$('#dvSelectFile').dialog({ autoOpen: false });
		$('#dvSelectFile').dialog("option","width",900);
		$('#dvSelectFile').dialog("option","height",255);
		$('#dvSelectFile').dialog("option","resizable",false);
	});
</script>
<div id="dvSelectFile" title="Selection Exists File.">
<div id="dvSelectFileItem" style="min-height:50px; max-height:175px; height:200px; overflow:auto">
<table>
  <tbody id="tblSelectFile_Body">
  <tr><td><input type="radio"></td><td>111111111111111111111111111111111111111111111</td>
  </tbody>
</table>
</div>
You select <input type="text" size="40" id="txtExistFile"/>
<input type="text" id="SelectFileID" value='' size="4"/>
<input type="button" id='btnSelectFile' value="Selected" disabled onClick="playlist_exist_file();"/>
</div>
<script type="text/javascript">
	$(function() {
		$('#dvSelectPlaylist').dialog({ autoOpen: false });
		$('#dvSelectPlaylist').dialog("option","width",900);
		$('#dvSelectPlaylist').dialog("option","height",255);
		$('#dvSelectPlaylist').dialog("option","resizable",false);
	});
</script>
<div id="dvSelectPlaylist" title="Selection Exists Playlist.">
<div id="dvSelectPlaylistItem" style="min-height:50px; max-height:175px; height:200px; overflow:auto">
<table>
  <tbody id="tblSelectPlaylist_Body">
  <tr><td><input type="radio"></td><td>111111111111111111111111111111111111111111111</td>
  </tbody>
</table>
</div>
You select <input type="text" size="40" id="txtExistPlaylist"/>
<input type="text" id="SelectPlaylistID" value='' size="4"/>
<input type="button" id='btnSelectPlaylist' value="Selected" disabled onClick="playlist_exist_playlist();"/>
</div>
</div>
<script type="text/javascript">
	$(function() {
		$('#dvOption').dialog({ autoOpen: false });
		$('#dvOption').dialog("option","width",500);
		$('#dvOption').dialog("option","height",255);
		$('#dvOption').dialog("option","resizable",false);
	});
</script>
<div id="dvOption" title="Playlist Option Selection">
<table>
  <tbody id="tblOption_Body">
  </tbody>
</table>
</div>
</body>
</html>