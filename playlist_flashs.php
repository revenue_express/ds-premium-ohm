<? 
	$DEBUG=FALSE;
	$TypeID=3;
	include("./includes/db_config.inc.php");
	include("./includes/sys_config.inc.php");
?>
<html>
<head>
<?	include("./javascript.php");?>
<script type="text/javascript" src="./js/playlist_flash.js"></script>
<script type="text/javascript" src="./js/playlist_head_option.js"></script>
<link rel="stylesheet" href="css/custom-theme/jquery-ui-1.8.23.custom.css">
<link rel="stylesheet" href="css/StyleSheet.css">
<script type="text/javascript">
	$('document').ready(function() {
		flash_head_list();
		
	});
	
	$(function(){
		$('#btnSelectFile, #btnSelectPlaylist').button();
		$('#txtExistFile,#txtExistPlaylist,#txtPara').height(15);
	});	
</script>
</head>
<body>
<div id="dvHeadResult" class="w-80 boxin">
	<div class="header">
  	<h3>Flash Playlist
    	<input id="btnListRefresh" type="button" value="refresh" onClick="flash_head_list();">
     <input id="txtSearch" type="text" value="" size="10">
     <input id="btnSearch" type="button" value="search" onClick="table_search('tblHead_Result',2,'txtSearch');">
<!--     <input id="btnEdit" type="button" value="edit" onClick="lm_update(parseInt($('#txtSearch').val()));"> -->
    </h3>
  </div>
  <table cellspacing="0">
  <thead>
  	<tr>
      <th class="w-10">Code</th>
      <th>Playlist Name</th>
      <th class="w-5">Item(s)</th>
      <th class="w-15">
      <input class="btnTH" type="button" value="Refresh" onClick="flash_head_list();"/>
      &nbsp; &nbsp;
      <input class="btnTH" type="button" value=" &nbsp;Add &nbsp;" onClick="playlist_head_add();"/>
      </th>
  	</tr>
  </thead>
  <tbody id="tblHead_Result">
    <tr>
      <td class="center">1</td>
      <td>2</td>
      <td>Item</td>
      <td class="center"><img class="preview" src="images/icons/edit.png" title="preview"></td>
      <td class="center"><img class="preview" src="images/icons/delete.png" title="preview"></td>
    </tr>
  </tbody>
  </table>
</div>
<div id="dvLineResult" class="boxin w-80">
	<div class="header">
  	<h3>Flash Playlist</h3><input style="margin-left:120px" class="btnHeader" type="button" value="Back" onClick="flash_head_list();"/>
  </div>
  <div style="margin-top:5px; margin-bottom:5px;">
<?
$onChange="EnableObject('btnHeadSave',false);";
$sDisplay="Do you Comfirm to Change Playlist Name?";
$onChange.=sprintf("changeInputValue('txtHeadName','%s',document.getElementById('txtHeadName').value);",$sDisplay);
$onChange.="EnableObject('btnHeadSave',! checkEqual('txtHeadName','txtOldName'));";

$onReset="EnableObject('btnHeadSave',false);";
$onReset.="setObjValue('txtOldName','txtHeadName');";
?>
    &nbsp; &nbsp;  Playlist Name : 
    <input class="txt" size="40" type="text" id="txtHeadName" name="txtHeadName" readonly/>
    <input class="txt" size="40" type="hidden" id="txtOldName" name="txtOldName" />
    <input class="btnTh" style="margin-left:10px;" type="button" id="btnHeadChange" value="Change" onClick="<?=$onChange;?>">
    <input class="btnTh" style="margin-left:10px;" type="button" id="btnHeadSave" value="Save" onClick="playlist_head_save('HeadID','txtHeadName');" disabled>
    <input class="btnTh" style="margin-left:10px;" type="button" id="btnHeadReset" value="Reset" onClick="<?=$onReset;?>">
    <input class="btnTh" style="margin-left:10px;" type="button" value="Playlist Option" onClick="playlist_head_option('HeadID','TypeID');"/>
  </div>
  <div class="boxin w-100">
    <div class="header left">
      <h3><span>Flash Playlist Items</span>
      <span style="margin-left:15px">
      <input class="btnTh" type="button" value="Refresh" onClick="flash_refresh(document.getElementById('HeadID').value);"/>
      <input class="btnTh" style="margin-left:10px;" type="button" value="Flash Library" onClick="flash_file_exists();"/>
      <input class="btnTh" style="margin-left:10px;" type="button" value="Flash Playlist Exists" onClick="flash_playlist_exists();"/>
      <input id="HeadID" type="hidden" size="2"/> <input id="TypeID" type="hidden" value="<?=$TypeID;?>" size="2"/>
      </span></h3>
    </div>
    <table cellspacing="0" class="w-100">
    <thead>
    	<tr><th>Flash Name</th><th class="w-30">Parameter</th><th class="w-13">Action</th></tr>
    </thead>
    <tbody id="tblLine_Result">
      <tr>
        <td class="w-50 center"></td>
        <td class="w-30 center">
          <div class="dvtr top5 w-100">
             <div class="dvtd_left w-20 center"><img class="preview" src="images/icons/preview.png" title="preview"></div>
             <div class="dvtd_left w-20 center"><img class="preview" src="images/icons/down.png" title="down"></div>
             <div class="dvtd_left w-20 center"><img class="preview" src="images/icons/up.png" title="up"></div>
             <div class="dvtd_left w-20 center"><img class="preview" src="images/icons/edit.png" title="edit"></div>
             <div class="dvtd_left w-20 center"><img class="preview" src="images/icons/delete.png" title="delete"></div>
          </div>
        </td>
      </tr>
    </tbody>
    </table>
  </div>
</div>
<script type="text/javascript">
	$(function() {
		$('#dvSelectFile').dialog({ autoOpen: false });
		$('#dvSelectFile').dialog("option","width",700);
		$('#dvSelectFile').dialog("option","height",270);
		$('#dvSelectFile').dialog("option","resizable",false);
	});
</script>
<div id="dvSelectFile" title="Selection Exists flash.">
<div id="dvSelectFileItem" style="min-height:50px; max-height:185px; height:200px; overflow:auto">
<table cellspacing="0">
  <tbody id="tblSelectFile_Body">
  <tr><td><input type="radio"></td><td>111111111111111111111111111111111111111111111</td>
  </tbody>
</table>
</div>
<div class="pagination">
	<div class="dvtr">
        <div class="dvtd_left w-15" style="margin-top:15px;">You select : </div>
        <div class="demo dvtd_right">
        	<input type="text" class="txt" style="margin-top:13px;" size="20" id="txtExistFile"/></div>
		<div class="dvtd_left" style="margin-top:15px; margin-left:5px;">Parameter :</div>
        <div class="demo dvtd_right"><input type="text" style="margin-top:13px;" class="txt" id="txtPara" value='' size="20"/>
			<input type="hidden" id="SelectFileID" value='' size="4"/>
       	</div>
        <div class="demo dvtd_right" style="padding-top:13px;">
        	<input type="button" id='btnSelectFile' value="Selected" disabled onClick="flash_exist_file();"/>
      	</div>
    </div>
</div>
<script type="text/javascript">
	$(function() {
		$('#dvSelectPlaylist').dialog({ autoOpen: false });
		$('#dvSelectPlaylist').dialog("option","width",700);
		$('#dvSelectPlaylist').dialog("option","height",270);
		$('#dvSelectPlaylist').dialog("option","resizable",false);
	});
</script>
<div id="dvSelectPlaylist" title="Selection Exists Playlist.">
<div id="dvSelectPlaylistItem" style="min-height:50px; max-height:185px; height:200px; overflow:auto">
<table cellspacing="0">
  <tbody id="tblSelectPlaylist_Body">
  <tr><td><input type="radio"></td><td>111111111111111111111111111111111111111111111</td>
  </tbody>
</table>
</div>
<div class="pagination">
	<div class="dvtr">
        <div class="dvtd_left w-25" style="margin-top:15px;">You select : </div>
        <div class="demo dvtd_right">
        	<input type="text" class="txt" style="margin-top:13px;" size="40" id="txtExistPlaylist"/>
			<input type="hidden" id="SelectPlaylistID" value='' size="4"/>
        </div>
		<div class="demo dvtd_right" style="padding-top:13px;">
         	<input type="button" id='btnSelectPlaylist' value="Selected" disabled onClick="flash_exist_playlist();"/>
       	</div>
	</div>
</div>
</div>
</div>
<script type="text/javascript">
	$(function() {
		$('#dvOption').dialog({ autoOpen: false });
		$('#dvOption').dialog("option","width",500);
		$('#dvOption').dialog("option","height",180);
		$('#dvOption').dialog("option","resizable",false);
	});
</script>
<div id="dvOption" title="Playlist Option Selection">
<table>
  <tbody id="tblOption_Body">
  </tbody>
</table>
</div>
</body>
</html>