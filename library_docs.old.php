<? 
	$DEBUG=FALSE;
	$TypeID=9;
	include("./includes/db_config.inc.php");
	include("./includes/sys_config.inc.php");
	include("./functions/library.func.php");
?>
<html>
<head>
<script type="text/javascript" src="./js/checkValid.js"></script>
<script type="text/javascript" src="./js/SolmetraUploader.js"></script>
<script type="text/javascript">
  SolmetraUploader.setErrorHandler('test');
  function test (id, str) { alert('ERROR: ' + str); }
</script>
<script type="text/javascript" src="js/jquery-1.8.0.min.js"></script>
<script type="text/javascript" src="js/jquery-ui-1.8.23.custom.min.js"></script>
<script>
	$(function() {
		$( "input:submit, a, button", ".demo" ).button();
		$( "a", ".demo" ).click(function() { return false; });
		$( "#tblLibResult tr:odd").addClass("even");
	});
</script>

<link rel="stylesheet" href="css/custom-theme/jquery-ui-1.8.23.custom.css">
<link rel="stylesheet" href="css/StyleSheet.css">
</head>
<body>
<div id="dvLibUploadVideos" class="well w-50">
	<div class="left head">Media Library (Document)</div>
	<?
	if ($DEBUG) { echo "Request<pre>";		print_r($_REQUEST);		echo "</pre>"; }
	if ($DEBUG) { echo "File<pre>";		print_r($_FILES);		echo "</pre>"; }
	//		echo "<pre>"; print_r($_SERVER); echo "</pre>";
	include("./SolmetraUploader.php");
	
	$solmetraUploader = new SolmetraUploader('./','./upload.php','./config_docs.php');
	//		$solmetraUploader->setDemo(102400000);
	$solmetraUploader->gatherUploadedFiles();
	if (isset($_FILES) && sizeof($_FILES)) {
		if ($DEBUG) {
			echo '<h2>Uploaded files</h2>';
		echo '<pre>'; print_r($_FILES['firstfile']); echo '</pre>';
		}
		$uploadName=$_FILES['firstfile']['tmp_name'];
		$oldName=$_FILES['firstfile']['name'];
		$fSize=$_FILES['firstfile']['size'];
		if (($oldName != '') and ($txtNewName !='')  and ($uploadName != '') and ($fSize > 0) ) {
			$result=library_add('docs',$oldName,$txtNewName,$uploadName,$fSize);
		} else {
			$rResult="Data Not Completed.<BR>";
			$rResult.="oldName = '$oldName'<BR>";
			$rResult.="txtNewName = '$txtNewName'<BR>";
			$rResult.="uploadName = '$uploadName'<BR>";
			$rResult.="size = '".$fdata['size']."'<BR>";
			unlink($uploadName);
		}
	}
	
	if (isset($btnDelete)) {
		if ($DEBUG) echo "<br> Delete File => $btnDelete";
		$RowAffect=library_active($btnDelete);
		if ($RowAffect <> 1) {
			if ($DEBUG) echo ".....Delete Completed";
		} else {
			if ($DEBUG) echo ".....Delete Failed";
		}
	} elseif (isset($btnRestore)) {
		if ($DEBUG) echo "<br> Restore File => $btnRestore";
		$RowAffect=library_active($btnRestore,1);
		if ($RowAffect <> 1) {
			if ($DEBUG) echo ".....Restore Completed";
		} else {
			if ($DEBUG) echo ".....Restore Failed";
		}
	} else {
		if (isset($_SERVER['HTTP_REFERER']))	{
			if ($_SERVER['HTTP_URI'] = $PHP_SELF) {
				echo ("No File Upload");
			}
		}
	}	
?>
    <form  style="padding-left:20px; padding-top:15px;" method="post"/>
    <input type="hidden" id="TypeID" name="TypeID" size="2" value="9">
	<? if (strlen($rResult)>0) {?><div class="dvtd_left w-20"><?=$rResult;?></div><? } ?>
    <? echo $solmetraUploader->getInstance('firstfile', 500,30,true); ?>

    <div class="dvtr top10">
        <div class="dvtd_right">Display Name : </div>
        <div class="dvtd_right">
            <input type="text" class="txt" id="txtNewName" name="txtNewName" size="45" /> 
        </div>
        <div class="demo"><input type="submit" name="btnSubmit" value="Save"/></div>
    </div>
	</form>
</div>
<?	
	$FileList=json_decode(library_list($ComID,$TypeID,1),true);
	if ($DEBUG) {
//	echo "Count(Data) =".count($data);
//	$FileList=json_decode($data);
		echo "Count(FileList) =".count($FileList);
		echo "<pre>";
		print_r($FileList);
		echo "</pre>";
//	for ($iCount=0; $iCount < count($FileList); $iCount++) {
//		$arrData=array();
//		$arrData=$FileList[$iCount];
//		echo "<pre>";
//		print_r($arrData);
//		echo "</pre>";
//		echo $arrData['file_id'];
	}
?>

<div style="margin-top:15px;" class="boxin w-100">
    <div class="header">
        <h3>Media Library (Document)</h3>
    </div>
    <table id="tblLibResult" cellspacing="0">
        <thead>
          <tr>
            <th>Display Name</th>
            <th>Original Name</th>
            <th>Sys Name</th>
            <th>Size(KB)</th>
            <th>In Used</th>
        <?	if ($USER['usr_level'] <> 'Administrator') { ?>
            <th colspan="2">Action</th>
        <? } else { ?>
            <th colspan="3">Action</th>
        <?	} ?>
          </tr>
        </thead>
        <tbody>
    <?
        if (count($FileList) > 0) {
            for ($iCount=0;$iCount<count($FileList);$iCount++) {
                $sFileName=sprintf("./data/docs/%s",$FileList[$iCount]['file_sname']);
                $onViewClick=sprintf("window.open('%s','_view');",$sFileName);
                $formDel=sprintf("formDelete_%d",$FileList[$iCount]['file_id']);  
                $onDelete=sprintf("if (confirm('Do you confirm to delete')) {document.getElementById('%s').submit(); }",$formDel);
                $formRestore=sprintf("formRestore_%d",$FileList[$iCount]['file_id']);  
                $onRestore=sprintf("if (confirm('Do you confirm to restore data')) {document.getElementById('%s').submit(); }",$formRestore);
								$FileInUse=TRUE;
								if (file_used($FileList[$iCount]['file_id']) == 0) $FileInUse=FALSE;
                $clsTR=($FileInUse)?"data_used":"data_free";
								if ($FileInUse) {
										if (strtolower($USER['usr_level']) == 'operator') {
											$onDelete=sprintf("alert('%s');","File in used. Cannot Delete.");
										} else {
											$Mesg='!!!!! File in used !!!!!\n Do you confirm to delete ?';
											$Confirm='!!!!! File in used !!!!!\n Are you sure to delete ?';
											$onDelete=sprintf("if (confirm('%s')) { if (confirm('%s')) { document.getElementById('%s').submit();} }",$Mesg,$Confirm,$formDel);
										}
								}	
    ?>
      <tr>
        <td><?=$FileList[$iCount]['file_dname'];?></td>
        <td nowrap="nowrap"><?=$FileList[$iCount]['file_oname'];?></td>
        <td align="left" nowrap>
        <?=$FileList[$iCount]['file_sname'];?></td>
        <td align="right" class="filesize"><?=number_format($FileList[$iCount]['file_size']);?></td>
        <td align="center"><?=($FileInUse)?"Yes":"No";?></td>
        <td align="center" width="16"><img class="preview" src="./images/icons/preview.png" onClick="<?=$onViewClick;?> " title="preview"></td>
					<? if ($FileList[$iCount]['file_active']==0) { ?>
      <form action="<?=$PHP_SELF;?>" method="post" id="<?=$formRestore;?>" name="<?=$formRestore;?>">
          <input type="hidden" id="btnRestore" name="btnRestore" 
              value="<?=$FileList[$iCount]['file_id'];?>">
      <?	if (strtolower($USER['usr_level']) != 'operator') { ?>
              <td class="center">
              <img width="16" height="16" src="images/icons/restore.png" onClick="<?=$onRestore;?>" title="restore">
              </td>
          <? } ?>
      </form>
      <?	} ?>
      <form action="<?=$PHP_SELF;?>" method="post" id="<?=$formDel;?>" name="<?=$formDel;?>">
          <input type="hidden" id="btnDelete" name="btnDelete" value="<?=$FileList[$iCount]['file_id'];?>">
              <? if ($FileList[$iCount]['file_active']==1) { ?>
              <td class="center">
              <? if ( (strtolower($USER['usr_level']) == 'operator')  ){  ?>
              <?		if (! $FileInUse) { ?>
                  <img width="16" height="16" src="images/icons/delete.png" onClick="<?=$onDelete;?>" title="delete">
              <?		} ?>
              <? } else {?>
                  <img width="16" height="16" src="images/icons/delete.png" onClick="<?=$onDelete;?>" title="delete">
              <?	} ?>
              </td>
              <? } ?>
      </form>
      </tr>
    <? } }  else { ?>
      <tr>
    <?	$ColSize=($USER['usr_level'] == 'Administrator')?9:8; ?>
        <td align="center" colspan="<?=$ColSize;?>">NO FOUND</td>
     </tr>
    <?	} ?>
      </tbody>
    </table>
</div>
</body>
</html>