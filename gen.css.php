@font-face{
            font-family: "th_baijam";
            src: url(../font/TH_Baijam.ttf) format("truetype");
}
@font-face{
            font-family: "th_chakra_petch";
            src: url(../font/TH_Chakra_Petch.ttf) format("truetype");
}
@font-face{
            font-family: "th_charm_of_au";
            src: url(../font/TH_Charm_of_AU.ttf) format("truetype");
}
@font-face{
            font-family: "th_charmonman";
            src: url(../font/TH_Charmonman.ttf) format("truetype");
}
@font-face{
            font-family: "th_fahkwang";
            src: url(../font/TH_Fahkwang.ttf) format("truetype");
}
@font-face{
            font-family: "th_k2d_july8";
            src: url(../font/TH_K2D_July8.ttf) format("truetype");
}
@font-face{
            font-family: "th_kodchasal";
            src: url(../font/TH_Kodchasal.ttf) format("truetype");
}
@font-face{
            font-family: "th_koho";
            src: url(../font/TH_KoHo.ttf) format("truetype");
}
@font-face{
            font-family: "th_krub";
            src: url(../font/TH_Krub.ttf) format("truetype");
}
@font-face{
            font-family: "th_mali_grade6";
            src: url(../font/TH_Mali_Grade6.ttf) format("truetype");
}
@font-face{
            font-family: "th_niramit_as";
            src: url(../font/TH_Niramit_AS.ttf) format("truetype");
}
@font-face{
            font-family: "th_sarabun";
            src: url(../font/TH_Sarabun.ttf) format("truetype");
}


body{ 
	/* font-family: "custom_font"; */
	font-family: "THSarabun",Verdana,Arial,sans-serif;
	font-size:22px;
  background-color:#000000;
}

<? 
	$DEBUG=FALSE;
	$ComID=1;
	include("./includes/db_config.inc.php");
	include("./functions/playlist.func.php");
//	include("./includes/sys_config.inc.php");
	$DefHeader=json_decode(playlist_head_option_list(1),true);
	$DefFooter=json_decode(playlist_head_option_list(2),true);
	if ($DEBUG) {
		echo "<br>Header<pre>"; print_r($DefHeader); echo "</pre><br>";
		echo "<br>Footer<pre>"; print_r($DefFooter); echo "</pre><br>";
	}

	$CSSName=NULL;
	$CSSData=NULL;
	$LayoutItems=NULL;
	$clsTitle=array("position"=>"fixed","top"=>0,"left"=>0,"width"=>0,"height"=>0,"display"=>"none","z-index"=>100,"text-align"=>"center");
	$clsHeader=array("position"=>"fixed","top"=>0,"left"=>0,"width"=>0,"height"=>0,"display"=>"none","z-index"=>100,
		"text-align"=>"left","color"=>"#FFFFFF","font-size"=>22);
	$clsFooter=array("position"=>"fixed","top"=>0,"left"=>0,"width"=>0,"height"=>0,"display"=>"none","z-index"=>100,
		"text-align"=>"center","color"=>"#FFFFFF","font-size"=>22);
	$clsLayout=array("position"=>"fixed","top"=>0,"left"=>0,"width"=>0,"height"=>0,"display"=>"none","z-index"=>0);
	$clsFont=array(
		"font-family"=>"Helvetica, Arial, sans-serif",
		"font-size"=>"70px",
		"font-weight"=>"bold",
		"font-style"=>"italic",
		"font-transform"=>"uppercase",
		"color"=>"#f57df5"		);

	if (count($DefHeader) > 0) {
		for($i=0; $i<count($DefHeader); $i++) {
			if ((strtolower($DefHeader[$i]['pho_name'])=='fontstyle')) {
				if ((strtolower($DefHeader[$i]['pho_value'])=='bold')) {
					$clsHeader['font-weight']='bold';
				}
				if ((strtolower($DefHeader[$i]['pho_value'])=='italic')) {
					$clsHeader['font-style']='italic';
				}
			}
			if ((strtolower($DefHeader[$i]['pho_name'])=='fontsize')) {
				$clsHeader['font-size']=$DefHeader[$i]['pho_value'];
			}
			if ((strtolower($DefHeader[$i]['pho_name'])=='fontcolor')) {
				$clsHeader['font-color']=$DefHeader[$i]['pho_value'];
			}
		}
	}
	if (count($DefFooter) > 0) {
		for($i=0; $i<count($DefFooter); $i++) {
			if ((strtolower($DefFooter[$i]['pho_name'])=='fontstyle')) {
				if ((strtolower($DefFooter[$i]['pho_value'])=='bold')) {
					$clsFooter['font-weight']='bold';
				}
				if ((strtolower($DefFooter[$i]['pho_value'])=='italic')) {
					$clsFooter['font-style']='italic';
				}
			}
			if ((strtolower($DefFooter[$i]['pho_name'])=='fontsize')) {
				$clsFooter['font-size']=$DefFooter[$i]['pho_value'];
			}
			if ((strtolower($DefFooter[$i]['pho_name'])=='fontcolor')) {
				$clsFooter['font-color']=$DefFooter[$i]['pho_value'];
			}
		}
	}


	$CSSArray=array(
		"t-body"=>$clsLayout,
		"video1"=>$clsLayout,
		"video1title"=>$clsHeader,
		"video1filetitle"=>$clsFooter,
		"video2"=>$clsLayout,
		"video2title"=>$clsHeader,
		"video2filetitle"=>$clsFooter,
		"video3"=>$clsLayout,
		"video3title"=>$clsHeader,
		"video3filetitle"=>$clsFooter,
		"video4"=>$clsLayout,
		"video4title"=>$clsHeader,
		"video4filetitle"=>$clsFooter,
		"image1"=>$clsLayout,
		"image1title"=>$clsHeader,
		"image1filetitle"=>$clsFooter,
		"image2"=>$clsLayout,
		"image2title"=>$clsHeader,
		"image2filetitle"=>$clsFooter,
		"image3"=>$clsLayout,
		"image3title"=>$clsHeader,
		"image3filetitle"=>$clsFooter,
		"image4"=>$clsLayout,
		"image4title"=>$clsHeader,
		"image4filetitle"=>$clsFooter,
		"plugin1"=>$clsLayout,
		"plugin2"=>$clsLayout,
		"feed1panel"=>$clsLayout,
		"feed2panel"=>$clsLayout,
//		"backpanel"=>$clsLayout,
//		"coverpanel"=>$clsLayout,
		"imgcover"=>$clsLayout,
		"txt1"=>$clsFont,
		"txt2"=>$clsFont);


	if (count($DefHeader) > 0) {
		for($i=0; $i<count($DefHeader); $i++) {
			if ((strtolower($DefHeader[$i]['pho_name'])=='fontstyle')) {
				if ((strtolower($DefHeader[$i]['pho_value'])=='bold')) {
					$clsHeader['font-weight']='bold';
				}
				if ((strtolower($DefHeader[$i]['pho_value'])=='italic')) {
					$clsHeader['font-style']='italic';
				}
			}
			if ((strtolower($DefHeader[$i]['pho_name'])=='fontsize')) {
				$clsHeader['font-size']=$DefHeader[$i]['pho_value'];
			}
		}
	}


	if (isset($TID)) {
		include("./functions/template.func.php");
		$CSSData="";
//	Get Layout Information 		
		$Header=json_decode(template_head_info($TID),true);
		if ($DEBUG) { echo sprintf("<br><br>Header <pre>",$iRun); print_r($Header); echo "</pre>";  }
		if (count($Header)>0) {
			$CSSArray['t-body']['height']=$Header[0]['disp_height'];
			$CSSArray['t-body']['width']=$Header[0]['disp_width'];
				
//			$CSSData.=sprintf("#t-body{\nheight=%dpx; width:%dpx}\n\n",$Header[0]['disp_height'],$Header[0]['disp_width']); 
//	Get Layout Details 		
			$Line=json_decode(template_line_list($ComID,$TID,$USER['usr_id']),true);
// Initial Array to keep Object Count
			$Item=array();
			for ($iRun=0; $iRun<count($Line); $iRun++) {
//				if (isset($Item[$Line[$iRun]['type_name']])) {
				if (isset($Item[$Line[$iRun]['type_panel']])) {
//	Last Found This Object
//					$Item[$Line[$iRun]['type_name']]++;
//					$iCount=$Item[$Line[$iRun]['type_name']];
					$Item[$Line[$iRun]['type_panel']]++;
					$iCount=$Item[$Line[$iRun]['type_panel']];
				} else {
//	Not Found This Object
					$iCount=1;
//					$Item[$Line[$iRun]['type_name']]=1;							
					$Item[$Line[$iRun]['type_panel']]=1;							
				}
//				$panelName=sprintf("%s%d",$Line[$iRun]['type_panel'],$Item[$Line[$iRun]['type_name']]);
//				$panelTitle=sprintf("%s%dtitle",$Line[$iRun]['type_panel'],$Item[$Line[$iRun]['type_name']]);
//				$panelFile=sprintf("%s%dfiletitle",$Line[$iRun]['type_panel'],$Item[$Line[$iRun]['type_name']]);

				$panelName=sprintf("%s%d",$Line[$iRun]['type_panel'],$Item[$Line[$iRun]['type_panel']]);

				$panelTitle=sprintf("%s%dtitle",$Line[$iRun]['type_panel'],$Item[$Line[$iRun]['type_panel']]);
				$panelFile=sprintf("%s%dfiletitle",$Line[$iRun]['type_panel'],$Item[$Line[$iRun]['type_panel']]);

				if ($Line[$iRun]['type_id']==5) $panelName.="panel";

				$pHeight=$Line[$iRun]['tmpl_height'];
				$pWidth=$Line[$iRun]['tmpl_width'];
				$pTop=$Line[$iRun]['tmpl_top'];
				$pLeft=$Line[$iRun]['tmpl_left'];
				$zOrder=$Line[$iRun]['tmpl_order'];
				if ($DEBUG) { echo sprintf("<br><br>Item No.%d => %s",$iRun,$panelName); echo "<pre>"; print_r($Line[$iRun]); echo "</pre>";  }
				$CSSArray[$panelName]['left']=$pLeft;
				$CSSArray[$panelName]['top']=$pTop;
				$CSSArray[$panelName]['width']=$pWidth;
				$CSSArray[$panelName]['height']=$pHeight;
				$CSSArray[$panelName]['z-index']=$zOrder;
				$CSSArray[$panelName]['display']='';
//
/*
				if ( ($Line[$iRun]['type_id']==1) || ($Line[$iRun]['type_id']==2) ) { 
					$HeadIdx=-1;
					$FooterIdx=-1;
					if (($Line[$iRun]['type_id']==1) || ($Line[$iRun]['type_id']==2) ) {
						$OptionLine=json_decode(template_option_list($Line[$iRun]['tmpl_id']),true);
						for ($iOption=0; $iOption<count($OptionLine); $iOption++) {
							if ($OptionLine[$iOption]['tplo_name'] == 'header') $HeadIdx=$iOption;
							if ($OptionLine[$iOption]['tplo_name'] == 'footer') $FooterIdx=$iOption;
						}
					}

					$CSSArray[$panelTitle]['left']=$pLeft;
					$CSSArray[$panelTitle]['top']=$pTop-$TitleName_Height;
					$CSSArray[$panelTitle]['width']=$pWidth;
					$CSSArray[$panelTitle]['height']=$TitleName_Height;
					$CSSArray[$panelTitle]['display']='none';

					$CSSArray[$panelFile]['left']=$pLeft;
					$CSSArray[$panelFile]['top']=$pTop+$pHeight-$TitleFile_Height;
					$CSSArray[$panelFile]['width']=$pWidth;
					$CSSArray[$panelFile]['height']=$TitleFile_Height;
					$CSSArray[$panelFile]['display']='none';

					if ($HeadIdx > -1) {
						$CSSArray[$panelTitle]['left']=$OptionLine[$HeadIdx]['tplo_left'];
						$CSSArray[$panelTitle]['top']=$OptionLine[$HeadIdx]['tplo_top'];
						$CSSArray[$panelTitle]['width']=$OptionLine[$HeadIdx]['tplo_width'];
						$CSSArray[$panelTitle]['height']=$OptionLine[$HeadIdx]['tplo_height'];
						$CSSArray[$panelFile]['display']='';
					}

					if ($FooterIdx > -1) {
						$CSSArray[$panelFile]['left']=$OptionLine[$FooterIdx]['tplo_left'];
						$CSSArray[$panelFile]['top']=$OptionLine[$FooterIdx]['tplo_top'];
						$CSSArray[$panelFile]['width']=$OptionLine[$FooterIdx]['tplo_width'];
						$CSSArray[$panelFile]['height']=$OptionLine[$FooterIdx]['tplo_height'];
						$CSSArray[$panelFile]['display']='';
					}
				}
				
*/
				$OptionLine=json_decode(template_option_list($Line[$iRun]['tmpl_id']),true);
				$iOptionMax=count($OptionLine);
				$HeadIdx=-1;
				$FooterIdx=-1;
				if ($iOptionMax > 0) {
					for ($iOption=0; $iOption<$iOptionMax; $iOption++) {
						if ($OptionLine[$iOption]['tplo_name'] == 'header') $HeadIdx=$iOption;
						if ($OptionLine[$iOption]['tplo_name'] == 'footer') $FooterIdx=$iOption;
					}
					if ($HeadIdx > -1) {
						$CSSArray[$panelTitle]['left']=$OptionLine[$HeadIdx]['tplo_left'];
						$CSSArray[$panelTitle]['top']=$OptionLine[$HeadIdx]['tplo_top'];
						$CSSArray[$panelTitle]['width']=$OptionLine[$HeadIdx]['tplo_width'];
						$CSSArray[$panelTitle]['height']=$OptionLine[$HeadIdx]['tplo_height'];
						$CSSArray[$panelTitle]['display']='';
					}

					if ($FooterIdx > -1) {
						$CSSArray[$panelFile]['left']=$OptionLine[$FooterIdx]['tplo_left'];
						$CSSArray[$panelFile]['top']=$OptionLine[$FooterIdx]['tplo_top'];
						$CSSArray[$panelFile]['width']=$OptionLine[$FooterIdx]['tplo_width'];
						$CSSArray[$panelFile]['height']=$OptionLine[$FooterIdx]['tplo_height'];
						$CSSArray[$panelFile]['display']='';
					}
				}
//				$CSSData.=sprintf("#%s {\nposition:fixed; ",$panelName);
//				$CSSData.=sprintf("left=%dpx; top=%dpx; height:%dpx; width:%dpx;}\n\n",$pLeft,$pTop,$pHeight,$pWidth);
	
			}
		}
	}
	if ($DEBUG) { echo "CSSData =>\n<pre>"; print_r($CSSArray); echo "</pre><br>"; }
	$CSSData.=sprintf("\n#t-body { height:%dpx; width:%dpx; }",$CSSArray['t-body']['height'],$CSSArray['t-body']['width']);
/* Image,Video,Feed Panel */
	for ($iPanel=1; $iPanel<=4; $iPanel++) {
		if ($iPanel==1) $TName="image";
		if ($iPanel==2) $TName="video";
		if ($iPanel==3) $TName="feed";
		if ($iPanel==4) $TName="plugin";
		for ($iRun=1; $iRun<=4; $iRun++) {
			$PName=sprintf("%s%d",$TName,$iRun);
			$PTitle=sprintf("%s%dtitle",$TName,$iRun);
			$PFile=sprintf("%s%dfiletitle",$TName,$iRun);
			if ($iPanel==3) $PName.="panel";
			if (isset($CSSArray[$PName])) {
				$CSSData.=sprintf("\n#%s { position:fixed; ",$PName);
				$CSSData.=sprintf("left:%dpx; ",$CSSArray[$PName]['left']);
				$CSSData.=sprintf("top:%dpx; ",$CSSArray[$PName]['top']);
				$CSSData.=sprintf("width:%dpx; ",$CSSArray[$PName]['width']);
				$CSSData.=sprintf("height:%dpx; ",$CSSArray[$PName]['height']);
				$CSSData.=sprintf("z-index: %d; ",$CSSArray[$PName]['z-index']);
				if (! is_null($CSSArray[$PName]['font-style'])) { $CSSData.=sprintf("font-style: %s;",$CSSArray[$PName]['font-style']); } 
				if (! is_null($CSSArray[$PName]['font-weight'])) { $CSSData.=sprintf("font-weight: %s;",$CSSArray[$PName]['font-weight']); } 
				if (! is_null($CSSArray[$PName]['font-color'])) { $CSSData.=sprintf("color: %s;",$CSSArray[$PName]['font-color']); } 
				if ($CSSArray[$PName]['display']=='none') { $CSSData.=sprintf("display: none; ");} 
				$CSSData.=" } ";
			}
			if (isset($CSSArray[$PTitle])) {
				$CSSData.=sprintf("\n#%s { position:fixed; ",$PTitle);
				$CSSData.=sprintf("left:%dpx; ",$CSSArray[$PTitle]['left']);
				$CSSData.=sprintf("top:%dpx; ",$CSSArray[$PTitle]['top']);
				$CSSData.=sprintf("width:%dpx; ",$CSSArray[$PTitle]['width']);
				$CSSData.=sprintf("height:%dpx; ",$CSSArray[$PTitle]['height']);
				$CSSData.=sprintf("z-index: %d; ",$CSSArray[$PTitle]['z-index']);
				$CSSData.=sprintf("text-align: %s; ",$CSSArray[$PTitle]['text-align']);
//				$CSSData.=sprintf("color: %s; ",$CSSArray[$PTitle]['color']);
				$CSSData.=sprintf("font-size:%dpx; ",$CSSArray[$PTitle]['font-size']);
				if (! is_null($CSSArray[$PTitle]['font-style'])) { $CSSData.=sprintf("font-style: %s; ",$CSSArray[$PTitle]['font-style']); } 
				if (! is_null($CSSArray[$PTitle]['font-weight'])) { $CSSData.=sprintf("font-weight: %s; ",$CSSArray[$PTitle]['font-weight']); } 
				if (! is_null($CSSArray[$PTitle]['font-color'])) { $CSSData.=sprintf("color: %s; ",strtoupper($CSSArray[$PTitle]['font-color'])); } 
				if ($CSSArray[$PTitle]['display']=='none') { $CSSData.=sprintf("display: none; ");} 
				$CSSData.=" } ";
			}
			if (isset($CSSArray[$PFile])) {
				$CSSData.=sprintf("\n#%s { position:fixed; ",$PFile);
				$CSSData.=sprintf("left:%dpx; ",$CSSArray[$PFile]['left']);
				$CSSData.=sprintf("top:%dpx; ",$CSSArray[$PFile]['top']);
				$CSSData.=sprintf("width:%dpx; ",$CSSArray[$PFile]['width']);
				$CSSData.=sprintf("height:%dpx; ",$CSSArray[$PFile]['height']);
				$CSSData.=sprintf("z-index: %d; ",$CSSArray[$PFile]['z-index']);
				$CSSData.=sprintf("text-align: %s; ",$CSSArray[$PFile]['text-align']);
				$CSSData.=sprintf("color: %s; ",$CSSArray[$PFile]['color']);
				$CSSData.=sprintf("font-size:%dpx; ",$CSSArray[$PFile]['font-size']);
				if ($CSSArray[$PFile]['display']=='none') { $CSSData.=sprintf("display: none; ");} 
				$CSSData.=" } ";
			}
		}
	}
	
/* Back Panel and Cover Panel*/
/*
	for ($iRun=1; $iRun<=2; $iRun++) {
		if ($iRun==1) $PName="cover";
		if ($iRun==2) $PName="back";
		$PName.=sprintf("%dpanel",$iRun);
		if (isset($CSSArray[$PName])) {
			$CSSData.=sprintf("\n#%s { position=fixed; ",$PName);
			$CSSData.=sprintf("left:%dpx; ",$CSSArray[$PName]['left']);
			$CSSData.=sprintf("top:%dpx; ",$CSSArray[$PName]['top']);
			$CSSData.=sprintf("width=%dpx; ",$CSSArray[$PName]['width']);
			$CSSData.=sprintf("height=%dpx; ",$CSSArray[$PName]['height']);
			$CSSData.=sprintf("z-index: %d; ",$CSSArray[$PName]['z-index']);
			if ($CSSArray[$PName]['display']=='none') { $CSSData.=sprintf("display: none; ");} 
			$CSSData.=" } ";
		}
	}


*/
/*Cover Panel */
/*
	$PName="coverpanel";
	if (isset($CSSArray[$PName])) {
		$URL=sprintf('%s/ajax/layout_cover_info.php?HeadID=%d',$LocalHost,$TID);
		$CoverReturn=json_decode(file_get_contents($URL),true);
		if ($DEBUG) { echo "Cover $URL<pre>"; print_r($CoverReturn); echo "</pre>"; }
		if (count($CoverReturn) > 0) {
			$CSSArray[$PName]['display']='block';
			$CSSData.=sprintf("\n#%s { position:fixed; ",$PName);
			$CSSData.=sprintf("left:%dpx; ",$CSSArray[$PName]['left']);
			$CSSData.=sprintf("top:%dpx; ",$CSSArray[$PName]['top']);
			$CSSData.=sprintf("width:%dpx; ",$Header[0]['disp_width']);
			$CSSData.=sprintf("height:%dpx; ",$Header[0]['disp_height']);
			$CSSData.=sprintf("z-index: %d; ",99);
			if ($CSSArray[$PName]['display']=='none') { $CSSData.=sprintf("display: none; ");} 
			$CSSData.=" } ";
		}
	}
*/
	$PName="imgcover";
	if (isset($CSSArray[$PName])) {
		$URL=sprintf('%s/ajax/layout_cover_info.php?HeadID=%d',$LocalHost,$TID);
		$CoverReturn=json_decode(file_get_contents($URL),true);
		if ($DEBUG) { echo "Cover $URL<pre>"; print_r($CoverReturn); echo "</pre>"; }
		if (count($CoverReturn) > 0) {
			$CSSArray[$PName]['display']='block';
			$CSSData.=sprintf("\n#%s { position:fixed; ",$PName);
			$CSSData.=sprintf("left:%dpx; ",$CSSArray[$PName]['left']);
			$CSSData.=sprintf("top:%dpx; ",$CSSArray[$PName]['top']);
			$CSSData.=sprintf("width:%dpx; ",$Header[0]['disp_width']);
			$CSSData.=sprintf("height:%dpx; ",$Header[0]['disp_height']);
			$CSSData.=sprintf("z-index: %d; ",99);
			if ($CSSArray[$PName]['display']=='none') { $CSSData.=sprintf("display: none; ");} 
			$CSSData.=" } ";
		}
	}

/* Text Panel */
	for ($iRun=1; $iRun<=2; $iRun++) {
		$PName=sprintf("txt%d",$iRun);
		if (isset($CSSArray[$PName])) {
			$CSSData.=sprintf("\n#%s { ",$PName);
			$CSSData.=sprintf("font-family: %s; ",$CSSArray[$PName]['font-family']);
			$CSSData.=sprintf("font-size: %s; ",$CSSArray[$PName]['font-size']);
			$CSSData.=sprintf("font-style: %s; ",$CSSArray[$PName]['font-style']);
			$CSSData.=sprintf("font-transform: %s; ",$CSSArray[$PName]['font-transform']);
			$CSSData.=sprintf("color: %s; ",$CSSArray[$PName]['color']);
			$CSSData.=" } ";
		}
	}
	if ($DEBUG) {
		echo nl2br($CSSData);
	} else {
		echo $CSSData;
	}
?>
	
