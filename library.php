<? 
	include("./includes/db_config.inc.php");
	include("./functions/library.func.php");
	$DEBUG=TRUE;
?>
<html>
<head>
<script type="text/javascript" src="js/jquery-1.7.2.min.js"></script>
<script type="text/javascript" src="js/jquery-ui-1.8.21.custom.min.js"></script>
	<script type="text/javascript" src="./js/SolmetraUploader.js"></script>
  <script type="text/javascript">
  SolmetraUploader.setErrorHandler('test');
  function test (id, str) { alert('ERROR: ' + str); }
  </script>
</head>
<body>
<div id="dvLibUploadImages">
<?
		if ($DEBUG) { echo "Request<pre>";		print_r($_REQUEST);		echo "</pre>"; }
		if ($DEBUG) { echo "File<pre>";		print_r($_FILES);		echo "</pre>"; }
//		echo "<pre>"; print_r($_SERVER); echo "</pre>";
		include("./SolmetraUploader.php");

		$solmetraUploader = new SolmetraUploader('./','./upload.php','./config_images.php');
		$solmetraUploader->gatherUploadedFiles();
		if (isset($_FILES) && sizeof($_FILES)) {
			if ($DEBUG) {
 	 			echo '<h2>Uploaded files</h2>';
  			echo '<pre>'; print_r($_FILES['firstfile']); echo '</pre>';
			}
			$uploadName=$_FILES['firstfile']['tmp_name'];
			$oldName=$_FILES['firstfile']['name'];
			$fSize=$_FILES['firstfile']['size'];
			if (($oldName != '') and ($txtNewName !='')  and ($uploadName != '') and ($fSize > 0) ) {
				$result=library_add('images',$oldName,$txtNewName,$uploadName,$fSize);
			} else {
				$rResult="Data Not Completed.<BR>";
				$rResult.="oldName = '$oldName'<BR>";
				$rResult.="txtNewName = '$txtNewName'<BR>";
				$rResult.="uploadName = '$uploadName'<BR>";
				$rResult.="size = '".$fdata['size']."'<BR>";
				unlink($uploadName);
			}
		} else {
			if (isset($_SERVER['HTTP_REFERER']))		echo ("No File Upload");
		}
?>
    <table class="allPage">
    <form  action="library.php" method="post"/>
    <tr><td>
      <fieldset width="300">
<? if (strlen($rResult)>0) {?><div align="center"><?=$rResult;?><br /></div><? } ?>
<? echo $solmetraUploader->getInstance('firstfile', 500,40,true); ?>
Display Name : <input type="text" id="txtNewName" name="txtNewName" size="32" /> <input type="submit" class="btn_Add" name="btnSubmit" value="Upload"/>
      </fieldset>
      </td></tr>
    </form>
      <tr height="10"><td></td></tr>
    </table>

</div>
<div id="dvLibFile">
	<div id="dvLibFileHead"><button id="btnLibFileRefresh" onClick="AddTable('tblLibResult');">Add</button></div>
	<div id="dvLibFileLine"></div>
</div>
<br>
<?	
	$FileList=json_decode(library_list(1,1,1),true);
	if ($DEBUG) {
	echo "Count(FileList) =".count($FileList);
	echo "<pre>";
	print_r($FileList);
	echo "</pre>";
	}
//	for ($iCount=0; $iCount < count($FileList); $iCount++) {
//		$arrData=array();
//		$arrData=$FileList[$iCount];
//		echo "<pre>";
//		print_r($arrData);
//		echo "</pre>";
//		echo $arrData['file_id'];
//	}
?>
<table id="tblLibResult" width="98%" border="1" align="center" cellpadding="3" cellspacing="0">
	<thead>
  <tr>
    <th width="25" bgcolor="#33CCFF" scope="col">ID</th>
    <th bgcolor="#33CCFF" scope="col">Display Name</th>
    <th width="100" bgcolor="#33CCFF" scope="col">Old Name</th>
    <th bgcolor="#33CCFF" scope="col">Sys Name</th>
    <th width="100" bgcolor="#33CCFF" scope="col">Size(KB)</th>
    <th width="15" nowrap="nowrap" bgcolor="#33CCFF" scope="col">In Used</th>
  </tr>
	</thead>
	<tbody>
<?
	if (count($FileList) > 0) {
		for ($iCount=0;$iCount<count($FileList);$iCount++) {
			$sFileName=sprintf("./data/images/%s",$FileList[$iCount]['file_sname']);
			$onClick=sprintf("window.open('%s','_view');",$sFileName);
			$LinkDelete=sprintf("'./ajax/library.del.php?id=%d'",$FileList[$iCount]['file_id']);
			$onDelete=sprintf("if (confirm('Do you confirm to delete')) { library_delete(%s); doRemoveItem(this); }",$LinkDelete);  
?>
  <tr file_id="<?=$FileList[$iCount]['file_id'];?>">
    <td align="center" bgcolor="#FFCCFF"><?=$FileList[$iCount]['file_id'];?></td>
    <td bgcolor="#FFCCFF"><?=$FileList[$iCount]['file_dname'];?></td>
    <td align="right" nowrap="nowrap" bgcolor="#FFCCFF"><?=$FileList[$iCount]['file_oname'];?></td>
    <td align="left" bgcolor="#FFCCFF"><img class="preview" src="./images/icons/preview.png" onClick="<?=$onClick;?>"><?=$FileList[$iCount]['file_sname'];?></td>
    <td align="right" bgcolor="#FFCCFF"><?=$FileList[$iCount]['file_size'];?></td>
    <td align="center" bgcolor="#FFCCFF"><img class="delete" src="./images/icons/delete.png"></td>
  </tr>
<? } }  else { ?>
  <tr>
    <td align="center" bgcolor="#FFCCFF" colspan="6">NO FOUND</td>
  </tr>
<?	} ?>
  </tbody>
</table>
</body>
</html>