<?
	include("../includes/db_config.inc.php");
	include("../includes/sys_config.inc.php");
	include("../functions/playlist.func.php");
	$DEBUG=TRUE;
	$Return=0;
	if( (isset($PlayListID)) && 	(isset($txtType))  && 	(isset($txtCategory)) &&	(isset($txtValue)) ){
		$Data=json_decode(playlist_line_option_list($PlayListID),true);
		$FoundID=0;
		if ($DEBUG) { echo "<pre>"; print_r($Data); echo "</pre>"; }
		for ($iCount=0; $iCount < count($Data); $iCount++) {
			if ($txtCategory == $Data[$iCount]['pho_name']) {
				$FoundID=$Data[$iCount]['pho_id'];
			}
		}

		if ($FoundID > 0) {
			$Result=playlist_line_option_edit($FoundID,$txtType,$txtCategory,$txtValue);
		} else {
			$Result=playlist_line_option_add($PlayListID,$txtType,$txtCategory,$txtValue);
		}
		if ($Result > 0) $Return=1;
	}
	echo $Return;		
?>