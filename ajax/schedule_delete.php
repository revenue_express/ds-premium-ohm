<?
	$DEBUG=FALSE;
	$ComID=1;
	include("../includes/db_config.inc.php");
	include("../includes/sys_config.inc.php");
	include("../functions/schedule.func.php");
	$Data=NULL;
	if (isset($schID)) {
		//	Delete Schedule Layout
		$sSQL=sprintf("DELETE FROM schedule_line WHERE schl_schh_id=%d ",$schID);
		if ($DEBUG) echo "[".__FUNCTION__."] sSQL -> '$sSQL'<br>";
		$DBCount=AffectedSQL($sSQL);
		if ($DEBUG)	$Return.=sprintf("\nDelete %d template in this schedule.",$DBCount);
//	Delete Schedule Period
		$sSQL=sprintf("DELETE FROM schedule_period WHERE schp_schh_id=%d ",$schID);
		if ($DEBUG) echo "[".__FUNCTION__."] sSQL -> '$sSQL'<br>";
		$DBCount=AffectedSQL($sSQL);
		if ($DEBUG) $Return.=sprintf("\nDelete %d period in this schedule.",$DBCount);
//	Delete Schedule Head
		$sSQL=sprintf("DELETE FROM schedule_head WHERE schh_id=%d ",$schID);
		if ($DEBUG) echo "[".__FUNCTION__."] sSQL -> '$sSQL'<br>";
		$DBCount=AffectedSQL($sSQL);
		if ($DBCount ==1) {
			$Return.=sprintf("Delete this schedule successful.",$DBCount);
		} else {
			$Return.=sprintf("Delete this schedule failed.",$DBCount);
		}
		$Data=$Return;
	}
	echo $Data;		
?>