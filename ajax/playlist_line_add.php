<?
	$DEBUG=FALSE;
	include("../includes/db_config.inc.php");
//	include("../includes/sys_config.inc.php");
	include("../functions/playlist.func.php");
	$Data=NULL;
	if ( (isset($HeadID)) and (isset($FileID)) ) {
		if (isset($Para)) {
			$Data=playlist_line_add($HeadID,$FileID,$Para);
		} else {
			$arrFile=split(",",$FileID);
			if (count($arrFile) > 1) {
				for ($iCount=1; $iCount<count($arrFile); $iCount++) {
					$Data=playlist_line_add($HeadID,$arrFile[$iCount]);
					if ($DEBUG) { echo sprintf("<br> playlist_line_add(%d,%d) => %d",$HeadID,$arrFile[$iCount],$Data); }
				}
			} else {
				$Data=playlist_line_add($HeadID,$FileID);
			}
		}
	}
	echo json_encode($Data);		
?>