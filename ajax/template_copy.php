<?
	$DEBUG=FALSE;
	$ComID=1;
	include("../includes/db_config.inc.php");
//	include("../includes/sys_config.inc.php");
	include("../functions/playlist.func.php");
	include("../functions/layout.func.php");
	include("../functions/feed.func.php");
	include("../functions/stream.func.php");

	$Data=NULL;
	if (isset($LayoutID)) {
		ExecuteNonReader("BEGIN");
		$CloneSuccess=TRUE;
		// Clone Layout Head and Get New LayoutID
		$NewLayoutID=layout_head_clone($ComID,$LayoutID);
		if ($DEBUG) {	echo sprintf("<br>LayoutID => '%d'; NewLayoutID=> '%d'",$LayoutID,$NewLayoutID); }
		if ($NewLayoutID) {
			// Get Layout Line of Master Layout
			$LayoutList=json_decode(layout_line_list($LayoutID),true);
			if ($DEBUG) {	echo "<br>LayoutList::<pre>"; print_r($LayoutList); echo "</pre>";}
			for ($iLayout=0; $iLayout<count($LayoutList) and $CloneSuccess ; $iLayout++) {
				// clone playlist in layout line and get new playlist id
				$TemplateID=$LayoutList[$iLayout]['layl_tmpl_id'];
				$CurrPlayListID=$LayoutList[$iLayout]['layl_plh_id'];
				$NewPlayListID=playlist_head_clone($ComID,$CurrPlayListID);
				if ($LayoutList[$iLayout]['type_id'] == 5) {
					$MaxPlayLine=playlist_feed_count($CurrPlayListID);
					$CountRow=playlist_feed_clone($NewPlayListID,$CurrPlayListID);
				} else if ($LayoutList[$iLayout]['type_id'] == 8) {
					$MaxPlayLine=playlist_stream_count($CurrPlayListID);
					$CountRow=playlist_stream_clone($NewPlayListID,$CurrPlayListID);
				} else {
					$MaxPlayLine=playlist_line_count($CurrPlayListID);
					$CountRow=playlist_line_clone($NewPlayListID,$CurrPlayListID);
				}

				// Generate new Playlist ID to Master List
				// Add Max Playlist line  and Clone Run Counter
				$LayoutList[$iLayout]['new_playlist_id']=$NewPalylistID;
				$LayoutList[$iLayout]['max_playlist']=$MaxPlayLine;
				$LayoutList[$iLayout]['run_playlist']=$CountRow;
				
				if ($DEBUG) {	echo sprintf("<br>MaxPlayLine => '%d'; CountRow=> '%d'",$MaxPlayLine,$CountRow); }
				if ($MaxPlayLine <> $CountRow) {
					// Data in Old Playlist not Equal New Playlist
					$CloneSuccess = FALSE;
				} else {
					// add new layout line with old layout data and change only new playlist id
					$NewLineID=layout_line_add($NewLayoutID,$TemplateID,$NewPlayListID);
					if ($NewLineID < 0 ) {
						$CloneSuccess=FALSE;
					}
				}
			}
			$Data=$NewLayoutID;
			if (! $CloneSuccess) {
				ExecuteNonReader("ROLLBACK");
/*
				if ($DEBUG) {	echo sprintf("<br>Copy Failed"); }
				// Restore Add Section
				$iRun=0;
				while ( ($iRun < count($LayoutList)) and ($LayoutList[$iRun]['max_playlist'] <> $LayoutList[$iRun]['run_playlist']) ) {
					playlist_head_active($LayoutList[$iRun]['new_playlist_id'],0);
					$iRun++;
				}
				$Data=NULL;
				layout_head_active($NextLayoutID,0);
*/
			} else {
				ExecuteNonReader("COMMIT");
			}
		}
	}
	echo json_encode($Data);		
?>