<?
	$DEBUG=FALSE;
	include("../includes/db_config.inc.php");
	include("../functions/template.func.php");
	include("../functions/cover.func.php");

	foreach ($_REQUEST as $Key => $Value) { $$Key=addslashes(urldecode($Value)); }
	$ComID=1;
	$Return=0;
	if (isset($HeadID)) {
		$OldID=$HeadID;
		$NewID=template_head_clone($ComID,$OldID);
		$CoverData=json_decode(layout_cover_info($OldID),true);
		$CoverID=0;
		if (count($CoverData)>0) {
			$CoverID=layout_cover_add($NewID,$CoverData[0]['file_id']);
			if ($DEBUG)
			echo sprintf("\n<br>Cover: FileID=>(%d),Old Cover=>(%d),New ID=>(%d)",$CoverData[0]['tmpc_file_id'],$CoverData[0]['tmpc_id'],$CoverID);
		}
		$OldCount=template_line_count($OldID);
		if ($DEBUG) echo "After Head Clone (NewID) => $NewID <br>";
		if ( $NewID > 0) {
			if ($DEBUG) echo "Clone Head  ........ Success<br>";
			$TemplateID=$NewID;
			if ($OldCount > 0) {
				$RowCount=template_line_clone($ComID,$OldID,$NewID);
				if ($RowCount == $OldCount) {
					if ($DEBUG) echo "Clone Line  ........ Success $RowCount Record(s)<br>";
					$Return=1;
				} else {
					if ($DEBUG) echo "Clone Line ......... Failed  ($RowCount/$OldCount)<br>";
				}
			} else {
				$Return=1;
			}
		} else {
			if ($DEBUG) echo "Clone Head ......... Failed";
		}
	}
	echo $Return;
?>