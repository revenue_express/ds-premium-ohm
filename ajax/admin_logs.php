<?
	$DEBUG=FALSE;
	$ComID=1;
	include("../includes/db_config.inc.php");
	include("../functions/client.func.php");
	$arrData=NULL;
	$bSQL=TRUE;	
	if (! isset($cmd)) $cmd=NULL;
	switch (strtolower($cmd)) {
		case 'live' :
//			$sSQL="SELECT * FROM access_log LEFT JOIN client_info ON (log_ip = cli_address) AND (cli_active=1) ";
			$sSQL="SELECT * FROM access_log WHERE (log_id > 0) ";
//			$sSQL.=" WHERE  (log_script like '%gen.all.schedule.php%') AND (log_session='') ";
			$sSQL.=" AND (log_cmd = 'none') AND (log_session='') ";
			if ($client > 0) {
				$sSQL.=sprintf(" AND (log_client = %d)",$client)	;
//				$sSQL.=sprintf(" AND (log_request like '%%ClientID=>%d%%')",$client)	;
			}
			if (isset($ip)) {
				$sSQL.=sprintf(" AND (log_ip LIKE '%%%s%%')",$ip);
			}

			if (isset($dstart)) {
				$start=sprintf("%s ",$dstart);
				if (strlen($tstart) == 8) { 
					$start.=$tstart; 
				} else {
					$start.="00:00:00";
				}
				$sSQL.=sprintf(" AND (log_dtime >= '%s')",$start);
			}
			if (isset($dstop)) {
				$stop=sprintf("%s ",$dstop);
				if (strlen($tstop) == 8) { 
					$stop.=$tstop; 
				} else {
					$stop.="23:59:59";
				}
				$sSQL.=sprintf(" AND (log_dtime <= '%s')",$stop);
			}
			$sSQL.=" ORDER BY log_dtime DESC LIMIT 0,1000";

			$sTSQL=sprintf("SELECT * FROM (%s) as T1 ORDER BY T1.log_dtime",$sSQL);
			if ($DEBUG) echo "SQL =>".$sTSQL;
			$arrData=json_decode(JSonSQL($sTSQL),true);
			if ($DEBUG) { echo "\n\r"; print_r($arrData); }
			break;

		case 'all':
			$sSQL="SELECT * FROM access_log WHERE (log_id > 0) ";
			if (isset($client)) {
				if ($client > 0) $sSQL.=sprintf(" AND (log_client = %d)",$client);
			}
			if (isset($ip)) {
				$sSQL.=sprintf(" AND (log_ip LIKE '%%%s%%')",$ip);
			}
			if (isset($dstart)) {
				$start=sprintf("%s ",$dstart);
				if (strlen($tstart) == 8) { 
					$start.=$tstart; 
				} else {
					$start.="00:00:00";
				}
				$sSQL.=sprintf(" AND (log_dtime >= '%s')",$start);
			}
			if (isset($dstop)) {
				$stop=sprintf("%s ",$dstop);
				if (strlen($tstop) == 8) { 
					$stop.=$tstop; 
				} else {
					$stop.="23:59:59";
				}
				$sSQL.=sprintf(" AND (log_dtime <= '%s')",$stop);
			}
			$sSQL.=" ORDER BY log_dtime DESC LIMIT 0,1000";

			$sTSQL=sprintf("SELECT * FROM (%s) as T1 ORDER BY T1.log_dtime",$sSQL);
			if ($DEBUG) echo "SQL =>".$sTSQL;
			$arrData=json_decode(JSonSQL($sTSQL),true);
			for ($iCount=0; $iCount<count($arrData); $iCount++) {
				$sScript=split("/",$arrData[$iCount]['log_script']);
				$sFile=$sScript[count($sScript)-1];
				if ($arrData[$iCount]['log_cmd']=='none') {
					$arrData[$iCount]['log_cmd']=$sFile;
					if ($sFile=='gen.all.schedule.php') {
						$arrData[$iCount]['log_cmd']='get all schedule';
					}
				}
				if ($arrData[$iCount]['log_feed']==0) 	$arrData[$iCount]['log_feed']='-';
			}
			if ($DEBUG) { echo "\n\r"; print_r($arrData); }
			break;

		case 'feed':
			$sSQL="SELECT * FROM access_log WHERE (log_id > 0) ";
			$sSQL.="AND (log_cmd like '%feed%') ";
			if (isset($client)) {
				if ($client > 0) $sSQL.=sprintf(" AND (log_client = %d)",$client);
			}
			if (isset($ip)) {
				$sSQL.=sprintf(" AND (log_ip LIKE '%%%s%%')",$ip);
			}
			if (isset($dstart)) {
				$start=sprintf("%s ",$dstart);
				if (strlen($tstart) == 8) { 
					$start.=$tstart; 
				} else {
					$start.="00:00:00";
				}
				$sSQL.=sprintf(" AND (log_dtime >= '%s')",$start);
			}
			if (isset($dstop)) {
				$stop=sprintf("%s ",$dstop);
				if (strlen($tstop) == 8) { 
					$stop.=$tstop; 
				} else {
					$stop.="23:59:59";
				}
				$sSQL.=sprintf(" AND (log_dtime <= '%s')",$stop);
			}
			$sSQL.=" ORDER BY log_dtime DESC LIMIT 0,1000";

			$sTSQL=sprintf("SELECT * FROM (%s) as T1 ORDER BY T1.log_dtime",$sSQL);
			if ($DEBUG) echo "SQL =>".$sTSQL;
			$arrData=json_decode(JSonSQL($sTSQL),true);
			for ($iCount=0; $iCount<count($arrData); $iCount++) {
				if ($arrData[$iCount]['log_feed']==0) 	$arrData[$iCount]['log_feed']='-';
			}
			if ($DEBUG) { echo "\n\r"; print_r($arrData); }
			break;
		case 'usage':
			$sSQL="SELECT * FROM access_log WHERE (log_id > 0) ";
			$sSQL.="AND (log_script like '%test3%') ";
			if (isset($ip)) {
				$sSQL.=sprintf(" AND (log_ip LIKE '%%%s%%')",$ip);
			}
			if (isset($dstart)) {
				$start=sprintf("%s ",$dstart);
				if (strlen($tstart) == 8) { 
					$start.=$tstart; 
				} else {
					$start.="00:00:00";
				}
				$sSQL.=sprintf(" AND (log_dtime >= '%s')",$start);
			}
			if (isset($dstop)) {
				$stop=sprintf("%s ",$dstop);
				if (strlen($tstop) == 8) { 
					$stop.=$tstop; 
				} else {
					$stop.="23:59:59";
				}
				$sSQL.=sprintf(" AND (log_dtime <= '%s')",$stop);
			}
			$sSQL.=" ORDER BY log_dtime DESC LIMIT 0,1000";

			$sTSQL=sprintf("SELECT * FROM (%s) as T1 ORDER BY T1.log_dtime",$sSQL);
			if ($DEBUG) echo "SQL =>".$sTSQL;
			$arrData=json_decode(JSonSQL($sTSQL),true);
			for ($iCount=0; $iCount<count($arrData); $iCount++) {
				$arrValue=json_decode($arrData[$iCount]['log_session'],true);
				foreach ($arrValue as $Key => $Value) { 
					$arrData[$iCount][$Key]=$Value;
				}
			}
			if ($DEBUG) { echo "\n\r"; print_r($arrData); }
			break;
		default :
			break;
	}
	echo json_encode($arrData);		
?>