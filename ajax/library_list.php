<?
	$DEBUG=FALSE;
	$ComID=1;
	include("../includes/db_config.inc.php");
//	include("../includes/sys_config.inc.php");
	include("../functions/library.func.new.php");
	$Data=NULL;

	if (isset($TypeID)) {
		switch($TypeID) {
			case 1:	//	Image
				$Data=json_decode(library_list($ComID,$TypeID),true);
				for ($iCount=0; $iCount<count($Data); $iCount++) {
					$Data[$iCount]['Used']=file_used($Data[$iCount]['file_id']);
        	$sFileName=sprintf("../data/images/%s",$Data[$iCount]['file_sname']);
					$parts=pathinfo($sFileName);
					if (file_exists($sFileName)) {
						clearstatcache(); 
						$iSize=getimagesize($sFileName);
						$Data[$iCount]['Size']=sprintf("%d X %d",$iSize[0],$iSize[1]);
						$Data[$iCount]['file_small']=sprintf("./data/images/%s_s.%s",$parts['filename'],$parts['extension']);
						$Data[$iCount]['file_gallery']=sprintf("./data/images/%s_g.%s",$parts['filename'],$parts['extension']);
					}
				}
				break;
			case 2:	//	Video
				$Data=json_decode(library_list($ComID,$TypeID),true);
				for ($iCount=0; $iCount<count($Data); $iCount++) {
					$Data[$iCount]['Used']=file_used($Data[$iCount]['file_id']);
        	$sFileName=sprintf("../data/videos/%s",$Data[$iCount]['file_sname']);
					if (file_exists($sFileName)) {
						clearstatcache(); 
						$movie = new ffmpeg_movie($sFileName);
						$mTime=$movie->getDuration();
						$msize=array("height"=>$movie->getFrameHeight(),"width"=>$movie->getFrameWidth());
				
						$Data[$iCount]['Duration']=sprintf("%s",gmdate('H:i:s',$mTime));
						$Data[$iCount]['Size']=sprintf("%d X %d",$msize['width'],$msize['height']);
				//		$movie->destory();
						unset($movie);		
					}
				}
				break;
			case 4:	//	Audio
				$Data=json_decode(library_list($ComID,$TypeID),true);
				for ($iCount=0; $iCount<count($Data); $iCount++) {
					$Data[$iCount]['Used']=file_used($Data[$iCount]['file_id']);
        	$sFileName=sprintf("../data/audios/%s",$Data[$iCount]['file_sname']);
					if (file_exists($sFileName)) {
						clearstatcache(); 
						$movie = new ffmpeg_movie($sFileName);
						$mTime=$movie->getDuration();
						$Data[$iCount]['Duration']=sprintf("%s",gmdate('H:i:s',$mTime));
				//		$movie->destory();
						unset($movie);		
					}
				}
				break;
			case 3:	//	Flash
			case 9:	//	Document
			case 10:	//	Acrobat
			case 12:	//	Powerpoint
				$Data=json_decode(library_list($ComID,$TypeID),true);
				for ($iCount=0; $iCount<count($Data); $iCount++) {
					$Data[$iCount]['Used']=file_used($Data[$iCount]['file_id']);
				}
				break;
			case 5:	//	Text
				include("../functions/feed.func.php");
				$Data=json_decode(feed_list(),true);
				for ($iCount=0; $iCount<count($Data); $iCount++) {
					$Data[$iCount]['Used']=feed_used($Data[$iCount]['feed_id']);
				}
				break;
			case 6:	//	Rss
			case 7:	//	WebPage
			case 8:	//	Streaming
				break;
			case 11:	//	Cover
				$Data=json_decode(library_list($ComID,$TypeID),true);
				for ($iCount=0; $iCount<count($Data); $iCount++) {
					$Data[$iCount]['Used']=file_used($Data[$iCount]['file_id']);
        	$sFileName=sprintf("../data/covers/%s",$Data[$iCount]['file_sname']);
					$parts=pathinfo($sFileName);
					if (file_exists($sFileName)) {
						clearstatcache(); 
						$iSize=getimagesize($sFileName);
						$Data[$iCount]['Size']=sprintf("%d X %d",$iSize[0],$iSize[1]);
						$Data[$iCount]['file_small']=sprintf("./data/covers/%s_s.%s",$parts['filename'],$parts['extension']);
						$Data[$iCount]['file_gallery']=sprintf("./data/covers/%s_g.%s",$parts['filename'],$parts['extension']);
					}
				}
				break;
		}
	}
	echo json_encode($Data);		
?>