<?
	$DEBUG=FALSE;
	include("../includes/db_config.inc.php");
	include("../functions/layout.func.php");
	$Data=NULL;
	if ((isset($LName)) and (isset($TID))) {
		$LayoutName=$LName;
		$TemplateID=$TID;
		$AddSuccess=FALSE;
		// Add Layout Head and Get New LayoutID
		ExecuteNonReader("BEGIN");
		$LayoutID=layout_head_add($ComID,$LayoutName,$TemplateID);
		if ($LayoutID) {
			$AddSuccess=TRUE;
			include("../functions/template.func.php");
			include("../functions/playlist.func.php");
			// Get Template Line for Select Template
			$TemplateList=json_decode(template_line_list($ComID,$TemplateID),true);
//			if ($DEBUG) { echo "TemplateList<pre>"; print_r($TemplateList); echo "</pre>"; }
			if ($DEBUG) { echo "TemplateList".$TemplateList; }
			for($iRun=0; $iRun<count($TemplateList) && $AddSuccess; $iRun++) {
			// Add Playlist match with each Template Item
				$PlayListID=playlist_head_add($ComID,$TemplateList[$iRun]['tmpl_type_id']);
				$TLineID=$TemplateList[$iRun]['tmpl_id'];
				if ($PlayListID > 0) {
			// Add Layout Item with Item ID and Playlist ID
					$LineID=layout_line_add($LayoutID,$TLineID,$PlayListID);
					if ($LineID > 0) {
						if ($DEBUG) echo sprintf("<br>Add Layout Line with tmpl_id=>%d,tmpl_pll_id=>%d",$TLineID,$PlayListID);
					} else {
						$AddSuccess = false;
					}
				} else {
					$AddSuccess = false;
				}
			}
			$Data=$LayoutID;
		}
		if ($AddSuccess) {
			ExecuteNonReader("COMMIT");
		} else {
			ExecuteNonReader("ROLLBACK");
		}
	}
	echo json_encode($Data);		
?>