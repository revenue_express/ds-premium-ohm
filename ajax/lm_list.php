<? 
	$DEBUG=FALSE;
	$ComID=1;
	include("../includes/db_config.inc.php");

	foreach ($_REQUEST as $Key => $Value) { $$Key=addslashes(urldecode($Value)); }

	$CSSName=NULL;
	$CSSData=NULL;
	$LMData=NULL;

	$LayoutItems=NULL;
	$clsTitle=array("id"=>0,"title"=>"title","type_id"=>0,"type_name"=>"type_name","ttype"=>"header",
		"position"=>"fixed","top"=>0,"left"=>0,"width"=>0,"height"=>0,
		"display"=>"none","z-index"=>100,"opacity"=>1,"bgcolor"=>'#FFFFFF',"border"=>0.1);

	$clsLayout=array("id"=>0,"title"=>"title","type_id"=>0, "type_name"=>"type_name","ttype"=>"",
		"position"=>"fixed","top"=>0,"left"=>0,"width"=>0,"height"=>0,
		"display"=>"none","z-index"=>0,"opacity"=>1,"bgcolor"=>'#FFFFFF',"border"=>0.1);

	$clsFont=array(
		"font-family"=>"Helvetica, Arial, sans-serif",
		"font-size"=>"70px",
		"font-weight"=>"bold",
		"font-style"=>"italic",
		"font-transform"=>"uppercase",
		"color"=>"#f57df5"		);

	$CSSArray=array(
		"t-body"=>$clsLayout,
		"video1title"=>$clsTitle,
		"video1"=>$clsLayout,
		"video1filetitle"=>$clsTitle,
		"video2title"=>$clsTitle,
		"video2"=>$clsLayout,
		"video2filetitle"=>$clsTitle,
		"video3title"=>$clsTitle,
		"video3"=>$clsLayout,
		"video3filetitle"=>$clsTitle,
		"video4title"=>$clsTitle,
		"video4"=>$clsLayout,
		"video4filetitle"=>$clsTitle,
		"image1title"=>$clsTitle,
		"image1"=>$clsLayout,
		"image1filetitle"=>$clsTitle,
		"image2title"=>$clsTitle,
		"image2"=>$clsLayout,
		"image2filetitle"=>$clsTitle,
		"image3title"=>$clsTitle,
		"image3"=>$clsLayout,
		"image3filetitle"=>$clsTitle,
		"image4title"=>$clsTitle,
		"image4"=>$clsLayout,
		"image4filetitle"=>$clsTitle,
		"plugin1"=>$clsLayout,
		"plugin2"=>$clsLayout,
		"feed1panel"=>$clsLayout,
		"feed2panel"=>$clsLayout,
		"backpanel"=>$clsLayout,
		"coverpanel"=>$clsLayout,
		"txt1"=>$clsFont,
		"txt2"=>$clsFont);


	if (isset($TID)) {
		include("../functions/template.func.php");
		$CSSData="";
//	Get Layout Information 		
		$Header=json_decode(template_head_info($TID),true);
		if ($DEBUG) { echo sprintf("<br><br>Header <pre>",$iRun); print_r($Header); echo "</pre>";  }
		if (count($Header)>0) {
			$CSSArray['t-body']['height']=$Header[0]['disp_height'];
			$CSSArray['t-body']['width']=$Header[0]['disp_width'];
			$LMData['LayoutID']=$TID;
			$LMData['LayoutName']=$Header[0]['tmph_name'];
			$LMData['tbody']['width']=$Header[0]['disp_width'];
			$LMData['tbody']['height']=$Header[0]['disp_height'];
				
//			$CSSData.=sprintf("#t-body{\nheight=%dpx; width:%dpx}\n\n",$Header[0]['disp_height'],$Header[0]['disp_width']); 
//	Get Layout Details 		
			$Line=json_decode(template_line_list($ComID,$TID,$USER['usr_id']),true);
// Initial Array to keep Object Count
			$Item=array();
			for ($iRun=0; $iRun<count($Line); $iRun++) {
				if ($Line[$iRun]['tmpl_active']==0) continue;
//				if (isset($Item[$Line[$iRun]['type_name']])) {
				if (isset($Item[$Line[$iRun]['type_panel']])) {
//	Last Found This Object
//					$Item[$Line[$iRun]['type_name']]++;
					$Item[$Line[$iRun]['type_panel']]++;
//					$iCount=$Item[$Line[$iRun]['type_name']];
					$iCount=$Item[$Line[$iRun]['type_panel']];
				} else {
//	Not Found This Object
					$iCount=1;
//					$Item[$Line[$iRun]['type_name']]=1;							
					$Item[$Line[$iRun]['type_panel']]=1;							
				}
//				$panelName=sprintf("%s%d",$Line[$iRun]['type_panel'],$Item[$Line[$iRun]['type_name']]);
				$panelName=sprintf("%s%d",$Line[$iRun]['type_panel'],$Item[$Line[$iRun]['type_panel']]);

//				$panelHead=sprintf("%s%dtitle",$Line[$iRun]['type_panel'],$Item[$Line[$iRun]['type_name']]);
//				$panelFoot=sprintf("%s%dfiletitle",$Line[$iRun]['type_panel'],$Item[$Line[$iRun]['type_name']]);
				$panelHead=sprintf("%s%dtitle",$Line[$iRun]['type_panel'],$Item[$Line[$iRun]['type_panel']]);
				$panelFoot=sprintf("%s%dfiletitle",$Line[$iRun]['type_panel'],$Item[$Line[$iRun]['type_panel']]);

				if ($Line[$iRun]['type_id']==5) $panelName.="panel";

				$pHeight=$Line[$iRun]['tmpl_height'];
				$pWidth=$Line[$iRun]['tmpl_width'];
				$pTop=$Line[$iRun]['tmpl_top'];
				$pLeft=$Line[$iRun]['tmpl_left'];
				$zOrder=$Line[$iRun]['tmpl_order'];
				if ($DEBUG) { echo sprintf("<br><br>Item No.%d => %s",$iRun,$panelName); echo "<pre>"; print_r($Line[$iRun]); echo "</pre>";  }
				$CSSArray[$panelName]['left']=$pLeft;
				$CSSArray[$panelName]['top']=$pTop;
				$CSSArray[$panelName]['width']=$pWidth;
				$CSSArray[$panelName]['height']=$pHeight;
				$CSSArray[$panelName]['z-index']=$zOrder;
				$CSSArray[$panelName]['display']='';
				
				$HeadIdx=-1;
				$FooterIdx=-1;
				if (($Line[$iRun]['type_id']==1) || ($Line[$iRun]['type_id']==2) ) {
					$OptionLine=json_decode(template_option_list($Line[$iRun]['tmpl_id']),true);
					for ($iOption=0; $iOption<count($OptionLine); $iOption++) {
						if ($OptionLine[$iOption]['tplo_name'] == 'header') $HeadIdx=$iOption;
						if ($OptionLine[$iOption]['tplo_name'] == 'footer') $FooterIdx=$iOption;
					}
				}
//				if (($Line[$iRun]['tmpl_header']==1) && ($HeadIdx > -1)){
				if ($HeadIdx > -1) {
					$panelHead=$panelName."title";
					// Header Description
					$LMData['PanelObject'][$panelHead]['id']=sprintf("%d",$OptionLine[$HeadIdx]['tplo_id']);
					$LMData['PanelObject'][$panelHead]['ttype']="Header";
					$LMData['PanelObject'][$panelHead]['title']=$panelHead;
					$LMData['PanelObject'][$panelHead]['left']=$OptionLine[$HeadIdx]['tplo_left'];
					$LMData['PanelObject'][$panelHead]['top']=$OptionLine[$HeadIdx]['tplo_top'];
					$LMData['PanelObject'][$panelHead]['width']=$OptionLine[$HeadIdx]['tplo_width'];
					$LMData['PanelObject'][$panelHead]['height']=$OptionLine[$HeadIdx]['tplo_height'];
					$LMData['PanelObject'][$panelHead]['zindex']=$OptionLine[$HeadIdx]['tplo_order'];
					$LMData['PanelObject'][$panelHead]['opacity']=$OptionLine[$HeadIdx]['tplo_opacity'];
					$LMData['PanelObject'][$panelHead]['color']=$OptionLine[$HeadIdx]['tplo_color'];
					$LMData['PanelObject'][$panelHead]['bgcolor']=$OptionLine[$HeadIdx]['tplo_bgcolor'];
					$LMData['PanelObject'][$panelHead]['border']=$OptionLine[$HeadIdx]['tplo_border'];
				}

//				$panelName=$Line[$iRun]['tmpl_name'];
				$LMData['PanelObject'][$panelName]['id']=sprintf("%d",$Line[$iRun]['tmpl_id']);
//				$LMData['PanelObject'][$panelName]['ttype']=$Line[$iRun]['type_panel'];
				$LMData['PanelObject'][$panelName]['ttype']="media";
				$LMData['PanelObject'][$panelName]['title']=$Line[$iRun]['tmpl_name'];
				$LMData['PanelObject'][$panelName]['left']=$Line[$iRun]['tmpl_left'];
				$LMData['PanelObject'][$panelName]['top']=$Line[$iRun]['tmpl_top'];
				$LMData['PanelObject'][$panelName]['width']=$Line[$iRun]['tmpl_width'];
				$LMData['PanelObject'][$panelName]['height']=$Line[$iRun]['tmpl_height'];
				$LMData['PanelObject'][$panelName]['zindex']=$Line[$iRun]['tmpl_order'];
				$LMData['PanelObject'][$panelName]['opacity']=$Line[$iRun]['tmpl_opacity'];
				$LMData['PanelObject'][$panelName]['color']=$Line[$iRun]['tmpl_color'];
				$LMData['PanelObject'][$panelName]['bgcolor']=$Line[$iRun]['tmpl_bgcolor'];
				$LMData['PanelObject'][$panelName]['border']=$Line[$iRun]['tmpl_border'];

//				if (($Line[$iRun]['tmpl_footer']==1)&& ($FooterIdx > -1)) {
				if ($FooterIdx > -1) {
					// Footer Description
					$panelFooter=$panelName."filetitle";
					$LMData['PanelObject'][$panelFooter]['id']=sprintf("%d",$OptionLine[$FooterIdx]['tplo_id']);
					$LMData['PanelObject'][$panelFooter]['ttype']="Footer";
					$LMData['PanelObject'][$panelFooter]['title']=$panelFooter;
					$LMData['PanelObject'][$panelFooter]['left']=$OptionLine[$FooterIdx]['tplo_left'];
					$LMData['PanelObject'][$panelFooter]['top']=$OptionLine[$FooterIdx]['tplo_top'];
					$LMData['PanelObject'][$panelFooter]['width']=$OptionLine[$FooterIdx]['tplo_width'];
					$LMData['PanelObject'][$panelFooter]['height']=$OptionLine[$FooterIdx]['tplo_height'];
					$LMData['PanelObject'][$panelFooter]['zindex']=$OptionLine[$FooterIdx]['tplo_order'];
					$LMData['PanelObject'][$panelFooter]['opacity']=$OptionLine[$FooterIdx]['tplo_opacity'];
					$LMData['PanelObject'][$panelFooter]['color']=$OptionLine[$FooterIdx]['tplo_color'];
					$LMData['PanelObject'][$panelFooter]['bgcolor']=$OptionLine[$FooterIdx]['tplo_bgcolor'];
					$LMData['PanelObject'][$panelFooter]['border']=$OptionLine[$FooterIdx]['tplo_border'];
				}
//
				if ( ($Line[$iRun]['type_id']==1) || ($Line[$iRun]['type_id']==2) ) { 
					$CSSArray[$panelHead]['left']=$pLeft;
					$CSSArray[$panelHead]['top']=$pTop-$TitleName_Height;
					$CSSArray[$panelHead]['width']=$pWidth;
					$CSSArray[$panelHead]['height']=$TitleName_Height;
					$CSSArray[$panelHead]['display']='';

					$CSSArray[$panelFoot]['left']=$pLeft;
					$CSSArray[$panelFoot]['top']=$pTop+$pHeight-$TitleFile_Height;
					$CSSArray[$panelFoot]['width']=$pWidth;
					$CSSArray[$panelFoot]['height']=$TitleFile_Height;
					$CSSArray[$panelFoot]['display']='';
				}
				

//				$CSSData.=sprintf("#%s {\nposition:fixed; ",$panelName);
//				$CSSData.=sprintf("left=%dpx; top=%dpx; height:%dpx; width:%dpx;}\n\n",$pLeft,$pTop,$pHeight,$pWidth);
	
			}
		}
	}
//	if ($DEBUG) { echo "CSSData =>\n<pre>"; print_r($CSSArray); echo "</pre><br>"; }
	$CSSData.=sprintf("\n#t-body { height:%dpx; width:%dpx; }",$CSSArray['t-body']['height'],$CSSArray['t-body']['width']);
/* Image,Video,Feed Panel */
	for ($iPanel=1; $iPanel<=4; $iPanel++) {
		if ($iPanel==1) $TName="image";
		if ($iPanel==2) $TName="video";
		if ($iPanel==3) $TName="feed";
		if ($iPanel==4) $TName="plugin";
		for ($iRun=1; $iRun<=4; $iRun++) {
			$PName=sprintf("%s%d",$TName,$iRun);
			$PTitle=sprintf("%s%dtitle",$TName,$iRun);
			$PFile=sprintf("%s%dfiletitle",$TName,$iRun);
		if ($iPanel==3) $PName.="panel";
			if (isset($CSSArray[$PName])) {
				$CSSData.=sprintf("\n#%s { position:fixed; ",$PName);
				$CSSData.=sprintf("left:%dpx; ",$CSSArray[$PName]['left']);
				$CSSData.=sprintf("top:%dpx; ",$CSSArray[$PName]['top']);
				$CSSData.=sprintf("width:%dpx; ",$CSSArray[$PName]['width']);
				$CSSData.=sprintf("height:%dpx; ",$CSSArray[$PName]['height']);
				$CSSData.=sprintf("z-index: %d; ",$CSSArray[$PName]['z-index']);
				if ($CSSArray[$PName]['display']=='none') { $CSSData.=sprintf("display: none; ");} 
				$CSSData.=" } ";
			}
			if (isset($CSSArray[$PTitle])) {
				$CSSData.=sprintf("\n#%s { position:fixed; ",$PTitle);
				$CSSData.=sprintf("left:%dpx; ",$CSSArray[$PTitle]['left']);
				$CSSData.=sprintf("top:%dpx; ",$CSSArray[$PTitle]['top']);
				$CSSData.=sprintf("width:%dpx; ",$CSSArray[$PTitle]['width']);
				$CSSData.=sprintf("height:%dpx; ",$CSSArray[$PTitle]['height']);
				$CSSData.=sprintf("z-index: %d; ",$CSSArray[$PTitle]['z-index']);
				if ($CSSArray[$PTitle]['display']=='none') { $CSSData.=sprintf("display: none; ");} 
				$CSSData.=" } ";
			}
			if (isset($CSSArray[$PFile])) {
				$CSSData.=sprintf("\n#%s { position:fixed; ",$PFile);
				$CSSData.=sprintf("left:%dpx; ",$CSSArray[$PFile]['left']);
				$CSSData.=sprintf("top:%dpx; ",$CSSArray[$PFile]['top']);
				$CSSData.=sprintf("width:%dpx; ",$CSSArray[$PFile]['width']);
				$CSSData.=sprintf("height:%dpx; ",$CSSArray[$PFile]['height']);
				$CSSData.=sprintf("z-index: %d; ",$CSSArray[$PFile]['z-index']);
				if ($CSSArray[$PFile]['display']=='none') { $CSSData.=sprintf("display: none; ");} 
				$CSSData.=" } ";
			}
		}
	}
	
/* Back Panel and Cover Panel*/
/*
	for ($iRun=1; $iRun<=2; $iRun++) {
		if ($iRun==1) $PName="cover";
		if ($iRun==2) $PName="back";
		$PName.=sprintf("%dpanel",$iRun);
		if (isset($CSSArray[$PName])) {
			$CSSData.=sprintf("\n#%s { position=fixed; ",$PName);
			$CSSData.=sprintf("left:%dpx; ",$CSSArray[$PName]['left']);
			$CSSData.=sprintf("top:%dpx; ",$CSSArray[$PName]['top']);
			$CSSData.=sprintf("width=%dpx; ",$CSSArray[$PName]['width']);
			$CSSData.=sprintf("height=%dpx; ",$CSSArray[$PName]['height']);
			$CSSData.=sprintf("z-index: %d; ",$CSSArray[$PName]['z-index']);
			if ($CSSArray[$PName]['display']=='none') { $CSSData.=sprintf("display: none; ");} 
			$CSSData.=" } ";
		}
	}


*/
/*Cover Panel */
	$PName="coverpanel";
	if (isset($CSSArray[$PName])) {
		$URL=sprintf('%s/ajax/layout_cover_info.php?HeadID=%d',$LocalHost,$TID);
		$CoverReturn=json_decode(file_get_contents($URL),true);
		if ($DEBUG) { echo "Cover $URL<pre>"; print_r($CoverReturn); echo "</pre>"; }
		if (count($CoverReturn) > 0) {
			$CSSArray[$PName]['display']='block';
			$CSSData.=sprintf("\n#%s { position=fixed; ",$PName);
			$CSSData.=sprintf("left:%dpx; ",$CSSArray[$PName]['left']);
			$CSSData.=sprintf("top:%dpx; ",$CSSArray[$PName]['top']);
			$CSSData.=sprintf("width=%dpx; ",$Header[0]['disp_width']);
			$CSSData.=sprintf("height=%dpx; ",$Header[0]['disp_height']);
			$CSSData.=sprintf("z-index: %d; ",99);
			if ($CSSArray[$PName]['display']=='none') { $CSSData.=sprintf("display: none; ");} 
			$CSSData.=" } ";
			
			$Path1="../data/covers";
			$Path2="./data/covers";
			$Path=$Path1;
			if (! is_dir($Path1)) $Path = $Path2;

			$FileName=sprintf("%s/%s",$Path,$CoverReturn['file_sname']);
			if (file_exists($FileName)) {
				$Path=$Path1;
				if (is_dir($Path)) $Path = $Path2;

				$LMData['coverimg']['id']=$CoverReturn['file_id'];
				$LMData['coverimg']['src']=sprintf("%s/%s",$Path,$CoverReturn['file_sname']);
			}
		}
	}

/* Text Panel */
	for ($iRun=1; $iRun<=2; $iRun++) {
		$PName=sprintf("txt%d",$iRun);
		if (isset($CSSArray[$PName])) {
			$CSSData.=sprintf("\n#%s { ",$PName);
			$CSSData.=sprintf("font-family: %s; ",$CSSArray[$PName]['font-family']);
			$CSSData.=sprintf("font-size: %s; ",$CSSArray[$PName]['font-size']);
			$CSSData.=sprintf("font-style: %s; ",$CSSArray[$PName]['font-style']);
			$CSSData.=sprintf("font-transform: %s; ",$CSSArray[$PName]['font-transform']);
			$CSSData.=sprintf("color: %s; ",$CSSArray[$PName]['color']);
			$CSSData.=" } ";
		}
	}
//	echo $CSSData;
	echo json_encode($LMData);	
?>
	