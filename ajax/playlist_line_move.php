<?
	include("../includes/db_config.inc.php");
	include("../includes/sys_config.inc.php");
	include("../functions/playlist.func.php");
	$DEBUG=FALSE;
	$Data=NULL;
	if ( (isset($CurrID)) and (isset($NextID)) and (isset($TypeID)) and (isset($PlayListID))) {
		$CurrOrder='0';
		$NextOrder='0';
		$Items=json_decode(playlist_line_list($ComID,$TypeID,$PlayListID),true);
		for ($iCount=0; $iCount<count($Items); $iCount++) {
			if ($Items[$iCount]['pll_id'] == $CurrID) { $CurrOrder=$Items[$iCount]['pll_order']; }
			if ($Items[$iCount]['pll_id'] == $NextID) { $NextOrder=$Items[$iCount]['pll_order']; }
		}
		if ($DEBUG) echo "<br>CurrOrder ($CurrID)= $CurrOrder<br>NextOrder($NextID) = $NextOrder<br>";
		$Data=playlist_line_order($CurrID,$NextOrder);
		$Data+=playlist_line_order($NextID,$CurrOrder);
		if ($Data <> 2) {
			$Data=NULL;
		}
	}
	echo json_encode($Data);		
?>