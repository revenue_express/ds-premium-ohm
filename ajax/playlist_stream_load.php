<?
	$DEBUG=FALSE;
	include("../includes/db_config.inc.php");
	include("../includes/sys_config.inc.php");
	include("../functions/stream.func.php");
	$Data=0;
	if ( (isset($TypeID)) && (isset($SrcID)) && (isset($DstID))) {
		$Data=json_decode(playlist_stream_info($SrcID),true);
		$iMax=count($Data);
		$iCount=0;
		for ($iRun=0; $iRun < $iMax; $iRun++) {
			$iReturn=playlist_stream_add($DstID,$Data[$iRun]['plstm_stm_id']);
			if ($iReturn > 0) $iCount++;
		}
		$Data=($iMax == $iCount)?1:0;
	}
	echo $Data;		
?>