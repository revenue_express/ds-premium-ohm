<? 
	$DEBUG=FALSE;
	$ComID=1;
	include("../includes/db_config.inc.php");
//	include("../includes/sys_config.inc.php");
	$CSSName=NULL;
	$LayoutItems=NULL;
	$FileList=NULL;
	$arrFileList=array();
	$ServerIP=$_SERVER["SERVER_ADDR"];
	$TypeUseFile=array(1,2,3,4,9,10,11,12);
	
	if  (isset($ScheduleID)) {
		load_config();
		include("../functions/schedule.func.php");
		include("../functions/layout.func.php");
		include("../functions/playlist.func.php");
		include("../functions/command.func.php");


		$Group=sprintf('%s',date('YmdHis'));
		$FRetry=3;					

		/*	Get Schedule Header Information */
		$schInfo=json_decode(schedule_head_view($ScheduleID),true);
		if ($DEBUG) {  echo "<br>schInfo<pre>"; print_r($schInfo); echo "</pre>"; }

		$ShowTime="";
		/*	Get Period ShowTime Information */
		$schDate=json_decode(schedule_period_list($ScheduleID),true);
		if ($DEBUG) {  echo "<br>schDate<pre>"; print_r($schDate); echo "</pre>"; }

		for ($iCount=0; $iCount < count($schDate); $iCount++) {
			$ShowTime[]=array("start"=>$schDate[$iCount]['schp_start'],"stop"=>$schDate[$iCount]['schp_stop']);
		}
		if ($DEBUG) {  echo "<br>ShowTime :<pre>"; print_r($ShowTime); echo "</pre>"; }

		/*	Get Template in Schedule*/
		$schItem=json_decode(schedule_layout_list($ScheduleID,1),true);
		if ($DEBUG) {  echo "<br>schItem<pre>"; print_r($schItem); echo "</pre>"; }
		/*	Loop for Get Template Information */
		for ($iSch=0; $iSch < count($schItem); $iSch++) {
			if ($DEBUG) {  echo sprintf("<br>schItem [%d] [%d]<pre>",$schItem[$iSch],$iSch); print_r($schItem[$iSch]); echo "</pre>"; }
			/*	Get Each Template Detail*/
			$TItem=json_decode(layout_line_list($schItem[$iSch]['layh_id']),true);		
			if ($DEBUG) {  echo sprintf("<br>LayItem on Layout %d<pre>",$schItem[$iSch]['layh_tmph_id']); print_r($TItem); echo "</pre>"; }
			$Panel=NULL;
			/* Loop Each Template Detail to Collect Playlist*/
			$ItemCount=array();
			/* CREATE CSS FILE*/
			$CSSData=file_get_contents($LocalHost.'/gen.css.php?TID='.$schItem[$iSch]['layh_tmph_id']);
//			$CSSName=sprintf("%d_%s.css",$schItem[$iSch]['layh_tmph_id'],date('Ymdhis'));
			$CSSName=sprintf("%d.css",$schItem[$iSch]['layh_tmph_id']);
			$CSSPath=sprintf("../%s/css",$DataPath);
			$CSSFile=sprintf("%s/%s",$CSSPath,$CSSName);
			file_put_contents($CSSFile,$CSSData);
			$URL=sprintf("http://%s/%s/%s/css/%s",$ServerIP,$LocalPath,$DataPath,$CSSName);
			$Hash=hash_file('md5',$CSSFile);
//			$DEBUG=TRUE;
//			$Link=sprintf("%s/?cmd=download&cmdid=%d&p1=layout&p2=%s&p3=%s&p4=%s&sid=%s&grp=%s",$cURL,$CMD,$CSSName,$URL,$Hash,$Secret,$Group);
			if (! isset($FileList[$CSSName])){
				$FileList[$CSSName]="1";
				$arrFileList[]=array(	"name"=>$CSSName,
																		"TypeID"=>0,
																		"group"=>$Group,
																		"dst"=>"/layout",
																		"url"=>$URL,
																		"hash"=>$Hash,
																		"title"=>$CSSName,
																		"retry"=>$RetryCount,
				);
			} else {
				continue;
			}

			/* CREATE Cover FILE*/
			$CoverReturn=file_get_contents($LocalHost.'/ajax/layout_cover_info.php?HeadID='.$schItem[$iSch]['layh_tmph_id']);
			$CoverData=json_decode($CoverReturn,true);
			$CoverFile=sprintf("media/cover/%s",$CoverData['file_sname']);

			if (count($CoverData) > 0) {
				$CoverName=$CoverData['file_sname'];
				$CoverPath=sprintf("../%s/covers/",$DataPath);
				$CoverFile=sprintf("%s/%s",$CoverPath,$CoverName);
				$URL=sprintf("http://%s/%s/%s/covers/%s",$ServerIP,$LocalPath,$DataPath,$CoverName);
				$Hash=hash_file('md5',$CoverFile);
	//			$DEBUG=TRUE;
	//			$Link=sprintf("%s/?cmd=download&cmdid=%d&p1=layout&p2=%s&p3=%s&p4=%s&sid=%s&grp=%s",$cURL,$CMD,$CSSName,$URL,$Hash,$Secret,$Group);
				
				if (! isset($FileList[$CoverName])){
					$FileList[$CoverName]="1";
					$arrFileList[]=array(	"name"=>$CoverData['file_sname'],
																		"TypeID"=>11,
																		"group"=>$Group,
																		"dst"=>"media/cover",
																		"url"=>$URL,
																		"hash"=>$Hash,
																		"title"=>$CoverData['file_dname'],
																		"retry"=>$RetryCount,
					);
				}

			}

//			$DEBUG=FALSE;

			for ($iTmp=0; $iTmp < count($TItem); $iTmp++) {
				$PLHead=json_decode(playlist_head_view($TItem[$iTmp]['layl_plh_id']),true);
				if ($DEBUG) {  echo sprintf("<br>PLHead %d<pre>",$TItem[$iTmp]['layl_plh_id']); print_r($PLHead); echo "</pre>"; }
				/* Get Playlist Items*/
				if (in_array($PLHead[0]['plh_type_id'],$TypeUseFile)) {
					$PLLine=json_decode(playlist_line_list($ComID,$PLHead[0]['plh_type_id'],$PLHead[0]['plh_id']),true);
				}
				if ($DEBUG) {  echo sprintf("<br>PLLine %d<pre>",$TItem[$iTmp]['layl_plh_id']); print_r($PLLine); echo "</pre>"; }
				$PLItem="";

				/*Loop for create Playlist Items Array*/
				for ($iPlay=0; $iPlay < count($PLLine); $iPlay++) {
//					$FPath=sprintf("/media/%s/%s",$PLLine[$iPlay]['file_path'],$PLLine[$iPlay]['file_sname']);
					$FSPath=sprintf("%s/%s",$PLLine[$iPlay]['type_spath'],$PLLine[$iPlay]['file_sname']);
					$FPath=sprintf("../%s/%s/%s",$DataPath,$PLLine[$iPlay]['type_path'],$PLLine[$iPlay]['file_sname']);
					$PLItem[]=array(
					"id"=>$PLLine[$iPlay]['file_id'],
					"title"=>$PLLine[$iPlay]['file_oname'],
//					"type"=>$PLLine[$iPlay]['type_name'],
					"type"=>mime_content_type($FPath),
					"src"=>$FSPath,
					);
					
					$FName=$PLLine[$iPlay]['file_sname'];
					$FPath=sprintf("%s/%s/%s",$DataPath,$PLLine[$iPlay]['type_path'],$PLLine[$iPlay]['file_sname']);
					$FDest=$PLLine[$iPlay]['type_spath'];
					$FType=$PLLine[$iPlay]['type_name'];
					switch ($PLLine[$iPlay]['type_id']) {
						case 9:
						case 10:
						case 12:
							 $FType="Flash"; 
						break;
					}
					$URL=sprintf("http://%s/%s/%s",$ServerIP,$LocalPath,$FPath);
					$Hash=hash_file('md5',"../".$FPath);
//					$DEBUG=TRUE;
					if ($DEBUG) echo " return => '$CMD'";
					if (! isset($FileList[$FName])){
						$FileList[$FName]="1";
						$arrFileList[]=array(	"name"=>$FName,
																				"TypeID"=>$PLLine[$iPlay]['type_id'],
																				"group"=>$Group,
																				"dst"=>$FDest,
																				"url"=>$URL,
																				"hash"=>$Hash,
																				"title"=>$PLLine[$iPlay]['file_dname'],
																				"retry"=>$RetryCount,
						);
					}
//					$DEBUG=FALSE;

				}
			}
			
		}

		/*Create Schedule Array*/
	}
	echo json_encode($arrFileList);
	
?>
	