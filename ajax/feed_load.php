<?
	include("../includes/db_config.inc.php");
	include("../includes/sys_config.inc.php");
	include("../functions/feed.func.php");
	$DEBUG=TRUE;
	$Data=NULL;
	if ( (isset($SrcID)) && (isset($DstID))) {
		$Data=json_decode(playlist_feed_line_list($SrcID),true);
		$iMax=count($Data);
		for ($iRun=0; $iRun < $iMax; $iRun++) {
			$Order=playlist_feed_last_order($DstID)+1;
			playlist_feed_add($DstID,$Data[$iRun]['plf_feed_id'],$Order);
		}
	}
	echo json_encode($Data);		
?>