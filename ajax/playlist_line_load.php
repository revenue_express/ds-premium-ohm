<?
	include("../includes/db_config.inc.php");
	include("../includes/sys_config.inc.php");
	include("../functions/playlist.func.php");
	$DEBUG=FALSE;
	$Data=0;
	if ( (isset($TypeID)) && (isset($SrcID)) && (isset($DstID))) {
		$Data=json_decode(playlist_line_list($ComID,$TypeID,$SrcID),true);
		$iMax=count($Data);
		$iCount=0;
		for ($iRun=0; $iRun < $iMax; $iRun++) {
			$iReturn=playlist_line_add($DstID,$Data[$iRun]['pll_file_id']);
			if ($iReturn > 0) $iCount++;
		}
		$Data=($iMax == $iCount)?1:0;
	}
	echo $Data;		
?>