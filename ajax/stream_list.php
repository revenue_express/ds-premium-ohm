<?
	include("../includes/db_config.inc.php");
	include("../includes/sys_config.inc.php");
	include("../functions/stream.func.php");
	$DEBUG=FALSE;
	$Data=NULL;
	if (! isset($Page)) $Page=0;
	if (! isset($PerPage)) $PerPage=0;
	$Data=json_decode(stream_list($Page,$PerPage),true);
	for ($iCount=0; $iCount<count($Data); $iCount++) {
		$Data[$iCount]['Used']=stream_used($Data[$iCount]['stm_id']);
	}
	echo json_encode($Data);		
?>