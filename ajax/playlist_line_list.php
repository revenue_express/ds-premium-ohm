<?
	$DEBUG=FALSE;
	include("../includes/db_config.inc.php");
//	include("../includes/sys_config.inc.php");
	include("../functions/playlist.func.php");
	include("../functions/library.func.php");
	$Data=NULL;
	if (isset($HeadID) and ($TypeID)) {
		$Data=json_decode(playlist_line_list($ComID,$TypeID,$HeadID),true);
		for ($iCount=0; $iCount<count($Data); $iCount++) {
			$sFileName=sprintf("../data/%s/%s",$Data[$iCount]['file_path'],$Data[$iCount]['file_sname']);
			if (file_exists($sFileName)) {
				clearstatcache();
				if (($TypeID == 1)) { 
					$iSize=getimagesize($sFileName);
					$Data[$iCount]['Size']=sprintf("%d X %d",$iSize[0],$iSize[1]);
				}

				if (($TypeID == 2) || ($TypeID == 4)) { 
					$movie = new ffmpeg_movie($sFileName);
					$mTime=$movie->getDuration();
					$msize=array("height"=>$movie->getFrameHeight(),"width"=>$movie->getFrameWidth());
			
					$Data[$iCount]['Size']=sprintf("%d X %d",$msize['width'],$msize['height']);
					$Data[$iCount]['Duration']=sprintf("%s",gmdate('H:i:s',$mTime));
					unset($movie);		
				}
			}
		}
	}
	if ($DEBUG) echo "<br>";
	echo json_encode($Data);		
?>