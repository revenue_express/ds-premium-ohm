<?
	$DEBUG=FALSE;
	include("../includes/db_config.inc.php");
//	include("../includes/sys_config.inc.php");
	include("../functions/library.func.php");
	include("../functions/type.func.php");
	$Return=NULL;
	$MediaPath="./data/";
	if (! is_dir($MediaPath)) $MediaPath="../data/";

	foreach ($_REQUEST as $Key => $Value) { $$Key=addslashes(urldecode($Value)); }
	$ComID=1;
	if (isset($TypeID)) {
		switch($TypeID) {
			case 1:	//	Image
			case 2:	//	Video
			case 3:	//	Flash
			case 4:	//	Audio
			case 9:	//	Document
			case 10:	//	Acrobat
			case 12:	//	Powerpoint
				$Data=json_decode(library_list($ComID,$TypeID),true);
				for ($iCount=0; $iCount<count($Data); $iCount++) {
					$Data[$iCount]['Used']=file_used($Data[$iCount]['file_id']);
					if (($Data[$iCount]['Used'] ==0) and ($Data[$iCount]['file_active']==0)) {
						$Result=sprintf("DEBUG=>file_delete(%d)",$Data[$iCount]['file_id']);
						$Result=file_delete($Data[$iCount]['file_id']);
						$MediaFile=sprintf("%s/%s/%s",$MediaPath,$Data[$iCount]['file_path'],$Data[$iCount]['file_sname']);
						if (file_exists($MediaFile)) unlink($MediaFile);
						$Return[]=array("ID"=>$Data[$iCount]['file_id'],
																	"Name"=>$Data[$iCount]['file_dname'],
																	"Active"=>$Data[$iCount]['file_active'],
																	"Used"=>$Data[$iCount]['Used'],
																	"Result"=>$Result);
					}
				}
				break;
			case 5:	//	Text
				include("../functions/feed.func.php");
				$Data=json_decode(feed_list(),true);
				for ($iCount=0; $iCount<count($Data); $iCount++) {
					$Data[$iCount]['Used']=feed_used($Data[$iCount]['feed_id']);
					if (($Data[$iCount]['Used'] ==0) and ($Data[$iCount]['feed_active']==0)) {
						$Result=sprintf("DEBUG=>feed_delete(%d)",$Data[$iCount]['feed_id']);
						$Result=feed_delete($Data[$iCount]['feed_id']);
						$Return[]=array("ID"=>$Data[$iCount]['feed_id'],
																	"Name"=>$Data[$iCount]['feed_data'],
																	"Active"=>$Data[$iCount]['feed_active'],
																	"Used"=>$Data[$iCount]['Used'],
																	"Result"=>$Result);
					}
				}
				break;
			case 6:	//	Rss
			case 7:	//	WebPage
			case 8:	//	Streaming
				break;
			case 11:	//	Cover
				$Data=json_decode(library_list($ComID,$TypeID),true);
				for ($iCount=0; $iCount<count($Data); $iCount++) {
					$Data[$iCount]['Used']=cover_used($Data[$iCount]['file_id']);
					if (($Data[$iCount]['Used'] ==0) and ($Data[$iCount]['file_active']==0)) {
						$Result=sprintf("DEBUG=>file_delete(%d)",$Data[$iCount]['file_id']);
						$Result=file_delete($Data[$iCount]['file_id']);
						$MediaFile=sprintf("%s/%s/%s",$MediaPath,$Data[$iCount]['file_path'],$Data[$iCount]['file_sname']);
						if (file_exists($MediaFile)) unlink($MediaFile);
						$Return[]=array("ID"=>$Data[$iCount]['file_id'],
																	"Name"=>$Data[$iCount]['feed_data'],
																	"Active"=>$Data[$iCount]['feed_active'],
																	"Used"=>$Data[$iCount]['Used'],
																	"Result"=>$Result);
					}
				}
				break;
		}
	}
	echo json_encode($Return);		
?>