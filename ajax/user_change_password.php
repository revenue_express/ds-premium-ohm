<?
	include("../includes/db_config.inc.php");
	include("../includes/sys_config.inc.php");
	include("../functions/user.func.php");
	$DEBUG=FALSE;
	$Data=NULL;
	if ( (isset($usrID)) && (isset($usrOldPass)) && (isset($usrNewPass)) ) {
		if ($DEBUG) foreach ($_REQUEST as $Key => $Value) { echo "<br>$Key=>".addslashes(urldecode($Value)); }
		if (user_check_password($usrID,$usrOldPass)) {
			$Result=user_change_pass($usrID,$usrOldPass,$usrNewPass);
			if ($Result <> 1) {
				$Data=0;
			} else {
				$Data=1;
			}
		} else {
			$Data="-1";
		}
	}
	echo $Data;		
?>