<?
	include("../includes/db_config.inc.php");
	include("../includes/sys_config.inc.php");
	include("../functions/schedule.func.php");
	$DEBUG=FALSE;
	$Data=NULL;
	if ( (isset($schID)) and  (isset($PeriodID)) ) {
		$Data=schedule_period_active($PeriodID,0);
		if ($Data <> 1) $Data=NULL;
	}
	echo json_encode($Data);		
?>