<?
	include("../includes/db_config.inc.php");
	include("../includes/sys_config.inc.php");
	include("../functions/feed.func.php");
	$DEBUG=FALSE;
	$Data=0;
	if ( (isset($SrcID)) && (isset($DstID))) {
		$Data=json_decode(playlist_feed_line_list($SrcID),true);
		$iMax=count($Data);
		$iCount=0;
		for ($iRun=0; $iRun < $iMax; $iRun++) {
			$Order=playlist_feed_last_order($DstID)+1;
			$iReturn=playlist_feed_add($DstID,$Data[$iRun]['plf_feed_id'],$Order);
			if ($iReturn > 0) $iCount++;
		}
		$Data=($iMax == $iCount)?1:0;
	}
	echo $Data;		
?>