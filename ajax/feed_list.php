<?
	$DEBUG=FALSE;
	include("../includes/db_config.inc.php");
	include("../functions/feed.func.php");
	$Data=NULL;
	if (! isset($Page)) $Page=0;
	if (! isset($PerPage)) $PerPage=0;
	$Data=json_decode(feed_list($Page,$PerPage,$ComID),true);
	for ($iCount=0; $iCount<count($Data); $iCount++) {
		$Data[$iCount]['feed_used']=feed_used($Data[$iCount]['feed_id']);
	}
	echo json_encode($Data);		
?>