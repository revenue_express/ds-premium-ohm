<?
	include("../includes/db_config.inc.php");
	include("../includes/sys_config.inc.php");
	include("../functions/feed.func.php");
	$DEBUG=FALSE;
	$Data=NULL;
	if ( (isset($CurrID)) and (isset($NextID)) and (isset($HeadID))) {
		$CurrOrder='0';
		$NextOrder='0';
		$Items=json_decode(playlist_feed_line_list($HeadID),true);
		for ($iCount=0; $iCount<count($Items); $iCount++) {
			if ($Items[$iCount]['plf_id'] == $CurrID) { $CurrOrder=$Items[$iCount]['plf_order']; }
			if ($Items[$iCount]['plf_id'] == $NextID) { $NextOrder=$Items[$iCount]['plf_order']; }
		}
		if ($DEBUG) echo "<br>CurrOrder ($CurrID)= $CurrOrder<br>NextOrder($NextID) = $NextOrder<br>";
		$Data=playlist_feed_order($CurrID,$NextOrder);
		$Data+=playlist_feed_order($NextID,$CurrOrder);
		if ($Data <> 2) {
			$Data=NULL;
		}
	}
	echo json_encode($Data);		
?>