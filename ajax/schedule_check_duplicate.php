<? 
	$DEBUG=FALSE;
	include("../includes/db_config.inc.php");
	include("../functions/schedule.func.php");
	$Return=0;
	$Founded=FALSE;
	
	function InTime($Start,$Stop,$Check) {
		global $DEBUG;
		$TStart=strtotime($Start);
		$TStop=strtotime($Stop);
		$TCheck=strtotime($Check);
		$Result=(($TStart <= $TCheck) && ($TCheck <= $TStop));
		if ($DEBUG) { echo sprintf("<br>[%s] Start [%s] Stop [%s] Check [%s] ==> %s",__FUNCTION__,$Start,$Stop,$Check,($Result)?"true":"false"); }
		return ($Result);
	}
	
	function InRange($Start,$Stop,$cStart,$cStop) {
		global $DEBUG;
		$TStart=strtotime($Start);
		$TStop=strtotime($Stop);
		$PStart=strtotime($cStart);
		$PStop=strtotime($cStop);
		$bFront=(($PStart < $TStart) && ($PStop <= $TStart));
		$bBack=(($TStop <= $PStart) && ($TStop < $PStop));
		$Result=(!($bFront || $bBack));
		if ($DEBUG) { 
			echo sprintf("<br>[%s] Start [%s-%s] Check [%s-%s]",__FUNCTION__,$Start,$Stop,$cStart,$cStop);
			echo sprintf("<br>[%s] bFront=> %s, bBack=> %s",__FUNCTION__,($bFront)?"true":"false",($bBack)?"true":"false");
			echo sprintf("<br>[%s] Result => %s ",__FUNCTION__,($Result)?"true":"false");
		}
		return ($Result);
	}
	
	if ($DEBUG) foreach ($_REQUEST as $Key => $Val) { echo sprintf("<br>%s=>%s",$Key,addslashes(urldecode($Val))); }

	if (isset($schID)) {
		$Items=json_decode(schedule_layout_list($schID),true);
		if (! isset($Sun)) $Sun=0;
		if (! isset($Mon)) $Mon=0;
		if (! isset($Tue)) $Tue=0;
		if (! isset($Wed)) $Wed=0;
		if (! isset($Thu)) $Thu=0;
		if (! isset($Fri)) $Fri=0;
		if (! isset($Sat)) $Sat=0;
		if (! isset($TStart)) $TStart="00:00:01";
		if (! isset($TStop)) $TStop="23:59:59";

//		$Items=array( array("schl_start"=>"08:00:00","schl_stop"=>"12:00:00","schl_monday"=>1),
//												array("schl_start"=>"13:00:00","schl_stop"=>"17:00:00","schl_monday"=>1) );
		if($DEBUG) { echo "[Items]<pre>"; echo print_r($Items); echo "</pre>"; }
		for ($iCount=0; $iCount < count($Items) && (! $Founded); $iCount++) {
			if ($Items[$iCount]['schl_id']== $LineID) {
				if ($DEBUG) echo "<br>skip ".$iCount;
				continue;
			} else {
				if ($DEBUG) echo "<br>run ".$iCount;
			}
			$schStart=$Items[$iCount]['schl_start'];
			$schStop=$Items[$iCount]['schl_stop'];
			if (($Sun == 1) && ($Items[$iCount]['schl_sunday']==1) && (! $Founded)) {
				$bFound=InRange($schStart,$schStop,$TStart,$TStop);
				$Founded = ($Founded || $bFound);
				if($DEBUG) { echo sprintf("Sun => '%s'",($Founded)?"true":"false"); }
			}
			if (($Mon == 1) && ($Items[$iCount]['schl_monday']==1) && (! $Founded)) {
				$bFound=InRange($schStart,$schStop,$TStart,$TStop);
				$Founded = ($Founded || $bFound);
				if($DEBUG) { echo sprintf("Mon => '%s'",($Founded)?"true":"false"); }
			}
			if (($Tue == 1) && ($Items[$iCount]['schl_tuesday']==1) && (! $Founded)) {
				$bFound=InRange($schStart,$schStop,$TStart,$TStop);
				$Founded = ($Founded || $bFound);
				if($DEBUG) { echo sprintf("Tue => '%s'",($Founded)?"true":"false"); }
			}
			if (($Wed == 1) && ($Items[$iCount]['schl_wednesday']==1) && (! $Founded)) {
				$bFound=InRange($schStart,$schStop,$TStart,$TStop);
				$Founded = ($Founded || $bFound);
				if($DEBUG) { echo sprintf("Wed => '%s'",($Founded)?"true":"false"); }
			}
			if (($Thu == 1) && ($Items[$iCount]['schl_thursday']==1) && (! $Founded)) {
				$bFound=InRange($schStart,$schStop,$TStart,$TStop);
				$Founded = ($Founded || $bFound);
				if($DEBUG) { echo sprintf("Thu => '%s'",($Founded)?"true":"false"); }
			}
			if (($Fri == 1) && ($Items[$iCount]['schl_friday']==1) && (! $Founded)) {
				$bFound=InRange($schStart,$schStop,$TStart,$TStop);
				$Founded = ($Founded || $bFound);
				if($DEBUG) { echo sprintf("Fri => '%s'",($Founded)?"true":"false"); }
			}
			if (($Sat == 1) && ($Items[$iCount]['schl_saturday']==1) && (! $Founded)) {
				$bFound=InRange($schStart,$schStop,$TStart,$TStop);
				$Founded = ($Founded || $bFound);
				if($DEBUG) { echo sprintf("Sat => '%s'",($Founded)?"true":"false"); }
			}
		}
		if ($Founded) { $Return=1;} 
		
	}
	if ($DEBUG) echo "<br> Return <br>";
	echo json_encode($Return);
?>
	