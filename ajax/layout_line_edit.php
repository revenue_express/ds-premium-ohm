<?
	$DEBUG=FALSE;
	include("../includes/db_config.inc.php");
//	include("../includes/sys_config.inc.php");
	include("../functions/template.func.php");
	$Data=0;
	foreach ($_REQUEST as $Key => $Value) { $$Key=addslashes(urldecode($Value)); }
	$ComID=1;
	if (! isset($Name)) $Name=NULL;
	if (! isset($HeadID)) $HeadID=0;
	if (! isset($TypeID)) $TypeID=0;
	if (! isset($Order)) $Order=-9999;
	if (! isset($Left)) $Left=-9999;
	if (! isset($Top)) $Top=-9999;
	if (! isset($Width)) $Width=-9999;
	if (! isset($Height)) $Height=-9999;
	if (! isset($chkHeader)) $chkHeader=false;
	if (! isset($chkFooter)) $chkFooter=false;

	if ($DEBUG) echo "<br>chkHeader => $chkHeader";
	if ($DEBUG) echo "<br>chkFooter => $chkFooter";

	if ( (isset($LineID)) &&(isset($HeadID)) && (isset($TypeID)) && (isset($Name)) && (isset($Order)) && 
		(isset($Left)) && (isset($Top)) && (isset($Width)) && (isset($Height))  ) {
		if (template_line_edit($LineID,$HeadID,$TypeID,$Name,$Order,$Left,$Top,$Width,$Height)) {
			$Data=1;
			$arrData=json_decode(template_option_list($LineID),true);
			$iMaxData=count($arrData);
			$iHeader=-1;
			$iFooter=-1;
			for ($iRun=0; $iRun<$iMaxData; $iRun++) {
				if (strtolower($arrData[$iRun]['tplo_name'])=='header') $iHeader=$iRun;
				if (strtolower($arrData[$iRun]['tplo_name'])=='footer') $iFooter=$iRun;
			}
			
			if ($DEBUG) { echo "<br>iHeader => $iHeader, iFooter => $iFooter \r\n"; }
// Header Operation
			if ($chkHeader=='true') {
				if ($DEBUG) echo "<br>chkHeader => True";
				if ($iHeader == -1) {
					$Data=0;
					if ($DEBUG) echo "<br>Add Header=>";
					if (template_option_add($LineID,"header",100,$Left,$Top-35,$Width,35)) $Data=1;
				}
			} else {
				if ($DEBUG) echo "<br>chkHeader => False";
				if ($iHeader > -1) {
					$Data=0;
					if ($DEBUG) echo "<br>Delete Header=>";
					if (template_option_delete($LineID,"header")) $Data=1;
				}
			}
			
// Footer Operation
			if ($chkFooter=='true') {
				if ($DEBUG) echo "<br>chkFooter => True";
				if ($iFooter == -1) {
					$Data=0;
					if ($DEBUG) echo "<br>Add Footer=>";
					if (template_option_add($LineID,"footer",100,$Left,$Top+$Height,$Width,35)) $Data=1;
				}
			} else {
				if ($DEBUG) echo "<br>chkFooter => False";
				if ($iFooter > -1) {
					$Data=0;
					if ($DEBUG) echo "<br>Delete Footer=>";
					if (template_option_delete($LineID,"footer")) $Data=1;
				}
			}
		}
	}
	echo $Data;		
?>