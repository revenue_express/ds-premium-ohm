<?
	$DEBUG=FALSE;
	$ComID=1;
	$bRun=TRUE;
	include("../includes/db_config.inc.php");
//	include("../includes/sys_config.inc.php");
	include("../functions/schedule.func.php");

	$vData=NULL;
	if (isset($schID)) {
		$bCloneSuccess=TRUE;
		/* get schedule head info to array Head*/
		$arrHead=json_decode(schedule_head_view($schID),true);
		/* get all schedule period to array Period*/
		$arrPeriod=json_decode(schedule_period_list($schID),true);
		/* get all schedule layout to array Layout*/
		$arrLayout=json_decode(schedule_layout_list($schID),true);
		/* Set New Schedule Name to Format "OldName_CurrDatetime" */
		$sHeadName=sprintf("%s_%s",$arrHead[0]['schh_name'],date('Ymshis'));

		if ($DEBUG) { echo "arrHead<pre>"; print_r($arrHead); echo "</pre><br>"; }
		if ($DEBUG) { echo "arrPeriod<pre>"; print_r($arrPeriod); echo "</pre><br>"; }
		if ($DEBUG) { echo "arrLayout<pre>"; print_r($arrLayout); echo "</pre><br>"; }

		/* Start Data For Rollback, Commit*/
		ExecuteNonReader("begin;");

		$iSchID=0;
		if ($bRun) {
			/*	Add schedule head and return new schedule ID*/
			$iSchID=schedule_head_add($ComID,$sHeadName,$arrHead[0]['schh_disp_id']);
		}

		if (($iSchID > 0) ){
			/*	Add schedule head completed*/
			/* Initial Data For Loop*/
			$iRunPeriod=0; $iRunLayout=0;
			$iMaxPeriod=count($arrPeriod);
			$iMaxLayout=count($arrLayout);
			if ($DEBUG) { echo sprintf("New Schedule ID => %d",$iSchID); }		

			for ($iCount=0; $iCount < $iMaxPeriod; $iCount++) {
				/* Loop for insert period of schedule*/
				if ($DEBUG) { echo "<br> Period ".$iCount." <pre>"; print_r($arrPeriod[$iCount]); echo "</pre>";}
				if ($bRun) {
					$sDate=split("-",$arrPeriod[$iCount]['schp_start']);
					$sStart=implode("/",array_reverse($sDate));
					if ($DEBUG) { echo "<br> Start=>";print_r($sDate);  echo " =>"; print_r($sStart); }
					$sDate=split("-",$arrPeriod[$iCount]['schp_stop']);
					$sStop=implode("/",array_reverse($sDate));
					if ($DEBUG) { echo "<br> Stop=>";print_r($sDate);  echo " =>"; print_r($sStop); }

					$iRec=schedule_period_add($iSchID,$sStart,$sStop);
					/* If insert successful, increase iRunPeriod*/
					if ($iRec > 0) $iRunPeriod++;
				}
			}
			/* if iRunPeriod not equal iMaxPeriod, uncompleted information*/	
			if ($iRunPeriod <> $iMaxPeriod) $bCloneSuccess=FALSE;

			for ($iCount=0; $iCount < $iMaxLayout; $iCount++) {
				/* Loop for insert layout of schedule*/
				$iLayoutID=$arrLayout[$iCount]['schl_layh_id'];
				$sStart=$arrLayout[$iCount]['schl_start'];
				$sStop=$arrLayout[$iCount]['schl_start'];
				$iSun=$arrLayout[$iCount]['schl_sunday'];
				$iMon=$arrLayout[$iCount]['schl_sunday'];
				$iTue=$arrLayout[$iCount]['schl_sunday'];
				$iWed=$arrLayout[$iCount]['schl_sunday'];
				$iThu=$arrLayout[$iCount]['schl_sunday'];
				$iFri=$arrLayout[$iCount]['schl_sunday'];
				$iSat=$arrLayout[$iCount]['schl_sunday'];
				if ($DEBUG) { echo "<br> Layout ".$iCount." <pre>"; print_r($arrLayout[$iCount]); echo "</pre>";}
				if ($bRun) {
					$iRec=schedule_layout_add($iSchID,$iLayoutID,$iSun,$iMon,$iTue,$iWed,$iThu,$iFri,$iSat,$sStart,$sStop);
					/* If insert successful, increase iRunPeriod*/
					if ($iRec > 0) $iRunLayout++;
				}
			}
			/* if iRunLayout not equal iMaxLayout, uncompleted information*/	
			if ($iRunLayout < $iMaxLayout) $bCloneSuccess=FALSE;
		} else {
			/*	Add schedule head failed*/
			$bCloneSuccess=FALSE;
		}

		if ($bCloneSuccess) {
			/* Clone successful.*/
			ExecuteNonReader("commit;");		
			$vData=$iSchID;		
		} else {
			/* Clone failed.*/
			ExecuteNonReader("rollback;");
		}
	}
	echo $vData;		
?>