<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html>
<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />     
    <title>Log in · Info Express</title>
    <script type="text/javascript" src="js/jquery-1.8.0.min.js"></script>
	<script type="text/javascript" src="js/jquery-ui-1.8.23.custom.min.js"></script>
    <script>
        $(function() {
			$( "#tabs" ).tabs();
        });
    </script>
    <link rel="stylesheet" href="css/custom-theme/jquery-ui-1.8.23.custom.css">
    <link rel="stylesheet" href="css/StyleSheet.css" type="text/css" />
</head>
<body>
    <div class="w-100 demo nohover">
        <div id="tabs">
            <ul>
                <li><a href="#tabs-1">Nunc tincidunt</a></li>
                <li><a href="#tabs-2">Proin dolor</a></li>
                <li><a href="#tabs-3">Aenean lacinia</a></li>                                             
            </ul>
            <div id="tabs-1">
            	<table class="w-100" cellspacing="0">
                	<? 	
					
					$data = 6;
					$rows = 4;
					$width = 260;
					$height = 205;
						
					for($i=0;$i<$data;$i++){
						if($i%$rows==0) echo "<tr>";
					?>
                    	<td <? if($i>=$rows){ ?> class="top15" <? } ?>>
                        	<div style="height:<?=$height;?>px; width:<?=$width;?>px;" class="boxin">
                            	<div class="header">
                                	<span style="font-size:15px;"><?=$i.":".$i%$rows." Information";?></span>
                                    <span style="float:right; margin-right:5px;">
                                    	<img id="Config1" src="images/calendar1.png" width="24" height="24"><img id="Config2" src="images/system_config_services.png" width="24" height="24" style="margin-left:4px;">
                                    </span>
                                </div>
                                <div style="margin:5px;">
                                	<img id="imgClient_7" src="./data/images/7.png" width="100%" height="155">
                                </div>
                            </div>
                     	</td>
   					<?
						if($i%$rows==$rows-1) echo "</tr>";
					} ?>
                </table>
            </div>
            <div id="tabs-2">
                <p>Tab of JqueryUI</p>
            </div>
            <div id="tabs-3">
                <p>Tab of JqueryUI</p>
            </div>
        </div>
	</div>
</body>
</html>