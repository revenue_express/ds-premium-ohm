<? 
	$TypeID=5;
	include("./includes/db_config.inc.php");
	include("./includes/sys_config.inc.php");
?>
<html>
<head>
<?	include("./javascript.php");?>
<script type="text/javascript" src="./js/playlist_feed.js"></script>
<script type="text/javascript" src="./js/playlist_head_option.js"></script>

<link rel="stylesheet" href="css/custom-theme/jquery-ui-1.8.23.custom.css">
<link rel="stylesheet" href="css/StyleSheet.css">
<script type="text/javascript">
	$('document').ready(function() {
		feed_head_list();
		
	});
	
	$(function(){
		$('#btnFeedAdd, #btnSelectFeed, #btnSelectPlaylist').button();
		$('#txtExistFeed,#txtExistPlaylist').height(15);
	});
</script>
</head>
<body>
<div id="dvHeadResult" class="w-80 boxin">
	<div class="header">
  	<h3>Playlist (Text Feed)
    	<input id="btnListRefresh" type="button" value="refresh" onClick="feed_head_list();">
     <input id="txtSearch" type="text" value="" size="10">
     <input id="btnSearch" type="button" value="search" onClick="table_search('tblHead_Result',2,'txtSearch');">
<!--     <input id="btnEdit" type="button" value="edit" onClick="lm_update(parseInt($('#txtSearch').val()));"> -->
    </h3>
  </div>
  <div class="w-100">
  <div style="float:left; padding-left:10px;">Preview::</div>
  <div style="float:left;" class="w-80"><marquee id="feedHeadPreview" class="w-100">Test Feed ::::: </marquee></div>
  </div>
  <table cellspacing="0">
  <thead>
  	<tr>
      <th class="w-10">Code</th>
      <th>Playlist Name</th>
      <th class="w-5">Item(s)</th>
      <th class="w-15">
      <input class="btnTH" type="button" value="refresh" onClick="feed_line_list('HeadID');"/>
      &nbsp; &nbsp;
      <input class="btnTH" type="button" value=" &nbsp;Add &nbsp;" onClick="feed_head_add();"/>
      </th>
  	</tr>
  </thead>
  <tbody id="tblHead_Result">
    <tr>
      <td class="center">1</td>
      <td>2</td>
      <td>Item</td>
      <td class="center"><img class="btnEdit" title="Edit"></td>
      <td class="center"><img class="btnDel" title="Delete"></td>
    </tr>
  </tbody>
  </table>
</div>
<div id="dvLineResult" class="boxin w-80">
	<div class="header">
  	<h3>Playlist (Text Feed)</h3><input class="btnHeader" style="margin-left:170px;" type="button" value="Back" onClick="feed_head_list();"/>
  </div>
  <div style="margin-top:5px; margin-bottom:5px;">
<?
$onChange="EnableObject('btnHeadSave',false);";
$sDisplay="Do you Comfirm to Change Playlist Name?";
$onChange.=sprintf("changeInputValue('txtHeadName','%s',document.getElementById('txtHeadName').value);",$sDisplay);
$onChange.="EnableObject('btnHeadSave',! checkEqual('txtHeadName','txtOldName'));";

$onReset="EnableObject('btnHeadSave',false);";
$onReset.="setObjValue('txtOldName','txtHeadName');";
?>
    &nbsp; &nbsp;  Playlist Name : 
    <input class="txt" size="40" type="text" id="txtHeadName" name="txtHeadName" readonly/>
    <input class="txt" size="40" type="hidden" id="txtOldName" name="txtOldName" />
    <input class="btnTh" style="margin-left:10px;" type="button" id="btnHeadChange" value="Change" onClick="<?=$onChange;?>">
    <input class="btnTh" style="margin-left:10px;" type="button" id="btnHeadSave" value="Save" onClick="playlist_head_save('HeadID','txtHeadName');" disabled>
    <input class="btnTh" style="margin-left:10px;" type="button" id="btnHeadReset" value="Reset" onClick="<?=$onReset;?>">
    <input class="btnTh" style="margin-left:10px;" type="button" value="Playlist Option" onClick="playlist_head_option('HeadID','TypeID');"/>
  </div>
  <div class="boxin w-100">
    <div class="header left">
      <h3><span>Text Feed Items</span>
      <span style="margin-left:20">
      <input class="btnTh" type="button" value="Refresh" onClick="feed_refresh(document.getElementById('HeadID').value);"/>
      <input class="btnTh" style="margin-left:10px;" type="button" value="Add New" onClick="feed_line_show_add();"/>
      <input class="btnTh" style="margin-left:10px;" type="button" value="Feed Library" onClick="feed_line_show_feed_exists();"/>
      <input class="btnTh" style="margin-left:10px;" type="button" value="Feed Playlist Exists" onClick="feed_line_show_playlist_exists();"/>
      <input id="HeadID" type="hidden" size="2"/> <input id="TypeID" type="hidden" value="5" size="2"/>
      </span></h3>
    </div>
    <div class="boxin w-100">
      <div style="float:left; margin-left:10px;">Preview::</div>
      <div style="float:left" class="w-90"><marquee id="feedLinePreview" class="w-100">1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0</marquee></div>
    </div>
    <table cellspacing="0" class="w-100">
    <tbody id="tblLine_Result">
      <tr>
        <td class="w-80 center"></td>
        <td class="w-20 center">
          <div class="dvtr top5 w-100">
             <div class="dvtd_left w-20 center"><img class="preview" src="images/icons/preview.png" title="preview"></div>
             <div class="dvtd_left w-20 center"><img class="preview" src="images/icons/down.png" title="down"></div>
             <div class="dvtd_left w-20 center"><img class="preview" src="images/icons/up.png" title="up"></div>
             <div class="dvtd_left w-20 center"><img class="preview" src="images/icons/edit.png" title="edit"></div>
             <div class="dvtd_left w-20 center"><img class="preview" src="images/icons/delete.png" title="delete"></div>
          </div>
        </td>
      </tr>
    </tbody>
    </table>
  </div>
</div>
<script type="text/javascript">
	$(function() {
		$('#dvAddText').dialog({ autoOpen: false });
		$('#dvAddText').dialog("option","width",900);
		$('#dvAddText').dialog("option","height",100);
		$('#dvAddText').dialog("option","resizable",false);
	});
</script>
<div id="dvAddText" title="Add New Feed Item.">
<div class="demo top15">
<input type="text" class="txt" size="125" maxlength="255" id="txtNewFeed"/>
<input type="hidden" id="FeedID" value='' size="2"/>
<input type="hidden" id="LineID" value='' size="2"/>
<input type="button" id='btnFeedAdd' value="Save" onClick="feed_line_add();"/>
</div>
</div>
<script type="text/javascript">
	$(function() {
		$('#dvEditText').dialog({ autoOpen: false });
		$('#dvEditText').dialog("option","width",900);
		$('#dvEditText').dialog("option","height",100);
		$('#dvEditText').dialog("option","resizable",false);
	});
</script>
<div id="dvEditText" title="Edit Feed Item.">
<div class="demo">
<input type="text" size="72" id="txtFeed_Edit" value=""/>
<input type="checkbox" id="chkFeedOnly" value="ThisOnly"/>This Only
<input type="text" id="FeedID_Edit" value='' size="2"/>
<input type="text" id="LineID_Edit" value='' size="2"/>
<input type="button" id='btnFeedEdit' value="Save" onClick="feed_line_save();"/>
</div>
</div>
<script type="text/javascript">
	$(function() {
		$('#dvSelectText').dialog({ autoOpen: false });
		$('#dvSelectText').dialog("option","width",900);
		$('#dvSelectText').dialog("option","height",280);
		$('#dvSelectText').dialog("option","resizable",false);
	});
</script>
<div id="dvSelectText" title="Selection Exists Feed.">
<div id="dvSelectTextItem" style="min-height:50px; height:190px; overflow:auto">
<table cellpadding="0" cellspacing="0">
  <tbody id="tblSelectText_Body">
  	<tr>
    	<td><input type="radio"></td>
    	<td>111111111111111111111111111111111111111111111</td>
    </tr>
  </tbody>
</table>
</div>
<div class="pagination">
	<div class="dvtr">
        <div class="dvtd_left w-25" style="margin-top:15px;">You select : </div>
        <div class="demo dvtd_right">
        	<input type="text" class="txt" style="margin-top:13px;" size="60" id="txtExistFeed"/>
			<input type="hidden" id="SelectFeedID" value='' size="4"/>
		</div>
        <div class="demo dvtd_right" style="padding-top:13px;">
			<input type="button" id='btnSelectFeed' value="Selected" disabled onClick="feed_exist_feed();"/>
		</div>
  	</div>
</div>
<script type="text/javascript">
	$(function() {
		$('#dvSelectPlaylist').dialog({ autoOpen: false });
		$('#dvSelectPlaylist').dialog("option","width",900);
		$('#dvSelectPlaylist').dialog("option","height",270);
		$('#dvSelectPlaylist').dialog("option","resizable",false);
	});
</script>
<div id="dvSelectPlaylist" title="Selection Exists Playlist.">
<div id="dvSelectPlaylistItem" style="min-height:50px; height:187px; overflow:auto">
<table cellpadding="0" cellspacing="0">
  <tbody id="tblSelectPlaylist_Body">
  <tr><td><input type="radio"></td><td>111111111111111111111111111111111111111111111</td>
  </tbody>
</table>
</div>
<div class="pagination">
	<div class="dvtr">
        <div class="dvtd_left w-25" style="margin-top:15px;">You select : </div>
        <div class="demo dvtd_right">
        	<input type="text" class="txt" style="margin-top:13px;" size="60" id="txtExistPlaylist"/>
			<input type="hidden" id="SelectPlaylistID" value='' size="4"/>
      	</div>
        <div class="demo dvtd_right" style="padding-top:13px;">
			<input type="button" id='btnSelectPlaylist' value="Selected" disabled onClick="feed_exist_playlist();"/>
		</div>
  	</div>
</div>
</div>
<script type="text/javascript">
	$(function() {
		$('#dvOption').dialog({ autoOpen: false });
		$('#dvOption').dialog("option","width",500);
		$('#dvOption').dialog("option","height",310);
		$('#dvOption').dialog("option","resizable",false);
	});
</script>
<div class="demo">
<div id="dvOption" title="Playlist Option Selection">
  <div id="tblOption_Body">
  </div>
</div>
</div>
</div>
</body>
</html>