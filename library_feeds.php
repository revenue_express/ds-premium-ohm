<? 
	$DEBUG=FALSE;
	$TypeID=5;
	include("./includes/db_config.inc.php");
	include("./functions/library.func.php");
?>
<html>
<head>
<?	include("./javascript.php");?>
<link rel="stylesheet" href="css/StyleSheet.css">
<link rel="stylesheet" href="css/custom-theme/jquery-ui-1.8.23.custom.css">
<script type="text/javascript" src="./js/library_feed.js"></script>
<script type="text/javascript">
	$('document').ready(function() {
		feed_list();
	});
</script>
</head>
<body>
<div id="dvForm">
    <div style="margin-bottom:10px;"><button class="btnBack" onClick="feed_list();"/></div>
    <div class="boxin">
        <span>
            <marquee id="txtExample" class="marquee" direction="left" vspace="5" hspace="5">
                Example Feeding. 1 2 3 4 5 6 7 8 9 0
            </marquee>
        </span>
        <table cellspacing="0">
        <thead>
            <tr>
<!--                <th class="w-10">Language</th> -->
                <th class="center">Feed Text</th>
                <th class="w-10" colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
    
            <tr class="even">
<!--
                <td>
                    <select id="selLang" name="selLang">
                        <option value="UTF-8">Thai/English</option>
                    </select>
                </td>
-->
                <td><input type="text" class="txt" name="txtFeed" id="txtFeed" size="120" maxlength="255"></td>
              	<td class="center">
                 	<div class="btnPre" onClick="feed_show('txtExample','txtFeed','selLang');" title="view"></div>
                </td>
                <td class="center">
                	<div class="btnSave" onClick="feed_save('FeedID','txtFeed','selLang');" title="Save"></div>
                </td>
                <td class="center">
                    <div class="btnRes" onClick="feed_reset();" title="Reset"></div>
                    <input id="FeedID" type="hidden" value=""><input type="hidden" id="txtOldFeed">
										<input type="hidden" id="selLang" name="selLang" value="UTF-8">
              	</td>
            </tr>
        </tbody>
        </table>
    </div>
</div>
<div id="dvResult">
    <div class="w-100 boxin">
        <div class="header">
        <span>Text Feed Library</span>
        <span style="padding-left:20px;">
        <input type="button" class="btnTh" id="btnHeadRefresh" value="Refresh" onClick="feed_list();">
        &nbsp; &nbsp;
        <input type="button" class="btnTh" id="btnHeadRefresh" value="Add New" onClick="feed_show_add();">
        </span>
        </div>
        <table cellspacing="0">
        	<span>
            	<marquee id="txtReview" class="marquee" direction="left" vspace="5" hspace="5">
            		Example Feeding. 1 2 3 4 5 6 7 8 9 0
               	</marquee>
            </span>	
            <thead>
            <tr>
<!--                <th class="w-10">Language</th> -->
                <th>Feed Text</th>
                <th width="9%" colspan="3">Action</th>
            </tr>
            </thead>
            <tbody id="tblFeed_Result">
            <tr>
<!--                <td>Thai</td> -->
                <td>Example Feeding</td>
                <td class="center">
                    <img class=" ui-icon ui-icon-search" onClick="feed_preview('txtReview','12345798','UTF-8');" title="view">
                </td>
                <td class="center">
                    <img class=" ui-icon ui-icon-pencil" onClick="feed_preview('txtReview','12345798','UTF-8');" title="edit">
                </td>
                <td class="center">
                    <img class=" ui-icon ui-icon-cancel" onClick="feed_preview('txtReview','12345798','UTF-8');" title="delete"></td>
            </tr>
            </tbody>
        </table>
    </div>
</div>
</body>
</html>