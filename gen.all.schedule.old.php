<? 
	include("./includes/db_config.inc.php");
//	include("./includes/sys_config.inc.php");
	include("./functions/schedule.func.php");
	include("./functions/layout.func.php");
	include("./functions/playlist.func.php");
	include("./functions/schedule_client.func.php");
	include("./functions/client.func.php");

	$DEBUG=FALSE;
	$CSSName=NULL;
	$LayoutItems=NULL;
	$TypeUseFile=array(1,2,3,4,9,10,11);
	

	foreach ($_REQUEST as $Key => $Value) { $$Key=addslashes(urldecode($Value)); }
	if (isset($ClientID)) {
		$ScheduleList=json_decode(schedule_client_list_from("Client",$ClientID,0),true);
		$ClientInfo=json_decode(client_info($ClientID),true);
		if ($DEBUG) {  echo "<br>ClientInfo<pre>"; print_r($ClientInfo); echo "</pre>"; }
		$ComID=$ClientInfo[0]['cli_com_id'];
		$Schedule=NULL;
		$mPanel=NULL;
//		echo "<pre>"; print_r($ScheduleList); echo "<pre>"; echo "<br>".count($ScheduleList);
		for($iSum=0; $iSum < count($ScheduleList); $iSum++) {
			$schID=$ScheduleList[$iSum]['schc_schh_id'];
//			echo sprintf("<br>%d , %d<br>",$iSum,$schID);
			/*	Get Schedule Header Information */
			$schInfo=json_decode(schedule_head_view($schID),true);
			if ($DEBUG) {  echo "<br>schInfo<pre>"; print_r($schInfo); echo "</pre>"; }
	
			$ShowTime="";
			/*	Get Period ShowTime Information */
			$schDate=json_decode(schedule_period_list($schID),true);
			if ($DEBUG) {  echo "<br>schDate<pre>"; print_r($schDate); echo "</pre>"; }
	
			for ($iCount=0; $iCount < count($schDate); $iCount++) {
				$ShowTime[]=array("start"=>$schDate[$iCount]['schp_start'],"stop"=>$schDate[$iCount]['schp_stop']);
			}
			if ($DEBUG) {  echo "<br>ShowTime :<pre>"; print_r($ShowTime); echo "</pre>"; }
	
			/*	Get Template in Schedule*/
			$schItem=json_decode(schedule_layout_list($schID),true);
			if ($DEBUG) {  echo "<br>schItem<pre>"; print_r($schItem); echo "</pre>"; }
			/*	Loop for Get Template Information */
			for ($iSch=0; $iSch < count($schItem); $iSch++) {
				if ($DEBUG) {  echo sprintf("<br>schItem [%d] [%d]<pre>",$schItem[$iSch],$iSch); print_r($schItem[$iSch]); echo "</pre>"; }
				/*	Get Each Template Detail*/
				$PanelCount=array(0,0,0,0,0,0,0,0,0,0,0,0,0,0,0);
				$TItem=json_decode(layout_line_list($schItem[$iSch]['layh_id']),true);		
				if ($DEBUG) {  echo sprintf("<br>LayItem on Layout %d<pre>",$schItem[$iSch]['layh_tmph_id']); print_r($TItem); echo "</pre>"; }
				$Panel=NULL;
				/* Loop Each Template Detail to Collect Playlist*/
				$ItemCount=array();
				for ($iTmp=0; $iTmp < count($TItem); $iTmp++) {
					/* Get Playlist Header Information*/
					$PlayList=NULL;
					$PLHead=json_decode(playlist_head_view($TItem[$iTmp]['layl_plh_id']),true);
					if ($DEBUG) {  echo sprintf("<br>PLHead %d<pre>",$TItem[$iTmp]['layl_plh_id']); print_r($PLHead); echo "</pre>"; }
					/* Get Playlist Header Option Information*/
					$PLHeadOption=json_decode(playlist_head_option_list($TItem[$iTmp]['layl_plh_id']),true);
					$PLConfig=NULL;
					for ($iPLOption=0; $iPLOption < count($PLHeadOption); $iPLOption++) {
						$Head=$PLHeadOption[$iPLOption]['pho_name'];
						$Value=$PLHeadOption[$iPLOption]['pho_value'];
						$PLConfig['option'][$Head]=$Value;
					}
	
					/* Get Playlist Items*/
					$PLLine=NULL;
					if ($PLHead[0]['plh_type_id'] != 5) {
						$PLLine=json_decode(playlist_line_list($ComID,$PLHead[0]['plh_type_id'],$PLHead[0]['plh_id']),true);
					}
					$iPLLCount=count($PLLine);
					if ($DEBUG) {  echo sprintf("<br>PLLine %d<pre>",$TItem[$iTmp]['layl_plh_id']); print_r($PLLine); echo "</pre>"; }
					$PLItem="";
	
					/*Loop for create Playlist Items Array*/
					if ($DEBUG) { echo sprintf("\r\n<br>Max PLLine %d ",$iPLLCount);  } 
					for ($iPlay=0; $iPlay < $iPLLCount; $iPlay++) {
	//					$FPath=sprintf("/media/%s/%s",$PLLine[$iPlay]['file_path'],$PLLine[$iPlay]['file_sname']);
						if ($DEBUG) { echo sprintf("\r\n<br>PLLine %d => ",$iPlay); print_r($PLLine[$iPlay]); echo "\r\n<br>";} 
						$FPath=sprintf("%s/%s",$PLLine[$iPlay]['type_spath'],$PLLine[$iPlay]['file_sname']);
						$Files=sprintf("./data/%s/%s",$PLLine[$iPlay]['type_path'],$PLLine[$iPlay]['file_sname']);
						$FMime=mime_content_type($Files);
						if ($PLHead[0]['plh_type_id'] == 2) {
							if ($FMime == "application/octet-stream") {
								$FMime="video/webm";
							}
						}
						$PLItemData=array(
	//						"pll_id"=>$PLLine[$iPlay]['pll_id'],
							"id"=>$PLLine[$iPlay]['file_id'],
							"title"=>$PLLine[$iPlay]['file_dname'],
//							"type"=>$PLLine[$iPlay]['type_name'],
							"type"=>$FMime,
							"src"=>$FPath);

						if ($PLLine[$iPlay]['type_id']==3) {
							$PLItemData['para']=$PLLine[$iPlay]['pll_para'];
						}

						if (in_array($PLLine[$iPlay]['type_id'],$TypeUseFile)) {
							$PLLineOption=json_decode(playlist_line_option_list($PLLine[$iPlay]['pll_id']),true);
							if (count($PLLineOption) > 0) {
								for ($iCount=0; $iCount<count($PLLineOption); $iCount++) {
								$Head=$PLLineOption[$iCount]['plo_name'];
								$Value=$PLLineOption[$iCount]['plo_value'];
									$PLItemData['option'][$Head]=$Value;
	//								$PLConfig['option'][$Head]=$Value;
								}
							}
						}
						$PLItem[]=$PLItemData;
					}
	
					/*Create Playlist Array*/
					$myPlayList=array(
						"id"=>$PLHead[0]['plh_id'],
						"title"=>$PLHead[0]['plh_name'],
//						"config"=>NULL
						"config"=>$PLConfig['option']
						);
	
//					for ($iCount=0; $iCount<count($PLHeadOption); $iCount++) {
//						$Head=$PLHeadOption[$iCount]['pho_name'];
//						$Value=$PLHeadOption[$iCount]['pho_value'];
//						$myPlayList[$Head]=$Value;
//					}
					
					if ($PLHead[0]['plh_type_id'] == 5) {
	
					} elseif ($PLHead[0]['plh_type_id'] == 8) {
						$sURL=sprintf("%s://%s:%d/%s",$PLLine[0]['stm_protocol'],$PLLine[0]['stm_address'],$PLLine[0]['stm_port'],$PLLine[0]['stm_para01']);
						$myPlayList['url']=$sURL;
					} else {
						$myPlayList["item"]=$PLItem	;
					}
					$PlayList[]=$myPlayList;
	//				echo "<pre>"; print_r($PLHeadOption); echo "</pre><br>\n";
	//				echo "<pre>"; print_r($myPlayList); echo "</pre><br>\n";
	
	/*Create Panel Array*/
					if (isset($ItemCount[$TItem[$iTmp]['type_panel']])) {
	//	Last Found This Object
						$ItemCount[$TItem[$iTmp]['type_panel']]++;
						$iCount=$ItemCount[$TItem[$iTmp]['type_panel']];
					} else {
	//	Not Found This Object
						$iCount=1;
						$ItemCount[$TItem[$iTmp]['type_panel']]=1;							
					}
					$PanelCount[$TItem[$iTmp]['type_id']]++;
//					$PanelCount[$PLHead[0]['type_panel']]++;
//					$PanelName=sprintf("%s%d",$PLHead[0]['type_panel'],$PanelCount[$PLHead[0]['type_id']]);
					$PanelName=sprintf("%s%d",$PLHead[0]['type_panel'],$ItemCount[$PLHead[0]['type_panel']]);
					$PanelDisp=sprintf("%s%dpanel",$TItem[$iTmp]['type_name'],$iCount);
					$TypeName=$TItem[$iTmp]['type_name'];
//	Change Type to Flash
					if (in_array($TItem[$iTmp]['type_id'],array(9,10,12)) ) $TypeName="Flash";
					$PlaylistTitle=$PlayList[0]['title'];
//
					$Panel[$PanelName]=array(
						"type"=>$TypeName,
						"name"=>$PlaylistTitle,
//						"name"=>$PanelDisp,
						"id"=>$TItem[$iTmp]['layl_id'],
						"title"=>$TItem[$iTmp]['tmpl_name'],
						"playlist"=>$PlayList);
					if	 (($PLHead[0]['type_id']=='2') || ($PLHead[0]['type_id']=='4')){
						$mPanel=$PanelName;
					}
						
				}
				
				$CSSPath="data/css";
				$CSSData=file_get_contents($LocalHost.'/gen.css.php?TID='.$schItem[$iSch]['layh_tmph_id']);
				$CSSName=sprintf("%d.css",$schItem[$iSch]['layh_tmph_id']);
				$CSSFile=sprintf("%s/%s",$CSSPath,$CSSName);
				unlink($CSSFile);
				file_put_contents($CSSFile,$CSSData);

				$CoverReturn=file_get_contents($LocalHost.'/ajax/layout_cover_info.php?HeadID='.$schItem[$iSch]['layh_tmph_id']);
				$CoverData=json_decode($CoverReturn,true);
				$CoverFile=sprintf("media/cover/%s",$CoverData['file_sname']);

				$Cover=array("src"=>"","enable"=>"false");
				if (count($CoverData) > 0) {
					$Cover=array("src"=>$CoverFile,"enable"=>"true");
				}
				$Plugin1=array();
				$Plugin2=array();
				$TxtFeed1=array();
				$TxtFeed2=array();

				$Template[]=array(
					"id"=>$schItem[$iSch]['layh_id'],
					"title"=>$schItem[$iSch]['layh_name'],
//					"src"=>sprintf("/layout/%s",$CSSName),
					"src"=>sprintf("%s",$CSSName),
					"start"=>$schItem[$iSch]['schl_start'],
					"stop"=>$schItem[$iSch]['schl_stop'],
					"day"=>array( 
						"sunday"=>($schItem[$iSch]['schl_sunday'])?"1":"0",
						"monday"=>($schItem[$iSch]['schl_monday'])?"1":"0",
						"tuesday"=>($schItem[$iSch]['schl_tuesday'])?"1":"0",
						"wednesday"=>($schItem[$iSch]['schl_wednesday'])?"1":"0",
						"thursday"=>($schItem[$iSch]['schl_thursday'])?"1":"0",
						"friday"=>($schItem[$iSch]['schl_friday'])?"1":"0",
						"saturday"=>($schItem[$iSch]['schl_saturday'])?"1":"0"	),
					"mpanel"=>(is_null($mPanel)?"video1":$mPanel),
					"panel"=>$Panel,
					"cover"=>$Cover,
					"plugin1"=>$Plugin1,
					"plugin2"=>$Plugin2,
					"textfeed1"=>$TxtFeed1,
					"textfeed2"=>$TxtFeed2
					);
			}
	
			/*Create Schedule Array*/
			$Schedule[]=array(
				"id"=>$schInfo[0]['schh_id'],
				"title"=>$schInfo[0]['schh_name'],
				"showtime"=>$ShowTime[0],
				"template"=>$Template );
		}
		load_config(NULL,NULL,json_encode(array("schedule"=>$Schedule)));
	}
	$Data=array("schedule"=>$Schedule);
	echo json_encode($Data);

?>
	
