<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<? 
	$DEBUG=FALSE;
	include("./includes/db_config.inc.php");
	include("./includes/sys_config.inc.php");
	include("./functions/group.func.php");
	include("./functions/client.func.php");
?>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<?	include("./javascript.php");?>
<link rel="stylesheet" href="css/custom-theme/jquery-ui-1.8.23.custom.css">
<link rel="stylesheet" href="css/StyleSheet.css">
<script type="text/javascript" src="./js/purge_data.js"></script>
<script type="text/javascript">
	$('document').ready(function() {
		$('#tabs').tabs({
			beforeLoad: function( event, ui ) {
				ui.jqXHR.error(function() {
					ui.panel.html("Couldn't load this tab. We'll try to fix this as soon as possible. " + "If this wouldn't be a demo." );
				});
			}
		}).addClass( "ui-tabs-vertical ui-helper-clearfix" );
		$( "#tabs li" ).removeClass( "ui-corner-top" ).addClass( "ui-corner-left" );


	});

</script>
    <style>
<!--    .ui-tabs-vertical { width: 55em; } -->
    .ui-tabs-vertical { width: 95%; }
    .ui-tabs-vertical .ui-tabs-nav { padding: .2em .1em .2em .2em; float: left; width: 12em; }
    .ui-tabs-vertical .ui-tabs-nav li { clear: left; width: 100%; border-bottom-width: 1px !important; border-right-width: 0 !important; margin: 0 -1px .2em 0; }
    .ui-tabs-vertical .ui-tabs-nav li a { display:block; }
    .ui-tabs-vertical .ui-tabs-nav li.ui-tabs-active { padding-bottom: 0; padding-right: .1em; border-right-width: 1px; border-right-width: 1px; }
<!--    .ui-tabs-vertical .ui-tabs-panel { padding: 1em; float: right; width: 40em;} -->
    .ui-tabs-vertical .ui-tabs-panel { padding: 1em; float: left; width: 80%;}
    </style>
</head>
<body>
 <div class="w-100 demo nohover">
	<div id="tabs">
		<ul>
    	<li><a href="#tabs-schedule"> Schedule </a></li>
    	<li><a href="#tabs-template"> Template </a></li>
    	<li><a href="#tabs-playlist"> Playlist </a></li>
    	<li><a href="#tabs-layout"> Layout </a></li>
    	<li><a href="#tabs-library"> Library </a></li>
    </ul>
		<div id="tabs-schedule">
    	Schedule ::
      <input id="btnPurgeSchedule" type="button" class="btnJQR" value="Purge" onclick="purge_schedule('tblPurgeSchedule');"/><p>
      <table class="boxin" align="left">
      	<thead>
        	<tr><th class="w-15">ID</th><th class="w-35">Schedule Name</th><th>Action/Result</th></tr>
        </thead>
        <tbody id="tblPurgeSchedule">
        <tr><td align="center" colspan="3">No Record Found.</td></tr>
        </tbody>
      </table>
    </div>
		<div id="tabs-template">
    	Schedule ::
      <input id="btnPurgeTemplate" type="button" class="btnJQR" value="Purge" onclick="purge_template('tblPurgeTemplate')"/><p>
      <table class="boxin" align="left">
      	<thead>
        	<tr><th class="w-15">ID</th><th class="w-35">Template Name</th><th>Result.</th></tr>
        </thead>
        <tbody id="tblPurgeTemplate">
        <tr><td align="center" colspan="3">No Record Found.</td></tr>
        </tbody>
      </table>
     </div>
		<div id="tabs-playlist">
    	<div>
    	<div style="float:left; text-align:center">
       Image<br /><input id="btnPurgePlaylistImage" type="button" class="btnJQR" value="Purge" onclick="purge_playlist('tblPurgePlaylist',1)"/>
      </div>
    	<div style="float:left; text-align:center">
      Video<br /><input id="btnPurgePlaylistVideo" type="button" class="btnJQR" value="Purge" onclick="purge_playlist('tblPurgePlaylist',2)"/>
      </div>
    	<div style="float:left; text-align:center">
       Flash<br /><input id="btnPurgePlaylistFlash" type="button" class="btnJQR" value="Purge" onclick="purge_playlist('tblPurgePlaylist',3)"/>
      </div>
    	<div style="float:left; text-align:center">
       Audio<br /><input id="btnPurgePlaylistAudio" type="button" class="btnJQR" value="Purge" onclick="purge_playlist('tblPurgePlaylist',4)"/>
      </div>
    	<div style="float:left; text-align:center">
      Text<br /><input id="btnPurgePlaylistText" type="button" class="btnJQR" value="Purge" onclick="purge_playlist('tblPurgePlaylist',5)"/>
      </div>
    	<div style="float:left; text-align:center">
      Acrobat<br /><input id="btnPurgePlaylistPDF" type="button" class="btnJQR" value="Purge" onclick="purge_playlist('tblPurgePlaylist',10)"/>
      </div>
    	<div style="float:left; text-align:center">
      Powerpoint<br /><input id="btnPurgePlaylistPPT" type="button" class="btnJQR" value="Purge" onclick="purge_playlist('tblPurgePlaylist',12)"/>
      </div>
      </div>
      <table class="boxin" align="left">
      	<thead>
        	<tr><th class="w-15">ID</th><th class="w-35">Playlist Name</th><th>Result.</th></tr>
        </thead>
        <tbody id="tblPurgePlaylist">
        <tr><td align="center" colspan="3">No Record Found.</td></tr>
        </tbody>
      </table>
    </div>
		<div id="tabs-layout">
    	Layout ::
      <input id="btnPurgeLayout" type="button" class="btnJQR" value="Purge" onclick="purge_layout('tblPurgeLayout')"/><p>
      <table class="boxin" align="left">
      	<thead>
        	<tr><th class="w-15">ID</th><th class="w-35">Layout Name</th><th>Result.</th></tr>
        </thead>
        <tbody id="tblPurgeLayout">
        <tr><td align="center" colspan="3">No Record Found.</td></tr>
        </tbody>
      </table>
    </div>
		<div id="tabs-library">
    	<div>
    	<div style="float:left; text-align:center">
       Image<br /><input id="btnPurgePlaylistImage" type="button" class="btnJQR" value="Purge" onclick="purge_library('tblPurgeLibrary',1)"/>
      </div>
    	<div style="float:left; text-align:center">
      Video<br /><input id="btnPurgePlaylistVideo" type="button" class="btnJQR" value="Purge" onclick="purge_library('tblPurgeLibrary',2)"/>
      </div>
    	<div style="float:left; text-align:center">
       Flash<br /><input id="btnPurgePlaylistFlash" type="button" class="btnJQR" value="Purge" onclick="purge_library('tblPurgeLibrary',3)"/>
      </div>
    	<div style="float:left; text-align:center">
       Audio<br /><input id="btnPurgePlaylistAudio" type="button" class="btnJQR" value="Purge" onclick="purge_library('tblPurgeLibrary',4)"/>
      </div>
    	<div style="float:left; text-align:center">
      Text<br /><input id="btnPurgePlaylistText" type="button" class="btnJQR" value="Purge" onclick="purge_library('tblPurgeLibrary',5)"/>
      </div>
    	<div style="float:left; text-align:center">
      Acrobat<br /><input id="btnPurgePlaylistPDF" type="button" class="btnJQR" value="Purge" onclick="purge_library('tblPurgeLibrary',10)"/>
      </div>
    	<div style="float:left; text-align:center">
      Powerpoint<br /><input id="btnPurgePlaylistPPT" type="button" class="btnJQR" value="Purge" onclick="purge_library('tblPurgeLibrary',12)"/>
      </div>
    	<div style="float:left; text-align:center">
      Cover<br /><input id="btnPurgePlaylistCover" type="button" class="btnJQR" value="Purge" onclick="purge_library('tblPurgeLibrary',11)"/>
      </div>
      </div>
      <table class="boxin" align="left" style="font-size:12px;">
      	<thead>
        	<tr><th class="w-15">ID</th><th class="w-35">Library Name</th><th>Result.</th></tr>
        </thead>
        <tbody id="tblPurgeLibrary">
        <tr><td align="center" colspan="3">No Record Found.</td></tr>
        </tbody>
      </table>
     </div>
	</div>
</div>
</body>
</html>