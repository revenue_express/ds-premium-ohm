<? 
	$DEBUG=FALSE;
	include("./includes/db_config.inc.php");
?>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<? include("./javascript.php");?>
<script type="text/javascript" src="./js/layout.js"></script>
<!--<script src="./js/mouseover_popup.js" language="JavaScript" type="text/javascript"></script> -->
<?	include("./layout.js.php");?>
<script type="text/javascript" src="./js/playlist.js"></script>
<script type="text/javascript">
	$(function() {
		$("#btnSelTemplate,#btnSelTemplateSave,#btnReset,#btnLayoutName,#btnLayoutView,#btnHeadChange").button();
		$("#btnHeadReset,#Playlist_Option,#btnHeaderRefresh,#btnHeaderLibrary,#btnHeaderPlaylist,#btnFeedShow").button();
	});
	
	$('document').ready(function() {
<? if (isset($TID)) {?>
	layout_loadmenu(<?=$TID;?>);
<?  } else {?>
		load_headlist();
<?	}?>
	});
	
	$(function(){
		$('#txtExistPlaylist,#txtExistFile,#txtExistFlash,#txtPara').height(15);
	});
</script>
<style type="text/css">
.spMenu {
	background-color:;
	font-size:x-large;
	color:#FFF;
	text-shadow:#CCC;
	font-weight:bolder;
	padding-top:10px;
	padding-bottom: 10px;
	padding-left:30px;
	padding-right:30px;
}
</style>
</head>
<body>
<div style="display: none; position: absolute; z-index: 110; left: 400; top: 100; width: 15; height: 15" id="preview_div"></div>
<? $onBack="clear_allsection();ShowObject('dvHeader',true,'');ShowObject('dvHeadResult',true,'');load_headlist();";?>
<input class="txt" type="hidden" id="TemplateID" name="TemplateID" value=""/>
<div id="dvHeader" class="w-100 boxin">
	<div class="header">
    	<h3>
      	<span>Template Management</span> 
      	<span id="spHeadSearch">&nbsp;
    	  <input class="btnTH" id="btnListRefresh" type="button" value="refresh" onClick="load_headlist();">&nbsp;
        <input class="txt" id="txtSearch" type="text" value="" size="20">&nbsp;
        <input class="btnTH" id="btnSearch" type="button" value="search" onClick="table_search('showHead_Data',1,'txtSearch');">
<!--    <input id="btnEdit" type="button" value="edit" onClick="layout_loadmenu(parseInt($('#txtSearch').val()));"> -->
				</span>
				<span style="float:right; margin-top:15px; margin-right:30px;">
        <input class="btnTH" type="button" id="btnHeadAdd" value=" &nbsp; New &nbsp; " onClick="layout_add_new();" title="Add Template"/>
        </span>
        </h3>
    </div>
	<div><?		if ($DEBUG) {		echo "request<pre>"; print_r($_REQUEST); echo "</pre>"; } ?></div>

    <div id="dvHeadResult">
    <table cellspacing="0">
        <thead>
          <tr>
            <th>Template Name</th>
            <th>Layout Name</th>
            <th width="10%" colspan="5" nowrap>Operation</th>
          </tr>
      	</thead>
      	<tbody id="showHead_Data">
      	</tbody>
    </table>
    </div>
</div>
<div id="dvHeadAddForm" class="top15">
<span><input class="btnJQR" type="button" id="btnHeadRefresh" onClick="<?=$onBack;?>" value=" Back "/></span><br>
<? 
	$Url=sprintf("%s?FName=%s&TID=%s&TName=%s&BName=%s",'popup_template.php','formLayoutAdd','TemplateID','txtTemplateAdd','btnSubmit');
	$Option=sprintf("width=%d,height=%d,location=no,menubar=no,resizeable=no,scrollbars=no,status=no,toolbar=no",710,280);
	$onAddLayout=sprintf("window.open('%s','_template','%s');",$Url,$Option);
	
	$sSubmit="You can not change template Layout. Do you confirm to add this layout ?";
	$sAjax="layout_add(document.getElementById('txtName').value";
	$sAjax.=",document.getElementById('TemplateID').value";
	$sAjax.=",document.getElementById('txtTemplateAdd').value);";
//	$onSubmit=sprintf("if (confirm('%s')) {document.getElementById('%s').submit(); }",$sSubmit,'formLayoutAdd');
	$onSubmit=sprintf("if (confirm('%s')) {%s }",$sSubmit,$sAjax);

	$onReset="document.forms['formLayoutAdd'].reset();EnableObject('btnSubmit',false)";
?>
	<div class="w-50 top15">
    	<div class="well boxin" id="tblFormAdd">
            <form id="formLayoutAdd" name="formLayoutAdd" method="post">
            <input type="hidden" id="btnLayoutAdd" name="btnLayoutAdd" value="LayoutAdd"/>
            <div class="left head">Template Information</div>
            <div class="dvtr top15">
                <div class="dvtd_left w-25 top5">Template Name : </div>
              	<div class="dvtd_right"><input type="text" class="txt" id="txtName" name="txtName" size="35" maxlength="64"/></div>
            </div>
           	<div class="dvtr top5">
            	<div class="dvtd_left w-25 top5">Layout Name : </div>
				<div class="dvtd_right">
              		<input type="text" id="txtTemplateAdd" class="txt" name="txtTemplateAdd" size="35" maxlength="64" readonly/></div>
                    <div class="demo dvtd_left"><input type="button" id="btnSelTemplate" value="Select Layout" onClick="layout_show_template();"/></div>			
           	</div>
           	<div class="dvtr top15 demo">
            	<div class="dvtd_left w-50">
              		<input type="button" id="btnSelTemplateSave" name="btnSelTemplateSave" value="Save" onClick="<?=$sAjax;?>"/>
                </div>
                <div class="dvtd_right">
            		<input type="button" id="btnReset" value="Reset" onClick="<?=$onReset;?>"/>
           		</div>
            </div>
            </form>
		</div>
    </div>
</div>
<div id="dvLayout" class="top15">
<span><input class="btnJQR" type="button" id="btnHeadRefresh"  onClick="<?=$onBack;?>" value="Back"/></span>
<? 
$onView="window.open('');";
//$onChange="EnableObject('btnSaveLayout',false);";
$onChange.=sprintf("changeInputValue('txtLayoutName','%s',document.getElementById('txtLayoutName').value);",$sDisplay);
$onChange.="EnableObject('btnSaveLayout',! checkEqual('txtLayoutName','txtOldLayoutName'));";
$onSave="layout_head_edit(document.getElementById('HeadID').value,";
$onSave.="document.getElementById('txtLayoutName').value)";
?>
<div class="well boxin w-50">
    <div class="left head"><h3>Template Information</h3></div>
    <div class="dvtr top15">
        <div class="dvtd_left top5 w-25">Template Name : </div>
        <div class="dvtd_right demo">
        <input type="text" class="txt" id="txtLayoutName" name="txtLayoutName" size="32" maxlength="64" readonly disabled/>
        <span><input class="ui-button" type="button" id="btnLayoutName" value="Change Name" onClick="<?=$onChange;?>" />
        <input type="hidden" id="txtOldLayoutName" readonly disabled>
        <input type="button" id="btnSaveLayout" class="btnJQR" value="Save" onClick="<?=$onSave;?>" disabled/></span>
        </div>
    </div>
    <div class="dvtr top5">
    <? $onLayoutAdd=sprintf("layout_popup(document.getElementById('TemplateID').value);"); ?>
    <? $onLayoutAdd=sprintf("layout_popup(document.getElementById('HeadID').value);"); ?>
        <div class="dvtd_left top5 w-25">Layout Name : </div>
        <div class="dvtd_right demo">
        <input type="text" class="txt" id="txtTemplate" name="txtTemplate" size="32" maxlength="64" readonly disabled/>
        <input class="ui-button" type="button" id="btnLayoutView" value="View" onClick="<?=$onLayoutAdd;?>" />
        </div>
    </div>
</div>
<div class="boxin w-100" style="margin-top:10px;" id="LineMenu"></div>
<div id="LineData" class="boxin w-100">
  <div class="header">
      <h3><span class="spTypeName">Image</span><span> Playlist</span></h3>
    </div>
    <div id="dvHeaderFeedShow" class="boxno w-90" style="margin-left:5%; margin-top:10px;">
        <marquee id="FeedAll" direction="left" vspace="5" hspace="5">Example Feeding. 1 2 3 4 5 6 7 8 9 0</marquee>
    </div>
    <div id="dvPlaylistHeader" class="top10" style="margin-bottom:10px;">
    <?
        $onChange="EnableObject('btnHeadSave',false);";
        $sDisplay="Do you Comfirm to Change Playlist Name?";
        $onChange.=sprintf("changeInputValue('txtHeadName','%s',document.getElementById('txtHeadName').value);",$sDisplay);
        $onChange.="EnableObject('btnHeadSave',! checkEqual('txtHeadName','txtOldName'));";
        
        $onReset="EnableObject('btnHeadSave',false);";
        $onReset.="setObjValue('txtOldName','txtHeadName');";
        ?>
    &nbsp; &nbsp;  Playlist Name :
    <input class="txt" size="40" type="text" id="txtHeadName" name="txtHeadName" readonly/>
    <input class="txt" size="40" type="hidden" id="txtOldName" name="txtOldName" />
    <span class="demo">
    <input type="button" id="btnHeadChange" value="Change" onClick="<?=$onChange;?>">
    <input class="btnJQR" type="button" id="btnHeadSave" value="Save" onClick="playlist_head_save('HeadID','txtHeadName');" disabled>
    <input type="button" id="btnHeadReset" value="Reset" onClick="<?=$onReset;?>">
    <input type="button" id="Playlist_Option" value="Playlist Option" onClick="playlist_head_option('HeadID','TypeID');"/>
    
    <span id="spBtnFeedShow">
      <input id="btnFeedShow" class="ui-button-text" type="button" value="Feed Preview" onClick="feed_head_preview('FeedAll',document.getElementById('HeadID').value);"/>
    </span>
    </span>
</div>
<div id="dvHeaderPlaylist" class="header left">
  <h3><span class="spTypeName">Flash</span><span> Playlist Items</span>
  <span class="demo" style="margin-left:20">
  <input id="btnHeaderRefresh" class="ui-button-text" type="button" value="Refresh" onClick="playlist_refresh(document.getElementById('HeadID').value);"/>
  <input id="btnHeaderLibrary" class="ui-button-text" type="button" value="Library" onClick="show_file_exists();"/>
  <input id="btnHeaderPlaylist" class="ui-button-text" type="button" value="Playlist Exists" onClick="show_playlist_exists();"/>
  <input id="TypeID" type="hidden" value="3" size="2"/><input id="HeadID" type="hidden" size="2"/>
  <input id="PlayListID" type="hidden" size="2"/><input id="LineID" type="hidden"  size="2"/>
  </span></h3>
</div>
    <table cellspacing="0" class="w-100">
    <tr><th colspan="2">
<div id="dvLineFeedShow" class="boxno w-90" style="margin:10px 0 10px 5%;">
    <marquee id="feedLinePreview" direction="left" vspace="5" hspace="5">Example Feeding. 1 2 3 4 5 6 7 8 9 0</marquee>
</div>
    </th>
    </tr>
    <tbody id="tblLine_Result">
      <tr>
        <td class="w-80 center"></td>
        <td class="w-20 center">
          <div class="dvtr top5 w-120">
             <div class="dvtd_left w-20 center"><img class="preview" src="images/icons/preview.png" title="preview"></div>
             <div class="dvtd_left w-20 center"><img class="preview" src="images/icons/down.png" title="down"></div>
             <div class="dvtd_left w-20 center"><img class="preview" src="images/icons/up.png" title="up"></div>
             <div class="dvtd_left w-20 center"><img class="preview" src="images/icons/edit.png" title="edit"></div>
             <div class="dvtd_left w-20 center"><img class="preview" src="images/icons/delete.png" title="delete"></div>
             <div class="dvtd_left w-20 center"><img class="preview" src="images/icons/options.png" title="option"></div>
          </div>
        </td>
      </tr>
    </tbody>
    </table>
  </div>
</div>
<script type="text/javascript">
	$(function() {
		$('#dvSelectFlash').dialog({ autoOpen: false , show: "blind", hide: "explode"});
		$('#dvSelectFlash').dialog("option","width",900);
		$('#dvSelectFlash').dialog("option","height",275);
		$('#dvSelectFlash').dialog("option","resizable",false);
	});
</script>
<div id="dvSelectFlash" title="Selection Exists Flash.">
<div id="dvSelectFileItem" style="min-height:50px; height:190px; overflow:auto">
<table cellspacing="0">
  <tbody id="tblSelectFlash_Body">
  <tr><td><input type="radio"></td><td>111111111111111111111111111111111111111111111</td></tr>
  </tbody>
</table>
</div>
<div class="pagination">
	<div class="demo dvtr">
        <div class="dvtd_left" style="margin-top:10px;">
          <span id="spHeadSearch" style="padding-right:20px;">
          <input class="txt" id="txtFlashSearch" type="text" value="" size="20">
          <input class="btnJQR" id="btnFlashSearch" type="button" value="search" onClick="table_search('tblSelectFlash_Body',3,'txtFlashSearch');">
  <!--    <input id="btnEdit" type="button" value="edit" onClick="layout_loadmenu(parseInt($('#txtSearch').val()));"> -->
          </span>
        </div> 
    <div class="dvtd_right" style="float:right; margin-right:5px;">
    
    <input style="margin-top:13px;" class="txt" type="hidden" size="20" id="txtExistFlash" readonly/>
    <span class="set_font_table">Parameter : </span><input style="margin-top:13px;" class="txt" type="text" id="txtPara" value='' size="20"/>
    <input type="hidden" id="SelectFlashID" value='' size="4"/>
    <input type="button" class="btnJQR" style="margin-top:13px;" id='btnSelectFlash' value="Selected" disabled onClick="layout_exist_file();"/>
    </div>
	</div>
</div>
</div>
<script type="text/javascript">
	$(function() {
		$('#dvSelectFile').dialog({ autoOpen: false , show: "blind", hide: "explode"});
		$('#dvSelectFile').dialog("option","width",900);
		$('#dvSelectFile').dialog("option","height",400);
		$('#dvSelectFile').dialog("option","resizable",false);
	});
</script>
<div id="dvSelectFile" title="Selection Exists File.">
    <div id="dvSelectFileItem" style="height:300px; overflow:auto">
    <table cellspacing="0">
      <tbody id="tblSelectFile_Body" >
      <tr><td><input type="radio"></td><td>111111111111111111111111111111111111111111111</td></tr>
      </tbody>
    </table>
    </div>
		<div class="pagination">
      <div class="demo dvtr">
        <div class="dvtd_left" style="margin-top:10px;">
          <span id="spHeadSearch" style="padding-right:20px;">
          <input class="txt" id="txtFileSearch" type="text" value="" size="20">
          <input class="btnJQR" id="btnFileSearch" type="button" value="search" onClick="table_search('tblSelectFile_Body',3,'txtFileSearch');">
  <!--    <input id="btnEdit" type="button" value="edit" onClick="layout_loadmenu(parseInt($('#txtSearch').val()));"> -->
          </span>
        </div> 
        <div class="dvtd_right" style="margin-top:10px; padding-right:5px; float:right">
          <input class="text" style="margin-top:13px;" type="hidden" size="72" id="txtExistFile"/>
          <input type="hidden" id="SelectFileID" value='' size="4"/>
          <input type="hidden" id="SelectFileCount" value='' size="4"/>
          <input type="button" id='btnSelectFile' class="btnJQR" value="Selected" disabled onClick="layout_exist_file();"/>
        </div>
      </div>
    </div>
</div>

<script type="text/javascript">
	$(function() {
		$('#dvSelectPlaylist').dialog({ autoOpen: false , show: "blind", hide: "explode"});
		$('#dvSelectPlaylist').dialog("option","width",900);
		$('#dvSelectPlaylist').dialog("option","height",275);
		$('#dvSelectPlaylist').dialog("option","resizable",false);
	});
</script>
<div id="dvSelectPlaylist" title="Selection Exists Playlist.">
<div id="dvSelectPlaylistItem" style="min-height:50px; height:190px; overflow:auto">
<table cellpadding="0" cellspacing="0">
  <tbody id="tblSelectPlaylist_Body">
  <tr><td><input type="radio"></td><td>111111111111111111111111111111111111111111111</td></tr>
  </tbody>
</table>
</div>

<div class="pagination">
	<div class="demo dvtr">
    <div class="dvtd_left" style="margin-top:10px;">
      <span id="spHeadSearch" style="padding-right:20px;">
      <input class="txt" id="txtPlaylistSearch" type="text" value="" size="20">
      <input class="btnJQR" id="btnPlaylistSearch" type="button" value="search" onClick="table_search('tblSelectPlaylist_Body',2,'txtPlaylistSearch');">
<!--    <input id="btnEdit" type="button" value="edit" onClick="layout_loadmenu(parseInt($('#txtSearch').val()));"> -->
      </span>
    </div> 
    <div class="dvtd_right">
      <input class="txt" style="margin-top:13px;" type="hidden" size="72" id="txtExistPlaylist"/>
  		<input type="hidden" id="SelectPlaylistID" value='' size="4"/></div>
    <div class="dvtd_right" style="padding-top:13px; padding-right:5px; float:right;">
      <input type="button" class="btnJQR" id='btnSelectPlaylist' value="Selected" disabled onClick="layout_exist_playlist();"/>
    </div>
	</div>
</div>

<script type="text/javascript">
	$(function() {
		$('#dvOption').dialog({ autoOpen: false , show: "blind", hide: "explode"});
		$('#dvOption').dialog("option","width",700);
		$('#dvOption').dialog("option","height",205);
		$('#dvOption').dialog("option","resizable",false);
	});
</script>
<div id="dvOption" title="Playlist Option Selection">
<table>
  <tbody id="tblOption_Body">
  <tr><td><input type="radio"></td><td>111111111111111111111111111111111111111111111</td></tr>
  </tbody>
</table>
</div>

<script type="text/javascript">
	$(function() {
		$('#dvItemOption').dialog({ autoOpen: false , show: "blind", hide: "explode"});
		$('#dvItemOption').dialog("option","width",700);
		$('#dvItemOption').dialog("option","height",205);
		$('#dvItemOption').dialog("option","resizable",false);
	});
</script>
<div id="dvItemOption" title="Item Option Selection">
<table>
  <tbody id="tblItemOption_Body">
  <tr><td><input type="radio"></td><td>111111111111111111111111111111111111111111111</td></tr>
  </tbody>
</table>
</div>

<script type="text/javascript">
	$(function() {
		$('#dvSelectLayout').dialog({ autoOpen: false , show: "blind", hide: "blind"});
		$('#dvSelectLayout').dialog("option","width",900);
		$('#dvSelectLayout').dialog("option","height",285);
		$('#dvSelectLayout').dialog("option","resizable",false);
	});
</script>
<div id="dvSelectLayout" title="Layout Selection">
	<div class="boxin">
    <div id="dvSelectPlaylistItem" style="min-height:50px; max-height:185px; overflow:auto">
    <table cellspacing="0">
      <tbody id="tblSelectLayout_Body">
      <tr><td><input type="radio"></td><td>111111111111111111111111111111111111111111111</td></tr>
      </tbody>
    </table>
    </div>
    <div class="pagination">
      <div class="demo dvtr">
        <div class="dvtd_left" style="margin-top:10px;">
          <span id="spLayoutSearch" style="padding-right:20px;">
          <input class="txt" id="txtLayoutSearch" type="text" value="" size="20">
          <input class="btnJQR" id="btnLayoutSearch" type="button" value="search" onClick="table_search('tblSelectLayout_Body',2,'txtLayoutSearch');">
    <!--    <input id="btnEdit" type="button" value="edit" onClick="layout_loadmenu(parseInt($('#txtSearch').val()));"> -->
          </span>
        </div> 
        <div class="dvtd_right">
          <input  class="txt" type="hidden" size="40" id="txtExistLayout"/>
          <input type="hidden" id="SelectLayoutID" value='' size="4"/>
          </div>
        <div class="dvtd_right" style="padding-top:13px; padding-right:5px; float:right;">
    			<input type="button" id='btnSelectLayout' class="btnJQR" value="Selected" disabled onClick="layout_select_template(parseInt(document.getElementById('SelectLayoutID').value),document.getElementById('txtExistLayout').value);"/>
        </div>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
	$(function() {
		$('#dvAddText').dialog({ autoOpen: false });
		$('#dvAddText').dialog("option","width",900);
		$('#dvAddText').dialog("option","height",100);
		$('#dvAddText').dialog("option","resizable",false);
	});
</script>
<div id="dvAddText" title="Add New Feed Item.">
<div class="demo">
<input type="text" size="72" id="txtNewFeed"/>
<input type="hidden" id="FeedID" value='' size="2"/>
<!--<input type="text" id="LineID" value='' size="2"/> -->
<input type="button" id='btnFeedAdd' value="Save" onClick="feed_line_add();"/>
</div>
</div>
<script type="text/javascript">
	$(function() {
		$('#dvEditText').dialog({ autoOpen: false });
		$('#dvEditText').dialog("option","width",900);
		$('#dvEditText').dialog("option","height",100);
		$('#dvEditText').dialog("option","resizable",false);
	});
</script>
<div id="dvEditText" title="Edit Feed Item.">
<div class="demo">
<input type="text" size="72" id="txtFeed_Edit" value=""/>
<input type="checkbox" id="chkFeedOnly" value="ThisOnly"/>This Only
<input type="hidden" id="FeedID_Edit" value='' size="2"/>
<input type="hidden" id="LineID_Edit" value='' size="2"/>
<input type="button" id='btnFeedEdit' value="Save" onClick="feed_line_save();"/>
</div>

<script type="text/javascript">
	$(function() {
		$('#dvLineOption').dialog({ autoOpen: false });
		$('#dvLineOption').dialog("option","width",500);
		$('#dvLineOption').dialog("option","height",255);
		$('#dvLineOption').dialog("option","resizable",false);
	});
</script>
<div id="dvLineOption" title="Item Option Selection">
<table>
  <tbody id="tblLineOption_Body">
  <tr><td></td><td><input type="radio"></td></tr>
  </tbody>
</table>
</div>

<script type="text/javascript">
	$(function() {
		$('#dvStreamFile').dialog({ autoOpen: false });
		$('#dvStreamFile').dialog("option","width",900);
		$('#dvStreamFile').dialog("option","height",270);
		$('#dvStreamFile').dialog("option","resizable",false);
	});
</script>
<div id="dvStreamFile" title="Select Exists Streaming">
<div id="dvStreamFileItem" style="min-height:50px; height:185px; overflow:auto">
<table cellspacing="0">
  <tbody id="tblStreamFile_Body">
  <tr><td><input type="radio"></td><td>111111111111111111111111111111111111111111111</td>
  </tbody>
</table>
</div>
<div class="pagination">
	<div class="dvtr">
        <div class="dvtd_left w-25" style="margin-top:15px;">You select : </div>
        <div class="demo dvtd_right" style="padding-top:13px;">
        	<input type="text" class="txt" size="50" id="txtExistStream"/>
			<input type="hidden" id="SelectStreamID" value='' size="4"/>
        </div>
        <div class="demo dvtd_right" style="padding-top:13px;">
			<input type="button" id='btnSelectStream' value="Selected" disabled onClick="playlist_exist_stream();"/>
        </div>
   	</div>
</div>
<script type="text/javascript">
	$(function() {
		$('#dvStreamPlaylist').dialog({ autoOpen: false });
		$('#dvStreamPlaylist').dialog("option","width",900);
		$('#dvStreamPlaylist').dialog("option","height",270);
		$('#dvStreamPlaylist').dialog("option","resizable",false);
	});
</script>
<div id="dvStreamPlaylist" title="Selection Exists Playlist.">
<div id="dvStreamPlaylistItem" style="min-height:50px; height:185px; overflow:auto">
<table cellspacing="0">
  <tbody id="tblStreamPlaylist_Body">
  <tr><td><input type="radio"></td><td>111111111111111111111111111111111111111111111</td>
  </tbody>
</table>
</div>
<div class="pagination">
	<div class="dvtr">
        <div class="dvtd_left w-25" style="margin-top:15px;">You select : </div>
        <div class="demo dvtd_right" style="padding-top:13px;">
        	<input type="text" class="txt" size="50" id="txtExistPlaylist"/>
			<input type="hidden" id="StreamPlaylistID" value='' size="4"/>
        </div>
        <div class="demo dvtd_right" style="padding-top:13px;">
			<input type="button" id='btnStreamPlaylist' value="Selected" disabled onClick="playlist_exist_stream_playlist();"/>
		</div>
   	</div>
</div>
</div>

</body>
</head>
