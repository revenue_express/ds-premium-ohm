<? 
	$DEBUG=FALSE;
	$TypeID=8;
	include("./includes/db_config.inc.php");
	include("./functions/library.func.php");
?>
<html>
<head>
<?	include("./javascript.php"); ?>
<script type="text/javascript" src="./js/library_stream.js"></script>
<script type="text/javascript">
	$('document').ready(function() {
		stream_list();
	});
</script>
</head>
<body>
<div class="dvHeader">Stream Library</div>

<div id="dvForm">
<div class="w-100 top15"><!-- .altbox for alternative box's color -->
	<div class="boxin">
    <div class="header">
        <h3>Media Library (Streaming) <button onClick="stream_list();">Back</button> </h3>
    </div>
    <table cellspacing="0">
      <thead>
        <tr>
          <th style="width:50px;">Protocol</th>
          <th>Address</th>
          <th style="width:100px;">Port</th>
          <th style="width:150px;">Path 1</th>
          <th style="width:150px;">Path 2</th>
          <th style="width:120px;">Action</th>
        </tr>
      </thead>
      <tr>
      <td>
        <select id="selProtocol" name="selProtocol">
          <option value="http">http</option>
          <option value="rtsp">rtsp</option>
          <option value="mms">mms</option>
        </select>
      </td>
      <td><input type="text" name="txtAddress" id="txtAddress" size="60" maxlength="255"></td>
      <td><input type="text" name="txtPort" id="txtPort" size="5" maxlength="8"></td>
      <td><input type="text" name="txtPara01" id="txtPara01" size="16" maxlength="64"></td>
      <td><input type="text" name="txtPara02" id="txtPara02" size="16" maxlength="64"></td>
      <td>
      <input id="btnSave" type="button" value="Save" onClick="stream_save('StreamID','selProtocol','txtAddress','txtPort','txtPara01','txtPara02');">
      <input id="btnReset" type="reset" value="Reset">
      <input id="StreamID" type="hidden" value="">
      </td>
      </tr>
  	</table>
	</div>
</div>
</div>

<div id="dvResult">
<div class="w-100 top15"><!-- .altbox for alternative box's color -->
	<div class="boxin">
        <div class="header">
            <h3>Media Library (Streaming) 
						<button onClick="stream_show_add();">Add New</button>
            <input id="btnListRefresh" type="button" value="refresh" onClick="stream_list();">
            <input id="txtSearch" type="text" value="" size="10"><input id="btnSearch" type="button" value="search" onClick="table_search('tblList_Body',1,'txtSearch');">
            </h3>
        </div>
        <table cellspacing="0">
          <thead>
            <tr>
          		<th style="width:50px;">Protocol</th>
              <th>Address</th>
              <th style="width:100px;">Port</th>
              <th style="width:150px;">Path 1</th>
              <th style="width:150px;">Path 2</th>
               <th width="9%">Action</th>
            </tr>
          </thead>
        	<tbody id="tblList_Body">
          	<tr id="tr_NoRecord"><td colspan="6" align="center">No Record Found.</td></tr>
        	</tbody>
      </table>
	</div>
</div>
<!--
<br clear="all">
<div class="box box-100 altbox">
<div class="boxin">
<div class="content">
<table border="1">
<thead>
<tr><th>Protocol</th><th>Address</th><th>Port</th><th>Parameter 1</th><th>Parameter 2</th>
<th colspan="3" align="center">
<img src="./images/icons/refresh_24x24.png" width="24" height="24" onClick="stream_list();">
<img src="./images/icons/add_24x24.png" width="24" height="24" onClick="stream_show_add();">
</th>
</tr>
</thead>
<tbody id="tblStream_Result">
<tr>
	<td>Protocol</td>
  <td>Address</td>
  <td>Port</td>
  <td>Parameter 1</td>
  <td>Parameter 2</td>
	<td width="32"><img class=" ui-icon ui-icon-search" onClick="feed_preview('txtReview','12345798','UTF-8');" title="view"></td>
	<td width="32"><img class=" ui-icon ui-icon-pencil" onClick="feed_preview('txtReview','12345798','UTF-8');" title="edit"></td>
	<td width="32"><img class=" ui-icon ui-icon-cancel" onClick="feed_preview('txtReview','12345798','UTF-8');" title="delete"></td>
</tr>
</tbody>
</table>
</div>
</div>
</div>
-->
</div>
</body>
</html>