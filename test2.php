<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html>
<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />     
    <title>Log in · Info Express</title>
    <script type="text/javascript" src="js/jquery-1.8.0.min.js"></script>
	<script type="text/javascript" src="js/jquery-ui-1.8.23.custom.min.js"></script>
    <script>
        $(function() {
            $( "input:submit, button", ".demo" ).button();
			$( "#tabs" ).tabs();
			$( "#datepicker" ).datepicker();
        });
		
		$.fx.speeds._default = 1000;
		$(function() {
			$( "#dialog" ).dialog({
				autoOpen: false,
				show: "blind",
				hide: "explode"
			});
	
			$( "#opener" ).click(function() {
				$( "#dialog" ).dialog( "open" );
				return false;
			});
		});
    </script>
    <link rel="stylesheet" href="css/custom-theme/jquery-ui-1.8.23.custom.css">
    <link rel="stylesheet" href="css/StyleSheet.css" type="text/css" />
</head>
<body>
	<form class="table" action="_test3.php" method="post">
		<input class="btnSky" type="submit" value="หน้าหลัก" /> ยินดีต้อนรับเข้าสู่หน้าหลัก
    </form>
    <br /><br />
	<!-- 
       	box-60 for width 60%;
    	boxin for shadow
    -->
    <div class="w-60 boxin">   
        <div class="header">
            <h3>Media Library (Image)</h3>
            <div><input class="btnHeader" style="margin-left:200px;" type="submit" value="back" /></div>
        </div>
        <table cellspacing="0">
            <thead>
                <tr>
                    <th>Display Name</th>
                    <th>Original Name</th>
                    <th>Sys Name</th>
                    <th>Size(KB)</th>
                    <th><input class="btnTh" type="submit" value="Edit" /></th>
                </tr>
            </thead>
            <tbody>
                <tr class="first">
                    <td>DTAC 111</td>
                    <td>Dtac_Portriat.jpg</td>
                    <td>20120717141128285.jpg</td>
                    <td>1,712,934</td>
                    <td>
                    <div class="dvtr">
                        <div class="dvtd_left w-30">
                        	<img class="btnPre" title="preview">
                        </div>
                        <div class="dvtd_right w-30 center">
                        	<img class="btnEdit" title="Edit">
                        </div>
                        <div class="dvtd_right">
                        	<img class="btnDel" title="Delete">
                        </div>
                    </div>
                    </td>
                </tr>
                <tr class="even">
                    <td>DTAC Wifi</td>
                    <td>Dtac_Wifi.jpg</td>
                    <td>20120717141956458.jpg</td>
                    <td>1,208,062</td>
                    <td>
                   	<div class="dvtr">
                        <div class="dvtd_left w-30">
                        	<img class="btnPre" title="preview">
                        </div>
                        <div class="dvtd_right w-30 center">
                        	<img class="btnEdit" title="Edit">
                        </div>
                        <div class="dvtd_right">
                        	<img class="btnDel" title="Delete">
                        </div>
                    </div>
                    </td>                
                </tr>
                <tr>
                    <td>DTAC Atm</td>
                    <td>Dtac_Atm.jpg</td>
                    <td>20120717185357285.jpg</td>
                    <td>1,856,822</td>
                    <td align="center">No</td>                
                </tr>
            </tbody>
        </table>

        <div class="pagination">
            <ul>
                <li><a href="#">previous</a></li>
                <li><a href="#">1</a></li>
                <li><a href="#">2</a></li>
                <li><strong>3</strong></li>
                <li><a href="#">4</a></li>
                <li><a href="#">5</a></li>
                <li><a href="#">next</a></li>
            </ul>
    	</div>
    </div>
    
    
    <br />
    <br />
    
        <div class="w-60 boxin">   
        <table cellspacing="0">
            <thead>
                <tr>
                    <th class="left">Display Name</th><!-- Plase call case left of use left radius -->
                    <th>Original Name</th>
                    <th>Sys Name</th>
                    <th>Size(KB)</th>
                    <th class="right">In Used</th><!-- Plase call case left of use right radius -->
                </tr>
            </thead>
            <tbody>
                <tr class="first">
                    <td>DTAC 111</td>
                    <td>Dtac_Portriat.jpg</td>
                    <td>20120717141128285.jpg</td>
                    <td>1,712,934</td>
                    <td>
                    <div class="dvtr">
                        <div class="dvtd_left w-30">
                        	<img class="btnPre" title="preview">
                        </div>
                        <div class="dvtd_right w-30 center">
                        	<img class="btnEdit" title="Edit">
                        </div>
                        <div class="dvtd_right">
                        	<img class="btnDel" title="Delete">
                        </div>
                    </div>
                    </td>
                </tr>
                <tr class="even">
                    <td>DTAC Wifi</td>
                    <td>Dtac_Wifi.jpg</td>
                    <td>20120717141956458.jpg</td>
                    <td>1,208,062</td>
                    <td>
                   	<div class="dvtr">
                        <div class="dvtd_left w-30">
                        	<img class="btnPre" title="preview">
                        </div>
                        <div class="dvtd_right w-30 center">
                        	<img class="btnEdit" title="Edit">
                        </div>
                        <div class="dvtd_right">
                        	<img class="btnDel" title="Delete">
                        </div>
                    </div>
                    </td>                
                </tr>
                <tr>
                    <td>DTAC Atm</td>
                    <td>Dtac_Atm.jpg</td>
                    <td>20120717185357285.jpg</td>
                    <td>1,856,822</td>
                    <td align="center">No</td>                
                </tr>
            </tbody>
        </table>
    </div>
    
    <br />
    
    <div>
    	<div class="btnBack"></div>
    </div>
    <br />
    
    <div>
    	<div style="float:left" class="btnPre"></div>
        <div style="float:left; margin-left:10px;" class="btnEdit"></div>
        <div style="float:left; margin-left:10px;" class="btnRes"></div>
        <div style="float:left; margin-left:10px;" class="btnCopy"></div>
        <div style="float:left; margin-left:10px;" class="btnDel"></div>
    </div>
    
    <br />
    <br />
    
    <div class="dvHeader">Title : Media Library (Image)</div>
    
    <br />
    <br />
        
    <div class="w-50 demo">
        <div id="tabs">
            <ul>
                <li><a href="#tabs-1">Nunc tincidunt</a></li>
                <li><a href="#tabs-2">Proin dolor</a></li>
                <li><a href="#tabs-3">Aenean lacinia</a></li>
            </ul>
            <div id="tabs-1">
                <p>Tab of JqueryUI</p>
            </div>
            <div id="tabs-2">
                <p>Tab of JqueryUI</p>
            </div>
            <div id="tabs-3">
                <p>Tab of JqueryUI</p>
            </div>
        </div>
    </div>

    <br />
    <br />
    
    <div class="demo"><!-- Fix Class="demo", Becuase Use CSS of jqueryUI -->
    	<input type="submit" id="btnSubmit" name="btnSubmit" value="Button"/>
        <br />
        <br />
        <div id="dialog" title="Basic dialog">
            <p>This is an animated dialog which is useful for displaying information. The dialog window can be moved, 
            	resized and closed with the 'x' icon.</p>
        </div>
        <button id="opener">Open Dialog</button>

	</div><!-- End demo -->
   
    <br />
    <br />
    
    <div class="demo"><input type="text" id="datepicker"></div><!-- End demo -->

    
    <br />
    <br />
   
    <div class="txt_blue">Text Blue</div>
   
    <br />
    <br />
   
    <div class="txt_red">Text Red</div>
    
    <br />
    <br />
    <div class="w-50"> 
    	<div class="well">
        	<div class="dvtr canter"><h3>.: Form Input Data :.</h3></div>
            <div class="dvtr top15">
            	<div class="dvtd_left w-50 top5">Name - Surname : </div>
                <div class="dvtd_right"><input class="txt" size="20" type="text" id="some1" name="some1" /></div>
            </div>
            <div class="dvtr">
            	<div class="dvtd_left w-50 top5">Age : </div>
                <div class="dvtd_right"><input class="txt" size="10" type="text" id="some2" name="some2" /></div>
            </div>
            <div class="dvtr top15">
            	<div class="dvtd_left w-30">w 30, Default Right |</div>
                <div class="dvtd_right w-40 center">w 40, Default left But setcenter </div>
                <div class="dvtd_right">| w last, Default left</div>
            </div>
            <div class="dvtr top15">
            	<div class="dvtd_left w-50"><input class="btnSky" type="submit" value="Save" /></div>
                <div class="dvtd_right"><input class="btnSky" type="submit" value="Reset" /></div>
            </div>
        </div>
    </div>
    
    <br />
    <br />
    
</body>
</html>