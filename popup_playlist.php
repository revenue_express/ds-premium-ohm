<? 
	include("./includes/db_config.inc.php");
	include("./includes/sys_config.inc.php");
	include("./functions/playlist.func.php");
	$DEBUG=FALSE;
?>
<html>
<head>
<script type="text/javascript" src="js/jquery-1.7.2.min.js"></script>
<script type="text/javascript" src="js/jquery-ui-1.8.21.custom.min.js"></script>
<script type="text/javascript" src="./js/checkValid.js"></script>
<script type="text/javascript" src="./js/signage.js"></script>
<link rel="stylesheet" href="./css/display.css">
</head>
<body>
<?
//	$TypeData=array("Name"=>'image',"Path"=>"images");
//	$TypeData=json_decode(file_type_list($TypeID),true);
	if ($DEBUG) { echo "<li>Request<pre>"; print_r($_REQUEST); echo "</pre>"; }
	$bSuccess=false;
	$sScript="";
?>
<div class="dvHeader">Template Library</div>
<?	
	$ItemList=json_decode(template_head_list($ComID),true);
//		$ItemList=array();
//	echo "Count(Data) =".count($data);
//	$FileList=json_decode($data);
//	echo "Count(FileList) =".count($FileList);
//	echo "<pre>";
//	print_r($FileList);
//	echo "</pre>";
//	for ($iCount=0; $iCount < count($FileList); $iCount++) {
//		$arrData=array();
//		$arrData=$FileList[$iCount];
//		echo "<pre>";
//		print_r($arrData);
//		echo "</pre>";
//		echo $arrData['file_id'];
//	}
?>
<div id="dvImageMain" align="center">
	<div id="dvImageFile" style="height:150px; width:700px; overflow:auto;">
  <table border="1" width="100%">
  	<thead>
    <tr><th></th>
    <th nowrap>Template Name</th><th nowrap>Display Size</th><th nowrap>Item(s)</th></tr>
    </thead>
    <tbody>
<?
	if (count($ItemList) > 0) {
		for ($iCount=0;$iCount<count($ItemList);$iCount++) {
//			$sFileName=sprintf("./data/%s/%s",$TypeData[0]['type_path'],$FileList[$iCount]['file_sname']);
//			$onViewClick=sprintf("window.open('%s','_view');",$sFileName);
			$onRadioClick=sprintf("setValue('FileName','%s','formPopUp');",$ItemList[$iCount]['tmph_name']);
			$onRadioClick.=sprintf("setValue('FileID',%d,'formPopUp');",$ItemList[$iCount]['tmph_id']);
			$onRadioClick.=sprintf("EnableObject('btnPopUpSave',true,'formPopUp');");
//			$sDisplay="Do you confirm to insert this file to playlist?";

//			$onSaveClick=sprintf("showConfirm('%s','formPopUpLibrary','btnPopUpSave','submit','Save');",$sDisplay);
			$onSaveClick=sprintf("SetParentValue('%s','%s','FileID');",$FName,$TID);
			$onSaveClick.=sprintf("SetParentValue('%s','%s','FileName');",$FName,$TName);
			$onSaveClick.=sprintf("EnableParentObject('%s',true,'%s');",$BName,$FName);
			$onSaveClick.=sprintf("window.close();");

			$onResetClick="EnableObject('btnPopUpSave',false,'formPopUp');";
//			$onCloseClick=sprintf("window.opener.location.href=window.opener.location.href+'?PlayListID=%d';",$PlayListID);
//			$onCloseClick.="window.opener.location.replace(window.opener.location.href);";
			$onCloseClick="window.close();";
//			$onOptionClick="Toggles('tblBodyImageOption')";
//			$onDisplayChange=sprintf("setValue('Option_1',this.value,'formPopUpLibrary');");
//			$onInChange=sprintf("setValue('Option_2',this.value,'formPopUpLibrary');");
//			$onOutChange=sprintf("setValue('Option_3',this.value,'formPopUpLibrary');");
//			$onIntervalChange=sprintf("setValue('Option_4',this.value,'formPopUpLibrary');");
			$Selected="";
			if ($SID==$ItemList[$iCount]['tmph_id']) { 
				$Selected=" CHECKED";
				$FileName=$ItemList[$iCount]['tmph_name']; 
			}
?>
    <tr>
    <td width="20" align="center"><input type="radio" id="rdoFile" name="rdoFile" onClick="<?=$onRadioClick;?>"<?=$Selected;?>></td>
    <td width="80%"><?=$ItemList[$iCount]['tmph_name'];?></td>
    <td nowrap='nowrap'><?=sprintf("%d X %d",$ItemList[$iCount]['disp_width'],$ItemList[$iCount]['disp_height']);?></td>
<td align="right"><?=template_line_count($ItemList[$iCount]['tmph_id']);?></td>
    </tbody>
<?	} } else{ ?>
    <tr><td colspan="4" align="center">NOT FOUND ITEM</td></tr>		
<?	} ?>
  </table>
	</div>
  <br>
	<div id="dvDataResult">
	<form id="formPopUp" method="post">
  	<input type="hidden" id="FileID" name="FileID" value="">
    You select <input type="text" maxlength="80" id="FileName" name="FileName" size="55" value="<?=$FileName;?>"readonly disabled>
		<span id="formPopUpLibraryOption">
		<input type="hidden" id="Option_1" name="Option_1" value="">
		<input type="hidden" id="Option_2" name="Option_2" value="">
		<input type="hidden" id="Option_3" name="Option_3" value="">
		<input type="hidden" id="Option_4" name="Option_4" value="">
		<input type="hidden" id="Option_5" name="Option_5" value="">
		<input type="hidden" id="Option_6" name="Option_6" value="">
		<input type="hidden" id="Option_7" name="Option_7" value="">
		<input type="hidden" id="Option_8" name="Option_8" value="">
		<input type="hidden" id="Option_9" name="Option_9" value="">
    <input type="button" id="btnPopUpSave" value="Save" class="button" disabled onClick="<?=$onSaveClick;?>">
    <input type="reset" id="btnPopUpReset" value="Reset" class="button" onClick="<?=$onResetClick;?>">
    <input type="button" id="btnPopUpClose" value="Close" class="button" onClick="<?=$onCloseClick;?>">
    </span>
	</form>
	</div>
</div>

</body>
</html>