<ul class="dropdown">
    <li><a href="main3.php">Home</a></li>
    <li><a href="#">Status</a>
        <ul class="sub_menu">
             <li><a href="main3.php?menu=online_status">Online Status</a></li>
             <li><a href="main3.php?menu=monitor_status">Monitor</a></li>
        </ul>
    </li>
    <li><a href="#">Admin</a>
        <ul class="sub_menu">
            <li><a href="main3.php?menu=admin_groups">Group</a></li>
            <li><a href="main3.php?menu=admin_client">Client</a></li>
            <li><a href="main3.php?menu=admin_user">User</a></li>
            <li><a href="main3.php?menu=admin_module">Module</a></li>
        </ul>
    </li>
    <li><a href="#">Main Menu</a>
        <ul class="sub_menu">
             <li>
                <a href="#">Library</a>
                <ul>
                    <li><a href="main3.php?menu=library_images">Image Library</a></li>
                    <li><a href="main3.php?menu=library_videos">Video Library</a></li>
                    <li><a href="main3.php?menu=library_flashs">Flash Library</a></li>
                    <li><a href="main3.php?menu=library_audios">Audio Library</a></li>
                </ul>
             </li>
             <li><a href="main3.php?menu=template">Layout Manager</a></li>
             <li>
                <a href="#">Playlist Manager</a>
                <ul>
                    <li><a href="main3.php?menu=playlist_images">Image Playlist</a></li>
                    <li><a href="main3.php?menu=playlist_videos">Video Playlist</a></li>
                    <li><a href="main3.php?menu=playlist_flashs">Flash Playlist</a></li>
                    <li><a href="main3.php?menu=playlist_audios">Audio Playlist</a></li>
                </ul>
             </li>
             <li><a href="main3.php?menu=layout">Template Manager</a></li>
             <li><a href="main3.php?menu=schedule">Schedule Content</a></li>
             <li><a href="main3.php?menu=send">Send Approved</a></li>
             <li><a href="main3.php?menu=approve">Approve Content</a></li>
             <li>
                <a href="#">Report</a>
                <ul>
                    <li><a href="main3.php?menu=report_01">Report 01</a></li>
                    <li><a href="main3.php?menu=report_02">Report 02</a></li>
                    <li><a href="main3.php?menu=report_03">Report 03</a></li>
                </ul>
             </li>
        </ul>
    </li>
</ul>