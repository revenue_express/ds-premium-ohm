<? 
	include("./includes/db_config.inc.php");
	include("./includes/sys_config.inc.php");
	$DEBUG=FALSE;
?>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<script type="text/javascript" src="./js/checkValid.js"></script>
<script type="text/javascript" src="./js/signage.js"></script>
<script type="text/javascript" src="./js/admin_user.js"></script>
<script type="text/javascript" src="js/jquery-1.8.0.min.js"></script>
<script type="text/javascript" src="js/jquery-ui-1.8.23.custom.min.js"></script>
<script>
	$(function() {
		$( "input:submit, input:button, input:reset, a, button", ".demo" ).button();
		$( "a", ".demo" ).click(function() { return false; });
	});
</script>
<link rel="stylesheet" href="css/custom-theme/jquery-ui-1.8.23.custom.css">
<link rel="stylesheet" href="css/StyleSheet.css" type="text/css" />
<body>
<div id="dvChangePassword" class="w-50">
	<div class="well">
		<div class="dvtr center"><h3>Change Password</h3></div>
        <div class="dvtr top15">
        	<div class="dvtd_left w-50 top5">Name : </div>
          <div class="dvtd_right"><span id="spChangeName"><?=$USER['usr_name'];?></span></div>
       	</div>
				<div class="dvtr">
        	<div class="dvtd_left w-50 top5">Login : </div>
          <div class="dvtd_right"><span id="spChangeLogin"><?=$USER['usr_login'];?></span></div>
        </div>
				<div class="dvtr">
        	<div class="dvtd_left w-50 top5">Old Password : </div>
					<div class="dvtd_right"><input class="txt" id="txtOldPass" type="password" size="20" maxlength="127" onKeyUp="user_change_check();"></div>
        </div>
				<div class="dvtr">
        	<div class="dvtd_left w-50 top5">New Password : </div>
					<div class="dvtd_right"><input class="txt" id="txtNewPass1" type="password" size="20" maxlength="127" onKeyUp="user_change_strength();user_change_check();">
    <span id='spChangeStrength'>&nbsp; &nbsp; &nbsp;</span></div>
   			</div>
				<div class="dvtr">
        	<div class="dvtd_left w-50 top5">Confirm New : </div>
					<div class="dvtd_right"><input class="txt" id="txtNewPass2" type="password" size="20" maxlength="127" onKeyUp="user_change_samepass();user_change_check();">
    <span id='spChangeSame'>&nbsp; &nbsp; &nbsp;</span></div>
				</div>
				<div class="dvtr center top15">
					<input type="button" class="btnSky" id="btnChangeInfo" value="Save" onClick="user_change_password();" disabled>
					<input type="button" class="btnSky" id="btnChangeCancel" value="Reset">
					<input type="hidden" size="5" id="UserID" name="UserID" value="<?=$USER['usr_id'];?>">
				</div>
     </div>
</div>
</body>
</html>