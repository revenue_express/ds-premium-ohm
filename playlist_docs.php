<? 
	include("./includes/db_config.inc.php");
	include("./includes/sys_config.inc.php");
	$DEBUG=FALSE;
?>
<html>
<head>
<?	include("./javascript.php");?>
<script type="text/javascript" src="./js/playlist_doc.js"></script>
<script type="text/javascript" src="./js/playlist_head_option.js"></script>
<link rel="stylesheet" href="css/custom-theme/jquery-ui-1.8.23.custom.css">
<link rel="stylesheet" href="css/StyleSheet.css">
<script type="text/javascript">
	$('document').ready(function() {
		doc_head_list();
		
	});
	
	$(function(){
		$('#btnSelectFile, #btnSelectPlaylist').button();
		$('#txtExistFile,#txtExistPlaylist').height(15);
	});	
</script>
</head>
<body>
<div id="dvHeadResult" class="w-80 boxin">
	<div class="header">
  	<h3>Document Playlist</h3>
  </div>
  <table cellspacing="0">
  <thead>
  	<tr>
      <th class="w-10">Code</th>
      <th>Playlist Name</th>
      <th class="w-5">Item(s)</th>
      <th class="w-15">
      <input class="btnTH" type="button" value="Refresh" onClick="doc_head_list();"/>
      &nbsp; &nbsp;
      <input class="btnTH" type="button" value=" &nbsp;Add &nbsp;" onClick="playlist_head_add();"/>
      </th>
  	</tr>
  </thead>
  <tbody id="tblHead_Result">
    <tr>
      <td class="center">1</td>
      <td>2</td>
      <td>Item</td>
      <td class="center"><img class="preview" src="images/icons/edit.png" title="preview"></td>
      <td class="center"><img class="preview" src="images/icons/delete.png" title="preview"></td>
    </tr>
  </tbody>
  </table>
</div>
<div id="dvLineResult" class="boxin w-80">
	<div class="header">
  	<h3>Document Playlist</h3><input class="btnHeader" style="margin-left:165px;" type="button" value="Back" onClick="doc_head_list();"/>
  </div>
  <div style="margin-top:5px; margin-bottom:5px;">
<?
$onChange="EnableObject('btnHeadSave',false);";
$sDisplay="Do you Comfirm to Change Playlist Name?";
$onChange.=sprintf("changeInputValue('txtHeadName','%s',document.getElementById('txtHeadName').value);",$sDisplay);
$onChange.="EnableObject('btnHeadSave',! checkEqual('txtHeadName','txtOldName'));";

$onReset="EnableObject('btnHeadSave',false);";
$onReset.="setObjValue('txtOldName','txtHeadName');";
?>
    &nbsp; &nbsp;  Playlist Name : 
    <input class="txt" size="40" type="text" id="txtHeadName" name="txtHeadName" readonly/>
    <input class="txt" size="40" type="hidden" id="txtOldName" name="txtOldName" />
    <input class="btnTh" style="margin-left:10px;" type="button" id="btnHeadChange" value="Change" onClick="<?=$onChange;?>">
    <input class="btnTh" style="margin-left:10px;" type="button" id="btnHeadSave" value="Save" onClick="playlist_head_save('HeadID','txtHeadName');" disabled>
    <input class="btnTh" style="margin-left:10px;" type="button" id="btnHeadReset" value="Reset" onClick="<?=$onReset;?>">
    <input class="btnTh" style="margin-left:10px;" type="button" value="Playlist Option" onClick="playlist_head_option('HeadID','TypeID');"/>
  </div>
  <div class="boxin w-100">
    <div class="header left">
      <h3><span>Document Playlist Items</span>
      <span style="margin-left:10">
      <input class="btnTh" type="button" value="Refresh" onClick="doc_refresh(document.getElementById('HeadID').value);"/>
      <input class="btnTh" style="margin-left:10px;" type="button" value="Document Library" onClick="show_file_exists();"/>
      <input class="btnTh" style="margin-left:10px;" type="button" value="Document Playlist Exists" onClick="show_playlist_exists();"/>
      <input id="HeadID" type="hidden" size="2"/> <input id="TypeID" type="hidden" value="9" size="2"/>
      </span></h3>
    </div>
    <table cellspacing="0" class="w-100">
    <tbody id="tblLine_Result">
      <tr>
        <td class="w-80 center"></td>
        <td class="w-20 center">
          <div class="dvtr top5 w-100">
             <div class="dvtd_left w-20 center"><img class="preview" src="images/icons/preview.png" title="preview"></div>
             <div class="dvtd_left w-20 center"><img class="preview" src="images/icons/down.png" title="down"></div>
             <div class="dvtd_left w-20 center"><img class="preview" src="images/icons/up.png" title="up"></div>
             <div class="dvtd_left w-20 center"><img class="preview" src="images/icons/edit.png" title="edit"></div>
             <div class="dvtd_left w-20 center"><img class="preview" src="images/icons/delete.png" title="delete"></div>
          </div>
        </td>
      </tr>
    </tbody>
    </table>
  </div>
</div>
<script type="text/javascript">
	$(function() {
		$('#dvSelectFile').dialog({ autoOpen: false });
		$('#dvSelectFile').dialog("option","width",900);
		$('#dvSelectFile').dialog("option","height",270);
		$('#dvSelectFile').dialog("option","resizable",false);
	});
</script>
<div id="dvSelectFile" title="Selection Exists Image.">
<div id="dvSelectFileItem" style="min-height:50px; height:185px; overflow:auto">
<table cellpadding="0" cellspacing="0">
  <tbody id="tblSelectFile_Body">
  <tr><td><input type="radio"></td><td>111111111111111111111111111111111111111111111</td></tr>
  </tbody>
</table>
</div>
<div class="pagination">
	<div class="dvtr">
        <div class="dvtd_left w-25" style="margin-top:15px;">You select : </div>
        <div class="demo dvtd_right">
        	<input type="text" class="txt" style="margin-top:13px;" size="60" id="txtExistFile"/>
			<input type="hidden" id="SelectFileID" value='' size="4"/>
       	</div>
        <div class="demo dvtd_right" style="padding-top:13px;">
			<input type="button" id='btnSelectFile' value="Selected" disabled onClick="playlist_exist_file();"/>
		</div>
  	</div>
</div>
<script type="text/javascript">
	$(function() {
		$('#dvSelectPlaylist').dialog({ autoOpen: false });
		$('#dvSelectPlaylist').dialog("option","width",900);
		$('#dvSelectPlaylist').dialog("option","height",270);
		$('#dvSelectPlaylist').dialog("option","resizable",false);
	});
</script>
<div id="dvSelectPlaylist" title="Selection Exists Playlist.">
<div id="dvSelectPlaylistItem" style="min-height:50px; height:185px; overflow:auto">
<table cellpadding="0" cellspacing="0">
  <tbody id="tblSelectPlaylist_Body">
  <tr><td><input type="radio"></td><td>111111111111111111111111111111111111111111111</td></tr>
  </tbody>
</table>
</div>
<div class="pagination">
	<div class="dvtr">
        <div class="dvtd_left w-25" style="margin-top:15px;">You select : </div>
        <div class="demo dvtd_right">
        	<input type="text" class="txt" style="margin-top:13px;" size="60" id="txtExistPlaylist"/>
			<input type="hidden" id="SelectPlaylistID" value='' size="4"/>
       	</div>
       	<div class="demo dvtd_right" style="padding-top:13px;">
			<input type="button" id='btnSelectPlaylist' value="Selected" disabled onClick="playlist_exist_playlist();"/>
		</div>
  	</div>
</div>
</div>
<script type="text/javascript">
	$(function() {
		$('#dvOption').dialog({ autoOpen: false });
		$('#dvOption').dialog("option","width",500);
		$('#dvOption').dialog("option","height",255);
		$('#dvOption').dialog("option","resizable",false);
	});
</script>
<div id="dvOption" title="Playlist Option Selection">
<table>
  <tbody id="tblOption_Body">
  </tbody>
</table>
</div>
</div>
</body>
</html>