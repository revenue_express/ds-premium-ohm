<? 
	$DEBUG=FALSE;
	$ComID=1;
	include("./includes/db_config.inc.php");
//	include("./includes/sys_config.inc.php");
	foreach ($_REQUEST as $Key => $Value) { $$Key=addslashes(urldecode($Value)); }

	$CSSName=NULL;
	$LayoutItems=NULL;
	$FileList=NULL;
	$ServerIP=$_SERVER["SERVER_ADDR"];
	$TypeUseFile=array(1,2,3,4,9,10,11);
	
	if (! isset($Run)) $Run=1;
	if (! isset($Local)) $Local=1;

	if  ( (isset($schID)) && (isset($ClientID)) ) {
		include("./functions/schedule.func.php");
		include("./functions/layout.func.php");
		include("./functions/playlist.func.php");
		include("./functions/command.func.php");


		$Data=file_get_contents($LocalHost.'/ajax/client_info.php?ClientID='.$ClientID);
		$ClientInfo=json_decode($Data,true);
		$cURL=$ClientInfo[0]['cli_url'];
		if ($Local==1) {
			$cURL="http://192.168.1.107:3001/ctl/command";
		}
		$Secret=$ClientInfo[0]['cli_syscode'];
		$Group=sprintf('%s',date('YmdHis'));
		$FRetry=3;					

		/*	Get Schedule Header Information */
		$schInfo=json_decode(schedule_head_view($schID),true);
		if ($DEBUG) {  echo "<br>schInfo<pre>"; print_r($schInfo); echo "</pre>"; }

		$ShowTime="";
		/*	Get Period ShowTime Information */
		$schDate=json_decode(schedule_period_list($schID),true);
		if ($DEBUG) {  echo "<br>schDate<pre>"; print_r($schDate); echo "</pre>"; }

		for ($iCount=0; $iCount < count($schDate); $iCount++) {
			$ShowTime[]=array("start"=>$schDate[$iCount]['schp_start'],"stop"=>$schDate[$iCount]['schp_stop']);
		}
		if ($DEBUG) {  echo "<br>ShowTime :<pre>"; print_r($ShowTime); echo "</pre>"; }

		/*	Get Template in Schedule*/
		$schItem=json_decode(schedule_layout_list($schID,1),true);
		if ($DEBUG) {  echo "<br>schItem<pre>"; print_r($schItem); echo "</pre>"; }
		/*	Loop for Get Template Information */
		for ($iSch=0; $iSch < count($schItem); $iSch++) {
			if ($DEBUG) {  echo sprintf("<br>schItem [%d] [%d]<pre>",$schItem[$iSch],$iSch); print_r($schItem[$iSch]); echo "</pre>"; }
			/*	Get Each Template Detail*/
			$TItem=json_decode(layout_line_list($schItem[$iSch]['layh_id']),true);		
			if ($DEBUG) {  echo sprintf("<br>LayItem on Layout %d<pre>",$schItem[$iSch]['layh_tmph_id']); print_r($TItem); echo "</pre>"; }
			$Panel=NULL;
			/* Loop Each Template Detail to Collect Playlist*/
			$ItemCount=array();
			/* CREATE CSS FILE*/
			$CSSData=file_get_contents($LocalHost.'/gen.css.php?TID='.$schItem[$iSch]['layh_tmph_id']);
			$CSSName=sprintf("%d.css",$schItem[$iSch]['layh_tmph_id']);
			$CSSPath="./data/css";
			$CSSFile=sprintf("%s/%s",$CSSPath,$CSSName);
			file_put_contents($CSSFile,$CSSData);
			$URL=sprintf("http://%s/%s/data/css/%s",$ServerIP,$LocalPath,$CSSName);
			$Hash=hash_file('md5',$CSSFile);
//			$DEBUG=TRUE;
			if ($Run==0) {
				$CMD=sprintf("%s%03d",date('His'),(round(microtime(true)*1000)%1000));
//				$CMD=sprintf("%s%03d",date('His'),(microtime(true)%1000));
//				$CMD=sprintf("%s",date('Hisu'));
//				$CMD=sprintf("%s",date('Hisz'));
			} else {
				$CMD=command_add_download($ComID,$ClientID,'layout',$CSSName,$URL,$Hash,$Group);
			}
//			$Link=sprintf("%s/?cmd=download&cmdid=%d&p1=layout&p2=%s&p3=%s&p4=%s&sid=%s&grp=%s",$cURL,$CMD,$CSSName,$URL,$Hash,$Secret,$Group);
			$Link=sprintf("%s/?cmd=download&cmdid=%d&sid=%s",$cURL,$CMD,$Secret);
			$Link.=sprintf("&grp=%s",$Group);
			$Link.=sprintf("&p1=/layout");
			$Link.=sprintf("&p2=%s",$CSSName);
			$Link.=sprintf("&p3=%s",$URL);
			$Link.=sprintf("&p4=%s",$Hash);
			$Link.=sprintf("&p5=%s",$CSSName);
			$Link.=sprintf("&p6=%d",$FRetry);
			if ($DEBUG) echo " return => '$CMD'";
			
			if (! isset($FileList[$CSSName])){
				$FileList[$CSSName]=$Link;
				$arrFileList[]=array("cmdid"=>$CMD,
					"fileid"=>$CMD,
					"filename"=>$CSSName,
					"filetitle"=>"",
					"filedest"=>"/home/smp/public_html/xsmp/media/layout",
					"filehash"=>$Hash,
					"srcurl"=>$URL,
					"desc"=>"");
			}

			/* CREATE Cover FILE*/
			$CoverReturn=file_get_contents($LocalHost.'/ajax/layout_cover_info.php?HeadID='.$schItem[$iSch]['layh_tmph_id']);
			$CoverData=json_decode($CoverReturn,true);
			$CoverFile=sprintf("media/cover/%s",$CoverData['file_sname']);

			if (! is_null($CoverData)) {
				$CoverName=$CoverData['file_sname'];
				$CoverPath="./data/covers/";
				$CoverFile=sprintf("%s/%s",$CoverPath,$CoverName);
				$URL=sprintf("http://%s/%s/data/covers/%s",$ServerIP,$LocalPath,$CoverName);
				$Hash=hash_file('md5',$CoverFile);
	//			$DEBUG=TRUE;
				if ($Run==0) {
//					$CMD=sprintf("%s%03d",date('His'),round(microtime(true) * 1000));
					$CMD=sprintf("%s%03d",date('His'),(microtime(true)%1000));
					$CMD=sprintf("%d",round(microtime(true)*1000));
				$CMD=sprintf("%s%03d",date('His'),(round(microtime(true)*1000)%1000));
//					$CMD=sprintf("%s%03d",date('His'),microtime()%1000);
//				$CMD=sprintf("%s",date('Hisz'));
				} else {
					$CMD=command_add_download($ComID,$ClientID,'cover',$CoverName,$URL,$Hash,$Group);
				}
	//			$Link=sprintf("%s/?cmd=download&cmdid=%d&p1=layout&p2=%s&p3=%s&p4=%s&sid=%s&grp=%s",$cURL,$CMD,$CSSName,$URL,$Hash,$Secret,$Group);
				$Link=sprintf("%s/?cmd=download&cmdid=%d&sid=%s",$cURL,$CMD,$Secret);
				$Link.=sprintf("&grp=%s",$Group);
				$Link.=sprintf("&p1=media/cover");
				$Link.=sprintf("&p2=%s",$CoverName);
				$Link.=sprintf("&p3=%s",$URL);
				$Link.=sprintf("&p4=%s",$Hash);
				$Link.=sprintf("&p5=%s",$CoverName);
				$Link.=sprintf("&p6=%d",$FRetry);
				if ($DEBUG) echo " return => '$CMD'";
				
				if (! isset($FileList[$CoverName])){
					$FileList[$CoverName]=$Link;
					$arrFileList[]=array("cmdid"=>$CMD,
						"fileid"=>$CoverData['file_id'],
						"filename"=>$CoverName,
						"filetitle"=>"",
						"filedest"=>"/home/smp/public_html/xsmp/media/cover",
						"filehash"=>$Hash,
						"srcurl"=>$URL,
						"desc"=>"");
					}

			}
//			$DEBUG=FALSE;

			for ($iTmp=0; $iTmp < count($TItem); $iTmp++) {
				/* Get Playlist Header Information*/
				$PlayList=NULL;
				$PLHead=json_decode(playlist_head_view($TItem[$iTmp]['layl_plh_id']),true);
				if ($DEBUG) {  echo sprintf("<br>PLHead %d<pre>",$TItem[$iTmp]['layl_plh_id']); print_r($PLHead); echo "</pre>"; }
				/* Get Playlist Header Option Information*/
				$PLHeadOption=json_decode(playlist_head_option_list($TItem[$iTmp]['layl_plh_id']),true);

				
				/* Get Playlist Items*/
				if (in_array($PLHead[0]['plh_type_id'],$TypeUseFile)) {
					$PLLine=json_decode(playlist_line_list($ComID,$PLHead[0]['plh_type_id'],$PLHead[0]['plh_id']),true);
				} else {
				}
				if ($DEBUG) {  echo sprintf("<br>PLLine %d<pre>",$TItem[$iTmp]['layl_plh_id']); print_r($PLLine); echo "</pre>"; }
				$PLItem="";

				/*Loop for create Playlist Items Array*/
				for ($iPlay=0; $iPlay < count($PLLine); $iPlay++) {
//					$FPath=sprintf("/media/%s/%s",$PLLine[$iPlay]['file_path'],$PLLine[$iPlay]['file_sname']);
					$FPath=sprintf("%s/%s",$PLLine[$iPlay]['type_spath'],$PLLine[$iPlay]['file_sname']);
					$PLItem[]=array(
					"id"=>$PLLine[$iPlay]['file_id'],
					"title"=>$PLLine[$iPlay]['file_oname'],
//					"type"=>$PLLine[$iPlay]['type_name'],
					"type"=>mime_content_type($FPath),
					"src"=>$FPath,
					);
					
					$FName=$PLLine[$iPlay]['file_sname'];
					$FPath=sprintf("data/%s/%s",$PLLine[$iPlay]['type_path'],$PLLine[$iPlay]['file_sname']);
					$FDest=$PLLine[$iPlay]['type_spath'];
					$FType=$PLLine[$iPlay]['type_name'];
					$URL=sprintf("http://%s/%s/%s",$ServerIP,$LocalPath,$FPath);
					$Hash=hash_file('md5',"./".$FPath);
//					$DEBUG=TRUE;
					if ($Run==0) {
//						$CMD=sprintf("%s",date('Hisu'));
//						$CMD=sprintf("%s%03d",date('His'),round(microtime(true) * 1000));
						$CMD=sprintf("%s%03d",date('His'),(microtime(true)%1000));
						$CMD=sprintf("%d",round(microtime(true)*1000));
						$CMD=sprintf("%s%03d",date('His'),(round(microtime(true)*1000)%1000));
//						$CMD=sprintf("%s%03d",date('His'),microtime()%1000);
					} else {
						$CMD=command_add_download($ComID,$ClientID,$FType,$FName,$URL,$Hash,$Group);
					}
					$Link=sprintf("%s/?cmd=download&cmdid=%d&sid=%s",$cURL,$CMD,$Secret);
					$Link.=sprintf("&grp=%s",$Group);
					$Link.=sprintf("&p1=%s",$FDest);
					$Link.=sprintf("&p2=%s",urlencode($PLLine[$iPlay]['file_dname']));
					$Link.=sprintf("&p3=%s",$URL);
					$Link.=sprintf("&p4=%s",$Hash);
					$Link.=sprintf("&p5=%s",$FName);
					$Link.=sprintf("&p6=%d",$FRetry);
					if ($DEBUG) echo " return => '$CMD'";
					if (! isset($FileList[$FName])){
						$FileList[$FName]=$Link;
						$arrFileList[]=array("cmdid"=>$CMD,
							"fileid"=>$PLLine[$iPlay]['file_id'],
							"filename"=>$FName,
							"filetitle"=>urlencode($PLLine[$iPlay]['file_dname']),
							"filedest"=>sprintf("/home/smp/public_html/xsmp/%s",$FDest),
							"filehash"=>$Hash,
							"srcurl"=>$URL,
							"desc"=>"");
					}
//					$DEBUG=FALSE;

				}

				/*Create Playlist Array*/
				$myPlaylist=array(
					"id"=>$PLHead[0]['plh_id'],
					"title"=>$PLHead[0]['plh_name']);

				for ($iCount=0; $iCount<count($PLHeadOption); $iCount++) {
					$Head=$PLHeadOption[$iCount]['pho_name'];
					$Value=$PLHeadOption[$iCount]['pho_value'];
					$myPlaylist[$Head]=$Value;
				}
				$myPlaylist['item']=$PLItem;
				$DEBUG=FALSE;
				if ($DEBUG) { echo "<pre>"; print_r($myPlaylist); echo "</pre>\n<br>"; }

//				$PlayList[]=$myPlaylist;
				$PlayList=$myPlaylist;

/*Create Panel Array*/
				if (isset($ItemCount[$TItem[$iTmp]['type_name']])) {
//	Last Found This Object
					$ItemCount[$TItem[$iTmp]['type_name']]++;
					$iCount=$ItemCount[$TItem[$iTmp]['type_name']];
				} else {
//	Not Found This Object
					$iCount=1;
					$ItemCount[$TItem[$iTmp]['type_name']]=1;							
				}
			}
			
		}

		/*Create Schedule Array*/

		$arrData=array("cmdlist"=>$arrFileList);
		echo json_encode($arrData);
		$postData=http_build_query($arrData);
		$opts = array('http' =>
			array(
					'method'  => 'POST',
					'header'  => 'Content-type: application/x-www-form-urlencoded',
					'timeout' => 90,
					'content' => $postData)
			);
		$context = stream_context_create($opts);
		$CURL=sprintf("http://%s:3001/download/",$ClientInfo[0]['cli_address']);
		echo "<div>Result :<br>".$CURL."</div>";
		$Result=file_get_contents($CURL,false,$context);
		echo "<div>Result :<br>".$Result."</div>";
	}
//	$Data=array("schedule"=>$Schedule);

/*
	$aFile=array();
	foreach ($FileList as $File=>$Addr) {
		if ($Run==1) {
			$rMsg=file_get_contents($Addr);
			if ($rMsg === false) {} else {} 
		}
//		for ($iCount=1; $iCount<1000; $iCount++) { };
		if ($DEBUG) {		echo sprintf("%s,%s<br>return=>%s\n<br>",$File,$Addr,$Msg['timeStamp']); }
		$aFile[]=array("name"=>$File, "addr"=>$Addr,"return"=>$rMsg);
	}
	echo json_encode($aFile);
*/
	
?>
	