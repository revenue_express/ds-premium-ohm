<? 
	include("./includes/db_config.inc.php");
	include("./includes/sys_config.inc.php");
	$DEBUG=FALSE;
?>
<html>
<head>
<?	include("./javascript.php");?>
<script type="text/javascript" src="./js/playlist_stream.js"></script>
<script type="text/javascript" src="./js/playlist_head_option.js"></script>
<link rel="stylesheet" href="css/custom-theme/jquery-ui-1.8.23.custom.css">
<link rel="stylesheet" href="css/StyleSheet.css">
<script type="text/javascript">
	$('document').ready(function() {
		stream_head_list();
		
	});
	
	$(function(){
		$('#btnStreamFile,#btnStreamPlaylist').button();
		$('#txtExistFile,#txtExistPlaylist').height(15);
	});	
</script>
</head>
<body>
<div id="dvHeadResult" class="w-80 boxin">
	<div class="header">
  	<h3>Streaming Playlist
    	<input id="btnListRefresh" type="button" value="refresh" onClick="stream_head_list();">
     <input id="txtSearch" type="text" value="" size="10">
     <input id="btnSearch" type="button" value="search" onClick="table_search('tblHead_Result',2,'txtSearch');">
<!--     <input id="btnEdit" type="button" value="edit" onClick="lm_update(parseInt($('#txtSearch').val()));"> -->
    </h3>
  </div>
  <table cellspacing="0">
  <thead>
  	<tr>
      <th class="w-10">Code</th>
      <th>Playlist Name</th>
      <th class="w-15">
      <input class="btnTH" type="button" value="Refresh" onClick="stream_head_list();"/>
      &nbsp; &nbsp;
      <input class="btnTH" type="button" value=" &nbsp;Add &nbsp;" onClick="playlist_head_add();"/>
      </th>
  	</tr>
  </thead>
  <tbody id="tblHead_Result">
    <tr>
      <td class="center">1</td>
      <td>2</td>
      <td>Item</td>
      <td class="center"><img class="preview" src="images/icons/edit.png" title="preview"></td>
      <td class="center"><img class="preview" src="images/icons/delete.png" title="preview"></td>
    </tr>
  </tbody>
  </table>
</div>
<div id="dvLineResult" class="boxin w-80">
	<div class="header">
  	<h3>Streaming Playlist</h3><input class="btnHeader" style="margin-left:170px;" type="button" value="Back" onClick="stream_head_list();"/>
  </div>
  <div style="margin-top:5px; margin-bottom:5px;">
<?
$onChange="EnableObject('btnHeadSave',false);";
$sDisplay="Do you Comfirm to Change Playlist Name?";
$onChange.=sprintf("changeInputValue('txtHeadName','%s',document.getElementById('txtHeadName').value);",$sDisplay);
$onChange.="EnableObject('btnHeadSave',! checkEqual('txtHeadName','txtOldName'));";

$onReset="EnableObject('btnHeadSave',false);";
$onReset.="setObjValue('txtOldName','txtHeadName');";
?>
    &nbsp; &nbsp;  Playlist Name : 
    <input class="txt" size="40" type="text" id="txtHeadName" name="txtHeadName" readonly/>
    <input class="txt" size="40" type="hidden" id="txtOldName" name="txtOldName" />
    <input class="btnTh" style="margin-left:10px;" type="button" id="btnHeadChange" value="Change" onClick="<?=$onChange;?>">
    <input class="btnTh" style="margin-left:10px;" type="button" id="btnHeadSave" value="Save" onClick="playlist_head_save('HeadID','txtHeadName');" disabled>
    <input class="btnTh" style="margin-left:10px;" type="button" id="btnHeadReset" value="Reset" onClick="<?=$onReset;?>">
    <input class="btnTh" style="margin-left:10px;" type="button" value="Playlist Option" onClick="playlist_head_option('HeadID','TypeID');"/>
  </div>
  <div class="boxin w-100">
    <div class="header left">
      <h3><span>Streaming Playlist Items</span>
      <span>
      <input class="btnTh" style="margin-left:5px;" type="button" value="Refresh" onClick="stream_refresh(document.getElementById('HeadID').value);"/>
      <input class="btnTh" style="margin-left:10px;" type="button" value="Streaming Library" onClick="show_stream_exists();"/>
      <input class="btnTh" style="margin-left:10px;" type="button" value="Streaming Playlist Exists" onClick="show_stream_playlist_exists();"/>
      <input id="HeadID" type="hidden" size="2"/> <input id="TypeID" type="hidden" value="8" size="2"/>
      </span></h3>
    </div>
    <table cellspacing="0" class="w-100">
    <tbody id="tblLine_Result">
      <tr>
        <td class="w-80 center"></td>
        <td class="w-20 center">
          <div class="dvtr top5 w-100">
             <div class="dvtd_left w-20 center"><img class="preview" src="images/icons/preview.png" title="preview"></div>
             <div class="dvtd_left w-20 center"><img class="preview" src="images/icons/down.png" title="down"></div>
             <div class="dvtd_left w-20 center"><img class="preview" src="images/icons/up.png" title="up"></div>
             <div class="dvtd_left w-20 center"><img class="preview" src="images/icons/edit.png" title="edit"></div>
             <div class="dvtd_left w-20 center"><img class="preview" src="images/icons/delete.png" title="delete"></div>
          </div>
        </td>
      </tr>
    </tbody>
    </table>
  </div>
</div>
<script type="text/javascript">
	$(function() {
		$('#dvStreamFile').dialog({ autoOpen: false });
		$('#dvStreamFile').dialog("option","width",700);
		$('#dvStreamFile').dialog("option","height",270);
		$('#dvStreamFile').dialog("option","resizable",false);
	});
</script>
<div id="dvStreamFile" title="Select Exists Streaming">
<div id="dvStreamFileItem" style="min-height:50px; height:185px; overflow:auto">
<table cellspacing="0">
  <tbody id="tblStreamFile_Body">
  <tr><td><input type="radio"></td><td>111111111111111111111111111111111111111111111</td>
  </tbody>
</table>
</div>
<div class="pagination">
	<div class="dvtr">
        <div class="dvtd_left w-25" style="margin-top:15px;">You select : </div>
        <div class="demo dvtd_right" style="padding-top:13px;">
        	<input type="text" class="txt" size="50" id="txtExistStream"/>
			<input type="hidden" id="SelectStreamID" value='' size="4"/>
        </div>
        <div class="demo dvtd_right" style="padding-top:13px;">
			<input type="button" id='btnSelectStream' value="Selected" disabled onClick="playlist_exist_stream();"/>
        </div>
   	</div>
</div>
<script type="text/javascript">
	$(function() {
		$('#dvStreamPlaylist').dialog({ autoOpen: false });
		$('#dvStreamPlaylist').dialog("option","width",700);
		$('#dvStreamPlaylist').dialog("option","height",270);
		$('#dvStreamPlaylist').dialog("option","resizable",false);
	});
</script>
<div id="dvStreamPlaylist" title="Selection Exists Playlist.">
<div id="dvStreamPlaylistItem" style="min-height:50px; height:185px; overflow:auto">
<table cellspacing="0">
  <tbody id="tblStreamPlaylist_Body">
  <tr><td><input type="radio"></td><td>111111111111111111111111111111111111111111111</td>
  </tbody>
</table>
</div>
<div class="pagination">
	<div class="dvtr">
        <div class="dvtd_left w-25" style="margin-top:15px;">You select : </div>
        <div class="demo dvtd_right" style="padding-top:13px;">
        	<input type="text" class="txt" size="50" id="txtExistPlaylist"/>
			<input type="hidden" id="StreamPlaylistID" value='' size="4"/>
        </div>
        <div class="demo dvtd_right" style="padding-top:13px;">
			<input type="button" id='btnStreamPlaylist' value="Selected" disabled onClick="playlist_exist_stream_playlist();"/>
		</div>
   	</div>
</div>
</div>
<script type="text/javascript">
	$(function() {
		$('#dvOption').dialog({ autoOpen: false });
		$('#dvOption').dialog("option","width",500);
		$('#dvOption').dialog("option","height",255);
		$('#dvOption').dialog("option","resizable",false);
	});
</script>
<div id="dvOption" title="Playlist Option Selection">
<table>
  <tbody id="tblOption_Body">
  </tbody>
</table>
</div>
</body>
</html>