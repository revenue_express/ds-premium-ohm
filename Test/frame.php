<?
	ob_start();
	session_cache_expire(5);
	session_start(); 
	
	if (empty($_SESSION['usr_login'])) {
		header("Location: ./logout.php",true);
	}

	foreach ($_SESSION as $Key=>$Val) {
//		echo sprintf("\n<br> %s = '%s'",$Key,$Val);
	}
?>
<html>
<head>
<script type="text/javascript">
function showRSS(obj,str) {
	if (str.length==0)	{ 
  	document.getElementById(obj).innerHTML="";
  	return;
	}
	if (window.XMLHttpRequest){
	// code for IE7+, Firefox, Chrome, Opera, Safari
  	xmlhttp=new XMLHttpRequest();
  }	else {
	// code for IE6, IE5
  	xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
	xmlhttp.onreadystatechange=function(){
		document.getElementById(obj).innerHTML="";
  	if (xmlhttp.readyState==4 && xmlhttp.status==200){
    	document.getElementById(obj).innerHTML=xmlhttp.responseText;
    }
  }
	var URL="getrss.php?Link="+
	encodeURIComponent(str);
	alert("URL => "+URL);
	xmlhttp.open("GET",URL,true);
	xmlhttp.send();
}

function htmlspecialchars (string, quote_style, charset, double_encode) {
    // Convert special characters to HTML entities  
    // 
    // version: 1109.2015
    // discuss at: http://phpjs.org/functions/htmlspecialchars
    // +   original by: Mirek Slugen
    // +   improved by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
    // +   bugfixed by: Nathan
    // +   bugfixed by: Arno
    // +    revised by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
    // +    bugfixed by: Brett Zamir (http://brett-zamir.me)
    // +      input by: Ratheous
    // +      input by: Mailfaker (http://www.weedem.fr/)
    // +      reimplemented by: Brett Zamir (http://brett-zamir.me)
    // +      input by: felix
    // +    bugfixed by: Brett Zamir (http://brett-zamir.me)
    // %        note 1: charset argument not supported
    // *     example 1: htmlspecialchars("<a href='test'>Test</a>", 'ENT_QUOTES');
    // *     returns 1: '&lt;a href=&#039;test&#039;&gt;Test&lt;/a&gt;'
    // *     example 2: htmlspecialchars("ab\"c'd", ['ENT_NOQUOTES', 'ENT_QUOTES']);
    // *     returns 2: 'ab"c&#039;d'
    // *     example 3: htmlspecialchars("my "&entity;" is still here", null, null, false);
    // *     returns 3: 'my &quot;&entity;&quot; is still here'
    var optTemp = 0,
        i = 0,
        noquotes = false;
    if (typeof quote_style === 'undefined' || quote_style === null) {
        quote_style = 2;
    }
    string = string.toString();
    if (double_encode !== false) { // Put this first to avoid double-encoding
        string = string.replace(/&/g, '&amp;');
    }
    string = string.replace(/</g, '&lt;').replace(/>/g, '&gt;');
//    string = string.replace(/</g, '&lt;').replace(/>/g, '&gt;').replace(/\?/g,'&#63;');
 
    var OPTS = {
        'ENT_NOQUOTES': 0,
        'ENT_HTML_QUOTE_SINGLE': 1,
        'ENT_HTML_QUOTE_DOUBLE': 2,
        'ENT_COMPAT': 2,
        'ENT_QUOTES': 3,
        'ENT_IGNORE': 4
    };
    if (quote_style === 0) {
        noquotes = true;
    }
    if (typeof quote_style !== 'number') { // Allow for a single string or an array of string flags
        quote_style = [].concat(quote_style);
        for (i = 0; i < quote_style.length; i++) {
            // Resolve string input to bitwise e.g. 'ENT_IGNORE' becomes 4
            if (OPTS[quote_style[i]] === 0) {
                noquotes = true;
            }
            else if (OPTS[quote_style[i]]) {
                optTemp = optTemp | OPTS[quote_style[i]];
            }
        }
        quote_style = optTemp;
    }
    if (quote_style & OPTS.ENT_HTML_QUOTE_SINGLE) {
        string = string.replace(/'/g, '&#039;');
    }
    if (!noquotes) {
        string = string.replace(/"/g, '&quot;');
    }
 
    return string;
}

	function JqueryFeed(URL) {
		$.getFeed({
       url: URL,
       success: function(feed) {
           alert(feed.title);
       }
   	});
	}
	 
</script>
</head>
<body>
<script type="text/javascript">
	var sDisplay="Data\nUser Login : ";
	sDisplay += window.parent.document.getElementById('usr_login').value;
	sDisplay += "\nUser Name: ";
	sDisplay += window.parent.document.getElementById('usr_name').value;
	sDisplay += "\nUser Level: ";
	sDisplay += window.parent.document.getElementById('usr_level').value;
	alert (sDisplay);
</script>
<div id="showUser"></div>
<form>
<input id='txtLink' type="text" value="RSS Link Here"  size="80"/>
<input type="button" onClick="showRSS('rssOutput',document.getElementById('txtLink').value);" value="Show Feed" />
</form>
<br />
<div id="rssOutput">RSS-feed will be listed here...</div>
</body>
</html>	
