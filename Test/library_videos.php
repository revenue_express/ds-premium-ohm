<? 
	include("../includes/db_config.inc.php");
//	include("../includes/sys_config.inc.php");
	include("./library.func.php");
	$DEBUG=TRUE;
	$TypeID=2;
	$ComID=1;
	foreach ($_REQUEST as $Key => $Value) { $$Key=addslashes(urldecode($Value)); }
?>
<html>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />     
<head>
<script type="text/javascript" src="../js/jquery-1.7.2.min.js"></script>
<script type="text/javascript" src="../js/jquery-ui-1.8.21.custom.min.js"></script>
<script type="text/javascript" src="../js/checkValid.js"></script>
<script type="text/javascript" src="../js/SolmetraUploader.js"></script>
<script type="text/javascript">
  SolmetraUploader.setErrorHandler('test');
  function test (id, str) { alert('ERROR: ' + str); }
</script>
<link rel="stylesheet" href="../css/stylesheet.css">
</head>
<body>
<div class="dvHeader">Media Library (Video)</div>
<div id="dvLibUploadVideos">
<?
		if ($DEBUG) { echo "Request<pre>";		print_r($_REQUEST);		echo "</pre>"; }
		if ($DEBUG) { echo "File<pre>";		print_r($_FILES);		echo "</pre>"; }
//		echo "<pre>"; print_r($_SERVER); echo "</pre>";
		include("./SolmetraUploader.php");

		$solmetraUploader = new SolmetraUploader('./','./upload.php','./config_videos.php');
//		$solmetraUploader->setDemo(102400000);
		$solmetraUploader->gatherUploadedFiles();
		if (isset($_FILES) && sizeof($_FILES)) {
			if ($DEBUG) {
 	 		echo '<h2>Uploaded files</h2>';
  			echo '<pre>'; print_r($_FILES['firstfile']); echo '</pre>';
			}
			$uploadName=$_FILES['firstfile']['tmp_name'];
			$oldName=$_FILES['firstfile']['name'];
			$fSize=$_FILES['firstfile']['size'];
			if (($oldName != '') and ($txtNewName !='')  and ($uploadName != '') and ($fSize > 0) ) {
				$result=library_add('videos',$oldName,$txtNewName,$uploadName,$fSize);
			} else {
				$rResult="Data Not Completed.<BR>";
				$rResult.="oldName = '$oldName'<BR>";
				$rResult.="txtNewName = '$txtNewName'<BR>";
				$rResult.="uploadName = '$uploadName'<BR>";
				$rResult.="size = '".$fdata['size']."'<BR>";
				unlink($uploadName);
			}
		}

		if (isset($btnDelete)) {
			if ($DEBUG) echo "<br> Delete File => $btnDelete";
			$RowAffect=library_active($btnDelete);
			if ($RowAffect <> 1) {
				if ($DEBUG) echo ".....Delete Completed";
			} else {
				if ($DEBUG) echo ".....Delete Failed";
			}
		} elseif (isset($btnRestore)) {
			if ($DEBUG) echo "<br> Restore File => $btnRestore";
			$RowAffect=library_active($btnRestore,1);
			if ($RowAffect <> 1) {
				if ($DEBUG) echo ".....Restore Completed";
			} else {
				if ($DEBUG) echo ".....Restore Failed";
			}
		} else {
			if (isset($_SERVER['HTTP_REFERER']))	{
				if ($_SERVER['HTTP_URI'] = $PHP_SELF) {
					echo ("No File Upload");
				}
			}
		}
		
?>
    <div class="well" style="width:700px;">
    <form style="padding-left:90px; padding-top:10px;" method="post"/>
<? if (strlen($rResult)>0) {?><div align="center"><?=$rResult;?><br /></div><? } ?>
<? echo $solmetraUploader->getInstance('firstfile', 520,40,true); ?><br>
Display Name : <input type="text" id="txtNewName" name="txtNewName" size="32" />&nbsp; <input type="submit" class="btnSubmit" name="btnSubmit" id="btnSubmit" value="Save"/>
    </form>
	</div>
</div>
<br>
<?	
	$FileList=json_decode(library_list($ComID,$TypeID,1),true);
	if ($DEBUG) {
//	echo "Count(Data) =".count($data);
//	$FileList=json_decode($data);
		echo "Count(FileList) =".count($FileList);
		echo "<pre>";
		print_r($FileList);
		echo "</pre>";
//	for ($iCount=0; $iCount < count($FileList); $iCount++) {
//		$arrData=array();
//		$arrData=$FileList[$iCount];
//		echo "<pre>";
//		print_r($arrData);
//		echo "</pre>";
//		echo $arrData['file_id'];
	}
?>
<table id="mytable" cellspacing="0">
  <tr>
<!--    <th width="25" bgcolor="#33CCFF" scope="col">ID</th> -->
    <th width="30%" scope="col" class="spec">Display Name</th>
    <th width="35%" scope="col" nowrap="nowrap">Original Name</th>
    <th width="15%" scope="col">Sys Name</th>
    <th width="10%" scope="col">Size(KB)</th>
    <th width="10%" nowrap="nowrap" scope="col">In Used</th>
<?	if ($USER['usr_level'] <> 'Administrator') { ?>
    <th width="10%" nowrap="nowrap" scope="col" colspan="2">Action</th>
<? } else { ?>
    <th width="10%" nowrap="nowrap" scope="col" colspan="3">Action</th>
<?	} ?>
  </tr>
<?
	if (count($FileList) > 0) {
		for ($iCount=0;$iCount<count($FileList);$iCount++) {
			$sFileName=sprintf("./data/videos/%s",$FileList[$iCount]['file_sname']);
			$onViewClick=sprintf("window.open('%s','_view');",$sFileName);
			$formDel=sprintf("formDelete_%d",$FileList[$iCount]['file_id']);  
			$onDelete=sprintf("if (confirm('Do you confirm to delete')) {document.getElementById('%s').submit(); }",$formDel);
			$formRestore=sprintf("formRestore_%d",$FileList[$iCount]['file_id']);  
			$onRestore=sprintf("if (confirm('Do you confirm to restore data')) {document.getElementById('%s').submit(); }",$formRestore);
			$FileInUse=FALSE;
			//$clsTR=($FileInUse)?"data_used":"data_free";
			$clsTR=($FileInUse)?"":"alt";
			if ($FileInUse) {
				$onDelete=sprintf("alert('%s')","File in used. Cannot Delete.");
			}
?>
  <tr>
<!--    <td align="center" bgcolor="#FFCCFF"><?=$FileList[$iCount]['file_id'];?></td>  -->
    <td class="spec_<?=$clsTR;?>"><?=$FileList[$iCount]['file_dname'];?></td>
    <td class="<?=$clsTR;?>" nowrap="nowrap"><?=$FileList[$iCount]['file_oname'];?></td>
    <td class="<?=$clsTR;?>" align="left" nowrap><?=$FileList[$iCount]['file_sname'];?></td>
    <td class="<?=$clsTR;?>" align="right"><?=number_format($FileList[$iCount]['file_size']);?></td>
    <td class="<?=$clsTR;?>" align="center"><?=($FileInUse)?"Yes":"No";?></td>
    <td class="<?=$clsTR;?>" align="center" width="16"><img class="preview" src="./images/icons/preview.png" onClick="<?=$onViewClick;?> " title="preview"></td>
<?	if ($USER['usr_level'] == 'Administrator') { ?>
	<form action="<?=$PHP_SELF;?>" method="post" id="<?=$formRestore;?>" name="<?=$formRestore;?>">
    <input type="hidden" id="btnRestore" name="btnRestore" value="<?=$FileList[$iCount]['file_id'];?>">
    <td class="<?=$clsTR;?>" align="center" width="16">
    <? if ($FileList[$iCount]['file_active']==0) { ?><img width="16" height="16" src="./images/icons/restore.png" onClick="<?=$onRestore;?>" title="restore"><? } else { echo "&nbsp;";} ?></td>
    </form>
<?	} ?>
		<form action="<?=$PHP_SELF;?>" method="post" id="<?=$formDel;?>" name="<?=$formDel;?>">
    <input type="hidden" id="btnDelete" name="btnDelete" value="<?=$FileList[$iCount]['file_id'];?>">
    <td class="<?=$clsTR;?>" align="center" width="16">
    <? if ($FileList[$iCount]['file_active']==1) { ?><img width="16" height="16" src="./images/icons/delete.png" onClick="<?=$onDelete;?>" title="delete"><? } else { echo "&nbsp;";} ?></td>
    </form>
  </tr>
<? } }  else { ?>
  <tr>
<?	$ColSize=($USER['usr_level'] == 'Administrator')?9:8; ?>
    <td class="spec" align="center" colspan="<?=$ColSize;?>">NO FOUND</td>
 </tr>
<?	} ?>
</table>
</body>
</html>