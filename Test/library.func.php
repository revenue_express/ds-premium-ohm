<?
	function file_type_list($TypeID=1) {
		global $DEBUG,$USER;
		$sSQL=sprintf("SELECT * FROM type_info WHERE (type_id = %d) LIMIT 1",$TypeID);
		if ($DEBUG) echo "[".__FUNCTION__."] sSQL -> '$sSQL'<br>";
		$DBReturn=JSonSQL($sSQL);
//		echo $DBReturn;
		return($DBReturn);
	}

	function library_list($comID=1,$typeID=1,$login=1, $ppage=0, $limit=0, $spage=0) {
		global $DEBUG,$USER;
		$sSQL=sprintf("SELECT * FROM  file_info WHERE (file_com_id = %d) ",$comID);
		if ($USER['usr_level'] <> 'Administrator') $sSQL.="AND (file_active = 1) "; 
		if ($typeID <> 0)  $sSQL.=sprintf("AND file_type_id=%d ",$typeID);
		$sSQL.="Order by file_id";
		if ( ($ppage <> 0) && ($limite<> 0) && ($spage <> 0) ) {
			$sSQL.=sprintf(" LIMIT %d,%d",$spage*$limit,$limit);
		}
		if ($DEBUG) echo "[".__FUNCTION__."] sSQL -> '$sSQL'<br>";
		$DBReturn=JSonSQL($sSQL);
//		echo $DBReturn;
		return($DBReturn);
	}

	function library_add($type,$OldName,$NewName,$SysName,$FileSize,$ComID='1') {
		global $DBReturn,$DEBUG;
		$DBReturn=0;
		$InsertID=0;
		$NewPath="./data/".$type."/";
		$OldPath="./data/";
		$sNewFile=$NewName;
		$mTime=microtime();
		if ($DEBUG) echo "mTime -> '$mTime'";
		list($sTime)=explode(" ",$mTime);
		$sSysName=sprintf("%s%03d",date('YmdHis'),$sTime*1000);
		$exten=strtolower(end(explode(".", $OldName)));
		$newFileName=sprintf("%s%s.%s",$NewPath,$sSysName,$exten);
		switch(strtolower($type)) {
			case "images" : $typeID=1; break;
			case "videos" : $typeID=2; 
				$OldFileName=$newFileName;
				$newFileName=sprintf("%s%s.webm",$NewPath,$sSysName);
				$cmd=sprintf("ffmpeg -i %s -f webm %s",$OldFileName,$newFileName);
				
				exec($cmd,$output,$return);	
				unlink($OldFileName);
				$exten="webm";
			break;
			case "flashs" : $typeID=3; break;
			case "audios" : $typeID=4; break;
			case "docs" : $typeID=9; break;
			case "pdfs" : $typeID=10; break;
			case "covers" : $typeID=11; break;
			case "ppts" : $typeID=12; break;
		}
		rename($SysName,$newFileName);
		$CRC=hash_file('md5',$newFileName);
		$sSQL="INSERT INTO file_info SET ";
		$sSQL.="file_com_id=$ComID ";
		$sSQL.=sprintf(", file_type_id=%d ",$typeID);
		$sSQL.=sprintf(", file_sname='%s.%s'",$sSysName,$exten);
		$sSQL.=", file_oname='$OldName'";
		$sSQL.=sprintf(", file_dname='%s'",$sNewFile);
		$sSQL.=", file_path='$type'";
		$sSQL.=", file_size=$FileSize";
		$sSQL.=", file_crc='$CRC'";
		if ($DEBUG) echo "[".__FUNCTION__."] sSQL -> '$sSQL'<br>";
//		$InsertID=InsertSQL($sSQL);
//		return ($InsertID);
	}
	
	function library_edit($FileID,$FileName) {
		global $DEBUG;
		
		$sSQL=sprintf("UPDATE file_info SET file_dname = '%s' ",$FileName);
		$sSQL.=sprintf("WHERE file_id=%d",$FileID);
		if ($DEBUG) echo "[".__FUNCTION__."] sSQL -> '$sSQL'<br>";
		$DBReturn=AffectedSQL($sSQL);
//		echo $DBReturn;
		return($DBReturn);
	}
	
	function library_active($FileID,$active=0) {
		global $DEBUG;
		
		$sSQL=sprintf("UPDATE file_info SET file_active = %d ",$active);
		$sSQL.=sprintf("WHERE file_id=%d",$FileID);
		if ($DEBUG) echo "[".__FUNCTION__."] sSQL -> '$sSQL'<br>";
		$DBReturn=AffectedSQL($sSQL);
//		echo $DBReturn;
		return($DBReturn);
	}
	
?>