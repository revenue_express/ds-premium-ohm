<? 
	$DEBUG=FALSE;
	include("./includes/db_config.inc.php");
//	include("./includes/sys_config.inc.php");
	include("./functions/library.func.php");
	include("./functions/display.func.php");
?>
<html>
<head>
<script language="javascript" src="js/jquery.js"></script>
<?	include("./javascript.php");?>
<!--
<script type="text/javascript" src="js/jquery-1.7.2.min.js"></script>
<script type="text/javascript" src="js/jquery-ui-1.8.21.custom.min.js"></script>
<script type="text/javascript" src="./js/checkValid.js"></script>
<script type="text/javascript" src="./js/signage.js"></script>
-->
<script>
	$(function() {
		$( "#btnPopUpReset,#btnPopUpClose" ).button();
		$( "#tabs" ).tabs();
		$( "#datepicker" ).datepicker();
	});
</script>
<link rel="stylesheet" href="css/custom-theme/jquery-ui-1.8.23.custom.css">
<link rel="stylesheet" href="./css/StyleSheet.css">
</head>
<body>
<?
//	$TypeData=array("Name"=>'image',"Path"=>"images");
//	$TypeData=json_decode(file_type_list($TypeID),true);
	if ($DEBUG) { echo "<li>Request<pre>"; print_r($_REQUEST); echo "</pre>"; }
	$bSuccess=false;
	$sScript="";

	$FileList=json_decode(display_list(),true);
//	echo "Count(Data) =".count($data);
//	$FileList=json_decode($data);
//	echo "Count(FileList) =".count($FileList);
//	echo "<pre>";
//	print_r($FileList);
//	echo "</pre>";
//	for ($iCount=0; $iCount < count($FileList); $iCount++) {
//		$arrData=array();
//		$arrData=$FileList[$iCount];
//		echo "<pre>";
//		print_r($arrData);
//		echo "</pre>";
//		echo $arrData['file_id'];
//	}
?>
<div id="dvImageMain" class="boxin">
	<div class="header"><span>Display Library</span>
    <span id="spHeadSearch">
    	  <input id="btnListRefresh" type="button" value="refresh" onClick="history.go(0);">
        <input id="txtSearch" type="text" value="" size="10">
        <input id="btnSearch" type="button" value="search" onClick="table_search('tblResult_body',2,'txtSearch');">
<!--		    <input id="btnEdit" type="button" value="edit" onClick="schedule_edit(parseInt($('#txtSearch').val()));"> -->
    		</span>
  </div>
	<div id="dvImageFile">
  <table cellspacing="0">
  	<thead>
    <tr>
    	<th>&nbsp;</th>
        <th nowrap>Display Name</th>
        <th nowrap>Display Size</th>
    </tr>
    </thead>
    <tbody id="tblResult_body">
    	<tr><td colspan="3" align="center">NOT FOUND ITEM</td></tr>		
<?
	if (count($FileList) > 0) {
		for ($iCount=0;$iCount<count($FileList);$iCount++) {
//			$sFileName=sprintf("./data/%s/%s",$TypeData[0]['type_path'],$FileList[$iCount]['file_sname']);
//			$onViewClick=sprintf("window.open('%s','_view');",$sFileName);
			$onRadioClick=sprintf("setValue('FileName','%s','formPopUpDisplay');",$FileList[$iCount]['disp_name']);
			$onRadioClick.=sprintf("setValue('FileID',%d,'formPopUpDisplay');",$FileList[$iCount]['disp_id']);
			$onRadioClick.=sprintf("EnableObject('btnPopUpSave',true,'formPopUpDisplay');");
//			$sDisplay="Do you confirm to insert this file to playlist?";

//			$onSaveClick=sprintf("showConfirm('%s','formPopUpLibrary','btnPopUpSave','submit','Save');",$sDisplay);
			$onSaveClick=sprintf("SetParentValue('%s','%s','FileID');",$FormName,$DispID);
			$onSaveClick.=sprintf("SetParentValue('%s','%s','FileName');",$FormName,$DispName);
			$onSaveClick.=sprintf("EnableParentObject('%s',true,'%s');",$btnData,$FormName);
			$onSaveClick.=sprintf("window.close();");

			$onResetClick="EnableObject('btnPopUpSave',false,'formPopUpLibrary');";
//			$onCloseClick=sprintf("window.opener.location.href=window.opener.location.href+'?PlayListID=%d';",$PlayListID);
//			$onCloseClick.="window.opener.location.replace(window.opener.location.href);";

			$onCloseClick="window.close();";

//			$onOptionClick="Toggles('tblBodyImageOption')";
//			$onDisplayChange=sprintf("setValue('Option_1',this.value,'formPopUpLibrary');");
//			$onInChange=sprintf("setValue('Option_2',this.value,'formPopUpLibrary');");
//			$onOutChange=sprintf("setValue('Option_3',this.value,'formPopUpLibrary');");
//			$onIntervalChange=sprintf("setValue('Option_4',this.value,'formPopUpLibrary');");
			$Selected="";
			if ($OldID==$FileList[$iCount]['disp_id']) { 
				$Selected=" CHECKED";
				$FileName=$FileList[$iCount]['disp_name']; 
			}
?>
    <tr>
    <td width="20" align="center"><input type="radio" id="rdoFile" name="rdoFile" onClick="<?=$onRadioClick;?>"<?=$Selected;?>></td>
    <td width="80%"><?=$FileList[$iCount]['disp_name'];?></td>
    <td nowrap='nowrap'><?=sprintf("%d X %d",$FileList[$iCount]['disp_width'],$FileList[$iCount]['disp_height']);?></td></tr>
<?	} } ?>
    </tbody>
  </table>
</div>
	<div id="dvImageData" class="center pagination top15">
	<form id="formPopUpDisplay" method="post">

  	<input type="hidden" id="FileID" name="FileID" value="">
    You select <input type="text" maxlength="80" class="txt" id="FileName" name="FileName" size="55" value="<?=$FileName;?>"readonly disabled>
		<span id="formPopUpLibraryOption">
		<input type="hidden" id="Option_1" name="Option_1" value="">
		<input type="hidden" id="Option_2" name="Option_2" value="">
		<input type="hidden" id="Option_3" name="Option_3" value="">
		<input type="hidden" id="Option_4" name="Option_4" value="">
		<input type="hidden" id="Option_5" name="Option_5" value="">
		<input type="hidden" id="Option_6" name="Option_6" value="">
		<input type="hidden" id="Option_7" name="Option_7" value="">
		<input type="hidden" id="Option_8" name="Option_8" value="">
		<input type="hidden" id="Option_9" name="Option_9" value="">
    	</span>
    <div class="demo" style="float:right; margin-right:20px;"> 
    <input type="button" class="btnJQR" id="btnPopUpSave" value="Save" disabled onClick="<?=$onSaveClick;?>">
    <input type="reset" id="btnPopUpReset" value="Reset"  onClick="<?=$onResetClick;?>">
    <input type="button" id="btnPopUpClose" value="Close"  onClick="<?=$onCloseClick;?>">
    </div>
	</form>
	</div>
</div>
</div>
<script>
	$('#tblResult_body tr:first').hide(); 
</script>
</body>
</html>