<?PHP
  // Original PHP code by Chirp Internet: www.chirp.com.au
  // Please acknowledge use of this code by including this header.
	include("../includes/db_config.inc.php");
	function cleanData(&$str){
		$str = preg_replace("/\t/", "\\t", $str);
		$str = preg_replace("/\r?\n/", "\\n", $str);
		if(strstr($str, '"')) $str = '"' . str_replace('"', '""', $str) . '"';
	}
	
	// filename for download
	//$filename = "website_data_" . date('Ymd') . ".xls";
	
	//header("Content-Disposition: attachment; filename=\"$filename\"");
	//header("Content-Type: application/vnd.ms-excel");
	
	$flag = false;
	echo $result = mysql_query("SELECT * FROM client_config",$Connection) or die(mysql_query());
	while(false !== ($row = mysql_num_rows($result))) {
		if(!$flag) {
		  // display field/column names as first row
		  echo implode("\t", array_keys($row)) . "\r\n";
		  $flag = true;
		}
		
		array_walk($row, 'cleanData');
		echo implode("\t", array_values($row)) . "\r\n";
	}
	exit;
?>