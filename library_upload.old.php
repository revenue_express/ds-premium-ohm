<head>
<?php 
//	ini_set('session.save_path',realpath(dirname($_SERVER['DOCUMENT_ROOT']) . '/neptune/session'));
//	session_save_path('/var/www/neptune/session/');
//	session_start();
//	$_COOKIE['PHPSESSID'] = $_POST['PHPSESSID'];
	include("./includes/config_db.inc.php");
	include("./functions/library.func.php");
	include("./javascript.php");
?>
<script src="js/jquery.uploadify.min.js" type="text/javascript"></script>
<link rel="stylesheet" type="text/css" href="css/uploadify.css">
<style type="text/css">
body {
	font: 13px Arial, Helvetica, Sans-serif;
}
</style>
</head>
<body>
	<form>
		<div id="queue"></div>
		<input id="file_upload" name="file_upload" type="file" multiple="true">
	</form>

<script type="text/javascript">
	<?php $timestamp = time();?>
	$(function() {
		$('#file_upload').uploadify({
			'scriptData': { 'session': '<?php echo session_id(); ?>'},
			'formData'     : {
				'<?php echo session_name();?>' : '<?php echo session_id();?>',
				'session' : '<?php echo session_id();?>',
				'timestamp' : '<?php echo $timestamp;?>',
				'token'     : '<?php echo md5('unique_salt' . $timestamp);?>',
				'extension'     : 'jpg,jpeg,gif,png,mov,avi,mkv,mp4,flv,webm',
				'path' : 'images',
				'stype' : 'images',
				'ComID' : '<?php echo $_SESSION['usr_com_id'];?>',
			},
			'swf'      : 'uploadify.swf',
			'uploader' : 'uploadify.php',
			'onUploadSuccess' : function(file,data,response) {
				console.log(data);
				var arrData=$.parseJSON(data);
				console.log(arrData);
				if (arrData['Return']) {
					alert('Upload Success '+arrData['Result']);
				} else {
					alert('Upload Failed '+arrData['Result']);
				}
			}
		});
	});
</script>
<?php
	echo sprintf("<br> session_save_path=> '%s'",session_save_path());
	echo sprintf("<br> session_id=> '%s'",session_id());
	echo "<br> Before <br>".$_RETURN;
	echo "<br> After";
	foreach ($_SESSION as $Key => $Value) { echo sprintf("<br>session [%s] => '%s'",$Key,addslashes(urldecode($Value))); }
	foreach ($_REQUEST as $Key => $Value) { echo sprintf("<br>request [%s] => '%s'",$Key,addslashes(urldecode($Value))); }
	foreach ($_COOKIE as $Key => $Value) { echo sprintf("<br>cookies [%s] => '%s'",$Key,addslashes(urldecode($Value))); }
?>
</body>
