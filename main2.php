<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Accordion</title>
<link type="text/css" href="css/jquery-ui-1.8.21.custom.css" rel="stylesheet" />
<link type="text/css" href="css/display.css" rel="stylesheet" />
<script type="text/javascript" src="js/jquery-1.7.2.min.js"></script>
<script type="text/javascript" src="js/jquery-ui-1.8.21.custom.min.js"></script>
<script type="text/javascript">
	$(function(){
		// Accordion
		$("#accordion").accordion({
			autoHeight: false,
			navigation: true });
	});
	
	jQuery(document).ready(function(){
		$('.accordion .head').click(function() {
			$(this).next().toggle('slow');
			return false;
		}).next().hide();
	});

</script>
</head>
<? $menu=$_REQUEST['menu'];?>
<body>
<div id="page">
	<div id="head">
            <div style="float:left; margin-left:10px; margin-top:15px;">
                <img src="images/logo.png" style="float:left;">
            </div>
            <div style="float:right; margin-right:40px; margin-top:30px; text-align:left; font-family:tahoma; font-size:24pt; color:#636363;">InfoExpress Designer<br>
            <span style="float:left; padding-top:5px; text-align:left; font-family:tahoma; font-size:12pt; color:#b1b1b1;">and Schedule Management System</span>
            </div>
    </div>
	<div id="main">
    	<div id="left"><div style="margin:3px;"><? include "menu2.php";?></div></div>
        <div id="right">
        	<div class="well" style="margin:3px; overflow:inherit">
            	<iframe name="frmDisplay" id="frmDisplay" src="<?=$menu.".php";?>" frameborder="0" cellspacing="0" style="border-style: none;width: 100%; height: 600px;"></iframe>
            </div>
       	</div>
    </div>
    <div id="footer">
    	<div id="txtfooter">
			Copyright 2011-2012 Revenue Express Co., Ltd. All rights reserved.
		</div>
    </div>
</div>
</body>
</html>