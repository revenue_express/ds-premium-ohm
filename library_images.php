<head>
<?php 
	$DEBUG=FALSE;
	$TypeID=1;
	include("./includes/db_config.inc.php");
	include("./functions/library.func.php");
	include("./javascript.php");
	$iWidth=600;
	$iHeight=300;
	$sURL=sprintf("./library_upload.php?typeID=%d",$TypeID);
	$sWindowOption=sprintf("toolbar=no,location=no,status=no,menubar=no,scrollbars=yes,resizable=no");
	$sWindowOption.=sprintf(",width=%d,height=%d",$iWidth,$iHeight);
	$sWindowTarget='UploadWindows';
	$sOpenWindow=sprintf("window.open('%s','%s','%s')",$sURL,$sWindowTarget,$sWindowOption);
	$sOpenWindow="$('#dvUpload').dialog('open');";
	$sURL=sprintf("library_upload.php?typeID=%d",$TypeID);
	$sOpenWindow=sprintf("window.location.href='%s'",$sURL);
	
?>
<script type="text/javascript" src="js/library_file.js"></script>
<!--<script type="text/javascript" src="js/jquery-1.8.0.min.js"></script>
<script type="text/javascript" src="js/jquery-ui-1.8.23.custom.min.js"></script>
<script type="text/javascript" src="js/signage.js"></script>
-->
<script>
	$(function() {
		$( "input:submit, a, button", ".demo" ).button();
		$( "a", ".demo" ).click(function() { return false; });
	});
</script>
<link rel="stylesheet" href="css/custom-theme/jquery-ui-1.8.23.custom.css">
<link rel="stylesheet" href="css/StyleSheet.css">
<script type="text/javascript">
	$('document').ready(function() {
		file_list(<?=$TypeID;?>);
	});
</script>
<script src="js/jquery.uploadfile.min.js"></script>
<link href="css/uploadfile.min.css" rel="stylesheet">
</head>
<body>
<script type="text/javascript">
	$(function() {
		$('#dvRename').dialog({ autoOpen: false, modal: true, draggable: false });
		$('#dvRename').dialog("option","width",700);
		$('#dvRename').dialog("option","height",100);
		$('#dvRename').dialog("option","resizable",false);
	});

	function checkRename() {
		console.log("check");
		$('#btnRenameSave').attr('disabled',true);	
    var key = event.keyCode || event.charCode;
    if( key == 8 || key == 46) 
			$('#btnRenameSave').attr('disabled',false);	
		if ($('#txtRenameFName').val() != $('#txtOldFName').val()) {
			$('#btnRenameSave').attr('disabled',false);	
		}
	}
</script>
<div class="demo">
	<div id="dvRename" title="Description">
  <table>
    <tbody id="tblRename_Body">
    <tr>
      <td class="left top10" nowrap>
      <input type="hidden" id="txtOldFName" value="" size="40" maxlength="127">
      <input type="text" id="txtRenameFName" value="" size="40" maxlength="127" onKeyUp="checkRename();">
      <input type="hidden" id="txtRenameFID" value="">
      <input type="button" id="btnRenameSave" onClick="file_rename($('#txtRenameFID').val(),$('#txtRenameFName').val(),'dvRename',<?=$TypeID;?>);" value="Save"disabled>
      <input type="button" id="btnRenameCancel" onClick="$('#dvRename').dialog('close');" value="Cancel">
      </td>
    </tr>
    </tbody>
  </table>
	</div>
</div>

<div class="w-100 top15"><!-- .altbox for alternative box's color -->
	<div class="boxin">
        <div class="header">
            <h3><span>Image Library</span>
            <span>&nbsp; 
            <input class="btnTH" id="btnUpload" type="button" value="Upload" onClick="<?php echo $sOpenWindow;?>">&nbsp; &nbsp; 
            <input class="btnTH" id="btnListRefresh" type="button" value="refresh" onClick="file_list(<?=$TypeID;?>);">&nbsp;
            <input class="txt" id="txtSearch" type="text" value="" size="20" placeholder="กรุณาใส่คำค้นที่ต้องการ"> &nbsp;
            <input class="btnTH" id="btnSearch" type="button" value="search" onClick="table_search('tblList_Body',2,'txtSearch');">
						</span>
            </h3>
        </div>
        <table cellspacing="0">
          <thead>
            <tr>
                <th width="100">Preview</th>
                <th>Name / Description</th>
                <th width="10%">File size</th>
                <th width="10%">Dimension</th>
                <th width="9%">Operation</th>
            </tr>
          </thead>
        	<tbody id="tblList_Body">
        	</tbody>
      </table>
	</div>
</div>
</body>
