<?php
	include("./includes/db_config.inc.php");
	include("./includes/sys_config.inc.php");
	load_config();
?>
<!DOCTYPE html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" href="css/StyleSheet.css" type="text/css" />
<link rel="stylesheet" href="css/custom-theme/jquery-ui-1.8.23.custom.css">
<link rel="shortcut icon" href="images/RE%20icon_24x24.PNG" type="image/png" />
<title>Revenue Express | บริษัท เรเวนิว เอ็กซ์เพรส จำกัด</title>
<style type="text/css">
body{ 
	margin:auto; 
	padding:0px;
	/* background: #FFF url(images/bgfoot.png) repeat-x; background-position:bottom;*/
	overflow:hidden;
}
.company  {
	color:#999; font-weight:bold; font-style:normal; text-decoration: none;
}
</style>
<?php include("./javascript.php");?>
<script type="text/javascript" src="jquery/jquery.session.js"></script> 
</head>
<?php
	$menu="";	
	if( isset($_REQUEST['menu'])) { $menu=$_REQUEST['menu']; }
?>
<body>
<div id="usrInfo" style="display:none;">
<input type="text" id="usr_id" value="<?=$_SESSION['usr_id'];?>" />
<input type="text" id="usr_login" value="<?=$_SESSION['usr_login'];?>" />
<input type="text" id="usr_name" value="<?=$_SESSION['usr_name'];?>" />
<input type="text" id="usr_level" value="<?=$_SESSION['usr_level'];?>" />
<input type="text" id="usr_com_id" value="<?=$_SESSION['usr_com_id'];?>" />
<input type="text" id="usr_company" value="<?=$_SESSION['usr_company'];?>" />
<input type="text" id="server_ip" value="<?=$_SERVER['SERVER_ADDR'];?>" />
<input type="text" id="server_path" value="<?=$_SESSION['srv_path'];?>" />
<input type="text" id="lvl_weight" value="<?=$_SESSION['lvl_weight'];?>" />
<br>
<?php print_r($_SESSION);?>
</div>
<script>
	
	var Count = 0;
	//$('iframe.auto-height').iframeAutoHeight({minHeight: 500});
	$(document).ready(function(){
		//alert("On Load => "+$.session("ses_header"));	
		if($.session("ses_header")=="hide"){
			$("div#header").hide();
			$("#show_header").show();
			$("#hidden_header").hide();
		}else{
			$("div#header").show();
			$("#show_header").hide();
			$("#hidden_header").show();
		}
		
		formate_screen();
	})
	 
	 function formate_screen(){
	 	var allheight = window.innerHeight;// document.body.offsetHeight;
		
		if($.session("ses_header")=="hide"){
			var div_head = 0;
			var div_foot = 0;
		}else{
			var div_head = 146;
			var div_foot = 60;
		}
		var div_menu = 32;
		var div_main = allheight-div_head-div_menu-div_foot;
		
		$('#frmDisplay').height(div_main);
		$('div#main').css({
     		minHeight: (div_main-4.5)
    	});
	 }
	 
	 function theheight(){
		 var the_height=document.getElementById('frmDisplay').contentWindow.
		 document.body.scrollHeight;

		// alert("the_height [1] = "+the_height);
		// if(the_height>500){
		//	 the_height=500;
		//	 formate_screen();
		// }
		// alert("the_height [2] = "+the_height);
		 return the_height;
		 }
	 
	 function calcHeight(){
		//find the height of the internal page
		 //alert("On fc calcHeight");
		 
		 //alert("the_height =>"+the_height);
		 //change the height of the iframe
		 //alert("Count = "+Count);
		 document.getElementById('frmDisplay').height=theheight();
		 //Count++;
	 }
	 
	 
	 $(document).ready(function(){
		 //$('iframe').iframeAutoHeight({debug: true});  
		 $("#main").mouseover( //mousemove  mouseover focusin
	 		function(){
				//calcHeight();
			}
		 );
		 
		 $('#frmDisplay:button').click( 
		 //mousemove  mouseover focusin
	 		function(){
				//alert("frmDisplay:button");
			}
		 );

		 $("#show_header").click(
		 	function(){
				$("div#header").show();
				$("div#footer").show();
				$("#show_header").hide();
				$("#hidden_header").show();
				
				//alert("Now => "+$.session("ses_header"));
				$.session("ses_header","show");
				//alert("After => "+$.session("ses_header"));
				formate_screen();
			}
		 );
		 
		 $("#hidden_header").click(
		 	function(){
				$("div#header").hide();
				$("div#footer").hide();
				$("#show_header").show();
				$("#hidden_header").hide();
				
				//alert("Now => "+$.session("ses_header"));
				$.session("ses_header","hide");
				//alert("After => "+$.session("ses_header"));
				formate_screen();
			}
		 );
		 
	});	  

</script>
<center>
	<div id="hidden_header" style="position:fixed; top:0px; right:0px;"><img src="images/btnHide.gif"></div>
  <div id="show_header" style="position:fixed; top:0px; right:0px;"><img src="images/btnShow.gif"></div>
	<div id="index">
   	<div id="header">
    	<div style="float:left; padding:20px 0 10px 30px; width:50%;">
      	<div style="text-align:left"><img src="images/info express_Header.png"></div>
       	<div style="text-align:left; padding-top:8px; font-size:32px; text-shadow: 0.1em 0.1em #F0F0F0; color:#06C;">Designer & Schedule Management System</div>
      </div>
      <div style="padding:5px 20px 10px 0;">
            	<!--div style="text-align:right; float:right; font-size:30px; padding:70px 0 0 20px;">
                <img src="images/logo3.png"></div-->
                <!--div style="text-align:right; float:right"><img src="images/logo2.png"></div-->
      </div>
    </div>
    <div id="menu">
<?php include "menu.php"; ?>
    </div>
    <div id="main">       
    	<iframe name="frmDisplay" id="frmDisplay" src="<?php if($menu!=""){ echo $menu.".php"; }else{ echo "home.php"; }?>"  frameborder="0" scrolling="yes" align="top" width="100%" height="100%" style="border-style:none; margin-top:-35px;"></iframe>
    </div>
    <div id="footer">
    	<div class="left">&nbsp;</div>
      <div class="center">
      	<div style="margin-top:27px;">Copyright 2012 <a class="company" href="http://www.revenue-express.com" target="_new">Revenue Express Co., Ltd.</a> All rights reserved. Version <span title="<?php echo $_RETURN;?>">2.3.0<span> (Neptune) </div>
    	</div>
    	<div class="right">&nbsp;</div>
  	</div> 
  </div>
</center>
<div class="demo">
	<div id="dvAlert" title="">
  	<br>
		<h3 id="dvAlertMsg"></h3>
	</div>
</div>
<script>
$(document).ready(function() {
	$('#dvAlert').dialog({ autoOpen: false, modal: true, draggable: false });
	$('#dvAlert').dialog("option","width",900);
	$('#dvAlert').dialog("option","resizable",false);
});
</script>
</body>
</html>