<?
	$DEBUG=FALSE;
	$ComID=1;
	include("./includes/db_config.inc.php");
//	include("./includes/sys_config.inc.php");
	foreach ($_REQUEST as $Key => $Value) { $$Key=addslashes(urldecode($Value)); }

	$URL=sprintf("%s/ajax/schedule_list.php?ComID=%d",$LocalHost,$ComID);
	$Data=file_get_contents($URL);
	$arrSchedule=json_decode($Data,true);

	$URL=sprintf("%s/ajax/client_list.php?ComID=%d",$LocalHost,$ComID);
	$Data=file_get_contents($URL);
	$arrClient=json_decode($Data,true);
?>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<? include("./javascript.php");?>
<link rel="stylesheet" href="css/custom-theme/jquery-ui-1.8.23.custom.css" />
<link rel="stylesheet" href="css/StyleSheet.css" />
<style type="text/css"> 
.Schedule table { font-size:!important 10px; }
.Schedule table th { font-size:!important 10px; }
.Schedule table td { font-size:!important 10px; }
textarea.Result { font-family:Fixedsys, Arial,"Lucida Sans Unicode"; font-size:!important 9px; font-family;}
</style>
<script type="text/javascript">
	$(document).ready(function() {
		$('#tabs').tabs();
	});

	function GenScheduleFile() {
		var URL='./new.1.all.schedule.php';
		var Params={"ClientID":$('#selClient').val()};
		$('#dvGenScheduleResult').empty();
		$.post(URL,Params,function(data) {
			console.log("GenScheduleFile");
//			console.log(data);
			if (data.length > 0) {
				$('#dvGenScheduleResult').val(data);
			} else {
				alert("");
			}
		});
	}

	function GenDownload() {
	}

	function SendContext() {
		var URL='./new.send.context.php';
		var Params={"sKey":$('#txtContextName').val(),
			"JData":$('#txtContextInput').val(),
			"sURL":$('#txtContextURL').val()};
		console.log("SendContext");
		console.log('URL=>'+URL);
		console.log(Params);
		$.post(URL,Params,function(data) {
			console.log(data);
			if (data != NULL) {
				if (data.length > 0) {
					$('#txtContextResult').val(data);
				} else {
					alert("Error");
				}
			} else {
				console.log('Error No Return');
			}
		},"json");
	}

	function SendCommand() {
		var URL='http://'+$('#txtCommandIP').val()+$('#selCommand').val();
		var Params={};
		console.log("SendCommand");
		console.log('URL=>'+URL);
		console.log(Params);
		$.post(URL,Params,function(data) {
			console.log(data);
			if (data != NULL) {
				if (data.length > 0) {
					$('#txtCommandResult').append("<br>"+URL);
					$('#txtCommandResult').append("<br>"+data);
				} else {
					alert("Error");
				}
			} else {
				console.log('Error No Return');
			}
		},"json");
	}
</script>
</head>
<body>
<?
	if ($DEBUG) {
		echo "arrSchedule=> <pre>"; print_r($arrSchedule); echo "</pre>";
		echo "arrClient=> <pre>"; print_r($arrClient); echo "</pre>";
	}
?>
<div class="demo" id="tabs">
	<ul>
  	<li><a href="#tabs-1">Send Schedule File</a></li>
  	<li><a href="#tabs-2">Send Download File</a></li>
  	<li><a href="#tabs-3">Send Context</a></li>
  	<li><a href="#tabs-4">Send Command</a></li>
  </ul>
  <div id="tabs-1">
    <div class="demo boxin">
      <div class="header">Test Send File From Schedule</div>
      <table class="demo">
        <thead><tr><th class="right">Data</th><th>Value</th></tr></thead>
        <tr>
          <td class="right">Client Address</td>
          <td>
            <select id="selClient">
  <?	for ($iCount=0; $iCount<count($arrClient); $iCount++) { ?>
              <option value="<?=$arrClient[$iCount]['cli_id'];?>"><?=$arrClient[$iCount]['cli_name'];?> (<?=$arrClient[$iCount]['cli_address'];?>)</option>
  <?	} ?>
            </select><input type="button" value="Execute" onClick="GenScheduleFile();"/>
          </td>
        <tr valign="top"><td class="right">Result :</td><td><textarea class="Result" id="dvGenScheduleResult" cols="120" rows="25"></textarea></td></tr>
      </table>
    </div>
  </div> <!-- End tabs-1 -->
	<div id="tabs-2">
    <div class="demo boxin">
      <div class="header">Send File From Schedule</div>
      <table class="demo">
        <thead><tr><th class="right">Data</th><th>Value</th></tr></thead>
        <tr>
          <td class="right">Client Address</td>
          <td>
            <select id="selDownloadClient">
  <?	for ($iCount=0; $iCount<count($arrClient); $iCount++) { ?>
              <option value="<?=$arrClient[$iCount]['cli_id'];?>"><?=$arrClient[$iCount]['cli_name'];?> (<?=$arrClient[$iCount]['cli_address'];?>)</option>
  <?	} ?>
            </select>
          </td>
        </tr>
        <tr>
          <td class="right">Schedule</td>
          <td>
            <select id="selDownloadSchedule">
  <?	for ($iCount=0; $iCount<count($arrSchedule); $iCount++) { ?>
              <option value="<?=$arrSchedule[$iCount]['schh_id'];?>"><?=$arrSchedule[$iCount]['schh_name'];?></option>
  <?	} ?>
            </select>
          </td></tr>
          <tr>
          <td></td>
          <td>
          <input id="chkDownload" type="checkbox" />View Data
          <input id="chkDownloadRun" type="checkbox" />Run
          <input type="button" value="Execute" onClick="GenDownload();"/>
          </td></tr>
        <tr valign="top"><td class="right">Result :</td><td><textarea class="Result" id="dvDownloadResult" cols="120" rows="22"></textarea></td></tr>
      </table>
    </div>
  </div> <!-- End tabs-2 -->
	<div id="tabs-3">
    <div class="demo boxin">
      <div class="header">Send File From Schedule</div>
      <table class="demo">
        <tr>
        	<td class="right">URL</td>
        	<td><input id="txtContextURL" type="text" class="search" size="60" value="http://192.168.1.212:3001/addsch/"/></td>
        </tr>
        <tr valign="top">
          <td class="right"><input id="txtContextName" type="text" class="search" size="16" value="xschlist"/></td>
          <td>
              <textarea id="txtContextInput" cols="120" rows="20"></textarea>
              <input type="button" value="Execute" onClick="SendContext();"/>
          </td>
        </tr>
        <tr valign="top"><td class="right">Result :</td><td><textarea class="Result" id="txtContextResult" cols="120" rows="5"></textarea></td></tr>
			</table>
    </div>
  </div> <!-- End tabs-3 -->
	<div id="tabs-4">
    <div class="demo boxin">
      <div class="header">Send Command</div>
      <table class="demo">
        <tr>
          <td class="right">Client IP Address</td>
          <td><input id="txtCommandIP" type="text" class="search" size="20" value="192.168.1.212"/></td>
        </tr>
        <tr>
          <td class="right">Protocol</td>
          <td>
          	<select id="selCommand">
            <option value=":3001/getfinlist/">Get Final List</option>
            <option value=":3001/getfaillist/">Get Fail List</option>
            <option value=":3001/getpendinglist/">Get Pending List</option>
            <option value=":3003/startdownloadnew/">Start Download</option>
            <option value=":3003/hashall/">Hash All</option>
            <option value=":3003/clearfile/">Clear File</option>
            </select>
          </td>
        </tr>
        <tr valign="top"><td class="right">Result :</td>
        	<td>
          <input type="button" value="Execute" onClick="SendCommand();"/>
          <input type="button" value="Clear" onClick="ClearCommand();"/>
					<br /><textarea  class="Result" id="txtCommandResult" cols="120" rows="20"></textarea>
          </td>
        </tr>
			</table>
    </div>
  </div> <!-- End tabs-4 -->
</div>

</body>
</html>