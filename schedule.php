<? 
	include("./includes/db_config.inc.php");
//	include("./includes/sys_config.inc.php");
	$DEBUG=FALSE;
?>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<? include("./javascript.php");?>
<!--
<script type="text/javascript" src="./js/jquery-1.7.2.min.js"></script>
<script type="text/javascript" src="./js/jquery-ui-1.8.21.custom.min.js"></script>
<script type="text/javascript" src="./js/checkValid.js"></script>
<script type="text/javascript" src="./js/signage.js"></script>
-->
<!--link rel="stylesheet" href="./css/display.css"-->
<link rel="stylesheet" href="css/custom-theme/jquery-ui-1.8.23.custom.css">
<link rel="stylesheet" href="./css/StyleSheet.css">
<script type="text/javascript" src="./js/schedule.js"></script>
<script type="text/javascript">
	$('document').ready(function() {
		clear_allsection();
<?	if (isset($schID)) { ?>
		schedule_edit(<?=$schID;?>);
<?	} else {?>
		load_headlist();
<?	} ?>
	});

	$(function() {
		$( "#btnHeadReset,#btnChangeDisp" ).button();
		$( "#txtPeriodStart,#txtPeriodStop" ).datepicker({
			showOn: "button",
			buttonImage: "images/calendar.gif",
			buttonImageOnly: true,
			dateFormat: "dd/mm/yy"
		});
	});
/*
	$("#txtPeriodStart").change(function() {
		alert('txtPeriodStart Change');
		if ($.datepicker("#txtPeriodStart").getDate() > $.datepicker("#txtPeriodStop").getDate()) {
			alert('Start Date is More than Stop Date');
			$("#txtPeriodStart").addClass("Error");
		}
	});
	
	$("#txtPeriodStop").change(function() {
		alert('txtPeriodStop Change');
		if ($.datepicker("#txtPeriodStop").getDate() < $.datepicker("#txtPeriodStart").getDate()) {
			alert('Stop Date is Less than Start Date');
			$("#txtPeriodStop").addClass("Error");
		}
	});
*/
</script>
<style type="text/css">
.spMenu { cursor: pointer; }
.tdFileSize { text-align:right; padding-right: 5px;}
</style>
</head>
<body>
<div><?	if ($DEBUG) { echo "request<pre>"; print_r($_REQUEST); echo "</pre>"; } ?></div>
<div id="dvHeadResult" style="margin-left:0px; margin-top:0px; padding-left:0px; width:100%">
  <div class="boxin w-100"><!-- .altbox for alternative box's color -->
    <div class="header">
      <h3>
      <span>Schedule Management</span>
      <span style="margin-left:25px;" id="spHeadSearch">
      <input class="btnTH" id="btnListRefresh" type="button" value=" refresh " onClick="load_headlist();">&nbsp;
      <input class="txt" id="txtSearch" type="text" value="" size="20">&nbsp;
      <input class="btnTH" id="btnSearch" type="button" value=" search " onClick="table_search('showHead_Data',1,'txtSearch');">
<!--		    <input id="btnEdit" type="button" value="edit" onClick="schedule_edit(parseInt($('#txtSearch').val()));"> -->
      </span>
      <span style="float:right; margin-right:30px; margin-top:15px;">
      <input type="button" class="btnTh" id="btnHeadAdd" value=" Add " onClick="schedule_add_new();" title="Add schedule">
      </span>
			</h3>
    </div>
    <table width="100%" cellspacing="0">
    <thead>
      <tr>
        <th>Schedule Name</th>
        <th width="15%">Display Size</th>
        <th width="10%">Template</th>
        <th colspan="4" width="10%">Operation</th>
        </tr>
    </thead>
    <tbody id="showHead_Data">
    </tbody>
    </table>
  </div>
</div>
<div id="dvHeadEdit" style="display:block; ">
<span><input class="demo btnJQR" type="button" id="btnChangeDisp" value=" Back " onClick="clear_allsection();load_headlist();" /></span><p>
<? 
$onChange="EnableObject('btnSaveLayout',false);";
//$onChange.=sprintf("changeInputValue('txtLayoutName','%s',document.getElementById('txtLayoutName').value);",$sDisplay);
$onFocus="EnableObject('btnHeadSave',! checkEqual('txtSchedule','txtOldSchedule'));";
$onHeadSave="schedule_head_edit(document.getElementById('ScheduleID').value,";
$onHeadSave.="document.getElementById('txtSchedule').value,";
$onHeadSave.="document.getElementById('DispID').value)";
$onHeadReset="setObjValue('txtOldSchedule','txtSchedule');";
$onHeadReset.="setObjValue('txtOldDisplay','txtDisplay');";
$onHeadReset.="setObjValue('OldDispID','DispID');";
$onHeadReset.="EnableObject('btnHeadSave',false);";

$URL="./popup_display.php?DispID=DispID&&DispName=txtDisplay&btnData=btnHeadSave&OldID=";
$Option=sprintf("width=%d,height=%d,location=no,menubar=no,resizeable=no,scrollbars=no,status=no,toolbar=no",710,280);
$onChangeDisp=sprintf("window.open('%s','popup_display','%s');",$URL,$Option);
$onChangeDisp="schedule_display_select();";
?>
    <div class="w-80 boxin" id="tblHeadEdit">
			<div class="demo header">
      <h3>Schedule Information</h3>
      </div>
      <table>
      	<tr class="header"><th>Schedule name</th><th>Display resolution</th><th>Operation</th></tr>
        <tr>
        	<td>
          <input type="text" class="txt" id="txtSchedule" name="txtSchedule" size="60" maxlength="64" onKeyPress="<?=$onFocus;?>"/>
          <input type="hidden" id="txtOldSchedule">
          </td>
          <td>
          <input type="hidden" id="DispID" name="DispID"/><input type="hidden" id="OldDispID" name="OldDispID"/>
          <input type="text" id="txtDisplay" name="txtDisplay" size="30" class="txt"  maxlength="64" readonly disabled/>
          <input type="hidden" id="txtOldDisplay" name="txtOldDisplay"/> 
          <input class="demo btnJQR" type="button" id="btnChangeDisp" value=" ... " onClick="<?=$onChangeDisp;?>" />
					</td>
					<td>
				<input type="hidden" id="ScheduleID" name="ScheduleID"/>
        <input type="button" class="demo btnJQR" id="btnHeadSave" value="Save" onClick="<?=$onHeadSave;?>" disabled/> &nbsp;
        <input type="button" class="demo btnJQR" id="btnHeadReset" value="Reset" onClick="<?=$onHeadReset;?>"/>
          </td>
        </tr>
      </table>
 <!--
        <div class="left head">Schedule Information</div>
        <div class="dvtr top15">
            <div class="dvtd_left w-35 top5">Schedule Name :</div>
            <div class="dvtd_right">
                <input type="text" class="txt" id="txtSchedule" name="txtSchedule" size="40" maxlength="64" onKeyPress="<?=$onFocus;?>"/>
                <span><input type="hidden" id="txtOldSchedule"></span>
            </div>
        </div>
        <div class="dvtr top5">
            <div class="dvtd_left w-35 top5">
            Display Size :<input type="hidden" id="DispID" name="DispID"/><input type="hidden" id="OldDispID" name="OldDispID"/></div>
            <div class="dvtd_right demo">
                <input type="text" id="txtDisplay" name="txtDisplay" size="30" class="txt"  maxlength="64" readonly disabled/><input type="hidden" id="txtOldDisplay" name="txtOldDisplay"/>
                <input type="button" id="btnChangeDisp" value=" Change " onClick="<?=$onChangeDisp;?>" />
            </div>
        </div>
    	<div class="dvtr top15 demo">
        <div class="dvtd_left w-35">
        <input type="button" class="btnJQR" id="btnHeadSave" value="Save" onClick="<?=$onHeadSave;?>" disabled/>
        </div>
        <div class="dvtd_right">
          <input type="button" id="btnHeadReset" value="Reset" onClick="<?=$onHeadReset;?>"/>
        </div>
      </div>

    </div>
-->
	</div>
</div>
<div id="dvPeriod" class="top15" style="display:block;">
<?
$onPeriodAdd="$('#tblPeriodEdit').show();";
$onPeriodAdd.="$('#tblPeriodLine').hide();";
$onPeriodAdd.="document.getElementById('btnPeriodSave').value='Add';";
$onPeriodAdd.="document.getElementById('PeriodID').value='';";
$onPeriodCancel="$('#tblPeriodEdit').hide();";
$onPeriodCancel.="document.getElementById('txtPeriodStart').value='';";
$onPeriodCancel.="document.getElementById('txtPeriodStop').value='';";
$onPeriodCancel.="schedule_period_list(document.getElementById('ScheduleID').value);";

$onPeriodSave="schedule_period_add(document.getElementById('ScheduleID').value";
$onPeriodSave.=",document.getElementById('txtPeriodStart').value";
$onPeriodSave.=",document.getElementById('txtPeriodStop').value);";
$onPeriodEdit="schedule_period_edit(document.getElementById('ScheduleID').value";
$onPeriodEdit.=",document.getElementById('PeriodID').value";
$onPeriodEdit.=",document.getElementById('txtPeriodStart').value";
$onPeriodEdit.=",document.getElementById('txtPeriodStop').value);";
$onPeriodClick="if (document.getElementById('PeriodID').value>0) { ";
$onPeriodClick.=$onPeriodEdit." } else { ";
$onPeriodClick.=$onPeriodSave." }";

?>
    <div class="w-60 boxin">
        <div class="header">
        <span>Schedule Period</span>
        <span id="spPeriodHead" style="margin-left:15px;">
        <input class="btnTh" type="button" id="btnPeriodAdd" value="Add" onClick="<?=$onPeriodAdd;?>" />
        </span>
        </div>
        <table cellspacing="0">
            <thead id="tblPeriodHead">
                <tr>
                    <th class="w-45">Start</th>
                    <th>Stop</th>
                    <th colspan="2" class="w-17">Operation</th>
                </tr>
            </thead>
            <thead id="tblPeriodEdit" style="display:none;">
                <tr>
                    <td class="center">
                        <input type="hidden" id="txtOldPeriodStart"/>
                        <input type="text" class="txt" style="margin-right:5px;" id="txtPeriodStart" name="txtPeriodStart" size="16" maxlength="16"/>
                    </td>
                    <td class="center">
                        <input type="hidden" id="txtOldPeriodStop"/>
                        <input type="text" class="txt" style="margin-right:5px;" id="txtPeriodStop" name="txtPeriodStop" size="16" maxlength="16"/>
                    </td>
                    <td colspan="2" class="center">
                        <input type="hidden" id="PeriodID" name="PeriodID"/>
                        <input class="btnTh" type="button" id="btnPeriodSave" value="Save" onClick="<?=$onPeriodClick;?>"/>
                        &nbsp; &nbsp;
                        <input class="btnTh" type="button" id="btnPeriodCancel" value="Cancel" onClick="<?=$onPeriodCancel;?>"/>  
                    </td>
                </tr>
            </thead>
            <tbody id="tblPeriodLine">
                <tr>
                    <td>XXXX-XX-XX YY:YY:YY</td>
                    <td>XXXX-XX-XX YY:YY:YY</td>
                    <td>Ed</td>
                    <td>De</td>
                </tr>
            </tbody>
        </table>
	</div>
</div>
<div id="dvLayout" class="top15" style="display:block;">
<? $onLayoutAdd=sprintf("schedule_layout_popup('ScheduleID');"); ?>
    <div class="w-100 boxin">
        <div class="header">
        <h3>
        <span>Template List</span>
        <span id="spPeriodHead" style=" float:right; margin-right:30px; margin-top:7px;">
				<input class="btnTh" style="height:30px" type="button" id="btnLayoutAdd" value="Add" onClick="<?=$onLayoutAdd;?>" />
        </span>
        </h3>
        </div>
        <table cellspacing="0">
        	<thead id="tblLayoutHead">
                <tr>
                    <th class="left" rowspan="2"><div class="center">Template Name / Layout Name</div></th>
                    <th colspan="7" width="20%">Day of Week</th>
                    <th rowspan="2" width="5%">Start</th>
                    <th rowspan="2" width="5%">Stop</th>
                    <th colspan="3" rowspan="2" class="right" width="5%">Operation</th>
                </tr>
                <tr>
                    <th class="day_00">Sun</th>
                    <th class="day_00">Mon</th>
                    <th class="day_00">Tue</th>
                    <th class="day_00">Wed</th>
                    <th class="day_00">Thu</th>
                    <th class="day_00">Fri</th>
                    <th class="day_00">Sat</th>
                </tr>
        	</thead>
        	<tbody id="tblLayoutLine">
        		<tr>
                    <td>
                    	<span style="font-weight:bolder">Template :</span><span>1234</span><br>
                    	<span style="font-weight:bolder">Layout :</span><span>1234</span>
                    </td>
                    <td>X</td>
                    <td>X</td>
                    <td>X</td>
                    <td>X</td>
                    <td>X</td>
                    <td>X</td>
                    <td>X</td>
                    <td>YY:YY:YY</td>
                    <td>YY:YY:YY</td>
                    <td>S</td>
                    <td>V</td>
                    <td>E</td>
                    <td>D</td>
        		</tr>
        	</tbody>
        </table>
    </div>
</div>
</body>
</head>