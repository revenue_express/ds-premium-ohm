<? 
	include("./includes/db_config.inc.php");
	include("./includes/sys_config.inc.php");
	include("./functions/command.func.php");
	$DEBUG=FALSE;
	$Data=NULL;
//	$Data=file_get_contents('http://localhost/signage/ajax/group_list.php');
//	$Group=json_decode($Data,true);
	$Data=file_get_contents('http://localhost/signage/ajax/client_list.php');
	$Client=json_decode($Data,true);
//	$Data=file_get_contents('http://localhost/signage/ajax/schedule_list.php');
//	$Schedule=json_decode($Data,true);

?>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<? include("./javascript.php"); ?>
<link rel="stylesheet" href="css/custom-theme/jquery-ui-1.8.23.custom.css">
<link rel="stylesheet" href="css/StyleSheet.css">
<script language="javascript">

	$(function() {
		$('#showData').accordion();
	});
	
	function chkGroup(id,ListItem) {
//		alert("id "+id+"\nListItem = "+ListItem);
		var objGroup=document.getElementById('AllGroup_'+id);
		var bCheck=objGroup.checked;
		var Items=ListItem.split(",");
		objGroup.checked = bCheck;
		for (var i=0; i < Items.length; i++) {
			var chkObj=document.getElementById('chkClient_'+Items[i]);
			if (chkObj) {
				chkObj.checked = bCheck;
//				if (bCheck) {
					chkClient('txtList',Items[i]);
//				}
			}
		}
	}
	
	function chkClient(objName,ClientID) {
//		alert("Change "+ClientID);
		var ListItem=document.getElementById(objName).value;
		ListItem+=","+ClientID;
		var Items=ListItem.split(",");
		Items.sort();
		var OldData=0,NewData=0;
		var ListData="0";
		var ListValue="0";
		for (var iCount=0; iCount<Items.length; iCount++) {
			NewData=parseInt(Items[iCount]);
			if (NewData != OldData) {
				var objCheck=document.getElementById('chkClient_'+NewData);
				if (objCheck) {
					if (objCheck.checked) {
						ListData+=","+NewData;
						ListValue+=","+objCheck.value;
						OldData=NewData;
						
					}
				}
			}
		}
		document.getElementById(objName).value=ListData;
		document.getElementById('txtValue').value=ListValue;

		$('#btnSelect').attr("disabled","disabled");
		if (ListValue.toString().length > 2) {
			$('#btnSelect').removeAttr("disabled");
		}
	}
	
	function onSelect() {
		var Display="Do you Confirm to select this schedule?";
		if (confirm(Display)) {
			return true;
		}
		return false;
	}
</script>
</head>
<body>
<?
//	echo "<pre>"; print_r($_REQUEST); echo "</pre>";
	$txtResult='';
	if ( (isset($txtValue)) && (isset($selSchedule)) ) {
//		$DEBUG=TRUE;
		if ($DEBUG) { echo "Client<pre>"; print_r($Client); echo "</pre>"; }
//		if ($DEBUG) { echo "ClientList<pre>"; print_r($ClientList); echo "</pre>"; }
//		if ($DEBUG) { echo "ClientList<pre>"; print_r($ClientList); echo "</pre>"; }
//		if ($DEBUG) { echo "Request<pre>"; print_r($_REQUEST); echo "</pre>"; }
		$ClientList=split(",",$txtValue);
//		if ($DEBUG) { echo "ClientList<pre>"; print_r($ClientList); echo "</pre>"; }

		for($iClient=1; $iClient<count($ClientList); $iClient++) {
			$ClientID=$ClientList[$iClient];
			if ($DEBUG) { echo sprintf("<br>%d : %d",$iClient,$ClientID); }
			$ClientNumber=0;
			while (($ClientNumber >= 0) && ($ClientNumber < count($Client)) && ($Client[$ClientNumber]['cli_id'] <> $ClientID) ) {
				if ($DEBUG) { echo sprintf("<br> Skip => %d",$ClientNumber); }
				$ClientNumber++;
			}
			if ($DEBUG) { echo sprintf("<br> Found on => %d",$ClientNumber); }
			if ( ($ClientNumber >= 0) && ($ClientNumber < count($Client)) ) {
				$URL=sprintf("http://localhost/signage/ajax/schedule_client_add.php?ClientID=%d&SchID=%d",$ClientID,$selSchedule);
				if ($DEBUG) { echo sprintf("URL => %s",$URL);}
				$Return=file_get_contents($URL);
				if ($Return == "1") {
					$txtResult.=sprintf("\nAdd New Schedule(%d) to Client (%s) .... successful.",$selSchedule,$ClientID);
				} else {
					$txtResult.=sprintf("\nExists Schedule(%d) on Client (%s)...... failed",$selSchedule,$ClientID);
				}
				$CMD=command_add_schedule($ComID,$ClientID,$selSchedule);
				$Link=sprintf("%s/?cmd=setsch&cmdid=%d&p1=%d",$Client[$ClientNumber]['cli_url'],$CMD,$selSchedule);	
				$Return=file_get_contents($Link);
				if ($Return === true) {
//					$txtResult.=sprintf("\n%s => %s",$Link,$Return);
					$txtResult.=sprintf("\nSend schedule to Client (%d).... successful.",$ClientID);
					if ($DEBUG) { echo "<br>add Return '$CMD'"; }
				} else {
//					$txtResult.=sprintf("\n%s => %s",$Link,$Return);
					$txtResult.=sprintf("\nSend schedule to Client (%d).... failed.",$ClientID);
				}
			} else {
				$txtResult.=sprintf("\nNot Found Client => %d",$ClientID);
			}
		}
	}
	

	$Data=file_get_contents('http://localhost/signage/ajax/group_list.php');
	$Group=json_decode($Data,true);
	if ($DEBUG) { echo "<pre>"; print_r($Data); echo "</pre>"; }

	$Data=file_get_contents('http://localhost/signage/ajax/schedule_list.php');
	$Schedule=json_decode($Data,true);
	if ($DEBUG) { echo "<pre>"; print_r($Data); echo "</pre>"; }

	$Data=file_get_contents('http://localhost/signage/ajax/client_list.php');
	if ($DEBUG) { $Client=json_decode($Data,true); }
	if ($DEBUG) { echo "<pre>"; print_r($Data); echo "</pre>"; }

//	echo "<br>Schedule<pre>"; print_r($Schedule); echo "</pre>";
	
?>
<div class="demo w-80">
<form name="SendSchedule" method="post" action="">
<div class="header">
	<h3>
  	<span>Schedule  Select :: </span>
    <span>
  	<select id='selSchedule' name="selSchedule">
<? 
	for ($iCount=0; $iCount<count($Schedule); $iCount++) {
		$Name=sprintf("%s [%s]",$Schedule[$iCount]['schh_name'],$Schedule[$iCount]['disp_size']);
		echo sprintf("<option value=\"%d\">%s</option>",$Schedule[$iCount]['schh_id'],$Name);
	}
?>
			</select>
    </span>
    <span><input class="btnSky" type="submit" id="btnSelect" name="btnSelect" value="Select" onClick="onSelect();" disabled></span>
	</h3>
<input type="hidden" id="txtList" name="txtList" size="80"> 
<input type="hidden" id="txtValue" name="txtValue" size="80">
</div>
</form>
<? if (strlen($txtResult) > 0){ ?>
<div>
<textarea  id="txtURL" cols="120" rows="4"><?=$txtResult;?></textarea>
</div>
<br>
<?	}?>
<div class="header"><h3>Client Select ::</h3></div>
<div id="showData">
<?
	for ($iGroup=0; $iGroup < count($Group); $iGroup++) {
		$GroupID=$Group[$iGroup]['grp_id'];
		$ClientData="";
		$ClientItem=array();
		$ClientList='0';
		$ClientCount=0;
		for ($iClient=0; $iClient < count($Client); $iClient++) {
			if ($Client[$iClient]['cli_grp_id'] == $GroupID) {
				$ClientCount++;
				$ClientID=$Client[$iClient]['cli_id'];
				$ClientName=$Client[$iClient]['cli_name'];
				$ClientItem[]=array("ID"=>$ClientID,"Name"=>$ClientName);
				$ClientList.=sprintf(",%d",$ClientID);
//				$onClick=sprintf("chkClient('txtList',%d);",$ClientCount);
				$onClick=sprintf("chkClient('txtList',%d);",$ClientID);
//				$ClientData.=sprintf("<span><input type='checkbox' id='chkClient_%d' onClick=\"%s\" value='%d'>%s</span>",$ClientCount,$onClick,$ClientID,$ClientName); 
				$ClientData.=sprintf("<span><input type='checkbox' id='chkClient_%d' onClick=\"%s\" value='%d'>%s</span>",$ClientID,$onClick,$ClientID,$ClientName); 
			}
		}
		$onClick=sprintf("chkGroup(%d,'%s');",$GroupID,$ClientList);
		$GroupData=sprintf("<input type='checkbox' id='AllGroup_%d' onClick=\"%s\" value='%d'> All in Group.",$GroupID,$onClick,$GroupID);
		if ($ClientCount > 0) {
			echo sprintf("<h3><a href='#'>[Group of %s]</a></h3>",$Group[$iGroup]['grp_name']);
			echo sprintf("<div>%s %s</div>",$GroupData,$ClientData);
		}
	}
?>
</div>
</div>

</body>
</html>	