<? 
	$DEBUG=FALSE;
	include("./includes/db_config.inc.php");
	include("./includes/sys_config.inc.php");
	$CSSName=NULL;
	$LayoutItems=NULL;
	$Data=NULL;

	$Data=file_get_contents($LocalHost.'/ajax/client_list.php');
	$Client=json_decode($Data,true);
	$Data=file_get_contents($LocalHost.'/ajax/schedule_list.php');
	$Schedule=json_decode($Data,true);
	
	$Data=NULL;
	if ( (isset($schID)) && (isset($ClientID)) ){
		if ($DEBUG) { echo "<pre>"; print_r($_REQUEST); echo "</pre>"; }
		$URL=$LocalHost.'/gen.schedule.file.php?schID='.$schID."&ClientID=".$ClientID;
		$URL.="&Run=";
		$URL.=(isset($chkRun))?"1":"0";
		$URL.="&Local=";
		$URL.=(isset($chkLocal))?"1":"0";
		$Data=file_get_contents($URL);
//		$Data=str_replace("},{","},{\n",$Data);
//		$Data=str_replace(":[{",":\n[{",$Data);
//		$Data=str_replace("}]","}]\n",$Data);
		$Data=str_replace("\/","/",$Data);
		$Table=json_decode($Data,true);
	}
?>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<?	include("./javascript.php");?>
<link rel="stylesheet" href="css/custom-theme/jquery-ui-1.8.23.custom.css">
<link rel="stylesheet" href="css/StyleSheet.css">
</head>
<body>
<form>
<table border="1" class="w-50">
	<tr>	<th>Client</th><th>Schedule</th><th>Action</th><th>Exec</th><th>Local</th></tr>
	<tr>
  	<td>
    	<select name="ClientID">
<?
		for($iRun=0; $iRun<count($Client); $iRun++) {
			$Selected=($Client[$iRun]['cli_id'] == $ClientID)?" SELECTED":"";
			echo sprintf("<option value='%d'%s>%s</option>",$Client[$iRun]['cli_id'],$Selected,$Client[$iRun]['cli_name']);
		}
?>
      </select>
     </td>
     <td>
    	<select name="schID">
<?
		for($iRun=0; $iRun<count($Schedule); $iRun++) {
			$Selected=($Schedule[$iRun]['schh_id'] == $schID)?" SELECTED":"";
			echo sprintf("<option value='%d'%s>%s</option>",$Schedule[$iRun]['schh_id'],$Selected,$Schedule[$iRun]['schh_name']);
		}
?>
      </select>
     </td>
    <td align="center"><button>selected</button></td>
    <td align="center"><input type="checkbox" name="chkRun"></td>
    <td align="center"><input type="checkbox" name="chkLocal"></td>
  </tr>
</table>
</form>
<table border="1">
<tr><th>File</th><th>Address</th><th>Request</th></tr>
<?	if (count($Table) > 0) {?>
<tr><td colspan='3'><?=$URL?></td></tr>
<? for ($iCount=0; $iCount<count($Table); $iCount++) { ?>
<tr>
	<td><?=$Table[$iCount]['name'];?></td>
  <td>
<?	if (isset($chkRun)) {?>
  <span title="<?=$Table[$iCount]['addr'];?>">Link</span>
<? } else { ?>
  <a href="<?=$Table[$iCount]['addr'];?>" target="_new" title="<?=$Table[$iCount]['addr'];?>">Link</a>
<? } ?>
  </td>
	<td>
	<? 
	if (isset($chkRun)) { 
		$Return=substr($Table[$iCount]['return'],0,60);
		if (strlen($Table[$iCount]['return'])>60) { $Return.=" ... "; }
		echo sprintf("<span title='%s'>%s</span>",$Table[$iCount]['return'],$Return);
	}
	?>
  </td>
</tr>
<?	} ?>
<?	} else { ?>
<tr><td colspan='3'>No Record.</td></tr>
<? } ?>
</table>
</body>
</html>	