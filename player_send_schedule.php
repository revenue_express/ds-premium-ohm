<? 
	include("./includes/db_config.inc.php");
	include("./includes/sys_config.inc.php");
	include("./functions/command.func.php");
	$DEBUG=FALSE;
	$Data=NULL;
	$Data=file_get_contents($LocalHost+'/ajax/client_list.php');
	$Client=json_decode($Data,true);
	$Data=file_get_contents($LocalHost+'/ajax/schedule_list.php');
	$Schedule=json_decode($Data,true);

?>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<script type="text/javascript" src="./js/jquery-1.7.2.min.js"></script>
<script type="text/javascript" src="./js/jquery-ui-1.8.21.custom.min.js"></script>
<script type="text/javascript" src="./js/jquery-ui-timepicker-addon.js"></script>
<script type="text/javascript" src="./js/checkValid.js"></script>
<script type="text/javascript" src="./js/signage.js"></script>
<link rel="stylesheet" href="./css/custom-theme/jquery-ui-1.8.22.custom.css">
</head>
<body>
<?
	if ( (isset($ClientID)) && (isset($ScheduleID)) ){
		$DEBUG=FALSE;
		$ClientInfo=NULL;$ScheduleInfo=NULL;

		for($iRun=0; $iRun<count($Client); $iRun++) {
			if ($Client[$iRun]['cli_id'] == $ClientID) { $ClientInfo[]=$Client[$iRun]; }
		}
		$CMD=command_add_schedule($ComID,$ClientID,$ScheduleID);
		if ($DEBUG) { echo "<br>add Return '$CMD'"; }
		$Link=sprintf("%s/?cmd=setsch&cmdid=%d&p1=%d",$ClientInfo[0]['cli_url'],$CMD,$ScheduleID);	
		$Return=file_get_contents($Link);
	}
	
?>
<form>
<div>
<table border="1">
	<tr>	<th>Client</th><th>Schedule</th><th>Action</th></tr>
	<tr>
  	<td>
    	<select name="ClientID">
<?
		for($iRun=0; $iRun<count($Client); $iRun++) {
			$Selected=($Client[$iRun]['cli_id'] == $ClientID)?" SELECTED":"";
			echo sprintf("<option value='%d'%s>%s</option>",$Client[$iRun]['cli_id'],$Selected,$Client[$iRun]['cli_name']);
		}
?>
      </select>
     </td>
     <td>
    	<select name="ScheduleID">
<?
		for($iRun=0; $iRun<count($Schedule); $iRun++) {
			$Selected=($Schedule[$iRun]['schh_id'] == $ScheduleID)?" SELECTED":"";
			echo sprintf("<option value='%d'%s>%s</option>",$Schedule[$iRun]['schh_id'],$Selected,$Schedule[$iRun]['schh_name']);
		}
?>
      </select>
     </td>
    <td><button>selected</button></td>
  </tr>
</table>
</div>
</form>
<input type="text" id="URL" value="<?=$Link;?>" size="80" maxlength="128">
</body>
</html>	
