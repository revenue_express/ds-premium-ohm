<?
	$DEBUG=TRUE;
	include("./includes/db_config.inc.php");
//	include("./includes/sys_config.inc.php");
//	$ComID=1;
//	foreach ($_REQUEST as $Key => $Value) { $$Key=addslashes(urldecode($Value)); }
	$Paths=split("/",$_SERVER['SCRIPT_NAME']);
	$Path="http://".$_SERVER['SERVER_ADDR'];
	for ($iCount=1; $iCount<count($Paths)-1; $iCount++) {
		$Path.=sprintf("/%s",$Paths[$iCount]);
	}
?>
<html>
<head>
<script type="text/javascript" src="js/jquery-1.7.2.min.js"></script>
<script type="text/javascript" src="js/jquery-ui-1.8.21.custom.min.js"></script>
<script type="text/javascript" src="./js/checkValid.js"></script>
<script type="text/javascript" src="./js/signage.js"></script>
<link rel="stylesheet" href="./css/custom-theme/jquery-ui-1.8.23.custom.css">
<link rel="stylesheet" href="./css/StyleSheet.css">
</head>
<script>
	function clearValue(id) {
		$('#txtPara'+id).val("");
		$('#txtValue'+id).val("");
	}
	
	function run(output) {
//		$('#'+output).val("");
		$('#'+output).html("");
		var URL=$('#txtProg').val();
		var Params={};
		if ($('#txtPara1').val() != null) Params[$('#txtPara1').val()] = $('#txtValue1').val();
		if ($('#txtPara2').val() != null) Params[$('#txtPara2').val()] = $('#txtValue2').val();
		if ($('#txtPara3').val() != null) Params[$('#txtPara3').val()] = $('#txtValue3').val();
		if ($('#txtPara4').val() != null) Params[$('#txtPara4').val()] = $('#txtValue4').val();
		if ($('#txtPara5').val() != null) Params[$('#txtPara5').val()] = $('#txtValue5').val();
		if ($('#txtPara6').val() != null) Params[$('#txtPara6').val()] = $('#txtValue6').val();
		if ($('#txtPara7').val() != null) Params[$('#txtPara7').val()] = $('#txtValue7').val();
		if ($('#txtPara8').val() != null) Params[$('#txtPara8').val()] = $('#txtValue8').val();
		if ($('#txtPara9').val() != null) Params[$('#txtPara9').val()] = $('#txtValue9').val();
		if ($('#txtPara10').val() != null) Params[$('#txtPara10').val()] = $('#txtValue10').val();
		if ($('#txtPara11').val() != null) Params[$('#txtPara11').val()] = $('#txtValue11').val();
		if ($('#txtPara12').val() != null) Params[$('#txtPara12').val()] = $('#txtValue12').val();
//		alert(dump(Params));
		var Result="";
		$.ajaxSetup({
        error: function(jqXHR, exception) {
					var msgError="";
//						alert("jqXHR\n"+dump(jqXHR));
//						alert("exception\n"+dump(exception));
            if (jqXHR.status === 0) {
//                alert('Not connect.\n Verify Network.\n');
								msgError='Not connect.\n Verify Network.\n';
            } else if (jqXHR.status == 404) {
//                alert('Requested page not found. [404]');
								msgError='Requested page not found. [404]';
            } else if (jqXHR.status == 500) {
//                alert('Internal Server Error [500].');
								msgError='Internal Server Error [500].';
            } else if (exception === 'parsererror') {
//                alert('Requested JSON parse failed.');
								msgError='Requested JSON parse failed.';
            } else if (exception === 'timeout') {
//                alert('Time out error.');
								msgError='Time out error.';
            } else if (exception === 'abort') {
//                alert('Ajax request aborted.');
								msgError='Ajax request aborted.';
            } else {
//                alert('Uncaught Error.\n' + jqXHR.responseText);
								msgError='Uncaught Error.\n' + jqXHR.responseText;
            }
//						$('#'+output).val("Error : "+msgError);
						$('#'+output).html("Error : "+msgError);
        }
    });
		if (! $('#chkLocal').attr('checked')) {
			URL=$('#txtServer').val()+"/"+$('#txtProg').val();
		}
		console.log("URL => "+URL);
		$.post(URL,Params,function(data) {
//				$('#'+output).val(data);
			$('#'+output).html(data);
		});
	}
</script>
<body bgcolor="#000000">
<table border="1" align="center" class="w-95">
<tr class="header"><th>Ajax Tester</th></tr>
<tr>
  <form>
	<td>
  <input type="text" id="txtServer" value="<?=$Path;?>" size="60"/>
  <input type="text" id="txtProg" value="" size="60"/>
	<input id="chkLocal" type="checkbox" />only localhost
  <input type="button" onClick="run('dvResult')" value="Run App"/><br />
  <table border="1" style="border-width:thin; padding:0px;">
    <tr class="header"><th colspan="4">Parameter(s)</th></tr>
<?
	for ($iCount=1; $iCount<= 6; $iCount++) {
		$iCount1=($iCount*2)-1;
		$iCount2=($iCount*2)
?>
    <tr <? if ($iCount >0) echo 'style="padding:0px;"'; ?>>
      <td  class="right" style="font-weight:bolder">Para<?=$iCount1;?> :</td>
      <td><input type="text" id="txtPara<?=$iCount1?>" size="10" />	 = <input type="text" id="txtValue<?=$iCount1;?>" size="40" />
      <input type="button" onClick="clearValue(<?=$iCount1;?>);" value="clear"/></td>
      <td class="right" style="font-weight:bolder">Para<?=$iCount2;?> :</td>
      <td><input type="text" id="txtPara<?=$iCount2?>" size="10" />	 = <input type="text" id="txtValue<?=$iCount2;?>" size="40" />
      <input type="button" onClick="clearValue(<?=$iCount2;?>);" value="clear"/></td>
			</td>
    </tr>
<?
	}
?>
  </table>
  Result <br />
  <div id="dvResult" style=" width:100%; height:250px; border-style:none; overflow:auto; font-size:14px; font-family:Tahoma; background:#CCC"></div>
<!--  <textarea id="txtResult" cols="120" rows="10"></textarea> -->
	<br>
  </td>
  </form>
</tr>
</table>
</body>
</html>