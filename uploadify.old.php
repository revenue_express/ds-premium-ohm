<?php
	$session_name = session_name();
	if (!isset($_POST['session_name'])) {
    exit;
	} else {
    session_id($_POST['session_name']);
	}
	include("./includes/db_config.inc.php");
	include("./functions/library.func.php");
/*
Uploadify
Copyright (c) 2012 Reactive Apps, Ronnie Garcia
Released under the MIT License <http://www.opensource.org/licenses/mit-license.php> 
*/

// Define a destination
$targetFolder = '/neptune/data/'; // Relative to the root
$verifyToken = md5('unique_salt' . $_POST['timestamp']);

$arrReturn=array('Return'=>false,'Result'=>'failed upload');

if (!empty($_FILES) && $_POST['token'] == $verifyToken) {
	$tempFile = $_FILES['Filedata']['tmp_name'];
	$targetPath = $_SERVER['DOCUMENT_ROOT'] . $targetFolder. $_POST['path'];
	$oldFileName = strtolower($_FILES['Filedata']['name']);
	$targetFile = rtrim($targetPath,'/') . '/' . $oldFileName;
	
	// Validate the file type
//	$fileTypes = array('jpg','jpeg','gif','png'); // File extensions
	$fileTypes = explode(',',$_POST['extension']); 
	$fileParts = pathinfo(strtolower($_FILES['Filedata']['name']));
	
	if (in_array($fileParts['extension'],$fileTypes)) {
		$newName=sprintf("%s",date('YmdHisz'));
		$newFileName = sprintf('%s.%s',$newName,$fileParts['extension']);
		$newTargetFile=  rtrim($targetPath,'/') . '/' .$newFileName;
		move_uploaded_file($tempFile,$newTargetFile);
		$sFizeSize='0';
		if (file_exists($newTargetFile)) $sFileSize=sprintf("%d",filesize($newTargetFile));
		$iInsertID=library_add($_POST['stype'],$oldFileName,$newName,$newFileName,$sFileSize,$_POST['ComID']);
		if ($iInsertID > 0) {
			$arrReturn['Return']=true;
			$arrReturn['Result']='Upload to '.$newTargetFile;
		}
	} else {
		$arrReturn['Result']='File Type Not Exists support only '+$_POST['extension'];
	}
} else {
	$arrReturn['Result']='Not found upload file OR No valid token';
}

echo json_encode($arrReturn);
?>