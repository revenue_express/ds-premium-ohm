<? 
	include("./includes/db_config.inc.php");
	include("./includes/sys_config.inc.php");
	$DEBUG=FALSE;
?>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<script type="text/javascript" src="./js/checkValid.js"></script>
<script type="text/javascript" src="./js/signage.js"></script>
<script type="text/javascript" src="./js/admin_user.js"></script>
<script type="text/javascript" src="js/jquery-1.8.0.min.js"></script>
<script type="text/javascript" src="js/jquery-ui-1.8.23.custom.min.js"></script>
<script>
	$(function() {
		$( "input:submit, input:button, input:reset, a, button", ".demo" ).button();
		$( "a", ".demo" ).click(function() { return false; });
	});
</script>
<link rel="stylesheet" href="css/custom-theme/jquery-ui-1.8.23.custom.css">
<link rel="stylesheet" href="css/StyleSheet.css" type="text/css" />
<body>
<?
	$URL=sprintf("%s/ajax/user_info.php?usrID=%d",$LocalHost,$USER['usr_id']);
//	echo "<br>URL => ".$URL;
	$File=file_get_contents($URL);
	$Data=json_decode($File,true);

//	echo "<pre>"; print_r($File); echo "</pre>";
	if ($File === false) {
//		echo  "<br> false";
	} else {
//		echo  "<br> TRUE";
//		echo "<pre>"; print_r($Data); echo "</pre>";
		if (count($Data) > 0) {
			$Info=$Data[0];
		}
	}
?>
<div id="dvUserEdit" class="w-50">
    	<div class="well">
				<div class="dvtr center"><h3>.: User Information :. </h3></div>
        <div class="dvtr top15">
        	<div class="dvtd_left w-30 top5">Name : </div>
          <div class="dvtd_right"><input class="txt" id="txtEditName" type="text" size="40" maxlength="127" onKeyUp="user_edit_check();" value="<?=$Info['usr_name']?>"></div>
        </div>
				<div class="dvtr">
        	<div class="dvtd_left w-30 top5">Login : </div>
          <div class="dvtd_right"><input  class="txt"id="txtEditLogin" type="text" size="16" maxlength="32" readonly disabled value="<?=$Info['usr_login']?>"></div>
       	</div>
    		<div class="dvtr center top15"><h3>.: Forget Password :.</h3></div>
				<div class="dvtr top15">
        	<div class="dvtd_left w-30 top5">Question : </div>
					<div class="dvtd_right">
            <select id="selEditQuest" name="selEditQuest" onChange="user_edit_quest();user_edit_check();">
              <option value="Question 1">Question 1</option>
              <option value="Question 2">Question 2</option>
              <option value="Question 3">Question 3</option>
            </select>
            <span><input class="txt" id="txtEditQuest" type="text" size="30" maxlength="127" onKeyUp="user_edit_check();" value="<?=$Info['usr_hint']?>"></span>
    			</div>
        </div>
				<div class="dvtr">
        	<div class="dvtd_left w-30 top5">Answer : </div>
					<div class="dvtd_right"><input class="txt" id="txtEditAnswer" type="text" size="40" maxlength="127" onKeyUp="user_edit_check();" value="<?=$Info['usr_answer']?>"></div>
        </div>
				<div class="dvtr">
        	<div class="dvtd_left w-30 top5">Email : </div>
					<div class="dvtd_right"><input class="txt" id="txtEditEmail" type="text" size="40" maxlength="127" onKeyUp="user_edit_check();" value="<?=$Info['usr_email']?>"></div>
        </div>
				<div class="dvtr center top15">
          <input type="button" class="btnSky" id="btnEditInfo" value="Save" onClick="user_edit();" disabled>
          <input type="reset" class="btnSky" id="btnEditCancel" value="Reset">
          <input type="hidden" size="5" id="UserID" name="UserID" value="<?=$USER['usr_id'];?>" readonly>
          <input type="hidden" size="5" id="txtName" name="txtName" value="<?=$Info['usr_name'];?>" readonly>
          <input type="hidden" size="5" id="txtQuest" name="txtQuest" value="<?=$Info['usr_hint'];?>" readonly>
          <input type="hidden" size="5" id="txtAnswer" name="txtAnswer" value="<?=$Info['usr_answer'];?>" readonly>
          <input type="hidden" size="5" id="txtEmail" name="txtEmail" value="<?=$Info['usr_email'];?>" readonly>
          <input type="hidden" size="5" id="Mode" name="Mode" value="<?=$USER['usr_level'];?>" readonly>
          <input type="hidden" size="5" id="selEditLevel" name="selEditLevel" value="<?=$Info['usr_lvl_id'];?>" readonly>
				</div>
   		</div>
</div>
</div>
</body>
