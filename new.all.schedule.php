<? 
	$ComID=1;
	include("./includes/db_config.inc.php");
//	include("./includes/sys_config.inc.php");
	include("./functions/schedule.func.php");
	include("./functions/layout.func.php");
	include("./functions/playlist.func.php");
	include("./functions/feed.func.php");
	include("./functions/schedule_client.func.php");

	$Schedule=NULL;

	$arrTypeUseFile=array(1,2,3,4,9,10,11);
	$arrPanelData=array("enable"=>false,"type"=>0,"title"=>NULL);
	$arrPanel=array(
		"video1"=>array("enable"=>false,"type"=>0,"title"=>NULL),
		"video2"=>array("enable"=>false,"type"=>0,"title"=>NULL),
		"video3"=>array("enable"=>false,"type"=>0,"title"=>NULL),
		"video4"=>array("enable"=>false,"type"=>0,"title"=>NULL),
		"image1"=>array("enable"=>false,"type"=>1,"title"=>NULL),
		"image2"=>array("enable"=>false,"type"=>1,"title"=>NULL),
		"image3"=>array("enable"=>false,"type"=>1,"title"=>NULL),
		"image4"=>array("enable"=>false,"type"=>1,"title"=>NULL),
		"audio"=>array("enable"=>false,"type"=>2,"title"=>NULL),
		"feed1"=>array("enable"=>false,"type"=>3,"title"=>NULL),
		"feed2"=>array("enable"=>false,"type"=>3,"title"=>NULL),
		"plugin1"=>array("enable"=>false,"type"=>4,"title"=>NULL),
		"plugin2"=>array("enable"=>false,"type"=>4,"title"=>NULL),
		"cover"=>array("enable"=>false,"type"=>5,"title"=>NULL),
	);									

	$arrFileListData=array("id"=>"id","filename"=>"filename");

	$arrDownload=NULL;
	$arrFileList=NULL;
	$arrPlaylist=NULL;
	$arrTemplateData=array(
		"id"=>(int) "1",
		"layout"=>"1.css",
		"opt_layout"=>NULL,
		"title"=>"title",
		"start"=>array("hours"=>(int)"0","minutes"=>(int)"0","seconds"=>(int)"0","milliseconds"=>(int)"0"),
		"end"=>array("hours"=>(int)"23","minutes"=>(int)"59","seconds"=>(int)"59","milliseconds"=>(int)"0"),
		"day"=>array(true,true,true,true,true,true,true),
		"mpanel"=>"video1",
		"panellist"=>$arrPanel,
		"playlist"=>$arrPlaylist,
		"filelist"=>$arrFileList,				
		);


	if (isset($ClientID) && ($ClientID > 0)) {
		$arrSchList=json_decode(schedule_client_list_from("Client",$ClientID,0),true); 
		$Schedule=NULL;
		if ($DEBUG) { echo "<pre>"; print_r($arrSchList); echo "<pre>"; echo "<br>".count($arrSchList); }
		for ($iSch=0; $iSch < count($arrSchList); $iSch++) {
			$arrDownload=NULL;
			$schID=$arrSchList[$iSch]['schh_id'];
			$schName=$arrSchList[$iSch]['schh_name'];

			/*	Get Schedule Header Information */
			$arrSchInfo=json_decode(schedule_head_view($schID),true);
			if ($DEBUG) {  echo "<br>schInfo<pre>"; print_r($arrSchInfo); echo "</pre>"; }

			$arrPeriod=json_decode(schedule_period_list($schID),true);
			if ($DEBUG) {  echo "<br>schDate<pre>"; print_r($arrPeriod); echo "</pre>"; }
	
			for ($iCount=0; $iCount < count($arrPeriod); $iCount++) {
				$ShowTime[]=array("start"=>$arrPeriod[$iCount]['schp_start'],"stop"=>$arrPeriod[$iCount]['schp_stop']);
			}
			if ($DEBUG) {  echo "<br>ShowTime :<pre>"; print_r($ShowTime); echo "</pre>"; }
			
			list($dStart,$tStart)=split(" ",$ShowTime[0]['start']);
			list($dStop,$tStop)=split(" ",$ShowTime[0]['stop']);
			
			if (is_null($tStart)) $tStart="00:00:00";
			if (is_null($tStop)) $tStop="23:59:59";

			$arrDStart=split("-",$dStart);	$arrDStop=split("-",$dStop);
			$arrTStart=split(":",$tStart);	$arrTStop=split(":",$tStop);

			$arrShowTime=array(
				"start"=>array("year"=>(int)$arrDStart[0],"month"=>(int)$arrDStart[1],"day"=>(int)$arrDStart[2],
														"hours"=>(int)$arrTStart[0],"minutes"=>(int)$arrTStart[1],"seconds"=>(int)$arrTStart[2],"milliseconds"=>0),
				"stop"=>array("year"=>(int)$arrDStop[0],"month"=>(int)$arrDStop[1],"day"=>(int)$arrDStop[2],
														"hours"=>(int)$arrTStop[0],"minutes"=>(int)$arrTStop[1],"seconds"=>(int)$arrTStop[2],"milliseconds"=>0) );

			$arrTemplate=NULL;

			$arrSchItem=json_decode(schedule_layout_list($schID),true);
			if ($DEBUG) {  echo "<br>schItem<pre>"; print_r($arrSchItem); echo "</pre>"; }
			/*	Loop for Get Template Information */
			
			$arrTemplate=NULL;
			for ($iTemplate=0; $iTemplate < count($arrSchItem); $iTemplate++) {
				$arrFileList=NULL;
				$arrTemplate[]=$arrTemplateData;
				$iCurrLayout=$arrSchItem[$iTemplate]['layh_id'];
				$arrTemplate[$iTemplate]['id']=$arrSchItem[$iTemplate]['layh_id'];
				$arrTemplate[$iTemplate]['layout']=sprintf("%d.css",$arrSchItem[$iTemplate]['tmph_id']);
				$arrTemplate[$iTemplate]['title']=$arrSchItem[$iTemplate]['layh_name'];

				$arrFileList[]=array("id"=>"0","filename"=>$arrTemplate[$iTemplate]['layout']);
				$arrDownload[$arrTemplate[$iTemplate]['layout']]=array(
					"id"=>0,
					"type"=>0,
					"title"=>$arrTemplate[$iTemplate]['title'],
					"name"=>$arrTemplate[$iTemplate]['layout']);

				$arrTStart=split(":",$arrSchItem[$iTemplate]['schl_start']);
				$arrTStop=split(":",$arrSchItem[$iTemplate]['schl_stop']);

				$arrTemplate[$iTemplate]['start']['hours']=(int)$arrTStart[0];
				$arrTemplate[$iTemplate]['start']['minutes']=(int)$arrTStart[1];
				$arrTemplate[$iTemplate]['start']['seconds']=(int)$arrTStart[2];

				$arrTemplate[$iTemplate]['end']['hours']=(int)$arrTStop[0];
				$arrTemplate[$iTemplate]['end']['minutes']=(int)$arrTStop[1];
				$arrTemplate[$iTemplate]['end']['seconds']=(int)$arrTStop[2];

				$arrTemplate[$iTemplate]['day'][0]=($arrSchItem[$iTemplate]['schl_sunday']==1);
				$arrTemplate[$iTemplate]['day'][1]=($arrSchItem[$iTemplate]['schl_monday']==1);
				$arrTemplate[$iTemplate]['day'][2]=($arrSchItem[$iTemplate]['schl_tuesday']==1);
				$arrTemplate[$iTemplate]['day'][3]=($arrSchItem[$iTemplate]['schl_wednesday']==1);
				$arrTemplate[$iTemplate]['day'][4]=($arrSchItem[$iTemplate]['schl_thursday']==1);
				$arrTemplate[$iTemplate]['day'][5]=($arrSchItem[$iTemplate]['schl_friday']==1);
				$arrTemplate[$iTemplate]['day'][6]=($arrSchItem[$iTemplate]['schl_saturday']==1);

				$arrItemCount=array("image"=>0,"video"=>0,"feed"=>0,"plugin"=>0,"cover"=>0);

				$CoverReturn=file_get_contents($LocalHost.'/ajax/layout_cover_info.php?HeadID='.$arrSchItem[$iTemplate]['layh_tmph_id']);
				$arrCover=json_decode($CoverReturn,true);
				$CoverFile=sprintf("media/cover/%s",$arrCover['file_sname']);
				if (count($CoverData>0)) {
					$arrTemplate[$iTemplate]['panellist']['cover']['enable']=true;
					$arrTemplate[$iTemplate]['panellist']['cover']['title']=$arrCover['file_dname'];
					$arrTemplate[$iTemplate]['panellist']['cover']['type']=5;
					$arrTemplate[$iTemplate]['playlist']['cover']['src']=$CoverFile;
					$arrFileList[]=array("id"=>$arrCover['file_id'],"filename"=>$arrCover['file_sname']);
					$arrDownload[$arrCover['file_sname']]=array(
						"id"=>(int)$arrCover['file_id'],
						"type"=>11,
						"title"=>$arrCover['file_dname'],
						"name"=>$arrCover['file_sname']);
				}

				$arrLayout=json_decode(layout_line_list($iCurrLayout),true);		
				if ($DEBUG) {  echo sprintf("<br>LayItem on Layout %s<pre>",$arrSchItem[$iTemplate]['layh_name']); print_r($arrLayout); echo "</pre>"; }
				for ($iLayout=0; $iLayout < count($arrLayout); $iLayout++) {
					$iTypeID=$arrLayout[$iLayout]['type_id'];
					$arrItemCount[$arrLayout[$iLayout]['type_panel']]++;
					$sPanelName=sprintf("%s%d",$arrLayout[$iLayout]['type_panel'],$arrItemCount[$arrLayout[$iLayout]['type_panel']]);
					$arrTemplate[$iTemplate]['panellist'][$sPanelName]['enable']=true;
					
					$arrPlaylist=array();
					$iPlaylistID=$arrLayout[$iLayout]['layl_plh_id'];
					$arrPLHead=json_decode(playlist_head_view($iPlaylistID),true);
					if ($DEBUG) {  echo sprintf("<br>PLHead %s<pre>",$iPlaylistID); print_r($arrPLHead); echo "</pre>"; }

					$arrTemplate[$iTemplate]['panellist'][$sPanelName]['title']=$arrPLHead[0]['plh_name'];
					$arrPLHeadOption=json_decode(playlist_head_option_list($iPlaylistID),true);
					if ($DEBUG) { echo sprintf("<br>LayItem on Layout %s<pre>",$iPlaylistName); print_r($arrPLHeadOption); echo "</pre>"; }
					$arrPLHeadConfig=NULL;


					for ($iPLOption=0; $iPLOption < count($arrPLHeadOption); $iPLOption++) {
						$Head=$arrPLHeadOption[$iPLOption]['pho_name'];
						$Value=$arrPLHeadOption[$iPLOption]['pho_value'];
						$arrPLHeadConfig[$Head]=$Value;
					}

					if (in_array($iTypeID,$arrTypeUseFile)) {
						$arrPLLine=json_decode(playlist_line_list($ComID,$iTypeID,$iPlaylistID),true);
						if ($DEBUG) {  echo sprintf("<br>PLLine of %s<pre>",$PLHead[0]['plh_name']); print_r($arrPLLine); echo "</pre>"; }
						for ($iPLCount=0; $iPLCount<count($arrPLLine); $iPLCount++) {
							$arrFileConfig=$arrPLHeadConfig;
							$arrCuePoints=NULL;
							$sFileName=sprintf("%s/%s",$arrPLLine[$iPLCount]['type_spath'],$arrPLLine[$iPLCount]['file_sname']);
							$sTitle=$arrPLLine[$iPLCount]['file_dname'];
							$sType=$arrPLLine[$iPLCount]['file_dname'];
							$arrFileList[]=array("id"=>$arrPLLine[$iPLCount]['file_id'],"filename"=>$arrPLLine[$iPLCount]['file_sname']);
							$arrDownload[$arrPLLine[$iPLCount]['file_sname']]=array(
								"id"=>(int)$arrPLLine[$iPLCount]['file_id'],
								"type"=>(int)$iTypeID,
								"title"=>$arrPLLine[$iPLCount]['file_dname'],
								"name"=>$arrPLLine[$iPLCount]['file_sname']);

							$arrPLLineOption=json_decode(playlist_line_option_list($arrPLLine[$iPLCount]['pll_id']),true);

							if ($arrLayout[$iLayout]['type_panel'] == 'image') {
								$arrPlaylist[]=array(
									"src"=>$sFileName,"title"=>$sTitle,"config"=>$arrFileConfig);
							} else if ($arrLayout[$iLayout]['type_panel'] == 'video') {
								if (! isset($arrFileConfig['autoplay'])) $arrFileConfig['autoplay']=true;
//								if (! isset($arrFileConfig['abc'])) $arrFileConfig['abc']='abc'; 
								if (! isset($arrFileConfig['disablePause'])) $arrFileConfig['disablePause']=true; 
								if (! isset($arrFileConfig['disallowSkip'])) $arrFileConfig['disallowSkip']=true; 
								if (! isset($arrFileConfig['disallowSkip'])) $arrFileConfig['disallowSkip']=true; 
								$arrFileConfig['autoplay']=true;
								$arrCuePoints[]=array(
									"on"=>"00:00",
									"value"=>array("title"=>$sFileName),
									"callback"=>"setVideoTitle");
								$arrPlaylist[]=array(
									"0"=>array("src"=>$sFileName,"type"=>$sType),
									"config"=>$arrFileConfig,
									"cuepoints"=>$arrCuePoints
									);
							} else if ($arrLayout[$iLayout]['type_panel'] == 'plugin') {
								$arrPlaylist[]=array(
									"src"=>$sFileName,"title"=>$sTitle,"config"=>$arrFileConfig,"parameter"=>$arrPLLine[$iPLCount]['pll_para']);
							} else {
								$arrPlaylist=array(
									"src"=>$sFileName,"title"=>$sTitle,"config"=>$arrFileConfig);
							}
						}
					} else {
						if ($iTypeID == 5) {
							$arrPLLine=json_decode(playlist_feed_line_list($iPlaylistID),true);
							if ($DEBUG) {  echo sprintf("<br>PLLine of %s<pre>",$PLHead[0]['plh_name']); print_r($arrPLLine); echo "</pre>"; }
							for ($iPLCount=0; $iPLCount <count($arrPLLine); $iPLCount++) {
								$arrFeedConfig=$arrPLHeadConfig;
								$sFontStyle="";
								foreach ($arrFeedConfig as $Key=>$Value) {
									$sFontStyle.=sprintf("%s: %s;",$Key,addslashes($Value));
								}
								$arrPlaylist[]=array(
									"msgid"=>$arrPLLine[$iPLCount]['feed_id'],
//									"msgtxt"=>sprintf("<span style='%s'>%s</span>",$sFontStyle,$arrPLLine[$iPLCount]['feed_data']) );
									"msgtxt"=>sprintf("%s",$arrPLLine[$iPLCount]['feed_data']) );
//									"config"=>$arrFeedConfig);
							}
						}
					}
					$arrTemplate[$iTemplate]['playlist'][$sPanelName]=$arrPlaylist;
				}
				$arrTemplate[$iTemplate]['filelist']=$arrFileList;
			}

			$Schedule[$iSch]=array(
				"id"=>$schID,
				"title"=>$schName,
				"showtime"=>$arrShowTime,
				"template"=>$arrTemplate);

			$sSchName=sprintf("./%s/filedownload/file.%d.json",$DataPath,$schID);
			file_put_contents($sSchName,json_encode($arrDownload));

			$sSchName=sprintf("./%s/filedownload/sch.%d.json",$DataPath,$schID);
			file_put_contents($sSchName,json_encode($Schedule[$iSch]));
		}
	}

	$Data=array("schedule"=>$Schedule);
	if ($DEBUG) { echo "<pre>"; print_r($Data); echo "</pre>"; }
		echo json_encode($Data);
//	echo str_replace("}]","}]<br>",str_replace("},","},<br>",json_encode($Data)));
//	echo "<p>".str_replace("},","},<br>",json_encode($arrDownload));

?>
	
