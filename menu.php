<ul class="dropdown">
    <li><a href="main.php">Home</a></li>
<?php	if (strtolower($USER['usr_level'])!='operator') { ?>
    <li><a href="#">Admin</a>
        <ul class="sub_menu">
            <li><a href="main.php?menu=admin_groups">Group</a></li>
            <li><a href="main.php?menu=admin_client">Client</a></li>
            <li><a href="main.php?menu=admin_user">User</a></li>
            <li><a href="main.php?menu=purge_data">Purge Data</a></li>
            <li><a href="main.php?menu=config_email">Email Config</a></li>
            <li><a href="main.php?menu=admin_logs">Logs</a></li>
            <!--<li><a href="#">User policy</a></li>
            <li><a href="#">Display size</a></li>
            <li><a href="#">Question option</a></li>
<?php	//if (strtolower($USER['usr_level'])=='administrator') {?>
            <li><a href="#">Playlist option</a></li>-->
<?php	//} ?>
        </ul>
    </li>
<?php	} ?>
    <li><a href="#">Library</a>
        <ul class="sub_menu">
            <li><a href="main.php?menu=library_images">Image Library</a></li>
            <li><a href="main.php?menu=library_videos">Video Library</a></li>
            <li><a href="main.php?menu=library_flashs">Flash Library</a></li>
            <li><a href="main.php?menu=library_audios">Audio Library</a></li>
            <li><a href="main.php?menu=library_feeds">Feed Library</a></li>
            <li><a href="main.php?menu=library_docs">Document Library</a></li>
            <li><a href="main.php?menu=library_pdfs">PDF Library</a></li> 
            <li><a href="main.php?menu=library_ppts">Powerpoint Library</a></li>
            <li><a href="main.php?menu=library_covers">Frame Library</a></li>
<!--            <li><a href="main.php?menu=library_rss">RSS Playlist</a></li>         -->           
<!--            <li><a href="main.php?menu=library_streams">Streaming Library</a></li> -->
        </ul>
     </li>
    <li><a href="#">Layout</a>
      <ul class="sub_menu">
        <li><a href="main.php?menu=template">Layout Management</a></li>
        <li><a href="main.php?menu=config_header">Config Header</a></li>
        <li><a href="main.php?menu=config_footer">Config Footer</a></li>
      </ul>
    </li>
<!--
     <li>
        <a href="#">Playlist</a>
       <ul class="sub_menu">
            <li><a href="main.php?menu=playlist_images">Image Playlist</a></li>
            <li><a href="main.php?menu=playlist_videos">Video Playlist</a></li>
            <li><a href="main.php?menu=playlist_flashs">Flash Playlist</a></li>
            <li><a href="main.php?menu=playlist_audios">Audio Playlist</a></li>
            <li><a href="main.php?menu=playlist_feeds">Feed Playlist</a></li>
            <li><a href="main.php?menu=playlist_pdfs">PDF Playlist</a></li>
            <li><a href="main.php?menu=playlist_ppts">Powerpoint Playlist</a></li>                    
            <li><a href="main.php?menu=playlist_rss">RSS Playlist</a></li>      
            <li><a href="main.php?menu=playlist_streams">Streaming Playlist</a></li>                    
        </ul>
     </li>
-->
     <li><a href="main.php?menu=layout">Template</a></li>
     <li>
     		<a href="#">Schedule</a>
       <ul class="sub_menu">
            <li><a href="main.php?menu=schedule">Schedule Management</a></li>
<!--            <li><a href="main.php?menu=player_group_schedule">Send Schedule</a></li> -->
       </ul>
     </li>
    <span style="width:auto; float:right; text-align:left">
    <span style="color:#FFF; float:left">[ <?php echo $_SESSION['usr_company'];?> ] Welcome <?php echo $_SESSION['usr_name'];?> </span>
    <li style="margin:12px 5px 10px 5px; color:#FFF; height:37px; cursor:pointer">[Account]<ul class="sub_menu" style="margin-top:-12px;">
            <li><a href="main.php?menu=user_change_profile">My Profile</a></li>
            <li><a href="main.php?menu=user_change_password">Change Password</a></li>
            <li><a href="index.php">Logout</a></li>
        </ul>
    </li>
    </span>
</ul>