<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" " http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd ">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<? include ("javascript.php"); ?>
<script type="text/javascript">
</script>
</head>
<body>
<button id="request">Request</button>
<script src="./js/screenfull.js"></script>
<script>
	$(function() {
		screenfull.onchange = function(e) {
			var elem = screenfull.element;

			$('#status').text('Is fullscreen: ' + screenfull.isFullscreen);

			if (elem) {
				$('#element').text('Element: ' + elem.localName + (elem.id ? '#' + elem.id : ''));
			}

			if (!screenfull.isFullscreen) {
				$('#external-iframe').remove();
				document.body.style.overflow = 'auto';
			}
		};

		$('#supported').text('Supported/allowed: ' + !!screenfull.enabled);
		
		$('#request').click(function() {
			screenfull.request();
		});

		$('document').ready(function() {
			screenfull.request();
			if (! screenfull.isFullscreen) {
				document.body.style.overflow = 'auto';
			}
		});

		// Trigger the onchange() to set the initial values
		screenfull.onchange();

	});
</script>
<section>
  <ul>
    <li id="supported"></li>
    <li id="status"></li>
    <li id="element"></li>
  </ul>
</section>
</body>
</html>