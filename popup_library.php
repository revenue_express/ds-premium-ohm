<? 
	include("./includes/db_config.inc.php");
	include("./includes/sys_config.inc.php");
	include("./functions/library.func.php");
	include("./functions/playlist.func.php");
	$DEBUG=FALSE;
	$USER['usr_level']='Administrator';
?>
<html>
<head>
<script type="text/javascript" src="./js/checkValid.js"></script>
<script type="text/javascript" src="./js/signage.js"></script>
<script type="text/javascript" src="js/jquery-1.8.0.min.js"></script>
<script type="text/javascript" src="js/jquery-ui-1.8.23.custom.min.js"></script>
<script>
	$(function() {
		$( "input:submit, a, button", ".demo" ).button();
		$( "a", ".demo" ).click(function() { return false; });
	});
</script>
<link rel="stylesheet" href="css/style.css">
</head>
<body>
<?
//	$TypeData=array("Name"=>'image',"Path"=>"images");
	$TypeData=json_decode(file_type_list($TypeID),true);
	if ($DEBUG) { echo "<li>Request<pre>"; print_r($_REQUEST); echo "</pre>"; }
	$bSuccess=false;
	$sScript="";
	if (isset($btnSave)) {
		if (playlist_line_add($PlayListID,$FileID)) {
			$sShow="Insert New Item to Playlist Success.";
			$sScript=sprintf("window.opener.location.href=window.opener.location.href+'?PlayListID=%d';",$PlayListID);
			$sScript.="window.opener.location.replace(window.openr.location.href);";
			$bSuccess=true;
		} else {
			$sShow="Insert New Item to Playlist Failed.";
		}
		echo sprintf("<script language='javascript'>alert('%s');%s</script>",$sShow,$sScript);
	}
?>
<div class="dvHeader">Media Library (<?=strtoupper($TypeData[0]['type_name']);?>)</div>
<?	
	$FileList=json_decode(library_list(1,$TypeID,1),true);
//	echo "Count(Data) =".count($data);
//	$FileList=json_decode($data);
//	echo "Count(FileList) =".count($FileList);
//	echo "<pre>";
//	print_r($FileList);
//	echo "</pre>";
//	for ($iCount=0; $iCount < count($FileList); $iCount++) {
//		$arrData=array();
//		$arrData=$FileList[$iCount];
//		echo "<pre>";
//		print_r($arrData);
//		echo "</pre>";
//		echo $arrData['file_id'];
//	}
?>
<div id="dvImageMain" align="center">
	<div id="dvImageFile" style="height:150px; width:700px; overflow:auto;">
  <table border="1" width="100%">
  	<thead>
    <tr><th></th><th></th><th>Item Name</th><th>Size(KB)</th></tr>
    </thead>
    <tbody>
<?
	if (count($FileList) > 0) {
		for ($iCount=0;$iCount<count($FileList);$iCount++) {
			$sFileName=sprintf("./data/%s/%s",$TypeData[0]['type_path'],$FileList[$iCount]['file_sname']);
			$onViewClick=sprintf("window.open('%s','_view');",$sFileName);
			$onRadioClick=sprintf("setValue('FileName','%s','formPopUpLibrary');",$FileList[$iCount]['file_dname']);
			$onRadioClick.=sprintf("setValue('FileID',%s,'formPopUpLibrary');",$FileList[$iCount]['file_id']);
			$onRadioClick.=sprintf("EnableObject('btnPopUpSave',true,'formPopUpLibrary');");
			$sDisplay="Do you confirm to insert this file to playlist?";
			$onSaveClick=sprintf("showConfirm('%s','formPopUpLibrary','btnPopUpSave','submit','Save');",$sDisplay);
			$onResetClick="EnableObject('btnPopUpSave',false,'formPopUpLibrary');";
			$onCloseClick=sprintf("window.opener.location.href=window.opener.location.href+'?PlayListID=%d';",$PlayListID);
			$onCloseClick.="window.opener.location.replace(window.opener.location.href);";
			$onCloseClick.="window.close();";
			$onOptionClick="Toggles('tblBodyImageOption')";
			$onDisplayChange=sprintf("setValue('Option_1',this.value,'formPopUpLibrary');");
			$onInChange=sprintf("setValue('Option_2',this.value,'formPopUpLibrary');");
			$onOutChange=sprintf("setValue('Option_3',this.value,'formPopUpLibrary');");
			$onIntervalChange=sprintf("setValue('Option_4',this.value,'formPopUpLibrary');");
?>
    <tr>
    <td width="20" align="center"><input type="radio" id="rdoFile" name="rdoFile" onClick="<?=$onRadioClick;?>"></td>
    <td width="20" align="center"><img class="preview" src="./images/icons/preview.png" onClick="<?=$onViewClick;?> " title="preview"></td>
    <td width="100%"><?=$FileList[$iCount]['file_dname'];?></td>
    <td align="right" nowrap='nowrap'><?=number_format($FileList[$iCount]['file_size']);?></td>
    </tbody>
<?	} } else{ ?>
    <tr><td colspan="4">NOT FOUND ITEM</td></tr>		
<?	} ?>
  </table>
	</div>
<?	if (isset($ShowOption)) {?>
	<div id="dvImageOption"  style="display:none">
  <table border="1">
	<thead>
	<tr><td>Image Option<button id="btnImageOption" onClick="<?=$onOptionClick;?>">View</button></td></tr>
	</thead>
  <tbody id="tblBodyImageOption" style="display:none;">
  <tr>
  <td align="right">Display : </td>
  <td>
  	<select name="OptionDisplay" onChange="<?=$onDisplayChange?>">
  	  <option value="Actual">Actual Size</option>
  	  <option value="Aspect">Aspect Ratio</option>
  	  <option value="Stretch" selected>Stretch</option>
		</select>
  </td>
  </tr>
  <tr>
  <td align="right">Transition IN : </td>
  <td>
  	<select name="OptionIn" onChange="<?=$onInChange?>">
  	  <option value="Default" selected>Default</option>
  	  <option value="Fade">Fade In</option>
  	  <option value="Zoom">Zoom In</option>
  	  <option value="Blink">Blink</option>
  	  <option value="Sprial">Sprial</option>
    </select>
  </td>
  </tr>
  <tr>
  <td align="right">Transition Out : </td>
  <td>
  	<select name="OptionOut" onChange="<?=$onOutChange?>">
  	  <option value="Default" selected>Default</option>
  	  <option value="Fade">Fade Out</option>
  	  <option value="Zoom">Zoom Out</option>
  	  <option value="Blink">Blink</option>
  	  <option value="Sprial">Sprial</option>
    </select>
  </td>
  </tr>
  <tr>
  <td align="right">Interval :</td>
  <td><input type="text" id="interval" name="txtInterval" maxlength="3" size="4" value="5" onChange="<?=$onIntervalChange;?>"> (Sec)</td>
  </tr>
  </tbody>
  </table>
	</div>
<? } ?>
	<div id="dvImageData">
	<table id="tblSubmit" border="1" width="700">
	<form id="formPopUpLibrary" method="post">
  	<input type="hidden" id="btnSave" name="btnSave" value="Save">
  	<input type="hidden" id="PlayListID" name="PlayListID" value="<?=$PlayListID;?>">
  	<input type="hidden" id="TypeID" name="TypeID" value="<?=$TypeID;?>">
  	<input type="hidden" id="FileID" name="FileID" value="">
  	<tr>
    <td>
    You select <input type="text" maxlength="80" id="FileName" name="FileName" size="55" readonly disabled>
		<span id="formPopUpLibraryOption">
		<input type="hidden" id="Option_1" name="Option_1" value="">
		<input type="hidden" id="Option_2" name="Option_2" value="">
		<input type="hidden" id="Option_3" name="Option_3" value="">
		<input type="hidden" id="Option_4" name="Option_4" value="">
		<input type="hidden" id="Option_5" name="Option_5" value="">
		<input type="hidden" id="Option_6" name="Option_6" value="">
		<input type="hidden" id="Option_7" name="Option_7" value="">
		<input type="hidden" id="Option_8" name="Option_8" value="">
		<input type="hidden" id="Option_9" name="Option_9" value="">
    </span>
    </td>
    <td><input type="button" id="btnPopUpSave" value="Save" disabled onClick="<?=$onSaveClick;?>"></td>
    <td><input type="reset" id="btnPopUpReset" value="Reset" onClick="<?=$onResetClick;?>"></td>
    <td><input type="button" id="btnPopUpClose" value="Close" onClick="<?=$onCloseClick;?>"></td>
    </tr>
	</form>
  </table>
	</div>
</div>

</body>
</html>