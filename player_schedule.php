<? 
	include("./includes/db_config.inc.php");
	include("./includes/sys_config.inc.php");
	$DEBUG=TRUE;
	$CSSName=NULL;
	$LayoutItems=NULL;
	$Data=NULL;
	if (isset($schID)) {
		$Data=file_get_contents($LocalHost+'/gen.schedule.php?schID='.$schID);
		$Data=str_replace("},{","},{\n",$Data);
		$Data=str_replace(":[{",":\n[{",$Data);
		$Data=str_replace("}]","}]\n",$Data);
		$Data=str_replace("\/","/",$Data);
	}
?>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<script type="text/javascript" src="./js/jquery-1.7.2.min.js"></script>
<script type="text/javascript" src="./js/jquery-ui-1.8.21.custom.min.js"></script>
<script type="text/javascript" src="./js/jquery-ui-timepicker-addon.js"></script>
<script type="text/javascript" src="./js/checkValid.js"></script>
<script type="text/javascript" src="./js/signage.js"></script>
<link rel="stylesheet" href="./css/custom-theme/jquery-ui-1.8.22.custom.css">
</head>
<body>
<form>
<div>
<span>Schedule ID</span><span><input type="text" id="schID" name="schID" size="4" maxlength="6" value="<?=$schID?>" /></span>
<button>Generate</button>
</div>
</form>
<textarea id="code" rows="60" cols="160"><?=$Data;?></textarea>
</body>
</html>	
