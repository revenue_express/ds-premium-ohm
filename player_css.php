<? 
	include("./includes/db_config.inc.php");
	include("./includes/sys_config.inc.php");
	include("./functions/template.func.php");
	$DEBUG=TRUE;
	$CSSName=NULL;
	$LayoutItems=NULL;
	$Data=NULL;
	if (isset($TID)) {
		$Data=file_get_contents('http://localhost/signage/gen.css.php?TID='.$TID);
	}
	$Item=json_decode(template_head_list($ComID),true);
?>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<script type="text/javascript" src="./js/jquery-1.7.2.min.js"></script>
<script type="text/javascript" src="./js/jquery-ui-1.8.21.custom.min.js"></script>
<script type="text/javascript" src="./js/jquery-ui-timepicker-addon.js"></script>
<script type="text/javascript" src="./js/checkValid.js"></script>
<script type="text/javascript" src="./js/signage.js"></script>
<link rel="stylesheet" href="./css/custom-theme/jquery-ui-1.8.22.custom.css">
</head>
<body>
<form>
<div>
<span>Layout ID</span>
<span>
<select id="TID" name="TID">
<?
	$Header="";
	for ($iRun=0; $iRun<count($Item); $iRun++) {
		$Selected="";
		if ($TID==$Item[$iRun]['tmph_id']) {
			$Selected=" SELECTED";
			$Header="CSS For ".$Item[$iRun]['tmph_name'].", TID = ".$TID;
		}
		echo sprintf("<option value='%d'%s>%s</option>",$Item[$iRun]['tmph_id'],$Selected,$Item[$iRun]['tmph_name']);
	}
?>
</select>
</span>
<button>Generate</button>
</div>
</form>
<textarea id="code" rows="20" cols="150"><?=sprintf("%s\n%s",$Header,$Data);?></textarea>
</body>
</html>	