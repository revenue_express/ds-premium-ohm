<? 
	$DEBUG=FALSE;
	$TypeID=1;
	include("./includes/db_config.inc.php");
	include("./includes/sys_config.inc.php");
	include("./functions/library.func.php");
?>
<script type="text/javascript" src="js/checkValid.js"></script>
<script type="text/javascript" src="js/signage.js"></script>
<script type="text/javascript" src="js/SolmetraUploader.js"></script>
<script type="text/javascript">
  SolmetraUploader.setErrorHandler('test');
  function test (id, str) { alert('ERROR: ' + str); }
</script>
<script type="text/javascript" src="js/jquery-1.8.0.min.js"></script>
<script type="text/javascript" src="js/jquery-ui-1.8.23.custom.min.js"></script>
<script>
	function library_upload(type,oldName,newName,upName,fSize) {
		var URL="./ajax/library_add.php";
//	if ( isset($Type) && isset($oldName) && isset($newName) && isset($fsize) )  {
		var Params={'Type':type,
			'oldName':encodeURIComponent(oldName),
			'newName':encodeURIComponent(newName),
			'upName':encodeURIComponent(upName),
			'DEBUG':false,
			'fsize':fSize};
//		document.getElementById('URL').value=URL;
//		document.getElementById('Params').value=dump(Params);
//		alert("Params =>"+dump(Params));
		$.post(URL,Params,function(data) {
//			alert(dump(data));
			if (parseInt(data) > 0) {
				alert("Covert File Successful.");
			} else {
//				alert(" File Failed");
			}
			$('#dvConvert').dialog("close");
		});
		$('#dvConvert').dialog("open");
	}

	$(function() {
		$( "input:submit, a, button", ".demo" ).button();
		$( "a", ".demo" ).click(function() { return false; });
	});
</script>
<link rel="stylesheet" href="css/StyleSheet.css">
<link rel="stylesheet" href="css/custom-theme/jquery-ui-1.8.23.custom.css">
<body>
<script type="text/javascript">
	$(function() {
		$('#dvConvert').dialog({ autoOpen: false, modal: true, draggable: false });
		$('#dvConvert').dialog("option","width",700);
		$('#dvConvert').dialog("option","height",250);
		$('#dvConvert').dialog("option","resizable",false);
	});
</script>
<div class="demo">
<div id="dvConvert" title="Upload and Coverting Format">
<table>
  <tbody id="tblUpload_Body">
  <tr>
  	<td><img src="./images/loading.gif" width="125" height="125"></td>
    <td class="left top10">
		This Dialog will disappear after convert completed.<br>
    Please waiting until completed.
  </td></tr>
  </tbody>
</table>
</div>
</div>

<div class="w-50">
	<div class="well">
    	<div class="left head">Media Library (Image)</div>
   		<div id="dvLibUploadImages">
<?
    if ($DEBUG) { echo "Request<pre>";		print_r($_REQUEST);		echo "</pre>"; }
    if ($DEBUG) { echo "File<pre>";		print_r($_FILES);		echo "</pre>"; }
//		echo "<pre>"; print_r($_SERVER); echo "</pre>";
    include("SolmetraUploader.php");

    $solmetraUploader = new SolmetraUploader('./','./upload.php','./config_images.php');
//		$solmetraUploader->setDemo(102400000);
    $solmetraUploader->gatherUploadedFiles();
    if (isset($_FILES) && sizeof($_FILES)) {
        if ($DEBUG) {
            echo '<h2>Uploaded files</h2>';
        echo '<pre>'; print_r($_FILES['firstfile']); echo '</pre>';
        }
        $uploadName=$_FILES['firstfile']['tmp_name'];
        $oldName=$_FILES['firstfile']['name'];
        $fSize=$_FILES['firstfile']['size'];
        if (($oldName != '') and ($txtNewName !='')  and ($uploadName != '') and ($fSize > 0) ) {
//            $result=library_add('images',$oldName,$txtNewName,$uploadName,$fSize);
?>
		<script language="javascript">
			var Display="oldName => <?=$oldName;?>\n NewName=> <?=$txtNewName;?>\n uploadName=> <?=$uploadName;?> ";
//			alert(Display);
			$(document).ready(function() {
				$('#dvConvert').dialog("open");
				library_upload('images','<?=$oldName;?>','<?=$txtNewName;?>','.<?=$uploadName;?>',<?=$fSize;?>);
			});
		</script>
<?		
        } else {
            $rResult="Data Not Completed.<BR>";
            $rResult.="oldName = '$oldName'<BR>";
            $rResult.="txtNewName = '$txtNewName'<BR>";
            $rResult.="uploadName = '$uploadName'<BR>";
            $rResult.="size = '".$fdata['size']."'<BR>";
            unlink($uploadName);
        }
    }
    
    if (isset($btnDelete)) {
        if ($DEBUG) echo "<br> Delete File => $btnDelete";
        $RowAffect=library_active($btnDelete);
        if ($RowAffect <> 1) {
            if ($DEBUG) echo ".....Delete Completed";
        } else {
            if ($DEBUG) echo ".....Delete Failed";
        }
    } elseif (isset($btnRestore)) {
        if ($DEBUG) echo "<br> Restore File => $btnRestore";
        $RowAffect=library_active($btnRestore,1);
        if ($RowAffect <> 1) {
            if ($DEBUG) echo ".....Restore Completed";
        } else {
            if ($DEBUG) echo ".....Restore Failed";
        }
    } else {
        if (isset($_SERVER['HTTP_REFERER']))	{
            if ($_SERVER['HTTP_URI'] = $PHP_SELF) {
                echo ("No File Upload");
            }
        }
    }
?>
        <form style="padding-left:20px; padding-top:15px;" method="post"/>
            <? if (strlen($rResult)>0) {?><div class="dvtd_left w-20"><?=$rResult;?></div><? } ?>
            <? echo $solmetraUploader->getInstance('firstfile', 500,30,true); ?>
            <div class="dvtr top10">
            	<div class="dvtd_right">Display Name : </div>
                <div class="dvtd_right">
                	<input type="text" class="txt" id="txtNewName" name="txtNewName" size="46" />
                </div>
            	<div class="demo">&nbsp;<input type="submit" id="btnSubmit" name="btnSubmit" value="Save"/></div>
            </div>
        </form>
    	</div>
	</div>
</div>
<?	
$FileList=json_decode(library_list($ComID,$TypeID,1),true);
//	echo "Count(Data) =".count($data);
//	$FileList=json_decode($data);
//	echo "Count(FileList) =".count($FileList);
//	echo "<pre>";
//	print_r($FileList);
//	echo "</pre>";
//	for ($iCount=0; $iCount < count($FileList); $iCount++) {
//		$arrData=array();
//		$arrData=$FileList[$iCount];
//		echo "<pre>";
//		print_r($arrData);
//		echo "</pre>";
//		echo $arrData['file_id'];
//	}
?>
<div class="w-100 top15"><!-- .altbox for alternative box's color -->
	<div class="boxin">
        <div class="header">
            <h3>Media Library (Image)</h3>
        </div>
        <table cellspacing="0">
            <thead>
                <tr>
                    <th>Display Name</th>
                    <th>Original Name</th>
                    <th>Size(KB)</th>
                    <th>W X H</th>
                    <th>In Used</th>
                    <?	if ($USER['usr_level'] <> 'Administrator') { ?>
                    <th colspan="2">Action</th>
                    <? } else { ?>
                    <th colspan="3">Action</th>
                    <? } ?>
                </tr>
            </thead>
            <tbody>
            <?
            if (count($FileList) > 0) {
								$iRun=-1;
                for ($iCount=0;$iCount<count($FileList);$iCount++) {
										if ( ($FileList[$iCount]['file_active']==0) && (strtolower($USER['usr_level'])=='operator') ) {
											continue;
										}
										$iRun++;
                    $sFileName=sprintf("./data/images/%s",$FileList[$iCount]['file_sname']);
                    $onViewClick=sprintf("window.open('%s','_view');",$sFileName);
                    $formDel=sprintf("formDelete_%d",$FileList[$iCount]['file_id']);  
                    $onDelete=sprintf("if (confirm('Do you confirm to delete')) {document.getElementById('%s').submit(); }",$formDel);
                    $formRestore=sprintf("formRestore_%d",$FileList[$iCount]['file_id']);  
                    $onRestore=sprintf("if (confirm('Do you confirm to restore data')) {document.getElementById('%s').submit(); }",$formRestore);
                    $FileInUse=TRUE;
										if (file_used($FileList[$iCount]['file_id']) == 0) $FileInUse=FALSE;
                                            
                    if ($FileInUse) {
												if (strtolower($USER['usr_level']) == 'operator') {
                        	$onDelete=sprintf("alert('%s');","File in used. Cannot Delete.");
												} else {
													$Mesg='!!!!! File in used !!!!!\n Do you confirm to delete ?';
													$Confirm='!!!!! File in used !!!!!\n Are you sure to delete ?';
                    			$onDelete=sprintf("if (confirm('%s')) { if (confirm('%s')) { document.getElementById('%s').submit();} }",$Mesg,$Confirm,$formDel);
												}
                    }	
//										$images=new ffmpeg_frame($sFileName);
//										$isize=array("width"=>$images->getWidth(),"height"=>$images->getWidth());
//										$image_size=sprintf("%d X %d",$isize['width'],$isize['height']);
										$image_size="0 X 0";
										if (file_exists($sFileName)) {
											$iSize=getimagesize($sFileName);
											$image_size=sprintf("%d X %d",$iSize[0],$iSize[1]);
										}
																
            ?>
                <tr class="<? if($iRun==0){ ?>first even<? }else if($iRun%2==0){?>even<? } ?>">
                    <td><?=$FileList[$iCount]['file_dname'];?></td>
                    <td><?=$FileList[$iCount]['file_oname'];?></td>
                    <td class="right"><?=number_format($FileList[$iCount]['file_size']/1024,3);?></td>
                    <td class="center"><?=$image_size;?></td>
                    <td class="center"><?=($FileInUse)?"Yes":"No";?></td>
                    <td class="center">
                        <img class="preview" src="images/icons/preview.png" onClick="<?=$onViewClick;?> " title="preview">
                    </td>
                        <? if ($FileList[$iCount]['file_active']==0) { ?>
                    <form action="<?=$PHP_SELF;?>" method="post" id="<?=$formRestore;?>" name="<?=$formRestore;?>">
                        <input type="hidden" id="btnRestore" name="btnRestore" 
                            value="<?=$FileList[$iCount]['file_id'];?>">
                    <?	if (strtolower($USER['usr_level']) != 'operator') { ?>
                            <td class="center">
                            <img width="16" height="16" src="images/icons/restore.png" onClick="<?=$onRestore;?>" title="restore">
                            </td>
                        <? } ?>
                    </form>
                    <?	} ?>
                    <form action="<?=$PHP_SELF;?>" method="post" id="<?=$formDel;?>" name="<?=$formDel;?>">
                        <input type="hidden" id="btnDelete" name="btnDelete" value="<?=$FileList[$iCount]['file_id'];?>">
                            <? if ($FileList[$iCount]['file_active']==1) { ?>
                            <td class="center">
                            <? if ( (strtolower($USER['usr_level']) == 'operator')  ){  ?>
														<?		if (! $FileInUse) { ?>
                                <img width="16" height="16" src="images/icons/delete.png" onClick="<?=$onDelete;?>" title="delete">
														<?		} ?>
                            <? } else {?>
                                <img width="16" height="16" src="images/icons/delete.png" onClick="<?=$onDelete;?>" title="delete">
														<?	} ?>
                            </td>
                            <? } ?>
                    </form>
                </tr>
            <? } ?>
        <? }else{ ?>
          <tr>
                <? $ColSize=(strtolower($USER['usr_level']) == 'operator')?8:9; ?>
                <td class="center" colspan="<?=$ColSize;?>">NO FOUND</td>
          </tr>
        <? }?>
        </tbody>
        </table>
	</div>
</div>
</body>
