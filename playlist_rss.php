<? 
	include("./includes/db_config.inc.php");
	include("./includes/sys_config.inc.php");
	$DEBUG=FALSE;
?>
<html>
<head>
<?	include("./javascript.php");?>
<script type="text/javascript" src="./js/playlist_rss.js"></script>
<script type="text/javascript" src="./js/playlist_head_option.js"></script>

<link rel="stylesheet" href="css/custom-theme/jquery-ui-1.8.23.custom.css">
<link rel="stylesheet" href="css/StyleSheet.css">
<script type="text/javascript">
	$('document').ready(function() {
		rss_head_list();
		
	});
</script>
</head>
<body>
<div id="dvHeadResult" class="w-80 boxin">
	<div class="header">
  	<h3>Playlist (RSS Feed)
    	<input id="btnListRefresh" type="button" value="refresh" onClick="rss_head_list();">
     <input id="txtSearch" type="text" value="" size="10">
     <input id="btnSearch" type="button" value="search" onClick="table_search('tblHead_Result',2,'txtSearch');">
<!--     <input id="btnEdit" type="button" value="edit" onClick="lm_update(parseInt($('#txtSearch').val()));"> -->
    </h3>
  </div>
  <div class="w-100">
  <div style="float:left; padding-left:10px;">Preview::</div>
  <div style="float:left" class="w-80"><marquee id="feedHeadPreview" class="w-100">Test Feed ::::: </marquee></div>
  </div>
  <table cellspacing="0">
  <thead>
  	<tr>
      <th>RSS Name</th>
      <th>RSS URL</th>
      <th class="w-15">Last Active</th>
      <th class="w-5">Item(s)</th>
      <th class="w-15">
      <input class="btnTH" type="button" value="refresh" onClick="rss_head_list();"/>
      &nbsp; &nbsp;
      <input class="btnTH" type="button" value=" &nbsp;Add &nbsp;" onClick="playlist_rss_add();"/>
      </th>
  	</tr>
  </thead>
  <tbody id="tblHead_Result">
    <tr>
      <td class="center">1</td>
      <td>2</td>
      <td>Item</td>
      <td class="center"><img class="btnEdit" title="Edit"></td>
      <td class="center"><img class="btnDel" title="Delete"></td>
    </tr>
  </tbody>
  </table>
</div>
<div id="dvLineResult" class="boxin w-80">
	<div class="header">
  	<h3>Playlist (RSS Feed)</h3><input class="btnHeader" type="button" value="Back" onClick="rss_head_list();"/>
  </div>
  <div>
<?
$onChange="EnableObject('btnHeadSave',false);";
$sDisplay="Do you Comfirm to Change Playlist Name?";
$onChange.=sprintf("changeInputValue('txtHeadName','%s',document.getElementById('txtHeadName').value);",$sDisplay);
$onChange.="EnableObject('btnHeadSave',! checkEqual('txtHeadName','txtOldName'));";

$onReset="EnableObject('btnHeadSave',false);";
$onReset.="setObjValue('txtOldName','txtHeadName');";
?>
    &nbsp; &nbsp;  Playlist Name : 
    <input class="txt" size="40" type="text" id="txtHeadName" name="txtHeadName" readonly/>
    <input class="txt" size="40" type="hidden" id="txtOldName" name="txtOldName" />
    <input class="ui-button-text" type="button" id="btnHeadChange" value="Change" onClick="<?=$onChange;?>">
    <input class="ui-button-text" type="button" id="btnHeadSave" value="Save" onClick="rss_head_save('HeadID','txtHeadName');" disabled>
    <input class="ui-button-text" type="button" id="btnHeadReset" value="Reset" onClick="<?=$onReset;?>">
    <input class="ui-button-text" type="button" value="Playlist Option" onClick="playlist_head_option('HeadID','TypeID');"/>
  </div>
  <div class="boxin w-100">
    <div class="header left">
      <h3><span>RSS Feed Items</span>
      <span style="margin-left:20">
      <input class="ui-button-text" type="button" value="Refresh" onClick="rss_refresh(document.getElementById('HeadID').value);"/>
      <input class="ui-button-text" type="button" value="Updated RSS" onClick="rss_show_url();"/>
      <input id="HeadID" type="hidden" size="2"/>
      <input id="RssID" type="hidden" size="2"/>
      <input id="TypeID" type="hidden" value="6" size="2"/>
      <input id="RssURL" type="hidden" size="60"/>
      </span></h3>
    </div>
    <div class="boxin w-100">
      <div style="float:left">Preview::</div>
      <div style="float:left" class="w-90"><marquee id="feedLinePreview" class="w-100">1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0</marquee></div>
    </div>
    <table cellspacing="0" class="w-100">
    <thead>
    <tr><th>Title/Content</th><th>Expired</th><th>Action</th></tr>
    </thead>
    <tbody id="tblLine_Result">
      <tr>
        <td class="w-75"><b>Head Line Title RSS</b>
        <br>Description...................RSS...................RSS...................RSS..............................
        </td>
        <td class="w-10 center">31/12/2012</td>
        <td class="w-15 center">
          <div class="dvtr top5 w-100">
             <div class="dvtd_left w-25 center"><img class="preview" src="images/icons/preview.png" title="preview"></div>
             <div class="dvtd_left w-25 center"><img class="preview" src="images/icons/down.png" title="down"></div>
             <div class="dvtd_left w-25 center"><img class="preview" src="images/icons/up.png" title="up"></div>
             <div class="dvtd_left w-25 center"><img class="preview" src="images/icons/delete.png" title="delete"></div>
          </div>
        </td>
      </tr>
      <tr>
        <td class="w-75"><b>Head Line Title RSS</b>
        <br>Description...................RSS...................RSS...................RSS..............................
        </td>
        <td class="w-10 center">31/12/2012</td>
        <td class="w-15 center">
          <div class="dvtr top5 w-100">
             <div class="dvtd_left w-25 center"><img class="preview" src="images/icons/preview.png" title="preview"></div>
             <div class="dvtd_left w-25 center"><img class="preview" src="images/icons/down.png" title="down"></div>
             <div class="dvtd_left w-25 center"><img class="preview" src="images/icons/up.png" title="up"></div>
             <div class="dvtd_left w-25 center"><img class="preview" src="images/icons/delete.png" title="delete"></div>
          </div>
        </td>
      </tr>
    </tbody>
    </table>
  </div>
</div>
<script type="text/javascript">
	$(function() {
		$('#dvUpdated').dialog({ autoOpen: false });
		$('#dvUpdated').dialog("option","width",1024);
		$('#dvUpdated').dialog("option","height",80);
		$('#dvUpdated').dialog("option","resizable",false);
	});
</script>
<div class="demo">
<div id="dvUpdated" title="Updated RSS URL.">
  <div id="dvUpdated_Body" class="top5">
    <input style="font-size:16px" type="text" size="60" id="txtRSS"/>
    <select style="font-size:16px" id="selExpired">
      <option value="7">7 Days</option>
      <option value="15" selected>2 Weeks (15 Days)</option>
      <option value="30">1 Month (30 Days)</option>
    </select>
    <input type="checkbox" id="chkOldCheck" checked> Keep Old
    <input class="btnSky" type="button" id='btnRssUpdate' value="Update" onClick="rss_update();"/>
  </div>
</div>
</div>
<script type="text/javascript">
	$(function() {
		$('#dvOption').dialog({ autoOpen: false });
		$('#dvOption').dialog("option","width",500);
		$('#dvOption').dialog("option","height",255);
		$('#dvOption').dialog("option","resizable",false);
	});
</script>
<div class="demo">
<div id="dvOption" title="Playlist Option Selection">
  <div id="tblOption_Body">
  </div>
</div>
</div>
</body>
</html>