<?php 
	$DEBUG=TRUE;
	include("./includes/db_config.inc.php");
//	include("./includes/sys_config.inc.php");
//	include("./functions/template.func.php");
//	$USER['usr_level']="Administrator";
?>
<html>
<head>
<script language="javascript" src="js/jquery.js"></script>
<?php	include("./javascript.php");?>
<script language="javascript" src="js/template.js"></script>
<link rel="stylesheet" href="css/custom-theme/jquery-ui-1.8.23.custom.css">
<link rel="stylesheet" href="css/StyleSheet.css">
<script type="text/javascript" src="js/msdropdown/jquery.dd.js"></script>
<link rel="stylesheet" href="css/msdropdown/dd.css">
<script type="text/javascript">
	$('document').ready(function() {
<?php	if (isset($TID)) {	?>
		lm_update(<?php echo $TID;?>);
<?php	} else {	?>
		lm_head_list();
<?php	}?>

//		$('#txtInsertID,#txtInsertOrder,#txtInsertObjectName,#selInsertObject').height(20).css("font-size","14px");
//		$('#txtUpdateID,#txtUpdateOrder,#selUpdateName,#txtUpdateObjectName,#txtUpdateType').height(20).css("font-size","14px");
			$('input[id^="txtInsert"]').height(20).css("font-size","14px");
			$('input[id^="txtUpdate"]').height(20).css("font-size","14px");
			$('select[id^="selInsert"]').css("font-size","14px").css("font-style","normal");
	});
	
</script>
</head>
<body>
<div id="dvHeadResult" class="w-100 boxin">
	<div class="header">
  	<h3>
    <span>Layout Management</span>
   	<span>&nbsp;
    	<input class="btnTH" id="btnListRefresh" type="button" value="refresh" onClick="lm_head_list();">&nbsp;
     	<input class="txt" id="txtSearch" type="text" value="" size="20" placeholder='กรุณากรอกคำค้นหา'>&nbsp;
     	<input class="btnTH" id="btnSearch" type="button" value="Search" onClick="table_search('tblHead_Result',1,'txtSearch');">
<!--     <input id="btnEdit" type="button" value="edit" onClick="lm_update(parseInt($('#txtSearch').val()));"> -->
			</span>
			<span style="float:right; margin-top:15px; margin-right:30px;">
    	<input class="btnTH" type="button" value=" &nbsp; New &nbsp;" onClick="lm_head_add();" title="Add Layout"/>
   	</span>
		</h3>
  </div>
  <table cellspacing="0" id="tblHead">
  <thead>
  	<tr>
      <th width="60%">Name</th>
      <th width="20%">Resolution</th>
      <th width="5%">Items</th>
      <th width="10%" colspan="5">Operation</th>
  	</tr>
  </thead>
  <tbody id="tblHead_Result">
    <tr>
      <td class="center">1</td>
      <td>2</td>
      <td>Item</td>
      <td class="center"><img class="preview" src="images/icons/edit.png" title="preview"></td>
      <td class="center"><img class="preview" src="images/icons/delete.png" title="preview"></td>
    </tr>
  </tbody>
  </table>
</div>
<div id="dvLineResult" class="boxin w-100">
<form id="formHead" name="formHead">
<div style="display:none;">
  txtOldName:<input type="text" size="2" id="txtOldName" value="">
  txtNewName:<input type="text" size="2" id="txtNewName" value="">
  txtOldDispID:<input type="text" size="2" id="txtOldDispID" value="">
  txtNewDispID<input type="text" size="2" id="txtNewDispID" value="">
  <br>TemplateID<input type="text" size="2" id="TemplateID" value="">
  InUsed<input type="text" size="2" id="InUsed" value="">
  <br>CoverID<input type="text" size="2" id="CoverID" value="">
  CoverFileID<input type="text" size="2" id="CoverFileID" value="">
  <br>LineID<input type="text" size="2" id="LineID" name="LineID" value="">
  LineName<input type="text" size="2" id="LineName" name="LineName" value="">
  LineTypeID<input type="text" size="2" id="LineTypeID" name="LineTypeID" value="">
  LineTypeName<input type="text" size="2" id="LineTypeName" name="LineTypeName" value="">
  LineTName<input type="text" size="2" id="LineTName" name="LineTName" value="">
  LineHeader<input type="text" size="2" id="LineHeader" name="LineHeader" value="">
  LineFooter<input type="text" size="2" id="LineFooter" name="LineFooter" value="">
</div>
	<div class="header">
  	<h3>Layout Information</h3><input class="btnHeader" style="margin-left:200px;" type="button" value="Back" onClick="location.reload();"/>
  </div>
<?php
		$onNameClick=sprintf("changeInputValue('txtName','Change Layout Name to ',$('#txtOldName').val(),'formHead');");
		$onNameClick.=sprintf("setObjValue('txtName','txtNewName','formHead');");
		$onNameClick.=sprintf("EnableObject('btnHeadSubmit',! checkEqual('txtNewName','txtOldName','formHead'),'formHead');");
		$sDisplay="Do you confirm to change template information?";
		$onSubmitClick=sprintf("lm_head_edit();");
		
		$onDispChange=sprintf("EnableObject('btnHeadSubmit',! checkEqual('txtOldDispID,'txtNewDispID','formHead'),'formHead');");

//		$Url=sprintf("popup_display.php?FormName=formHead&OldID=$('#txtOldDispID').val()");
//		$Url.=sprintf("&DispID=txtNewDispID&DispName=txtSize");
//		$Url.=sprintf("&btnData=%s",'btnHeadSubmit');
//		$Option=sprintf("width=%d,height=%d,location=no,menubar=no,resizeable=no,scrollbars=no,status=no,toolbar=no",710,280);
//		$onDisplayClick=sprintf("window.open('%s','_display','%s');",$Url,$Option);
		$onDisplayClick="popup_display();";

		$onReset=sprintf("EnableObject('btnHeadSubmit',false,'formHead');");

?>
	<div class="well w-95 center" style="height:120px;">
		<div style="width:100%; margin-top:10px; float:left">
			<div style="width:35%; float:left; padding-top:5px;" align="right">Layout Name : </div>
    	<div style="padding-left:5px; float:left">
   			<input class="txt" size="40" type="text" id="txtName" name="txtName" value="" disabled readonly>
      </div>
      <div style="float:left">
        	<input class="btnSky"  type="button" onClick="<?php echo $onNameClick;?>" value="Rename">
        </div>
    </div>
    
    <div style="width:100%; float:left">
		<div style="width:35%; float:left; padding-top:5px;" align="right">Display Size : </div>
    	<div style="padding-left:5px; float:left">
   			<input class="txt" size="40" type="text" id="txtSize" name="txtSize" value="<?php echo $Header[0]['disp_name'];?>" disabled readonly  onFocus="<?php echo $onDispChange;?>">
        </div>
       	<div style="float:left">
        	<input class="btnSky"  type="button" onClick="<?php echo $onDisplayClick;?>" value="Change">
        </div>
    </div>
    <div style="width:100%;">
			<div style="width:50%; float:left;" align="right">
    		<input  class="btnSky" type="button" id="btnHeadSubmit" name="btnHeadSubmit" value="Save" disabled onClick="<?php echo $onSubmitClick;?>">
      </div>
    	<div style="float:left">
   			<input  class="btnSky" type="Reset" id="btnReset" name="btnReset" value="Reset" onClick="<?php echo $onReset;?>">
      </div>
    </div>
	</div>
</form>

	<div class="boxin w-95 center">
	<div class="header left" style="vertical-align:middle;">
    Frame Image :: 
    	<select id="selFile"name="selFile" style="width:400px;"></select>
    	<select id="selFileLayer" style="display:none">
      	<option value="1" selected>Front</option>
      	<option value="0">Back</option>
      </select>&nbsp; &nbsp;
    	<input class="btnSky" type="button" value="Save" onClick="lm_file_select();"></th>
  </div>
	</div>
  <br/>

	<div class="boxin w-95 center">
	<div class="header left">
  	<span>Layout Object</span>
    <span>&nbsp;
    	<input class="btnTH" type="button" value="Refresh" onClick="lm_update($('#TemplateID').val());"> &nbsp; &nbsp;
    	<input class="btnTH" id="btnInsertObject" type="button" value="Add Object" onClick="lm_line_insert();"> &nbsp; 
    </span>
		<span style="float:right">
    	<input class="btnTh" type="button" value="Preview" onClick="layout_popup($('#TemplateID').val());"> &nbsp; &nbsp;
    	<input class="btnTh" type="button" value="Visual Layout" onClick="lm_manager();"> &nbsp; &nbsp;
    </span>
  </div>
  <table cellspacing="0">
	<thead>
  	<th nowrap>Order</th>
  	<th>Type</th>
  	<th>Object Name</th>
  	<th nowrap width="10%">Position</th>
  	<th nowrap width="10%">Dimension</th>
  	<th nowrap width="20%">Option</th>
  	<th nowrap colspan="3"width="5%">Operation</th>
  </thead>
  <tbody id="tblLine_Result">
    <tr>
      <td>5</td>
      <td>Flash</td>
      <td>Flash Object</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>
      	<input type="button" class="btnSky" value="Edit" onClick="$('#dvUpdate').dialog('open');"> &nbsp; 
        <input type="button" class="btnSky" value="Delete">
       </td>
    </tr>
  </tbody>
  </table>
	</div>
  <br>
</div>

<script type="text/javascript">
	$(function() {
		$('#dvInsert').dialog({ autoOpen: false });
		$('#dvInsert').dialog("option","width",1000);
		$('#dvInsert').dialog("option","height",175);
		$('#dvInsert').dialog("option","resizable",false);
	});
</script>
<div class="demo">
  <div id="dvInsert" title="Insert New Object">
    <div id="dvInsertObjectFull" class="center" style="display:none;"><span>Maximum Object</span></div>
    <div id="dvInsertTable" class="boxin">
      <table class="set_font_table">
        <thead>
          <tr>
          <th>Layer Order</th>
          <th>Object Type</th>
          <th>Object Name</th>
          <th width="10%">Position</th>
          <th width="10%">Dimension</th>
          <th width="20%">Option</th>
        </thead>
        <tr>
          <td class="right">
          <input class="right" id='txtInsertOrder' type="text" size="4" onKeyUp="lm_check_insert_object()" title="Order :: Over background must be more than 100">
          </td>
          <td>
            <select id="selInsertObject" onChange="lm_object_select('selInsertObject');">
              <option>Image</option>
              <option>Video</option>
              <option>Flash</option>
              <option>Audio</option>
              <option>Text Feed</option>
              <option>Acrobat</option>
              <option>PowerPoint</option>
            </select>
          </td>
          <td><input id='txtInsertObjectName' type="text" size="32" onKeyUp="lm_check_insert_object();" title="Object Name :: Mininum 3 charactors"></td>
          <td nowrap>
          <input class="right" id='txtInsertLeft' type="text" size="2" title="Placement :: Left position"> ,
          <input class="right" id='txtInsertTop' type="text" size="2" title="Placement :: Top position">
          </td>
          <td nowrap>
          <input class="right" id='txtInsertWidth' type="text" size="2" title="Resolution :: Width"> x 
          <input class="right" id='txtInsertHeight' type="text" size="2" title="Resolution :: Height"></td>
          <td>
          <span id="spInsertHeader"><input id='chkInsertHeader' type="checkbox">Title</span>
          <span id="spInsertFooter"><input id='chkInsertFooter' type="checkbox">Description</span>
          </td>
        </tr>
      </table>
    </div>
    <div class="right" id="dvInsertObjectOK" style="display:block; padding-top:10px;">
    <input id="btnObjectInsert" type="button" class="btnSky" value="Insert" onClick="lm_line_insert_select();" title="Insert Object" disabled>
    </div>
  </div>
</div>

<script type="text/javascript">
	$(function() {
		$('#dvUpdate').dialog({ autoOpen: false });
		$('#dvUpdate').dialog("option","width",1000);
		$('#dvUpdate').dialog("option","height",175);
		$('#dvUpdate').dialog("option","resizable",false);
	});
</script>
<div class="demo">
<div id="dvUpdate" title="Edit Object Information">
<div id="dvUpdateTable" class="boxin">
<table>
  <thead>
  	<tr>
  	<th>Layer Order</th>
  	<th>Object Type</th>
  	<th>Object Name</th>
  	<th>Position</th>
    <th>Dimension</th>
  	<th colspan="2">Option</th>
  </thead>
<?php 
	$onCheckEvent="$('#btnObjectUpdate').attr('disabled',false);";
?>
  <tr>
  	<td class="right">
    <input class="right" id='txtUpdateOrder' type="text" size="4" onKeyUp="<?php echo $onCheckEvent;?>" title="Order :: Over background must be more than 100">
    </td>
  	<td>
			<input id="txtUpdateType" type="text" size="8" value="" readonly>
    </td>
  	<td><input id='txtUpdateObjectName' type="text" size="32" onKeyUp="<?php echo $onCheckEvent;?>"></td>
  	<td><input class="right" id='txtUpdateLeft' type="text" size="3" onKeyUp="<?php echo $onCheckEvent;?>">
    , <input class="right" id='txtUpdateTop' type="text" size="3" onKeyUp="<?php echo $onCheckEvent;?>"> 
 		</td>
    <td><input class="right" id='txtUpdateWidth' type="text" size="3" onKeyUp="<?php echo $onCheckEvent;?>">
    x <input class="right" id='txtUpdateHeight' type="text" size="3" onKeyUp="<?php echo $onCheckEvent;?>">
    </td>
  	<td><span id="spUpdateHeader"><input id='chkUpdateHeader' type="checkbox" onChange="<?php echo $onCheckEvent;?>">Title</span></td>
  	<td><span id="spUpdateFooter"><input id='chkUpdateFooter' type="checkbox" onChange="<?php echo $onCheckEvent;?>">Description</span></td>
  </tr>
</table>
</div>
<div class="right" id="dvObjectOK" style="display:block; padding-top:10px;">
	<input id="btnObjectUpdate" type="button" class="btnSky" value="Update"  onClick="lm_line_update_select();" title="Update Object">
</div>
</div>
</div>

</body>
</html>
