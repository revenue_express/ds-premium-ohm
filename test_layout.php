<? 
	include("./includes/db_config.inc.php");
	include("./includes/sys_config.inc.php");
	$DEBUG=FALSE;
?>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<script type="text/javascript" src="./js/jquery-1.7.2.min.js"></script>
<script type="text/javascript" src="./js/jquery-ui-1.8.21.custom.min.js"></script>
<script type="text/javascript" src="./js/checkValid.js"></script>
<script type="text/javascript" src="./js/signage.js"></script>
</script>
<!--<link rel="stylesheet" href="./css/style.css">-->
<link rel="stylesheet" href="./css/custom-theme/jquery-ui-1.8.22.custom.css">
<script type="text/javascript" src="./js/layout.js"></script>
<style type="text/css">
.spMenu { cursor: pointer; }
.tdFileSize { text-align:right; padding-right: 5px;}
</style>
</head>
<body>
<div>
PlayListID :: <input type="text" id="PlayListID" size="5" value="50"/><br />
TypeID :: <input type="text" id="TypeID" size="5"/><br />
<input type='button' value="Show" onclick="layout_playlist_option('PlayListID','TypeID');"/>
</div>
<script type="text/javascript">
	$(function() {
		$('#dvOption').dialog({ autoOpen: false });
		$('#dvOption').dialog("option","width",750);
		$('#dvOption').dialog("option","height",255);
		$('#dvOption').dialog("option","resizable",false);
		
	});
</script>
<div id="dvOption" title="Option Selection">
<table>
	<thead>
  <tr><th>Category</th><th width="20%">Value</th></tr>
  </thead>
  <tbody id="tblOption_Body">
  </tbody>
</table>
</div>
</body>