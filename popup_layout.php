<? 
	$DEBUG=FALSE;
	include("./includes/db_config.inc.php");
?>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<? include("./javascript.php");?>
<!--
<script type="text/javascript" src="./js/jquery-1.7.2.min.js"></script>
<script type="text/javascript" src="./js/jquery-ui-1.8.21.custom.min.js"></script>
<script type="text/javascript" src="./js/checkValid.js"></script>
<script type="text/javascript" src="./js/signage.js"></script>
-->
<script type="text/javascript">
	$('document').ready(function() {

		$( "#TimeStart,#TimeStop" ).timepicker({
			hourGrid:4, 
			minuteGrid:10,
			showSecond: true,
			secondGrid:10,
			timeFormat: 'hh:mm:ss'
		});

		load_headlist(document.getElementById('ScheduleID').value);

		$.extend($.datepicker,{_checkOffset:function(inst,offset,isFixed){
			console.log(inst);
			console.log(offset);
			console.log(isFixed);
			offset.top -= 350;
			return offset
			}});
	});

/*	
	$(function() {
		$( "#TimeStart,#TimeStop" ).timepicker({
			hourGrid:4, 
			minuteGrid:10,
			showSecond: true,
			secondGrid:10,
			timeFormat: 'hh:mm:ss'
		});
	});
*/


	function checkSave() {
		schID=document.getElementById('ScheduleID').value;
		LineID=document.getElementById('LineID').value;
		LayoutID=document.getElementById('LayoutID').value;
		LineID=document.getElementById('LineID').value;
		LayoutID=document.getElementById('LayoutID').value;
	}
</script>
<script type="text/javascript" src="./js/popup_layout.js"></script>
</head>
<body>
<?
	if ($DEBUG) { echo "<li>Request<pre>"; print_r($_REQUEST); echo "</pre>"; }
	$bSuccess=false;
	$sScript="";
?>
<div class="boxin">
	<div class="header">
  	<span>Template Library</span>
    <span id="spHeadSearch">
    	  <input id="btnListRefresh" type="button" value="refresh" onClick="load_headlist($('#ScheduleID').val());">
        <input id="txtSearch" type="text" value="" size="10">
        <input id="btnSearch" type="button" value="search" onClick="table_search('tblLayout_body',2,'txtSearch');">
<!--		    <input id="btnEdit" type="button" value="edit" onClick="schedule_edit(parseInt($('#txtSearch').val()));"> -->
    		</span>
  </div>
	<? $onSaveClick="schedule_check_duplicate()"; ?>
  	<div style="max-height:300px; overflow:auto;">
      <table cellspacing="0">
      <thead id="tblLayout_head">
          <tr>
            <th class="w-10"></th>
            <th>Template</th>
            <th>Layout on  <input type="text" id="DispSize" size="8" readonly></th>
          </tr>
      </thead>
      <tbody id="tblLayout_body">
          <tr>
            <td><input type="radio" id="rdoCheck01" name="rdoCheck"></td>
            <td>Template Name</td>
            <td>Layout Name </td>
            <td>[preview]</td>
          </tr>
      </tbody>
      </table>
    </div>
    </div>
    <div class="boxin well">
        <div class="dvtr">
            <div class="dvtd_left top5">Template Name : 
            <input type="hidden" id="ScheduleID" value="<?=$schID;?>">
            <input type="hidden" id="LineID" value="<?=$LineID;?>">
            <input type="text" id="LayoutName" size="64" readonly><input type="hidden" id="LayoutID" value="<?=$LayoutID?>"></div>
        </div>
        <div class="dvtr top10">
            <div class="dvtd_right">
            <div class="boxno">
            <table cellspacing="0">
            <thead>
              <tr><th colspan="7">Day of week</th><th colspan="2">Time</th></tr>
              <tr>
              	<th>Sun</th>
                <th>Mon</th>
                <th>Tue</th>
                <th>Wed</th>
                <th>Thu</th>
                <th>Fri</th>
                <th>Sat</th>
                <th>Start</th>
                <th>Stop</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td><input type="checkbox" id="chkSun"></td>
                <td><input type="checkbox" id="chkMon"></td>
                <td><input type="checkbox" id="chkTue"></td>
                <td><input type="checkbox" id="chkWed"></td>
                <td><input type="checkbox" id="chkThu"></td>
                <td><input type="checkbox" id="chkFri"></td>
                <td><input type="checkbox" id="chkSat"></td>
                <td><input type="text" id="TimeStart" size="8" maxlength="8" value="08:00:00">
                  <input type="hidden" id="OldTimeStart"></td>
                <td><input type="text" id="TimeStop" size="8" maxlength="8" value="17:00:00">
                  <input type="hidden" id="OldTimeStop"></td>
              </tr>
            </tbody>
            </table>
            </div>
            </div>
        </div>
        <div class="dvtr top5">
            <div class="dvtd_left top5">
            <input type="button" class="btnSky" id="btnSave" value="Save" onClick="<?=$onSaveClick;?>"></div>
            <div class="dvtd_right top5"><input class="btnSky" type="reset" id="btnReset" value="Reset"></div>
        </div>
    </div>
</div>
</body>
</html>