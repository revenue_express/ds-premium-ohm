<? 
	$DEBUG=FALSE;
	include("./includes/db_config.inc.php");

//	include("./includes/sys_config.inc.php");

	include("./functions/client.func.php");
	include("./functions/feed.func.php");
	include("./functions/rss.func.php");
	include("./functions/command.func.php");
	include("./functions/playlist_head_option.func.php");
	$Data=NULL;

//	foreach ($_REQUEST as $Key => $Value) { $$Key=addslashes(urldecode($Value)); }
	if ( (isset($cmd)) && (isset($sid)) && (isset($cli)) ){
		if ($cli > 0) {
			$ClientInfo=json_decode(client_info($cli),true);
			$Authen=0;
			if ($DEBUG) { echo "<pre>"; 	print_r($ClientInfo); echo "</pre>"; }
			if ($ClientInfo[0]['cli_syscode'] == $sid) { $Authen = 1;}
			if ($Authen==1) {
				switch (strtolower($cmd)) {
					case 'getsch':
						$URL=sprintf("%s/gen.all.schedule.php?ClientID=%d",$LocalHost,$cli);
						$Result=file_get_contents($URL);
						load_config(NULL,$Result);
						echo $Result;
						break;
					case 'getfilelist':
						if (isset($schID)) {
							$URL=sprintf("%s/gen.schedule.file.php?schID=%d&ClientID=%d",$LocalHost,$schID,$cli);
							if ($DEBUG) echo sprintf("URL => %s",$URL);
							$Result=file_get_contents($URL);
						} else {
							$Result=NULL;
						}
						load_config(NULL,$Result);
						echo $Result;
						break;
					case 'response':
						$Result=command_response($cmdid,$status);
						if ($Result ==1) $Data="1";
						load_config(NULL,$Data);
						echo $Data;
						break;
					case 'feed':
						$Result=json_decode(playlist_feed_line_list($fid),true);
						$Data=array();
						if (count($Result) > 0) {
							for ($iCount=0; $iCount<count($Result); $iCount++) {
								$Data[]=array("Lang"=>$Result[$iCount]['feed_charset'],"feed"=>$Result[$iCount]['feed_data']);
							}
						} else {
//							$Data[]=array("Lang"=>"UTF-8","feed"=>"Now is testing system");
							$Data[]=array("Lang"=>"UTF-8","feed"=>"");
						}
						load_config(NULL,NULL,json_encode($Data));
						echo json_encode($Data);
						break;
					case 'fontfeed':
						$Result=json_decode(playlist_head_option_list($fid),true);
						$Data=NULL;
						if (count($Result) > 0) {
							$Data=array();
							for ($iCount=0; $iCount<count($Result); $iCount++) {
	//							$Data[]=array("name"=>$Result[$iCount]['pho_name'],"data"=>$Result[$iCount]['pho_value']);
								$Data[$Result[$iCount]['pho_name']]=$Result[$iCount]['pho_value'];
							}
						}
						load_config(NULL,json_decode($Data),json_encode($Data));
						echo json_encode($Data);
						break;
					case 'rss':
						$Result=json_decode(rss_line_list($rid),true);
						if (count($Result > 0)) {
							$Data=array();
							for ($iCount=0; $iCount<count($Result); $iCount++) {
								$Data[]=array("Title"=>$Result[$iCount]['rinl_title'],"Desc"=>$Result[$iCount]['rinl_desc'],"EDate"=>$Result[$iCount]['rinl_edate']);
							}
						}
						load_config(NULL,json_decode($Data),json_encode($Data));
						echo json_encode($Data);
						break;
					default :
						echo $Data;
	//					echo "<pre>"; 	print_r($_REQUEST); echo "</pre>";
						load_config(NULL);
				}
			} else {
				echo $Data;
			}
		} else {
			echo $Data;
		}
	} else {
		echo $Data;
	}
?>
