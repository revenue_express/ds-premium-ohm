	function playlist_clear() {
		var TypeID=parseInt(document.getElementById('TypeID').value);
		$('#dvHeadResult').hide();
		$('#dvLineResult').hide();
	}

	function playlist_refresh(HeadID) {
		$.ajaxSetup({async:false});

		playlist_head_info(HeadID);
		playlist_line_list(HeadID);

		$.ajaxSetup({async:true});

	}

	function playlist_head_list() {
		playlist_clear();
		var TypeID=parseInt(document.getElementById('TypeID').value);
		$('#dvHeadResult').show();
		var URL='./ajax/playlist_head_list.php';
//		alert(URL);
		$('#tblHead_Result').empty();
		var Params={'TypeID':TypeID};
		$.post(URL,Params,function(data) {
			var html="<tr><td align='center' colspan='4'>No Record Found.</td></tr>";
//			alert('Data => '+data);
			if (data.length > 0) {
				$.each(data,function(entryIndex,entry) {
					html +="<tr>";
					html +='<td class="w-10">'+entry['plh_code']+'</td>';
					html +='<td>'+entry['plh_name']+'</td>';
					html +='<td class="right">'+entry['counter']+'</td>';
					html += '<td class="center"><img class="btnPre" onClick="feed_head_preview(\'feedHeadPreview\','+entry['plh_id']+');" title="preview"> &nbsp;';
					html += '<img class="btnEdit" onClick="feed_refresh('+entry['plh_id']+');" title="edit"> &nbsp;';
					html += '<img class="btnCopy" onClick="feed_clone('+entry['plh_id']+');" title="clone"> &nbsp;';
					if (entry['plh_active']==1) {
						html += '<img class="btnDel" onClick="feed_head_active('+entry['plh_id']+',\''+entry['plh_name']+'\',0);" title="delete"></td>';
					} else {
						html += '<img class="btnRes" onClick="feed_head_active('+entry['plh_id']+',\''+entry['plh_name']+'\',1);" title="restore"></td>';
					}
					html +="</tr>";
				});
			}
			$('#tblHead_Result').append(html);
		},"json");
	}

	function playlist_line_list(HeadID) {
//		alert('playlist_line_list');
		var TypeID=parseInt(document.getElementById('TypeID').value);
		var html="<tr><td align='center' colspan='4'>No Record Found.</td></tr>";

		$('#tblLine_Result').empty();
		$('#tblLine_Result').append(html);
		switch (TypeID) {
			case 1: image_line_list(HeadID); break;
			case 2: video_line_list(HeadID); break;
			case 3: flash_line_list(HeadID); break;
			case 4: audio_line_list(HeadID); break;
			case 5: feed_line_list(HeadID); break;
			case 8: stream_line_list(HeadID); break;
			case 9: doc_line_list(HeadID); break;
			case 10: pdf_line_list(HeadID); break;
			case 12: ppt_line_list(HeadID); break;
//			case 11: ppt_line_list(HeadID); break;
		}
	}
	
	function show_file_exists() {
//		alert("show_file_exists()");
		var TypeID=parseInt(document.getElementById('TypeID').value);
		var URL='';
		var Params={};
		var TypeName='';
		switch (TypeID) {
			case 1: TypeName="images"; iCols=5; break;
			case 2: TypeName="videos"; iCols=6; break;
			case 3: TypeName="flashs"; iCols=5; break;
			case 4: TypeName="audios"; iCols=5; break;
			case 9: TypeName="docs"; iCols=4; break;
			case 10: TypeName="pdfs"; iCols=4; break;
			case 11: TypeName="covers"; iCols=5; break;
			case 12: TypeName="ppts"; iCols=4; break;
		}
		console.log('show_file_exists('+TypeName+')');
		if (TypeID == 5) {
		$('#dvSelectFile').dialog('open');
			URL='./ajax/feed_list.php';
			Params={};
			$.post(URL,Params,function(data) {
//				alert('Data => '+dump(data));
				var html="<tr><td align='center' colspan='2'>No Found</td></tr>";
				$('#tblSelectFile_Body').empty();
				if (data != null) {
					if (data.length > 0) {
						html='';
						$.each(data,function(entryIndex,entry) {
//							onClick='layout_select_file('+entry['feed_id']+",'"+entry['feed_data']+"');";
							onClick="layout_check_feed('tblSelectFile_Body','SelectFileID','btnSelectFile');";
							if (parseInt(entry['feed_active'])==1) {
								chkItem="chkItem_"+entry['feed_id'];
								html+="\n<tr><td class=\'w-5\'><input id='"+chkItem+"' name='"+chkItem+"' type='checkbox' onClick=\""+onClick+"\" value='"+entry['feed_id']+"'></td>";
								html+="<td class='left'>"+entry['feed_data'];+"</td></tr>";
							}
						});
					}
				} else {
					alert("Add Exists Failed\nURL\n"+dump(URL)+"\nParams\n"+dump(Params)+"\nData\n"+dump(data));
				}
				$('#tblSelectFile_Body').append(html);
				$('tr:odd').addClass('even');
			},"json");
		} else if (TypeID == 8) {
		$('#dvStreamFile').dialog('open');
			URL='./ajax/stream_list.php';
			Params={};
			$.post(URL,Params,function(data) {
//				alert('Data => '+dump(data));
				var html="<tr><td align='center' colspan='2'>No Found</td></tr>";
				$('#tblStreamFile_Body').empty();
				if (data != null) {
					if (data.length > 0) {
						html='';
						$.each(data,function(entryIndex,entry) {
							stmAddress=entry['stm_protocol']+"://"+entry['stm_address']+":"+entry['stm_port']+entry['stm_para1'];
							onClick='playlist_select_stream('+entry['stm_id']+",'"+entry['stm_address']+"');";
//							onClick='layout_select_file('+entry['stm_id']+",'"+stmAddress+"');";
							if (parseInt(entry['stm_active'])==1) {
								html+="\n<tr><td class=\'w-5\'><input id='rdoItem' name='rdoItem' type='radio' onClick=\""+onClick+"\"></td>";
								html+="<td class='left'>"+stmAddress+"</td></tr>";
							}
						});
					}
				} else {
					alert("Add Exists Failed\nURL\n"+dump(URL)+"\nParams\n"+dump(Params)+"\nData\n"+dump(data));
				}
				$('#tblStreamFile_Body').append(html);
				$('#tblStreamFile_Body tr:visible:even').addClass('even');
			},"json");
		} else if (TypeID == 3) {
		$('#dvSelectFlash').dialog('open');
			URL='./ajax/file_list.php';
			Params={'TypeID':TypeID};
			$.post(URL,Params,function(data) {
//				alert('Data => '+dump(data));
				var html="<tr><td align='center' colspan='3'>No record found</td></tr>";
				$('#tblSelectFlash_Body').empty();
				$('#tblSelectFlash_Body').append(html);
				if (data != null) {
					if (data.length > 0) {
						$.each(data,function(entryIndex,entry) {
							onClick='layout_select_flash('+entry['file_id']+",'"+entry['file_dname']+"');";
							html="";
							if (parseInt(entry['file_active'])==1) {
								html+="<tr><td class=\"w-5\"><input id='rdoItem' name='rdoItem' type='radio' onClick=\""+onClick+"\"></td>";
								html+="<td class='left'>"+entry['file_dname'];+"</td></tr>";
							}
							$('#tblSelectFlash_Body').append(html);
						});
						$('#tblSelectFlash_Body tr:first').hide();
						$('#tblSelectFlash_Body tr:odd').addClass('even');
					}
				} else {
					alert("Add Exists Failed\nURL\n"+dump(URL)+"\nParams\n"+dump(Params)+"\nData\n"+dump(data));
				}
			},"json");
		} else {
		$('#dvSelectFile').dialog('open');
			URL='./ajax/library_list.php';
			Params={'TypeID':TypeID};
			$.post(URL,Params,function(data) {
//				alert('Data => '+dump(data));
				var html="<tr><td align='center' colspan='5'>No record found</td></tr>";
				$('#tblSelectFile_Body').empty();
				$('#tblSelectFile_Body').append(html);
				if (data != null) {
					if (data.length > 0) {
						html='';
						$.each(data,function(entryIndex,entry) {
//							onClick='layout_select_file('+entry['file_id']+",'"+entry['file_dname']+"');";
							onClick="layout_check_file('tblSelectFile_Body','SelectFileID','btnSelectFile');";
							chkItem="chkItem_"+entry['file_id'];
							var FileName="./data/"+TypeName+"/"+entry['file_sname'];
							var onView="window.open('"+FileName+"','_view');";
							html="";
							if (parseInt(entry['file_active'])==1) {
//								html+="\n<tr><td class='w-5'><input id='rdoItem' name='rdoItem' type='radio' onClick=\""+onClick+"\"></td>";
								html="\n<tr><td class='w-5'><input id='"+chkItem+"' name='"+chkItem+"' type='checkbox' onClick=\""+onClick+"\" value='"+entry['file_id']+"'></td>";
								if (TypeID == 1) {
								var GalName=entry['file_sname'];
								var GalleryName="./data/"+TypeName+"/"+GalName.replace('.','_s.');
								html+='<td><image src="'+GalleryName+'" id="imgView_'+entry['file_id']+'" onClick="'+onView+'" title="Preview" style="cursor:pointer;"></td>';
								html+="<td class='left'>"+entry['file_dname']+" ("+entry['file_oname']+") </td>";
								} else {
								html+='<td class="w-5"><image src="./images/icons/preview.png" id="imgView_'+entry['file_id']+'" class="iconList" onClick="'+onView+'" title="Preview"></td>';
								html+="<td class='left'>"+entry['file_dname'];+"</td>";
								}
								if ((TypeID == 1) || (TypeID==2) ){
									html+="<td class='right w-15'>"+entry['Size'];+"</td>";
								}
								if ((TypeID == 2) || (TypeID == 4) ){
									html+="<td class='right w-10'>"+entry['Duration'];+"</td>";
								}
								html+="</tr>";
							}
							$('#tblSelectFile_Body').append(html);
						});
						$('#tblSelectFile_Body tr:first').hide();
						$('#tblSelectFile_Body tr:odd').addClass('even');
					}
				} else {
					alert("Add Exists Failed\nURL\n"+dump(URL)+"\nParams\n"+dump(Params)+"\nData\n"+dump(data));
				}
			},"json");
		}
	}

	function show_playlist_exists() {
		var HeadID=document.getElementById('HeadID').value;
		var TypeID=parseInt(document.getElementById('TypeID').value);
		var URL='';
		var Params={};
		console.log("show_playlist_exist("+TypeID+")");
		if (TypeID==5) {
			$('#dvSelectPlaylist').dialog('open');
			URL='./ajax/playlist_feed_head_list.php';
//			alert(URL);
			Params={};
			$.post(URL,Params,function(data) {
	//			alert(dump(data));
				$('#tblSelectPlaylist_Body').empty();
				var html="<tr><td align='center' colspan='3'>No Record Found</td></tr>";
				$('#tblSelectPlaylist_Body').append(html);
				if (data.length > 0) {
					$.each(data,function(entryIndex,entry) {
						html='';
						onClick='layout_select_playlist('+entry['plh_id']+",'"+entry['plh_name']+"');";
						if ( (parseInt(entry['counter']) > 0) && (parseInt(entry['plh_id']) != parseInt(HeadID)) ) {
							html+="\n<tr><td class=\"w-5\"><input id='rdoItem' name='rdoItem' type='radio' onClick=\""+onClick+"\"></td>";
							html+="<td>"+entry['plh_name'];+"</td>";
							html+="<td class='right w-5'>"+entry['counter']+"</td></tr>";
						}
						$('#tblSelectPlaylist_Body').append(html);
					});
					$('#tblSelectPlaylist_Body tr:first').hide();
				}
			},"json");
		} else if (TypeID==8) {
			$('#dvStreamPlaylist').dialog('open');
			URL='./ajax/playlist_stream_head_list.php';
//			alert(URL);
			Params={};
			$.post(URL,Params,function(data) {
	//			alert(dump(data));
				console.log(data);
				$('#tblStreamPlaylist_Body').empty();
				var html="<tr><td align='center' colspan='3'>No Record Found</td></tr>";
				$('#tblStreamPlaylist_Body').append(html);
				if (data.length > 0) {
					$.each(data,function(entryIndex,entry) {
						html='';
						onClick='layout_stream_playlist('+entry['plh_id']+",'"+entry['plh_name']+"');";
						if ( (parseInt(entry['counter']) > 0) && (parseInt(entry['plh_id']) != parseInt(HeadID)) ) {
							html="<tr><td class=\"w-5\"><input id='rdoItem' name='rdoItem' type='radio' onClick=\""+onClick+"\"></td>";
							html+="<td>"+entry['plh_name'];+"</td>";
							html+="<td class='right'>"+entry['counter']+"</td></tr>";
						}
						$('#tblStreamPlaylist_Body').append(html);
					});
					$('#tblStreamPlaylist_Body tr:first').hide();
				}
			},"json");
		} else {
			$('#dvSelectPlaylist').dialog('open');
			URL='./ajax/playlist_head_list.php';
//			alert(URL);
			Params={'TypeID':TypeID};
			$.post(URL,Params,function(data) {
	//			alert(dump(data));
				$('#tblSelectPlaylist_Body').empty();
				var html="<tr><td align='center' colspan='3'>No record found</td></tr>";
				$('#tblSelectPlaylist_Body').append(html);
				if (data.length > 0) {
					$.each(data,function(entryIndex,entry) {
						html='';
						onClick='layout_select_playlist('+entry['plh_id']+",'"+entry['plh_name']+"');";
						if ( (parseInt(entry['counter']) > 0) && (parseInt(entry['plh_id']) != parseInt(HeadID)) ) {
							html="<tr><td class='w-5'><input id='rdoItem' name='rdoItem' type='radio' onClick=\""+onClick+"\"></td>";
							html+="<td>"+entry['plh_name'];+"</td>";
							html+="<td class='right'>"+entry['counter']+"</td></tr>";
						}
						$('#tblSelectPlaylist_Body').append(html);
					});
					$('#tblSelectPlaylist_Body tr:first').hide();
				}
			},"json");
		}
	}
	
	function layout_select_file(file_id,file_name) {
		document.getElementById('SelectFileID').value=file_id;		
		document.getElementById('txtExistFile').value=file_name;		
		document.getElementById('btnSelectFile').disabled=false;		
	}

	function layout_check_file(Head,To,Button) {
		var val="0";
		console.log("layout_check_file("+Head+","+To+")");
		$('#'+To).val(val);
		$('#'+Button).attr('disabled','disabled');
//		$("#"+Button).removeClass('btnJQR');
		$('#'+Head+' input:checkbox:checked').each(function(i)  {
			console.log("[playlist_check_file] "+i+"==>"+$(this).val()); 
			val=val+','+$(this).val();
			$('#'+To).val(val);
/*
			if (! $('#'+Button).hasClass('btnJQR')) {
				console.log("not has Class");
				$('#'+Button).addClass('btnJQR');
			}
*/
			$('#'+Button).removeAttr('disabled');
		});
	}

	function layout_check_feed(Head,To,Button) {
		var val="0";
		console.log("layout_check_feed("+Head+","+To+")");
		$('#'+To).val(val);
		$('#'+Button).attr('disabled','disabled');
//		$("#"+Button).removeClass();
		$('#'+Head+' input:checkbox:checked').each(function(i)  {
			console.log("[layout_check_feed] "+i+"==>"+$(this).val()); 
			val=val+','+$(this).val();
			$('#'+To).val(val);
/*
			if (! $('#'+Button).hasClass('btnButton')) {
				console.log("not has Class");
				$('#'+Button).addClass('btnButton');
			}
*/
			$('#'+Button).removeAttr('disabled');
		});
	}

	function layout_select_flash(file_id,file_name) {
		document.getElementById('SelectFlashID').value=file_id;		
		document.getElementById('txtExistFlash').value=file_name;		
		document.getElementById('btnSelectFlash').disabled=false;		
	}

	function layout_select_playlist(playlist_id,playlist_name) {
		document.getElementById('SelectPlaylistID').value=playlist_id;		
		document.getElementById('txtExistPlaylist').value=playlist_name;		
		document.getElementById('btnSelectPlaylist').disabled=false;		
	}

	function layout_stream_playlist(playlist_id,playlist_name) {
		document.getElementById('StreamPlaylistID').value=playlist_id;		
		document.getElementById('txtExistPlaylist').value=playlist_name;		
		document.getElementById('btnStreamPlaylist').disabled=false;		
	}

	function layout_exist_file() {
		var TypeID=parseInt(document.getElementById('TypeID').value);
		var URL='';
		var Params={};
		switch (TypeID) {
			case 3:
				URL='./ajax/playlist_line_add.php';
				Params={'FileID':document.getElementById('SelectFlashID').value,
					'HeadID':document.getElementById('HeadID').value,
					'Para':encodeURI(document.getElementById('txtPara').value)}
				break;
			case 5:
				URL='./ajax/playlist_feed_add.php';
				Params={'FeedID':document.getElementById('SelectFileID').value,
					'HeadID':document.getElementById('HeadID').value}
				break;
			default :
				URL='./ajax/playlist_line_add.php';
				Params={'HeadID':document.getElementById('HeadID').value,
				'FileID':document.getElementById('SelectFileID').value};
		}
		$.post(URL,Params,function(data) {
//			alert(dump(data));
			if (parseInt(data) > 0) {
				alert("Add Exists Successful.");
//				alert("Add Exists Successful.\nURL\n"+dump(URL)+"\nParams\n"+dump(Params)+"\nData\n"+dump(data));
				playlist_refresh(document.getElementById('HeadID').value);
				$('#dvSelectFile').dialog('close');
				$('#dvSelectFlash').dialog('close');
			} else {
				alert("Add Exists Failed\nURL\n"+dump(URL)+"\nParams\n"+dump(Params)+"\nData\n"+dump(data));
			}
		});
	}

	function layout_exist_playlist() {
		var TypeID=parseInt(document.getElementById('TypeID').value);
		var URL='';
		var Params={};
		Params={'SrcID':document.getElementById('SelectPlaylistID').value,
			'DstID':document.getElementById('HeadID').value,
			'TypeID':TypeID}
		switch (TypeID) {
			case 5:	// FEED Libary
				URL='./ajax/playlist_feed_load.php';
				break;
			case 8: // STREAMING 
				URL='./ajax/playlist_stream_load.php';
				break;
			default :
				URL='./ajax/playlist_line_load.php';
		}
		console.log('layout_exist_playlist');
		console.log(Params);
		$.post(URL,Params,function(data) {
//			alert(dump(data));
			console.log('Return :: ' +data);
			if (parseInt(data) > 0) {
				alert("Add Exists Successful.");
				playlist_refresh(document.getElementById('HeadID').value);
				$('#dvSelectPlaylist').dialog('close');
			} else {
				alert("Add Exists Failed\nURL\n"+dump(URL)+"\nParams\n"+dump(Params)+"\nData\n"+dump(data));
			}
		});
	}
	
	function layout_add_new() {
		$('#dvHeader').hide();
		$('#dvHeadResult').hide();
		$('#dvLineResult').hide();
		$('#dvHeadAddForm').show();
		$('#dvSelectLayout').hide();
	}
