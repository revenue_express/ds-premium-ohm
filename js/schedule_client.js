	function schedule_client_clear() {
		$('#spSendSchedule').removeClass("spSchStatusWaiting").removeClass("spSchStatusOK").removeClass("spSchStatusFailed").html("");
		$('#spSendFileList').removeClass("spSchStatusWaiting").removeClass("spSchStatusOK").removeClass("spSchStatusFailed").html("");
		$('#tblFileList_Body').empty();
		$('#spDownload').removeClass("spSchStatusWaiting").removeClass("spSchStatusOK").removeClass("spSchStatusFailed").html("");
	}


	function schedule_client_run(ScheduleID) {
		console.log("schedule_client_run("+ScheduleID+")");
		$('#ScheduleID').val(parseInt(ScheduleID));
		schedule_client_clear();
		$('#dvCommand').dialog('open');
		schedule_client_resend_schedule('ClientID','ScheduleID');
		schedule_client_resend_filelist('ClientID','ScheduleID');
		schedule_client_download('ClientID');
	}


	function schedule_client_resend_schedule(ClientID,ScheduleID) {
		var URL='./ajax/send_schedule.php';
		var Params={'ClientID':$('#ClientID').val(),'ScheduleID':$('#ScheduleID').val()}
//		alert (URL+":: ClientID : "+ClientID+ "; ScheduleID : "+ScheduleID);
//		console.log("schedule_client_resend_schedule");
//		console.log("URL => "+URL);
//		console.log(Params);
		schedule_client_clear;
		$('#spSendSchedule').removeClass("spSchStatusOK").removeClass("spSchStatusFailed").html("");
		$('#spSendSchedule').addClass("spSchStatusWait").text("Waiting");
		if ( ($('#ScheduleID').val()!="") && ($('#ClientID').val() !="") ){
			$.post(URL,Params,function(data) {
//				console.log(data);
				if (parseInt(data) > 0) {
					$('#spSendSchedule').removeClass("spSchStatusWait").addClass("spSchStatusOK").text("Successful");
				} else {
					$('#spSendSchedule').removeClass("spSchStatusWait").addClass("spSchStatusFailed").text("Failed");
				}
			});
		} else {
//			console.log("schedule_client_resend_schedule :... Data Not Completed.");
			$('#spSendSchedule').removeClass("spSchStatusWait").addClass("spSchStatusFailed").text("Failed");
		}
	}
	
	function schedule_client_resend_filelist(ClientID,ScheduleID) {
		var URL='./ajax/get_download_list.php';
		var Params={'ClientID':$('#ClientID').val(),'ScheduleID':$('#ScheduleID').val()}
//		alert (URL+":: ClientID : "+ClientID+ "; ScheduleID : "+ScheduleID);
//		console.log("schedule_client_resend_filelist");
//		console.log("URL => "+URL);
//		console.log(Params);
		$('#spSendFileList').removeClass("spSchStatusWaiting").removeClass("spSchStatusOK").removeClass("spSchStatusFailed").html("");
		$.post(URL,Params,function(data) {
//			console.log(data);
			var iRun=0;
//			iMaxFile=data.length;
			if (data.length > 0) {
				$('#tblFileList_Body').empty();
				$.each(data,function(entryIndex,entry) {
//					console.log(entry);
					html="<tr><td>"+decodeURI(entry['title'])+"</td>";
					html+="<td id='sendFile_"+(iRun+1)+"'></td>";
					$('#tblFileList_Body').append(html);
					iRun++;
				});
				$('#tblFileList_Body tr:odd').addClass('even');
//				alert(Founded);

				schedule_client_send_download(ClientID,data);
/*
				$.each(data,function(entryIndex,entry) {
					console.log(entry);
					var URL2='./ajax/send_file_download.php';
					var Param2={'ClientID':$('#ClientID').val(),
														'TypeID':entry['TypeID'],
														'Title':entry['title'],
														'FName':entry['name'],
														'Hash':entry['hash'],
														'Group':entry['group']}
					console.log( iRun+" URL => "+URL2);
					console.log(Param2);
					$.post(URL2,Param2,function(data2) {
						console.log(data2);
						if (parseInt(data2) > 0) {
							$('#sendFile_'+(iRun+1)).html('success.');
							console.log(dump(data2)+" URL2 -> "+URL2+" ["+iRun+"] successful");
							iCount++;
						} else {
							console.log(dump(data2)+"URL2 -> "+URL2+" ["+iRun+"] failed");
							$('#sendFile_'+(iRun+1)).html('failed.');
						}
						iRun++;
						$('#spSendFileList').removeClass("spSchStatusWaiting").removeClass("spSchStatusOK").removeClass("spSchStatusFailed").html("");
					})
					.done(function() {
						if (iCount == iMaxFile) {
							$('#spSendFileList').removeClass("spSchStatusWait").removeClass("spSchStatusFailed").addClass("spSchStatusOK").text("Successful");
						} else {
							$('#spSendFileList').removeClass("spSchStatusWait").removeClass("spSchStatusOK").addClass("spSchStatusFailed").text("Failed");
						}
					})
					.fail(function() {
						if (iCount == iMaxFile) {
							$('#spSendFileList').removeClass("spSchStatusWait").removeClass("spSchStatusFailed").addClass("spSchStatusOK").text("Successful");
						} else {
							$('#spSendFileList').removeClass("spSchStatusWait").removeClass("spSchStatusOK").addClass("spSchStatusFailed").text("Failed");
						}
					});
*/
			} else {
//				alert('Please Try Again.');
				$('#spSendFileList').removeClass("spSchStatusWait").addClass("spSchStatusFailed").text("Failed");
			}
		},"json");
	}
	
	function schedule_client_active(ClientID,ScheduleID,Active) {
		var URL='./ajax/schedule_client_active.php';
		var Params={'ClientID':ClientID,'SchID':ScheduleID,'Active':Active};
		var Mode="Disabled";
		if (Active == 1) Mode="Enabled";
		var 	msg="Do you confirm to "+Mode+" this schedule ? ";
		if (Active == 1) msg = "Current enabled schedule will disabled.\r\n"+msg;
		var Item=new Array();
//		alert (URL+":: ClientID : "+ClientID+ "; ScheduleID : "+ScheduleID+"; Active : "+Active);
		console.log("schedule_client_active");
		console.log("URL => "+URL);
		console.log(Params);
		if(confirm(msg)) {
			$.post(URL,Params,function(data) {
				console.log(data);
				if (parseInt(data) > 0) {
//					alert(Mode+ ' Successful.');
				} else {
//					alert('Please Try Again.');
				}
				window.location.reload(true);
			});
		}
	}
	
	function schedule_client_add(ClientID,ScheduleID) {
		var URL='./ajax/schedule_client_add.php';
		var Params={'ClientID':ClientID,'SchID':ScheduleID};
		var Item=new Array();
//		alert (URL+":: ClientID : "+ClientID+ "; ScheduleID : "+ScheduleID);
		console.log("schedule_client_add");
		console.log("URL => "+URL);
		console.log(Params);
		var msg="Do you confirm to add this schedule ? ";
		if (confirm(msg)) {
			$.post(URL,Params,function(data) {
				console.log(data);
				if (parseInt(data) > 0) {
//					alert('Add Successful.');
				} else {
//					alert('Please Try Again.');
				}
				window.location.reload(true);
			});
		}
	}
	
	function schedule_client_delete(ClientID,ScheduleID) {
		var URL='./ajax/schedule_client_delete.php';
		var Params={'ClientID':ClientID,'SchID':ScheduleID};
		var Item=new Array();
//		alert (URL+":: ClientID : "+ClientID+ "; ScheduleID : "+ScheduleID);
		var Display="Do you want to delete this schedule?";
		if (confirm(Display)) {
			console.log("schedule_client_delete");
			console.log("URL => "+URL);
			console.log(Params);
			$.post(URL,Params,function(data) {
//				console.log("link => "+URL);
//				console.log("Params => "+dump(Params));
//				console.log("data => "+dump(data));
				console.log(data);
				if (parseInt(data) > 0) {
//					alert('Delete Successful.');
				} else {
//					alert('Please Try Again.');
				}
				window.location.reload(true);
			});
		}
	}
	
	function schedule_client_download(ClientID) {
		var URL='./ajax/schedule_client_download.php';
		var Params={'ClientID':$('#ClientID').val()}
//		alert (URL+":: ClientID : "+ClientID+ "; ScheduleID : "+ScheduleID);
		$('#spDownload').addClass("spSchStatusWaiting").html("waiting...");
		console.log("schedule_client_download");
		console.log("URL => "+URL);
		console.log(Params);
		$.post(URL,Params,function(data) {
			console.log(data);
			if (parseInt(data)>0) {
//				alert('Download Successful.');
				$('#spDownload').removeClass("spSchStatusWaiting").addClass("spSchStatusOK").html("Successful");
			} else {
//				alert('Please Try Again.');
				$('#spDownload').removeClass("spSchStatusWaiting").addClass("spSchStatusFailed").html("Failed");
			}
		});
	}

	function schedule_client_timer(ClientID,Start,Stop) {
		var URL='./ajax/player_timer.php';
		var Params={'ClientID':ClientID,'Start':Start,'Stop':Stop};
//		alert (URL+":: ClientID : "+ClientID+ "; ScheduleID : "+ScheduleID);
		console.log("schedule_client_timer");
		console.log("URL => "+URL);
		console.log(Params);
		$.post(URL,Params,function(data) {
			console.log(data);
			if (data != null) {
//				alert('Set Download Timer Successful.');
			} else {
//				alert('Please Try Again.');
			}
		});
	}

	function schedule_client_send_download(ClientID,Data) {
		var iRun=0;
		var iCount=0;

		$.each(Data,function(entryIndex,entry) {
			var URL='./ajax/send_file_download.php';
			var Param={'ClientID':$('#ClientID').val(),
												'TypeID':entry['TypeID'],
												'Title':entry['title'],
												'FName':entry['name'],
												'Hash':entry['hash'],
												'Group':entry['group']}
			console.log( iRun+" URL => "+URL);
			console.log(Param);
			$.post(URL,Param,function(data) {
				if (parseInt(data) > 0) {
					$('#sendFile_'+(iRun+1)).html('success.');
					console.log(dump(data)+" URL -> "+URL+" ["+iRun+"] successful");
					iCount++;
				} else {
					console.log(dump(data)+"URL -> "+URL+" ["+iRun+"] failed");
					$('#sendFile_'+(iRun+1)).html('failed.');
				}
				iRun++;
			});
			sleep(1000);
		});

		if (iCount == iRun) {
			$('#spSendFileList').removeClass("spSchStatusWait").removeClass("spSchStatusFailed").addClass("spSchStatusOK").text("Successful");
		} else {
			$('#spSendFileList').removeClass("spSchStatusWait").removeClass("spSchStatusOK").addClass("spSchStatusFailed").text("Failed");
		}

	}
	
	function schedule_client_history(ClientID) {
		$('#dvHistoryList').accordion("destroy").text("");
		var URL='./ajax/schedule_client_history.php';
		var Params={'ClientID':$('#'+ClientID).val()};
//		alert (URL+":: ClientID : "+ClientID+ "; ScheduleID : "+ScheduleID);
		console.log("schedule_client_history");
		console.log("URL => "+URL);
		console.log(Params);
		var iRun=0;
		var sHead="";
		$.post(URL,Params,function(data) {
			console.log(data);
			if (data.length > 0) {
				$.each(data,function(entryIndex,entry) {
					html="";
					if (entry['cmd_para4'] != sHead) {
						html="<h3><span style='margin-left:20px; font-weight:18px;'>"+entry['cmd_para4']+"<span></h3>";
						html+="<div style='overflow:auto; max-height:300px;'><table id='tbl_"+entry['cmd_para4']+"' class='demo' border='1'>";
						html+="<tr><th>Type</th><th>File Name</th><th>Send</th><th>Response</th><th></th></tr></table></div>";
						sHead=entry['cmd_para4'];
						$('#dvHistoryList').append(html);
					} else {
						bSame=(entry['cmd_rtime']!=entry['cmd_stime']);
						html="<tr>";
						html+="<td>"+entry['cmd_para1']+"</td>";
						html+="<td>"+entry['cmd_para2']+"</td>";
						html+="<td nowrap>"+entry['cmd_stime']+"</td>";
						html+="<td nowrap>"+entry['cmd_rtime']+"</td>";
						html+="<td>"+((bSame)?"OK":"NO")+"</td>";
						html+="</tr>";
						$('#tbl_'+entry['cmd_para4']).append(html);
					}
					console.log("html=>"+html);
				});
				$('#dvHistoryList').accordion({heightStyle: "content"});
			}
		},"json");
	}