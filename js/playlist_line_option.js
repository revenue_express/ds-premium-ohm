	function playlist_line_option(TypeID,LineID) {
		var URL='./ajax/type_option_list.php';
//		alert(URL);
		var Params={'TypeID':$('#TypeID').val()};
		var Item=new Array();
		console.log("[playlist_line_option] Start::")
		$.ajaxSetup({async:false});
		$.post(URL,Params,function(data) {
//			alert('data =>'+dump(data));
			$('#tblLineOption_Body').empty();
			var ItemList=LineID.toString();
			var OptionName="";
//			alert(dump(data));
			if (data != null) {
				if (data.length > 0) {
					var html='<div class="well">';
					$.each(data,function(entryIndex,entry) {
						console.log("[playlist_line_option] Loop:: "+entryIndex);
	//				for (iRun=0; iRun<Item.length; iRun++) {
	//					html+="\n<tr><td>"+Item[iRun]['Name']+"</td><td>";
						html+="\n<div class='dvtr'><div class='dvtd_left w-40'>"+entry['Desc']+"</div>";
						if (entry['Type'].toLowerCase()=='option') {
							OptionName="sel_line_"+entry['Name'];
							html+='<div class="dvtd_right"><label><select id="'+OptionName+'" name="'+entry['Desc']+'">';
							for (iCount=0; iCount < entry['Item'].length; iCount++) {
								html+='<option value="'+entry['Item'][iCount]['Value']+'">'+entry['Item'][iCount]['Name']+'</option>';
							}
							html+='</label></select>';
						} else if (entry['Type'].toLowerCase()=='check') {
							OptionName="";
							html+="<div class='dvtd_right top5'>";
							for (iCount=0; iCount < entry['Item'].length; iCount++) {
								chkName="chk_line_"+entry['Name'].toLowerCase()+"_"+entry['Item'][iCount]['Name'].toLowerCase();
								var sName=entry['Desc']+" "+entry['Item'][iCount]['Name'];
								html+='<input id="'+chkName+'"type="checkbox" name="'+sName+'">'+entry['Item'][iCount]['Name'];
								if (iCount > 0) {
									OptionName+=","+chkName;
								} else {
									OptionName=chkName;
								}
							}
						} else if (entry['Type'].toLowerCase()=='css') {
								html+="<div class='dvtd_right'>";
								OptionName="css_line_"+entry['Name'];
								html +='<input id="'+OptionName+'" type="color" name="'+entry['Desc']+'">';	
								onClick="$('#"+OptionName+"').val('')";
								CheckName="chk_line_"+OptionName;
								html +='<input id="'+CheckName+'" type="checkbox"> No Color';					
						} else if (entry['Type'].toLowerCase()=='text') {
	//					} else if (entry['Type']=='TEXT') {
								html+="<div class='dvtd_right'>";
								OptionName="txt_line_"+entry['Name'];
								html +='<input class="txt" id="'+OptionName+'" size="40" name="'+entry['Desc']+'">';						
						} else if (entry['Type'].toLowerCase()=='range') {
	//					} else if (entry['Type']=='TEXT') {
								html+="<div class='dvtd_right'>";
								OptionName="range_line_"+entry['Name'];
								html +='<input type="range" id="'+OptionName+'" name="'+entry['Desc']+'" ';
								html +='min="'+entry['Item'][0]['Value']+'" max="'+entry['Item'][0]['Val02']+'" step="'+entry['Item'][0]['Val03']+'">';						
						} else if (entry['Type'].toLowerCase()=='int') {
								html+="<div class='dvtd_right'>";
								OptionName="int_line_"+entry['Name'];
								html +='<input type="number" class="txt" id="'+OptionName+'" name="'+entry['Desc']+'">';						
						}
						ItemList+=","+OptionName;
						console.log("[playlist_line_option] :: Create '"+OptionName+"'"); 
						html +='</div></div>';
	//					alert("html => "+html); 
					});
	//				alert("ItemList ->"+ItemList);
					html+="<div class='dvtr top10 center demo'>";
					html+="<input id='btnLineOptionSave' class='btnSky' type='button' onClick='playlist_line_option_save("+ItemList+");' value='Save'>";
					html+="</div>";
					html+="</div>";
					console.log("[playlist_line_option] ItemList::"+ItemList)
				} else {
					var html="<tr><td align='center'>No Layout Information</td></tr>"
				}
			} else {
					html="<tr><td align='center'>No Playlist Option.</td></tr>"
			}
			$('#tblLineOption_Body').append(html);
			$('#dvLineOption').dialog('open');
		},"json");
		$.ajaxSetup({async:true});

		playlist_line_option_load($('#LineID').val());
		console.log("[playlist_line_option] Finished::")
	}
	
	function playlist_line_option_save() {
//		alert("layout_playlist_option_save \n"+dump(arguments));
		P_ID=arguments[0].id;
		P_Name=arguments[0].name;
		PID=arguments[0].value;
//		PID=$('#'+P_Name).val();

		console.log("[playlist_line_option_save]");
		console.log(arguments);
//		alert( "P_ID =>"+P_ID+ "; P_Name => "+P_Name+"; PID=>"+PID);
//		Type=argument[1];
		$.ajaxSetup({async:false});
		var Params;
		var Result="";
		for (var iCount=1; iCount<arguments.length; iCount++ ) {
			sID=arguments[iCount].id;
			sName=arguments[iCount].name;
			sValue=arguments[iCount].value;
			var iPos=sID.indexOf("_");
			var sObjName=sID.split("_");
			
			console.log('Name : '+sID+' => '+dump(sObjName));
			sHead=sObjName[0].toLowerCase();
//			sHead=sID.substr(0,iPos).toLowerCase();
			if (sHead=="chk") {
				sObject='Check';
			} else if (sHead=="sel") {
				sObject='Option';
			} else if (sHead=="int") {
				sObject='INT';
			} else if (sHead == "txt") {
				sObject='TEXT';
			} else if (sHead == 'css') {
				sObject='CSS';
			} else if (sHead == "range") {
				sObject='RANGE';
			}
			sVal01=sObjName[2];
			sVal02=sObjName[3];
			URL='./ajax/playlist_line_option_save.php';
			if (sHead=='chk') { 
				Params={'ItemID':PID,
					'txtType':sObject,
					'txtCategory':sVal01,
					'txtValue':sVal02};
				if (! arguments[iCount].checked) {
					URL='./ajax/playlist_line_option_delete.php';
				}
			} else if (sHead=='css') { 
				Params={'ItemID':PID,
					'txtType':sObject,
					'txtCategory':sVal01,
					'txtValue':sValue};
				var chkData=$('#chk_'+sID).attr('checked');
				if (chkData) {
					URL='./ajax/playlist_line_option_delete.php';
				}
			} else { 
//				sVal01=sID.substr(iPos+1);
				Params={'ItemID':PID,
					'txtType':sObject,
					'txtCategory':sVal01,
					'txtValue':sValue};
				if (sValue.length == 0) {
					URL='./ajax/playlist_line_option_delete.php';
				}
			} 
			sDisplay="\nID=>"+sID;
			sDisplay+="\nValue=>"+sValue;
			sDisplay+="\nName=>"+sName;
			sDisplay+="::=>"+sHead;
			sDisplay+="::"+sVal01;
			sDisplay+="::"+sVal02;
//			alert(sDisplay+"\nURL=>"+URL+"\n"+dump(Params));
			console.log("playlist_line_option_save:: URL=>"+URL);
			console.log(Params);
			$.post(URL,Params,function(data) {
//				alert(URL+ '\n'+dump(Params)+'\n'+dump(data));
				if (data != '0') {
//					alert('Update '+sName+' Completed.');
					Result+='\nUpdate '+sName+' Completed.';
//				} else {
//					alert('Update '+sName+' Failed.'); 
//					Result+='\nUpdate '+sName+' Failed.';
				}
			});
		}
		$.ajaxSetup({async:true});
		$('#dvLineOption').dialog('close');
		alert(Result)
	}
	
	function playlist_line_option_load(ItemID) {
	var URL='./ajax/playlist_line_option_list.php';
	var Params={ 'ItemID': ItemID};
		console.log("[playlist_line_option_load]");
		console.log(Params);
		$.post(URL,Params,function(data) { 
			if (data.length > 0) {
//				alert("result =>"+dump(data));
				var objName='';
				$.each(data,function(entryIndex,entry) {
					console.log("data"+dump(entry));
					if (entry['plo_type'].toLowerCase() == 'check') {
						objName="chk_line_"+entry['plo_name']+"_"+entry['plo_value'].toLowerCase();
					} else if (entry['plo_type'].toLowerCase() == 'css') {
						objName="css_line_"+entry['plo_name'];
					} else if (entry['plo_type'].toLowerCase() == 'option') {
						objName="sel_line_"+entry['plo_name'];
					} else if (entry['plo_type'].toLowerCase() == 'int') {
						objName="int_line_"+entry['plo_name'];
					} else if (entry['plo_type'].toLowerCase() == 'text') {
						objName="txt_line_"+entry['plo_name'];
					} else if (entry['plo_type'].toLowerCase() == 'range') {
						objName="range_line_"+entry['plo_name'];
					} 
//					alert('ObjName =>'+objName);
					if (document.getElementById(objName) != undefined) {
//					if ($('#tblLineOption_Body input[name="'+objName+'"]') != undefined) {
//							console.log("[playlist_line_option_load] Found ["+objName+"]");
//							console.log($('#tblLineOption_Body input[name="'+objName+'"]').attr());
//						alert('ObjName =>'+objName+' value=>'+document.getElementById(objName).value);
						if (entry['plo_type'].toLowerCase() == 'check') {
//							alert('ObjName =>'+objName+" => check");
							document.getElementById(objName).checked=true;
//							$('#tblLineOption_Body input[name="'+objName+'"]').attr('checked',true);
						} else {
//							alert('ObjName =>'+objName+" Old => "+document.getElementById(objName).value +" New => "+entry['pho_value']);
							document.getElementById(objName).value=entry['plo_value'];
//							$('#tblLineOption_Body input[name="'+objName+'"]').val(entry['plo_value']);
						}
					} else {
//						alert('ObjName =>'+objName+" => No Defined.");
							console.log("[playlist_line_option_load] Error:: no found Item ["+objName+"]");
					}
				});
			}
		},"json");
	}