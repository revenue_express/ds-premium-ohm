	function feed_clear() {
		$('#dvHeadResult').hide();
		$('#dvLineResult').hide();
	}

	function feed_preview(Viewer,Feed) {
		display=document.getElementById(Viewer);
		display.innerHTML=Feed;
		display.focus();
	}

	function feed_head_list() {
		feed_clear();
		$('#dvHeadResult').show();
//		var URL='./ajax/playlist_head_list.php';
		var URL='./ajax/playlist_feed_head_list.php';
		var user_level=window.parent.document.getElementById('usr_level').value;
		var user_name=window.parent.document.getElementById('usr_name').value;
		var user_id=window.parent.document.getElementById('usr_id').value;
//		alert(URL);
		$('#tblHead_Result').empty();
		var Params={'TypeID':parseInt(document.getElementById('TypeID').value)};
		$.post(URL,Params,function(data) {
			var html='';
//			alert('Data => '+data);
			if (data.length > 0) {
				$.each(data,function(entryIndex,entry) {
					onEdit="feed_refresh("+entry['plh_id']+");";
					onCopy="feed_clone("+entry['plh_id']+")";
					onDelete="feed_head_active("+entry['plh_id']+",'"+entry['plh_name']+"',0);";
					onRestore="feed_head_active("+entry['plh_id']+",'"+entry['plh_name']+"',1);";

					if (entry['plh_active']==1) {
						html +="<tr>";
						html +='<td class="w-10">'+entry['plh_code']+'</td>';
						html +='<td>'+entry['plh_name']+'</td>';
						html +='<td class="right">'+entry['counter']+'</td>';
						html += '<td><div class="dvtr" style="margin-left:15%;">';
	//					html +='<img class="btnPre" onClick="feed_head_preview(\'feedHeadPreview\','+entry['plh_id']+');" title="preview"> &nbsp;';
						html += '<div class="dvtd_left w-30"><div class="btnEdit" onClick="'+onEdit+'" title="edit"></div></div>';
						html += '<div class="dvtd_left w-30"><div class="btnCopy" onClick="'+onCopy+'" title="clone"></div></div>';
						if (entry['Used'] == 0) {
							html += '<div class="dvtd_left"><div class="btnDel" onClick="'+onDelete+'" title="delete"></div></div>';
						}
						html +="</td>";
//						html +='<td class="right">'+entry['Used']+'</td>';
						html +='</tr>';
					} else {
						if (user_level.toString().toLowerCase()!='operator') {
							html +="<tr>";
							html +='<td class="w-10">'+entry['plh_code']+'</td>';
							html +='<td>'+entry['plh_name']+'</td>';
							html +='<td class="right">'+entry['counter']+'</td>';
							html += '<td><div class="dvtr" style="margin-left:15%;">';
		//					html +='<img class="btnPre" onClick="feed_head_preview(\'feedHeadPreview\','+entry['plh_id']+');" title="preview"> &nbsp;';
							html += '<div class="dvtd_left w-30"><div class="btnEdit" onClick="'+onEdit+'" title="edit"></div></div>';
							html += '<div class="dvtd_left w-30"><div class="btnCopy" onClick="'+onCopy+'" title="clone"></div></div>';
							html += '<div class="dvtd_left"><div class="btnRes" onClick="'+onRestore+'" title="restore"></div></div>';
							html +="</td>";
//							html +='<td class="right">'+entry['Used']+'</td>';
							html +='</tr>';
						}
					}

				});
			} else {
				var html="<tr><td align='center' colspan='4'>No Record Found.</td></tr>";
			}
			$('#tblHead_Result').append(html);
			$('tr:odd').addClass('even');
		},"json");
	}
	
	function feed_head_add() {
//		feed_clear();
		var URL='./ajax/playlist_head_add.php';
		var Params={'TypeID':5};
//		alert(URL);
		$.post(URL,Params,function(data) {
			var html='';
			console.log('[feed_head_add] data => '+dump(data));
			if (parseInt(data) > 0) {
				feed_refresh(parseInt(data));
			} else {
			}
		});
	}
	
	function feed_head_preview(ObjName,HeadID) {
		var URL='./ajax/playlist_feed_line_list.php';
//		alert(URL);
		var Params={'HeadID':HeadID};
		var html='Example :: ';
		$.post(URL,Params,function(data) {
			if (data.length > 0) {
//				alert ('Data'+dump(data));
				html="";
				$.each(data,function(entryIndex,entry) {
					html+=" &nbsp; &nbsp; ";
					html+=entry['feed_data']; 
				});
			} else {
				html+=" No Record Found";
			}
			feed_preview(ObjName,html);
		},"json");
	}
	
	function feed_refresh(HeadID) {
		feed_clear();
		$('#dvLineResult').show();
		document.getElementById('HeadID').value=HeadID;
		playlist_head_info(HeadID);
		feed_line_list(HeadID);
	}
	
	function playlist_head_info(HeadID) {
		var URL='./ajax/playlist_head_info.php';
		var Params={'HeadID':HeadID};
		var html='';
		$.post(URL,Params,function(data) {
			if (data.length > 0) {
				document.getElementById('TypeID').value=data[0]['plh_type_id'];
				document.getElementById('HeadID').value=HeadID;
				document.getElementById('txtHeadName').value=data[0]['plh_name'];
				document.getElementById('txtOldName').value=data[0]['plh_name'];
			}
		},"json");
	}
	
	function feed_line_list(HeadID) {
		var URL='./ajax/playlist_feed_line_list.php';
		var Params={'HeadID':HeadID};
		var user_level=window.parent.document.getElementById('usr_level').value;
		var user_name=window.parent.document.getElementById('usr_name').value;
		var user_id=window.parent.document.getElementById('usr_id').value;
		$.post(URL,Params,function(data) {
			var html='<tr><td colspan="2" class="center"><h3>No Feed Item.</h3></td></tr>';
			if (data.length > 0) {
				html='';
				var iRun=0;
				var LastID=0;
				var NextID=0;
				$.each(data,function(entryIndex,entry) {
					onView='feed_preview(\'feedLinePreview\',\''+entry['feed_data']+'\');"';
					onDelete="feed_line_active("+entry['plf_id']+",'"+entry['feed_data']+"',0)";
					onRestore="feed_line_active("+entry['plf_id']+",'"+entry['feed_data']+"',1)";
					onUp="feed_line_order("+HeadID+","+LastID+","+entry['plf_id']+");";
					onEdit="feed_line_show_edit("+entry['plf_id']+");";

					if (entry['plf_active']==1) {
						html+='<tr>';
						html+='<td class="left">'+entry['feed_data']+'</td>';
						html+='<td class="center" width="120">';
						html+='<div class="dvtr top5 w-100">';
						html+='<div class="dvtd_left w-20 center">';
						html+='<img class="preview" src="images/icons/preview.png" title="preview" onClick="'+onView+'"></div>';
          	html+='<div class="dvtd_left w-20 center">';
						if (iRun > 0) {
							html+='<img class="preview" src="images/icons/up.png" title="up" onClick="'+onUp+'">';
						} else { html+="&nbsp;"; }
						html+='</div>';
						html+='<div class="dvtd_left w-20 center">';
						if (iRun+1 < data.length) {
							NextID=data[iRun+1]['plf_id'];
							onDown="feed_line_order("+HeadID+","+NextID+","+entry['plf_id']+");";
							if (parseInt(data[iRun+1]['plf_active'])==1) {
								html+='<img class="preview" src="images/icons/down.png" title="down" onClick="'+onDown+'">';
							} else { html+="&nbsp;"; }
						} else { html+="&nbsp;"; }
						html+='</div>';
						html+='<div class="dvtd_left w-20 center">';
						html+='<img class="preview" src="images/icons/edit.png" title="edit" onClick="'+onEdit+'">';
						html+='</div>';
          	html+='<div class="dvtd_left w-20 center">';
						html+='<img class="preview" src="images/icons/delete.png" title="delete" onClick="'+onDelete+'">';
						html+='</div>';
						html+='</div>';
						html+='</td>';
						html+='</tr>';
						LastID=entry['plf_id'];
					} else {
						if (user_level.toString().toLowerCase()!='operator') {
							html+='<tr>';
							html+='<td class="left">'+entry['feed_data']+'</td>';
							html+='<td class="center" width="120">';
							html+='<div class="dvtr top5 w-100">';
							html+='<div class="dvtd_left w-20 center">';
							html+='<img class="preview" src="images/icons/preview.png" title="preview" onClick="'+onView+'"></div>';
							html+='<div class="dvtd_left w-20 center">&nbsp;</div>';
							html+="<div class='dvtd_left w-20 center'>&nbsp;</div>";
							html+='<div class="dvtd_left w-20 center">&nbsp;</div>';
							html+='<div class="dvtd_left w-20 center">';
							html+='<img class="preview" src="images/icons/restore.png" title="restore" onClick="'+onRestore+'">';
							html+='</div>';
							html+='</div>';
							html+='</td>';
							html+='</tr>';
						}
					}
					iRun++;
				});
			}
			$('#tblLine_Result').empty();
			$('#tblLine_Result').append(html);
			$('tr:odd').addClass('even');
		},"json");
	}
	
	
	function feed_line_show_feed_exists() {
		$('#dvSelectText').dialog('open');
		var URL='./ajax/feed_list.php';
//		alert(URL);
		var Params={};
		$.post(URL,Params,function(data) {
//			alert('Data => '+data);
			var html="<tr><td align='center' colspan='2'>No Found</td></tr>";
			$('#tblSelectText_Body').empty();
			if (data.length > 0) {
				html='';
				$.each(data,function(entryIndex,entry) {
					if (parseInt(entry['feed_active'])==1) {
						onClick='feed_select_text('+entry['feed_id']+",'"+entry['feed_data']+"');";
						html+="\n<tr><td class=\"w-5\"><input id='rdoItem' name='rdoItem' type='radio' onClick=\""+onClick+"\"></td>";
						html+="<td>"+entry['feed_data'];+"</td></tr>";
					}
				});
			}
			$('#tblSelectText_Body').append(html);
			$('tr:odd').addClass('even');
		},"json");
	}

	function feed_select_text(feed_id,feed_data) {
		document.getElementById('SelectFeedID').value=feed_id;		
		document.getElementById('txtExistFeed').value=feed_data;		
		$("#btnSelectFeed").removeClass();
		document.getElementById('btnSelectFeed').disabled=false;
		document.getElementById('btnSelectFeed').className = 'btnButton';
			
	}
	
	function feed_exist_feed() {
		var URL='./ajax/playlist_feed_add.php';
		var Params={'HeadID':document.getElementById('HeadID').value,
			'FeedID':document.getElementById('SelectFeedID').value,
			'txtName':encodeURI(document.getElementById('txtExistFeed').value)};
		$.post(URL,Params,function(data) {
//			alert(dump(data));
			if (parseInt(data) > 0) {
//				alert("Add Exists Successful.");
				feed_refresh(document.getElementById('HeadID').value);
				$('#dvSelectText').dialog('close');
			} else {
//				alert("Add Exists Failed");
			}
		});
	}

	function feed_line_show_playlist_exists() {
		var HeadID=document.getElementById('HeadID').value;
		$('#dvSelectPlaylist').dialog('open');
		var URL='./ajax/playlist_feed_head_list.php';
//		alert(URL);
		var Params={};
		$.post(URL,Params,function(data) {
//			alert(dump(data));
			$('#tblSelectPlaylist_Body').empty();
			var html="<tr><td align='center' colspan='2'>No Found</td></tr>";
			if (data.length > 0) {
				html='';
				$.each(data,function(entryIndex,entry) {
					onClick='feed_select_playlist('+entry['plh_id']+",'"+entry['plh_name']+"');";
					if ( (parseInt(entry['counter']) > 0) && (parseInt(entry['plh_id']) != parseInt(HeadID)) && (parseInt(entry['plh_active'])==1) ) {
						html+="\n<tr><td class=\"w-5\"><input id='rdoItem' name='rdoItem' type='radio' onClick=\""+onClick+"\"></td>";
						html+="<td>"+entry['plh_name'];+"</td>";
						html+="<td class='right'>"+entry['counter']+"</td></tr>";
					}
				});
			}
			$('#tblSelectPlaylist_Body').append(html);
			$('tr:odd').addClass('even');
		},"json");
	}

	function feed_select_playlist(playlist_id,playlist_name) {
		document.getElementById('SelectPlaylistID').value=playlist_id;		
		document.getElementById('txtExistPlaylist').value=playlist_name;		
		$("#btnSelectPlaylist").removeClass();
		document.getElementById('btnSelectPlaylist').disabled=false;
		document.getElementById('btnSelectPlaylist').className = 'btnButton';		
	}

	function feed_exist_playlist() {
		var URL='./ajax/playlist_feed_load.php';
		var Params={'SrcID':document.getElementById('SelectPlaylistID').value,
			'DstID':document.getElementById('HeadID').value}
		$.post(URL,Params,function(data) {
//			alert(dump(data));
			if (parseInt(data) > 0) {
//				alert("Add Exists Successful.");
				feed_refresh(document.getElementById('HeadID').value);
				$('#dvSelectPlaylist').dialog('close');
			} else {
//				alert("Add Exists Failed");
			}
		});
	}

	function feed_line_order(HeadID,DstID,SrcID) {
//		alert("Head => "+HeadID+"; Dest => "+DstID+"; Src => "+SrcID);
		var URL='./ajax/playlist_feed_move.php';
		var Params={'HeadID':HeadID,'NextID':DstID,'CurrID':SrcID};
		$.post(URL,Params,function(data) {
			if (parseInt(data) > 0) {
//				alert("Ordering Successful.");
				feed_refresh(HeadID);
			} else {
//				alert("Ordering Failed");
			}
		});
	}
	
	function feed_line_show_edit(LineID) {
		console.log("[feed_line_show_edit] LineID =>"+LineID);
		$('#dvEditText').dialog('open');
		document.getElementById('txtFeed_Edit').value='';
		document.getElementById('FeedID_Edit').value='';
		document.getElementById('LineID_Edit').value='';

		if (! ((LineID=='') || (LineID==undefined))) {
			var URL='./ajax/playlist_feed_info.php';
			var Params={'LineID':LineID};
			$.post(URL,Params,function(data) {
//				alert(dump(data));
				if (data.length > 0) {
					document.getElementById('txtFeed_Edit').value=data[0]['feed_data'];
					document.getElementById('LineID_Edit').value=LineID;
					document.getElementById('FeedID_Edit').value=data[0]['feed_id'];
				}
			},"json");
		}
	}
	
	function feed_line_show_add() {
		$('#dvAddText').dialog('open');
		document.getElementById('txtNewFeed').value='';
		document.getElementById('FeedID').value='';
		document.getElementById('LineID').value='';
	}

	function feed_line_add() {
		var HeadID=document.getElementById('HeadID').value;
		var txtFeed=document.getElementById('txtNewFeed').value;
		var Params={'HeadID':HeadID,'txtName':txtFeed};
		var URL='./ajax/playlist_feed_add.php';
		if (! ((HeadID=='') || (HeadID==undefined))) {
			$.post(URL,Params,function(data) {
//				alert (dump(data));
				if (parseInt(data) > 0) {
					alert('Add Completed');
					$('#dvAddText').dialog('close');
					feed_refresh(document.getElementById('HeadID').value);					
				} else {
					alert('Add Failed');
				}
			});
		}
	}

	function feed_line_save() {
		var HeadID=document.getElementById('HeadID').value;
		var FeedID=document.getElementById('FeedID_Edit').value;
		var LineID=document.getElementById('LineID_Edit').value;
		var txtFeed=document.getElementById('txtFeed_Edit').value;
		var bOnly=document.getElementById('chkFeedOnly').checked;
		var Params={'HeadID':HeadID,'LineID':LineID, 'FeedID':FeedID, 'txtName':txtFeed, 'bOnly':bOnly };
		var URL='./ajax/playlist_feed_edit.php';
			$.post(URL,Params,function(data) {
				console.log("[feed_line_save] Return =>"+data);
				if (parseInt(data) > 0) {
//					alert('Update Completed');
					$('#dvEditText').dialog('close');
					feed_refresh(document.getElementById('HeadID').value);					
				} else {
//					alert('Update Failed');
				}
			});
	}
	
	function playlist_head_save() {
		var HeadID=parseInt(document.getElementById('HeadID').value);
		var TypeID=parseInt(document.getElementById('TypeID').value);
		var HeadName=encodeURI(document.getElementById('txtHeadName').value);
		var Params={'HeadID':HeadID,'Name':HeadName};
		var URL='./ajax/playlist_head_save.php';
		if (! ((HeadID=='') || (HeadID==undefined))) {
			$.post(URL,Params,function(data) {
//				alert (dump(data));
				if (parseInt(data) > 0) {
//					alert('Save Completed');
					if (TypeID == 5) {
						feed_refresh(document.getElementById('HeadID').value);					
					} else {
						playlist_refresh(document.getElementById('HeadID').value,TypeID);					
					}
				} else {
//					alert('Save Failed');
				}
			});
		}
	}
	
	function feed_head_active(HeadID,HeadName,Active) {
		if (! ((HeadID=='') || (HeadID==undefined))) {
			var sDisplay="Do you confirm to delete '"+HeadName+"' playlist ?";
			var answer=confirm(sDisplay);
			if (answer) {
				var Params={'HeadID':HeadID,'Active':Active};
				var URL='./ajax/playlist_head_active.php';
				$.post(URL,Params,function(data) {
//					alert (dump(data));
					if (parseInt(data) > 0) {
//						alert('Save Completed');
						feed_head_list();			
					} else {
//						alert('Save Failed');
					}
				});
			}
		}
	}
	
	function feed_line_active(LineID,LineValue,Active) {
//		var HeadID=document.getElementById('HeadID').value;
		var HeadID=parseInt(document.getElementById('HeadID').value);
		if (! ((LineID=='') || (LineID==undefined))) {
			var Mode='Delete';
			if (Active==1) Mode="Restore"; 
			var sDisplay="Do you confirm to "+Mode+" on '"+LineValue+"' Item ?";
			var answer=confirm(sDisplay);
			if (answer) {
				var Params={'LineID':LineID,'Active':Active};
				var URL='./ajax/playlist_feed_active.php';
				$.post(URL,Params,function(data) {
//					alert (dump(data));
					if (parseInt(data) > 0) {
//						alert(Mode+' Completed');
						feed_line_list(HeadID);			
					} else {
//						alert(Mode+' Failed');
					}
				});
			}
		}
	}

	function feed_clone(HeadID) {
		var URL='./ajax/playlist_head_add.php';
		var Params={'TypeID':5};
//		alert(URL);
		$.post(URL,Params,function(data) {
			var html='';
			console.log('[feed_clone] data => '+dump(data));
			if (parseInt(data) > 0) {
				newHeadID=parseInt(data);
				var URL='./ajax/playlist_feed_load.php';
				var Params={'SrcID':HeadID,'DstID':newHeadID}
				$.post(URL,Params,function(data) {
//			alert(dump(data));
					if (parseInt(data) > 0) {
						feed_refresh(newHeadID);
					}
				});
			}
		});

	}