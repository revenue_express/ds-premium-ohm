	function rss_clear() {
		$('#dvHeadResult').hide();
		$('#dvLineResult').hide();
	}

	function rss_preview(Viewer,Feed) {
		display=document.getElementById(Viewer);
		display.innerHTML=Feed;
		display.focus();
	}

	function rss_head_list() {
		rss_clear();
		$('#dvHeadResult').show();
		var URL='./ajax/rss_head_list.php';
//		alert(URL);
		$('#tblHead_Result').empty();
//		var Params={'TypeID':parseInt(document.getElementById('TypeID').value)};
		var Params={};
		$.post(URL,Params,function(data) {
			var html='';
//			alert('Data => '+data);
			if (data.length > 0) {
				$.each(data,function(entryIndex,entry) {
					html +="<tr>";
					html +='<td>'+entry['plh_name']+'</td>';
					html +='<td>'+entry['rinh_url']+'</td>';
					html +='<td class="right" nowrap>'+entry['rinh_adate']+'</td>';
					if (entry['counter']==null) {
						html +='<td class="right">0</td>';
					} else {
						html +='<td class="right">'+parseInt(entry['counter'])+'</td>';
					}
					html += '<td class="center"><img class="btnPre" onClick="rss_head_preview(\'feedHeadPreview\','+parseInt(entry['rinh_id'])+');" title="preview"> &nbsp;';
					html += '<img class="btnEdit" onClick="rss_refresh('+entry['plrss_plh_id']+','+entry['rinh_id']+');" title="edit"> &nbsp;';
					if (entry['rinh_active']==1) {
						html += '<img class="btnDel" onClick="rss_head_active('+entry['rinh_id']+',\''+entry['rinh_name']+'\',0);" title="delete"></td>';
					} else {
						html += '<img class="btnRes" onClick="rss_head_active('+entry['rinh_id']+',\''+entry['rinh_name']+'\',1);" title="restore"></td>';
					}
					html +="</tr>";
				});
			} else {
				var html="<tr><td align='center' colspan='4'>No Record Found.</td></tr>";
			}
			$('#tblHead_Result').append(html);
		},"json");
	}
	
	function playlist_rss_add() {
//		feed_clear();
		var URL='./ajax/playlist_rss_add.php';
		var Params={};
//		alert(URL);
		$.post(URL,Params,function(data) {
			alert('data => '+dump(data));
			if (data.length > 0) {
				rss_refresh(parseInt(data[0]['HeadID']),parseInt(data[0]['RssID']));
			} else {
				alert('data => '+dump(data));
			}
		},"json");
	}
	
	function rss_head_preview(ObjName,HeadID) {
		var URL='./ajax/rss_line_list.php';
//		alert(URL);
		var Params={'HeadID':HeadID};
		var html='Example :: ';
		$.post(URL,Params,function(data) {
			if (data.length > 0) {
//				alert ('Data'+dump(data));
				html="";
				$.each(data,function(entryIndex,entry) {
					html+=" &nbsp; &nbsp; ";
					html+=entry['rinl_data']; 
				});
			} else {
				html+=" No Record Found";
			}
			rss_preview(ObjName,html);
		},"json");
	}
	
	function rss_refresh(HeadID,RssID) {
		rss_clear();
//		alert("[rss_refresh] HeadID => "+HeadID+ "; RssID => "+RssID);
		$('#dvLineResult').show();
		document.getElementById('HeadID').value=HeadID;
		document.getElementById('RssID').value=RssID;
		playlist_head_info(HeadID);
		rss_line_list(RssID);
	}
	
	function playlist_head_info(HeadID) {
		var URL='./ajax/rss_head_info.php';
		var Params={'HeadID':HeadID};
		var html='';
//		alert("[playlist_head_info] HeadID => "+HeadID);
		$.post(URL,Params,function(data) {
			if (data.length > 0) {
				document.getElementById('TypeID').value=data[0]['plh_type_id'];
				document.getElementById('HeadID').value=HeadID;
				document.getElementById('txtHeadName').value=data[0]['plh_name'];
				document.getElementById('txtOldName').value=data[0]['plh_name'];
				document.getElementById('RssID').value=data[0]['plrss_id'];
				document.getElementById('RssURL').value=data[0]['rinh_url'];
				document.getElementById('txtRSS').value=data[0]['rinh_url'];
			}
		},"json");
	}
	
	function rss_line_list(HeadID) {
		var URL='./ajax/rss_line_list.php';
		var Params={'HeadID':HeadID};
		$.post(URL,Params,function(data) {
			var html='<tr><td colspan="2" class="center"><h3>No Feed Item.</h3></td></tr>';
			if (data.length > 0) {
				html='';
				var iRun=0;
				var LastID=0;
				var NextID=0;
				document.getElementById('RssID').value=data[0]['rinl_rinh_id'];
				$.each(data,function(entryIndex,entry) {
					onView="rss_preview('feedLinePreview','"+entry['rinl_data']+"');";
					onDelete="rss_line_active("+entry['rinl_id']+",'"+entry['rinl_title']+"',0);";
					onRestore="rss_line_active("+entry['rinl_id']+",'"+entry['rinl_title']+"',1);";
					onUp="rss_line_order("+HeadID+","+LastID+","+entry['rinl_id']+");";
					Desc=entry['rinl_data'];
					html+='<tr>';
					html+='<td class="left"><b>'+entry['rinl_title']+'</b>';
					if (Desc.length > 60) {
						html+='<br><span title="'+Desc+'">'+Desc.substring(0,60)+'...</span></td>';
					} else {
						html+='<br>'+entry['rinl_data']+'</td>';
					}
        	html+='<td class="right w-15" nowrap>'+entry['rinl_edate']+'</td>';
        	html+='<td class="center" width="120">';
          html+='<div class="dvtr top5 w-100">';
          html+='<div class="dvtd_left w-25 center">';
					html+='<img class="preview" src="images/icons/preview.png" title="preview" onClick="'+onView+'"></div>';
          html+='<div class="dvtd_left w-25 center">';
					if (iRun > 0) {
						html+='<img class="preview" src="images/icons/up.png" title="up '+onUp+'" onClick="'+onUp+'">';
					} else { html+="&nbsp;"; }
					html+='</div>';
          html+='<div class="dvtd_left w-25 center">';
					if (iRun+1 < data.length) {
						NextID=data[iRun+1]['rinl_id'];
						onDown="rss_line_order("+HeadID+","+NextID+","+entry['rinl_id']+");";
						html+='<img class="preview" src="images/icons/down.png" title="down '+onDown+'" onClick="'+onDown+'">';
					} else { html+="&nbsp;"; }
					html+='</div>';
          html+='<div class="dvtd_left w-25 center">';
					if (entry['rinl_active']==1) {
						html+='<img class="preview" src="images/icons/delete.png" title="delete" onClick="'+onDelete+'">';
					} else {
						html+='<img class="preview" src="images/icons/restore.png" title="restore" onClick="'+onRestore+'">';
					}
					html+='</div>';
       		html+='</div>';
        	html+='</td>';
      		html+='</tr>';
					LastID=entry['rinl_id'];
					iRun++;
				});
			}
			$('#tblLine_Result').empty();
			$('#tblLine_Result').append(html);
		},"json");
	}
	
	function rss_line_order(HeadID,DstID,SrcID) {
		var HeadID=parseInt(document.getElementById('HeadID').value);
		var RssID=parseInt(document.getElementById('RssID').value);
		
//		alert("Head => "+HeadID+"; Dest => "+DstID+"; Src => "+SrcID);
		var URL='./ajax/rss_line_move.php';
		var Params={'RssID':RssID,'NextID':DstID,'CurrID':SrcID};
		$.post(URL,Params,function(data) {
//			alert(dump(data));
			if (parseInt(data) > 0) {
				alert("Ordering Successful.");
				rss_refresh(HeadID,RssID);					
			} else {
				alert("Ordering Failed");
			}
		});
	}

	function rss_head_save() {
		var HeadID=parseInt(document.getElementById('HeadID').value);
		var TypeID=parseInt(document.getElementById('TypeID').value);
		var RssID=parseInt(document.getElementById('RssID').value);
		var HeadName=encodeURI(document.getElementById('txtHeadName').value);
		var Params={'HeadID':HeadID,'Name':HeadName};
		var URL='./ajax/playlist_head_save.php';
		if (! ((HeadID=='') || (HeadID==undefined))) {
			$.post(URL,Params,function(data) {
//				alert (dump(data));
				if (parseInt(data) > 0) {
//					alert('Save Completed');
					rss_refresh(HeadID,RssID);					
				} else {
//					alert('Save Failed');
				}
			});
		}
	}
	
	function feed_head_active(HeadID,HeadName,Active) {
		if (! ((HeadID=='') || (HeadID==undefined))) {
			var sDisplay="Do you confirm to delete '"+HeadName+"' playlist ?";
			var answer=confirm(sDisplay);
			if (answer) {
				var Params={'HeadID':HeadID,'Active':Active};
				var URL='./ajax/playlist_head_active.php';
				$.post(URL,Params,function(data) {
//					alert (dump(data));
					if (parseInt(data) > 0) {
//						alert('Save Completed');
						feed_head_list();			
					} else {
//						alert('Save Failed');
					}
				});
			}
		}
	}
	
	function rss_line_active(LineID,Title,Active) {
		var HeadID=parseInt(document.getElementById('HeadID').value);
		var RssID=parseInt(document.getElementById('RssID').value);
		if (! ((LineID=='') || (LineID==undefined))) {
			var Mode="Delete";
			var sDisplay="Do you confirm to delete '"+Title+"' Item ?";
			var answer=confirm(sDisplay);
			if (Active==1) Mode="Restore";
			if (answer) {
				var Params={'LineID':LineID,'Active':Active};
				var URL='./ajax/rss_line_active.php';
				$.post(URL,Params,function(data) {
//					alert (dump(data));
					if (parseInt(data) > 0) {
//						alert(Mode+' Completed');
						rss_refresh(HeadID,RssID);					
					} else {
//						alert(Mode+' Failed');
					}
				});
			}
		}
	}
	
	function rss_show_url() {
		var HeadID=document.getElementById('HeadID').value;
		var RssID=document.getElementById('RssID').value;
		$('#dvUpdated').dialog('open');
	}
	
	function rss_update() {
		var HeadID=parseInt(document.getElementById('HeadID').value);
		var RssID=parseInt(document.getElementById('RssID').value);
		var RssURL=encodeURIComponent(document.getElementById('txtRSS').value);
		var KeepOld=0;
		var Expired=parseInt(document.getElementById('selExpired').value);
		if (document.getElementById('chkOldCheck').checked) KeepOld=1;
		var Params={'RssID':RssID,
			'Link':RssURL,
			'Expired':Expired,
			'KeepOld':KeepOld};
		var URL='./ajax/rss_getupdate.php';
//		alert("URL => "+ URL + "; Data \n" + dump(Params));
		if (true) {
			$.post(URL,Params,function(data) {
//				alert (dump(data));
				if (parseInt(data) > 0) {
//					alert('Save Completed');
				} else {
//					alert('Save Failed');
				}
				$('#dvUpdated').dialog('close');
				rss_refresh(HeadID,RssID);
			});
		}
	}