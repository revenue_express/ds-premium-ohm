function checkValid(frmName,txtPara,dvDisplay) {
/*
	ฟังก์ชั่น ตรวจสอบว่าข้อมูลที่ป้อนเข้ามาถูกต้องหรือไม่ หากถูกต้องจะทำการ submit 
	แต่ถ้าไมครบถ้วนก็จะแสดง Error และปรับตัวแปรที่ต้องมีค่าให้เปลี่ยนสีเป็นแบบที่ต้องการ
	ตัวแปรที่รับเข้ามาในระบบ
	frmName =  ชื่อของฟอร์มที่ตัวแปรอยู่และจะส่งข้อมูล submit
	txtPara =  รายการตัวแปรที่ต้องการตรวจสอบ โดยมีรูปแบบดังต่อไปนี้  
		Target01[[::Source01]||Type01::Val01]=ErrorMesg01[##[Target0X[[::Source0X]||Type0X::Val0X]=ErrorMesg0X]]
		
		Target0X => ตัวแปรตัวที่ X ที่ทำการตรวจสอบ
		Source0X => ตัวแปรที่จะเปลี่ยนสีเมื่อข้อมูลไม่ถูกต้อง (หากไม่มีจะเอาตัว Target0X) แทน
		Type0X => วิธีในการตรวจสอบ เช่น Length, Value
		Val0X => ค่าที่ใช้ในการทดสอบ
		ErrorMesg0X => ข้อความแจ้งเดือนเมื่อตัวแปร X ข้อมูลไม่ถูกต้อง
	dvDisplay = ส่วนที่ทำการแสดงข้อความแจ้งเตือนเมื่อข้อมูลไม่ถูกต้อง
*/
	var objForm=document.forms[frmName];	// ตัวแปรเก็บ object FORM
	var objDisplay=document.getElementById(dvDisplay); // ตัวแปรเก็บ object DISPLAY 
	var objPara=document.getElementById(txtPara);	// ตัวแปรเก็บ Object ของตัวแปรการตรวจสอบ
	var objSource,objTarget;	// ตัวแปรสำหรับเก็บ Object องตัวตรวจสอบข้อมูล และตัวแสดงแจ้งเตือน
	var iLoop=0;	// ตัวแปรสำหรับใช้ในการวนลูปเพื่อตรวจสอบข้อมูล
	var bValid=true;	// ตัวแปรสำหรับตรวจสอบว่าข้อมูลถูกต้องหรือไม่
	var sHead,sData,sValue,sResult,sSourceName,sTargetName,sCheck,sCheckType,sCheckValue;	// ตัวแปรสำหรับเก็บข้อมูล
	var iPos;	// ตัวแปรสำหรับเก็บตำแหน่งในการค้นหาต่างๆ
	var sError="";	// ตัวแปรสำหรับเก็บข้อความแจ้งเตือน
	var arrPara,arrData;	// ตัวแปรอาเรย์เพื่อชุดข้อมูลที่จะทำการตรวจสอบ
	var sData="";	// ตัวแปรเก็บค่าของตัวแปรที่ถุกตรวจสอบ
	var bAllValid=true;	// ตัวแปรสำหรับตรวจสอบว่าข้อมูลถูกต้องหรือไม่
	var sCheck,iCheck; // ตัวแปรเก็บข้อมุลเพื่อทดสอบ
	var sAlert=""; // ตัวแปรสำหรับแสดงค่า DEBUG Alert
	var bDEBUG=false; // ตัวแปรเพื่อแสดงการ DEBUG ค่า

	if (bDEBUG) { alert ( "objForm=>"+objForm); }
	if (bDEBUG) { alert ( "objPara=>"+objPara); }
	if (objForm != undefined) {
		sAlert=sAlert+"Pass Form\r\n";
// ทำการแบ่งข้อมูลตาม Format ที่กำหนด
		var arrPara=objPara.value.split("##");
// หาจำนวนข้อมูลที่รับเข้ามา
		var iMaxPara=arrPara.length;
		for (iLoop=0; iLoop<iMaxPara; iLoop++) {
			var myPara=arrPara[iLoop];
//  ตรวจสอบข้อมูลที่เข้ามาที่ตรงรูปแบบที่กำหนด
			iPos=myPara.lastIndexOf("=");
			if (iPos > -1) {
//  ตรวจสอบข้อมูลที่เข้ามาที่ตรงรูปแบบที่กำหนด
//	หาแบ่งข้อมูลส่วนหัว กับ ส่วนของค่าแสดงเมื่อไม่ถูกต้อง
				sData=myPara.substr(0,iPos);
				sResult=myPara.substr(iPos+1);
				iPos=sData.lastIndexOf("||");
				sHead=sData;
				sCheckType="LEN_MT";
				sCheckValue="0";
				if (iPos > -1) {
					sHead=sData.substr(0,iPos);
					sCheck=sData.substr(iPos+2);
					iPos=sCheck.lastIndexOf("::");
					if (iPos > -1) {
						sCheckType=sCheck.substr(0,iPos);
						sCheckValue=sCheck.substr(iPos+2);
					}
				}
//	กำหนดชื่อของตัวแปร ส่วนตรวจสอบ กับ ส่วนการแสดงผล
				sTargetName=sHead;
				sSourceName=sHead;
//  ตรวจสอบข้อมูลที่เข้ามาที่ตรงรูปแบบย่อยที่กำหนด (ตัวตรวจสอบไม่ใช่ตัวที่ที่แสดงผล)
				iPos=sHead.lastIndexOf("::");
				if (iPos > -1) {
//	กำหนดชื่อของตัวแปร ส่วนตรวจสอบ กับ ส่วนการแสดงผล
					sTargetName=sHead.substr(0,iPos);
					sSourceName=sHead.substr(iPos+2);
				}
//	ทำการหา Object ชื่อตัวแปร กับ ชื่อส่วนแสดงผล
				objSource=document.getElementById(sSourceName);
				objTarget=document.getElementById(sTargetName);
//				alert ("Source Name : "+sSourceName +"=>"+objSource +"; Target Name : "+ sTargetName+ "=> "+objTarget);
//  ตรวจสอบว่าตัวแปรที่ได้รับมาถูกต้อง
				if ((objSource != undefined) && (objTarget != undefined)) {
//				alert ("Found Double Object");
//  ตรวจสอบว่าตัวแปรที่ได้รับมาว่าเป็นประเภทไหนเพื่อตรวจสอบค่าได้ถูกต้อง
					switch (objTarget.type) {
						case "select-one":
//  กรณีเป็น Select List (เลือกได้ข้อเดียว)
							var sData=objTarget.options[objTarget.selectedIndex].value;
							break;
						case "checkbox":
//  กรณีเป็น CheckBox
							sCheckType="VAL_EQ";
							sCheckValue=false;
							var sData=objTarget.checked;
							break;
						default:
//  กรณีอื่นๆ เช่น Textbox, TextArea และอื่นๆ
							var sData=objTarget.value;
					}

//  ตรวจสอบว่าตัวแปร เพื่อทำ Validation
					bValid=false;
// DEBUG VALUE
					sAlert=sAlert+"myPara=>"+myPara;
					sAlert=sAlert+"\r\n\r\nsData=>"+sData+":: sCheckType=>"+sCheckType+":: sCheckValue=>"+sCheckValue;
					switch (sCheckType) {
						case "LEN_EQ": 
// ตรวจสอบความยาวของค่าต้องเท่ากับที่กำหนด
// iCheck เก็บค่าความยาวตัวอักษรที่ต้องการตรวจสอบ
//							sData=sData.replace(/^s+/g,' ').replace(/s+$/g,' '); // ตัดช่องว่างหน้าหลัง
							iCheck=parseInt(sCheckValue);
							if (iCheck != NaN) {
								if (sData.length == iCheck) { bValid=true;}
							}
							break;
						case "LEN_MT": 
// ตรวจสอบความยาวของค่าต้องมากกว่าที่กำหนด
// iCheck เก็บค่าความยาวตัวอักษรที่ต้องการตรวจสอบ
//							sData=sData.replace(/^s+/g,' ').replace(/s+$/g,' '); // ตัดช่องว่างหน้าหลัง
							iCheck=parseInt(sCheckValue);
							if (iCheck != NaN) {
								if (sData.length > iCheck) { bValid=true;}
							}
							break;
						case "VAL_EQ": 
// ตรวจสอบค่าต้องเท่ากับที่กำหนด
								if (sData == sCheckValue) { bValid=true;}
							break;
					}
					sAlert=sAlert+"\r\n ==> "+bValid;
					if (bDEBUG) { alert(sAlert); }
					if (bValid==false) {
//  ไม่มีค่าในตัวแปรที่ทำการตรวจสอบ
// 	เ็ซ็ตต่า bValid เป็น False เพื่อเช็คว่าไม่ถูกต้อง
//	เปลี่ยนรูปแบบของ ตัวแสดงผลเพื่อแสดงว่าข้อมูลไม่ถูกต้อง
// 	เพิ่มข้อความแจ้งว่าเกิด Error อะไรขึ้นมา
						bAllValid=false;
						sError=sError +"<BR>"+sResult;
						objSource.style.backgroundColor="#FF7F7F";
//						objSource.style.background="#FF7F7F"
//						objSource.style.borderColor="#330000";
//						objSource.className="btn_100";
					} else {
//  ตัวแปรมีค่า
//  เปลี่ยนผลการแสดง หรือกลับไปเป็นแบบเดิม
						objSource.style.backgroundColor="#FFFFFF"
//						objSource.style.borderColor="#CCCCCC";
//						objSource.style.clear="left";
					}
				}
			}
		}
	}
	
// ตรวจสอบมีการป้อนข้อมูลผิดพลาดหรือไม่ หากมีข้อผิดพลาด bValid จะเท่ากับ false
	if (bAllValid == false) {
// ตรวจสอบ object ที่จะนำข้อความแจ้งเตือนข้อผิดพลาดแสดง
		if (objDisplay != undefined) {
// แสดงข้อความแจ้งเตือนข้อผิดพลาด
			objDisplay.innerHTML=sError;
		} else {
// แสดงข้อความแจ้งเตือนข้อผิดพลาดออกทาง Alert Message ในกรณีที่ไม่สามารถหาส่วนแสดงผลได้
			alert (sError.replace(/<br>/gi,"\r\n"));
		}
		
		return false;
	} else {
// เคลียร์ข้อมูลในการส่งค่า (ลดตัวแปร txtPara ในการส่งข้อมูลให้ Server)
		objPara.value="";
// ส่งข้อมูลให้ Server
		return true;
		//objForm.submit();
	}
}

function setClassName(objName,clsName) {
	var NameObj=document.getElementById(objName);
	if ((NameObj != undefined) && (clsName != undefined) ){
		NameObj.className=clsName;
	}
}
