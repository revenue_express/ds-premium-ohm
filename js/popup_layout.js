	
	function load_headlist(schID) {
		LineID=document.getElementById('LineID').value;
		setValue('btnSave','Add');
		if (LineID > 0) {
			var URL='./ajax/schedule_layout_info.php?schID='+schID+'&LineID=';
			URL += LineID;
//			alert(URL);
			$.getJSON(URL,function(data) {
				if (data.length > 0) {
					$.each(data,function(entryIndex,entry) {
						checkboxSet('chkSun',(entry['schl_sunday']==1));
						checkboxSet('chkMon',(entry['schl_monday']==1));
						checkboxSet('chkTue',(entry['schl_tuesday']==1));
						checkboxSet('chkWed',(entry['schl_wednesday']==1));
						checkboxSet('chkThu',(entry['schl_thursday']==1));
						checkboxSet('chkFri',(entry['schl_friday']==1));
						checkboxSet('chkSat',(entry['schl_saturday']==1));
						setValue('LayoutID',entry['schl_layh_id']);
						setValue('TimeStart',entry['schl_start']);
						setValue('OldTimeStart',entry['schl_start']);
						setValue('TimeStop',entry['schl_stop']);
						setValue('OldTimeStop',entry['schl_stop']);
						setValue('btnSave','Save');
					});
				}
			});
		}

		LayoutID=document.getElementById('LayoutID').value;
		load_template(schID,LayoutID)
/*
		URL='./ajax/popup_layout_list.php?';
		URL += 'schID='+schID;
		LayoutID=document.getElementById('LayoutID').value;
//		alert(URL);
		$.getJSON(URL,function(data) {
			$('#tblLayout_body').empty();
			if (data.length > 0) {
				$.each(data, function(entryIndex,entry) {
					if (parseInt(entry['layh_active']) >0) {
						var html='<tr>';
						var onRadio="";
						onRadio+="setValue('LayoutID',"+entry['layh_id']+");";
						onRadio+="setValue('LayoutName','"+entry['layh_name']+"');";
					
						var onView="layout_popup("+entry['tmph_id']+");";
						var onSelected="";
						if (entry['layh_id']==LayoutID) {
							onSelected=" CHECKED";
							setValue('LayoutID',entry['layh_id']);
							setValue('LayoutName',entry['layh_name']);
						} else {
						}
						html += '<td><input type="radio" id="rdoCheck_'+entry['layh_id']+'" name="rdoCheck" onClick="'+onRadio+'"'+onSelected+'></td>';
						html += '<td>'+entry['layh_name']+'</td>';
						html += '<td>'+entry['tmph_name']+'</td>';
						html +='<td align="center"><img src="./images/icons/preview.png" width="16" height="16" title="view" onClick="'+onView+'"></td>';
						setValue('DispSize',entry['DispSize']);					
						$('#tblLayout_body').append(html);
					}
				});
			} else {
				var html="<tr><td align='center' colspan='3'>No Record Found.</td></tr>";
				$('#tblLayout_body').append(html);
			}
		});
*/
	}
	
	function load_Information() {
	}
	
	function load_template(schID,LayoutID) {
		URL='./ajax/popup_layout_list.php?';
		URL += 'schID='+schID;
//		alert(URL);
		$('#tblLayout_body').empty();
		var html="<tr><td align='center' colspan='3'>No Record Found.</td></tr>";
		$('#tblLayout_body').append(html);
		$.getJSON(URL,function(data) {
			if (data.length > 0) {
				$.each(data, function(entryIndex,entry) {
					if (parseInt(entry['layh_active']) >0) {
						var html='<tr>';
						var onRadio="";
						onRadio+="setValue('LayoutID',"+entry['layh_id']+");";
						onRadio+="setValue('LayoutName','"+entry['layh_name']+"');";
					
						var onView="layout_popup("+entry['tmph_id']+");";
						var onSelected="";
						if (entry['layh_id']==LayoutID) {
							onSelected=" CHECKED";
							setValue('LayoutID',entry['layh_id']);
							setValue('LayoutName',entry['layh_name']);
						} else {
						}
						html += '<td><input type="radio" id="rdoCheck_'+entry['layh_id']+'" name="rdoCheck" onClick="'+onRadio+'"'+onSelected+'>';
						html +=' <img src="./images/icons/preview.png" width="16" height="16" title="view" onClick="'+onView+'"></td>';
						html += '<td>'+entry['layh_name']+'</td>';
						html += '<td>'+entry['tmph_name']+'</td>';
						setValue('DispSize',entry['DispSize']);					
						$('#tblLayout_body').append(html);
						$('#tblLayout_body tr:first').hide();
					}
				});
			}
		});
	}
	
	function layout_popup(ID) {
		var URL="popup_gencss.php?tmph_id="+ID;
		var Feature='"location=no,menubar=no,resizeable=no,scrollbars=no,status=no,toolbar=no,width=500,height=625"';
		Feature+=(screen.height/6);
		Feature+=',left='+(screen.width/4)+');"';
//		alert(Feature);
		window.open(URL,'popup_layout',Feature);
	}
	
	
	function schedule_layout_add() {
		var URL="./ajax/schedule_layout_add.php";
		URL +="?schID="+document.getElementById('ScheduleID').value;
		URL +="&LayoutID="+document.getElementById('LayoutID').value;
		URL +="&TimeStart="+encodeURI(document.getElementById('TimeStart').value);
		URL +="&TimeStop="+encodeURI(document.getElementById('TimeStop').value);
		URL +="&DaySun="; URL+=(document.getElementById('chkSun').checked)?1:0;
		URL +="&DayMon="; URL+=(document.getElementById('chkMon').checked)?1:0;
		URL +="&DayTue="; URL+=(document.getElementById('chkTue').checked)?1:0;
		URL +="&DayWed="; URL+=(document.getElementById('chkWed').checked)?1:0;
		URL +="&DayThu="; URL+=(document.getElementById('chkThu').checked)?1:0;
		URL +="&DayFri="; URL+=(document.getElementById('chkFri').checked)?1:0;
		URL +="&DaySat="; URL+=(document.getElementById('chkSat').checked)?1:0;
//		alert(URL);
		$.getJSON(URL,function(data) {
			if (data > 0) {
//				alert('add layout information completed');
				opener.schedule_layout_list(document.getElementById('ScheduleID').value);
				window.close();
			} else {
//				alert('add layout information Failed');
			}
		});

//		schedule_check_duplicate();
	}
	
	function schedule_layout_edit() {
		var URL="./ajax/schedule_layout_edit.php";
		URL +="?schID="+document.getElementById('ScheduleID').value;
		URL +="&LineID="+document.getElementById('LineID').value;
		URL +="&LayoutID="+document.getElementById('LayoutID').value;
		URL +="&TimeStart="+encodeURI(document.getElementById('TimeStart').value);
		URL +="&TimeStop="+encodeURI(document.getElementById('TimeStop').value);
		URL +="&DaySun="; URL+=(document.getElementById('chkSun').checked)?1:0;
		URL +="&DayMon="; URL+=(document.getElementById('chkMon').checked)?1:0;
		URL +="&DayTue="; URL+=(document.getElementById('chkTue').checked)?1:0;
		URL +="&DayWed="; URL+=(document.getElementById('chkWed').checked)?1:0;
		URL +="&DayThu="; URL+=(document.getElementById('chkThu').checked)?1:0;
		URL +="&DayFri="; URL+=(document.getElementById('chkFri').checked)?1:0;
		URL +="&DaySat="; URL+=(document.getElementById('chkSat').checked)?1:0;
//		alert(URL);
		$.getJSON(URL,function(data) {
			if (data > 0) {
//				alert('Edit layout information completed');
				opener.schedule_layout_list(document.getElementById('ScheduleID').value);
				window.close();
			} else {
//				alert('Edit layout information Failed');
			}
		});
	}
	
	function schedule_check_duplicate() {
//	alert (JSON.stringify(Params));
//	alert (URL);

	var URL='./ajax/schedule_check_duplicate.php';
	var Params={'schID':(document.getElementById('ScheduleID') ===undefined)?"":document.getElementById('ScheduleID').value,
	'LineID':(document.getElementById('LineID') ===undefined)?"":document.getElementById('LineID').value,
	'Sun':(document.getElementById('chkSun') === undefined)?0:(document.getElementById('chkSun').checked)?1:0,
	'Mon':(document.getElementById('chkMon') === undefined)?0:(document.getElementById('chkMon').checked)?1:0,
	'Tue':(document.getElementById('chkTue') === undefined)?0:(document.getElementById('chkTue').checked)?1:0,
	'Wed':(document.getElementById('chkWed') === undefined)?0:(document.getElementById('chkWed').checked)?1:0,
	'Thu':(document.getElementById('chkThu') === undefined)?0:(document.getElementById('chkThu').checked)?1:0,
	'Fri':(document.getElementById('chkFri') === undefined)?0:(document.getElementById('chkFri').checked)?1:0,
	'Sat':(document.getElementById('chkSat') === undefined)?0:(document.getElementById('chkSat').checked)?1:0,
	'TStart':(document.getElementById('TimeStart') === undefined)?'00:00:01':document.getElementById('TimeStart').value,
	'TStop':(document.getElementById('TimeStop') === undefined)?'23:59:59':document.getElementById('TimeStop').value}
		$.post(URL,Params,function(data) {
			if (data == 1 ) {
// Founded Duplicate.
				alert("Your Information is Duplicate with another");				
				xret=true;
			} else {
// Not Found
				if (document.getElementById('LineID').value == '') { 
// Add Mode
					schedule_layout_add();
				} else {
// Edit Mode
					schedule_layout_edit();
				}
			}
		},"json");
	
	}
		
	function schedule_find_duplicate() {
	var URL='./ajax/schedule_check_duplicate.php';
	var Params={'schID':(document.getElementById('ScheduleID') ===undefined)?"":document.getElementById('ScheduleID').value,
	'LineID':(document.getElementById('LineID') ===undefined)?"":document.getElementById('LineID').value,
	'Sun':(document.getElementById('chkSun') === undefined)?0:(document.getElementById('chkSun').checked)?1:0,
	'Mon':(document.getElementById('chkMon') === undefined)?0:(document.getElementById('chkMon').checked)?1:0,
	'Tue':(document.getElementById('chkTue') === undefined)?0:(document.getElementById('chkTue').checked)?1:0,
	'Wed':(document.getElementById('chkWed') === undefined)?0:(document.getElementById('chkWed').checked)?1:0,
	'Thu':(document.getElementById('chkThu') === undefined)?0:(document.getElementById('chkThu').checked)?1:0,
	'Fri':(document.getElementById('chkFri') === undefined)?0:(document.getElementById('chkFri').checked)?1:0,
	'Sat':(document.getElementById('chkSat') === undefined)?0:(document.getElementById('chkSat').checked)?1:0,
	'TStart':(document.getElementById('TimeStart') === undefined)?'00:00:01':document.getElementById('TimeStart').value,
	'TStop':(document.getElementById('TimeStop') === undefined)?'23:59:59':document.getElementById('TimeStop').value}
	alert (JSON.stringify(Params));
	$.post(URL,Params,function(data) {
			alert(data);
//			alert(data+ " -> "+ data.length);
	});

	}