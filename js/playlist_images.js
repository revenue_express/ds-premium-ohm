	function image_clear() {
		console.log("image_clear");
		$('#dvHeadResult').hide();
		$('#dvLineResult').hide();
		if ($('#dvSelectFile').dialog("isOpen")) $('#dvSelectFile').dialog('close');
		console.log("image_clear: dvSelectFile : Close");
		$('#dvSelectPlaylist').dialog('close');
		console.log("image_clear: dvSelectPlaylist : Close");

		$('#dvOption').dialog('close');
		console.log("image_clear: dvOption : Close");
		$('#dvLineOption').dialog('close');
		console.log("image_clear: dvLineOption : Close");
	}

	function image_head_list() {
		image_clear();
		$('#txtSearch').val('');
		$('#dvHeadResult').show();
		var user_level=window.parent.document.getElementById('usr_level').value;
		var user_name=window.parent.document.getElementById('usr_name').value;
		var user_id=window.parent.document.getElementById('usr_id').value;
		var URL='./ajax/playlist_image_head_list.php';
//		alert(URL);
		$('#tblHead_Result').empty();
		var html="<tr><td align='center' colspan='4'>No Record Found.</td></tr>";
		$('#tblHead_Result').append(html);
		var Params={};
		$.post(URL,Params,function(data) {
			var html='';
//			alert('Data => '+data);
			if (data.length > 0) {
				$.each(data,function(entryIndex,entry) {
					onEdit="image_refresh("+entry['plh_id']+");";
					onCopy="image_clone("+entry['plh_id']+")";
					onDelete="playlist_head_active("+entry['plh_id']+",'"+entry['plh_name']+"',0);";
					onRestore="playlist_head_active("+entry['plh_id']+",'"+entry['plh_name']+"',1);";
		
					var InUsed="NO";
					if (parseInt(entry['Used'])>0) InUsed="YES";
					if (entry['plh_active']==1) {
						html +="<tr>";
						html +='<td nowrap>'+entry['plh_code']+'</td>';
						html +='<td>'+strShow(entry['plh_name'],40)+'</td>';
						html +='<td class="right">'+entry['counter']+'</td>';
						html += '<td><div style="margin-left:15%;">';
	//					html +='<img class="btnPre" onClick="feed_head_preview(\'feedHeadPreview\','+entry['plh_id']+');" title="preview"> &nbsp;';
						html += '<div class="dvtd_left w-25"><div class="btnEdit" onClick="'+onEdit+'" title="edit"> &nbsp;</div></div>';
						html += '<div class="dvtd_left w-25"><div class="btnCopy" onClick="'+onCopy+'" title="clone"></div></div>';
						if (parseInt(entry['Used'])==0) {
							html += '<div class="dvtd_left w-25"><div class="btnDel" onClick="'+onDelete+'" title="delete"></div></div>';
							html += '<div class="dvtd_left w-25"><div class="btnPurge" onClick="'+onDelete+'" title="remove"></div></div>';
						} else {
							html += '<div class="dvtd_left w-50"><div>&nbsp;</div></div>';
						}
						html +='</div></td></tr>';
					} else {
						if (user_level.toString().toLowerCase()!='operator') {
							html +="<tr>";
							html +='<td class="w-10">'+entry['plh_code']+'</td>';
							html +='<td>'+entry['plh_name']+'</td>';
							html +='<td class="right">'+entry['counter']+'</td>';
							html += '<td><div style="margin-left:15%;">';
		//					html +='<img class="btnPre" onClick="feed_head_preview(\'feedHeadPreview\','+entry['plh_id']+');" title="preview"> &nbsp;';
							html += '<div class="dvtd_left w-30"><div class="btnEdit" onClick="'+onEdit+'" title="edit"> &nbsp;</div></div>';
							html += '<div class="dvtd_left w-30"><div class="btnCopy" onClick="'+onCopy+'" title="clone"></div></div>';
							html += '<div class="dvtd_left"><div class="btnRes" onClick="'+onRestore+'" title="restore"></div></div>';
							html +='</td></tr>';
						}
					}
				});
			} else {
				var html="<tr><td align='center' colspan='4'>No Record Found.</td></tr>";
			}
			$('#tblHead_Result').append(html);
			$('#tblHead_Result tr:first').hide();
			$('tr:odd').addClass('even');
		},"json");
	}



	function image_refresh(HeadID) {
		image_clear();
		$('#dvLineResult').show();
		document.getElementById('HeadID').value=HeadID;
		image_head_info(HeadID);
		image_line_list(HeadID);
	}
	
	function image_head_info(HeadID) {
		var URL='./ajax/playlist_head_info.php';
		var Params={'HeadID':HeadID};
		var html='';
		$.post(URL,Params,function(data) {
			if (data.length > 0) {
				document.getElementById('HeadID').value=HeadID;
				document.getElementById('txtHeadName').value=data[0]['plh_name'];
				document.getElementById('txtOldName').value=data[0]['plh_name'];
			}
		},"json");
	}

	function image_line_list(HeadID,Layout) {
//		if (typeof(Layout) === 'undefined') Layout=NULL;
		var URL='./ajax/playlist_line_list.php';
		var Params={'HeadID':HeadID,'TypeID':document.getElementById('TypeID').value};
		$('#tblLine_Result').empty();
		$.post(URL,Params,function(data) {
			var html='<tr><td colspan="3" class="center"><h3>No Record Found.</h3></td></tr>';
			if (data.length > 0) {
				html='';
				var iRun=0;
				var LastID=0;
				var NextID=0;
				$.each(data,function(entryIndex,entry) {
					onView='window.open(\'./data/'+entry['type_path']+'/'+entry['file_sname']+'\',\'preview\');';
					onDelete="playlist_line_active("+entry['pll_id']+",'"+entry['file_dname']+"',0)";
					onRestore="playlist_line_active("+entry['pll_id']+",'"+entry['file_dname']+"',1)";
					onUp="playlist_line_order("+HeadID+","+LastID+","+entry['pll_id']+");";
					onOptions="$('#LineID').val("+entry['pll_id']+"); playlist_line_option('TypeID','LineID');";
					onPreview='onmouseover="alert(\'Over\')" ';
//					onPreview='onmouseover="showtrail(\'./data/'+entry['type_path']+'/'+entry['file_sname'];
//					onPreview+='\', \''+entry['file_dname']+'\',390,530)" onmouseout="hidetrail()"';

					html+='<tr>';
//					html+='<td class="left">'+entry['file_dname']+'</td>';
					var GalName=entry['file_sname'];
					var GalleryFile='./data/'+entry['type_path']+'/'+GalName.replace('.','_s.');
//					html+='<td class="left">['+entry['pll_id']+'] '+entry['file_dname']+' </td>';
					html+='<td><img src="'+GalleryFile+'" title="preview" onClick="'+onView+'"></td>';
//					html+='<td class="left">'+entry['file_dname']+' ('+strShow(entry['file_oname'],60)+')</td>';
					html+='<td class="left">'+entry['file_dname']+' </td>';
					html+='<td class="right w-10">'+entry['Size']+' </td>';
        	html+='<td class="center" width="100">';
          html+='<div class="dvtr top5 w-100">';
//          html+='<div class="dvtd_left w-20 center">';
//					html+='<img class="preview" src="images/icons/preview.png" title="preview" onClick="'+onView+'"></div>';
//					html+='<img class="preview" src="images/icons/preview.png" title="preview" onClick="'+onView+' '+onPreview+'"></div>';
          html+='<div class="dvtd_left w-20 center">';
					if ((iRun > 0) && (entry['pll_active']==1)) {
						html+='<img class="preview" src="images/icons/up.png" title="up" onClick="'+onUp+'">';
					} else { html+="&nbsp;"; }
					html+='</div>';
          html+='<div class="dvtd_left w-20 center">';
					if ((iRun+1 < data.length) &&(entry['pll_active']==1)){
						NextID=data[iRun+1]['pll_id'];
						onDown="playlist_line_order("+HeadID+","+NextID+","+entry['pll_id']+");";
						if (data[iRun+1]['pll_active']==1) {
							html+='<img class="preview" src="images/icons/down.png" title="down" onClick="'+onDown+'">';
						} else {	 html+="&nbsp;"; }
					} else { html+="&nbsp;"; }
					html+='</div>';
          html+='<div class="dvtd_left w-20 center">';
					if (entry['pll_active']==1) {
						html+='<img class="preview" src="images/icons/delete.png" title="delete" onClick="'+onDelete+'">';
					} else {
						html+='<img class="preview" src="images/icons/restore.png" title="restore" onClick="'+onRestore+'">';
					}
					html+='</div>';
//          html+='<div class="dvtd_left w-20 center">';
//					html+='<img class="preview" src="images/icons/options.png" title="options" onClick="'+onOptions+'"></div>';
					html+='</div>';
       		html+='</div>';
        	html+='</td>';
      		html+='</tr>';
					LastID=entry['pll_id'];
					iRun++;
				});
			}
			$('#tblLine_Result').empty();
			$('#tblLine_Result').append(html);
			$('tr:odd').addClass('even');
		},"json");
	}

	function playlist_line_order(HeadID,DstID,SrcID) {
//		alert("Head => "+HeadID+"; Dest => "+DstID+"; Src => "+SrcID);
		var URL='./ajax/playlist_line_move.php';
		var Params={'TypeID':document.getElementById('TypeID').value,'PlayListID':HeadID,'NextID':DstID,'CurrID':SrcID};
		$.post(URL,Params,function(data) {
			if (parseInt(data) > 0) {
//				alert("Ordering Successful.");
				image_refresh(HeadID);
			} else {
//				alert("Ordering Failed");
			}
		});
	}

	function playlist_head_add() {
		var HeadID=document.getElementById('HeadID').value;
		var HeadName=encodeURI(document.getElementById('txtHeadName').value);
		var TypeID=document.getElementById('TypeID').value;
		var Params={'HeadID':HeadID,'Name':HeadName,'TypeID':TypeID};
		var URL='./ajax/playlist_head_add.php';
		if (! ((TypeID=='') || (TypeID==undefined))) {
			$.post(URL,Params,function(data) {
				alert (dump(data));
				newHeaderID=parseInt(data);
				if (newHeaderID > 0) {
//					alert('Add Completed');
					document.getElementById('HeadID').value=newHeaderID;
					image_refresh(newHeaderID);	
				} else {
//					alert('Add Failed');
				}
			});
		} else {
//			alert('Failed :: No TypeID');
		}
	}

	function playlist_head_save() {
		var HeadID=document.getElementById('HeadID').value;
		var HeadName=encodeURI(document.getElementById('txtHeadName').value);
		var Params={'HeadID':HeadID,'Name':HeadName};
		var URL='./ajax/playlist_head_save.php';
		if (! ((HeadID=='') || (HeadID==undefined))) {
			$.post(URL,Params,function(data) {
//				alert (dump(data));
				if (parseInt(data) > 0) {
//					alert('Save Completed');
					document.getElementById('btnHeadSave').disabled=true;
					image_refresh(document.getElementById('HeadID').value);					
				} else {
//					alert('Save Failed');
				}
			});
		}
	}

	function image_preview(FileID) {
	}
	
	function image_clone(HeadID) {
		var URL='./ajax/playlist_head_add.php';
		var Params={'TypeID':document.getElementById('TypeID').value};
//		alert(URL);
		$.post(URL,Params,function(data) {
			var html='';
//			alert('data => '+dump(data));
			if (parseInt(data) > 0) {
				newHeadID=parseInt(data);
				var URL='./ajax/playlist_line_load.php';
				var Params={'SrcID':HeadID,'DstID':newHeadID,'TypeID':document.getElementById('TypeID').value}
				$.post(URL,Params,function(data) {
//			alert(dump(data));
					if (parseInt(data) > 0) {
						image_refresh(newHeadID);
					}
				});
			}
		});

	}
	
	function show_file_exists() {
		$('#dvSelectFile').dialog('open');
		var URL='./ajax/library_list.php';
//		alert(URL);
		var Params={'TypeID':document.getElementById('TypeID').value};
		var TypeName="images";
		$.post(URL,Params,function(data) {
//			alert('Data => '+data);
			var html="<tr><td align='center' colspan='4'>No record found</td></tr>";
			$('#tblSelectFile_Body').empty();
			$('#tblSelectFile_Body').append(html);
			if (data.length > 0) {
				html='';
				var chkItem="";
				$.each(data,function(entryIndex,entry) {
//					onClick='playlist_select_file('+entry['file_id']+",'"+entry['file_dname']+"');";
					onClick="playlist_check_file('tblSelectFile_Body','SelectFileID');";
					var FileName="./data/"+TypeName+"/"+entry['file_sname'];
					var onView="window.open('"+FileName+"','_view');";
					var VIcon='<image src="./images/icons/preview.png" id="imgView_'+entry['file_id']+'" class="iconList" onClick="'+onView+'" title="Preview">';
					html=""
					if (parseInt(entry['file_active'])==1) {
//						html+="\n<tr><td class=\"w-5\"><input id='rdoItem' name='rdoItem' type='radio' onClick=\""+onClick+"\"></td>";
						chkItem="chkItem_"+entry['file_id'];
						html='\n<tr><td class="w-5">';
						html+='<input id="'+chkItem+'" name="'+chkItem+'" type="checkbox" onClick="'+onClick+'" value="'+entry['file_id']+'"></td>';
						html+='<td class="w-5"><image src="./images/icons/preview.png" id="imgView_'+entry['file_id']+'" class="iconList" onClick="'+onView+'" title="Preview"></td>';
						html+="<td>"+entry['file_dname'];+"</td>";
						html+="<td>"+entry['Size'];+"</td>";
						html+="</tr>";
					}
					$('#tblSelectFile_Body').append(html);
				});
				$('#tblSelectFile_Body tr:first').hide();
			}
			$('#tblSelectFile_Body tr:odd').addClass('even');
		},"json");
	}

	function playlist_select_file(file_id,file_name) {
		document.getElementById('SelectFileID').value=file_id;		
		document.getElementById('txtExistFile').value=file_name;		
		$("#btnSelectFile").removeClass();
		document.getElementById('btnSelectFile').disabled=false;
		document.getElementById('btnSelectFile').className = 'btnButton';
	}
	
	function playlist_check_file(Head,To) {
		var val="0";
		console.log("playlist_check_file("+Head+","+To+")");
		$('#'+To).val(val);
		$('#btnSelectFile').attr('disabled','disabled');
		$("#btnSelectFile").removeClass();
		$('#'+Head+' input:checkbox:checked').each(function(i)  {
			console.log("[playlist_check_file] "+i+"==>"+$(this).val()); 
			val=val+','+$(this).val();
			$('#'+To).val(val);
			if (! $('#btnSelectFile').hasClass('btnButton')) {
				console.log("not has Class");
				$('#btnSelectFile').addClass('btnButton');
			}
			$('#btnSelectFile').removeAttr('disabled');
		});
	}
	
	
	function playlist_exist_file() {
		var URL='./ajax/playlist_line_add.php';
		var Params={'HeadID':document.getElementById('HeadID').value,
			'FileID':document.getElementById('SelectFileID').value};
		$.post(URL,Params,function(data) {
//			alert(dump(data));
			if (parseInt(data) > 0) {
				alert("Add Exists Successful.");
				image_refresh(document.getElementById('HeadID').value);
				$('#dvSelectFile').dialog('close');
			} else {
				alert("Add Exists Failed");
			}
		});
	}

	function show_playlist_exists() {
		var HeadID=document.getElementById('HeadID').value;
		$('#dvSelectPlaylist').dialog('open');
		var URL='./ajax/playlist_image_head_list.php';
//		alert(URL);
		var Params={};
		$.post(URL,Params,function(data) {
//			alert(dump(data));
			$('#tblSelectPlaylist_Body').empty();
			var html="<tr><td align='center' colspan='2'>No Found</td></tr>";
			if (data.length > 0) {
				html='';
				$.each(data,function(entryIndex,entry) {
					onClick='playlist_select_playlist('+entry['plh_id']+",'"+entry['plh_name']+"');";
					if ( (parseInt(entry['counter']) > 0) && (parseInt(entry['plh_id']) != parseInt(HeadID)) &&(parseInt(entry['plh_active'])==1)) {
						html+="\n<tr><td class=\"w-5\"><input id='rdoItem' name='rdoItem' type='radio' onClick=\""+onClick+"\"></td>";
						html+="<td>"+entry['plh_name'];+"</td>";
						html+="<td class='right'>"+entry['counter']+"</td></tr>";
					}
				});
			}
			$('#tblSelectPlaylist_Body').append(html);
		},"json");
	}

	function playlist_select_playlist(playlist_id,playlist_name) {
		document.getElementById('SelectPlaylistID').value=playlist_id;		
		document.getElementById('txtExistPlaylist').value=playlist_name;
		$("#btnSelectPlaylist").removeClass();
		document.getElementById('btnSelectPlaylist').disabled=false;
		document.getElementById('btnSelectPlaylist').className = 'btnButton';
	}

	function playlist_exist_playlist() {
		var URL='./ajax/playlist_line_load.php';
		var Params={'SrcID':document.getElementById('SelectPlaylistID').value,
			'DstID':document.getElementById('HeadID').value,
			'TypeID':document.getElementById('TypeID').value}
		$.post(URL,Params,function(data) {
//			alert(dump(data));
			if (parseInt(data) > 0) {
//				alert("Add Exists Successful.");
				image_refresh(document.getElementById('HeadID').value);
				$('#dvSelectPlaylist').dialog('close');
			} else {
//				alert("Add Exists Failed");
			}
		});
	}

	function playlist_head_active(HeadID,HeadName,Active) {
		var Params={'HeadID':HeadID,'Active':Active};
		var URL='./ajax/playlist_head_active.php';
		var Mode="Delete";
		if (Active == 1) Mode="Restore";
		if (! ((HeadID=='') || (HeadID==undefined))) {
			var Display="Do you confirm to delete this playlist?";
			var bConfirm=true;
			if (Active == 0) bConfirm=confirm(Display);
			if (bConfirm==true) {
				$.post(URL,Params,function(data) {
	//				alert (dump(data));
					if (parseInt(data) > 0) {
						alert(Mode+' Completed');
						document.getElementById('btnHeadSave').disabled=true;
						image_head_list();					
					} else {
						alert(Mode+' Failed');
					}
				});
			}
		}
	}

	function playlist_line_active(LineID,LineName,Active) {
		var Params={'LineID':LineID,'Active':Active};
		var URL='./ajax/playlist_line_active.php';
		var Mode="Delete";
		if (Active == 1) Mode="Restore";
		if (! ((LineID=='') || (LineID==undefined))) {
			var Display="Do you confirm to delete this items?";
			var bConfirm=true;
			if (Active == 0) bConfirm=confirm(Display);
			if (bConfirm==true) {
				$.post(URL,Params,function(data) {
	//				alert (dump(data));
					if (parseInt(data) > 0) {
//						alert(Mode+' Completed');
						document.getElementById('btnHeadSave').disabled=true;
						image_line_list(document.getElementById('HeadID').value);
					} else {
//						alert(Mode+' Failed');
					}
				});
			}
		}
	}