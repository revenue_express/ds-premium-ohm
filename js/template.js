	function popup_display() {
		var URL="popup_display.php?FormName=formHead&OldID="+$('#txtOldDispID').val();
		URL+="&DispID=txtNewDispID&DispName=txtSize&btnData=btnHeadSubmit";
		var Option="width=710,height=280,location=no,menubar=no,resizeable=no,scrollbars=no,status=no,toolbar=no";
		window.open(URL,'popup_display',Option);
	}

	function layout_popup(ID) {
		//alert("On function");
		var URL="popup_gencss.php?tmph_id="+ID;
	
		var Feature='"location=no,menubar=no,resizeable=no,scrollbars=no,status=no,toolbar=no,width=500,height=625"';
		Feature+=(screen.height/6);
		Feature+=',left='+(screen.width/4)+');"';
	//		alert(Feature);
		window.open(URL,'popup_layout',Feature);
	}
	
	function lm_manager() {
		var URL="layout_manager.php?TID="+$('#TemplateID').val();
	
		var Feature='"location=0,menubar=0,resizeable=no,scrollbars=no,status=no,toolbar=no,width=';
		Feature+=(screen.width-100);
		Feature+=',height='+(screen.height-100)+'"';
	//		alert(Feature);
		window.open(URL,'layout_manager',Feature);
	}
		
	function lm_clear() {
		console.log("lm_clear");
		$('#dvHeadResult').hide();
		$('#dvLineResult').hide();
		$('#dvInsert').dialog('close');
		$('#dvUpdate').dialog('close');
		$('#btnHeadSubmit').attr("disabled",true);
	}

	function lm_head_list() {
		console.log("lm_head_list");
		lm_clear();
		$('#selFile').val('');
		$('#txtSearch').val('');
		$('#dvHeadResult').show();
		var user_level=window.parent.document.getElementById('usr_level').value.toLowerCase();
		var user_name=window.parent.document.getElementById('usr_name').value;
		var user_id=window.parent.document.getElementById('usr_id').value;
		var URL="./ajax/layout_head_list.php";
		var html="";
		var Params={};
		$('#tblHead_Result').empty();
		html="<tr><th colspan='7'>No Record Found</th></tr>";
		$('#tblHead_Result').append(html);
		$.post(URL,Params,function(data) {
			if (data != null) {
				if (data.length > 0) {
					$.each(data,function(entryIndex,entry) {
						onDelete="lm_head_active("+parseInt(entry['tmph_id'])+",0);";
						onClone="lm_clone("+parseInt(entry['tmph_id'])+");";
						onRestore="lm_head_active("+parseInt(entry['tmph_id'])+",1);";
						onView="layout_popup("+parseInt(entry['tmph_id'])+");";
						onPurge="lm_purge("+parseInt(entry['tmph_id'])+");";
						onEdit="lm_update("+parseInt(entry['tmph_id']);
						onEdit+=",'"+entry['tmph_name']+"',"+parseInt(entry['disp_id']);
						onEdit+=",'"+entry['disp_name']+"',"+parseInt(entry['Used'])+");";
						html ="<tr>";
//						html +='<td class="w-60">'+entry['tmph_name']+'</td>';
						html +='<td>'+entry['tmph_name']+'<span class="right" title="'+entry['tmph_id']+'">&nbsp;</span></td>';
						html +='<td>'+entry['disp_name']+'</td>';
						html +='<td align="right">'+entry['Counter']+'</td>';
						html +='<td><div class="btnPre" onClick="'+onView+'" title="view"></div></td>';
						html +='<td class="action"><div class="btnCopy" onClick="'+onClone+'" title="Clone"></div></td>';
						html +='<td class="action"><div class="btnEdit" onClick="'+onEdit+'" title="Edit"></div></td>';

//						html +='<td class="w-10">&nbsp;</td>';
//						html +='<td class="w-10">'+onDelete+'</td>';
//						html +='<td class="w-10">'+onRestore+'</td>';
						if (parseInt(entry['tmph_active'])==1) {
							if (parseInt(entry['Used']) > 0) {
								html +='<td class="action">&nbsp;</td>';
								html +='<td class="action">&nbsp;</td>';
							} else {
								html +='<td class="action"><div class="btnDel" onClick="'+onDelete+'" title="Delete"></div></td>';
								html +='<td class="action">&nbsp;</td>';
							}
						} else {
							if (user_level=='operator') {
								html +='<td class="action">&nbsp;</td>';
								html +='<td class="action">&nbsp;</td>';
							} else {
								html +='<td class="action"><div class="btnRes" onClick="'+onRestore+'" title="Restore"></div></td>';
								html +='<td class="action"><div class="btnPurge" onClick="'+onPurge+'" title="Purge"></div></td>';
							}
						}
						html +='</tr>';
						if ((user_level == 'operator') && (parseInt(entry['tmph_active'])==0) ) {
							html ="";
						}
						$('#tblHead_Result').append(html);
					});
					$('#tblHead_Result tr:first').hide();
//					$('#tblHead').dataTable();
				}
			}
		},"json");
	}
	
	function lm_head_set(TID,TName,DID,DName,Used) {
		$('#txtName').val(TName);
		$('#txtOldName').val(TName);
		$('#txtNewName').val('');

		$('#txtSize').val(DName);
		$('#txtOldDispID').val(parseInt(DID));
		$('#txtNewDispID').val('');
		
//		$('#TemplateID').val(parseInt(TID));
		$('#InUsed').val(parseInt(Used));
//		console.log("[lm_head_set] Used "+parseInt(Used));
	}

	function lm_update(TID,TName,DID,DName,Used) {
		console.log("lm_update");
		lm_clear();
		$('#dvLineResult').show();
		$.ajaxSetup({async:false});
		lm_head_info(TID);
//		lm_head_set(TID,TName,DID,DName,Used);
		lm_line_list(TID);
		lm_cover_info(TID);
		lm_file_list('selFile');
		$.ajaxSetup({async:true});
//		console.log("[lm_update] Used "+parseInt(Used));
	}

	function lm_object_list(selOption) {
		console.log("lm_object_list");
		var user_level=window.parent.document.getElementById('usr_level').value;
		var user_name=window.parent.document.getElementById('usr_name').value;
		var user_id=window.parent.document.getElementById('usr_id').value;
		var TID=$('#TemplateID').val();
		var URL="./ajax/template_object_list.php";
		var Params={"HeadID":TID};
		$.post(URL,Params,function(data) {
			if (data != null) {
				if (data.length > 0) {
					$('#'+selOption).empty();
//					console.log("lm_object_list :: data -> "+JSON.stringify(data));
					$.each(data,function(entryIndex,entry) {
//						console.log("lm_object_list :: Item -> "+JSON.stringify(entry));
						if ( (parseInt(entry['type_active']) > 0) && (parseInt(entry['type_run']) >= 1) ){ 
							$('#'+selOption).append(new Option(entry['type_name'],entry['type_id'], true, true));
						}
					});
				}
			}
		},"json");
	}
	
	function lm_line_list(TID) {
		console.log("lm_line_list");
		var user_level=window.parent.document.getElementById('usr_level').value;
		var user_name=window.parent.document.getElementById('usr_name').value;
		var user_id=window.parent.document.getElementById('usr_id').value;
		var Used=$('#InUsed').val();
		var URL="./ajax/layout_line_list.php";
		var html="";
		var Params={"HeadID":TID};
		var arrList=new Array(1,2);
		$('#btnInsertObject').attr("disabled",false);
		$.post(URL,Params,function(data) {
			$('#tblLine_Result').empty();				
			if (data != null) {
				if (data.length > 0) {
					var objCounter=5;
					console.log("lm_line_update_select :: Return=> "+JSON.stringify(data));
					$.each(data,function(entryIndex,entry) {
						onDelete="lm_line_active("+parseInt(entry['tmpl_id'])+",0);";
						onRestore="lm_line_active("+parseInt(entry['tmpl_id'])+",1);";
						onPurge="lm_line_remove("+TID+","+parseInt(entry['tmpl_id'])+");";
						onEdit="lm_line_update("+parseInt(entry['tmpl_id'])+",'"+entry['tmpl_name']+"',"+entry['tmpl_order']+","+entry['type_id'];
						onEdit+=",'"+entry['type_name']+"',"+entry['tmpl_header']+","+entry['tmpl_footer']+","+entry['tmpl_left']+","+entry['tmpl_top'];
						onEdit+=","+entry['tmpl_width']+","+entry['tmpl_height']+");";
						html ="<tr>";
						html +='<td class="w-5 right">'+entry['tmpl_order']+' </td>';
						html +='<td class="w-10">'+entry['type_name']+'</td>';
						html +='<td class="w-40">'+entry['tmpl_name']+'</td>';

						html +='<td class="center" nowrap> ('+entry['tmpl_left']+','+entry['tmpl_top']+')</td>';
						html +='<td class="center" nowrap> [ '+entry['tmpl_width']+' X '+entry['tmpl_height']+' ] </td>';

						if ( (entry['type_id']==1) || (entry['type_id']==2)) {					
							html +='<td>';
							var header="-";
							var footer="-";
							if (parseInt(entry['tmpl_header']) ==1) header="X";
							html +='[ '+header+' ] Title ';

							if (parseInt(entry['tmpl_footer']) ==1) footer="X";
							html +='[ '+footer+' ] Description ';
							html +='</td>';
						} else {
							html +='<td>&nbsp;</td>';
						}

						html +='<td>';
//						html +='<input type="button" class="btnSky" onClick="'+onEdit+'" value="Edit" title="'+onEdit+'"></td>';
						html += '<image src="./images/icons/edit.png" id="imgEdit_'+entry['tmpl_id']+'" class="iconList" onClick="'+onEdit+'" title="Edit">';
						html +='</td>';
		
//						html +='<td class="w-10">&nbsp;</td>';
//						html +='<td class="w-10">'+onDelete+'</td>';
//						html +='<td class="w-10">'+onRestore+'</td>';
						if (parseInt(entry['tmpl_active'])==1) {
							objCounter--;
							if (parseInt(Used) > 0) {
								html +='<td class="action left">&nbsp;</td>';
							} else {
//								<input type="button" class="btnSky" onClick="'+onDelete+'" value="Delete"></td>';
								html +='<td class="action left">';
								html += '<image src="./images/icons/delete.png" id="imgDelete_'+entry['tmpl_id']+'" class="iconList" onClick="'+onDelete+'" title="Delete">';
								html +='</td>';
								html +='<td class="action left"></td>';
							}
						} else {
							if (user_level=='operator') {
								html +='<td class="w-5">&nbsp;</td>';
							} else {
								html +='<td class="action left">';
								html += '<image src="./images/icons/restore.png" id="imgRestore_'+entry['tmpl_id']+'" class="iconList" onClick="'+onRestore+'" title="Restore">';
								html +='</td>';
								html +='<td class="action left">';
								html += '<image src="./images/icons/purge.png" id="imgPurge_'+entry['tmpl_id']+'" class="iconList" onClick="'+onPurge+'" title="Remove">';
								html +='</td>';
//								html +='<td class="w-5"><input type="button" class="btnSky" onClick="'+onRestore+'" value="Restore"></td>';
							}
						}
						html +='</tr>';
//						if ((user_level == 'Operator') && (parseInt(entry['tmpl_active'])==0) ) {
						if (parseInt(entry['tmpl_active'])==0) {
							if (user_level=='operator') {
								html ="";
							}
						}
						$('#tblLine_Result').append(html);
						if ((objCounter == 0) || (parseInt(Used) > 0) ) $('#btnInsertObject').attr("disabled",true);
						
					});
					$('tr:odd').addClass('even');
				} else {
//					alert("No any layout found.");
					$('#tblLine_Result').append('<tr><td colspan="5" align="center">No Object Found.</td></tr>');
				}
			} else {
//				alert("No any layout found.");
				$('#tblLine_Result').append('<tr><td colspan="5" align="center">No Object Found.</td></tr>');
			}
		},"json");
	}
	
	function lm_head_edit() {
		var URL="./ajax/layout_head_edit.php";
		var html="";
		var Params={"HeadID":$('#TemplateID').val(),"DispID":$('#txtNewDispID').val(),"Name":$('#txtNewName').val() };
		$.post(URL,Params,function(data) {
			if (parseInt(data) ==1) {
//				alert("Update Successful.");
				lm_update($('#TemplateID').val(),$('#txtNewName').val(),$('#txtNewDispID').val(),$('#txtSize').val(),$('#InUsed').val());
			} else {
//				alert("Update Failted.");
			}
		});
	}
	
	function lm_line_update(LineID,Name,Order,TypeID,TypeName,Header,Footer,PosX,PosY,SizeW,SizeH) {
		$('#LineID').val(parseInt(LineID));
		$('#txtUpdateOrder').val(parseInt(Order));
		$('#LineTypeID').val(TypeID);
		$('#LineTypeName').val(TypeName);
		$('#txtUpdateType').val(TypeName);
		$('#txtUpdateObjectName').val(Name);
		$('#LineHeader').val(Header);
		$('#LineFooter').val(Footer);			
		$('#dvUpdate').dialog('open');
		$('#spUpdateHeader').hide();
		$('#spUpdateFooter').hide();
		if ( (TypeID == 1) || (TypeID == 2) ) {
			$('#spUpdateHeader').show();
			$('#spUpdateFooter').show();
			$('#chkUpdateHeader').attr('checked',parseInt(Header)==1);
			$('#chkUpdateFooter').attr('checked',parseInt(Footer)==1);
		}
		$('#txtUpdateLeft').val(PosX);
		$('#txtUpdateTop').val(PosY);
		$('#txtUpdateWidth').val(SizeW);
		$('#txtUpdateHeight').val(SizeH);
		$('#btnObjectUpdate').attr('disabled','disabled');
	}
	
	function lm_check_insert_object() {
		console.log('[lm_check_insert_object]');
		var sOrder=$('#txtInsertOrder').val().toString();
		var sName=$('#txtInsertObjectName').val().toString();
		$('#btnObjectInsert').attr('disabled','disabled');
		console.log('[lm_check_update_object] Order=> '+ sOrder.length +' Name=> ' +sName.length);
		if ((sOrder.length > 0) && (sName.length >= 3 ) ) {
			$('#btnObjectInsert').removeAttr('disabled');
		}
	}

	function lm_check_update_object() {
		console.log('[lm_check_update_object]');
		var sOrder=$('#txtUpdateOrder').val().toString();
		var sName=$('#txtUpdateObjectName').val().toString();
		$('#btnObjectUpdate').attr('disabled','disabled');
		console.log('[lm_check_update_object] Order=> '+ sOrder.length +' Name=> ' +sName.length);
		if ((sOrder.length > 0) && (sName.length >= 3 ) ) {
			$('#btnObjectUpdate').removeAttr('disabled');
		}
	}

	function lm_line_insert() {
		lm_object_list('selInsertObject');
		$('#LineID').val(0);
		$('#txtInsertOrder').val('');
		$('#LineTypeID').val(0);
		$('#LineTypeName').val('');
		$('#txtInsertType').val('');
		$('#txtInsertObjectName').val('');
		$('#LineHeader').val('');
		$('#LineFooter').val('');			
		$('#txtInsertLeft').val('100');
		$('#txtInsertTop').val('100');
		$('#txtInsertWidth').val('150');
		$('#txtInsertHeight').val('150');
		$('#spInsertHeader').hide();
		$('#spInsertFooter').hide();
		$('#chkInsertHeader').attr('checked',false);
		$('#chkInsertFooter').attr('checked',false);
		console.log('lm_line_insert::');
		$('#dvInsert').dialog('open');
		$('#btnObjectInsert').attr('diabled','diabled');
	}
	
	function lm_file_list(selOption) {
		var user_level=window.parent.document.getElementById('usr_level').value;
		var user_name=window.parent.document.getElementById('usr_name').value;
		var user_id=window.parent.document.getElementById('usr_id').value;
		var URL="./ajax/layout_cover_list.php";
		var Params={};
		var oOption=new Option("None","0", true, true);
		$('#'+selOption).empty();
		$('#'+selOption).append(oOption);
		$.post(URL,Params,function(data) {
			if (data != null) {
				if (data.length > 0) {
//					console.log("lm_file_list :: data -> "+JSON.stringify(data));
					$.each(data,function(entryIndex,entry) {
						var oData=new Option(entry['file_dname'],entry['file_id'], false, false);
						oData.setAttribute("data-image","./data/covers/"+entry['file_small']);
						oData.setAttribute("data-description",entry['file_oname']);
//						oData.setAttribute("data-description",entry['file_oname']+' ('+entry['file_id']+') ');
//						if ($('#CoverID').val() == entry['file_id']) 
						if ($('#CoverFileID').val() == entry['file_id']) { 
							oData.setAttribute("selected","selected");
//							console.log(selOption +' ' +$('#CoverFileID').val() + ' == ' + entry['file_id']);
						}
						$('#'+selOption).append(oData);
					});
					$('#'+selOption).msDropDown({visibleRows:4});
//					$('#'+selOption).val($('#CoverFileID').val());

				}
			}
		},"json");
	}
	
	function lm_file_select() {
		var user_level=window.parent.document.getElementById('usr_level').value;
		var user_name=window.parent.document.getElementById('usr_name').value;
		var user_id=window.parent.document.getElementById('usr_id').value;
		var TID=$('#TemplateID').val();
		var CoverID=$('#CoverID').val();		
		var CoverFileID=$('#CoverFileID').val();
		var selFileID=$('#selFile').val();
		var URL="";
		var Params={};
		var Mode="";
		if (CoverID > 0) {
			if (selFileID > 0) 	{
				URL="./ajax/layout_cover_update.php";
				console.log("[lm_file_select: update'] "+selFileID);
				Params={"LineID":CoverID,"HeadID":TID,"FileID":selFileID}
				$.post(URL,Params,function(data) {
					if (parseInt(data) > 0) {
						showAlert('dvAlert','dvAlertMsg','Change Frame Image','Change Successful.','');
//						alert("Change Layout Image Successful");
						lm_cover_info(TID);
					} else {
//						alert("Change Layout Image Failed");
					}
				});
			} else {
				console.log("[lm_file_select: delete']");
				URL="./ajax/layout_cover_delete.php";
				Params={"LineID":CoverID}
				$.post(URL,Params,function(data) {
					console.log(data);
					if (parseInt(data) > 0) {
						showAlert('dvAlert','dvAlertMsg','Change Frame Image','Change Successful.','');
//						alert("Delete Layout Image Successful");
						lm_cover_info(TID);
					} else {
//						alert("Delete Layout Image Failed");
					}
				});
			}
		} else {
			if (selFileID > 0) {
				URL="./ajax/layout_cover_add.php";
				Params={"HeadID":TID,"FileID":selFileID}
				$.post(URL,Params,function(data) {
					if (parseInt(data) > 0) {
						showAlert('dvAlert','dvAlertMsg','Change Frame Image','Change Successful.','');
//						alert("Add Layout Image Successful");
						lm_cover_info(TID);
					} else {
//						alert("Add Layout Image Failed");
					}
				});
			}
		}
		$.post(URL,Params,function(data) {
		});
	}
	
	function lm_cover_info(ID) {
		var user_level=window.parent.document.getElementById('usr_level').value;
		var user_name=window.parent.document.getElementById('usr_name').value;
		var user_id=window.parent.document.getElementById('usr_id').value;
		var URL="./ajax/layout_cover_info.php";
		var Params={"HeadID":ID};
		console.log("lm_cover_info [before]:" +$('#TemplateID').val());
		console.log("lm_cover_info "+parseInt(ID)+" [before]:" +$('#selFile').val());
		$.post(URL,Params,function(data) {
			$('#selFile').val(0);
			if (data != null) {
				console.log("lm_cover_info :: data -> "+JSON.stringify(data));
				$('#CoverID').val(parseInt(data['tmpc_id']));
				$('#CoverFileID').val(parseInt(data['file_id']));
				$('#selFile').val(parseInt(data['file_id']));
				console.log("lm_cover_info [after]:" +$('#selFile').val());
			} else {
				$('#selFile').val(0);
			}
		},"json");
	}
	
	function lm_head_info(ID) {
		var user_level=window.parent.document.getElementById('usr_level').value;
		var user_name=window.parent.document.getElementById('usr_name').value;
		var user_id=window.parent.document.getElementById('usr_id').value;
		var URL="./ajax/layout_head_info.php";
		var Params={"HeadID":parseInt(ID)};
		$.post(URL,Params,function(data) {
			if (data != null) {
				$('#TemplateID').val(parseInt(data['tmph_id']));
				$('#txtName').val(data['tmph_name']);
				$('#txtOldName').val(data['tmph_name']);
				$('#txtNewName').val(data['tmph_name']);
				$('#txtSize').val(data['disp_name']);
				$('#txtOldDispID').val(data['disp_id']);
				$('#txtNewDispID').val(data['disp_id']);
				$('#InUsed').val(data['Used']);

				$('#LineID').val('');
				$('#LineName').val('');
				$('#LineTypeID').val('');
				$('#LineTypeName').val('');
				$('#LineTName').val('');
				$('#CoverID').val('');
				$('#CoverFileID').val('');
				$('#LineHeader').val('');
				$('#LineFooter').val('');
//				$('#').val('');
			}
		},"json");
	}
	
	function lm_head_add() {
		var user_level=window.parent.document.getElementById('usr_level').value;
		var user_name=window.parent.document.getElementById('usr_name').value;
		var user_id=window.parent.document.getElementById('usr_id').value;
		var URL="./ajax/layout_head_add.php";
		var Params={"DispID":1};
		$.post(URL,Params,function(data) {
			if (parseInt(data) > 0) {
//				alert("Insert Complete");
				lm_update(data);
			} else {
//				alert("Cannot Add New Layout");
			}
		});
	}
	
	function lm_line_insert_select() {
		var user_level=window.parent.document.getElementById('usr_level').value;
		var user_name=window.parent.document.getElementById('usr_name').value;
		var user_id=window.parent.document.getElementById('usr_id').value;
		var URL="./ajax/layout_line_add.php";
		var Params={"HeadID":$('#TemplateID').val(),
				"TypeID":$('#selInsertObject').val(),
				"Name":$('#txtInsertObjectName').val(),
				"Order":$('#txtInsertOrder').val(),
				"chkHeader":$('#chkInsertHeader').attr('checked'),
				"chkFooter":$('#chkInsertFooter').attr('checked'),
//				"Left":100,
//				"Top":100,
//				"Width":150,
//				"Height":150};
				"Left":$('#txtInsertLeft').val(),
				"Top":$('#txtInsertTop').val(),
				"Width":$('#txtInsertWidth').val(),
				"Height":$('#txtInsertHeight').val()};
		console.log("lm_line_insert_select :: Params=> "+JSON.stringify(Params));
		$.post(URL,Params,function(data) {
			if (parseInt(data) > 0) {
				console.log("lm_line_insert_select :: return .... success. =>"+JSON.stringify(data));
					showAlert('dvAlert','dvAlertMsg','Add New Object','Add Successful.','');
//				alert("Insert Object Completed.");
				lm_line_list($('#TemplateID').val());
				$('#dvInsert').dialog('close');
			} else {
				console.log("lm_line_insert_select :: return .... failed.");
					showAlert('dvAlert','dvAlertMsg','Add New Object','Add Failed.','');
//				alert("Cannot Add New Layout");
			}
		});
	}
	
	function lm_line_update_select() {
		var user_level=window.parent.document.getElementById('usr_level').value;
		var user_name=window.parent.document.getElementById('usr_name').value;
		var user_id=window.parent.document.getElementById('usr_id').value;
		var URL="./ajax/layout_line_edit.php";
		var Params={"LineID":$('#LineID').val(),
				"Name":$('#txtUpdateObjectName').val(),
				"Order":$('#txtUpdateOrder').val(),
				"Left":$('#txtUpdateLeft').val(),
				"Top":$('#txtUpdateTop').val(),
				"Width":$('#txtUpdateWidth').val(),
				"Height":$('#txtUpdateHeight').val(),
				"chkHeader":$('#chkUpdateHeader').is(':checked'),
				"chkFooter":$('#chkUpdateFooter').is(':checked'),
		};
		console.log("lm_line_update_select :: URL=> "+URL);
		console.log(Params);
		$.post(URL,Params,function(data) {
			if (parseInt(data) > 0) {
				console.log("lm_line_update_select :: return .... success. =>"+JSON.stringify(data));
//				alert("Update Layout Items ... Successful");
					showAlert('dvAlert','dvAlertMsg','Update Object Information ','Update Successful.','');
				lm_line_list($('#TemplateID').val());
				$('#dvUpdate').dialog('close');
			} else {
				console.log("lm_line_update_select :: return .... failed.");
					showAlert('dvAlert','dvAlertMsg','Update Object Information ','Update Failed.','');
//				alert("Update Layout Items ... Failed");
			}
		});
	}
	
	function lm_clone(ID) {
		var user_level=window.parent.document.getElementById('usr_level').value;
		var user_name=window.parent.document.getElementById('usr_name').value;
		var user_id=window.parent.document.getElementById('usr_id').value;
		var URL="./ajax/template_clone.php";
		var Params={"HeadID":ID};
		console.log("lm_clone :: Params=> "+JSON.stringify(Params));
		$.post(URL,Params,function(data) {
			if (parseInt(data) > 0) {
				console.log("lm_clone :: return .... success. =>"+JSON.stringify(data));
//				alert("Clone layout... Successful");
					showAlert('dvAlert','dvAlertMsg','Clone Layout ','Clone Successful.','');
				lm_head_list();
			} else {
				console.log("lm_clone :: return .... failed.");
//				alert("Update Layout Items ... Failed");
					showAlert('dvAlert','dvAlertMsg','Clone Layout ','Clone Failed.','');
			}
		});
	}
	
	function lm_head_active(ID,bActive) {
		var user_level=window.parent.document.getElementById('usr_level').value;
		var user_name=window.parent.document.getElementById('usr_name').value;
		var user_id=window.parent.document.getElementById('usr_id').value;
		var URL="./ajax/layout_head_active.php";
		var Params={"HeadID":ID,"Active":bActive};
		console.log("lm_head_active :: Params=> "+JSON.stringify(Params));
		var Mode="Delete";
		if (bActive) Mode="Restore";
		$.post(URL,Params,function(data) {
			if (parseInt(data) > 0) {
				console.log("lm_head_active :: return .... success. =>"+JSON.stringify(data));
//				alert(Mode+" layout... Successful");
				lm_head_list();
			} else {
				console.log("lm_head_active :: return .... failed.");
//				alert(Mode+" layout ... Failed");
			}
		});
	}
	
	function lm_line_active(ID,bActive) {
		var user_level=window.parent.document.getElementById('usr_level').value;
		var user_name=window.parent.document.getElementById('usr_name').value;
		var user_id=window.parent.document.getElementById('usr_id').value;
		var URL="./ajax/layout_line_active.php";
		var Params={"LineID":ID,"Active":bActive};
		console.log("lm_line_active :: Params=> "+JSON.stringify(Params));
		var Mode="Delete";
		if (bActive) Mode="Restore";
		$.post(URL,Params,function(data) {
			if (parseInt(data) > 0) {
				console.log("lm_line_active :: return .... success. =>"+JSON.stringify(data));
//				alert(Mode+" layout... Successful");
				lm_line_list($('#TemplateID').val());
			} else {
				console.log("lm_line_active :: return .... failed.");
//				alert(Mode+" layout ... Failed");
			}
		});
	}

	function lm_line_remove(TID,ID) {
		var URL="./ajax/layout_line_delete.php";
		var Params={"LineID":ID};
		console.log("lm_line_active :: Params=> "+JSON.stringify(Params));
		$.post(URL,Params,function(data) {
			if (data != null) {
				if (data['Result']) {
//					alert("Delete Successful.");
					showAlert('dvAlert','dvAlertMsg','Delete Object','Delete Successful.','');
					lm_line_list(TID);
				} else {
//					alert("Delete Failed.");
					showAlert('dvAlert','dvAlertMsg','Delete Object','Delete Failed.','');
				}
			}
		},"json");
	}
	
	function lm_purge(TID) {
		alert("Purge => "+TID);
		lm_head_list();
	}

	function lm_object_select(objSelect) {
		var TypeID=0;

		TypeID=$('#'+objSelect).val();
		$('#spInsertHeader').hide();
		$('#spInsertFooter').hide();
		if ( (TypeID == 1) || (TypeID == 2) ) {
			$('#spInsertHeader').show();
			$('#spInsertFooter').show();
		}
	}
