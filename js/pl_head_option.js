	function playlist_head_option(HeadID,TypeID,DivMain,DivBody) {
		var URL='./ajax/type_option_list.php';
//		alert(URL);
		var Params={'TypeID':document.getElementById(TypeID).value};
		var Item=new Array();
		$.ajaxSetup({async:false});
		$.post(URL,Params,function(data) {
//			alert('data =>'+dump(data));
			var html='';
			$('#'+DivBody).empty();
			var ItemList=HeadID;
			var OptionName="";
//			alert(dump(data));
			if (data != null) {
				if (data.length > 0) {
					$.each(data,function(entryIndex,entry) {
	//				for (iRun=0; iRun<Item.length; iRun++) {
	//					html+="\n<tr><td>"+Item[iRun]['Name']+"</td><td>";
						html+="\n<div class='dvtr'><div class='dvtd_left w-40'>"+entry['Desc']+"</div>";
						if (entry['Type'].toLowerCase()=='option') {
							OptionName="sel_"+entry['Name'];
							html+='<div class="dvtd_right"><label><select id="'+OptionName+'" name="'+entry['Desc']+'">';
							for (iCount=0; iCount < entry['Item'].length; iCount++) {
								html+='<option value="'+entry['Item'][iCount]['Value']+'">'+entry['Item'][iCount]['Name']+'</option>';
							}
							html+='</label></select>';
						} else if (entry['Type'].toLowerCase()=='check') {
							OptionName="";
							html+="<div class='dvtd_right top5'>";
							for (iCount=0; iCount < entry['Item'].length; iCount++) {
								chkName="chk_"+entry['Name'].toLowerCase()+"_"+entry['Item'][iCount]['Name'].toLowerCase();
								var sName=entry['Desc']+" "+entry['Item'][iCount]['Name'];
								html+='<input id="'+chkName+'"type="checkbox" name="'+sName+'">'+entry['Item'][iCount]['Name'];
								if (iCount > 0) {
									OptionName+=","+chkName;
								} else {
									OptionName=chkName;
								}
							}
						} else if (entry['Type'].toLowerCase()=='css') {
								html+="<div class='dvtd_right'>";
								OptionName="css_"+entry['Name'];
								CheckName="chk_"+OptionName;
								var onChange="$('#"+CheckName+"').attr('checked',false);";
								var onClick="playlist_head_checkbox('"+CheckName+"','"+OptionName+"')";
								html +='<input id="'+OptionName+'" type="color" name="'+entry['Desc']+'" onChange="'+onChange+'" value="#FFFFFF">';	
								html +='<input id="'+CheckName+'" type="checkbox" value="'+CheckName+'" checked onClick="'+onClick+ '"> No Color';
						} else if (entry['Type'].toLowerCase()=='text') {
	//					} else if (entry['Type']=='TEXT') {
								html+="<div class='dvtd_right'>";
								OptionName="txt_"+entry['Name'];
								html +='<input class="txt" id="'+OptionName+'" size="40" name="'+entry['Desc']+'">';						
						} else if (entry['Type'].toLowerCase()=='range') {
	//					} else if (entry['Type']=='TEXT') {
								html+="<div class='dvtd_right'>";
								OptionName="range_"+entry['Name'];
								html +='<input type="range" id="'+OptionName+'" name="'+entry['Desc']+'" ';
								html +='min="'+entry['Item'][0]['Value']+'" max="'+entry['Item'][0]['Val02']+'" step="'+entry['Item'][0]['Val03']+'">';						
						} else if (entry['Type'].toLowerCase()=='int') {
								html+="<div class='dvtd_right'>";
								OptionName="int_"+entry['Name'];
								html +='<input type="number" class="txt" id="'+OptionName+'" name="'+entry['Desc']+'">';						
						}
						ItemList+=","+OptionName;
						html +='</div></div>';
	//					alert("html => "+html); 
					});
	//				alert("ItemList ->"+ItemList);
					html+="<div class='dvtr top10 center demo'>";
					html+="<input id='btnOptionSave' class='btnSky' type='button' onClick='playlist_head_option_save("+ItemList+");' value='Save'>";
					html+="</div>";
//					html+="</div>";
				} else {
					var html="<tr><td align='center'>No Layout Information</td></tr>"
				}
			} else {
					html="<tr><td align='center'>No Playlist Option.</td></tr>"
			}
			$('#'+DivBody).append(html);
//			$('#'+DivMain).dialog('open');
		},"json");
		$.ajaxSetup({async:true});

		playlist_head_option_load(document.getElementById(HeadID).value);
		
	}
	
	function playlist_head_option_save() {
//		alert("layout_playlist_option_save \n"+dump(arguments));
		P_ID=arguments[0].id;
		P_Name=arguments[0].name;
		PID=arguments[0].value;
//		alert( "P_ID =>"+P_ID+ "; P_Name => "+P_Name+"; PID=>"+PID);
//		Type=argument[1];
		$.ajaxSetup({async:false});
		var Params;
		var Result="";
		for (var iCount=1; iCount<arguments.length; iCount++ ) {
			sID=arguments[iCount].id;
			sName=arguments[iCount].name;
			sValue=arguments[iCount].value;
			var iPos=sID.indexOf("_");
			var sObjName=sID.split("_");
			
			sHead=sObjName[0].toLowerCase();
			sHead=sID.substr(0,iPos).toLowerCase();
			if (sHead=="chk") {
				sObject='Check';
			} else if (sHead=="sel") {
				sObject='Option';
			} else if (sHead=="int") {
				sObject='INT';
			} else if (sHead == "txt") {
				sObject='TEXT';
			} else if (sHead == 'css') {
				sObject='CSS';
			} else if (sHead == "range") {
				sObject='RANGE';
			}
			sVal01=sObjName[1]
			sVal02=sObjName[2]
			URL='./ajax/playlist_head_option_save.php';
			if (sHead=='chk') { 
				Params={'PlayListID':PID,
					'txtType':sObject,
					'txtCategory':sVal01,
					'txtValue':sVal02};
				if (! arguments[iCount].checked) {
					URL='./ajax/playlist_head_option_delete.php';
				}
			} else if (sHead=='css') { 
				Params={'PlayListID':PID,
					'txtType':sObject,
					'txtCategory':sVal01,
					'txtValue':sValue};
				var chkData=$('#chk_'+sID).is(':checked');
				if (chkData) {
					URL='./ajax/playlist_head_option_delete.php';
				}
			} else { 
				sVal01=sID.substr(iPos+1);
				Params={'PlayListID':PID,
					'txtType':sObject,
					'txtCategory':sVal01,
					'txtValue':sValue};
				if (sValue.length == 0) {
					URL='./ajax/playlist_head_option_delete.php';
				}
			} 
			sDisplay="\nID=>"+sID;
			sDisplay+="\nValue=>"+sValue;
			sDisplay+="\nName=>"+sName;
			sDisplay+="::=>"+sHead;
			sDisplay+="::"+sVal01;
			sDisplay+="::"+sVal02;
//			alert(sDisplay+"\nURL=>"+URL+"\n"+dump(Params));
			console.log("playlist_head_option_save:: URL=>"+URL);
			console.log(Params);
			$.post(URL,Params,function(data) {
				console.log(data);
//				alert(URL+ '\n'+dump(Params)+'\n'+dump(data));
				if (data != '0') {
//					alert('Update '+sName+' Completed.');
					Result+='\nUpdate '+sName+' Completed.';
//				} else {
//					alert('Update '+sName+' Failed.'); 
//					Result+='\nUpdate '+sName+' Failed.';
				}
			});
		}
		$.ajaxSetup({async:true});
		$('#dvOption').dialog('close');
		alert(Result)
	}
	
	function playlist_head_option_load(PlaylistID) {
	var URL='./ajax/playlist_head_option_list.php';
	var Params={ 'PlaylistID': PlaylistID};
		$.post(URL,Params,function(data) { 
			if (data.length > 0) {
//				alert("result =>"+dump(data));
				var objName='';
				$.each(data,function(entryIndex,entry) {
					console.log(entry);
					if (entry['pho_type'].toLowerCase() == 'check') {
						objName="chk_"+entry['pho_name']+"_"+entry['pho_value'].toLowerCase();
					} else if (entry['pho_type'].toLowerCase() == 'css') {
						objName="css_"+entry['pho_name'];
					} else if (entry['pho_type'].toLowerCase() == 'option') {
						objName="sel_"+entry['pho_name'];
					} else if (entry['pho_type'].toLowerCase() == 'int') {
						objName="int_"+entry['pho_name'];
					} else if (entry['pho_type'].toLowerCase() == 'text') {
						objName="txt_"+entry['pho_name'];
					} else if (entry['pho_type'].toLowerCase() == 'range') {
						objName="range_"+entry['pho_name'];
					} 
//					alert('ObjName =>'+objName);
					if (document.getElementById(objName) != undefined) {
//						alert('ObjName =>'+objName+' value=>'+document.getElementById(objName).value);
						if (entry['pho_type'].toLowerCase() == 'check') {
//							alert('ObjName =>'+objName+" => check");
							document.getElementById(objName).checked=true;
						} else if (entry['pho_type'].toLowerCase() == 'css') {
//							$('#chk_'+entry['pho_name']).attr('checked',true);
							if (entry['pho_value'] != null) {
								$('#chk_'+entry['pho_name']).attr('checked',false);
							}
							$('#'+objName).val(entry['pho_value']);
						} else {
//							alert('ObjName =>'+objName+" Old => "+document.getElementById(objName).value +" New => "+entry['pho_value']);
							document.getElementById(objName).value=entry['pho_value'];
						}
					} else {
//						alert('ObjName =>'+objName+" => No Defined.");
					}
				});
			}
		},"json");
	}
	
	function playlist_head_checkbox(checkbox,input) {
		if ($('#'+checkbox).is(':checked')) {
			$('#'+input).val('#000000');
		} else {
			$('#'+input).val('#FFFFFF');
		}
	}