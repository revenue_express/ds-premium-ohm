	function showDialog(objDialog,objID,ObjName) {
		document.getElementById('ClientID').value=objID;
		$('#'+objDialog).dialog('open');
		$('#'+objDialog).dialog({ title: "Configuration "+ObjName });
		var URL='./ajax/client_info.php';
		var Params={'ClientID':objID};
		console.log("showDialog");
		$.post(URL,Params,function(data) {
			if (data != null) {
				if (data.length > 0) {
					document.getElementById('ClientIP').value = data[0]['cli_address'];
				}
			}
		},"json");
	}
	
	function client_capture() {
		var ClientID=document.getElementById('ClientID').value;
		var FileName="./images/no-file.png";
		var URL='./ajax/player_capture.php';
		var Params={'ClientID':ClientID};
		console.log("client_capture");
		$.post(URL,Params,function(data) {
			console.log(data);
			if (parseInt(data) > 0) {
				alert("Capture Successful.");
				FileName="./data/images/"+ClientID+".png";
			} else {
				console.log("client_capture Error :: "+dump(data));
				alert("Capture Failed.");
			}
			$('#imgClient_'+ClientID,window.frames['frmDisplay']).attr("src",FileName);
		});
	}
	
	function client_mute() {
		var ServerIP=document.getElementById('ServerIP').value;
		var ClientID=document.getElementById('ClientID').value;
		var ClientIP=document.getElementById('ClientIP').value;
		var PortNo=document.getElementById('PortNo').value;
		var user_level=window.parent.document.getElementById('usr_level').value;
		var user_name=window.parent.document.getElementById('usr_name').value;
		var user_id=window.parent.document.getElementById('usr_id').value;
		var server_ip=window.parent.document.getElementById('server_ip').value;
		var server_path=window.parent.document.getElementById('server_path').value;


//		alert("Send Mute")
//		var URL='http://'+ClientIP.toString()+':'+PortNo+'/setvolumn/0/';
		var URL="http://"+server_ip+"/"+server_path+"/ajax/player_volumn.php";
		var Params={'Address':ClientIP.toString(),'volumn':0};
		console.log("client_mute");
		$.post(URL,Params,function(data) {
//			alert(dump(data));
			console.log(data);
			if (data.length > 0) {
				alert("Mute Send Successful.");
			} else {
				console.log("client_mute Error :: "+dump(data));
				alert("Mute Failed Failed.");
			}
		});
	}
	
	function client_loud() {
		var ServerIP=document.getElementById('ServerIP').value;
		var ClientID=document.getElementById('ClientID').value;
		var ClientIP=document.getElementById('ClientIP').value;
		var PortNo=document.getElementById('PortNo').value;
		var user_level=window.parent.document.getElementById('usr_level').value;
		var user_name=window.parent.document.getElementById('usr_name').value;
		var user_id=window.parent.document.getElementById('usr_id').value;
		var server_ip=window.parent.document.getElementById('server_ip').value;
		var server_path=window.parent.document.getElementById('server_path').value;
		var Loud=document.getElementById('selLoud').value;
		var Sound=parseInt(Loud)*20;
//		alert("Send set volumn "+Sound+" %.")
//		var URL='http://'+ClientIP.toString()+':'+PortNo+'/setvolumn/'+Loud+'/';
//		var Params={};
		var URL="http://"+server_ip+"/"+server_path+"/ajax/player_volumn.php";
		var Params={'Address':ClientIP.toString(),'volumn':parseInt(Loud)};
		console.log("client_loud");
		$.post(URL,Params,function(data) {
			console.log(data);
//			alert(dump(data));
			if (data.length > 0) {
				alert("Set Volumn "+Sound+" % Successful.");
			} else {
				console.log("client_lound Error :: "+dump(data));
				alert("Set Volumn "+Sound+" % Failed.");
			}
		});
	}

	function client_urgent() {
		var ServerIP=document.getElementById('ServerIP').value;
		var ClientID=document.getElementById('ClientID').value;
		var ClientIP=document.getElementById('ClientIP').value;
		var PortNo=document.getElementById('PortNo').value;
		var user_level=window.parent.document.getElementById('usr_level').value;
		var user_name=window.parent.document.getElementById('usr_name').value;
		var user_id=window.parent.document.getElementById('usr_id').value;
		var server_ip=window.parent.document.getElementById('server_ip').value;
		var server_path=window.parent.document.getElementById('server_path').value;
		var Urgent=document.getElementById('txtMessage').value;
//		alert("Send Urgent Message '"+Urgent+"'.")
//		var URL='http://'+ClientIP.toString()+':'+PortNo+'/senturgent/'+encodeURIComponent(Urgent)+'/';
		var URL="http://"+server_ip+"/"+server_path+"/ajax/player_urgent.php";
		var Params={'Address':ClientIP.toString(),'Message':encodeURIComponent(Urgent)};
		console.log("client_urgent");
		$.post(URL,Params,function(data) {
			console.log(data);
			if (data.length > 0) {
				alert("Send Urgent Message Successful.");
				document.getElementById('txtMessage').value="";
				document.getElementById('txtMessage').focus();
				document.getElementById('btnMessage').disabled=true;
			} else {
				alert("Send Urgent Message Failed.");
				console.log("client_urgent Error:: "+dump(data));
			}
		});
	}

	function client_reset_urgent() {
		var ServerIP=document.getElementById('ServerIP').value;
		var ClientID=document.getElementById('ClientID').value;
		var ClientIP=document.getElementById('ClientIP').value;
		var PortNo=document.getElementById('PortNo').value;
		var user_level=window.parent.document.getElementById('usr_level').value;
		var user_name=window.parent.document.getElementById('usr_name').value;
		var user_id=window.parent.document.getElementById('usr_id').value;
		var server_ip=window.parent.document.getElementById('server_ip').value;
		var server_path=window.parent.document.getElementById('server_path').value;
		var Urgent=document.getElementById('txtMessage').value;
//		alert("Send Urgent Message '"+Urgent+"'.")
//		var URL='http://'+ClientIP.toString()+':'+PortNo+'/senturgent/'+encodeURIComponent(Urgent)+'/';
		var URL="http://"+server_ip+"/"+server_path+"/ajax/player_reset_urgent.php";
		var Params={'Address':ClientIP.toString()};
		console.log("client_reset_urgent");
		$.post(URL,Params,function(data) {
			console.log(data);
			if (data != null) {
				alert("Send Reset Urgent Message Successful.");
			} else {
				alert("Send Reset Urgent Message Failed.");
				console.log("client_reset_urgent Error:: "+dump(data));
			}
		});
	}

	function client_restart() {
		var ServerIP=document.getElementById('ServerIP').value;
		var ClientID=document.getElementById('ClientID').value;
		var ClientIP=document.getElementById('ClientIP').value;
		var PortNo=document.getElementById('PortNo').value;
		var user_level=window.parent.document.getElementById('usr_level').value;
		var user_name=window.parent.document.getElementById('usr_name').value;
		var user_id=window.parent.document.getElementById('usr_id').value;
		var server_ip=window.parent.document.getElementById('server_ip').value;
		var server_path=window.parent.document.getElementById('server_path').value;
//		alert("Send set volumn "+Loud+"%.")
//		var URL='http://'+ClientIP.toString()+':'+PortNo+'/reboot/';
		
		var URL="http://"+server_ip+"/"+server_path+"/ajax/player_reboot.php";
		var Params={'Address':ClientIP.toString()};
		var msg2 ="Do you confirm to restart this client?";
		var msg1 ="Are you sure to restart this client?";
//		alert("Send Restart \n"+URL);
		if (confirm(msg1)) {
			if (confirm(msg2)) {
				console.log("client_restart");
				$.post(URL,Params,function(data) {
					console.log(data);
					if (data != null) {
						alert("Restart Successful.");
					} else {
						alert("Restart Failed");
						console.log("client_restart Error :: "+dump(data));
					}
				},"json");
			}
		}
	}

	function client_shutdown() {
		var ServerIP=document.getElementById('ServerIP').value;
		var ClientID=document.getElementById('ClientID').value;
		var ClientIP=document.getElementById('ClientIP').value;
		var PortNo=document.getElementById('PortNo').value;
		var user_level=window.parent.document.getElementById('usr_level').value;
		var user_name=window.parent.document.getElementById('usr_name').value;
		var user_id=window.parent.document.getElementById('usr_id').value;
		var server_ip=window.parent.document.getElementById('server_ip').value;
		var server_path=window.parent.document.getElementById('server_path').value;
//		alert("Send set volumn "+Loud+"%.")
//		var URL='http://'+ClientIP.toString()+':'+PortNo+'/reboot/';
		
		var URL="http://"+server_ip+"/"+server_path+"/ajax/player_shutdown.php";
		var Params={'Address':ClientIP.toString()};
//		alert("Send Shutdown \n"+URL);
		var msg2 ="Do you confirm to restart this client?";
		var msg1 ="Are you sure to restart this client?";
		if (confirm(msg1)) {
			if (confirm(msg2)) {
				console.log("client_shutdown");
				$.post(URL,Params,function(data) {
					console.log(data);
					if (data != null) {
						alert("Shutdown Successful.");
					} else {
						alert("Shutdown Failed");
						console.log("client_shutdown Error :: "+dump(data));
					}
				},"json");
			}
		}
	}

	function client_default() {
		var ServerIP=document.getElementById('ServerIP').value;
		var ClientID=document.getElementById('ClientID').value;
		var ClientIP=document.getElementById('ClientIP').value;
		var PortNo=document.getElementById('PortNo').value;
		var Loud=document.getElementById('selLoud').value;
//		alert("Send set default")
//		var URL='http://'+ClientIP.toString()+':'+PortNo+'/default/';
		var URL="http://"+ServerIP+"/premium/ajax/player_default.php";
		var Params={'Address':ClientIP.toString()};
		console.log("client_default");
		$.post(URL,Params,function(data) {
//			alert(dump(data));
			console.log(data);
			if (data.length > 0) {
				alert("Set Default Layout Successful.");
			} else {
				console.log("client_default Error:: "+dump(data));
				alert("Set Default Layout Failed.");
			}
		});
	}
	
	function client_restore() {
		var ServerIP=document.getElementById('ServerIP').value;
		var ClientID=document.getElementById('ClientID').value;
		var ClientIP=document.getElementById('ClientIP').value;
		var PortNo=document.getElementById('PortNo').value;
		var Loud=document.getElementById('selLoud').value;
//		alert("Send set default")
//		var URL='http://'+ClientIP.toString()+':'+PortNo+'/default/';
		var URL="http://"+ServerIP+"/premium/ajax/player_restore.php";
		var Params={'Address':ClientIP.toString()};
		console.log("client_restore");
		$.post(URL,Params,function(data) {
//			alert(dump(data));
			console.log(data);
			if (data.length > 0) {
				alert("Restore Successful.");
			} else {
				console.log("client_restore Error :: "+dump(data));
				alert("Restore Failed.");
			}
		});
	}

	function client_download() {
		var ServerIP=document.getElementById('ServerIP').value;
		var ClientID=document.getElementById('ClientID').value;
		var ClientIP=document.getElementById('ClientIP').value;
		var PortNo=document.getElementById('PortNo').value;
		var Loud=document.getElementById('selLoud').value;
//		alert("Send set default")
//		var URL='http://'+ClientIP.toString()+':'+PortNo+'/default/';
		var URL="http://"+ServerIP+"/premium/ajax/schedule_client_download.php";
		var Params={'ClientID':ClientID};
		console.log("client_download");
		$.post(URL,Params,function(data) {
			console.log(data);
			if (data.length > 0) {
				alert("Download Successful.");
			} else {
				console.log("client_download Error :: "+dump(data));
				alert("Download Failed.");
			}
		},"json");
	}

	function client_status(imgObj,ClientID,ClientIP,ClientName) {
		var ServerIP=document.getElementById('ServerIP').value;
//		var URL='http://'+ClientIP.toString()+'/';
//		var URL='http://192.168.1.239/';
		var FileName;
		var Title;
		var URL="http://"+ServerIP+"/premium/ajax/player_status.php";
		var Params={'Address':ClientIP.toString()};
//		alert("URL -> "+URL);
		console.log("client_status");
		$.post(URL,Params,function(data) {
			console.log(data);
			if (data.toString().length > 0) {
				FileName="./images/btn_online.png";
//				FileName="./images/icons/connect_success.png";
				Title="'"+ClientName+"' is online.";
//				alert("Client Online");
			} else {
		 		FileName="./images/btn_offline.png";
//				FileName="./images/icons/connect_failed.png";
				Title="'"+ClientName+"' is offline.";
//				alert("Client Offline");
			}
			$('#'+imgObj).attr("src",FileName);
			$('#'+imgObj).attr("title",Title);
/*
		})
		.error(function(data) {
			FileName="./images/btn_offline.png";
			Title="'"+ClientName+"' is offline.";
			$('#'+imgObj).attr("src",FileName);
			$('#'+imgObj).attr("title",Title);
			alert("Error:: Client Offline");
*/
		});
	}

	function client_timer() {
		var ServerIP=document.getElementById('ServerIP').value;
		var ClientID=document.getElementById('ClientID').value;
		var ClientIP=document.getElementById('ClientIP').value;
		var PortNo=document.getElementById('PortNo').value;
		var Start=document.getElementById('selStart').value;
		var Stop=document.getElementById('selStop').value;
//		alert("Send set default")
//		var URL='http://'+ClientIP.toString()+':'+PortNo+'/default/';
		var URL="http://"+ServerIP+"/premium/ajax/player_timer.php";
		var Params={'Address':ClientIP.toString(),'start':Start,'stop':Stop};
		console.log("client_timer");
		$.post(URL,Params,function(data) {
			console.log(data);
			if (data != null) {
				alert("Set Timer Successful.");
			} else {
				console.log("client_timer Error :: "+dump(data));
				alert("Try Again.");
			}
		},"json");
	}
