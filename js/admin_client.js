	function client_clear_section() {
		$('#dvClientResult').hide();
		$('#dvClientForm').hide();
	}
	
	function user_show_add() {
//		alert("Add New");
		user_clear_section();
		$('#dvUserAdd').show();

		document.getElementById('UserID').value='';

		document.getElementById('txtName').value='';

		document.getElementById('txtLogin').value='';
		document.getElementById('spAddLogin').innerHTML='';
		document.getElementById('txtPass1').value='';
		document.getElementById('txtPass2').value='';

		document.getElementById('spAddStrength').innerHTML='';
		document.getElementById('spAddSame').innerHTML='';

		document.getElementById('selLevel').value=3;

		document.getElementById('selQuest').index=1;
		document.getElementById('txtQuest').value=document.getElementById('selQuest').value;
		document.getElementById('txtAnswer').value='';

		document.getElementById('txtEmail').value='';
		document.getElementById('btnAddInfo').disabled=true;
		
		client_show_group();
	}

	function client_show_form(UserID) {
		client_clear_section();
		$('#dvClientForm').show();
		client_info(UserID);
	}



	function client_refresh() {
		client_clear_section();
		$('#dvClientResult').show();
		client_list_all();
	}

	function client_list_all() {
		$('#tblResult_Body').empty();
		URL="./ajax/client_list.php";
		var com_id=window.parent.document.getElementById('usr_com_id').value;
		Params={'ComID':com_id};
		$.post(URL,Params,function(data){
			if (data.length > 0){
				var html="";
				$.each(data,function(entryIndex,entry) {
					html='<tr>';
					html += '<td class="right">'+entry['cli_id']+'</td>';
					html += '<td class="center">'+entry['grp_name']+'</td>';
					html += '<td>'+entry['cli_name']+'</td>';
					html += '<td class="center">'+entry['cli_address']+'</td>';
//					html += '<td class=\'center\'>'+entry['status']+'</td>';
//					html += '<td><input type="button" value="Change" onClick="user_show_change('+entry['usr_id']+');"></td>';
					html += '<td class=\'center\'><input type="button" class="btnEdit" onClick="client_show_form('+entry['cli_id']+');"></td>';
					if (entry['cli_active']==1) {
						html += '<td class=\'center action\'><input title="delete" type="button" class="btnDel" value="Delete" onClick="client_delete('+entry['cli_id']+');"></td>';
					} else {
						html += '<td class=\'center action\'><input title="restore" type="button" class="btnRes" value="Restore" onClick="client_restore('+entry['cli_id']+');"></td>';
					}
					$('#tblResult_Body').append(html);
					$('tr:odd').addClass('even');
				});
			} else {
				var html='<tr><td class="center" colspan="4">No record founded.</td></tr>';
				$('#tblResult_Body').append(html);
			}
		},"json");
	}
	
	function client_info(ClientID) {
		document.getElementById('ClientID').value='';
		document.getElementById('selGroup').value='';
		
		document.getElementById('txtCode').value='';
		document.getElementById('txtName').value='';

		document.getElementById('txtDesc').value='';
		document.getElementById('txtAddress').value='';
		document.getElementById('txtURL').value='';
		document.getElementById('txtPassword').value='';

		if (ClientID != undefined) {
			URL="./ajax/client_info.php";
			var Params={'ClientID':ClientID};
			$.post(URL,Params,function(data){
//				alert("data =>"+data);
				if (data.length > 0) {
					document.getElementById('ClientID').value=data[0]['cli_id'];
					document.getElementById('selGroup').value=data[0]['cli_grp_id'];
		
					document.getElementById('txtCode').value=data[0]['cli_code'];
					document.getElementById('txtName').value=data[0]['cli_name'];

					document.getElementById('txtDesc').value=data[0]['cli_desc'];
					document.getElementById('txtAddress').value=data[0]['cli_address'];
					document.getElementById('txtURL').value=data[0]['cli_url'];
					document.getElementById('txtPassword').value=data[0]['cli_syscode'];
					
					client_show_group(parseInt(data[0]['cli_grp_id']));
	
				} else {
					alert("Can not Find User Information");
				}
			},"json");
		} else {
		client_show_group();
		}
	}

	function client_submit() {
//		alert("user_edit");
		var Params={
			'ClientID':document.getElementById('ClientID').value,
			'Code':document.getElementById('txtCode').value,
			'Name':document.getElementById('txtName').value,
			'Desc':document.getElementById('txtDesc').value,
			'GroupID':document.getElementById('selGroup').value,
			'Password':document.getElementById('txtPassword').value,
			'Address':document.getElementById('txtAddress').value,
			'URL':"http://"+document.getElementById('txtAddress').value+":3001/ctl/command",
			};
		if (document.getElementById('ClientID').value > 0) {
			URL="./ajax/client_edit.php";
			method="Edit";
		} else {
			URL="./ajax/client_add.php";
			method="Add";
		}
//		alert(URL);
		$.post(URL,Params,function(data){
//			alert("data =>"+data);
			if (data != "0") {
				alert(method+" Successful");
				client_refresh();
			} else {
				alert(method+" Failed");
			}
		});
	}
		
	function client_add_check() {
		var bReturn=true;
		var Display="";
		var rValue=true;
		rValue=($('#txtName').val().length>=3);
		bReturn=bReturn && rValue;
		rValue=($('#txtLogin').val().length>=3);
		bReturn=bReturn && rValue;
		rValue=($('#txtQuest').val().length>=3);
		bReturn=bReturn && rValue;
		rValue=($('#txtAnswer').val().length>=3);
		bReturn=bReturn && rValue;
		rValue=($('#txtEmail').val().length>=5);
		bReturn=bReturn && rValue;
//		alert(Display);
		if (bReturn) {
			document.getElementById('btnSubmitInfo').disabled = false;
		} else {
			document.getElementById('btnSubmitInfo').disabled = true;
		}
	}
	
	function client_delete(ClientID) {
//		alert("Delete "+ClientID);
		URL="./ajax/client_active.php";
		var Params={ 'ClientID':ClientID, 'Active':0 };
		var Display="Do you want to delete this client?";
		if (confirm(Display)) {
			$.post(URL,Params,function(data){
	//			alert("data =>"+data);
				if (data != "0") {
//					alert("Delete Successful");
					client_refresh();
				} else {
//					alert("Delete Failed");
				}
			});
		}
	}

	function client_restore(ClientID) {
//		alert("Restore "+ClientID);
		URL="./ajax/client_active.php";
		var Params={ 'ClientID':ClientID, 'Active':1 };
		$.post(URL,Params,function(data){
//			alert("data =>"+data);
			if (data != "0") {
//				alert("restore Successful");
				client_refresh();
			} else {
//				alert("restore Failed");
			}
		});
	}

	function client_click_pass() {
		if (document.getElementById("chkShowPass").checked) {
//			alert("checked");
			document.getElementById("txtPassword").type="text";
			document.getElementById("spShowPass").innerHTML="hide";
		} else {
//			alert("unchecked");
			document.getElementById("txtPassword").type="password";
			document.getElementById("spShowPass").innerHTML="show";
		}
	}
	
	function client_show_group(GroupID) {
//		alert("Show Group "+GroupID);
		URL="./ajax/group_list.php";
		var Params={ };
		var selItem=0;
		$.post(URL,Params,function(data){
//			alert("data =>"+data);
			if (data.length > 0) {
				selGroup=document.getElementById('selGroup');
				$('#selGroup').empty();
//				console.log("GroupID =>"+GroupID);
				$.each(data,function(entryIndex,entry) {
//					console.log(dump(entry));
					if (parseInt(entry['grp_active'])==1) {
						selGroup.options[selGroup.length]= new Option(entry['grp_name'],entry['grp_id']);
						if (GroupID != undefined) {
							if (GroupID == parseInt(entry['grp_id']))	{
								selItem=selGroup.length-1;
//								console.log("Select "+selGroup.length-1);
							}
						}
					}
				});
				selGroup.selectedIndex=selItem;
			}
		},"json");
	}
