	function clear_allsection() {
		ShowObject('dvHeadResult',false);
		ShowObject('dvLayout',false);
		ShowObject('dvSelectFile',false);
		ShowObject('dvSelectPlaylist',false);
		ShowObject('dvHeadAddForm',false);
	}
	
	function load_headlist() {
// 		var URL='./ajax/list_layout_head.php';
 		var URL='./ajax/template_head_list.php';
		var user_level=window.parent.document.getElementById('usr_level').value;
		var user_name=window.parent.document.getElementById('usr_name').value;
		var user_id=window.parent.document.getElementById('usr_id').value;
		var server_path=window.parent.document.getElementById('server_path').value;
		var com_id=window.parent.document.getElementById('usr_com_id').value;
//		alert(URL);
		var Params={"ComID":com_id};
		$.post(URL,Params,function(data) {
			clear_allsection();

			$('#btnHeadAdd').show();
			$('#spHeadSearch').show();
			$('#dvHeadResult').show();
			$('#txtSearch').val('');
			$('#showHead_Data').empty();
			var html="<tr><th colspan='5'>No Record Found</th></tr>";
			$('#showHead_Data').append(html);
			console.log(data);
			if (data.length > 0) {
				$.each(data, function(entryIndex,entry) {
//					alert('data=>\n'+dump(entry));
					var formEdit='formEdit_'+entry['layh_id'];
//					var onEdit = "document.getElementById('"+formEdit+"').submit();";
					var onEdit = 'clear_allsection();';
					onEdit += 'layout_loadmenu('+entry['layh_id']+','+entry['type_id']+');';
//					onEdit += "ShowObject('dvLayout',true,'');";
					onEdit += "setValue('HeadID',"+entry['layh_id']+");";
					onEdit += "setValue('txtLayoutName','"+entry['layh_name']+"');";
					onEdit += "setValue('txtOldLayoutName','"+entry['layh_name']+"');";
					onEdit += "setValue('txtTemplate','"+entry['tmph_name']+"');";
					onEdit += "setValue('TemplateID',"+entry['tmph_id']+");";
					onView = "layout_popup("+entry['tmph_id']+");";
					onPurge = "layout_remove("+entry['layh_id']+");";

//					var formDel='formDelete_'+entry['layh_id'];
					var onDelete = "if (confirm('Do you confirm to delete this layout?')) { layout_delete("+entry['layh_id']+");}";

//					var formRestore='formRestore_'+entry['layh_id'];
					var onRestore = "if (confirm('Do you confirm to restore this playlist?')) { layout_restore("+entry['layh_id']+"); }";

//					var formClone='formClone_'+entry['layh_id'];
					var onClone = "if (confirm('Do you confirm to clone this layout?')) { layout_clone("+entry['layh_id']+");}";
//							alert(entry['Head']);
					html = '<tr>';
					html += '<td><span title="'+entry['layh_id']+'">'+entry['layh_name'] + '</span></td>';
					html += '<td><span title="'+entry['tmph_id']+'">'+entry['tmph_name'] + '</span></td>:';

					html += '<td><image src="./images/icons/preview.png" id="imgView_'+entry['layh_id']+'" class="iconList" onClick="'+onView+'" title="Preview"></td>';
					if (parseInt(entry['layh_active'])==0) {
						html += '<td class="action"></td>';
						html += '<td class="action"></td>';
						html += '<td class="action">';
//						html +='<div style="float:left;" class="btnRes" width="16" height="16" onClick="'+onRestore+'" title="restore"></div>';
					html += '<image src="./images/icons/restore.png" id="imgRestore_'+entry['layh_id']+'" class="iconList" onClick="'+onRestore+'" title="Restore">';
						html +='</td>';
						html += '<td class="action">';
//						html += '<div style="float:left;" class="btnPurge" width="16" height="16" onClick="'+onPurge+'" title="remove"></div>';
						html += '<image src="./images/icons/purge.png" id="imgPurge_'+entry['layh_id']+'" class="iconList" onClick="'+onPurge+'" title="Remove">';
						html += '</td>';
					} else {
						html += '<td class="action">';
//						html += '<div style="float:left;" class="btnEdit" width="16" height="16" onClick="'+onEdit+'" title="edit"></div>';
						html += '<image src="./images/icons/edit.png" id="imgEdit_'+entry['layh_id']+'" class="iconList" onClick="'+onEdit+'" title="Edit">';
						html += '</td>';
						html += '<td class="action">';
//						html += '<div style="float:left;" class="btnCopy" width="16" height="16" onClick="'+onClone+'" title="copy"></div>';
						html += '<image src="./images/icons/copy.png" id="imgCopy_'+entry['layh_id']+'" class="iconList" onClick="'+onClone+'" title="Copy">';
						html += '</td>';
						if (parseInt(entry['Used'])>0) {
							html += '<td class="action"></td>';
							html += '<td class="action"></td>';
						} else {
							html += '<td class="action">';
//							html += '<div style="float:left;" class="btnDel" width="16" height="16" onClick="'+onDelete+'" title="delete"></div>	';
						 	html += '<image src="./images/icons/delete.png" id="imgDelete_'+entry['layh_id']+'" class="iconList" onClick="'+onDelete+'" title="Delete">';
							html +='</td>';
							html += '<td class="action"></td>';
						}
					}
					html += '</td>';
					var InUsed="NO";
					if (parseInt(entry['Used'])>0) InUsed="YES";
//					html +="<td class='w-10'>"+InUsed+"</td>";
//					html += '</form>';

//					html += '<form method="post" id="'+formDel+'" name="'+formDel+'">';
//    			html += '<input type="hidden" id="btnDelete" name="btnDelete" value="'+entry['layh_id']+'">';
//					html += '<td align="center" width="16">';
//					html += '</td>';
//					html += '</form>';
					html += '</tr>';

					if (user_level.toLowerCase() == 'operator') {
						if (parseInt(entry['layh_active'])==1) {
							$('#showHead_Data').append(html);
//							$('#showHead_Data tr:odd').addClass('even');
						}
					} else {
						$('#showHead_Data').append(html);
//						$('#showHead_Data tr:odd').addClass('even');
					}
					$('#showHead_Data tr:first').hide();
				});
			}
		},"json");
//		alert("Reload Finished");
	}
	
	function layout_head_info(HeadID) {
		var URL='./ajax/template_head_info.php';
		var Params={'HeadID':parseInt(HeadID)};
		$.post(URL,Params,function(data) {
			if (data != null) {
				console.log(data);
				if (data.length > 0) {
					$.each(data,function(entryIndex,entry) {

//						setValue('HeadID',parseInt(entry['layh_id']));
//						setValue('txtLayoutName',parseInt(entry['layh_name']));
//						setValue('txtOldLayoutName',parseInt(entry['layh_name']));
//						setValue('txtTemplate',parseInt(entry['tmph_name']));
//						setValue('TemplateID',parseInt(entry['tmph_id']));
//						$('#HeadID').val(entry['layh_id']);
						$('#HeadID').val(entry['tmph_id']);
						$('#txtLayoutName').val(entry['layh_name']);
						$('#txtOldLayoutName').val(entry['layh_name']);
						$('#txtTemplate').val(entry['tmph_name']);
//						$('#TemplateID').val(parseInt(entry['tmph_id']));
						$('#TemplateID').val(parseInt(entry['layh_id']));
						$('#spHeadSearch').hide();
						$('#btnHeadAdd').hide();
					});
				} else {
					console.log('Length = 0 ');
				}
			} else {
				console.log('Error Loading Information');
			}
		},"json");
	}

	function layout_clickmenu(PlayListID,TypeID) {
		$('#LineData').show();
		$('#dvHeaderFeedShow').hide();
		$('#dvLineFeedShow').hide();
		$('#spBtnFeedShow').hide();
		$('#HeadID').value = PlayListID;
		$('#PlayListID').value = PlayListID;
		var TypeName='';
		var TypeLibrary='';
		var TypePlaylist='';
		var URL='./ajax/type_info.php';
		var Params={'TypeID':parseInt(TypeID)};
		$.post(URL,Params,function(data) {
			if (data != null) {
				if (data.length > 0) {
					TypeName=data[0]['type_name'];
					TypeLibrary=TypeName+' Library';
					TypePlaylist=TypeName+' Playlist';
					$('.spTypeName').html(TypeName);
					$('#btnHeaderLibrary').val(TypeLibrary);
					$('#btnHeaderPlaylist').val(TypePlaylist);

					switch (parseInt(TypeID)) {
						case 1,2,4,9,10,12:
							break;
						case 3:
							break;
						case 5:
							$('#dvHeaderFeedShow').show();
							$('#dvLineFeedShow').show();
							$('#spBtnFeedShow').show();
							break;
					}
					playlist_refresh(PlayListID);
				} else {
					alert('Error Loading Information');
				}
			} else {
				alert('Error Loading Information');
			}
		},"json");
	}

	function layout_loadmenu(HeadID) {
//		var URL='./ajax/list_layout_line.php?HeadID='+HeadID;
		var URL='./ajax/template_line_list.php';
//		alert(URL);
		clear_allsection();
		console.log('layout_loadmenu('+HeadID+')');
		layout_head_info(HeadID);
		var Params={"HeadID":HeadID};
		$.post(URL,Params,function(data) {
            //alert(data); //uncomment this for debug
            //alert (data.item1+" "+data.item2+" "+data.item3); //further debug
			$('#dvLayout').show();
			$('#LineMenu').empty();
			$('#LineData').hide();
			$('#LineData_Body').empty();
			if (data != null) {
				if (data.length > 0) {
/*
					html="";
					$.each(data, function(entryIndex,entry) {
						onClick="layout_clickmenu("+entry['layl_plh_id']+","+entry['tmpl_type_id']+");";
						html +='<li><a href="#LineData" onClick="'+onClick+'">'+entry['tmpl_name']+'</a></li>';
					});
					$('#LineMenu').append(html);
*/
					var html='<div class="header"><h3><span>Object Layer List :: </span><span class="demo">';
					$.each(data, function(entryIndex,entry) {
						onClick='';
	//					onClick="ShowObject('dvSelectFile',false,'');";
	//					onClick+="ShowObject('dvSelectPlaylist',false,'');";
						onClick+="layout_clickmenu("+entry['layl_plh_id']+","+entry['tmpl_type_id']+");";
						html +='<input class="btnJQR" type="button" onClick="'+onClick+'" value="'+entry['tmpl_name']+'"> &nbsp;  &nbsp;';
					});
					html += '</span></h3></div></div>';
					$('#LineMenu').append(html);
					//$( "input:submit, input:button,  input:reset, a, button", ".demo" ).button();
					
				} else {
					var html="<span>No Layout Information</span>";
					$('#LineMenu').append(html);
				}
			}
		},"json");
	}

	function layout_load_library(TypeID) {
//		var URL='./ajax/list_library.php?TypeID='+TypeID;
		var URL='./ajax/library_list.php';
//		alert(URL);
		var Params={"TypeID":TypeID};
		$.post(URL,Params,function(data) {
			if (data.length > 0) {
				$('#tblSelectFile_Body').empty();
				$.each(data, function(entryIdx,entry) {
					var html='<tr>';
					var onClick="setValue('FileName','"+entry['file_dname']+"','');";
					onClick+="setValue('FileID','"+entry['file_id']+"','');";
					onClick+="EnableObject('btnSelectFile',true,'');";
					if (entry['file_path'] =='videos') {
						var fileName="";
						var onView="";
					} else {
						var fileName='./data/'+entry['file_path']+'/'+entry['file_sname'];
						var onView='window.open("'+fileName+'","_preview");';
					}
					html += '<td nowrap><input type="radio" id="rdoFile" name="rdoFile" onClick="'+onClick+'">';
					html +='<img src="./images/icons/preview.png" width="16" height="16" title="view" onClick=\''+onView+'\'></td>';
					html += '<td>'+entry['file_dname']+'</td>';
					html +='<td class="tdFileSize">' + entry['file_size'].replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,")+ '</td>';
					html += '</tr>';
					$('#tblSelectFile_Body').append(html);
					$('tr:odd').addClass('even');
				});
			}
		},"json");
	}
	
	function layout_playlist_addfile(PlayListID,TypeID,FileID) {
//		var URL='./ajax/add_playlist_line.php?HeadID='+PlayListID+'&FileID='+FileID;
		var URL='./ajax/playlist_line_add.php';
//		alert(URL);
		var Params={"HeadID":PlaylistID,"FileID":FileID};
		$.post(URL,Params,function(data) {
			if (data >0) {
//				alert('Insert Completed.');
				EnableObject('btnSelectFile',false,'');
				$('#dvSelectFile').dialog('close');
				layout_clickmenu(PlayListID,TypeID);
			} else {
//				alert('Insert Failed.');
			} 
		});
	}
	
	function layout_playlist_order(PlayListID,TypeID,CurrID,NextID) {
//		var URL='./ajax/move_playlist_line.php?PlayListID='+PlayListID+'&TypeID='+TypeID+'&CurrID='+LineCurrID+'&NextID='+LineNextID;
//		var URL='./ajax/playlist_line_move.php?PlayListID='+PlayListID+'&TypeID='+TypeID+'&CurrID='+CurrID+'&NextID='+NextID;
//		alert(URL);
		var URL='./ajax/playlist_line_move.php';
		var Params={"PlayListID":PlaylistID,"TypeID":TypeID,"CurrID":LineCurrID,"NextID":LineNextID};
		$.post(URL,Params,function(data) {
			if (data >0) {
//				alert('Move Completed.');
				layout_clickmenu(PlayListID,TypeID);
			} else {
//				alert('Move Failed.');
			} 
			});
	}
	
	function layout_playlist_delete(PlayListID,TypeID,LineID) {
//		var URL='./ajax/delete_playlist_line.php?LineID='+LineID;
		var URL='./ajax/playlist_line_delete.php?LineID='+LineID;
//		alert(URL);
		var URL='./ajax/playlist_line_delete.php';
		var Params={"LineID":LineID};
		$.post(URL,Params,function(data) {
			if (data > 0) {
//				alert('Delete Completed.');
				layout_clickmenu(PlayListID,TypeID);
			} else {
//				alert('Delete Failed.');
			} 
			});
	}
	
	function layout_playlist_show(PlayListID,TypeID) {
//		var URL='./ajax/layout_playlist_show.php?TypeID='+TypeID;
//		var URL='./ajax/template_playlist_list.php?TypeID='+TypeID;
//		alert(URL);
		var URL='./ajax/template_playlist_list.php';
		var Params={"TypeID":TypeID};
		$.post(URL,Params,function(data) {
			if (data.length > 0) {
				$('#tblSelectPlaylist_Body').empty();
				$.each(data, function(entryIdx,entry) {
					var html='<tr>';
					var onRadio="setValue('PlayListName','"+entry['plh_name']+"','');";
					onRadio+="setValue('SelectPlayListID','"+entry['plh_id']+"','');";
					onRadio+="EnableObject('btnSelectPlaylist',true,'');";
					html += '<td><input id="rdoPlayList" name="rdoPlayList" type="radio" onClick="'+onRadio+'"></td>';
					html += '<td>'+entry['plh_name']+'</td>';
					html += '<td>'+entry['counter']+'</td>';
					html += '<td class="tdFileSize">'+entry['summary'].replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,")+'</td>';
					html+="</tr>";
					$('#tblSelectPlaylist_Body').append(html);
				});
				$('#dvSelectPlaylist').dialog('open');
			} 	else {
				var html="<tr><td align='center'>No Layout Information</td></tr>"
				$('#tblSelectPlaylist_Body').append(html);
			}
		},"json");
	}
	
	function layout_playlist_add(PlayListID,TypeID,LineID) {
//		var URL='./ajax/layout_playlist_add.php?HeadID='+PlayListID+'&LineID='+LineID;
//		var URL='./ajax/template_playlist_add.php?HeadID='+PlayListID+'&LineID='+LineID;
//		alert(URL);
		var URL='./ajax/template_playlist_add.php';
		var Params={"HeadID":PlayListID,"LineID":LineID};
		$.post(URL,Params,function(data) {
			if (data > 0) {
//				alert('Load Playlist Completed.');
				$('#dvSelectPlaylist').hide();
				layout_clickmenu(PlayListID,TypeID);
			} else {
//				alert('Load Playlist Failed.');
			} 
		});
	}
	
	function layout_add(LayoutName,TemplateID,TemplateName) {
//		var URL='./ajax/layout_add.php?LName='+encodeURI(LayoutName)+'&TID='+TemplateID;
//		var URL='./ajax/template_add.php?LName='+encodeURI(LayoutName)+'&TID='+TemplateID;
//		alert(URL);
		var URL='./ajax/template_add.php';
		var Params={"LName":encodeURI(LayoutName),"TID":TemplateID};
		$.post(URL,Params,function(data) {
//			alert('return value=>'+data);
			if (data > 0) {
//				alert('Add Template Completed.');
				$('#dvHeadAddForm').hide();
				clear_allsection();
				layout_loadmenu(data);
				setValue('HeadID',data);
				setValue('txtLayoutName',LayoutName);
				setValue('txtTemplate',TemplateName);
			} else {
//				alert('Add Template Failed.');
			} 
		},"json");
	}

	function layout_head_edit(HeadID,LayoutName) {
//		var URL='./ajax/layout_head_edit.php?HeadID='+HeadID+'&LName='+encodeURI(LayoutName);
//		var URL='./ajax/template_head_edit.php?HeadID='+HeadID+'&LName='+encodeURI(LayoutName);
//		alert(URL);
		var URL='./ajax/template_head_edit.php';
		var Params={"LName":encodeURI(LayoutName),"HeadID":HeadID};
		$.post(URL,Params,function(data) {
			if (data > 0) {
//				alert('Edit Template Completed.');
				clear_allsection();
				layout_loadmenu(HeadID);
				ShowObject('dvLayout',true,'');
				setValue('HeadID',+HeadID);
				setValue('txtLayoutName',LayoutName);
				EnableObject('btnSaveLayout',false);
			} else {
//				alert('Edit Template Failed.');
			} 
		});
	}
	
	function layout_delete(HeadID) {
//		var URL='./ajax/layout_active.php?HeadID='+HeadID+'&Active=0';
//		var URL='./ajax/template_active.php?HeadID='+HeadID+'&Active=0';
	//	alert(URL);
		var URL='./ajax/template_active.php';
		var Params={"Active":0,"HeadID":HeadID};
		$.post(URL,Params,function(data) {
			if (data > 0) {
//				alert('Delete Template Completed.');
				clear_allsection();
				load_headlist();
			} else {
//				alert('Delete Template Failed.');
			} 
		});
		load_headlist();
	}
	
	function layout_restore(HeadID) {
//		var URL='./ajax/layout_active.php?HeadID='+HeadID+'&Active=1';
//		var URL='./ajax/template_active.php?HeadID='+HeadID+'&Active=1';
//		alert(URL);
		var URL='./ajax/template_active.php';
		var Params={"Active":1,"HeadID":HeadID};
		$.post(URL,Params,function(data) {
			if (data > 0) {
//				alert('Restore Completed.');
				clear_allsection();
				load_headlist();
			} else {
//				alert('Restore Failed.');
			} 
		});
		load_headlist();
	}
	
	function layout_playlist_edit(PlayListID,TypeID,NewName) {
//		var URL='./ajax/layout_playlist_edit.php?HeadID='+PlayListID+'&NewName='+encodeURI(NewName);
//		var URL='./ajax/template_playlist_edit.php?HeadID='+PlayListID+'&NewName='+encodeURI(NewName);
//		alert(URL);
		var URL='./ajax/template_playlist_edit.php';
		var Params={"HeadID":PlayListID,"NewName":encodeURI(NewName)};
		$.post(URL,Params,function(data) {
			if (data > 0) {
//				alert('Update Playlist Completed.');
			} else {
//				alert('Update Playlist Failed.');
			} 
		});
		layout_clickmenu(PlayListID,TypeID);
	}
	
	function layout_clone(HeadID) {
//		var URL='./ajax/layout_clone.php?LayoutID='+HeadID;
		var URL='./ajax/template_copy.php';
//		var URL='./ajax/layout_clone.php';
//		var TypeID=document.getElementById('TypeID').value;
		var Params={'LayoutID':HeadID};
//		alert(URL);
		$.post(URL,Params,function(data) {
//			alert(data);
			if (parseInt(data) > 0) {
//				alert('Clone Completed.');
				clear_allsection();
				layout_loadmenu(parseInt(data));
//					onEdit += "ShowObject('dvLayout',true,'');
//				setValue('HeadID',"+entry['layh_id']+");
//				setValue('txtLayoutName','"+entry['layh_name']+"');
//				setValue('txtOldLayoutName','"+entry['layh_name']+"');
//				setValue('txtTemplate','"+entry['tmph_name']+"');
//				setValue('TemplateID',"+entry['tmph_id']+");
			} else {
//				alert('Clone Failed.');
			} 
			});
	}

	function layout_playlist_option(HeadID,TypeID) {
		var URL='./ajax/type_option_list.php';
//		alert(URL);
		var Params={'TypeID':$('#'+TypeID).val()};
		var Item=new Array();
		$.ajaxSetup({async:false});
		$.post(URL,Params,function(data) {
//			alert('data =>'+dump(data));
			var html='';
			$('#tblOption_Body').empty();
			var ItemList=HeadID;
			var OptionName="";
			if (data.length > 0) {
				$.each(data,function(entryIndex,entry) {
//				for (iRun=0; iRun<Item.length; iRun++) {
//					html+="\n<tr><td>"+Item[iRun]['Name']+"</td><td>";
					html+="\n<tr><td>"+entry['Desc']+"</td><td>";
					if (entry['Type'].toLowerCase()=='option') {
						OptionName="sel_"+entry['Name'];
						html+='<select id="'+OptionName+'" name="'+entry['Desc']+'">';
						for (iCount=0; iCount < entry['Item'].length; iCount++) {
							html+='<option value="'+entry['Item'][iCount]['Value'].toLowerCase()+'">'+entry['Item'][iCount]['Name']+'</option>';
						}
						html+='</select>';
					} else if (entry['Type'].toLowerCase()=='check') {
						OptionName="";
						for (iCount=0; iCount < entry['Item'].length; iCount++) {
							chkName="chk_"+entry['Name'].toLowerCase()+"_"+entry['Item'][iCount]['Name'].toLowerCase();
							var sName=entry['Desc']+" "+entry['Item'][iCount]['Name'];
							html+='<input id="'+chkName+'"type="checkbox" name="'+sName+'">'+entry['Item'][iCount]['Name'];
							OptionName+=","+chkName;
						}
					} else if (entry['Type'].toLowerCase()=='css') {
							OptionName="css_"+entry['Name'];
							html +='<input id="'+OptionName+'" class="color {hash:true,caps:false}" name="'+entry['Desc']+'">';						
					} else if (entry['Type'].toLowerCase()=='text') {
//					} else if (entry['Type']=='TEXT') {
							OptionName="txt_"+entry['Name'];
							html +='<input id="'+OptionName+'" size="40" name="'+entry['Desc']+'">';						
					} else if (entry['Type'].toLowerCase()=='int') {
							OptionName="int_"+entry['Name'];
							html +='<input id="'+OptionName+'" size="7" onKeyPress="return OnlyNumbers();" name="'+entry['Desc']+'">';						
					}
					ItemList+=","+OptionName;
					html +='</td></tr>';
//					alert("html => "+html); 
				});
//				alert("ItemList ->"+ItemList);
				html+="<tr><td align='right' colspan='2'>";
				html+="<input id='btnOptionSave' type='button' onClick='layout_playlist_option_save("+ItemList+");' value='Save'>";
				html+="</td></tr>";
			} else {
				var html="<tr><td align='center'>No Layout Information</td></tr>"
			}
			$('#tblOption_Body').append(html);
			$('#dvOption').dialog('open');
		},"json");
		$.ajaxSetup({async:true});

		layout_playlist_option_load($('#'+HeadID).val());
		
	}
	
	function layout_playlist_option_save() {
//		alert("layout_playlist_option_save \n"+dump(arguments));
		P_ID=arguments[0].id;
		P_Name=arguments[0].name;
		PID=arguments[0].value;
//		alert( "P_ID =>"+P_ID+ "; P_Name => "+P_Name+"; PID=>"+PID);
//		Type=argument[1];
		$.ajaxSetup({async:false});
		var Params;
		for (var iCount=1; iCount<arguments.length; iCount++ ) {
			sID=arguments[iCount].id;
			sName=arguments[iCount].name;
			sValue=arguments[iCount].value;
			var sObjName=sID.split("_");
			sHead=sObjName[0].toLowerCase();
			if (sHead=="chk") {
				sObject='Check';
			} else if (sHead=="sel") {
				sObject='Option';
			} else if (sHead=="int") {
				sObject='INT';
			} else if (sHead == "txt") {
				sObject='TEXT';
			} else if (sHead == 'css') {
				sObject='CSS';
			}
			sVal01=sObjName[1]
			sVal02=sObjName[2]
			URL='./ajax/playlist_head_option_save.php';
			if (sHead=='chk') { 
				Params={'PlayListID':PID,
					'txtType':sObject,
					'txtCategory':sVal01,
					'txtValue':sVal02};
				if (! arguments[iCount].checked) {
					URL='./ajax/playlist_head_option_delete.php';
				}
			} else { 
				Params={'PlayListID':PID,
					'txtType':sObject,
					'txtCategory':sVal01,
					'txtValue':sValue};
				if (sValue.length == 0) {
					URL='./ajax/playlist_head_option_delete.php';
				}
			} 
			sDisplay="\nID=>"+sID;
			sDisplay+="\nValue=>"+sValue;
			sDisplay+="\nName=>"+sName;
			sDisplay+="::=>"+sHead;
			sDisplay+="::"+sVal01;
			sDisplay+="::"+sVal02;
//			alert(sDisplay+"\nURL=>"+URL+"\n"+dump(Params));
			$.post(URL,Params,function(data) {
//				alert(URL+ '\n'+dump(Params)+'\n'+data);
				if (data != '0') {
					alert('Update '+sName+' Completed.'); 
				} else {
					alert('Update '+sName+' Failed.'); 
				}
			});
		}
		$.ajaxSetup({async:true});
	}
	
	function layout_playlist_option_load(PlaylistID) {
	var URL='./ajax/playlist_head_option_list.php';
	var Params={ 'PlaylistID': PlaylistID};
		$.post(URL,Params,function(data) { 
			if (data.length > 0) {
//				alert("result =>"+dump(data));
				var objName='';
				$.each(data,function(entryIndex,entry) {
					if (entry['pho_type'].toLowerCase() == 'check') {
						objName="chk_"+entry['pho_name']+"_"+entry['pho_value'].toLowerCase();
					} else if (entry['pho_type'].toLowerCase() == 'css') {
						objName="css_"+entry['pho_name'];
					} else if (entry['pho_type'].toLowerCase() == 'option') {
						objName="sel_"+entry['pho_name'];
					} else if (entry['pho_type'].toLowerCase() == 'int') {
						objName="int_"+entry['pho_name'];
					} else if (entry['pho_type'].toLowerCase() == 'text') {
						objName="txt_"+entry['pho_name'];
					}
//					alert('ObjName =>'+objName);
					if (document.getElementById(objName) != undefined) {
//						alert('ObjName =>'+objName+' value=>'+document.getElementById(objName).value);
						if (entry['pho_type'].toLowerCase() == 'check') {
//							alert('ObjName =>'+objName+" => check");
							document.getElementById(objName).checked=true;
						} else {
//							alert('ObjName =>'+objName+" Old => "+document.getElementById(objName).value +" New => "+entry['pho_value']);
							document.getElementById(objName).value=entry['pho_value'];
							console.log(objName+" => '"+entry['pho_value']+"' ");
							if (entry['pho_type'].toLowerCase() == 'css') {
								document.getElementById("chk_"+objName).checked=false;
								console.log("chk_"+objName+" => unchecked");
							}
						}
					} else {
//						alert('ObjName =>'+objName+" => No Defined.");
					}
				});
			}
		},"json");
	}
	
	function layout_popup(ID) {
		var URL="popup_gencss.php?tmph_id="+ID;

		var Feature='"location=no,menubar=no,resizeable=no,scrollbars=no,status=no,toolbar=no,width=500,height=625"';
		Feature+=(screen.height/6);
		Feature+=',left='+(screen.width/4)+');"';
//		alert(Feature);
		window.open(URL,'popup_layout',Feature);
	}
	
	function layout_show_template() {
		$('#dvSelectLayout').dialog('open');
		var URL='./ajax/layout_template_list.php';
		var Params={};
		var html='<tr><td class="center" colspan="4">No Record Found</td></tr>';
		$('#tblSelectLayout_Body').empty();
		$('#tblSelectLayout_Body').append(html);
		$.post(URL,Params,function(data) { 
			if (data != null) {
				if (data.length > 0) {
					$.each(data,function(entryIndex,entry) {
						var onRadio="setValue('txtExistLayout','"+entry['tmph_name']+"','');";
						onRadio+="setValue('SelectLayoutID','"+entry['tmph_id']+"','');";
						onRadio+="EnableObject('btnSelectLayout',true,'');";
						onView="layout_popup("+parseInt(entry['tmph_id'])+");";
//						onRadio+="document.getElementById('btnSelectLayout').disabled = null;";
//						onRadio+="alert($('#btnSelectLayout').is(\"disabled\"));";
						html="";
						html += '<tr>';
						html += '<td class="w-5" nowrap><input id="rdoLayout" name="rdoLayout" type="radio" onClick="'+onRadio+'">';
						html += '<image src="./images/icons/preview.png" id="imgView_'+entry['tmph_id']+'" class="iconList" onClick="'+onView+'" title="Preview"></td>';
						html += '<td>'+entry['tmph_name']+'</td>';
						html += '<td class="w-15">'+entry['disp_width']+' X ' +entry['disp_height']+'</td>';
						html += '<td class="right w-5">'+entry['counter']+'</td>';
						html+="</tr>";
						$('#tblSelectLayout_Body').append(html);
					});
					$('#tblSelectLayout_Body tr:first').hide();
				}
			}
		},"json");
	}
	
	function layout_select_template(file_id,file_name) {
		$('#dvSelectLayout').dialog('close');
		document.getElementById('TemplateID').value=file_id;		
		document.getElementById('txtTemplateAdd').value=file_name;		
		document.getElementById('btnSelTemplateSave').disabled=false;		
	}
	
	function layout_remove(HeadID) {
		console.log('layout_remove('+HeadID+')');
		var URL='./ajax/layout_remove.php';
		var Params={'HeadID':HeadID};
		$.post(URL,Params,function(data) { 
			if (parseInt(data) > 0) {
				console.log("remove.... successful");
				load_headlist();
			} else {
				console.log("remove.... failed");
			}
		});
	}