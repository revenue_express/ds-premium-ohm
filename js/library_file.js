	function file_rename(fid,fname,dialog,typeID) {
		var URL='./ajax/library_rename.php';
		var Params={"fid":fid, "fname":encodeURI(fname)};
		$.post(URL,Params,function(data) {
			if (eval(data) > 0) {
				showAlert('dvAlert','dvAlertMsg','Remame','Rename Successful.','');
//				alert("Rename Successful");
				$('#'+dialog).dialog("close");
				$('#btnListRefresh').click();
			} else {
//				alert("Rename Failed");
			}
		});
	}

	function file_active(fid,fname,Active) {
		var Mode=(parseInt(Active)>0)?"Restore":"Delete";
		var msg="Do you confirm to "+Mode+" this file ('"+fname+"') ?";
		console.log('file_active');
		if (confirm(msg)) {
			var URL="./ajax/library_active.php";
			var Params={'FID':fid,'Active':parseInt(Active)};
			$.post(URL,Params,function(data) {
				if (parseInt(data) > 0) {
					showAlert('dvAlert','dvAlertMsg',Mode+' File',Mode+' Successful.','');
					$('#btnListRefresh').click();
					console.log(Mode+' successful');
				} else {
					console.log(Mode+" Failed");
//					alert(Mode+" Failed");
				}
			});
		}
	}
	
	function file_delete(fid,fname,path,oname) {
		var msg1="Do you confirm to delete this file ('"+fname+"') from Server?";
		var msg2="Are you sure to delete this file ('"+fname+"') from Server?";
		if (confirm(msg1)) {
			if (confirm(msg2)) {
				var URL="./ajax/library_purge.php";
				var Params={'FID':fid,'Path':path,'Name':oname};
				console.log('file_delete URL:'+URL);
				console.log(Params);
				$.post(URL,Params,function(data) {
					if (parseInt(data) > 0) {
						showAlert('dvAlert','dvAlertMsg','Remove File','Remove Successful.','');
//						alert("Delete completed.");
						$('#btnListRefresh').click();
					} else {
//						alert("Delete failed");
					}
				});
			}
		}
	}

	function show_rename(fid,fname,dialog) {
		$('#txtRenameFID').val(fid);
		$('#txtRenameFName').val(fname);
		$('#txtOldFName').val(fname);
		$('#btnRenameSave').attr('disabled',true);	
		$('#'+dialog).dialog("open");
	}

	function file_list(TypeID) {
		var URL='./ajax/library_list.php';
		var Params={"TypeID":TypeID};

		var user_level=window.parent.document.getElementById('usr_level').value;
		var user_name=window.parent.document.getElementById('usr_name').value;
		var user_id=window.parent.document.getElementById('usr_id').value;

		var TypeName="";
		var iCols=5;
		$('#tblList_Body').empty();
//		console.log("file_list :: TypeID ->"+TypeID);
//		console.log("file_list :: user_level ->"+user_level +"; user_name -> "+user_name);
		
		switch (TypeID) {
			case 1: TypeName="images"; iCols=5; break;
			case 2: TypeName="videos"; iCols=6; break;
			case 3: TypeName="flashs"; iCols=5; break;
			case 4: TypeName="audios"; iCols=5; break;
			case 9: TypeName="docs"; iCols=4; break;
			case 10: TypeName="pdfs"; iCols=4; break;
			case 11: TypeName="covers"; iCols=5; break;
			case 12: TypeName="ppts"; iCols=4; break;
		}

		$('#tblList_Body').append('<tr><td colspan="'+iCols+'" align="center">No Record Found.</tr>');
		$.post(URL,Params,function(data) {
			if (data != null) {
				if (data.length > 0) {
					$.each(data,function(idx,entry) {
						console.log(entry)
//						ifilesize=entry['file_size']/1024;
						var onRename="show_rename("+entry['file_id']+",'"+entry['file_dname']+"','dvRename');";
						var onDelete="file_active("+entry['file_id']+",'"+entry['file_dname']+"',0);";
						var onRestore="file_active("+entry['file_id']+",'"+entry['file_dname']+"',1);";
						var onPurge="file_delete("+entry['file_id']+",'"+entry['file_dname']+"','"+TypeName+"','"+entry['file_sname']+"');";

						var EIcon='<image src="images/icons/edit.png" width="16" height="16" id="imgRename_'+entry['file_id']+'" class="iconListRename" onClick="'+onRename+'" title="Rename">';
            
						var FileName='./data/'+entry['file_path']+'/'+entry['file_sname'];
						var onView="window.open('"+FileName+"','_view');";

						var VIcon='<image src="./images/icons/preview.png" id="imgView_'+entry['file_id']+'" class="iconList" onClick="'+onView+'" title="Preview">';
						var DIcon='<image src="images/icons/delete.png" id="imgDelete_'+entry['file_id']+'" class="iconList" fid="'+entry['file_id']+'" onClick="'+onDelete+'" title="Delete">';
						var RIcon='<image  src="images/icons/restore.png"id="imgRestore_'+entry['file_id']+'" class="iconList" fid="'+entry['file_id']+'" title="Restore" onClick="'+onRestore+'">';
						var PIcon='<image src="images/icons/purge.png" id="imgPurge_'+entry['file_id']+'" class="iconList" fid="'+entry['file_id']+'" onClick="'+onPurge+'" title="'+onPurge+'">';

						html='<tr>';
						VIcon='';
						if ((entry['file_type_id']==1) || (entry['file_type_id']==11) ) {
							html+='<td><img style="cursor:pointer" src="'+entry['file_small']+'" onClick="'+onView+'"></td>';
						} else {
							var icoFile='./images/ico_'+entry['file_path']+'.png';
							html+='<td><img style="cursor:pointer" src="'+icoFile+'" onClick="'+onView+'"></td>';
						}
							html+='<td>'+strShow(entry['file_oname'],60);
							html+='<br>'+EIcon+'&nbsp;'+strShow(entry['file_dname'],60);
							html+='</td>';
//						} else {
//						html+='<td><div style="float:left">'+EIcon+'&nbsp; </div>'+strShow(entry['file_dname'],20)+'</td>';
//							html+='<td>'+strShow(entry['file_oname'],40)+'</td>';
//						}
//						html+='<td class="right">'+addCommas(ifilesize.toFixed(0))+'</td>';
						html+='<td class="right">'+HumanFileSize(entry['file_size'])+'</td>';

						if ((entry['file_type_id'] == 1) || (entry['file_type_id']==2) || (entry['file_type_id']==11)) {
						// Display Size of Images/Videos 
							html+='<td class="center">'+entry['Size']+'</td>';
						}
						console.log('TypeID => '+ TypeID);
						if ((entry['file_type_id'] == 2) || (entry['file_type_id']==4)){ // Duration of Videos/Audios
							html+='<td class="right">'+entry['Duration']+'</td>';
						}

						if (user_level.toString().toLowerCase()!='operator') {
							if (parseInt(entry['file_active'])==1) {
								if (parseInt(entry['Used'])>0) {
									html+='<td><div>'+VIcon+'</div></td>';							
								} else {
									html+='<td><div>'+VIcon+' &nbsp; '+DIcon+' &nbsp; '+PIcon+'</div></td>';
								}
							} else {
								if (parseInt(entry['Used'])>0) {
									html+='<td><div>'+VIcon+' &nbsp; '+RIcon+'</div></td>';							
								} else {
									html+='<td><div>'+VIcon+' &nbsp; '+RIcon+' &nbsp; '+PIcon+'</div></td>';							
								}
							}
							html+='</tr>';
						} else {
//	Operator User
							if (parseInt(entry['file_active'])==1) {
								if (parseInt(entry['Used'])>0) {
									html+='<td><div>'+VIcon+'</div></td>';							
								} else {
									html+='<td><div>'+VIcon+' &nbsp; '+DIcon+'</div></td>';							
								}
							} else {
								html="";
							}
						}
//						htmls+=html;
						$('#tblList_Body').append(html);
//						console.log("file_list :: Append "+html);
					});
					$('#tblList_Body tr:first').hide();
					$('#tblList_Body tr:odd').addClass('even');
				}
			}
		},"json");
	}
	