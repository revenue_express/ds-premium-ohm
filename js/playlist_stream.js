	function stream_clear() {
		$('#dvHeadResult').hide();
		$('#dvLineResult').hide();
	}

	function stream_head_list() {
		stream_clear();
		$('#dvHeadResult').show();
		var user_level=window.parent.document.getElementById('usr_level').value;
		var user_name=window.parent.document.getElementById('usr_name').value;
		var user_id=window.parent.document.getElementById('usr_id').value;
		var URL='./ajax/playlist_stream_head_list.php';
//		alert(URL);
		$('#tblHead_Result').empty();
		var Params={};
		$.post(URL,Params,function(data) {
			var html='';
//			alert('Data => '+data);
			if (data.length > 0) {
				$.each(data,function(entryIndex,entry) {
					onEdit="stream_refresh("+entry['plh_id']+");";
					onCopy="stream_clone("+entry['plh_id']+")";
					onDelete="playlist_head_active("+entry['plh_id']+",'"+entry['plh_name']+"',0);";
					onRestore="playlist_head_active("+entry['plh_id']+",'"+entry['plh_name']+"',1);";
					
					var InUsed="NO";
					if (parseInt(entry['Used'])>0) InUsed="YES";
					if (entry['plh_active']==1) {
						html +="<tr>";
						html +='<td class="w-10">'+entry['plh_code']+'</td>';
						html +='<td>'+entry['plh_name']+'</td>';
						html += '<td><div style="margin-left:15%;">';
	//					html +='<img class="btnPre" onClick="feed_head_preview(\'feedHeadPreview\','+entry['plh_id']+');" title="preview"> &nbsp;';
						html += '<div class="dvtd_left w-30"><div class="btnEdit" onClick="'+onEdit+'" title="edit"> &nbsp;</div></div>';
						html += '<div class="dvtd_left w-30"><div class="btnCopy" onClick="'+onCopy+'" title="clone"></div></div>';
						html += '<div class="dvtd_left w-30">';
						if (parseInt(entry['Used'])==0) {
							html +='<div class="btnDel" onClick="'+onDelete+'" title="delete"></div>';
						} else {
							html +='&nbsp;';
						}
						html +='</div></td></tr>';
					} else {
						if (user_level.toString().toLowerCase()!='operator') {
							html +="<tr>";
							html +='<td class="w-10">'+entry['plh_code']+'</td>';
							html +='<td>'+entry['plh_name']+'</td>';
							html += '<td><div style="margin-left:15%;">';
		//					html +='<img class="btnPre" onClick="feed_head_preview(\'feedHeadPreview\','+entry['plh_id']+');" title="preview"> &nbsp;';
							html += '<div class="dvtd_left w-30"><div class="btnEdit" onClick="'+onEdit+'" title="edit"> &nbsp;</div></div>';
							html += '<div class="dvtd_left w-30"><div class="btnCopy" onClick="'+onCopy+'" title="clone"></div></div>';
							html += '<div class="dvtd_left"><div class="btnRes" onClick="'+onRestore+'" title="restore"></div></div>';
							html +='</td></tr>';
						}
					}
				});
			} else {
				var html="<tr><td align='center' colspan='4'>No Record Found.</td></tr>";
			}
			$('#tblHead_Result').append(html);
			$('tr:odd').addClass('even');
		},"json");
	}



	function stream_refresh(HeadID) {
		stream_clear();
		$('#dvLineResult').show();
		document.getElementById('HeadID').value=HeadID;
		stream_head_info(HeadID);
		stream_line_list(HeadID);
	}
	
	function stream_head_info(HeadID) {
		var URL='./ajax/playlist_head_info.php';
		var Params={'HeadID':HeadID};
		var html='';
		$.post(URL,Params,function(data) {
			if (data.length > 0) {
				document.getElementById('HeadID').value=HeadID;
				document.getElementById('txtHeadName').value=data[0]['plh_name'];
				document.getElementById('txtOldName').value=data[0]['plh_name'];
			}
		},"json");
	}

	function stream_line_list(HeadID) {
		var URL='./ajax/playlist_stream_info.php';
		var Params={'HeadID':HeadID,'TypeID':8};
		$('#tblLine_Result').empty();
		console.log('stream_line_list('+HeadID+')');
		var html='<tr><td colspan="2" class="center"><h3>No Record Found</h3></td></tr>';
		$('#tblLine_Result').append(html);
		$.post(URL,Params,function(data) {
			if (data.length > 0) {
			console.log(data);
				html='';
				var iRun=0;
				var LastID=0;
				var NextID=0;
//				console.log("stream_line_list");
				$.each(data,function(entryIndex,entry) {
//					console.log(entry);
					stmAddress=entry['stm_protocol']+"://"+entry['stm_address']+":"+entry['stm_port']+entry['stm_para1'];
					if (entry['stm_para2'] != null) stmAddress+=entry['stm_para2']; 
					onView='window.open(\'./data/'+entry['type_path']+'/'+entry['file_sname']+'\',\'preview\');';
					onDelete="playlist_stream_active("+entry['plstm_id']+",0)";
					onRestore="playlist_stream_active("+entry['plstm_id']+",1)";

					html+='<tr>';
					html+='<td class="left">'+stmAddress+'</td>';
        	html+='<td class="center" width="120">';
          html+='<div class="dvtr top5 w-20">';
          html+='<div class="dvtd_left w-20 center">';
					if (entry['plstm_active']==1) {
						html+='<img class="preview" src="images/icons/delete.png" title="delete" onClick="'+onDelete+'">';
					} else {
						html+='<img class="preview" src="images/icons/restore.png" title="restore" onClick="'+onRestore+'">';
					}
					html+='</div>';
       		html+='</div>';
        	html+='</td>';
      		html+='</tr>';
					LastID=entry['plstm_id'];
					iRun++;
				});
			}
			$('#tblLine_Result').append(html);
			$('#tblLine_Result tr:first').hide();
			$('tr:even').addClass('even');
		},"json");
	}

	function playlist_line_order(HeadID,DstID,SrcID) {
//		alert("Head => "+HeadID+"; Dest => "+DstID+"; Src => "+SrcID);
		var URL='./ajax/playlist_line_move.php';
		var Params={'TypeID':document.getElementById('TypeID').value,'PlayListID':HeadID,'NextID':DstID,'CurrID':SrcID};
		$.post(URL,Params,function(data) {
			if (parseInt(data) > 0) {
//				alert("Ordering Successful.");
				stream_refresh(HeadID);
			} else {
//				alert("Ordering Failed");
			}
		});
	}

	function playlist_head_add() {
		var HeadID=document.getElementById('HeadID').value;
		var HeadName=encodeURI(document.getElementById('txtHeadName').value);
		var TypeID=document.getElementById('TypeID').value;
		var Params={'HeadID':HeadID,'Name':HeadName,'TypeID':TypeID};
		var URL='./ajax/playlist_head_add.php';
		if (! ((TypeID=='') || (TypeID==undefined))) {
			$.post(URL,Params,function(data) {
//				alert (dump(data));
				newHeaderID=parseInt(data);
				if (newHeaderID > 0) {
//					alert('Add Completed');
					document.getElementById('HeadID').value=newHeaderID;
					stream_refresh(newHeaderID);	
				} else {
//					alert('Add Failed');
				}
			});
		} else {
//			alert('Failed :: No TypeID');
		}
	}

	function playlist_head_save() {
		var HeadID=document.getElementById('HeadID').value;
		var HeadName=encodeURI(document.getElementById('txtHeadName').value);
		var Params={'HeadID':HeadID,'Name':HeadName};
		var URL='./ajax/playlist_head_save.php';
		if (! ((HeadID=='') || (HeadID==undefined))) {
			$.post(URL,Params,function(data) {
//				alert (dump(data));
				if (parseInt(data) > 0) {
//					alert('Save Completed');
					document.getElementById('btnHeadSave').disabled=true;
					stream_refresh(document.getElementById('HeadID').value);					
				} else {
//					alert('Save Failed');
				}
			});
		}
	}

	function stream_preview(FileID) {
	}
	
	function stream_clone(HeadID) {
		var URL='./ajax/playlist_head_add.php';
		var Params={'TypeID':document.getElementById('TypeID').value};
//		alert(URL);
		$.post(URL,Params,function(data) {
			var html='';
//			alert('data => '+dump(data));
			if (parseInt(data) > 0) {
				newHeadID=parseInt(data);
				var URL='./ajax/playlist_stream_load.php';
				var Params={'SrcID':HeadID,'DstID':newHeadID,'TypeID':document.getElementById('TypeID').value}
				$.post(URL,Params,function(data) {
//			alert(dump(data));
					if (parseInt(data) > 0) {
						stream_refresh(newHeadID);
					}
				});
			}
		});

	}
	
	function show_stream_exists() {
		$('#dvStreamFile').dialog('open');
		var URL='./ajax/stream_list.php';
//		alert(URL);
		var Params={'TypeID':document.getElementById('TypeID').value};
		$.post(URL,Params,function(data) {
//			alert('Data => '+data);
			var html="<tr><td align='center' colspan='2'>No Found</td></tr>";
			$('#tblStreamFile_Body').empty();
			if (data.length > 0) {
				html='';
				$.each(data,function(entryIndex,entry) {
					onClick='playlist_select_stream('+entry['stm_id']+",'"+entry['stm_address']+"');";
					if (parseInt(entry['stm_active'])==1) {
						html+="\n<tr><td class=\"w-5\"><input id='rdoItem' name='rdoItem' type='radio' onClick=\""+onClick+"\"></td>";
						html+="<td>"+entry['stm_protocol']+"://"+entry['stm_address']+":"+entry['stm_port']+"/"+entry['stm_para1']+"</td></tr>";
					}
				});
			}
			$('#tblStreamFile_Body').append(html);
			$('#tblStreamFile_Body tr:odd').addClass('even');
		},"json");
	}

	function playlist_select_stream(stream_id,stream_name) {
		document.getElementById('SelectStreamID').value=stream_id;		
		document.getElementById('txtExistStream').value=stream_name;		
		$("#btnSelectStream").removeClass();
		document.getElementById('btnSelectStream').disabled=false;
		document.getElementById('btnSelectStream').className = 'btnButton';	
	}
	
	function playlist_exist_stream() {
		var URL='./ajax/playlist_stream_add.php';
		var Params={'HeadID':document.getElementById('HeadID').value,
			'StreamID':document.getElementById('SelectStreamID').value};
		console.log('playlist_exist_stream()');
		console.log(Params);
		$.post(URL,Params,function(data) {
//			alert(dump(data));
			if (parseInt(data) > 0) {
//				alert("Add Exists Successful.");
				stream_refresh(document.getElementById('HeadID').value);
				$('#dvStreamFile').dialog('close');
			} else {
//				alert("Add Exists Failed");
			}
		});
	}

	function show_stream_playlist_exists() {
		var HeadID=document.getElementById('HeadID').value;
		$('#dvStreamPlaylist').dialog('open');
		var URL='./ajax/playlist_stream_head_list.php';
//		alert(URL);
		var Params={};
		console.log("show_stream_playlist_exists("+HeadID+")");
		$.post(URL,Params,function(data) {
//			alert(dump(data));
			$('#tblStreamPlaylist_Body').empty();
			var html="<tr><td align='center' colspan='2'>No RecordFound</td></tr>";
			$('#tblStreamPlaylist_Body').append(html);
			console.log(data);
			if (data.length > 0) {
				html='';
				$.each(data,function(entryIndex,entry) {
					html='';
					console.log(data);
					onClick='playlist_select_playlist('+entry['plh_id']+",'"+entry['plh_name']+"');";
//					if ( (parseInt(entry['counter']) > 0) && (parseInt(entry['plh_id']) != parseInt(HeadID)) && (parseInt(entry['plh_active'])==1) ) {
					if ( (parseInt(entry['plh_id']) != parseInt(HeadID)) && (parseInt(entry['plh_active'])==1) ) {
						html+="\n<tr><td class=\"w-5\"><input id='rdoItem' name='rdoItem' type='radio' onClick=\""+onClick+"\"></td>";
						html+="<td>"+entry['plh_name'];+"</td></tr>";
					}
					$('#tblStreamPlaylist_Body').append(html);
				});
			}
			$('#tblStreamPlaylist_Body tr:first').hide();
			$('#tblStreamPlaylist_Body tr:odd').addClass('even');
		},"json");
	}

	function playlist_select_playlist(playlist_id,playlist_name) {
		document.getElementById('StreamPlaylistID').value=playlist_id;		
		document.getElementById('txtExistPlaylist').value=playlist_name;		
		$("#btnStreamPlaylist").removeClass();
		document.getElementById('btnStreamPlaylist').disabled=false;
		document.getElementById('btnStreamPlaylist').className = 'btnButton';		
	}

	function playlist_exist_stream_playlist() {
		var URL='./ajax/playlist_stream_load.php';
		var Params={'SrcID':document.getElementById('StreamPlaylistID').value,
			'DstID':document.getElementById('HeadID').value,
			'TypeID':document.getElementById('TypeID').value}
		$.post(URL,Params,function(data) {
//			alert(dump(data));
			if (parseInt(data) > 0) {
//				alert("Add Exists Successful.");
				stream_refresh(document.getElementById('HeadID').value);
				$('#dvStreamPlaylist').dialog('close');
			} else {
//				alert("Add Exists Failed");
			}
		});
	}
	
	function playlist_head_active(HeadID,HeadName,Active) {
		var Params={'HeadID':HeadID,'Active':Active};
		var URL='./ajax/playlist_head_active.php';
		var Mode="Delete";
		if (Active == 1) Mode="Restore";
		if (! ((HeadID=='') || (HeadID==undefined))) {
			var Display="Do you confirm to delete this playlist?";
			var bConfirm=true;
			if (Active == 0) bConfirm=confirm(Display);
			if (bConfirm==true) {
				$.post(URL,Params,function(data) {
	//				alert (dump(data));
					if (parseInt(data) > 0) {
//						alert(Mode+' Completed');
						document.getElementById('btnHeadSave').disabled=true;
						stream_head_list();					
					} else {
//						alert(Mode+' Failed');
					}
				});
			}
		}
	}

	function playlist_stream_active(LineID,Active) {
		var Params={'LineID':LineID,'Active':Active};
		var URL='./ajax/playlist_stream_active.php';
		var Mode="Delete";
		if (Active == 1) Mode="Restore";
		if (! ((LineID=='') || (LineID==undefined))) {
			var Display="Do you confirm to delete this items?";
			var bConfirm=true;
			if (Active == 0) bConfirm=confirm(Display);
			if (bConfirm==true) {
				$.post(URL,Params,function(data) {
	//				alert (dump(data));
					if (parseInt(data) > 0) {
//						alert(Mode+' Completed');
						document.getElementById('btnHeadSave').disabled=true;
						stream_line_list(document.getElementById('HeadID').value);
					} else {
//						alert(Mode+' Failed');
					}
				});
			}
		}
	}
