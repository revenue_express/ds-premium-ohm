var user_level=window.parent.document.getElementById('usr_level').value;
var user_name=window.parent.document.getElementById('usr_name').value;
var user_id=window.parent.document.getElementById('usr_id').value;
var com_id=window.parent.document.getElementById('usr_com_id').value;

	function feed_show(Viewer,Feed,Language) {
		display=document.getElementById(Viewer);
		display.setAttribute('charset',document.getElementById(Language).value);
		display.innerHTML="Example Feeding ::::: "+document.getElementById(Feed).value;
		display.focus();
	}

	function feed_preview(Viewer,Feed,Language) {
		display=document.getElementById(Viewer);
		display.setAttribute('charset',Language);
//		display.innerHTML="Example Feeding ::::: "+Feed;
		display.innerHTML=Feed;
		display.focus();
	}
	
	function feed_list() {
		feed_clear();
		$('#dvResult').show();
		var URL='./ajax/feed_list.php';
		var user_level=window.parent.document.getElementById('usr_level').value;
		var user_name=window.parent.document.getElementById('usr_name').value;
		var user_id=window.parent.document.getElementById('usr_id').value;
//		alert(URL);
		$('#tblFeed_Result').empty();
		var Params={'ComID':com_id};
		$.post(URL,Params,function(data) {
			var html='';
//			alert('Data => '+data);
			if (data.length > 0) {
				$.each(data,function(entryIndex,entry) {
					var onPreview="feed_preview('txtReview','"+entry['feed_data']+"','"+entry['feed_charset']+"');";
					var onEdit="feed_show_edit("+entry['feed_id']+");";
					var onDelete="feed_delete("+entry['feed_id']+");";
					var onRestore="feed_restore("+entry['feed_id']+");";
					
					var InUsed="NO";
					if (parseInt(entry['feed_used'])>0) {
						var Msg="!!!! Feed In Used !!!\n\n\n Do you Confirm to Delete? ";
						var onDelete='if (confirm(\''+Msg+'\')) { feed_delete('+entry['feed_id']+');}';
						InUsed="YES";
					}
					html ="<tr>";
//					html +='<td>'+entry['feed_lang']+'</td>';
					html +='<td>'+entry['feed_data']+'</td>';
//					html +='<td class="center">'+InUsed+'</td>';
					html +='<td><center><div class="btnPre" onClick="'+onPreview+'" title="view"></div></center></td>';
					html += '<td><center><div class="btnEdit" onClick="'+onEdit+'" title="edit"></div></center></td>';
					if (entry['feed_active']==1) {
						if (parseInt(entry['feed_used']) == 0) {	
							html += '<td><center><div class="btnDel" onClick="'+onDelete+'" title="delete"></div></center></td>';
						}
					} else {
						html += '<td><center><div class="btnRes" onClick="'+onRestore+'" title="restore"></div></center></td>';
					}
					html +="</tr>";
					if (user_level.toLowerCase() != 'operator') {
						$('#tblFeed_Result').append(html);
					} else { 
						if (parseInt(entry['feed_active']) > 0) {
							$('#tblFeed_Result').append(html);
						}
					}
				});
			} else {
				var html="<tr><td align='center' colspan='5'>No Record Found.</td></tr>";
				$('#tblFeed_Result').append(html);
			}
			$('#dvResult tr:odd').addClass('even');
		},"json");
	}

	function feed_clear() {
		$('#dvResult').hide();
		$('#dvForm').hide();
	}
	
	function feed_reset() {
//		EnableObject('btnSave',false);
		document.getElementById('txtFeed').value=document.getElementById('txtOldFeed').value
	}
	
	function feed_show_edit(FeedID) {
		feed_clear();
		$('#dvForm').show();
		var URL='./ajax/feed_info.php';
//		alert(URL);
		var Params={'FeedID':FeedID};
		$.post(URL,Params,function(data) {
//			alert('Data => '+data);
			if (data.length > 0) {
				document.getElementById('FeedID').value=FeedID;
				document.getElementById('selLang').value=data[0]['feed_charset'];
				document.getElementById('txtFeed').value=data[0]['feed_data'];
				document.getElementById('txtOldFeed').value=data[0]['feed_data'];
			}
		},"json");
	}

	function feed_delete(FeedID) {
		var sConfirm='Do you confrim to delete this feed?';
		if (confirm(sConfirm)) {
			var URL='./ajax/feed_active.php';
	//		alert(URL);
			var Params={'FeedID':FeedID,'Active':0};
			$.post(URL,Params,function(data) {
				if (eval(data) > 0) {
//					alert('Delete Completed.');
					feed_list();
				} else {
//					alert('Delete Failed.');
				}
			});
		}
	}

	function feed_restore(FeedID) {
		var sConfirm='Do you confrim to restore this feed?';
		if (confirm(sConfirm)) {
			var URL='./ajax/feed_active.php';
	//		alert(URL);
			var Params={'FeedID':FeedID,'Active':1};
			$.post(URL,Params,function(data) {
				if (eval(data) > 0) {
//					alert('Restore Completed.');
					feed_list();
				} else {
//					alert('Restore Failed.');
				}
			});
		}
	}
	
	function feed_show_add() {
		feed_clear();
		$('#dvForm').show();
		document.getElementById('FeedID').value='';
		document.getElementById('selLang').selectedIndex=0;
		document.getElementById('txtFeed').value='';
	}

	function feed_save(ID,Data,Lang) {
		var sConfirm='Do you confrim to save this feed?';
		if (document.getElementById(ID).value=='') {
			var sMode="Add";
			var URL='./ajax/feed_add.php';
			var Params={
					'Data':encodeURI(document.getElementById(Data).value),
					'Charset':encodeURI(document.getElementById(Lang).value),
					'ComID':com_id,
					'Lang':encodeURI(document.getElementById(Lang).value)
//					'Lang':encodeURI(document.getElementById(Lang).options[document.getElementById(Lang).selectedIndex].text)
					};
		} else {
			var sMode="Edit";
			var URL='./ajax/feed_edit.php';
			var Params={
					'FeedID':document.getElementById(ID).value,
					'Data':encodeURI(document.getElementById(Data).value),
					'Charset':encodeURI(document.getElementById(Lang).value),
					'Lang':encodeURI(document.getElementById(Lang).value)
//					'Lang':encodeURI(document.getElementById(Lang).options[document.getElementById(Lang).selectedIndex].text)
					};
		}
		if (confirm(sConfirm)) {
//			alert(URL);
			$.post(URL,Params,function(data) {
//				prompt('data',data);
				if (data == '0') {
//					alert(sMode+' Failed.');
				} else {
//					alert(sMode+' Completed.');
					feed_list();
				}
			});
		}
	}
