	function clear_allsection() {
		$('#dvHeadResult').hide();
		$('#dvHeadEdit').hide();
		$('#dvPeriod').hide();
		$('#dvLayout').hide();
	}

	function load_headlist() {
		console.log('load_headlist::');
		var user_level=window.parent.document.getElementById('usr_level').value;
		var user_name=window.parent.document.getElementById('usr_name').value;
		var user_id=window.parent.document.getElementById('usr_id').value;
		var com_id=window.parent.document.getElementById('usr_com_id').value;

		var URL='./ajax/schedule_list.php';
//		alert(URL);
		var Params={"ComID":com_id};
		$.post(URL,Params,function(data) {
			$('#dvHeadResult').show();
			$('#txtSearch').val('');
			var html="<tr><td colspan='6' class='norecord'>No Record Found</td></tr>";
			$('#showHead_Data').empty();
			$('#showHead_Data').append(html);
			$('#spHeadSearch').show();
			if (data.length > 0) {
				$.each(data, function(entryIndex,entry) {
					html = '';
					var onView ='';
					var onEdit ='schedule_edit('+entry['schh_id']+');';
					var onRestore ='schedule_active('+entry['schh_id']+',1);';
					var onDelete ='schedule_active('+entry['schh_id']+',0);';
					var onPurge ='schedule_delete('+entry['schh_id']+');';
					var onClone ='schedule_clone('+entry['schh_id']+');';
					var InUsed="NO";
					if (parseInt(entry['Used'])>0) InUsed="YES";

					html = '<tr>';
					html += '<td>['+entry['schh_id']+'] '+entry['schh_name']+'</td>';
					html += '<td>'+entry['disp_name']+'</td>';
					html += '<td class="right">'+entry['sum_items']+'</td>';

					html += '<td class="center" width="16">';
					html += '<img class="iconList" width="16" height="16" src="./images/icons/edit.png" onClick="'+onEdit+'" title="edit">';
					html += '</td>';

					if (parseInt(entry['schh_active'])==0) {
						
						if (parseInt(entry['Used'])>0) {
							html += '<td class="center action" width="16">';
							html += '<img class="iconList" src="./images/icons/restore.png" onClick="'+onRestore+'" title="restore">';
							html += '</td>';
						} else {
							html += '<td class="center action" width="16">&nbsp;</td>';
							html += '<td class="center action" width="16">';
							html += '<img class="iconList" src="./images/icons/restore.png" onClick="'+onRestore+'" title="restore">';
							html += '</td>';
							html += '<td class="center action" width="16">';
							html += '<img class="iconList" src="./images/icons/purge.png" onClick="'+onPurge+'" title="remove">';
							html += '</td>';
						}
					} else {
						html += '<td class="action" align="center" width="16">';
						html += '<img class="preview" width="16" height="16" src="./images/icons/copy.png" onClick="'+onClone+'" title="clone">';
						html += '</td>';

						html += '<td class="action" align="left" width="16">';
						if (parseInt(entry['Used'])>0) {
							html += '&nbsp;';
						} else {
							html += '<img class="preview" width="16" height="16" src="./images/icons/delete.png" onClick="'+onDelete+'" title="delete">';
						}
						html += '</td><td class="action"></td>';
					}
					
					html += '</tr>';

					if (user_level.toLowerCase() == 'operator') {
						if (parseInt(entry['schh_active'])==1) {
							$('#showHead_Data').append(html);
							$('#showHead_Data tr:first').hide();
						}
					} else {
						$('#showHead_Data').append(html);
						$('#showHead_Data tr:first').hide();
					}
				});
			} 
		},"json");
	}
	
	function schedule_edit(schID) {
		clear_allsection();
		$('#spHeadSearch').hide();
		schedule_head_view(schID);
		schedule_period_list(schID);
		schedule_layout_list(schID);
	}
	
	function schedule_delete(schID) {
		var Display="Comfirm to delete Schedule?";
		if (confirm(Display)==true) {
			var URL='./ajax/schedule_delete.php';
			var Params={'schID':schID};
	//		alert(URL);
			if (confirm(Display)) {
				$.post(URL,Params,function(data) {
					alert(data);
					load_headlist();
				});
			}
		}
	}
	
	function schedule_active(ScheduleID,Active) {
		var Mode="delete";
		if (Active==1) Mode="restore";
		var Display="Comfirm to "+Mode+" this schedule?";
		var URL='./ajax/schedule_active.php';
		var Params={'ScheduleID':ScheduleID,'Active':Active};
//		alert(URL);
		if (confirm(Display)) {
			$.post(URL,Params,function(data) {
				if (parseInt(data) > 0) {
//					alert(Mode+' Completed.');
					load_headlist();
				} else {
//					alert(Mode+' Failed.');
				}
			});
		}
	}
	
	
	function schedule_restore(schID) {
		var URL='./ajax/schedule_restore.php?schID='+schID;
//		alert(URL);
		$.getJSON(URL,function(data) {
			if (data > 0) {
//				alert('Restore Completed.');
				schedule_edit(schID)
			} else {
//				alert('Restore Failed.');
			}
		});
	}
	
	function schedule_add_new() {
		var URL='./ajax/schedule_head_add.php';
//		alert(URL);
		$.getJSON(URL,function(data) {
			if (data > 0) {
				schedule_edit(data);
			}
		});
	}
	
	function schedule_head_view(schID) {
		var URL='./ajax/schedule_head_view.php?schID='+schID;
//		alert(URL);
		$.getJSON(URL,function(data) {
			if (data.length = 1) {
				$('#dvHeadEdit').show();
				$.each(data,function(entryIndex,entry) {
					document.getElementById('ScheduleID').value=entry['schh_id'];
					document.getElementById('txtSchedule').value=entry['schh_name'];
					document.getElementById('txtOldSchedule').value=entry['schh_name'];
					document.getElementById('DispID').value=entry['schh_disp_id'];
					document.getElementById('OldDispID').value=entry['schh_disp_id'];
					document.getElementById('txtDisplay').value=entry['disp_name'];
					document.getElementById('txtOldDisplay').value=entry['disp_name'];
				});
				document.getElementById('btnHeadSave').disabled=true;
			}
		});
	}
	
	function schedule_period_list(schID) {
		var URL='./ajax/schedule_period_list.php?schID='+schID;
		
		var user_level=window.parent.document.getElementById('usr_level').value;
		var user_name=window.parent.document.getElementById('usr_name').value;
		var user_id=window.parent.document.getElementById('usr_id').value;

		var Params={'schID':schID};

//		alert(URL);
		var html="<tr><td align='center' colspan='4'>No Record Found.</td></tr>";
		$('#tblPeriodLine').empty();
		$('#tblPeriodLine').append(html);
		var iShow=0;
		$.post(URL,Params,function(data) {
			$('#dvPeriod').show();
			$('#tblPeriodLine').show();
			if (data.length > 0) {
				$.each(data,function(entryIndex,entry) {
					var html='<tr>';
					onEdit="schedule_period_edit_form("+entry['schp_id']+",'"+entry['start']+"','"+entry['stop']+"');";
					onDelete="schedule_period_active("+schID+","+entry['schp_id']+",0);";
					onRestore="schedule_period_active("+schID+","+entry['schp_id']+",1);";
					onPurge="schedule_period_active("+schID+","+entry['schp_id']+",0);";

					html += "<td>"+entry['start']+"</td>";
					html += "<td>"+entry['stop']+"</td>";
					html += '<td><img class="preview" width="16" height="16" src="./images/icons/edit.png" onClick="'+onEdit+'" title="edit"></td>';
					if (entry['schp_active']==1) {
						html += '<td class="action"><img class="iconList" width="16" height="16" src="./images/icons/delete.png" onClick="'+onDelete+'" title="delete"></td>';
					} else {
						html += '<td class="action"><img class="iconList" width="16" height="16" src="./images/icons/restore.png" onClick="'+onRestore+'" title="restore"></td>';
						html += '<td class="action"><img class="iconList" width="16" height="16" src="./images/icons/remove.png" onClick="'+onPurge+'" title="remove"></td>';
					}
					html += '</tr>';
					
					if (parseInt(entry['schp_active'])==1) {
						iShow++;
						$('#tblPeriodLine').append(html);
					} else {
						if (user_level.toLowerCase() != 'operator') {
							iShow++;
							$('#tblPeriodLine').append(html);
						}
					}
					
					console.log('ishow => '+iShow);
					if (iShow > 0) {
						$('#spPeriodHead').hide();
					}
				});
				$('#tblPeriodLine tr:first').hide();
			} else {
				$('#spPeriodHead').show();
			}
		},"json");
	}

/*	
	function schedule_view(schID) {
		var URL='./ajax/schedule_view.php?schID='+schID;
		alert(URL);
		$.getJSON(URL,function(data) {
			$('#dvHeadEdit').show();
			schedule_head_view(schID);
			$('#dvPeriod').show();
			schedule_period_list(schID);
			$('#dvLineResult').show();
			schedule_line_list(schID);
			
			if (data.length > 0) { }
		});
	}
*/	
	function schedule_head_edit(schID,schName,DispID) {
		var URL='./ajax/schedule_head_edit.php?schID='+schID+'&schName='+encodeURI(schName)+'&DispID='+DispID;
//		alert(URL);
		$.getJSON(URL,function(data) {
//			alert ('Data =>' +data);
			if (data > 0) {
//				alert('Edit Completed.');
				schedule_edit(schID)
			} else {
//				alert('Edit Failed.');
			}
		});
	}

	function schedule_period_add(schID,schStart,schStop) {
		var URL='./ajax/schedule_period_add.php?schID='+schID+'&schStart='+encodeURI(schStart)+'&schStop='+encodeURI(schStop);
//		alert(URL);
		$.getJSON(URL,function(data) {
//			alert ('Data =>' +data);
			if (data > 0) {
//				alert('Add Period Completed.');
				$('#tblPeriodEdit').hide();
				document.getElementById('PeriodID').value='';
				schedule_period_list(schID);
			} else {
//				alert('Add Period Failed.');
			}
		});
	}
	
		function schedule_period_edit(schID,PeriodID,schStart,schStop) {
		var URL='./ajax/schedule_period_edit.php?PeriodID='+PeriodID+'&schStart='+encodeURI(schStart)+'&schStop='+encodeURI(schStop);
//		alert(URL);
		$.getJSON(URL,function(data) {
//			alert ('Data =>' +data);
			if (data > 0) {
//				alert('Edit Period Completed.');
				$('#tblPeriodEdit').hide();
				document.getElementById('PeriodID').value='';
				schedule_period_list(schID);
			} else {
//				alert('Edit Period Failed.');
			}
		});
	}
	
	function schedule_period_edit_form(PeriodID,dStart,dStop) {
		$('#tblPeriodLine').empty();
		$('#tblPeriodEdit').show();
		document.getElementById('PeriodID').value=PeriodID;
		document.getElementById('txtPeriodStart').value=dStart;
		document.getElementById('txtOldPeriodStart').value=dStart;
		document.getElementById('txtPeriodStop').value=dStop;
		document.getElementById('txtOldPeriodStop').value=dStop;
		document.getElementById('btnPeriodSave').value='Edit';
	}

	function schedule_period_active(schID,PeriodID,Active) {
//		var URL='./ajax/schedule_period_delete.php?schID='+schID+'&PeriodID='+PeriodID;
		var URL='./ajax/schedule_period_active.php';
//		alert(URL);
		var Params={'schID':schID,'PeriodID':PeriodID,'Active':Active};
		var Mode="Delete";
		if (parseInt(Active)==1) Mode="Restore";
		var Display="Do you want to "+Mode+" this period?";
		if (confirm(Display)) {
			$.post(URL,Params,function(data) {
	//			alert ('Data =>' +data);
				if (parseInt(data) > 0) {
//					alert(Mode+' Period Completed.');
					schedule_period_list(schID);
				} else {
//					alert(Mode+' Period Failed.');
				}
			});
		}
	}
	
	function schedule_layout_list(schID) {
		var URL='./ajax/schedule_layout_list.php';

		var user_level=window.parent.document.getElementById('usr_level').value;
		var user_name=window.parent.document.getElementById('usr_name').value;
		var user_id=window.parent.document.getElementById('usr_id').value;

		var Params={'schID':schID};
//		alert(URL);
		$.post(URL,Params,function(data) {
			$('#dvLayout').show();
			$('#tblLayoutLine').empty();
			if (data.length > 0) {
				$.each(data,function(entryIndex,entry) {

					var onView="layout_popup("+entry['tmph_id']+");";
					var onEdit="schedule_layout_popup('ScheduleID',"+entry['schl_layh_id']+","+entry['schl_id']+");";
					var onDelete="schedule_layout_delete("+entry['schl_schh_id']+","+entry['schl_id']+");";
					var onRestore="schedule_layout_restore("+entry['schl_schh_id']+","+entry['schl_id']+");";
					var onPurge="schedule_layout_purge("+entry['schl_id']+");";
					var onTemplate="document.location.href='layout.php?TID="+entry['schl_layh_id']+"';";

					var html='<tr><td>';
					
//					html +='<span style="font-weight:bolder">';
					html +='<span onClick="'+onTemplate+'" title="Edit Template" style="cursor:pointer;">';
					html +='[ '+entry['schl_layh_id']+' ]';
//					html +='<a href="layout.php?TID='+entry['layh_id']+'" style="font-style:none;">';
//					html +='<img class="preview" width="16" height="16" src="./images/icons/edit.png" title="edit"></a></span> ';
					html +='<span style="font-weight:bolder"> Template :</span><span>'+entry['layh_name']+'</span>';
					html +='</span><br>';
					html +='<span onClick="'+onView+'" title="View layout" style="cursor:pointer;">';
					html +='[ '+entry['tmph_id']+' ]';
					html +='<span style="font-weight:bolder"> Layout :</span><span>'+entry['tmph_name']+'</span>';
					html +='</span>';
					html +='</td>';
					var marking=(entry['schl_sunday']==1)?"X":"-";
					html +='<td class="center">'+marking+'</td>';
					marking=(entry['schl_monday']==1)?"X":"-";
					html +='<td class="center">'+marking+'</td>';
					marking=(entry['schl_tuesday']==1)?"X":"-";
					html +='<td class="center">'+marking+'</td>';
					marking=(entry['schl_wednesday']==1)?"X":"-";
					html +='<td class="center">'+marking+'</td>';
					marking=(entry['schl_thursday']==1)?"X":"-";
					html +='<td class="center">'+marking+'</td>';
					marking=(entry['schl_friday']==1)?"X":"-";
					html +='<td class="center">'+marking+'</td>';
					marking=(entry['schl_saturday']==1)?"X":"-";
					html +='<td class="center">'+marking+'</td>';
					html +='<td class="center">'+entry['schl_start']+'</td>';
					html +='<td class="center">'+entry['schl_stop']+'</td>';

					html += '<td class="center">';
					html += '<img class="iconList" src="./images/icons/edit.png" onClick="'+onEdit+'" title="edit"></td>';
					if (entry['schl_active']==1) {
						html +='<td class="center action">';
						html +='<img class="iconList" src="./images/icons/delete.png" onClick="'+onDelete+'" title="delete"></td>';
						html +='<td class="center action">&nbsp;</td>';
					} else {
						html +='<td class="center action">';
						html +='<img class="iconList" src="./images/icons/restore.png" onClick="'+onRestore+'" title="restore"></td>';
						html +='<td class="center action">';
						html +='<img class="iconList" src="./images/icons/purge.png" onClick="'+onPurge+'" title="remove"></td>';
					}
					html += '</tr>';
					
					if (user_level.toLowerCase() == 'operator') {
						if (parseInt(entry['schl_active'])==1) {
							$('#tblLayoutLine').append(html);
						}
					} else {
						$('#tblLayoutLine').append(html);
					}
				});
				
			} else {
				var html="<tr><td align='center' colspan='12'>No Record Found.</td></tr>";
				$('#tblLayoutLine').append(html);
			}
		},"json");
	}

	
	function schedule_layout_delete(schID,LineID) {
//		alert('Restore:: schedule_id=> '+schID+'; line_id => '+LineID);
		var URL='./ajax/schedule_layout_delete.php?LineID='+LineID;
//		alert(URL);
		var Params={};
		var Display="Do you want to delete this layout from schedule?";
		if (confirm(Display)) {
			$.post(URL,Params,function(data) {
				if (parseInt(data) > 0)  {
//					alert('Delete Layout Completed');
					schedule_layout_list(schID);
				} else {
//					alert('Delete Layout Failed');
				}
			});
		}
	}
	
	function schedule_layout_restore(schID,LineID) {
//		alert('Restore:: schedule_id=> '+schID+'; line_id => '+LineID);
		var URL='./ajax/schedule_layout_restore.php?LineID='+LineID;
//		alert(URL);
		var Params={};
		$.post(URL,Params,function(data) {
			if (data > 0)  {
//				alert('Restore Layout Completed');
				schedule_layout_list(schID);
			} else {
//				alert('Restore Layout Failed');
			}
		},"json");
	}

	function schedule_clone(schID) {
		var URL='./ajax/schedule_copy.php';
//		alert(URL);
		var Params={'schID':schID};
		$.post(URL,Params,function(data) {
			if (data > 0)  {
//				alert('Clone Completed');
				schedule_edit(data);
			} else {
//				alert('Clone Failed');
			}
		});
	}
	
	function schedule_layout_purge(LineID) {
		var URL='./ajax/schedule_layout_purge.php';
//		alert(URL);
		var Params={'LineID':LineID};
		$.post(URL,Params,function(data) {
			if (data > 0)  {
//				alert('Delete Successful');
				schedule_edit(data);
			} else {
//				alert('Delete Failed');
			}
		});
	}
	
	function schedule_display_select() {
		var URL="./popup_display.php?DispID=DispID&&DispName=txtDisplay&btnData=btnHeadSave&OldID="+$('#OldDispID').val();
		var Options="width=710,height=280,location=no,menubar=no,resizeable=no,scrollbars=no,status=no,toolbar=no";
		window.open(URL,'popup_display',Options);
	}