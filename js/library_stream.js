	function stream_clear() {
		$('#dvResult').hide();
		$('#dvForm').hide();
	}
	
	function stream_list() {
		stream_clear();
		$('#dvResult').show();

		var user_level=window.parent.document.getElementById('usr_level').value;
		var user_name=window.parent.document.getElementById('usr_name').value;
		var user_id=window.parent.document.getElementById('usr_id').value;

		var URL='./ajax/stream_list.php';
//		alert(URL);
		var Params={};
		$.post(URL,Params,function(data) {
//			alert('Data => '+dump(data));

			$('#tblList_Body').empty();
			var html="<tr id='tr_NoRecord'><td align='center' colspan='6'>No Record Found.</td></tr>";
			$('#tblList_Body').append(html);
			if (data.length > 0) {
				html="";
				$.each(data,function(entryIndex,entry) {

					var onView="window.open('./stream_view.php?stm_id="+entry['stm_id']+"','_streamView');";
					var onEdit="stream_show_edit("+entry['stm_id']+");";
					var onDelete="stream_delete("+entry['stm_id']+");";
					var onRestore="stream_restore("+entry['stm_id']+");";

					var VIcon='<image src="./images/icons/preview.png" width="16" height="16" id="imgView_'+entry['stm_id']+'" class="iconListView" onClick="'+onView+'">';
					var EIcon='<image src="images/icons/edit.png" width="16" height="16" id="imgRename_'+entry['stm_id']+'" class="iconListRename" onClick="'+onEdit+'">';
					var DIcon='<image src="images/icons/delete.png" id="imgDelete_'+entry['stm_id']+'" class="iconListDelete" fid="'+entry['stm_id']+'" onClick="'+onDelete+'">';
					var RIcon='<image src="images/icons/restore.png" id="imgRestore_'+entry['stm_id']+'" class="iconListDelete" fid="'+entry['stm_id']+'" onClick="'+onRestore+'">';


					html ="<tr>";
					html +='<td>'+entry['stm_protocol']+'</td>';
					html +='<td>'+entry['stm_address']+'</td>';
					html +='<td>'+entry['stm_port']+'</td>';
					html +='<td>'+(entry['stm_para1']==null?"-":entry['stm_para1'])+'</td>';
					html +='<td>'+(entry['stm_para2']==null?"-":entry['stm_para2'])+'</td>';

					if (user_level.toString().toLowerCase()!='operator') {
						if (entry['stm_active']) {
							if (parseInt(entry['Used'])>0) {
								html+='<td><div>'+VIcon+' &nbsp; '+EIcon+'</div></td>';							
							} else {
								html+='<td><div>'+VIcon+' &nbsp; '+EIcon+' &nbsp; '+DIcon+' </div></td>';							
							}
						} else {
							if (parseInt(entry['Used'])>0) {
								html+='<td><div>'+VIcon+' &nbsp; R</div></td>';							
							} else {
								html+='<td><div>'+VIcon+' &nbsp; '+DIcon+' &nbsp; '+RIcon+' </div></td>';							
							}
						}
						html+='</tr>';
					} else {
//	Operator User
						if (entry['stm_active']) {
							if (parseInt(entry['Used'])>0) {
								html+='<td><div>'+VIcon+' &nbsp; '+EIcon+'</div></td>';							
							} else {
								html+='<td><div>'+VIcon+' &nbsp; '+EIcon+' &nbsp; '+DIcon+' </div></td>';							
							}
						} else {
							html="";
						}
					}

					$('#tblList_Body').append(html);
				});
			}
			$('#tblList_Body tr:first').hide();
		},"json");
	}
	
	function stream_show_add() {
		stream_clear();
		$('#dvForm').show();
		document.getElementById('StreamID').value='';
		document.getElementById('selProtocol').selectedIndex=0;
		document.getElementById('txtAddress').value='';
		document.getElementById('txtPort').value='';
		document.getElementById('txtPara01').value='';
		document.getElementById('txtPara02').value='';
	}
	
	function stream_show_edit(StreamID) {
		stream_clear();
		$('#dvForm').show();
		var URL='./ajax/stream_info.php';
//		alert(URL);
		var Params={'StreamID':StreamID};
		$.post(URL,Params,function(data) {
//			alert('Data => '+data);
			if (data.length > 0) {
				document.getElementById('StreamID').value=data[0]['stm_id'];
				document.getElementById('selProtocol').value=data[0]['stm_protocol'];
				document.getElementById('txtAddress').value=data[0]['stm_address'];
				document.getElementById('txtPort').value=data[0]['stm_port'];
				document.getElementById('txtPara01').value=data[0]['stm_para1'];
				document.getElementById('txtPara02').value=data[0]['stm_para2'];
			}
		},"json");
	}
	
	function stream_delete(StreamID) {
		var sConfirm='Do you confrim to delete this stream?';
		if (confirm(sConfirm)) {
			var URL='./ajax/stream_active.php';
	//		alert(URL);
			var Params={'StreamID':StreamID,'Active':0};
			$.post(URL,Params,function(data) {
				if (eval(data) > 0) {
					alert('Delete Completed.');
					stream_list();
				} else {
					alert('Delete Failed.');
				}
			});
		}
	}

	function stream_restore(StreamID) {
		var sConfirm='Do you confrim to restore this stream?';
		if (confirm(sConfirm)) {
			var URL='./ajax/stream_active.php';
	//		alert(URL);
			var Params={'StreamID':StreamID,'Active':1};
			$.post(URL,Params,function(data) {
				if (eval(data) > 0) {
					alert('Restore Completed.');
					stream_list();
				} else {
					alert('Restore Failed.');
				}
			});
		}
	}
	
		function stream_save(ID,Protocol,Address,Port,Para1,Para2) {
		var sConfirm='Do you confrim to save this stream?';
		if (document.getElementById(ID).value=='') {
			var sMode="Add";
			var URL='./ajax/stream_add.php';
			var Params={
					'Protocol':encodeURI(document.getElementById(Protocol).options[document.getElementById(Protocol).selectedIndex].text),
					'Address':encodeURI(document.getElementById(Address).value),
					'Port':encodeURI(document.getElementById(Port).value),
					'Para01':encodeURI(document.getElementById(Para1).value),
					'Para02':encodeURI(document.getElementById(Para2).value)
					};
		} else {
			var sMode="Edit";
			var URL='./ajax/stream_edit.php';
			var Params={
					'StreamID':document.getElementById(ID).value,
					'Protocol':encodeURI(document.getElementById(Protocol).options[document.getElementById(Protocol).selectedIndex].text),
					'Address':encodeURI(document.getElementById(Address).value),
					'Port':encodeURI(document.getElementById(Port).value),
					'Para01':encodeURI(document.getElementById(Para1).value),
					'Para02':encodeURI(document.getElementById(Para2).value)
					};
		}
		if (confirm(sConfirm)) {
//			alert(URL);
			$.post(URL,Params,function(data) {
//				prompt('data',data);
				if (eval(data) > 0) {
					alert(sMode+' Completed.');
					stream_list();
				} else {
					alert(sMode+' Failed.');
				}
			});
		}
	}

