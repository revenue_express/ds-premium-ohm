	function purge_() {
		var user_level=window.parent.document.getElementById('usr_level').value;
		var user_name=window.parent.document.getElementById('usr_name').value;
		var user_id=window.parent.document.getElementById('usr_id').value;
		var server_ip=window.parent.document.getElementById('server_ip').value;
		var server_path=window.parent.document.getElementById('server_path').value;
//		var URL='http://'+ClientIP.toString()+':'+PortNo+'/setvolumn/0/';
		var URL="http://"+server_ip+"/"+server_path+"/ajax/purge_.php";
		var Params={};
//		alert("Server IP =>"+server_ip);
		if (user_level.toLowerCase()=='operator') {
//			alert("No Permission to Purge Data");
		} else {
			$.post(URL,Params,function(data) {
				if (data == null) {
					console.log("[Purge  ]:: No data to purge");
//					alert("No data to purge");
					$('#'+Target).empty();
					$('#'+Target).append('<tr><td colspan="3" align="center">No Record Found.</td></tr>');
				} else {
					if (data.length > 0) {
						html="";
						$.each(data,function(entryIndex,entry) {
							html +='<tr>';
							html +='<td>'+entry['ID']+'</td>';
							html +='<td>'+entry['Name']+'</td>';
							html +='<td>'+entry['Result']+'</td>';
							html +='</tr>';
						});
						$('#'+Target).empty();
						$('#'+Target).append(html);
					} else {
						console.log("[Purge ]:: No data to purge ");
//						alert("No data to purge");
						$('#'+Target).empty();
						$('#'+Target).append('<tr><td colspan="3">No Record Found.</td></tr>');
					}
				}
			},"json");
		}
	}

	function purge_schedule(Target) {
		var user_level=window.parent.document.getElementById('usr_level').value;
		var user_name=window.parent.document.getElementById('usr_name').value;
		var user_id=window.parent.document.getElementById('usr_id').value;
		var server_ip=window.parent.document.getElementById('server_ip').value;
		var server_path=window.parent.document.getElementById('server_path').value;
//		var URL='http://'+ClientIP.toString()+':'+PortNo+'/setvolumn/0/';
		var URL="http://"+server_ip+"/"+server+"/ajax/purge_schedule.php";
		var Params={};
//		alert("Server IP =>"+server_ip);
		if (user_level.toLowerCase()=='operator') {
//			alert("No Permission to Purge Data");
		} else {
			$.post(URL,Params,function(data) {
				if (data == null) {
					console.log("[Purge Schedule ]:: No data to purge");
//					alert("No data to purge");
					$('#'+Target).empty();
					$('#'+Target).append('<tr><td colspan="3" align="center">No Record Found.</td></tr>');
				} else {
					if (data.length > 0) {
						html="";
						$.each(data,function(entryIndex,entry) {
							html +='<tr>';
							html +='<td>'+entry['ID']+'</td>';
							html +='<td>'+entry['Name']+'</td>';
							html +='<td>'+entry['Result']+'</td>';
							html +='</tr>';
						});
						$('#'+Target).empty();
						$('#'+Target).append(html);
					} else {
						console.log("[Purge Schedule ]:: No data to purge ");
//						alert("No data to purge");
						$('#'+Target).empty();
						$('#'+Target).append('<tr><td colspan="3">No Record Found.</td></tr>');
					}
				}
			},"json");
		}
	}

	function purge_template(Target) {
		var user_level=window.parent.document.getElementById('usr_level').value;
		var user_name=window.parent.document.getElementById('usr_name').value;
		var user_id=window.parent.document.getElementById('usr_id').value;
		var server_ip=window.parent.document.getElementById('server_ip').value;
		var server_path=window.parent.document.getElementById('server_path').value;
//		var URL='http://'+ClientIP.toString()+':'+PortNo+'/setvolumn/0/';
		var URL="http://"+server_ip+"/"+server_path+"/ajax/purge_template.php";
		var Params={};
//		alert("Server IP =>"+server_ip);
		if (user_level.toLowerCase()=='operator') {
//			alert("No Permission to Purge Data");
		} else {
			$.post(URL,Params,function(data) {
				if (data == null) {
					console.log("[Purge template ]:: No data to purge ");
//					alert("No data to purge");
					$('#'+Target).empty();
					$('#'+Target).append('<tr><td colspan="3" align="center">No Record Found.</td></tr>');
				} else {
					if (data.length > 0) {
						html="";
						$.each(data,function(entryIndex,entry) {
							html +='<tr>';
							html +='<td>'+entry['ID']+'</td>';
							html +='<td>'+entry['Name']+'</td>';
							html +='<td>'+entry['Result']+'</td>';
							html +='</tr>';
						});
						$('#'+Target).empty();
						$('#'+Target).append(html);
					} else {
						console.log("[Purge template ]:: No data to purge ");
//						alert("No data to purge");
						$('#'+Target).empty();
						$('#'+Target).append('<tr><td colspan="3">No Record Found.</td></tr>');
					}
				}
			},"json");
		}
	}

	function purge_playlist(Target,TypeID) {
		var user_level=window.parent.document.getElementById('usr_level').value;
		var user_name=window.parent.document.getElementById('usr_name').value;
		var user_id=window.parent.document.getElementById('usr_id').value;
		var server_ip=window.parent.document.getElementById('server_ip').value;
		var server_path=window.parent.document.getElementById('server_path').value;
//		var URL='http://'+ClientIP.toString()+':'+PortNo+'/setvolumn/0/';
		var Params={};
		switch (parseInt(TypeID)) {
			case 1:
				var URL="http://"+server_ip+"/"+server_path+"/ajax/purge_playlist_image.php";
				break;
			case 2:
				var URL="http://"+server_ip+"/"+server_path+"/ajax/purge_playlist_video.php";
				break;
			case 3:
				var URL="http://"+server_ip+"/"+server_path+"/ajax/purge_playlist_flash.php";
				break;
			case 4:
				var URL="http://"+server_ip+"/"+server_path+"/ajax/purge_playlist_audio.php";
				break;
			case 5:
				var URL="http://"+server_ip+"/"+server_path+"/ajax/purge_playlist_feed.php";
				break;
			case 10:
				var URL="http://"+server_ip+"/"+server_path+"/ajax/purge_playlist_pdf.php";
				break;
			case 12:
				var URL="http://"+server_ip+"/"+server_path+"/ajax/purge_playlist_ppt.php";
				break;
		}
//		alert("Server IP =>"+server_ip);
		if (user_level.toLowerCase()=='operator') {
//			alert("No Permission to Purge Data");
		} else {
			$.post(URL,Params,function(data) {
				if (data == null) {
					console.log("[Purge playlist ]:: No data to purge ");
//					alert("No data to purge");
					$('#'+Target).empty();
					$('#'+Target).append('<tr><td colspan="3" align="center">No Record Found.</td></tr>');
				} else {
					if (data.length > 0) {
						html="";
						$.each(data,function(entryIndex,entry) {
							html +='<tr>';
							html +='<td>'+entry['ID']+'</td>';
							html +='<td>'+entry['Name']+'</td>';
							html +='<td>'+entry['Result']+'</td>';
							html +='</tr>';
						});
						$('#'+Target).empty();
						$('#'+Target).append(html);
					} else {
						console.log("[Purge playlist ]:: No data to purge ");
//						alert("No data to purge");
						$('#'+Target).empty();
						$('#'+Target).append('<tr><td colspan="3">No Record Found.</td></tr>');
					}
				}
			},"json");
		}
	}

	function purge_layout(Target) {
		var user_level=window.parent.document.getElementById('usr_level').value;
		var user_name=window.parent.document.getElementById('usr_name').value;
		var user_id=window.parent.document.getElementById('usr_id').value;
		var server_ip=window.parent.document.getElementById('server_ip').value;
		var server_path=window.parent.document.getElementById('server_path').value;
//		var URL='http://'+ClientIP.toString()+':'+PortNo+'/setvolumn/0/';
		var URL="http://"+server_ip+"/"+server_path+"/ajax/purge_layout.php";
		var Params={};
//		alert("Server IP =>"+server_ip);
		if (user_level.toLowerCase()=='operator') {
//			alert("No Permission to Purge Data");
		} else {
			$.post(URL,Params,function(data) {
				if (data == null) {
					console.log("[Purge Layout ]:: No data to purge");
//					alert("No data to purge");
					$('#'+Target).empty();
					$('#'+Target).append('<tr><td colspan="3" align="center">No Record Found.</td></tr>');
				} else {
					if (data.length > 0) {
						html="";
						$.each(data,function(entryIndex,entry) {
							html +='<tr>';
							html +='<td>'+entry['ID']+'</td>';
							html +='<td>'+entry['Name']+'</td>';
							html +='<td>'+entry['Result']+'</td>';
							html +='</tr>';
						});
						$('#'+Target).empty();
						$('#'+Target).append(html);
					} else {
						console.log("[Purge Layout ]:: No data to purge ");
//						alert("No data to purge");
						$('#'+Target).empty();
						$('#'+Target).append('<tr><td colspan="3">No Record Found.</td></tr>');
					}
				}
			},"json");
		}
	}

	function purge_library(Target,TypeID) {
		var user_level=window.parent.document.getElementById('usr_level').value;
		var user_name=window.parent.document.getElementById('usr_name').value;
		var user_id=window.parent.document.getElementById('usr_id').value;
		var server_ip=window.parent.document.getElementById('server_ip').value;
		var server_path=window.parent.document.getElementById('server_path').value;
//		var URL='http://'+ClientIP.toString()+':'+PortNo+'/setvolumn/0/';
		var URL="http://"+server_ip+"/"+server_path+"/ajax/purge_library.php";
		var Params={'TypeID':parseInt(TypeID)};
//		alert("Server IP =>"+server_ip);
		if (user_level.toLowerCase()=='operator') {
//			alert("No Permission to Purge Data");
		} else {
			$.post(URL,Params,function(data) {
				console.log("[Purge library ] URL= "+URL+" ::" +dump(Params));
				if (data == null) {
					console.log("[Purge library ] "+parseInt(TypeID)+":: No data to purge");
//					alert("No data to purge");
					$('#'+Target).empty();
					$('#'+Target).append('<tr><td colspan="3" align="center">No Record Found.</td></tr>');
				} else {
					if (data.length > 0) {
						console.log("[Purge library ] "+parseInt(TypeID)+":: "+data.length+" Record(s)");
						html="";
						$.each(data,function(entryIndex,entry) {
							html +='<tr>';
							html +='<td>'+entry['ID']+'</td>';
							html +='<td>'+entry['Name']+'</td>';
							html +='<td>'+entry['Result']+'</td>';
							html +='</tr>';
						});
						$('#'+Target).empty();
						$('#'+Target).append(html);
					} else {
						console.log("[Purge library ] "+parseInt(TypeID)+":: No data to purge ");
//						alert("No data to purge");
						$('#'+Target).empty();
						$('#'+Target).append('<tr><td colspan="3" align="center">No Record Found.</td></tr>');
					}
				}
			},"json");
		}
	}
