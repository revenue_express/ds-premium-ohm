	function user_clear_section() {
		$('#dvUserResult').hide();
		$('#dvUserAdd').hide();
		$('#dvUserEdit').hide();
		$('#dvChangePassword').hide();
		$('#dvResetPassword').hide();
	}
	
	function user_show_add() {
		//alert("Add New");
		user_clear_section();
		$('#dvUserAdd').show();

		document.getElementById('UserID').value='';

		document.getElementById('txtName').value='';

		document.getElementById('txtLogin').value='';
		document.getElementById('spAddLogin').innerHTML='';
		document.getElementById('txtPass1').value='';
		document.getElementById('txtPass2').value='';

		document.getElementById('spAddStrength').innerHTML='';
		document.getElementById('spAddSame').innerHTML='';

		document.getElementById('selLevel').value=3;

		document.getElementById('selQuest').index=1;
		document.getElementById('txtQuest').value=document.getElementById('selQuest').value;
		document.getElementById('txtAnswer').value='';

		document.getElementById('txtEmail').value='';
		document.getElementById('btnAddInfo').disabled=true;
		
	}

	function user_show_edit(UserID) {
		//alert("Edit "+UserID);
		user_clear_section();
		$('#dvUserEdit').show();
		user_info(UserID);
	}

	function user_show_change(UserID) {
		alert("Change Pass "+UserID);
		user_clear_section();
		$('#dvChangePassword').show();
		user_info(UserID);
	}


	function user_refresh() {
		user_clear_section();
		$('#dvUserResult').show();
		user_list_all();
	}


	function user_edit_level() {
		selLevel=document.getElementById('selEditLevel').value;
		//alert('You selected '+selLevel+'.');
		document.getElementById('LevelID').value=selLevel;
	}
	
	function user_edit_quest() {
		selQuest=document.getElementById('selEditQuest').value;
		//alert('You selected '+selQuest+'.');
		document.getElementById('txtEditQuest').value=selQuest;
	}
	
	function user_list_all() {
		$('#tblResult_Body').empty();
		URL="./ajax/user_list.php";
//		var UsrLevel=getCookie('usr_level');
//		alert("Cookie :: "+dump(document.cookie));
		var user_level=window.parent.document.getElementById('usr_level').value;
		var user_name=window.parent.document.getElementById('usr_name').value;
		var user_id=window.parent.document.getElementById('usr_id').value;
		var com_id=window.parent.document.getElementById('usr_com_id').value;
		Params={'ComID':com_id};
//		alert("usrLevel =>"+'');
		console.log('user_level => '+user_level);
		console.log('user_name => '+user_name);
		console.log('user_id => '+user_id);
		$.post(URL,Params,function(data){
			if (data.length > 0) {
				var html="";
				$.each(data,function(entryIndex,entry) {
					html=	'<tr>';
					html += '<td>'+entry['usr_login']+'</td>';
					html += '<td>'+entry['usr_name']+'</td>';
					html += '<td>'+entry['lvl_name']+'</td>';
//					html += '<td><input type="button" value="Change" onClick="user_show_change('+entry['usr_id']+');"></td>';
					console.log(entry);
					usrLevel=entry['usr_lvl_id'];
					if (usrLevel == 1) {
						html +="<td>&nbsp;</td><td>&nbsp;</td>";
					} else if (usrLevel == 2)  {
						if (user_level.toLowerCase() == 'sysadmin') {
						html += '<td><input style="margin-left:20px;"  type="button" class="btnKey" onClick="user_show_reset('+entry['usr_id']+');" title="Change password"></td>';
							html += '<td><input type="button" class="btnEdit" value=" " onClick="user_show_edit('+entry['usr_id']+');" title="Edit user information"></td>';
						} else {
							html += '<td>&nbsp;</td>';
							html += '<td><input type="button" class="btnEdit" value=" " onClick="user_show_edit('+entry['usr_id']+');" title="Edit user information"></td>';
						}
					} else {
						html += '<td><input style="margin-left:20px;"  type="button" class="btnKey" onClick="user_show_reset('+entry['usr_id']+');" title="Change password"></td>';
						html += '<td><input type="button" class="btnEdit" value=" " onClick="user_show_edit('+entry['usr_id']+');" title="Edit user information"></td>';
					}
/*
					if (entry['usr_level'].toLowerCase() == "operator") { 
						html += '<td><input style="margin-left:20px;"  type="button" class="btnKey" value=" " onClick="user_show_reset('+entry['usr_id']+');"></td>';
					} else {
						if (entry['usr_level'].toLowerCase() == "administrator") {
							html+="<td>&nbsp;</td>";
						} else {
							if (UsrLevel.toLowerCase() == "administrator") {
						html += '<td><input style="margin-left:20px;"  type="button" class="btnKey" value=" " onClick="user_show_reset('+entry['usr_id']+');"></td>';
							} else {
								html+="<td>&nbsp;</td>";
							}
						}
					}
*/
					if (entry['usr_active']==1) {
						html += '<td><input type="button" class="btnDel" value=" " onClick="user_delete('+entry['usr_id']+');" title="Delete this user"></td>';
					} else {
						html += '<td><input type="button" class="btnRes" value=" " onClick="user_restore('+entry['usr_id']+');" title="Restore this user"></td>';
					}
					if (user_level.toLowerCase() == 'sysadmin') {
						$('#tblResult_Body').append(html);
						$('tr:odd').addClass('even');
					} else {
						if (usrLevel > 2) {
							$('#tblResult_Body').append(html);
							$('tr:odd').addClass('even');
						}
					}
				});
			} else {
				var html='<tr><td class="center">No record founded.</td></tr>';
				$('#tblResult_Body').append(html);
			}
		},"json");
	}
	
	function user_info(usrID) {
		URL="./ajax/user_info.php";
		var Params={'usrID':usrID};
		$.post(URL,Params,function(data){
			if (data.length > 0) {
				document.getElementById('UserID').value=usrID;

				document.getElementById('txtName').value=data[0]['usr_name'];
				document.getElementById('txtEditName').value=data[0]['usr_name'];
				document.getElementById('spChangeName').innerHTML=data[0]['usr_name'];
				document.getElementById('spResetName').innerHTML=data[0]['usr_name'];

				document.getElementById('txtEditLogin').innerHTML=data[0]['usr_login'];
				document.getElementById('spChangeLogin').innerHTML=data[0]['usr_login'];
				document.getElementById('spResetLogin').innerHTML=data[0]['usr_login'];

				document.getElementById('selLevel').value=data[0]['usr_lvl_id'];
				document.getElementById('selEditLevel').value=data[0]['usr_lvl_id'];

				document.getElementById('selQuest').value=data[0]['usr_hint'];
				document.getElementById('txtQuest').value=data[0]['usr_hint'];
				document.getElementById('txtAnswer').value=data[0]['usr_answer'];

				document.getElementById('selEditQuest').value=data[0]['usr_hint'];
				document.getElementById('txtEditQuest').value=data[0]['usr_hint'];
				document.getElementById('txtEditAnswer').value=data[0]['usr_answer'];

				document.getElementById('txtEmail').value=data[0]['usr_email'];
				document.getElementById('txtEditEmail').value=data[0]['usr_email'];
				
				document.getElementById('btnAddInfo').disabled=true;
				document.getElementById('btnEditInfo').disabled=true;
				document.getElementById('btnChangeInfo').disabled=true;
				document.getElementById('btnResetInfo').disabled=true;
				
				document.getElementById('txtEmail').value=data[0]['usr_email'];
				document.getElementById('txtEditEmail').value=data[0]['usr_email'];

				document.getElementById('txtPass1').value='';
				document.getElementById('txtPass2').value='';
				document.getElementById('txtOldPass').value='';
				document.getElementById('txtNewPass1').value='';
				document.getElementById('txtNewPass2').value='';
				document.getElementById('txtResetPass1').value='';
				document.getElementById('txtResetPass2').value='';

			} else {
				alert("Can not Find User Information");
			}
		},"json");
	}

	function user_edit_check() {
		var bReturn=true;
		var Display="";
		var rValue=true;
		var user_level=window.parent.document.getElementById('usr_level').value;
		var user_name=window.parent.document.getElementById('usr_name').value;
		var user_id=window.parent.document.getElementById('usr_id').value;

		rValue=checkEqual('txtName','txtEditName');
		Display="txtName rValue =>";
		Display+=(rValue)?"true":"false";
		bReturn=bReturn && rValue;
		if (user_level.toLowerCase() == 'administrator') {
			rValue=checkEqual('selLevel','selEditLevel');
			Display +="\nselLevel rValue =>";
			Display+=(rValue)?"true":"false";
			bReturn=bReturn && rValue;
		}
		rValue=checkEqual('txtQuest','txtEditQuest');
		Display +="\ntxtQuest rValue =>";
		Display+=(rValue)?"true":"false";
		bReturn=bReturn && rValue;
		rValue=checkEqual('txtAnswer','txtEditAnswer');
		Display +="\ntxtAnswer rValue =>";
		Display+=(rValue)?"true":"false";
		bReturn=bReturn && rValue;
		rValue=checkEqual('txtEmail','txtEditEmail');
		Display +="\ntxtEmail rValue =>";
		Display+=(rValue)?"true":"false";
		bReturn=bReturn && rValue;
		Display +="\nbReturn =>";
		Display+=(bReturn)?"true":"false";
//		alert(Display);
		document.getElementById('btnEditInfo').disabled = bReturn;		
	}
	
	function user_edit() {
//		alert("user_edit");
		URL="./ajax/user_edit.php";
		var user_level=window.parent.document.getElementById('usr_level').value;
		var user_name=window.parent.document.getElementById('usr_name').value;
		var user_id=window.parent.document.getElementById('usr_id').value;
		var Params={
			'usrID':document.getElementById('UserID').value,
			'usrLevel':document.getElementById('selEditLevel').value,
			'usrName':document.getElementById('txtEditName').value,
			'usrQuest':document.getElementById('txtEditQuest').value,
			'usrAnswer':document.getElementById('txtEditAnswer').value,
			'usrEmail':document.getElementById('txtEditEmail').value
			};
//			alert(Params);
		$.post(URL,Params,function(data){
//			alert("data =>"+data);
			if (data != "0") {
				alert("Edit Successful");
				if (user_level.toLowerCase() == 'administrator') {
					user_refresh();
				}
			} else {
				alert("Edit Failed");
				user_show_edit(document.getElementById('UserID').value);
			}
		});
	}
		
	function user_add_check() {
		var bReturn=true;
		var Display="";
		var rValue=true;
		rValue=($('#txtName').val().length>=3);
		bReturn=bReturn && rValue;
		rValue=($('#txtLogin').val().length>=3);
		bReturn=bReturn && rValue;
		rValue=($('#txtQuest').val().length>=3);
		bReturn=bReturn && rValue;
		rValue=($('#txtAnswer').val().length>=3);
		bReturn=bReturn && rValue;
		rValue=($('#txtEmail').val().length>=5);
		bReturn=bReturn && rValue;
//		alert(Display);
		if (bReturn) {
			document.getElementById('btnAddInfo').disabled = false;
		} else {
			document.getElementById('btnAddInfo').disabled = true;
		}
	}
	
	function user_add_level() {
		selLevel=document.getElementById('selLevel').value;
		//alert('You selected '+selLevel+'.');
		document.getElementById('LevelID').value=selLevel;
	}
	
	function user_add_quest() {
		selQuest=document.getElementById('selQuest').value;
		//alert('You selected '+selQuest+'.');
		document.getElementById('txtQuest').value=selQuest;
	}

	function user_check_login() {
		URL="./ajax/user_check_login.php";
		var Params={'usrLogin':$('#txtLogin').val() };
		$.post(URL,Params,function(data){
			if (data > 0) {
				$('#spAddLogin').html("Account Exist.");
				$('#spAddLogin').addClass("wrong");
			} else {
				$('#spAddLogin').html("Account Avaiabled");
				$('#spAddLogin').addClass("correct");
			}
		});

	}
	
	function user_add_strength() {
		if ($('#txtPass1').val().length <= 4) {
		$('#spAddStrength').html("Easy");
		} else if ($('#txtPass1').val().length <= 6) {
			$('#spAddStrength').html("Normal");
		} else if ($('#txtPass1').val().length <= 9) {
			$('#spAddStrength').html("Hard");
		} else {
			$('#spAddStrength').html("Expert");
		}
	}
	
	function user_add_samepass() {
		if (checkEqual('txtPass1','txtPass2')) {
			$('#spAddSame').html("Matched.");
		} else {
			$('#spAddSame').html("No Matched");
		}
	}
	
	function user_add() {
//		alert("user_add");
		URL="./ajax/user_add.php";
		var Params={
			'usrLogin':document.getElementById('txtLogin').value,
			'usrName':document.getElementById('txtName').value,
			'usrPass':document.getElementById('txtPass1').value,
			'usrLevel':document.getElementById('selLevel').value,
			'usrQuest':document.getElementById('txtQuest').value,
			'usrAnswer':document.getElementById('txtAnswer').value,
			'usrEmail':document.getElementById('txtEmail').value
			};
//			alert(Params);
		$.post(URL,Params,function(data){
//			alert("data =>"+data);
			if (data != "0") {
				alert("Add Successful");
				user_refresh();
			} else {
				alert("Add Failed");
			}
		});
	}
	
	function user_delete(UserID) {
		//alert("Delete "+UserID);
		URL="./ajax/user_active.php";
		var Params={ 'usrID':UserID, 'usrActive':0 };
		$.post(URL,Params,function(data){
//			alert("data =>"+data);
			if (data != "0") {
				alert("delete Successful");
				user_refresh();
			} else {
				alert("Delete Failed");
			}
		});
	}

	function user_restore(UserID) {
		//alert("Delete "+UserID);
		URL="./ajax/user_active.php";
		var Params={ 'usrID':UserID, 'usrActive':1 };
		$.post(URL,Params,function(data){
//			alert("data =>"+data);
			if (data != "0") {
				alert("restore Successful");
				user_refresh();
			} else {
				alert("restore Failed");
			}
		});
	}

	function user_change_strength() {
		if ($('#txtNewPass1').val().length == 0) {
			$('#spChangeStrength').html("&nbsp; &nbsp; &nbsp;");
		} else if ($('#txtNewPass1').val().length <= 4) {
			$('#spChangeStrength').html("Easy");
		} else if ($('#txtNewPass1').val().length <= 6) {
			$('#spChangeStrength').html("Normal");
		} else if ($('#txtNewPass1').val().length <= 9) {
			$('#spChangeStrength').html("Hard");
		} else {
			$('#spChangeStrength').html("Expert");
		}
	}
	
	function user_change_samepass() {
		if (checkEqual('txtNewPass1','txtNewPass2')) {
			$('#spChangeSame').html("Matched.");
		} else {
			$('#spChangeSame').html("No Matched");
		}
	}

	function user_change_check() {
		var bReturn=true;
		var Display="";
		var rValue=true;
		rValue=($('#txtOldPass').val().length>=3);
		bReturn=bReturn && rValue;
		rValue=($('#txtNewPass1').val().length>=3);
		bReturn=bReturn && rValue;
		rValue=($('#txtNewPass2').val().length>=3);
		bReturn=bReturn && rValue;
		rValue=checkEqual('txtNewPass1','txtNewPass2');
		bReturn=bReturn && rValue;
//		alert(Display);
		if (bReturn) {
			document.getElementById('btnChangeInfo').disabled = false;
		} else {
			document.getElementById('btnChangeInfo').disabled = true;
		}
	}

	function user_change_password() {
		//alert("user_change_password");
//		alert("user_add");
		URL="./ajax/user_change_password.php";
		var Params={
			'usrID':document.getElementById('UserID').value,
			'usrOldPass':document.getElementById('txtOldPass').value,
			'usrNewPass':document.getElementById('txtNewPass1').value,
			};
//			alert(Params);
		$.post(URL,Params,function(data){
			//alert("data =>"+data);
			if (data == "1") {
				alert("Change Successful");
				if (Mode.toLowerCase() == 'administrator') {
					user_refresh();
				}
			} else if (data == "-1") {
				alert("Old Password Not Corrected");
			} else {
				alert("Change Failed");
			}
		});
	}
	
	function user_show_reset(UserID) {
		//alert("Reset "+UserID);
		user_clear_section();
		$('#dvResetPassword').show();
		user_info(UserID);
	}

	function user_reset_strength() {
		if ($('#txtResetPass1').val().length == 0) {
			$('#spResetStrength').html("&nbsp; &nbsp; &nbsp;");
		} else if ($('#txtResetPass1').val().length <= 6) {
			$('#spResetStrength').html("Normal");
		} else if ($('#txtRestPass1').val().length <= 9) {
			$('#spResetStrength').html("Hard");
		} else {
			$('#spResetStrength').html("Expert");
		}
	}
	
	function user_reset_samepass() {
		if (checkEqual('txtResetPass1','txtResetPass2')) {
			$('#spResetSame').html("Matched.");
		} else {
			$('#spResetSame').html("No Matched");
		}
	}

	function user_reset_check() {
		var bReturn=true;
		var Display="";
		var rValue=true;
		rValue=($('#txtResetPass1').val().length>=3);
		bReturn=bReturn && rValue;
		rValue=($('#txtResetPass2').val().length>=3);
		bReturn=bReturn && rValue;
		rValue=checkEqual('txtResetPass1','txtResetPass2');
		bReturn=bReturn && rValue;
//		alert(Display);
		if (bReturn) {
			document.getElementById('btnResetInfo').disabled = false;
		} else {
			document.getElementById('btnResetInfo').disabled = true;
		}
	}

	function user_reset_password() {
		alert(" user_reset_password");
//		alert("user_add");
		URL="./ajax/user_reset_password.php";
		var Params={
			'usrID':document.getElementById('UserID').value,
			'usrPass':document.getElementById('txtResetPass1').value,
			};
//			alert(Params);
		$.post(URL,Params,function(data){
			alert("data =>"+data);
			if (data != "0") {
				alert("Reset Successful");
				if (Mode.toLowerCase() == 'administrator') {
					user_refresh();
				}
			} else {
				alert("Reset Failed");
			}
		});
	}

