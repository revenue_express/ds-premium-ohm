<?
	ob_start();
	session_cache_expire(1);
	$cache_expire = session_cache_expire();
	session_save_path('/var/www/neptune/session/');
	session_start();
	session_destroy();
	session_start();
	$DEBUG=FALSE;
	if(! isset($txtCompany)) $txtCompany='ohm';
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<?
	$bLogin=false;
	$sAlert=NULL;
//	$DEBUG=FALSE;

	$Path=split('/',$_SERVER['SCRIPT_FILENAME']);
	if ( (isset($_REQUEST['txtLogin'])) && (isset($_REQUEST['txtPassword'])) ){
		$Login=$_REQUEST['txtLogin'];
		$Pass=$_REQUEST['txtPassword'];
		$Company=strtolower($_REQUEST['txtCompany']);
		if ($_REQUEST['txtCompany'] == NULL) { 
			$Company="0"; 
		}
//		echo "<br>";
		include("./includes/db_config.inc.php");
		include("./functions/authen.func.php");
//		echo "<br>";
		$UserInfo=json_decode(authen_check_pass($Login,$Pass,$Company),true);
//		echo "<br>";
//		$DEBUG=true;
//		$Path=split('/',$_SERVER['SCRIPT_FILENAME']);
		if ($DEBUG) { echo "<pre>"; print_r($UserInfo); echo "</pre>"; }
		if (count($UserInfo) > 0) {
			if (strtolower($UserInfo['Result'])=="successful") {
				$bLogin=true;
				$_SESSION['usr_id']=$UserInfo['Data']['usr_id'];
				$_SESSION['usr_login']=$UserInfo['Data']['usr_login'];
				$_SESSION['usr_name']=$UserInfo['Data']['usr_name'];
				$_SESSION['usr_level']=$UserInfo['Data']['lvl_name'];
				$_SESSION['lvl_weight']=$UserInfo['Data']['lvl_weight'];
				$_SESSION['usr_com_id']=$UserInfo['Data']['usr_com_id'];
				$_SESSION['usr_company']=$UserInfo['Data']['com_name'];
//				$_SESSION['srv_path']=$Path[count($Path)-2];
				$_SESSION['srv_path']=$Path[count($Path)-2];
		?>
        <script>
        	$.session("ses_test","1234");
			alert("ses_test = "+$.session("ses_test"));
        </script>
        <?
			} else {
				$sAlert=$UserInfo['Result'];
			}
		}
		load_config();
	}
//	echo "Path =>".$Path;
?>

<html>
<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />     
    <title>Log in · Info Express</title>
<?	include("./javascript.php");?>
    <script type="text/javascript" src="jquery/jquery.session.js"></script>    
    <script type="text/javascript">
        $(document).ready(function(){
            $.session("ses_header","show");
        });
    </script>    
</head>
<style type="text/css">
body{ 	
	overflow:auto; 
}
</style>
<body>
<script language="javascript">
<?	if ($bLogin) {?>
	window.open("./main.php","_self");
<?	
} else {
	if (! is_null($sAlert)) {
		echo sprintf("\nalert('%s');",$sAlert);
	}
}
?>
</script>
<div id="login">
	<!-- 
    	box in login for margin 200px; 
    	box-45 for width 470px;
    	boxin for shadow
    -->
    <div class="box boxLogin boxin bglogin">
        <div class="header">
            <img style="margin-top:10px;" src="images/infoexpress.png" width="154" height="40">
        </div>
        <form class="table" action="" method="post" style="margin-top:-15px;">
            <div class="msg-info">
                <p>ลงชื่อเข้าใช้งาน</p>
            </div>
            <div class="dvtr">
                <div class="dvtd_left top5 w-25"><label for="txtLogin">ชื่อผู้ใช้ : </label></div>
                <div class="dvtd_right"><input class="txt" type="text" id="txtLogin" name="txtLogin" /></div>
          	</div>
            <div class="dvtr">
                <div class="dvtd_left top5 w-25"><label for="txtPassword">รหัสผ่าน : </label></div>
                <div class="dvtd_right"><input class="txt pwd" type="password" id="txtPassword" name="txtPassword" /></div>
            </div>
            <div class="dvtr">
                <div class="dvtd_left top5 w-25"><label for="txtCompany">รหัสบริษัท : </label></div>
                <div class="dvtd_right"><input class="txt" type="text" id="txtCompany" name="txtCompany" value='<?php echo $txtCompany;?>'/></div>
          	</div>
            <div class="dvtr right">
                <div style="padding-right:52px; padding-top:5px;">
                	<input class="btnSky" style="font-size:19px;" type="submit" value="ลงชื่อเข้าใช้งาน" />
                </div>
           	</div>
        </form>
    </div>
</div>
<?php
	$_RETURN.=sprintf("<br> session_save_path => '%s'",session_save_path());
	$_RETURN.=sprintf("<br> session_id => '%s'",session_id());
	foreach ($_SESSION as $Key => $Value) { 
		$_RETURN.=sprintf("<br> session %s => '%s'",$Key,addslashes(urldecode($Value)));
		$_SESSION[$Key]=addslashes(urldecode($Value)); 
	}
	foreach ($_REQUEST as $Key => $Value) { 
		$_RETURN.=sprintf("<br> request %s => '%s'",$Key,addslashes(urldecode($Value)));
		$$Key=addslashes(urldecode($Value)); 
	}
	foreach ($_COOKIE as $Key => $Value) {
		$_RETURN.=sprintf("<br> cookie %s => '%s'",$Key,addslashes(urldecode($Value)));
		setcookie($Key,addslashes(urldecode($Value)),time()-3600); 
	}
	echo $_RETURN;
?>

</body>
</html>