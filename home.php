<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<? 
	$DEBUG=FALSE;
	include("./includes/db_config.inc.php");
//	include("./includes/sys_config.inc.php");
	include("./functions/group.func.php");
	include("./functions/client.func.php");
	$onKeyPress="KeyPress('txtMessage',5,'btnMessage')";
?>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<?	include("./javascript.php");?>
<link rel="stylesheet" href="css/custom-theme/jquery-ui-1.8.23.custom.css">
<link rel="stylesheet" href="css/StyleSheet.css">
<script type="text/javascript" src="./js/home.js"></script>
</head>
<body>
<script type="text/javascript">
	$('document').ready(function() {
		$('#tabs').tabs();
		$('#dvMonitor').dialog({ autoOpen: false });
		$('#dvMonitor').dialog("option","width",750);
		$('#dvMonitor').dialog("option","height",275);
		$('#dvMonitor').dialog("option","resizable",false);
	});
</script>
<div id="dvMonitor" >
  <div class="dvtr w-100 top10">
  	<div class="dvtd_right w-10">Player : </div>
    <div class="demo dvtd_left w-80 left">
		<input class="btnSky" type="button" id="btnCapture" onClick="client_capture();" value="Capture">
		<input class="btnSky" type="button" id="btnRestart" onClick="client_restart();" value="Restart">
		<input class="btnSky" type="button" id="btnShutdown" onClick="client_shutdown();" value="Shutdown">
    </div>
  </div>
  <div class="dvtr w-100 top10">
  	<div class="dvtd_right w-10">Template : </div>
    <div class="demo dvtd_left w-80 left">
		<input class="btnSky" type="button" id="btnDefault" onClick="client_default();" value="Default">
		<input class="btnSky" type="button" id="btnRestore" onClick="client_restore();" value="Restore">
		</div>
  </div>
  <div class="dvtr w-100 top10">
  	<div class="dvtd_right w-10 top5">Volumn : </div>
    <div class="demo dvtd_left w-75 left">
      <span class="demo">
      <span style="font-family:'Lucida Console', Monaco, monospace; font-size:14px;">
     <select id="selLoud">
      <? for ($iCount=1; $iCount<=5; $iCount++) { 
//				$num="          ".(string)$iCount;
//				$data=substr($num,strlen($num)-3,3);
//				$data=str_replace(' ','&nbsp;',$data);
//				echo sprintf("\n<option  value='%d'>%s %%</option>",$iCount,$data); 
				echo sprintf("\n<option  value='%d'>%d %%</option>",$iCount,($iCount*20)); 
			}?>
      </select>
      </span>
      <input class="btnSky" type="button" id="btnLoud" onClick="client_loud();" value="Save">
      </span>
      <span><input class="btnSky" type="button" id="btnMute" onClick="client_mute();" value="Mute"></span>
    </div>
  </div>
  <div class="dvtr w-100 top10">
  	<div class="dvtd_right w-100">
    	<span class="demo">
      <span class="top5">	<input type="text" class="txt" id='txtMessage' size="60" onKeyPress="<?=$onKeyPress;?>"></span>
    	<span><input class="btnSky" type="button" id="btnMessage" onClick="client_urgent();" value="Send Urgent" disabled></span>	
    	<span><input class="btnSky" type="button" id="btnResetMessage" onClick="client_reset_urgent();" value="Reset Urgent"></span>	
    	</span>
    </div>
  </div>
  <div class="dvtr w-100 top10">
  	<div class="dvtd_right w-20 top5">Download Period : </div>
    <div class="demo dvtd_left w-75 left">
      <span class="demo">
      <span style="font-family:'Lucida Console', Monaco, monospace; font-size:14px;">
     <select id="selStart">
      <? for ($iCount=0; $iCount<=23; $iCount++) { 
//				$num="          ".(string)$iCount;
//				$data=substr($num,strlen($num)-3,3);
//				$data=str_replace(' ','&nbsp;',$data);
				echo sprintf("\n<option  value='%d'>%02d:00</option>",$iCount,$iCount); 
//				echo sprintf("\n<option  value='%d'>%d %%</option>",$iCount,($iCount*20)); 
			}?>
      </select>
		- 
     <select id="selStop">
      <? for ($iCount=0; $iCount<=23; $iCount++) { 
//				$num="          ".(string)$iCount;
//				$data=substr($num,strlen($num)-3,3);
//				$data=str_replace(' ','&nbsp;',$data);
				echo sprintf("\n<option  value='%d'>%02d:00</option>",$iCount,$iCount); 
//				echo sprintf("\n<option  value='%d'>%d %%</option>",$iCount,($iCount*20)); 
			}?>
      </select>
      </span>
      </span>
      <span><input class="btnSky" type="button" id="btnTimer" onClick="client_timer();" value="Send Period"></span>
    </div>
  </div>
  <input type="hidden" id="ServerIP" size="2" value="<?=$_SERVER['SERVER_ADDR'];?>">
  <input type="hidden" id="ClientID" size="2">
  <input type="hidden" id="ClientIP" size="16">
  <input type="hidden" id="PortNo" size="2" value="3001">
</div>
<?
	$GroupMenu="";
	$Tabs="";

/*
	$ThumbData=file_get_contents("http://192.168.1.107:3002/getscreenthumb/");
	for($iCount=0; $iCount < 1000; $iCount++) {}
	$ThumbName=sprintf("%s.png",'107');
	$ThumbPath="./data/images";
	$ThumbFile=sprintf("%s/%s",$ThumbPath,$ThumbName);
	file_put_contents($ThumbFile,$ThumbData);
*/
	$GroupList=json_decode(home_group_list($ComID),true);
	$iRun=1;
	$CmdList=array();
	for ($iGroup=0; $iGroup < count($GroupList); $iGroup++) {
//		$GroupMenu.=$GroupList[$iGroup];
		$GroupMenu.=sprintf("\n<li><a href='#tabs-%d'>Group of '%s'</a></li>",$GroupList[$iGroup]['grp_id'],$GroupList[$iGroup]['grp_name']);
		$TabDisplay=sprintf("\n<div id='tabs-%d'><h3>Group '%s'</h3>",$GroupList[$iGroup]['grp_id'],$GroupList[$iGroup]['grp_name']);
		$TabDisplay.="<table class='w-100 menu' cellspacing='0'>";
		$ClientList=json_decode(client_list($ComID),true);
		$iCount=0;
		$GroupID=$GroupList[$iGroup]['grp_id'];
		if (count($ClientList) > 0) {
			for ($iClient=0; $iClient < count($ClientList); $iClient++) {
				if ($DEBUG) { echo sprintf("Group=%d <pre>",$GroupList[$iGroup]['grp_id']); print_r($ClientList[$iClient]); echo "</pre>"; }
				if ($ClientList[$iClient]['cli_active']==0) continue;
				$ClientID=$ClientList[$iClient]['cli_id'];
				$ClientGroup=$ClientList[$iClient]['cli_grp_id'];
				$ClientName=$ClientList[$iClient]['cli_name'];
				$ClientIP=$ClientList[$iClient]['cli_address'];

				$ThumbName=sprintf("%d.png",$ClientID);
				$ThumbPath="./data/images";
				$ThumbFile=sprintf("%s/%s",$ThumbPath,$ThumbName);
				if (! file_exists($ThumbFile)) {$ThumbFile = "./images/no-file.png"; }

				if ($ClientGroup == $GroupID) {
					$nameCfg1=sprintf("imgSchedule_%d",$ClientID);
					$nameCfg2=sprintf("imgConfig_%d",$ClientID);
					$nameCfg3=sprintf("imgStatus_%d",$ClientID);
					if ($DEBUG) echo "...IN Group\n";
					if ($iCount % 4 ==0) {
						$TabDisplay.='<tr valign="top">';
					}

					list($ImgWidth,$ImgHeight)=getimagesize($ThumbFile);
					$imgCfg1='./images/calendar1.png'; 
					$imgCfg2='./images/system_config_services.png';
//					$Result=(($iClient % 2) == 1);
//					if ($Result== false) {
//					$URL=sprintf("http://%s/live.php",$ClientIP);
//					$sc = stream_context_create(array( 'http' => array( 'timeout' => 1) ) );
//					$Result=file_get_contents($URL,0,$sc);
					if (empty($Result)) {
						$imgCfg3='./images/btn_offline.png';
						$txtCfg3=sprintf("'%s' is offline.",$ClientList[$iClient]['cli_name']);
					} else {
						$imgCfg3='./images/btn_online.png';
						$txtCfg3=sprintf("'%s' is online.",$ClientList[$iClient]['cli_name']);
					}
					$cssCfg1= 'margin-left:4px; cursor:pointer; width:16px; height=16px;';
					$cssCfg2= 'margin-left:4px; cursor:pointer; width:16px; height=16px;';
					$cssCfg3= 'margin-left:4px; cursor:pointer; width:16px; height=16px;';
					
					$txtCfg1=sprintf("View schedule of this client '%s'.",$ClientList[$iClient]['cli_name']);
					$txtCfg2=sprintf("Config this client '%s'.",$ClientList[$iClient]['cli_name']);

					$onConfig1Click=sprintf("window.open('schedule_client.php?ClientID=%d','_self');",$ClientID);
					$onConfig2Click=sprintf("showDialog('dvMonitor',%d,'%s');",$ClientID,$ClientName);
					$onConfig3Click=sprintf("client_status('%s',%d,'%s','%s');",$nameCfg3,$ClientID,$ClientIP,$ClientName);
					$CmdList[]=array("name"=>$nameCfg3,"function"=>$onConfig3Click);

//					$sDisplay=sprintf("<div style='width: %dpx;' class='boxin'>",($ImgWidth<228)?228:$ImgWidth);
					$sDisplay=sprintf("<div style='width: 100%;' class='boxin'>",($ImgWidth<228)?228:$ImgWidth);
					$sDisplay.='<div class="header">';
					$sDisplay.='<span style="font-size:15px;">'.$ClientList[$iClient]['cli_name'].'</span>';
					$sDisplay.='<span style="float:right; margin-right:5px;">';
					$sDisplay.=sprintf('<img id="%s" src="%s" style="%s" onClick="%s" title="%s">',$nameCfg1,$imgCfg1,$cssCfg1,$onConfig1Click,$txtCfg1);
					$sDisplay.=sprintf('<img id="%s" src="%s" style="%s" onClick="%s" title="%s">',$nameCfg2,$imgCfg2,$cssCfg2,$onConfig2Click,$txtCfg2);
					$sDisplay.=sprintf('<img id="%s" src="%s" style="%s" onClick="%s" title="%s">',$nameCfg3,$imgCfg3,$cssCfg3,$onConfig3Click,$txtCfg3);
					$sDisplay.='</span></div><div style="margin:5px;">';
					$sDisplay.='<img id="imgClient_'.$ClientID.'" src="'.$ThumbFile.'" width="100%">';
//					$sDisplay.='<img id="imgClient_'.$ClientID.'" src="'.$ThumbFile.' wi">';
//					$sDisplay.=sprintf("<br> %d x %d ",$ImgWidth,$ImgHeight);
					$sDisplay.='</div>';
					$sDisplay.='</div>';

					if ($iCount % 4 == 3) { 
						$TabDisplay.=sprintf("\n<td class='top15 w-25'>%s</td></tr>",$sDisplay);
					} else {
						$TabDisplay.=sprintf("\n<td class='top15 w-25'>%s</td>",$sDisplay);
					}
					$iCount++;
				} else {
					if ($DEBUG) echo "...No Group\n";
				}
			}
			while ($iCount % 4 <> 3) {
				$TabDisplay.="<td class='top15 w-25'></td>";
				$iCount++;
			}
			$TabDisplay.="\n</tr>";
		} else {
			$TabDisplay.="&nbsp;";
		}
		$TabDisplay.="\n</table>\n</div>";
		$Tabs.=$TabDisplay;
		if ($DEBUG) { echo sprintf("\n%s\n",$TabDisplay); }
	}
?>
 <div class="w-100 demo nohover">
	<div id="tabs">
<?	echo sprintf("<ul>%s\n</ul>",$GroupMenu); ?>
<?	echo $Tabs; ?>	
	</div>
</div>
<script language="javascript">
	$('document').ready(function() {
<?	
	for ($iCon=0; $iCon<count($CmdList); $iCon++) {
//		echo sprintf("\n%s", $CmdList[$iCon]['function']);
		echo sprintf("\nsetTimeout(\"%s\",5000);",$CmdList[$iCon]['function']);
	}
?>
	});
</script>
</body>
</html>