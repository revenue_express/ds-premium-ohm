<? 
	include("./includes/db_config.inc.php");
//	include("./includes/sys_config.inc.php");
	include("./functions/template.func.php");
	include("./functions/template_cover.func.php");
	
	$Header=json_decode(template_head_view($tmph_id),true);
	if ($DEBUG) { echo "Header<pre>"; print_r($Header); echo "</pre>"; }
	$ItemData=json_decode(template_line_list($ComID,$tmph_id,$USER['usr_id']),true);
	if ($DEBUG) { echo "ItemData<pre>"; print_r($ItemData); echo "</pre>"; }
	$CoverData=json_decode(template_cover_info($tmph_id),true);
	if ($DEBUG) { echo "CoverData<pre>"; print_r($CoverData); echo "</pre>"; }
	
	//echo "tmph_id => ".$_GET['tmph_id']; echo "<pre>"; print_r($ItemData); echo "</pre><br>";
	$bgcolor = array("#CF0","#FC0","#39F","#96C","#F9C","#999","#630","#063","#FFF","#FCC","#FFC","#366");
	if ($DEBUG) {
		echo "<br>Header<pre>"; print_r($Header); echo "</pre>";
		echo "<br>ItemData<pre>"; print_r($ItemData); echo "</pre>";
		echo "<br>CoverData<pre>"; print_r($CoverData); echo "</pre>";
	}

	$disp_width=$Header[0]['disp_width'];
	$disp_height=$Header[0]['disp_height'];
	$back_size[0]=$disp_width;
	$back_size[1]=$disp_height;
	$disp_backg=NULL;
	if (count($CoverData) > 0) {
		$disp_backg=sprintf("./%s/covers/%s",$DataPath,$CoverData[0]['file_sname']);
		if (file_exists($disp_backg)) {
			$back_size=getimagesize($disp_backg);
		}
	}
?>

<style>
	body{ margin:0px; overflow:hidden; }
</style>

<script type="text/javascript" src="js/jquery-1.7.2.min.js"></script>
<script type="text/javascript" src="js/jquery-ui-1.8.21.custom.min.js"></script>
<script>
	$(document).ready(function() {

		var screen_height = screen.height;// document.body.offsetHeight;
		var screen_width = screen.width;
	
		var template_height =  <?=$disp_height;?>;
		var template_width =  <?=$disp_width;?>;
		
		var disp_height = template_height;
		var disp_width = template_width;
		
		var back_height =template_height;
		var back_width= template_width;

		//var padding_width = window.outerWidth - document.body.clientWidth;
		//var padding_height = window.outerHeight - document.body.clientHeight;
		//alert(padding_width+"/"+padding_height);
		var iMul=1;
			
		while ((iMul < 12) && ((disp_height >= screen_height) || (disp_width >= screen_width)) ) {
			iMul++;
			disp_height=template_height / iMul;
			disp_width=template_width / iMul; 
		}
		
		back_height = <?=$back_size[1];?>/ iMul;		
		back_width = <?=$back_size[0];?>/ iMul;		

		console.log("disp_width x disp_height => "+disp_width+","+disp_height);
		console.log("back_width x back_height => "+back_width+","+back_height);
		window.resizeTo(disp_width+12,disp_height+64);
		
		//document.getElementById('dvCover').innerHTML="<img id=\"covers\" src=\"./data/covers/<//?=$disp_backg?>\" height="+window.outerHeight-64+" width="+window.outerWidth-16+">";
		//document.getElementById('covers').src="./data/covers/<?//=$disp_backg?>";
		//document.getElementById('covers').style.height=window.outerHeight-64;
		//document.getElementById('covers').style.width=window.outerWidth-16;	
		<? if(file_exists($disp_backg)){?>
//		document.getElementById('dvCover').innerHTML = "<img id=\"covers\" src=\"<?=$disp_backg?>\" width="+(window.outerWidth-16)+" height="+(window.outerHeight-64)+">";
		document.getElementById('dvCover').innerHTML = "<img id=\"covers\" src=\"<?=$disp_backg?>\" width="+back_width+" height="+back_height+">";
//		document.getElementById('dvCover').innerHTML = "<img id=\"covers\" src=\"<?=$disp_backg?>\" width="+back_width+" height="+back_height+">";
		document.getElementById('dvCover').style.zIndex=99;
		<? } ?>
	<?
		for($j=0; $j<count($ItemData); $j++){
			if ($ItemData[$j]['tmpl_active']==0) continue;
			
			$Line=json_decode(template_option_list($ItemData[$j]['tmpl_id']),true);


			$iMax=count($Line);
			$HeadIdx=-1;
			$FooterIdx=-1;
			if ($iMax > 0) {
				for ($iOption=0; $iOption<$iMax; $iOption++) {
					if (($Line[$iOption]['tplo_name'] == 'header') && (($Line[$iOption]['tplo_active'] == 1))) $HeadIdx=$iOption;
					if (($Line[$iOption]['tplo_name'] == 'footer') && (($Line[$iOption]['tplo_active'] == 1))) $FooterIdx=$iOption;
				}
			}

			$arrVal=array();
			$arrVal[0]['type']=$ItemData[$j]['type_name'];
			$arrVal[0]['order']=$ItemData[$j]['tmpl_order'];
			$arrVal[0]['name']=$ItemData[$j]['tmpl_name'];
			$arrVal[0]['left']=$ItemData[$j]['tmpl_left'];
			$arrVal[0]['top']=$ItemData[$j]['tmpl_top'];
			$arrVal[0]['width']=$ItemData[$j]['tmpl_width'];
			$arrVal[0]['height']=$ItemData[$j]['tmpl_height'];
			
			if ($HeadIdx >= 0) {
				$arrVal[]=array('type'=>"title",
					'order'=>100,
					'name'=>sprintf("%s title",$ItemData[$j]['tmpl_name']),
					'left'=>$Line[$HeadIdx]['tplo_left'],
					'top'=>$Line[$HeadIdx]['tplo_top'],
					'width'=>$Line[$HeadIdx]['tplo_width'],
					'height'=>$Line[$HeadIdx]['tplo_height']);
			}

			if ($FooterIdx >= 0) {
				$arrVal[]=array('type'=>"desc",
					'order'=>100,
					'name'=>sprintf("%s desc",$ItemData[$j]['tmpl_name']),
					'left'=>$Line[$FooterIdx]['tplo_left'],
					'top'=>$Line[$FooterIdx]['tplo_top'],
					'width'=>$Line[$FooterIdx]['tplo_width'],
					'height'=>$Line[$FooterIdx]['tplo_height']);
			}

			for ($iCount=0; $iCount<count($arrVal); $iCount++) {


				$type_name=$arrVal[$iCount]['type'];
				$tmpl_order=$arrVal[$iCount]['order'];
				$tmpl_name=$arrVal[$iCount]['name'];
				$tmpl_left=$arrVal[$iCount]['left'];
				$tmpl_top=$arrVal[$iCount]['top'];
				$tmpl_width=$arrVal[$iCount]['width'];
				$tmpl_height=$arrVal[$iCount]['height'];
				
				$height = "height:".($tmpl_height)."px;"; 
				$width = "width:".($tmpl_width)."px;";
				$left = "left:".($tmpl_left)."px;";
				$top = "top:".($tmpl_top)."px;";
				$zindex = "z-index:".($tmpl_order).";";
	?>

	var newdiv = document.createElement('div');
	
	var cal_width = window.outerWidth-16;
	var cal_height = window.outerHeight-64;
	
	/*
	var sDisplay ="Imul begin => "+ <?//=$j;?>;
	sDisplay += "\nscreen w2 "+ screen_width + " h2 "+ screen_height;
	sDisplay += "\ntemplate w1 "+ template_width + " h1 "+ template_height;
	sDisplay += "\nInner w3 "+ window.innerWidth + " h3 "+ window.innerHeight;
	sDisplay += "\nouter w4 "+ window.outerWidth + " h4 "+ window.outerHeight;
	sDisplay += "\ndisp w5 "+ disp_width + " h5 "+ disp_height;
	sDisplay += "\ncal w6 "+ cal_width + " h6 "+ cal_height;
	alert(sDisplay);
	*/
	
	var div_top=<?=$tmpl_top;?>/iMul;
	var div_left=<?=$tmpl_left;?>/iMul;
	var div_width=<?=$tmpl_width;?>/iMul;
	var div_height=<?=$tmpl_height;?>/iMul;
	
 	newdiv.setAttribute('id', '<?=$tmpl_id;?>');
	newdiv.style.display="table-cell";
	
	div_width = (cal_width * div_width)/disp_width;
	div_height = (cal_height * div_height)/disp_height;
	div_left = (cal_width * div_left)/disp_width;
	div_top = (cal_height * div_top)/disp_height;
	
	newdiv.style.width = div_width;
	newdiv.style.height = div_height;
	
	newdiv.style.position = "fixed";
	
	newdiv.style.left = div_left;
	newdiv.style.top = div_top;
	
	newdiv.style.backgroundColor = "<?=$bgcolor[$j];?>";
	newdiv.style.border = "1px solid #000";
	newdiv.style.textAlign="center";
	newdiv.style.zIndex=<?=$tmpl_order;?>;
	
	var sDisplay ="\ndiv_width "+ div_width;
	sDisplay += "\ndiv_height "+ div_height;
	sDisplay += "\ndiv_left "+ div_left;
	sDisplay += "\ndiv_top"+ div_top;
	console.log(sDisplay);
	
//	newdiv.style.lineHeight=div_height/2;
//	newdiv.style.marginTop=div_top+((div_height-div_top)/2);
	var innerHTML="<table width='100%' height='100%'><tr valign='middle'><td align='center' >";
<?	if (isset($size)) {?>
	var sSize="( <?=$tmpl_width;?> X <?=$tmpl_height;?> ) ";
<? } else {?>
	var sSize="";
<?	} ?>
	innerHTML+="<?=$tmpl_name;?>"+sSize+"</td></tr></table>"; 

	newdiv.innerHTML = innerHTML;
//	newdiv.innerHTML = "<?//=$tmpl_name;?>"; 
//	document.body.dvTable.appendChild(newdiv);
	$('#dvTable').append(newdiv);
	div_width = "";
	div_height = "";
<? } }?>
	var sDisplay ="Imul end => "+ iMul;
	sDisplay += "\nscreen w2 "+ screen_width + " h2 "+ screen_height;
	sDisplay += "\ntemplate w1 "+ template_width + " h1 "+ template_height; 
	sDisplay += "\nInner w3 "+ window.innerWidth + " h3 "+ window.innerHeight;
	sDisplay += "\nouter w4 "+ window.outerWidth + " h4 "+ window.outerHeight;
	console.log(sDisplay);
	
	document.getElementById('dvTable').style.display="table-cell";
//	document.getElementById('dvTable').style.width=window.innerWidth;
//	document.getElementById('dvTable').style.height=window.innerHeight;
	document.getElementById('dvTable').style.width=disp_width;
	document.getElementById('dvTable').style.height=disp_height;
	document.getElementById('dvTable').style.textAlign="center";
	document.getElementById('dvTable').style.zIndex=0;	
	window.focus();
	});
</script>
<body>
<div id="dvTable" style="background-color:#000;">
<div id="dvCover" style="z-index:99; position:fixed;">
</div>
<div id="dvBg" style="z-index:-100; position:fixed;">
</div>
</div>
</body>