<?
	header('Access-Control-Allow-Origin: *');
	$DEBUG=FALSE;
	include("./includes/db_config.inc.php");
	include("./includes/sys_config.inc.php");


	if ($btnSave) {
		$CfgList = json_decode(getAllConfig($ComID),true);
		for ($iCount=0; $iCount <count($CfgList); $iCount++) {
			$cfgID=$CfgList[$iCount]['cfg_id'];
			$cfgCategory=$CfgList[$iCount]['cfg_category'];
			$cfgName=$CfgList[$iCount]['cfg_name'];
			$cfgType=$CfgList[$iCount]['cfg_type'];
			$cfgData=$CfgList[$iCount]['cfg_data'];
			$cfgOption=$CfgList[$iCount]['cfg_option'];
			$ListCfg[$cfgCategory][$cfgName]['id']=$cfgID;
			$ListCfg[$cfgCategory][$cfgName]['data']=$cfgData;
			$ListCfg[$cfgCategory][$cfgName]['type']=$cfgType;
		}
		$ID=$ListCfg['Email']['SMTP_Server']['id'];
		setConfig($ComID,$ID,"Email","SMTP_Server","TEXT",$txtSMTPServer);
		$ID=$ListCfg['Email']['SMTP_Secure']['id'];
		setConfig($ComID,$ID,"Email","SMTP_Secure","TEXT",$txtSMTPSecure);
		$ID=$ListCfg['Email']['SMTP_Port']['id'];
		setConfig($ComID,$ID,"Email","SMTP_Port","TEXT",$txtSMTPPort);
		$ID=$ListCfg['Email']['SMTP_User']['id'];
		setConfig($ComID,$ID,"Email","SMTP_User","TEXT",$txtSMTPUser);
		$ID=$ListCfg['Email']['SMTP_Pass']['id'];
		setConfig($ComID,$ID,"Email","SMTP_Pass","TEXT",$txtSMTPPass);
		$ID=$ListCfg['Email']['SMTP_FormName']['id'];
		setConfig($ComID,$ID,"Email","SMTP_FormName","TEXT",$txtSMTPFromName);
		$ID=$ListCfg['Email']['SMTP_FormMail']['id'];
		setConfig($ComID,$ID,"Email","SMTP_FormMail","TEXT",$txtSMTPFromMail);
		$ID=$ListCfg['Email']['SMTP_ReplyName']['id'];
		setConfig($ComID,$ID,"Email","SMTP_ReplyName","TEXT",$txtSMTPReplayName);
		$ID=$ListCfg['Email']['SMTP_ReplyMail']['id'];
		setConfig($ComID,$ID,"Email","SMTP_ReplyMail","TEXT",$txtSMTPReplayMail);
		$ID=$ListCfg['Email']['Rcv_Name1']['id'];
		setConfig($ComID,$ID,"Email","Rcv_Name1","TEXT",$txtRcvName1);

		$ID=$ListCfg['Email']['Rcv_Mail1']['id'];
		setConfig($ComID,$ID,"Email","Rcv_Mail1","TEXT",$txtRcvMail1);

		$ID=$ListCfg['Email']['Rcv_Name2']['id'];
		setConfig($ComID,$ID,"Email","Rcv_Name2","TEXT",$txtRcvName2);

		$ID=$ListCfg['Email']['Rcv_Mail2']['id'];
		setConfig($ComID,$ID,"Email","Rcv_Mail2","TEXT",$txtRcvMail2);

		$ID=$ListCfg['Email']['Rcv_Name3']['id'];
		setConfig($ComID,$ID,"Email","Rcv_Name3","TEXT",$txtRcvName3);

		$ID=$ListCfg['Email']['Rcv_Mail3']['id'];
		setConfig($ComID,$ID,"Email","Rcv_Mail3","TEXT",$txtRcvMail3);
	}

	$CfgList = json_decode(getAllConfig($ComID),true);
	for ($iCount=0; $iCount <count($CfgList); $iCount++) {
		$cfgID=$CfgList[$iCount]['cfg_id'];
		$cfgCategory=$CfgList[$iCount]['cfg_category'];
		$cfgName=$CfgList[$iCount]['cfg_name'];
		$cfgType=$CfgList[$iCount]['cfg_type'];
		$cfgData=$CfgList[$iCount]['cfg_data'];
		$cfgOption=$CfgList[$iCount]['cfg_option'];
		$ListCfg[$cfgCategory][$cfgName]['id']=$cfgID;
		$ListCfg[$cfgCategory][$cfgName]['data']=$cfgData;
		$ListCfg[$cfgCategory][$cfgName]['type']=$cfgType;
	}
?>
<html>
<head>
<?	include("./javascript.php");?>
<link rel="stylesheet" href="css/custom-theme/jquery-ui-1.8.23.custom.css">
<link rel="stylesheet" href="css/StyleSheet.css">
<script type="text/javascript">
	function showPass() {
		var curType=$('#txtSMTPPass').attr('type');
		if (curType == 'passsword')  {
			curType='text'; 
		} else {
			curType='password'; 
		}
		$('#txtSMTPPass').attr('type',curType);
	}

    function changeType()
    {
        document.myform.txtSMTPPass.type=(document.myform.chkPass.value=(document.myform.chkPass.value==1)?'-1':'1')=='1'?'text':'password';
    }
</script>
</head>
<body>
<div class="w-60 center">
<table border="1">
<form id="myform" name="myform" method="post">
<tr>	<th colspan="2" class="header">Outgoing Mail</th></tr>
<tr>
	<td align="right">(SMTP) Mail Server :</td>
	<td><input type="text" id="txtSMTPServer" name="txtSMTPServer" value="<?=$ListCfg['Email']['SMTP_Server']['data'];?>" size="25" maxlength="40"/></td>
</tr>
<tr>
	<td align="right">(SMTP) Secure :</td>
  <td><input type="text" id="txtSMTPSecure" name="txtSMTPSecure"value="<?=$ListCfg['Email']['SMTP_Secure']['data'];?>" size="25" maxlength="40"/></td>
</tr>
<tr>
	<td align="right">(SMTP) Port :</td>
  <td><input type="text" id="txtSMTPPort" name="txtSMTPPort"value="<?=$ListCfg['Email']['SMTP_Port']['data'];?>" size="25" maxlength="40"/></td>
</tr>
<tr>
	<td align="right">(SMTP) User :</td>
  <td><input type="text" id="txtSMTPUser" name="txtSMTPUser" value="<?=$ListCfg['Email']['SMTP_User']['data'];?>" size="25" maxlength="40"/></td>
 </tr>
<tr>
	<td align="right">(SMTP) Password :</td>
  <td><input type="password" id="txtSMTPPass" name="txtSMTPPass" value="<?=$ListCfg['Email']['SMTP_Pass']['data'];?>" size="25" maxlength="40" data-typetoggle='#chkPass' />
  <input id="chkPass" name="chkPass" type="checkbox" onchange="changeType();"/>Show Password
  </td>
</tr>
<tr>
	<td align="right">(SMTP) From Name<br />Address</td>
  <td>
    <input type="text" id="txtSMTPFromName" name="txtSMTPFromName" value="<?=$ListCfg['Email']['SMTP_FormName']['data'];?>" size="25" maxlength="40"/><br />
    <input type="text" id="txtSMTPFromMail" name="txtSMTPFromMail" value="<?=$ListCfg['Email']['SMTP_FormMail']['data'];?>" size="40" maxlength="60"/>
  </td>
</tr>
<tr>
	<td align="right">(SMTP) Reply Name<br />Address</td>
	<td>
		<input type="text" id="txtSMTPReplayName" name="txtSMTPReplayName" value="<?=$ListCfg['Email']['SMTP_ReplyName']['data'];?>" size="25" maxlength="40"/><br />
		<input type="text" id="txtSMTPReplyMail" name="txtSMTPReplayMail" value="<?=$ListCfg['Email']['SMTP_ReplyMail']['data'];?>" size="40" maxlength="60"/>
  </td>
</tr>
<tr>	<th colspan="2" class="header">Recieve Mail</th></tr>
<tr>
	<td align="right">(1) Recieve Name<br />Email</td>
  <td>
  	<input type="text" id="txtRcvName1" name="txtRcvName1" value="<?=$ListCfg['Email']['Rcv_Name1']['data'];?>" size="25" maxlength="40"/><br />
  	<input type="text" id="txtRcvMail1" name="txtRcvMail1" value="<?=$ListCfg['Email']['Rcv_Mail1']['data'];?>" size="40" maxlength="60"/>
  </td>
</tr>
<tr>
	<td align="right">(2) Recieve Name<br />Email</td>
  <td>
  	<input type="text" id="txtRcvName2" name="txtRcvName2" value="<?=$ListCfg['Email']['Rcv_Name2']['data'];?>" size="25" maxlength="40"/><br />
  	<input type="text" id="txtRcvMail2" name="txtRcvMail2" value="<?=$ListCfg['Email']['Rcv_Mail2']['data'];?>" size="40" maxlength="60"/>
  </td>
</tr>
<tr>
	<td align="right">(3) Recieve Name<br />Email</td>
  <td>
  	<input type="text" id="txtRcvName3" name="txtRcvName3" value="<?=$ListCfg['Email']['Rcv_Name3']['data'];?>" size="25" maxlength="40"/><br />
  	<input type="text" id="txtRcvMail3" name="txtRcvMail3" value="<?=$ListCfg['Email']['Rcv_Mail3']['data'];?>" size="40" maxlength="60"/>
  </td>
</tr>
<tr>	<th colspan="2"><input name="btnSave" type="submit" value="Updated" onclick="config_save();"/></th></tr>
</form>
</table>
</div>
</body>
</html>