<head>
<? 
	$DEBUG=FALSE;
	$TypeID=11;
	include("./includes/db_config.inc.php");
	include("./functions/library.func.php");
?>
<script type="text/javascript" src="js/SolmetraUploader.js"></script>
<script type="text/javascript">
  SolmetraUploader.setErrorHandler('test');
  function test (id, str) { alert('ERROR: ' + str); }
</script>
<?	include("./javascript.php");?>
<script type="text/javascript" src="js/library_file.js"></script>
<script>
	function library_upload(type,oldName,newName,upName,fSize) {
		var URL="./ajax/library_add.php";
//	if ( isset($Type) && isset($oldName) && isset($newName) && isset($fsize) )  {
		var Params={'Type':type,
			'oldName':encodeURIComponent(oldName),
			'newName':encodeURIComponent(newName),
			'upName':encodeURIComponent(upName),
			'ComID':com_id,
			'DEBUG':false,
			'fsize':fSize};
//		document.getElementById('URL').value=URL;
//		document.getElementById('Params').value=dump(Params);
//		alert("Params =>"+dump(Params));
		$.post(URL,Params,function(data) {
//			alert(dump(data));
			if (parseInt(data) > 0) {
				alert("Covert File Successful.");
			} else {
//				alert(" File Failed");
			}
			$('#dvConvert').dialog("close");
			file_list(<?=$TypeID;?>);
		});
		$('#dvConvert').dialog("open");
	}

	$(function() {
		$( "input:submit, a, button", ".demo" ).button();
		$( "a", ".demo" ).click(function() { return false; });
	});
</script>
<link rel="stylesheet" href="css/custom-theme/jquery-ui-1.8.23.custom.css">
<link rel="stylesheet" href="css/StyleSheet.css">
<script type="text/javascript">
	$('document').ready(function() {
		file_list(<?=$TypeID;?>);
	});
</script>
</head>
<body>
<script type="text/javascript">
	$(function() {
		$('#dvConvert').dialog({ autoOpen: false, modal: true, draggable: false });
		$('#dvConvert').dialog("option","width",700);
		$('#dvConvert').dialog("option","height",250);
		$('#dvConvert').dialog("option","resizable",false);

		$('#dvRename').dialog({ autoOpen: false, modal: true, draggable: false });
		$('#dvRename').dialog("option","width",700);
		$('#dvRename').dialog("option","height",100);
		$('#dvRename').dialog("option","resizable",false);
	});
</script>
<div class="demo">
<div id="dvConvert" title="Upload and Coverting Format">
<table>
  <tbody id="tblUpload_Body">
  <tr>
  	<td><img src="./images/loading.gif" width="125" height="125"></td>
    <td class="left top10">
		This Dialog will disappear after convert completed.<br>
    Please waiting until completed.
  </td></tr>
  </tbody>
</table>
</div>
</div>

<div class="demo">
	<div id="dvRename" title="Rename Display Name">
  <table>
    <tbody id="tblRename_Body">
    <tr>
      <td class="left top10" nowrap>
      <input type="text" id="txtRenameFName" value="" size="40" maxlength="127">
      <input type="hidden" id="txtRenameFID" value="">
      <input type="button" onClick="file_rename($('#txtRenameFID').val(),$('#txtRenameFName').val(),'dvRename',<?=$TypeID;?>);" value="Rename">
      <input type="button" onClick="$('#dvRename').dialog('close');" value="Cancel">
      </td>
    </tr>
    </tbody>
  </table>
	</div>
</div>

<div class="w-50">
	<div class="well">
    	<div class="left head">Background Library</div>
   		<div id="dvLibUploadImages">
<?
    if ($DEBUG) { echo "Request<pre>";		print_r($_REQUEST);		echo "</pre>"; }
    if ($DEBUG) { echo "File<pre>";		print_r($_FILES);		echo "</pre>"; }
//		echo "<pre>"; print_r($_SERVER); echo "</pre>";
    include("SolmetraUploader.php");

    $solmetraUploader = new SolmetraUploader('./','./upload.php','./config_covers.php');
//		$solmetraUploader->setDemo(102400000);
    $solmetraUploader->gatherUploadedFiles();
    if (isset($_FILES) && sizeof($_FILES)) {
        if ($DEBUG) {
            echo '<h2>Uploaded files</h2>';
        echo '<pre>'; print_r($_FILES['firstfile']); echo '</pre>';
        }
        $uploadName=$_FILES['firstfile']['tmp_name'];
        $oldName=$_FILES['firstfile']['name'];
        $fSize=$_FILES['firstfile']['size'];
        if (($oldName != '') and ($txtNewName !='')  and ($uploadName != '') and ($fSize > 0) ) {
//            $result=library_add('images',$oldName,$txtNewName,$uploadName,$fSize);
?>
		<script language="javascript">
			var Display="oldName => <?=$oldName;?>\n NewName=> <?=$txtNewName;?>\n uploadName=> <?=$uploadName;?> ";
//			alert(Display);
			$(document).ready(function() {
				$('#dvConvert').dialog("open");
				library_upload('covers','<?=$oldName;?>','<?=$txtNewName;?>','.<?=$uploadName;?>',<?=$fSize;?>);
			});
		</script>
<?		
        } else {
            $rResult="Data Not Completed.<BR>";
            $rResult.="oldName = '$oldName'<BR>";
            $rResult.="txtNewName = '$txtNewName'<BR>";
            $rResult.="uploadName = '$uploadName'<BR>";
            $rResult.="size = '".$fdata['size']."'<BR>";
            unlink($uploadName);
        }
    }
    
    if (isset($btnDelete)) {
        if ($DEBUG) echo "<br> Delete File => $btnDelete";
        $RowAffect=library_active($btnDelete);
        if ($RowAffect <> 1) {
            if ($DEBUG) echo ".....Delete Completed";
        } else {
            if ($DEBUG) echo ".....Delete Failed";
        }
    } elseif (isset($btnRestore)) {
        if ($DEBUG) echo "<br> Restore File => $btnRestore";
        $RowAffect=library_active($btnRestore,1);
        if ($RowAffect <> 1) {
            if ($DEBUG) echo ".....Restore Completed";
        } else {
            if ($DEBUG) echo ".....Restore Failed";
        }
    } else {
        if (isset($_SERVER['HTTP_REFERER']))	{
            if ($_SERVER['HTTP_URI'] = $PHP_SELF) {
                echo ("No File Upload");
            }
        }
    }
?>
        <form style="padding-left:20px; padding-top:15px;" method="post"/>
            <? if (strlen($rResult)>0) {?><div class="dvtd_left w-20"><?=$rResult;?></div><? } ?>
            <div><? echo $solmetraUploader->getInstance('firstfile', 500,30,true); ?></div>
            <div class="dvtr top10">
            	<div class="dvtd_right">Display Name : </div>
                <div class="dvtd_right">
                	<input type="text" class="txt" id="txtNewName" name="txtNewName" size="46" />
                </div>
            	<div class="demo">&nbsp;<input type="submit" id="btnSubmit" name="btnSubmit" value="Save"/></div>
            </div>
        </form>
    	</div>
	</div>
</div>
<?	
//	echo "Count(Data) =".count($data);
//	$FileList=json_decode($data);
//	echo "Count(FileList) =".count($FileList);
//	echo "<pre>";
//	print_r($FileList);
//	echo "</pre>";
//	for ($iCount=0; $iCount < count($FileList); $iCount++) {
//		$arrData=array();
//		$arrData=$FileList[$iCount];
//		echo "<pre>";
//		print_r($arrData);
//		echo "</pre>";
//		echo $arrData['file_id'];
//	}
?>
<div class="w-100 top15"><!-- .altbox for alternative box's color -->
	<div class="boxin">
        <div class="header">
            <h3><span>Background Library</span>
            <span>&nbsp; 
            <input class="btnTH" id="btnListRefresh" type="button" value="refresh" onClick="file_list(<?=$TypeID;?>);">&nbsp;
            <input class="txt" id="txtSearch" type="text" value="" size="10">&nbsp;
            <input class="btnTH" id="btnSearch" type="button" value="search" onClick="table_search('tblList_Body',1,'txtSearch');">
						</span>
            </h3>
        </div>
        <table cellspacing="0">
          <thead>
            <tr>
                <th>Display Name</th>
                <th>Original Name</th>
                <th width="10%">File size</th>
                <th width="10%">Resolution</th>
                <th width="9%">Operation</th>
            </tr>
          </thead>
        	<tbody id="tblList_Body">
        	</tbody>
      </table>
	</div>
</div>
</body>
