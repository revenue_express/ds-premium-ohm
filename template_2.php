<? 
	$DEBUG=FALSE;
	include("./includes/db_config.inc.php");
	include("./includes/sys_config.inc.php");
//	include("./functions/template.func.php");
//	$USER['usr_level']="Administrator";
?>
<html>
<head>
<?	include("./javascript.php");?>
<script language="javascript" src="js/template.js"></script>
<link rel="stylesheet" href="css/custom-theme/jquery-ui-1.8.23.custom.css">
<link rel="stylesheet" href="css/StyleSheet.css">
<script type="text/javascript">
	$('document').ready(function() {
		lm_head_list();
		$('#txtInsertID,#txtInsertObjectName,#selInsertObject').height(20).css("font-size","14px");
		$('#txtUpdateID,#txtUpdateOrder,#selUpdateName,#txtUpdateName,#txtUpdateType').height(20).css("font-size","14px");
	});
	
	function popup_display() {
		var URL="popup_display.php?FormName=formHead&OldID="+$('#txtOldDispID').val();
		URL+="&DispID=txtNewDispID&DispName=txtSize&btnData=btnHeadSubmit";
		var Option="width=710,height=280,location=no,menubar=no,resizeable=no,scrollbars=no,status=no,toolbar=no";
		window.open(URL,'popup_display',Option);
	}
</script>
</head>
<body>
<div id="dvHeadResult" class="w-100 boxin">
	<div class="header">
  	<h3>Layout Management</h3>
  </div>
  <table cellspacing="0">
  <thead>
  	<tr>
      <th>Layout Name</th>
      <th>Screen Size</th>
      <th>Item(s)</th>
      <th colspan="3">
      <input class="btnTH" type="button" value="Refresh" onClick="lm_head_list();"/>
      &nbsp; &nbsp;
      <input class="btnTH" type="button" value=" &nbsp;Add &nbsp;" onClick="lm_head_add();"/>
      </th>
  	</tr>
  </thead>
  <tbody id="tblHead_Result">
    <tr>
      <td class="center">1</td>
      <td>2</td>
      <td>Item</td>
      <td class="center"><img class="preview" src="images/icons/edit.png" title="preview"></td>
      <td class="center"><img class="preview" src="images/icons/delete.png" title="preview"></td>
    </tr>
  </tbody>
  </table>
</div>
<div>
<div id="dvLineResult" class="boxin w-100">
<form id="formHead" name="formHead">
<input type="hidden" id="txtOldName" value="">
<input type="hidden" id="txtNewName" value="">
<input type="hidden" size="2" id="txtOldDispID" value="">
<input type="hidden" size="2" id="txtNewDispID" value="">
<input type="text" size="2" id="TemplateID" value="">
<input type="text" size="2" id="InUsed" value="">
<input type="text" size="2" id="LineID" name="LineID" value="">
<input type="text" size="2" id="CoverID" value="">
<input type="text" size="2" id="CoverFileID" value="">
<input type="hidden" id="LineName" name="LineName" value="">
<input type="hidden" size="2" id="LineTypeID" name="LineTypeID" value="">
<input type="hidden" size="2" id="LineTypeName" name="LineTypeName" value="">
<input type="hidden" id="LineTName" name="LineTName" value="">
<input type="hidden" size="2" id="LineHeader" name="LineHeader" value="">
<input type="hidden" size="2" id="LineFooter" name="LineFooter" value="">

	<div class="header">
  	<h3>Layout Information</h3><input class="btnHeader" style="margin-left:200px;" type="button" value="Back" onClick="lm_head_list();"/>
  </div>
<?
		$onNameClick=sprintf("changeInputValue('txtName','Change Playlist Name to ',$('#txtOldName').val(),'formHead');");
		$onNameClick.=sprintf("setObjValue('txtName','txtNewName','formHead');");
		$onNameClick.=sprintf("EnableObject('btnHeadSubmit',! checkEqual('txtNewName','txtOldName','formHead'),'formHead');");
		$sDisplay="Do you confirm to change template information?";
		$onSubmitClick=sprintf("lm_head_edit();");
		
		$onDispChange=sprintf("EnableObject('btnHeadSubmit',! checkEqual('txtOldDispID,'txtNewDispID','formHead'),'formHead');");

//		$Url=sprintf("popup_display.php?FormName=formHead&OldID=$('#txtOldDispID').val()");
//		$Url.=sprintf("&DispID=txtNewDispID&DispName=txtSize");
//		$Url.=sprintf("&btnData=%s",'btnHeadSubmit');
//		$Option=sprintf("width=%d,height=%d,location=no,menubar=no,resizeable=no,scrollbars=no,status=no,toolbar=no",710,280);
//		$onDisplayClick=sprintf("window.open('%s','_display','%s');",$Url,$Option);
		$onDisplayClick="popup_display();";

		$onReset=sprintf("EnableObject('btnHeadSubmit',false,'formHead');");

?>
	<div class="well w-95 center" style="height:120px;">
		<div style="width:100%; margin-top:10px; float:left">
			<div style="width:35%; float:left; padding-top:5px;" align="right">Layout Name : </div>
    	<div style="padding-left:5px; float:left">
   			<input class="txt" size="40" type="text" id="txtName" name="txtName" value="" disabled readonly>
      </div>
      <div style="float:left">
        	<input class="btnSky"  type="button" onClick="<?=$onNameClick;?>" value="Change">
        </div>
    </div>
    
    <div style="width:100%; float:left">
		<div style="width:35%; float:left; padding-top:5px;" align="right">Display Size : </div>
    	<div style="padding-left:5px; float:left">
   			<input class="txt" size="40" type="text" id="txtSize" name="txtSize" value="<?=$Header[0]['disp_name'];?>" disabled readonly  onFocus="<?=$onDispChange;?>">
        </div>
       	<div style="float:left">
        	<input class="btnSky"  type="button" onClick="<?=$onDisplayClick;?>" value="Change">
        </div>
    </div>
    <div style="width:100%;">
			<div style="width:50%; float:left;" align="right">
    		<input  class="btnSky" type="button" id="btnHeadSubmit" name="btnHeadSubmit" value="Save" disabled onClick="<?=$onSubmitClick;?>">
      </div>
    	<div style="float:left">
   			<input  class="btnSky" type="Reset" id="btnReset" name="btnReset" value="Reset" onClick="<?=$onReset;?>">
      </div>
    </div>
	</div>
</form>
  <table cellspacing="0" class="w-50">
<form>
	<thead>
  	<th nowrap>Layout Image :: <select id="selFile"></select></th>
  	<th nowrap>
    	<select id="selFileLayer" style="display:none">
      	<option value="1" selected>Front</option>
      	<option value="0">Back</option>
      </select>
    	<input class="btnSky" type="button" value="Selected" onClick="lm_file_select();"></th>
  </thead>
	</form>
	</table>
  <br/>
  <table cellspacing="0" class="w-100">
	<thead>
  	<th nowrap>Layer Order</th>
  	<th>Type</th>
  	<th>Object Name</th>
  	<th>Option</th>
  	<th nowrap>
    <input id="btnInsertObject" class="btnSky" type="button" value="New Object" onClick="lm_line_insert();"> &nbsp; 
    <input class="btnSky" type="button" value="Visual Layout" onClick="lm_manager();"></th>
  </thead>
  <tbody id="tblLine_Result">
    <tr>
      <td class="w-5 center">1</td>
      <td class="w-15 center">Image</td>
      <td class="w-40">Image Name</td>
      <td class="w-30">[ &nbsp; ] Header [ &nbsp; ] Footer</td>
      <td class="w-10 center">
      	<input type="button" class="btnSky" value="Edit" onClick="$('#dvUpdate').dialog('open');"> &nbsp; 
        <input type="button" class="btnSky" value="Delete">
        </td>
    </tr>
    <tr>
      <td class="w-5 center">2</td>
      <td class="w-15 center">Flash</td>
      <td class="w-40">Flash Object</td>
      <td class="w-30">&nbsp;</td>
      <td class="w-10 center">
      	<input type="button" class="btnSky" value="Edit" onClick="$('#dvUpdate').dialog('open');"> &nbsp; 
        <input type="button" class="btnSky" value="Delete">
        </td>
    </tr>
    <tr>
      <td class="w-5 center">3</td>
      <td class="w-15 center">Flash</td>
      <td class="w-40">Flash Object</td>
      <td class="w-30">&nbsp;</td>
      <td class="w-10 center">
      	<input type="button" class="btnSky" value="Edit" onClick="$('#dvUpdate').dialog('open');"> &nbsp; 
        <input type="button" class="btnSky" value="Delete">
        </td>
    </tr>
    <tr>
      <td class="w-5 center">4</td>
      <td class="w-15 center">Flash</td>
      <td class="w-40">Flash Object</td>
      <td class="w-30">&nbsp;</td>
      <td class="w-10 center">
      	<input type="button" class="btnSky" value="Edit" onClick="$('#dvUpdate').dialog('open');"> &nbsp; 
        <input type="button" class="btnSky" value="Delete">
        </td>
    </tr>
    <tr>
      <td class="w-5 center">5</td>
      <td class="w-15 center">Flash</td>
      <td class="w-40">Flash Object</td>
      <td class="w-30">&nbsp;</td>
      <td class="w-10 center">
      	<input type="button" class="btnSky" value="Edit" onClick="$('#dvUpdate').dialog('open');"> &nbsp; 
        <input type="button" class="btnSky" value="Delete">
        </td>
    </tr>
  </tbody>
  </table>
  
</div>
</div>
<script type="text/javascript">
	$(function() {
		$('#dvInsert').dialog({ autoOpen: false });
		$('#dvInsert').dialog("option","width",800);
		$('#dvInsert').dialog("option","height",155);
		$('#dvInsert').dialog("option","resizable",false);
	});
</script>
<div class="demo">
<div id="dvInsert" title="Insert New Object">
<div id="dvInsertTable">
<table>
	<thead>
  	<th>Layer Order</th>
  	<th>Object Type</th>
  	<th>Object Name</th>
  	<th colspan="2">Option</th>
	</thead>
  <tr>
  	<td><input id='txtInsertID' type="text" size="4"></td>
  	<td>
    	<select id="selInsertObject">
      	<option>Image</option>
      	<option>Video</option>
      	<option>Flash</option>
      	<option>Audio</option>
      	<option>Text Feed</option>
      	<option>Acrobat</option>
      	<option>PowerPoint</option>
      </select>
    </td>
  	<td><input id='txtInsertObjectName' type="text" size="40"></td>
  	<td><span id="spInsertHeader"><input id='chkInsertHeader' type="checkbox">Header</span></td>
  	<td><span id="spInsertFooter"><input id='chkInsertFooter' type="checkbox">Footer</span></td>
  </tr>
</table>
</div>
<div class="right" id="dvInsertObjectOK" style="display:block;"><input id="btnObjectInsert" type="button" class="btnSky" value="Insert"></div>
<div id="dvInsertObjectFull" class="center" style="display:none;"><span>Maximum Object</span></div>
</div>
</div>

<script type="text/javascript">
	$(function() {
		$('#dvUpdate').dialog({ autoOpen: false });
		$('#dvUpdate').dialog("option","width",800);
		$('#dvUpdate').dialog("option","height",155);
		$('#dvUpdate').dialog("option","resizable",false);
	});
</script>
<div class="demo">
<div id="dvUpdate" title="Insert New Object">
<div id="dvUpdateTable">
<table>
  <thead>
  	<th>Layer Order</th>
  	<th>Object Type</th>
  	<th>Object Name</th>
  	<th colspan="2">Option</th>
  </thead>
  <tr>
  	<td><input id='txtUpdateOrder' type="text" size="4"></td>
  	<td>
			<input type="hidden" id="selUpdateObject" value="">
			<input id="txtUpdateType" type="text" size="8" value="" readonly>
    </td>
  	<td><input id='txtUpdateName' type="text" size="40"></td>
  	<td><span id="spUpdateHeader"><input id='chkUpdateHeader' type="checkbox">Header</span></td>
  	<td><span id="spUpdateFooter"><input id='chkUpdateFooter' type="checkbox">Footer</span></td>
  </tr>
</table>
</div>
<div class="right" id="dvObjectOK" style="display:block;">
	<input id="btnObjectInsert" type="button" class="btnSky" value="Update">
</div>
</div>
</div>
</body>
</html>
