<? 
	include("./includes/db_config.inc.php");
	include("./includes/sys_config.inc.php");
	$DEBUG=FALSE;
?>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<script type="text/javascript" src="./js/jquery-1.7.2.min.js"></script>
<script type="text/javascript" src="./js/jquery-ui-1.8.21.custom.min.js"></script>
<script type="text/javascript" src="./js/checkValid.js"></script>
<script type="text/javascript" src="./js/jscolor.js"></script>
<script type="text/javascript" src="./js/signage.js"></script>
</script>
<link rel="stylesheet" href="./css/style.css">
<link rel="stylesheet" href="./css/custom-theme/jquery-ui-1.8.22.custom.css">
<script type="text/javascript" src="./js/layout.js"></script>
<script type="text/javascript">
	$('document').ready(function() {
		load_headlist();
	});
	
	function add_new() {
		ShowObject('dvHeadResult',false,'');
		ShowObject('dvLayout',false,'');
//		ShowObject('dvSelectFile',false,'');
		$('#dvSelectFile').dialog('close');
		ShowObject('dvSelectPlaylist',false,'');
		ShowObject('dvHeadAddForm',true,'');
	}
</script>
<style type="text/css">
.spMenu { cursor: pointer; }
.tdFileSize { text-align:right; padding-right: 5px;}
</style>
</head>
<body>
<div><h3>Template Management</h3></div>
<div><?		if ($DEBUG) {		echo "request<pre>"; print_r($_REQUEST); echo "</pre>"; } ?></div>

<div id="dvHeadResult">
<table width="100%" border="1">
	<thead>
  <tr>
  	<th>Template Name / Layout Name</th>
    <th>Status</th>
    <th colspan="4" nowrap>
    	<input class="ui-button" type="button" id="btnHeadRefresh" value="Refresh"onClick="load_headlist();">
    	<input class="ui-button" type="button" id="btnHeadAdd" value="Add" onClick="add_new();"/>
    </th>
  </tr>
  </thead>
  <tbody id="showHead_Data">
  </tbody>
</table>
</div>
<div id="dvLayout">
<? $onBack="clear_allsection();ShowObject('dvHeadResult',true,'');load_headlist();";?>
<span><input class="ui-button" type="button" id="btnHeadRefresh" value="Back" onClick="<?=$onBack;?>"/></span>
<? 
$onChange="EnableObject('btnSaveLayout',false);";
$onChange.=sprintf("changeInputValue('txtLayoutName','%s',document.getElementById('txtLayoutName').value);",$sDisplay);
$onChange.="EnableObject('btnSaveLayout',! checkEqual('txtLayoutName','txtOldLayoutName'));";
$onSave="layout_head_edit(document.getElementById('HeadID').value,";
$onSave.="document.getElementById('txtLayoutName').value)";
	?>
<table id="tblFormEdit">
<tr>
	<th colspan="2">Template Information</th>
</tr>
<tr>
  <td>Template Name</td>
  <td><input type="text" id="txtLayoutName" name="txtLayoutName" size="40" maxlength="64" readonly disabled/>
  <span><input class="ui-button" type="button" id="btnLayoutName" value="Change Name" onClick="<?=$onChange;?>" />
	<input type="hidden" id="txtOldLayoutName" readonly disabled>
	<input class="ui-button" type="button" id="btnSaveLayout" value="Save" onClick="<?=$onSave;?>" disabled/></span>
  </td>
</tr>
<tr>
  <td>Layout Name</td>
  <td>
  <input type="text" id="txtTemplate" name="txtTemplate" size="40" maxlength="64" readonly disabled/>
  </td>
</tr>
</table>
<?
//$onLoad="ShowObject('dvSelectPlaylist',true,'');";
$onLoad.="$('#dvSelectPlaylist').dialog('open');";
$onLoad.="ShowObject('dvShowPlayListItem',false,'');";
//$onLoad.="ShowObject('dvSelectFile',false,'');";
$onLoad.="$('#dvSelectFile').dialog('close');";
$onLoad.="layout_playlist_show(document.getElementById('HeadID').value,document.getElementById('TypeID').value);";
$onAddPlayList="layout_load_library(document.getElementById('TypeID').value);";
//$onAddPlayList.="ShowObject('dvSelectFile',true,'');";
$onAddPlayList.="$('#dvSelectFile').dialog('open');";
$onChange="EnableObject('btnSaveName',false);";
$sDisplay="Do you Comfirm to Change Playlist Name?";
$onChange.=sprintf("changeInputValue('txtPlayListName','%s',document.getElementById('txtPlayListName').value);",$sDisplay);
$onChange.="EnableObject('btnSaveName',! checkEqual('txtPlayListName','txtOldPlayListName'));";
$onSave="layout_playlist_edit(document.getElementById('PlayListID').value,";
$onSave.="document.getElementById('TypeID').value,";
$onSave.="document.getElementById('txtPlayListName').value)";
?>
  <span id="LineMenu"></span>
	<div id="LineData" style="height:400px; width:740px; overflow:auto">
    <table border='1' width="100%">
    <tr>
    <th colspan="2" nowrap>Playlist Name : <input type="text" id="txtPlayListName" value="" size="32" maxlength="80" readonly disabled>
    <span><input class="ui-button" type="button" id="btnChangeName" value="Change" onClick="<?=$onChange;?>" />
    <input type="hidden" id="txtOldPlayListName" readonly disabled><input type="hidden" id="PlayListID" readonly disabled>
    <input class="ui-button" type="button" id="btnSaveName" value="Save" onClick="<?=$onSave;?>" disabled/>
    <input class="ui-button" type="button" id="btnOption" value="Option" onClick="layout_playlist_option('PlayListID','TypeID');"/></span>
    </th>
    <th colspan="4">
    <span><input class="ui-button" type="button" id="btnLoadPlaylist" value="Load" onClick="<?=$onLoad;?>" /></span>
    <span><input class="ui-button" type="button" id="btnAddSingle" value="Add" onClick="<?=$onAddPlayList;?>"></span>
    </th>
    </tr>
		</table>
    <table border='1' width="100%">
<!--
    <tr>
    <th width="475">File</th>
    <th width="125">Size</th>
    <th colspan="3" width="50"></th>
    <th></th></tr>
-->
    <tbody id="LineData_Body">
    </tbody>
    </table>
  </div>
</div>
<div id="dvHeadAddForm">
<span><input class="ui-button" type="button" id="btnHeadRefresh" value="Back" onClick="<?=$onBack;?>"/></span>
<? 
	$Url=sprintf("%s?FName=%s&TID=%s&TName=%s&BName=%s",'popup_template.php','formLayoutAdd','TemplateID','txtTemplateAdd','btnSubmit');
	$Option=sprintf("width=%d,height=%d,location=no,menubar=no,resizeable=no,scrollbars=no,status=no,toolbar=no",710,280);
	$onAddLayout=sprintf("window.open('%s','_template','%s');",$Url,$Option);
	
	$sSubmit="You can not change template Layout. Do you confirm to add this layout ?";
	$sAjax="layout_add(document.getElementById('txtName').value";
	$sAjax.=",document.getElementById('TemplateID').value";
	$sAjax.=",document.getElementById('txtTemplateAdd').value);";
//	$onSubmit=sprintf("if (confirm('%s')) {document.getElementById('%s').submit(); }",$sSubmit,'formLayoutAdd');
	$onSubmit=sprintf("if (confirm('%s')) {%s }",$sSubmit,$sAjax);

	$onReset="document.forms['formLayoutAdd'].reset();EnableObject('btnSubmit',false)";
	?>
<table id="tblFormAdd">
<form id="formLayoutAdd" name="formLayoutAdd" method="post">
<input type="hidden" id="btnLayoutAdd" name="btnLayoutAdd" value="LayoutAdd"/>
<input type="hidden" id="TemplateID" name="TemplateID" value=""/>
<tr>
	<th colspan="2">Template Information</th>
</tr>
<tr>
  <td>Template Name</td>
  <td><input type="text" id="txtName" name="txtName" size="40" maxlength="64"/></td>
</tr>
<tr>
  <td>Layout Name</td>
  <td>
  <input type="text" id="txtTemplateAdd" name="txtTemplateAdd" size="40" maxlength="64" readonly/>
  <input type="button" id="btnSelTemplate" value="Select Layout" onClick="<?=$onAddLayout;?>"/>
  </td>
</tr>
<tr>
	<td colspan="2" align="right">
  <input type="button" id="btnSubmit" name="btnSubmit" value="Add Layout" onClick="<?=$onSubmit;?>" disabled/>
&nbsp;  <input type="button" id="btnReset" value="Reset" onClick="<?=$onReset;?>"/>
  </td>
 </tr>
</form>
</table>
</div>
<script type="text/javascript">
	$(function() {
		$('#dvSelectFile').dialog({ autoOpen: false });
		$('#dvSelectFile').dialog("option","width",750);
		$('#dvSelectFile').dialog("option","height",255);
		$('#dvSelectFile').dialog("option","resizable",false);
		
	});
</script>
<div id="dvSelectFile" title="Select File">
<div style="height:150px; width:700px; overflow:auto;">
<table border='1' width="100%">
<!--
	<thead>
  <tr><th width="40"></th><th>File Display</th><th width="20%">Size</th></tr>
  </thead>
-->
  <tbody id="tblSelectFile_Body">
  </tbody>
</table>
<?
$onSelectFile="layout_playlist_addfile(document.getElementById('PlayListID').value,";
$onSelectFile.="document.getElementById('TypeID').value,";
$onSelectFile.="document.getElementById('FileID').value);";
?>
</div>
<div style="width:700px;">
<table width="100%" border='1'>
<tr>
<td>You select <input type="text" size="40" id="FileName"/>
<input type="hidden" id="FileID" value='' size="4"/>
<input type="hidden" id="HeadID" value='' size="4"/>
<input type="hidden" id="TypeID" name="TypeID" value="" size="4">
<input type="button" id='btnSelectFile' value="Selected" disabled onClick="<?=$onSelectFile;?>"/></td></tr>
</table>
</div>
</div>
<script type="text/javascript">
	$(function() {
		$('#dvSelectPlaylist').dialog({ autoOpen: false });
		$('#dvSelectPlaylist').dialog("option","width",750);
		$('#dvSelectPlaylist').dialog("option","height",255);
		$('#dvSelectPlaylist').dialog("option","resizable",false);
		
	});
</script>
<div id="dvSelectPlaylist" title="Select Playlist">
<div id="dvSelectPlaylist_head" style="height:150px; width:700px; overflow:auto;">
<table border='1' width="100%">
<!--
	<thead>
  <tr><th width="40"></th><th>Playlist Name</th><th width="20%">Item(s)</th><th width="20%">Size(KB)</th></tr>
  </thead>
-->
  <tbody id="tblSelectPlaylist_Body">
  </tbody>
</table>
<?
$onSelectPlaylist="layout_playlist_add(document.getElementById('HeadID').value,";
$onSelectPlaylist.="document.getElementById('TypeID').value,";
$onSelectPlaylist.="document.getElementById('SelectPlayListID').value);";
?>
</div>
<div id="dvSelectPlaylist_form" style="width:700px;">
<table width="100%" border='1'>
<tr>
<td>You select <input type="text" size="40" id="PlayListName"/>
<input type="hidden" id="SelectPlayListID" value='' size="4"/>
<input type="button" id='btnSelectPlaylist' value="Selected" disabled onClick="<?=$onSelectPlaylist;?>"/></td></tr>
</table>
</div>
<div id="dvShowPlayListItem" style="height:150px; width:700px;">
<table border='1' width="100%">
<!--
	<thead>
  <tr><th>File Display</th><th width="20%">Size</th></tr>
  </thead>
-->
  <tbody id="tblShowPlayListItem">
  </tbody>
</table>
</div>
</div>
<script type="text/javascript">
	$(function() {
		$('#dvOption').dialog({ autoOpen: false });
		$('#dvOption').dialog("option","width",750);
		$('#dvOption').dialog("option","height",255);
		$('#dvOption').dialog("option","resizable",false);
		
	});
</script>
<div id="dvOption" title="Option Selection">
<table>
	<thead>
  <tr><th>Category</th><th width="20%">Value</th></tr>
  </thead>
  <tbody id="tblOption_Body">
  </tbody>
</table>
</div>
</body>
</head>