<?php
/**
 * Solmetra Uploader v1.0 upload handling script
 * 
 * @package uploader
 * @author Martynas Majeris <martynas@solmetra.com> 
 * @copyright UAB Solmetra   
 */
include 'SolmetraUploader.php';
$solmetraUploader = new SolmetraUploader();
$solmetraUploader->handleFlashUpload();
//$solmetraUploader->gatherUploadedFiles();
	$DEBUG=false;
	if ($DEBUG) { echo "<br>upload.php REQUEST<pre>"; print_r($_REQUEST); echo "</pre>"; }	
	if ($DEBUG) { echo "<br>upload.php FILES<pre>"; print_r($_FILES); echo "</pre>"; }	
?>