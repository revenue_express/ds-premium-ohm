<?php
	include("./includes/db_config.inc.php");
	include("./functions/library.func.new.php");

	$arrReturn[0]['Return']=false;
	$arrReturn[0]['Result']="Error";
	$arrReturn[0]['InsertID']=0;
	if(isset($_FILES["myfile"])) {
		$error =$_FILES["myfile"]["error"];
		//You need to handle  both cases
		//If Any browser does not support serializing of multiple files using FormData() 
		$targetFolder = '/neptune/data/'; // Relative to the root
		$fileTypes = explode(',',$_POST['extension']); 
		$targetPath = $_SERVER['DOCUMENT_ROOT'] . $targetFolder. $_POST['path'];
		if(!is_array($_FILES["myfile"]["name"])) //single file 
		{
				$oldFileName = strtolower($_FILES['myfile']['name']);
				$tempFile = $_FILES['myfile']['tmp_name'];
				$fileParts = pathinfo(strtolower($_FILES['myfile']['name']));
				if (in_array($fileParts['extension'],$fileTypes)) {
					$newName=sprintf("%s",date('YmdHis'));
					$newFileName = sprintf('%s.%s',$newName,$fileParts['extension']);
					$newTargetFile=  rtrim($targetPath,'/') . '/' .$newFileName;
					move_uploaded_file($tempFile,$newTargetFile);
					$sFizeSize='0';
					if (file_exists($newTargetFile)) $sFileSize=sprintf("%d",filesize($newTargetFile));
					$iInsertID=library_add($_POST['stype'],$oldFileName,$newName,$newTargetFile,$sFileSize,$ComID);
					$arrReturn[0]['InsertID']=$iInsertID;
					if ($iInsertID > 0) {
//						library_convert($iInsertID);
						$arrReturn[0]['Return']=true;
						$arrReturn[0]['Result']='Upload to '.$newTargetFile;
					}
				} else {
					$arrReturn[0]['Result']='File Type Not Exists support only '+$_POST['extension'];
				}
		} else  //Multiple files, file[]
		{
	  	$fileCount = count($_FILES["myfile"]["name"]);
	  	for($i=0; $i < $fileCount; $i++)	{
	
				$oldFileName = strtolower($_FILES['myfile']['name'][$i]);
				$tempFile = $_FILES['myfile']['tmp_name'][$i];
				$fileParts = pathinfo(strtolower($_FILES['myfile']['name'][$i]));
				$arrReturn[$i]['Return']=false;
				$arrReturn[$i]['Result']="Error";
				$arrReturn[$i]['InsertID']=0;
				if (in_array($fileParts['extension'],$fileTypes)) {
					$newName=sprintf("%s",date('YmdHis'));
					$newFileName = sprintf('%s.%s',$newName,$fileParts['extension']);
					$newTargetFile=  rtrim($targetPath,'/') . '/' .$newFileName;
					move_uploaded_file($tempFile,$newTargetFile);
					$sFizeSize='0';
					if (file_exists($newTargetFile)) $sFileSize=sprintf("%d",filesize($newTargetFile));
//					$iInsertID=library_add($_POST['stype'],$oldFileName,$newName,$newFileName,$sFileSize,$_POST['ComID']);
					$iInsertID=library_add($_POST['stype'],$oldFileName,$newName,$newTargetFile,$sFileSize,$ComID);
					$arrReturn[$i]['InsertID']=$iInsertID;
					if ($iInsertID > 0) {
//						library_convert($iInsertID);
						$arrReturn[$i]['Return']=true;
						$arrReturn[$i]['Result']='Upload to '.$newTargetFile;
					}
				} else {
					$arrData[$i]['Result']='File Type Not Exists support only '+$_POST['extension'];
				}
	  	}
		}
    echo json_encode($arrReturn);
 	}
 ?>