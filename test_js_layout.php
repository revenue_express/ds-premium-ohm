<? 
	include("./includes/db_config.inc.php");
	include("./includes/sys_config.inc.php");
	$DEBUG=TRUE;
?>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<script type="text/javascript" src="./js/jquery-1.7.2.min.js"></script>
<script type="text/javascript" src="./js/jquery-ui-1.8.21.custom.min.js"></script>
<script type="text/javascript" src="./js/checkValid.js"></script>
<script type="text/javascript" src="./js/signage.js"></script>
</script>
<!--link rel="stylesheet" href="./css/display.css"-->
<link rel="stylesheet" href="./css/custom-theme/jquery-ui-1.8.22.custom.css">
<script type="text/javascript" src="./js/layout.js"></script>
<script type="text/javascript">
	$('document').ready(function() {
		load_headlist();
	});
	
	function add_new() {
		ShowObject('dvHeadResult',false,'');
		ShowObject('dvHeadAddForm',true,'');
	}
</script>
<style type="text/css">
.spMenu { cursor: pointer; }
.tdFileSize { text-align:right; padding-right: 5px;}
</style>
</head>
<body>
<div>
<?	
	if ($DEBUG) {
		echo "request<pre>"; print_r($_REQUEST); echo "</pre>"; 
	}
?>
</div>
<div id="dvHeadResult">
<table width="100%" border="1">
	<thead>
  <tr>
  	<th>Layout Name / Template</th>
    <th>Status</th>
    <th colspan="4" nowrap>
    	<input class="ui-button" type="button" id="btnHeadRefresh" value="Refresh" onClick="load_headlist();"/>
    </th>
  </tr>
  </thead>
  <tbody id="showHead_Data">
  </tbody>
</table>
</div>
<div id="dvLayout" style="display:none; ">
<?
$onBack="ShowObject('dvLayout',false,'');";
$onBack.="ShowObject('dvHeadResult',true,'');";
$onBack.="ShowObject('dvSelectFile',false,'');";
$onBack.="ShowObject('dvSelectPlaylist',false,'');";
$onBack.="load_headlist();";
$onLoad="ShowObject('dvSelectPlaylist',true,'');";
$onLoad.="ShowObject('dvShowPlayListItem',false,'');";
$onLoad.="ShowObject('dvSelectFile',false,'');";
$onLoad.="layout_playlist_show(document.getElementById('HeadID').value,document.getElementById('TypeID').value);";
?>
	<span><input class="ui-button" type="button" id="btnHeadRefresh" value="Back" onClick="<?=$onBack;?>"/></span>
  <span id="LineMenu"></span>
	<div id="LineData" style="display:none;height:155px; width:700px; overflow:auto">
  <table border='1' width="100%">
  <tr>
  <th width="475">File</th>
  <th width="125">Size</th>
  <th colspan="3" width="50"><input class="ui-button" type="button" id="btnLoadPlaylist" value="Load" onClick="<?=$onLoad;?>" /></th>
  <th><img src="./images/icons/add.png" width="16" height="16" title="add" onClick="ShowObject('dvSelectFile',true,'');layout_load_library(document.getElementById('TypeID').value);"></th></tr>
  <tbody id="LineData_Body">
  </tbody>
  </table>
  </div>
</div>
<div id="dvSelectFile" style="display:none;">
<div style="height:150px; width:700px; overflow:auto;">
<table border='1' width="100%">
	<thead>
  <tr><th width="40"></th><th>File Display</th><th width="20%">Size</th></tr>
  </thead>
  <tbody id="tblSelectFile_Body">
  </tbody>
</table>
<?
$onSelectFile="layout_playlist_addfile(document.getElementById('HeadID').value,";
$onSelectFile.="document.getElementById('TypeID').value,";
$onSelectFile.="document.getElementById('FileID').value);";
?>
</div>
<div style="height:150px; width:700px;">
<table width="100%" border='1'>
<tr>
<td>You select <input type="text" size="40" id="FileName"/>
<input type="hidden" id="FileID" value='' size="4"/>
<input type="hidden" id="HeadID" value='' size="4"/>
<input type="hidden" id="TypeID" name="TypeID" value="" size="4">
<input type="button" id='btnSelectFile' value="Selected" disabled onClick="<?=$onSelectFile;?>"/></td></tr>
</table>
</div>
</div>

<div id="dvSelectPlaylist" style="display:none;">
<div id="dvSelectPlaylist_head" style="height:150px; width:700px; overflow:auto;">
<table border='1' width="100%">
	<thead>
  <tr><th width="40"></th><th>Playlist Name</th><th width="20%">Item(s)</th><th width="20%">Size(KB)</th></tr>
  </thead>
  <tbody id="tblSelectPlaylist_Body">
  </tbody>
</table>
<?
$onSelectPlaylist="layout_playlist_add(document.getElementById('HeadID').value,";
$onSelectPlaylist.="document.getElementById('TypeID').value,";
$onSelectPlaylist.="document.getElementById('PlayListID').value);";
?>
</div>
<div id="dvSelectPlaylist_form" style="height:150px; width:700px;">
<table width="100%" border='1'>
<tr>
<td>You select <input type="text" size="40" id="PlayListName"/>
<input type="textbox" id="PlayListID" value='' size="4"/>
<input type="button" id='btnSelectPlaylist' value="Selected" disabled onClick="<?=$onSelectPlaylist;?>"/></td></tr>
</table>
</div>
<div id="dvShowPlayListItem" style="height:150px; width:700px;">
<table border='1' width="100%">
	<thead>
  <tr><th>File Display</th><th width="20%">Size</th></tr>
  </thead>
  <tbody id="tblShowPlayListItem">
  </tbody>
</table>
</div>
</div>
</body>
</head>