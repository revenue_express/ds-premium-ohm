<? 
	include("./includes/db_config.inc.php");
//	include("./includes/sys_config.inc.php");
	include("./functions/command.func.php");
	$DEBUG=TRUE;
	$CSSName=NULL;
	$LayoutItems=NULL;
	$Data=NULL;
	$ClientID=8;
	foreach ($_REQUEST as $Key => $Value) { $$Key=addslashes(urldecode($Value)); }
	$ComID=1;
	if (isset($btnSubmit)) {
		$Item=json_decode(command_list($ComID,$ClientID,$selCommand,$selCompleted),true);
	} else {
		$Item=json_decode(command_list($ComID,$ClientID),true);
	}
?>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<script type="text/javascript" src="./js/jquery-1.7.2.min.js"></script>
<script type="text/javascript" src="./js/jquery-ui-1.8.21.custom.min.js"></script>
<script type="text/javascript" src="./js/jquery-ui-timepicker-addon.js"></script>
<script type="text/javascript" src="./js/checkValid.js"></script>
<script type="text/javascript" src="./js/signage.js"></script>
<link rel="stylesheet" href="./css/custom-theme/jquery-ui-1.8.22.custom.css">
</head>
<body>
<form>
<select name="selCommand">
<option value="Download File">Download File</option>
<option value="Send Schedule">Send Schedule</option>
</select>
<select name="selCompleted">
<option value="-1">All Status</option>
<option value="1">Only successful</option>
<option value="0">Only Send</option>
</select>
<input type="submit" name="btnSubmit" value="Find">
</form>
<?	showData($Item);?>
</body>
</html>	