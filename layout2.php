<? 
	include("./includes/db_config.inc.php");
	include("./includes/sys_config.inc.php");
	$DEBUG=FALSE;
?>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<?	include("./javascript.php");?>
<link rel="stylesheet" href="css/custom-theme/jquery-ui-1.8.23.custom.css">
<link rel="stylesheet" href="css/StyleSheet.css">
<?	include("./layout.js.php");?>
<script type="text/javascript" src="./js/layout.js"></script>
<script type="text/javascript" src="./js/playlist.js"></script>
<script type="text/javascript">
	$(function() {
		//$( "input:submit, input:button,  input:reset, a, button", ".demo" ).button();
		//$("input:submit, input:button,  input:reset, a, button", ".demo").removeClass()
		//$("input:submit, input:button,  input:reset, a, button", ".demo").height(25);
	});
	
	$('document').ready(function() {
		load_headlist();
	});
	
	$(function(){
		$('#txtExistPlaylist,#txtExistFile,#txtExistFlash,#txtPara').height(15);
	});
</script>
<style type="text/css">
.spMenu {
	background-color:;
	font-size:x-large;
	color:#FFF;
	text-shadow:#CCC;
	font-weight:bolder;
	padding-top:10px;
	padding-bottom: 10px;
	padding-left:30px;
	padding-right:30px;
}
</style>
</head>
<body>
<? $onBack="clear_allsection();ShowObject('dvHeadResult',true,'');load_headlist();";?>
<div class="w-100 boxin">
	<div class="header">
    	<h3>Template Management</h3>
    </div>
	<div><?		if ($DEBUG) {		echo "request<pre>"; print_r($_REQUEST); echo "</pre>"; } ?></div>

    <div id="dvHeadResult">
    <table cellspacing="0">
        <thead>
          <tr>
            <th>Template Name / Layout Name</th>
            <th>Status</th>
            <th class="w-10" colspan="4" nowrap>
                <input class="btnTh" type="button" id="btnHeadRefresh" value="Refresh"onClick="load_headlist();">
                &nbsp;&nbsp;
                <input class="btnTh" type="button" id="btnHeadAdd" value="Add" onClick="layout_add_new();"/>
            </th>
          </tr>
      	</thead>
      	<tbody id="showHead_Data">
      	</tbody>
    </table>
    </div>
</div>
<div id="dvHeadAddForm" class="top15">
<span><input class="btnBack" type="button" id="btnHeadRefresh" onClick="<?=$onBack;?>"/></span>
<? 
	$Url=sprintf("%s?FName=%s&TID=%s&TName=%s&BName=%s",'popup_template.php','formLayoutAdd','TemplateID','txtTemplateAdd','btnSubmit');
	$Option=sprintf("width=%d,height=%d,location=no,menubar=no,resizeable=no,scrollbars=no,status=no,toolbar=no",710,280);
	$onAddLayout=sprintf("window.open('%s','_template','%s');",$Url,$Option);
	
	$sSubmit="You can not change template Layout. Do you confirm to add this layout ?";
	$sAjax="layout_add(document.getElementById('txtName').value";
	$sAjax.=",document.getElementById('TemplateID').value";
	$sAjax.=",document.getElementById('txtTemplateAdd').value);";
//	$onSubmit=sprintf("if (confirm('%s')) {document.getElementById('%s').submit(); }",$sSubmit,'formLayoutAdd');
	$onSubmit=sprintf("if (confirm('%s')) {%s }",$sSubmit,$sAjax);

	$onReset="document.forms['formLayoutAdd'].reset();EnableObject('btnSubmit',false)";
?>
	<div class="w-50 top15">
    	<div class="well boxin" id="tblFormAdd">
            <form id="formLayoutAdd" name="formLayoutAdd" method="post">
            <input type="hidden" id="btnLayoutAdd" name="btnLayoutAdd" value="LayoutAdd"/>
            <input class="txt" type="hidden" id="TemplateID" name="TemplateID" value=""/>
            <div class="left head">Template Information</div>
            <div class="dvtr top15">
                <div class="dvtd_left w-25 top5">Template Name : </div>
              	<div class="dvtd_right"><input type="text" class="txt" id="txtName" name="txtName" size="35" maxlength="64"/></div>
            </div>
           	<div class="dvtr top5">
            	<div class="dvtd_left w-25 top5">Layout Name : </div>
				<div class="dvtd_right">
              		<input type="text" id="txtTemplateAdd" class="txt" name="txtTemplateAdd" size="35" maxlength="64" readonly/></div>
                    <div class="demo dvtd_left"><input type="button" id="btnSelTemplate" value="Select Template" onClick="layout_show_template();"/></div>			
           	</div>
           	<div class="dvtr top15 demo">
            	<div class="dvtd_left w-50">
              		<input type="button" id="btnSelTemplateSave" name="btnSelTemplateSave" value="Add Template" onClick="<?=$sAjax;?>"/>
                </div>
                <div class="dvtd_right">
            		<input type="button" id="btnReset" value="Reset" onClick="<?=$onReset;?>"/>
           		</div>
            </div>
            </form>
		</div>
    </div>
</div>

<div id="dvLayout" class="top15">
<span><input class="btnBack" type="button" id="btnHeadRefresh"  onClick="<?=$onBack;?>"/></span>
<? 
$onView="window.open('');";
$onChange="EnableObject('btnSaveLayout',false);";
$onChange.=sprintf("changeInputValue('txtLayoutName','%s',document.getElementById('txtLayoutName').value);",$sDisplay);
$onChange.="EnableObject('btnSaveLayout',! checkEqual('txtLayoutName','txtOldLayoutName'));";
$onSave="layout_head_edit(document.getElementById('HeadID').value,";
$onSave.="document.getElementById('txtLayoutName').value)";
?>
<div class="well boxin w-50">
    <div class="left head"><h3>Template Information</h3></div>
    <div class="dvtr top15">
        <div class="dvtd_left top5 w-25">Template Name : </div>
        <div class="dvtd_right demo">
        <input type="text" class="txt" id="txtLayoutName" name="txtLayoutName" size="32" maxlength="64" readonly disabled/>
        <span><input class="ui-button" type="button" id="btnLayoutName" value="Change Name" onClick="<?=$onChange;?>" />
        <input type="hidden" id="txtOldLayoutName" readonly disabled>
        <input class="ui-button" type="button" id="btnSaveLayout" value="Save" onClick="<?=$onSave;?>" disabled/></span>
        </div>
    </div>
    <div class="dvtr top5">
    <? $onLayoutAdd=sprintf("layout_popup(document.getElementById('TemplateID').value);"); ?>
        <div class="dvtd_left top5 w-25">Layout Name : </div>
        <div class="dvtd_right demo">
        <input type="text" class="txt" id="txtTemplate" name="txtTemplate" size="32" maxlength="64" readonly disabled/>
        <input class="ui-button" type="button" id="btnLayoutView" value="View" onClick="<?=$onLayoutAdd;?>" />
        </div>
    </div>
</div>
<div class="boxin w-80" style="margin-top:10px;" id="LineMenu"></div>
<div id="LineData" class="boxin w-80">
  <div class="header">
      <h3><span class="spTypeName">Image</span><span> Playlist</span></h3>
    </div>
    <div id="dvHeaderFeedShow" class="boxno w-90" style="margin-left:5%; margin-top:10px;">
        <marquee id="FeedAll" direction="left" vspace="5" hspace="5">Example Feeding. 1 2 3 4 5 6 7 8 9 0</marquee>
    </div>
    <div id="dvPlaylistHeader" class="top10" style="margin-bottom:10px;">
    <?
        $onChange="EnableObject('btnHeadSave',false);";
        $sDisplay="Do you Comfirm to Change Playlist Name?";
        $onChange.=sprintf("changeInputValue('txtHeadName','%s',document.getElementById('txtHeadName').value);",$sDisplay);
        $onChange.="EnableObject('btnHeadSave',! checkEqual('txtHeadName','txtOldName'));";
        
        $onReset="EnableObject('btnHeadSave',false);";
        $onReset.="setObjValue('txtOldName','txtHeadName');";
        ?>
    &nbsp; &nbsp;  Playlist Name :
    <input class="txt" size="40" type="text" id="txtHeadName" name="txtHeadName" readonly/>
    <input class="txt" size="40" type="hidden" id="txtOldName" name="txtOldName" />
    <span class="demo">
    <input class="ui-button-text" type="button" id="btnHeadChange" value="Change" onClick="<?=$onChange;?>">
    <input class="ui-button-text" type="button" id="btnHeadSave" value="Save" onClick="playlist_head_save('HeadID','txtHeadName');" disabled>
    <input class="ui-button-text" type="button" id="btnHeadReset" value="Reset" onClick="<?=$onReset;?>">
    <input class="ui-button-text" type="button" value="Playlist Option" onClick="playlist_head_option('HeadID','TypeID');"/>
    
    <span id="spBtnFeedShow">
      <input id="btnFeedShow" class="ui-button-text" type="button" value="Feed Preview" onClick="feed_head_preview('FeedAll',document.getElementById('HeadID').value);"/>
    </span>
    </span>
</div>
<div id="dvHeaderPlaylist" class="header left">
  <h3><span class="spTypeName">Flash</span><span> Playlist Items</span>
  <span class="demo" style="margin-left:20">
  <input id="btnHeaderRefresh" class="ui-button-text" type="button" value="Refresh" onClick="playlist_refresh(document.getElementById('HeadID').value);"/>
  <input id="btnHeaderLibrary" class="ui-button-text" type="button" value="Library" onClick="show_file_exists();"/>
  <input id="btnHeaderPlaylist" class="ui-button-text" type="button" value="Playlist Exists" onClick="show_playlist_exists();"/>
  <input id="HeadID" type="hidden" size="2"/> <input id="TypeID" type="hidden" value="3" size="2"/>
  </span></h3>
</div>
<div id="dvLineFeedShow" class="boxno w-90" style="margin:10px 0 10px 5%;">
    <marquee id="feedLinePreview" direction="left" vspace="5" hspace="5">Example Feeding. 1 2 3 4 5 6 7 8 9 0</marquee>
</div>
    <table cellspacing="0" class="w-100">
    <tbody id="tblLine_Result">
      <tr>
        <td class="w-80 center"></td>
        <td class="w-20 center">
          <div class="dvtr top5 w-100">
             <div class="dvtd_left w-20 center"><img class="preview" src="images/icons/preview.png" title="preview"></div>
             <div class="dvtd_left w-20 center"><img class="preview" src="images/icons/down.png" title="down"></div>
             <div class="dvtd_left w-20 center"><img class="preview" src="images/icons/up.png" title="up"></div>
             <div class="dvtd_left w-20 center"><img class="preview" src="images/icons/edit.png" title="edit"></div>
             <div class="dvtd_left w-20 center"><img class="preview" src="images/icons/delete.png" title="delete"></div>
          </div>
        </td>
      </tr>
    </tbody>
    </table>
  </div>
</div>
<script type="text/javascript">
	$(function() {
		$('#dvSelectFlash').dialog({ autoOpen: false , show: "blind", hide: "explode"});
		$('#dvSelectFlash').dialog("option","width",900);
		$('#dvSelectFlash').dialog("option","height",275);
		$('#dvSelectFlash').dialog("option","resizable",false);
	});
</script>
<div id="dvSelectFlash" title="Selection Exists Image.">
<div id="dvSelectFileItem" style="min-height:50px; height:190px; overflow:auto">
<table cellspacing="0">
  <tbody id="tblSelectFlash_Body">
  <tr><td><input type="radio"></td><td>111111111111111111111111111111111111111111111</td></tr>
  </tbody>
</table>
</div>
<div class="pagination">
	<div class="dvtr">
        <div class="dvtd_left w-15" style="margin-top:15px;">You select : </div>
        <div class="dvtd_right"><input style="margin-top:13px;" class="txt" type="text" size="20" id="txtExistFlash"/></div>
        <div class="dvtd_right" style="margin-top:15px;">Parameter : </div>
        <div class="dvtd_right"><input style="margin-top:13px;" class="txt" type="text" id="txtPara" value='' size="20"/>
        </div>
		<div class="dvtd_right demo">
			<input type="hidden" id="SelectFlashID" value='' size="4"/>
			<input type="button" style="margin-top:13px;" id='btnSelectFlash' value="Selected" disabled onClick="layout_exist_file();"/>
		</div>
	</div>
</div>
</div>
<script type="text/javascript">
	$(function() {
		$('#dvSelectFile').dialog({ autoOpen: false , show: "blind", hide: "explode"});
		$('#dvSelectFile').dialog("option","width",900);
		$('#dvSelectFile').dialog("option","height",275);
		$('#dvSelectFile').dialog("option","resizable",false);
	});
</script>
<div id="dvSelectFile" title="Selection Exists File.">
    <div id="dvSelectFileItem" style="height:190px; overflow:auto">
    <table cellspacing="0">
      <tbody id="tblSelectFile_Body" >
      <tr><td><input type="radio"></td><td>111111111111111111111111111111111111111111111</td></tr>
      </tbody>
    </table>
    </div>
    <div class="pagination">
        <div class="dvtr">
            <div class="dvtd_left w-25" style="margin-top:15px;">You select : </div> 
            <div class="dvtd_right">
            	<input  class="txt" style="margin-top:13px;" type="text" size="40" id="txtExistFile"/>
            	<input type="hidden" id="SelectFileID" value='' size="4"/></div>
            <div class="demo dvtd_right" style="padding-top:13px;">
            	<input type="button" id='btnSelectFile' value="Selected" disabled onClick="layout_exist_file();"/>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
	$(function() {
		$('#dvSelectPlaylist').dialog({ autoOpen: false , show: "blind", hide: "explode"});
		$('#dvSelectPlaylist').dialog("option","width",900);
		$('#dvSelectPlaylist').dialog("option","height",275);
		$('#dvSelectPlaylist').dialog("option","resizable",false);
	});
</script>
<div id="dvSelectPlaylist" title="Selection Exists Playlist.">
<div id="dvSelectPlaylistItem" style="min-height:50px; height:190px; overflow:auto">
<table cellpadding="0" cellspacing="0">
  <tbody id="tblSelectPlaylist_Body">
  <tr><td><input type="radio"></td><td>111111111111111111111111111111111111111111111</td></tr>
  </tbody>
</table>
</div>
<div class="pagination">
	<div class="dvtr">
        <div class="dvtd_left w-25" style="margin-top:15px;">You select : </div>
        <div class="dvtd_right">
        	<input class="txt" style="margin-top:13px;" type="text" size="40" id="txtExistPlaylist"/>
			<input type="hidden" id="SelectPlaylistID" value='' size="4"/></div>
        <div class="demo dvtd_right" style="padding-top:13px;">
        	<input type="button" id='btnSelectPlaylist' value="Selected" disabled onClick="layout_exist_playlist();"/>
        </div>
	</div>
</div>
</div>
<script type="text/javascript">
	$(function() {
		$('#dvOption').dialog({ autoOpen: false , show: "blind", hide: "explode"});
		$('#dvOption').dialog("option","width",500);
		$('#dvOption').dialog("option","height",205);
		$('#dvOption').dialog("option","resizable",false);
	});
</script>
<div id="dvOption" title="Playlist Option Selection">
<table>
  <tbody id="tblOption_Body">
  <tr><td><input type="radio"></td><td>111111111111111111111111111111111111111111111</td></tr>
  </tbody>
</table>
</div>
<script type="text/javascript">
	$(function() {
		$('#dvSelectLayout').dialog({ autoOpen: false , show: "blind", hide: "blind"});
		$('#dvSelectLayout').dialog("option","width",600);
		$('#dvSelectLayout').dialog("option","height",285);
		$('#dvSelectLayout').dialog("option","resizable",false);
	});
</script>
<div id="dvSelectLayout" title="Layout Selection">
	<div class="boxin">
    <div id="dvSelectPlaylistItem" style="min-height:50px; max-height:185px; overflow:auto">
    <table cellspacing="0">
      <tbody id="tblSelectLayout_Body">
      <tr><td><input type="radio"></td><td>111111111111111111111111111111111111111111111</td></tr>
      </tbody>
    </table>
    </div>
    <div class="pagination top15 left">
    <span style="margin-left:10%">&nbsp; &nbsp; You select : </span> 
    <span class="demo">
    <input  class="txt" type="text" size="40" id="txtExistLayout"/>
    <input type="hidden" id="SelectLayoutID" value='' size="4"/>
    <input type="button" id='btnSelectLayout' value="Selected" disabled onClick="layout_select_template(parseInt(document.getElementById('SelectLayoutID').value),document.getElementById('txtExistLayout').value);"/>
		</span>
    </div>
  </div>
</div>
<script type="text/javascript">
	$(function() {
		$('#dvAddText').dialog({ autoOpen: false });
		$('#dvAddText').dialog("option","width",900);
		$('#dvAddText').dialog("option","height",100);
		$('#dvAddText').dialog("option","resizable",false);
	});
</script>
<div id="dvAddText" title="Add New Feed Item.">
<div class="demo">
<input type="text" size="72" id="txtNewFeed"/>
<input type="text" id="FeedID" value='' size="2"/>
<input type="text" id="LineID" value='' size="2"/>
<input type="button" id='btnFeedAdd' value="Save" onClick="feed_line_add();"/>
</div>
</div>
<script type="text/javascript">
	$(function() {
		$('#dvEditText').dialog({ autoOpen: false });
		$('#dvEditText').dialog("option","width",900);
		$('#dvEditText').dialog("option","height",100);
		$('#dvEditText').dialog("option","resizable",false);
	});
</script>
<div id="dvEditText" title="Edit Feed Item.">
<div class="demo">
<input type="text" size="72" id="txtFeed_Edit" value=""/>
<input type="checkbox" id="chkFeedOnly" value="ThisOnly"/>This Only
<input type="text" id="FeedID_Edit" value='' size="2"/>
<input type="text" id="LineID_Edit" value='' size="2"/>
<input type="button" id='btnFeedEdit' value="Save" onClick="feed_line_save();"/>
</div>

</body>
</head>