<head>
<?php 
	$DEBUG=TRUE;
	$TypeID=11;
	include("./includes/db_config.inc.php");
	include("./functions/library.func.php");
	include("./javascript.php");
?>
<!-- http://rvera.github.io/image-picker/ -->
<script type="text/javascript" src="js/image-picker.js"></script>
<link rel="stylesheet" href="css/image-picker.css">
<script type="text/javascript" src="js/msdropdown/jquery.dd.js"></script>
<link rel="stylesheet" href="css/msdropdown/dd.css">
<style>
#selDropDown {
	width: 120px;
}
</style>
</head>
<body>
<form method="post">
<?php
	$arrData=json_decode(library_list('1',$TypeID),true);
?>
<select id="selGallery" name="selGallery" class="image-picker limit_callback show-html shows-labels" data-limit="10" multiple="multiple">
<?php
	for($iRun=0;$iRun<count($arrData); $iRun++) { 
		$fileParts=pathinfo($arrData[$iRun]['file_sname']);
		$imgSrc=sprintf("./data/%s/%s_g.%s",$arrData[$iRun]['file_path'],$fileParts['filename'],$fileParts['extension']);
?>
	<option data-img-src='<?php echo $imgSrc;?>' value='<?php echo $arrData[$iRun]['file_id'];?>' data-img-label='<?php echo $arrData[$iRun]['file_dname'];?>'>
	<?php echo $arrData[$iRun]['file_oname'];?></option>
<? 
	}
?>
</select>
<select id="selImage" name="selImage" class="image-picker limit_callback show-html" data-limit="10" multiple="multiple">
<?php
	for($iRun=0;$iRun<count($arrData); $iRun++) { 
		$fileParts=pathinfo($arrData[$iRun]['file_sname']);
		$imgSrc=sprintf("./data/%s/%s_s.%s",$arrData[$iRun]['file_path'],$fileParts['filename'],$fileParts['extension']);
		$selected="";
		echo sprintf("\n<option data-img-src='%s' value='%s' data-img-label='%s' title='XXXX'>%s</option>",$imgSrc,$arrData[$iRun]['file_id'],$arrData[$iRun]['file_oname'],$arrData[$iRun]['file_oname']);
	}
?>
</select>
<select id="selDropDown" name="selDropDown" style="width:550px;">
<?php
	for($iRun=0;$iRun<count($arrData); $iRun++) { 
		$fileParts=pathinfo($arrData[$iRun]['file_sname']);
		$selected="";
		$imgSrc=sprintf("./data/%s/%s_s.%s",$arrData[$iRun]['file_path'],$fileParts['filename'],$fileParts['extension']);
		$imgDesc=sprintf("%s", $arrData[$iRun]['file_oname']);
		$imgID=$arrData[$iRun]['file_id'];
		if ($_REQUEST['selDropDown']==$imgID) $selected=' selected';
?>
	<option data-image='<?php echo $imgSrc;?>' value='<?php echo $imgID;?>' data-description='<?php echo $imgDesc;?>'<?php echo $selected;?>><?php echo $arrData[$iRun]['file_sname'];?></option>
<? 
	}
?>
</select>
    <select id="payments" name="payments" style="width:250px;">
        <option value="" data-description="Choos your payment gateway">Payment Gateway</option>
        <option value="amex" data-image="./images/msdropdown/icons/Amex-56.png" data-description="My life. My card...">Amex</option>
        <option value="Discover" data-image="./images/msdropdown/icons/Discover-56.png" data-description="It pays to Discover...">Discover</option>
        <option value="Mastercard" data-image="./images/msdropdown/icons/Mastercard-56.png" data-title="For everything else..." data-description="For everything else...">Mastercard</option>
        <option value="cash" data-image="./images/msdropdown/icons/Cash-56.png" data-description="Sorry not available..." disabled="true">Cash on devlivery</option>
        <option value="Visa" data-image="./images/msdropdown/icons/Visa-56.png" data-description="All you need...">Visa</option>
        <option value="Paypal" data-image="./images/msdropdown/icons/Paypal-56.png" data-description="Pay and get paid...">Paypal</option>
    </select> &nbsp;
<br /><button type="submit">Submit</button>
</form>
<script>
	$("#selImage").imagepicker({show_label:true});
	$("#selGallery").imagepicker({show_label:true});
	$("#selDropDown").msDropDown({visibleRows:4});
	$("#payments").msDropdown({visibleRows:4});
	
	$("#selImage").data('picker');
</script>
<span title="<?php echo $_RETURN;?>">Session</span>
<?php 
$arrFile=getimagesize('./images/back.png');
	echo "<pre>"; print_r($arrFile); echo "</pre>";
?>
</body>
