<? 
	$DEBUG=FALSE;
	include("./includes/db_config.inc.php");
	include("./functions/schedule_client.func.php");
	include("./functions/client.func.php");
//	$DEBUG=TRUE;
	$Data=NULL;
//	load_config();
	$onKeyPress="KeyPress('txtMessage',5,'btnMessage')";
?>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<?	include("./javascript.php");?>
<link rel="stylesheet" href="css/custom-theme/jquery-ui-1.8.23.custom.css">
<link rel="stylesheet" href="css/StyleSheet.css">
<style type="text/css"> 
.Schedule table { font-size:!important 10px; }
.Schedule table th { font-size:!important 10px; }
.Schedule table td { font-size:!important 10px; }
</style>
<script type="text/javascript" src="./js/schedule_client.js"></script>
<script type="text/javascript" src="./js/home.js"></script>
<script type="text/javascript">
	$(document).ready(function() {
		$("#dvInfoBody").accordion({ active: parseInt($('#TabSelected').val())});//"option", "active", 2
		$('#dvInfoBody tr:odd').addClass('even');
	});

<?
	if ( (! isset($ClientID)) || ($ClientID < 1) ){
		echo sprintf("window.location='http://%s/%s/home.php';",$_SERVER["SERVER_ADDR"],$LocalPath);
	}
?>
</script>
</head>
<body>
<? 
	if ($DEBUG) { echo "<br>Session"; echo "<pre>"; print_r($_SESSION); echo "</pre>";}
	$TabSelected=0;
	$URL=sprintf("%s/ajax/client_info.php?ClientID=%s",$LocalHost,$ClientID);
	$Data=file_get_contents($URL);
	$Client=json_decode($Data,true);
//	echo "Client<pre>"; print_r($Client); echo "</pre>";

	$URL=sprintf("%s/ajax/schedule_list.php?ComID=%d",$LocalHost,$ComID);
	$Data=file_get_contents($URL);
	$Schedule=json_decode($Data,true);
	if ($DEBUG) { echo "<br>Schedule=> ".$URL; echo "<pre>"; print_r($Schedule); echo "</pre>";}

	$InUsed=json_decode(schedule_client_list_from('Client',$ClientID),true);
	if ($DEBUG) { echo "<br>InUsed"; print_r($InUsed); }
	$SchUsed=array();
	for($iCount=0; $iCount < count($InUsed); $iCount++) {
		$SchUsed[]=$InUsed[$iCount]['schc_schh_id'];
	}
	if ($DEBUG) { echo "<br>SchUsed"; echo "<pre>"; print_r($SchUsed); echo "</pre>";}

	$schList=array();
	for ($iCount=0; $iCount<count($Schedule); $iCount++) {
		if (! in_array($Schedule[$iCount]['schh_id'],$SchUsed)) {
			$schList[]=$Schedule[$iCount];
		}
	}
	if ($DEBUG) { echo "<br>schList"; echo "<pre>";print_r($schList); echo "</pre>";}
?>
<script type="text/javascript">
	$('document').ready(function() {
		$('#tabs').tabs();
		$('#dvMonitor').dialog({ autoOpen: false });
		$('#dvMonitor').dialog("option","width",750);
		$('#dvMonitor').dialog("option","height",275);
		$('#dvMonitor').dialog("option","resizable",false);
	});
</script>
<div id="dvMonitor" title="Configuration of Client '<?=$Client[0]['cli_name'];?>'">
  <div class="dvtr w-100 top10">
  	<div class="dvtd_right w-10">Player : </div>
    <div class="demo dvtd_left w-80 left">
		<input class="btnSky" type="button" id="btnRestart" onClick="client_restart();" value="Restart">
		<input class="btnSky" type="button" id="btnShutdown" onClick="client_shutdown();" value="Shutdown">
    </div>
  </div>
  <div class="dvtr w-100 top10">
  	<div class="dvtd_right w-10">Template : </div>
    <div class="demo dvtd_left w-80 left">
		<input class="btnSky" type="button" id="btnDefault" onClick="client_default();" value="Default">
		<input class="btnSky" type="button" id="btnRestore" onClick="client_restore();" value="Restore">
		</div>
  </div>
  <div class="dvtr w-100 top10">
  	<div class="dvtd_right w-10 top5">Volumn : </div>
    <div class="demo dvtd_left w-75 left">
      <span class="demo">
      <span style="font-family:'Lucida Console', Monaco, monospace; font-size:14px;">
     <select id="selLoud">
      <? for ($iCount=1; $iCount<=5; $iCount++) { 
//				$num="          ".(string)$iCount;
//				$data=substr($num,strlen($num)-3,3);
//				$data=str_replace(' ','&nbsp;',$data);
//				echo sprintf("\n<option  value='%d'>%s %%</option>",$iCount,$data); 
				echo sprintf("\n<option  value='%d'>%d %%</option>",$iCount,($iCount*20)); 
			}?>
      </select>
      </span>
      <input class="btnSky" type="button" id="btnLoud" onClick="client_loud();" value="Save">
      </span>
      <span><input class="btnSky" type="button" id="btnMute" onClick="client_mute();" value="Mute"></span>
    </div>
  </div>
  <div class="dvtr w-100 top10">
  	<div class="dvtd_right w-100">
    	<span class="demo">
      <span class="top5">	<input type="text" class="txt" id='txtMessage' size="60" onKeyPress="<?=$onKeyPress;?>"></span>
    	<span><input class="btnSky" type="button" id="btnMessage" onClick="client_urgent();" value="Send Urgent" disabled></span>	
    	<span><input class="btnSky" type="button" id="btnResetMessage" onClick="client_reset_urgent();" value="Reset Urgent"></span>	
    	</span>
    </div>
  </div>
  <div class="dvtr w-100 top10">
  	<div class="dvtd_right w-20 top5">Download Period : </div>
    <div class="demo dvtd_left w-75 left">
      <span class="demo">
      <span style="font-family:'Lucida Console', Monaco, monospace; font-size:14px;">
     <select id="selStart">
      <? for ($iCount=0; $iCount<=23; $iCount++) { 
//				$num="          ".(string)$iCount;
//				$data=substr($num,strlen($num)-3,3);
//				$data=str_replace(' ','&nbsp;',$data);
				echo sprintf("\n<option  value='%d'>%02d:00</option>",$iCount,$iCount); 
//				echo sprintf("\n<option  value='%d'>%d %%</option>",$iCount,($iCount*20)); 
			}?>
      </select>
		- 
     <select id="selStop">
      <? for ($iCount=0; $iCount<=23; $iCount++) { 
//				$num="          ".(string)$iCount;
//				$data=substr($num,strlen($num)-3,3);
//				$data=str_replace(' ','&nbsp;',$data);
				echo sprintf("\n<option  value='%d'>%02d:00</option>",$iCount,$iCount); 
//				echo sprintf("\n<option  value='%d'>%d %%</option>",$iCount,($iCount*20)); 
			}?>
      </select>
      </span>
      </span>
      <span><input class="btnSky" type="button" id="btnTimer" onClick="client_timer();" value="Send Period"></span>
    </div>
  </div>
  <input type="hidden" id="ServerIP" size="2" value="<?=$_SERVER['SERVER_ADDR'];?>">
  <input type="hidden" id="ClientID" size="2" value="<?=$ClientID;?>">
  <input type="hidden" id="ClientIP" size="16" value="<?=$Client[0]['cli_address'];?>">
  <input type="hidden" id="PortNo" size="2" value="3001">
</div>

<div class="boxin w-50" id="dvScheduleAdd">
  <div class="header">
  <span>Add schedule to client "<?=$Client[0]['cli_name'];?>"</span>
  </div>
	<div class="pagination">
  	<div class="dvtd_right w-20 top10">Schedule : </div>
    <div class="dvtd_left left top5">
    <select id='selSchedule' style="font-size:14px;">
<? for ($iSch=0; $iSch < count($schList); $iSch++) { 
?>
    <option value="<?=$schList[$iSch]['schh_id']?>"><?=$schList[$iSch]['schh_name']?></option>
<?	} ?>
    </select>
    <input type="button" class="btnSky" value="Add" onClick="schedule_client_add(<?=$ClientID?>,document.getElementById('selSchedule').value);">
    <input type="hidden" id="ClientID" size="2" value="<?=$ClientID;?>">
    <input type="hidden" id="ClientName" size="8" value="<?=$Client[0]['cli_name'];?>">
    <input type="hidden" id="ClientURL" size="32" value="<?=$Client[0]['cli_url'];?>">
    <input type="hidden" id="ClientAddress" size="16" value="<?=$Client[0]['cli_address'];?>">
    <input type="hidden" id="ClienSysCode" size="8" value="<?=$Client[0]['cli_syscode'];?>">
    </div>
  </div>
</div>
<?//	echo "InUsed<pre>"; print_r($InUsed); echo "</pre>"; ?>
<br>
<div class="demo boxin w-100" id="dvScheduleInfo">
  <div class="header">
  	<h3>
    <span>Schedule Information</span>
    <span><input class="btnButtonNorm" type="button" style="margin-left:0px;" id="btnSchRefresh" value="Refresh" onClick="document.location.reload(true);"></span>
    <span><input class="btnButtonNorm" type="button" style="margin-left:0px;" id="btnShowHistory" value="History" onClick="$('#dvHistory').dialog('open'); $('#btnHistoryRefresh').click();"></span>
    <span><input class="btnButtonNorm" type="button" style="margin-left:0px;" id="btnShowOption" value="Config" onClick="$('#dvMonitor').dialog('open');"></span>
    </h3>
  </div>
	<div class="w-100" id="dvInfoBody">
<?	
	for ($iUsed=0; $iUsed < count($InUsed); $iUsed++) { 
		$onSchedule=sprintf("schedule_client_resend_schedule(%d,%d);",$ClientID,$InUsed[$iUsed]['schh_id']);
		$onFilelist=sprintf("schedule_client_resend_filelist(%d,%d);",$ClientID,$InUsed[$iUsed]['schh_id']);
		$txtEnable="";
		$txtDisable="";
		if ($InUsed[$iUsed]['schc_active']==1) {
			$btnActiveValue="Disable";
			$Active='0';
			$TabSelected=$iUsed;
			$btnClass="btnButtonOff";
			$clsSpan="spanScheduleOn";
			$H3Class="btnButtonOff";
			$disEnable="disabled";
			$disDisable="";
			$txtDisable=sprintf("title=\"Disable schedule '%s'\"",$InUsed[$iUsed]['schh_name']);
			$txtTitle=sprintf("title=\"Disable schedule '%s'\"",$InUsed[$iUsed]['schh_name']);
		} else {
			$btnActiveValue="Enable";
			$Active='1';
			$btnClass="btnButtonOn";
			$clsSpan="spanScheduleOff";
			$H3Class="btnButtonOn";
			$disEnable="";
			$disDisable="disabled";
			$txtEnable=sprintf("title=\"Enable schedule '%s'\"",$InUsed[$iUsed]['schh_name']);
			$txtTitle=sprintf("title=\"Enable schedule '%s'\"",$InUsed[$iUsed]['schh_name']);
		}

		$btnEnable="btnButtonOff";
		$btnDisable="btnButtonOn";

		$onbtnEnable=sprintf("schedule_client_active(%d,%d,1);",$ClientID,$InUsed[$iUsed]['schh_id']);
		$onbtnDisable=sprintf("schedule_client_active(%d,%d,0);",$ClientID,$InUsed[$iUsed]['schh_id']);
		$onbtnAction=sprintf("schedule_client_active(%d,%d,%s);",$ClientID,$InUsed[$iUsed]['schh_id'],$Active);
		$onDelSchedule=sprintf("schedule_client_delete(%d,%d)",$ClientID,$InUsed[$iUsed]['schh_id']);
		$onDownload=sprintf("schedule_client_download(%d)",$ClientID);
		
?>
  	<h3 style="height:30px;">
    	<span style="padding-left:25px; vertical-align:text-bottom"><?=$InUsed[$iUsed]['schh_name'];?></span>
    	<span class="<?=$btnClass;?>" style="float:right; height:15px;"></span>
			<span style="padding-left:15px; width:100px; float:right; margin-top:-3px;">
    	<input class="btnButtonNorm" type="button" value=" <?=$btnActiveValue;?> " onClick="<?=$onbtnAction;?>" <?=$txtTitle?>>
      </span>
    	<span style="float:right; margin-top:-3px;">
<? //	if ($Active==0) {?>
    	<input class="btnButtonNorm" type="button" id="<?=$btnSchedule;?>" value="Resend Schedule" onClick="schedule_client_run(<?=$InUsed[$iUsed]['schh_id'];?>)">
<!--
    	<input class="btnButtonNorm" type="button" id="<?=$btnSchedule;?>" value="Resend Schedule" onClick="<?=$onSchedule;?>">
    	<input class="btnButtonNorm" type="button" id="<?=$btnReFilelist;?>" value="Resend Download File List" onClick="<?=$onFilelist;?>">
     	<input class="btnButtonNorm" type="button" id="<?=$btnDownload;?>" value="Download Schedule" onClick="<?=$onDownload;?>">
-->
<? //	}?>
     	&nbsp; <input class="btnButtonNorm" type="button" id="<?=$btnDelSchedule;?>" value="Delete" onClick="<?=$onDelSchedule;?>">
 			</span>
    </h3>
    <div  class="demo" style="overflow:auto;">
    	<div class="boxin" >
        <div class="header">
          <span>
          <a href="./schedule.php?schID=<?=$InUsed[$iUsed]['schh_id']?>">
          <img class="preview" width="16" height="16" src="./images/icons/edit.png" title="edit"></a>
          </span> 
        	<span style="font-size:large">Schedule Period :</span>
<?	
	$URL=sprintf("%s/ajax/schedule_period_list.php?schID=%d",$LocalHost,$InUsed[$iUsed]['schh_id']);
	$Data=file_get_contents($URL);
	$PeriodList=json_decode($Data,true);
//	echo "URL => ".$URL;
//	echo "Period<pre>"; print_r($PeriodList); echo "</pre>";
	
	for ($iPeriod=0; $iPeriod < count($PeriodList); $iPeriod++) {
//		$tStart=DBDate2Display($PeriodList[$iPeriod]['schp_start']);
//		$tStop=DBDate2Display($PeriodList[$iPeriod]['schp_stop']);
		$txtBR="";
		if (($iPeriod%4)==3) $txtBR="<br>";
?>
		  		<span><?=$PeriodList[$iPeriod]['start'];?> - <?=$PeriodList[$iPeriod]['stop'];?> &nbsp; </span>
					<?=$txtBR?>
<?
	} // End Loop for Period List

	$URL=sprintf("%s/ajax/schedule_layout_list.php?schID=%d",$LocalHost,$InUsed[$iUsed]['schh_id']);
	$Data=file_get_contents($URL);
	$TList=json_decode($Data,true);

//	echo "URL => ".$URL;
//	echo "Template<pre>"; print_r($TList); echo "</pre>";

?>
        </div>
        <div id='dvSch11' class="demo w-100" style="min-height:50px; max-height: 250px; overflow:auto;">
          <table cellspacing="0" cellpadding="0" class="set_font_table">
          <thead id="tblLayoutHead">
          <tr>
            <th rowspan="2" class="w-50">Template Name <br> Layout Name</th>
            <th colspan="7" <th width="21%">Day of Week</th>
            <th rowspan="2" class="w-5">Start</th>
            <th rowspan="2" class="w-5">Stop</th>
          </tr>
          <tr>
            <th width="3%" class="day_00">Sun</th>
            <th width="3%" class="day_00">Mon</th>
            <th width="3%" class="day_00">Tue</th>
            <th width="3%" class="day_00">Wed</th>
            <th width="3%" class="day_00">Thu</th>
            <th width="3%" class="day_00">Fri</th>
            <th width="3%" class="day_00">Sat</th>
          </tr>
          </thead>
          <tbody id="tblLayoutLine">
<?
	if (count($TList) > 0) { 
		for ($iRow=0; $iRow <count($TList); $iRow++) {
			if ($TList[$iRow]['schl_active']==1) {
?>
          <tr align="center">
            <td align="left" class="Schedule">
						<span class="top" style="font-weight:bolder"><a href="layout.php?TID=<?=$TList[$iRow]['layh_id'];?>">
						<img class="preview top" width="16" height="16" src="./images/icons/edit.png" title="edit"></a></span>
            <span style="font-weight:bolder">Template : </span><span><?=$TList[$iRow]['layh_name'];?></span><br>
						<span class="top15"><img class="preview" width="16" height="16" src="./images/icons/preview.png" 
            onClick="layout_popup(<?=$TList[$iRow]['tmph_id'];?>);" title="view"></span>
            <span style="font-weight:bolder">Layout : </span><span><?=$TList[$iRow]['tmph_name'];?></span></td>

            <td><?=($TList[$iRow]['schl_sunday']==1)?"X":"&nbsp;";?></td>
            <td><?=($TList[$iRow]['schl_monday']==1)?"X":"&nbsp;";?></td>
            <td><?=($TList[$iRow]['schl_tuesday']==1)?"X":"&nbsp;";?></td>
            <td><?=($TList[$iRow]['schl_wednesday']==1)?"X":"&nbsp;";?></td>
            <td><?=($TList[$iRow]['schl_thursday']==1)?"X":"&nbsp;";?></td>
            <td><?=($TList[$iRow]['schl_friday']==1)?"X":"&nbsp;";?></td>
            <td><?=($TList[$iRow]['schl_saturday']==1)?"X":"&nbsp;";?></td>
            <td><?=$TList[$iRow]['schl_start'];?></td>
            <td><?=$TList[$iRow]['schl_stop'];?></td>
          </tr>
<?
			}
		}
	} else { 
?>
          <tr align="center">
            <td colspan="10">No information found.</td>
          </tr>
<?
	}
?>
          </tbody id="tblLayoutLine">
          </table>
        </div>
      </div>
    </div>
<?	} // End Loop for Schedule Client List?>
  </div>
</div>
<input type="hidden" id="TabSelected" value="<?=$TabSelected;?>">
<script type="text/javascript">
	$(function() {
//		$('#dvCommand').dialog({ autoOpen: false, modal: true, draggable: false });
		$('#dvCommand').dialog({ autoOpen: false, modal: true, draggable: true });
		$('#dvCommand').dialog("option","width",700);
		$('#dvCommand').dialog("option","height",450);
		$('#dvCommand').dialog("option","resizable",false);
	});
</script>
<div class="demo">
<div id="dvCommand" title="Resend Schedule">
<table>
<tr><td>
	<input type="hidden" id="ClientID" value="<?=$ClientID;?>">
	<input type="hidden" id="ScheduleID" value="">
	<div class="boxin" style="margin-top:10px;">
		<div class="header" >
    	<span>Resend Schedule</span>
      <span style="font-size:12px;"><input id="btnSendSchedule" class="btnButtonNorm" type="button" onClick="schedule_client_resend_schedule('ClientID','ScheduleID');" value="Resend"></span>
      <span id="spSendSchedule" class="spSchStatusOK" >Success</span>
    </div>
  </div>
	<div class="boxin">
		<div class="header">
    <span>File List</span>
      <span style="font-size:12px; margin-top:-2px;"><input id="btnSendFile" class="btnButtonNorm" type="button" onClick="schedule_client_resend_filelist('ClientID','SchID');" value="Resend"></span>
    <span id="spSendFileList" class="spSchStatusWait" >Waiting</span>
    </div>
    <div id="spFileList" style="overflow:auto; height:250px;">
    <table border="1">
    	<tr><th>FileName</th><th width="25%">Status</th></tr>
      <tbody id="tblFileList_Body">
      </tbody>
    </table>
    </div>
	</div>
	<div class="boxin">
		<div class="header">
    	<span>Start Download</span>
      <span style="font-size:12px;margin-top:-5px;">
      <input class="btnButtonNorm" id="btnDownload" type="button" onClick="schedule_client_download('ClientID','SchID');" value="Resend">		</span>
      <span id="spDownload" class="spSchStatusFailed" >Failed</span>
    </div>
  </div>
</td>
</tr>
</table>
</div>
</div>

<script type="text/javascript">
	$(function() {
//		$('#dvCommand').dialog({ autoOpen: false, modal: true, draggable: false });
		$('#dvHistory').dialog({ autoOpen: false, modal: true, draggable: true });
		$('#dvHistory').dialog("option","width",1024);
		$('#dvHistory').dialog("option","height",450);
		$('#dvHistory').dialog("option","resizable",false);
	});
</script>
<div class="demo">
<div id="dvHistory" title="History">
	<div class="boxin">
		<div class="header">
    	<span>History</span>
      <span style="font-size:12px;margin-top:-5px;">
      <input class="btnButtonNorm" id="btnHistoryRefresh" type="button" onClick="schedule_client_history('ClientID');" value="Refresh"></span>
    </div>
    <div id="dvHistoryList">
    </div>
  </div>
</div>
</div>

</body>
</html>