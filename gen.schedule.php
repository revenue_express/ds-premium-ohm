<? 
	$DEBUG=FALSE;
	include("./includes/db_config.inc.php");
	include("./includes/sys_config.inc.php");
	$CSSName=NULL;
	$LayoutItems=NULL;
	$TypeUseFile=array(1,2,3,4,9,10,11);
	

	if (isset($schID)) {
		include("./functions/schedule.func.php");
		include("./functions/layout.func.php");
		include("./functions/playlist.func.php");
		/*	Get Schedule Header Information */
		$schInfo=json_decode(schedule_head_view($schID),true);
		if ($DEBUG) {  echo "<br>schInfo<pre>"; print_r($schInfo); echo "</pre>"; }

		$ShowTime="";
		/*	Get Period ShowTime Information */
		$schDate=json_decode(schedule_period_list($schID),true);
		if ($DEBUG) {  echo "<br>schDate<pre>"; print_r($schDate); echo "</pre>"; }

/*
		for ($iCount=0; $iCount < count($schDate); $iCount++) {
			$ShowTime[]=array("start"=>$schDate[$iCount]['schp_start'],"stop"=>$schDate[$iCount]['schp_stop']);
		}
*/
		if (count($schDate) > 0 ) {
			$ShowTime=array("startDate"=>$schDate[0]['schp_start'],
														"stopDate"=>$schDate[0]['schp_stop'],
														"startTime"=>"00:00:01",
														"stopTime"=>"23:59:59");
		}
		
		if ($DEBUG) {  echo "<br>ShowTime :<pre>"; print_r($ShowTime); echo "</pre>"; }

		/*	Get Template in Schedule*/
		$schItem=json_decode(schedule_layout_list($schID),true);
		if ($DEBUG) {  echo "<br>schItem<pre>"; print_r($schItem); echo "</pre>"; }
		/*	Loop for Get Template Information */
		for ($iSch=0; $iSch < count($schItem); $iSch++) {
			if ($DEBUG) {  echo sprintf("<br>schItem [%d] [%d]<pre>",$schItem[$iSch],$iSch); print_r($schItem[$iSch]); echo "</pre>"; }
			/*	Get Each Template Detail*/
			$PanelCount=array(0,0,0,0,0,0,0,0,0,0,0,0,0,0,0);
			$TItem=json_decode(layout_line_list($schItem[$iSch]['layh_id']),true);		
			if ($DEBUG) {  echo sprintf("<br>LayItem on Layout %d<pre>",$schItem[$iSch]['layh_tmph_id']); print_r($TItem); echo "</pre>"; }
			$MPanel=NULL;
			$Panel=NULL;
			/* Loop Each Template Detail to Collect Playlist*/
			$ItemCount=array();
			for ($iTmp=0; $iTmp < count($TItem); $iTmp++) {
				/* Get Playlist Header Information*/
				$PlayList=NULL;
				$PLHead=json_decode(playlist_head_view($TItem[$iTmp]['layl_plh_id']),true);
				if ($DEBUG) {  echo sprintf("<br>PLHead %d<pre>",$TItem[$iTmp]['layl_plh_id']); print_r($PLHead); echo "</pre>"; }
				/* Get Playlist Header Option Information*/
				$PLHeadOption=json_decode(playlist_head_option_list($TItem[$iTmp]['layl_plh_id']),true);

				/* Get Playlist Items*/
				if ($PLHead[0]['plh_type_id'] != 5) {
					$PLLine=json_decode(playlist_line_list($ComID,$PLHead[0]['plh_type_id'],$PLHead[0]['plh_id']),true);
				}

				if ($DEBUG) {  echo sprintf("<br>PLLine %d<pre>",$TItem[$iTmp]['layl_plh_id']); print_r($PLLine); echo "</pre>"; }
				$PLItem="";

				/*Loop for create Playlist Items Array*/
				for ($iPlay=0; $iPlay < count($PLLine); $iPlay++) {
//					$FPath=sprintf("/media/%s/%s",$PLLine[$iPlay]['file_path'],$PLLine[$iPlay]['file_sname']);
					$FileName=sprintf("data/%s/%s",$PLLine[$iPlay]['type_path'],$PLLine[$iPlay]['file_sname']);
					$FPath=sprintf("%s/%s",$PLLine[$iPlay]['type_spath'],$PLLine[$iPlay]['file_sname']);
					$FType=mime_content_type($FileName);
					if ($FType=="application/octet-stream") { $FType = "video/webm";}
					$PLItemData=array(
//						"pll_id"=>$PLLine[$iPlay]['pll_id'],
						"id"=>$PLLine[$iPlay]['file_id'],
						"title"=>$PLLine[$iPlay]['file_dname'],
//						"type"=>$PLLine[$iPlay]['type_name'],
						"type"=>$FType,
						"filename"=>$PLLine[$iPlay]['file_sname'],
						"src"=>$FPath);
					if ($PLLine[$iPlay]['type_id']==3) {
						$PLItemData['para']=$PLLine[$iPlay]['pll_para'];
					}
					$PLLineOption=json_decode(playlist_line_option_list($PLLine[$iPlay]['pll_id']),true);
					if (count($PLLineOption) > 0) {
						for ($iCount=0; $iCount<count($PLLineOption); $iCount++) {
						$Head=$PLLineOption[$iCount]['plo_name'];
						$Value=$PLLineOption[$iCount]['plo_value'];
							$PLItemData['option'][$Head]=$Value;
						}
					}
					$PLItem[]=$PLItemData;
				}

				/*Create Playlist Array*/
				$myPlayList=array(
					"id"=>$PLHead[0]['plh_id'],
					"title"=>$PLHead[0]['plh_name']);

				for ($iCount=0; $iCount<count($PLHeadOption); $iCount++) {
					$Head=$PLHeadOption[$iCount]['pho_name'];
					$Value=$PLHeadOption[$iCount]['pho_value'];
					$myPlayList['config'][$Head]=$Value;
				}
				
				if ($PLHead[0]['plh_type_id'] == 5) {
				} elseif ($PLHead[0]['plh_type_id'] == 8) {
					$sURL=sprintf("%s://%s:%d/%s",$PLLine[0]['stm_protocol'],$PLLine[0]['stm_address'],$PLLine[0]['stm_port'],$PLLine[0]['stm_para01']);
					$myPlayList['url']=$sURL;
				} else {
					$myPlayList["item"]=$PLItem	;
				}
				$PlayList=$myPlayList;
//				echo "<pre>"; print_r($PLHeadOption); echo "</pre><br>\n";
//				echo "<pre>"; print_r($myPlayList); echo "</pre><br>\n";

/*Create Panel Array*/
//				if (isset($ItemCount[$TItem[$iTmp]['type_name']])) {
				if (isset($ItemCount[$TItem[$iTmp]['type_panel']])) {
//	Last Found This Object
//					$ItemCount[$TItem[$iTmp]['type_name']]++;
//					$iCount=$ItemCount[$TItem[$iTmp]['type_name']];
					$ItemCount[$TItem[$iTmp]['type_panel']]++;
					$iCount=$ItemCount[$TItem[$iTmp]['type_panel']];
				} else {
//	Not Found This Object
					$iCount=1;
//					$ItemCount[$TItem[$iTmp]['type_name']]=1;							
					$ItemCount[$TItem[$iTmp]['type_panel']]=1;							
				}
//				$PanelCount[$PLHead[0]['type_id']]++;
				$PanelCount[$PLHead[0]['type_panel']]++;
//				$PanelName=sprintf("%s%d",$PLHead[0]['type_panel'],$PanelCount[$PLHead[0]['type_id']]);
				$PanelName=sprintf("%s%d",$PLHead[0]['type_panel'],$PanelCount[$PLHead[0]['type_panel']]);
				$Panel[$PanelName]=array(
					"type"=>$TItem[$iTmp]['type_name'],
//					"name"=>sprintf("%s%dpanel",$TItem[$iTmp]['type_name'],$iCount),
					"name"=>$Item[$iTmp]['layh_name'],
					"id"=>$TItem[$iTmp]['layl_id'],
					"title"=>$Item[$iTmp]['layh_name'],
					"playlist"=>$PlayList);
				if	 (($PLHead[0]['type_id']=='2') || ($PLHead[0]['type_id']=='4')){
					$MPanel=$PanelName;
				}
			}
			$URL=sprintf("http://localhost/premium/gen.css.php?TID=%d",$schItem[$iSch]['layh_tmph_id']);
			$CSSName=sprintf("%d.css",$schItem[$iSch]['layh_tmph_id']);
			$CSSPath="./data/css";
			$CSSFile=sprintf("%s/%s",$CSSPath,$CSSName);
			if ($DEBUG) { echo sprintf("<br>CSS Create %s <a href='%s'>Run</a><br>",$CSSName,$URL); } 
			$CSSData=file_get_contents($URL);
			for ($iWhile=0; $iWhile < 1000; $iWhile++) { }
				file_put_contents($CSSFile,$CSSData);
				if ($DEBUG) { echo sprintf("<br>CSS Create %s <a href='%s'>Line</a><br>",$CSSFile,$CSSFile); } 
				$Template[]=array(
					"id"=>$schItem[$iSch]['layh_id'],
					"title"=>$schItem[$iSch]['layh_name'],
	//				"src"=>sprintf("/layout/%s",$CSSName),
					"src"=>sprintf("%s",$CSSName),
					"start"=>$schItem[$iSch]['schl_start'],
					"stop"=>$schItem[$iSch]['schl_stop'],
					"day"=>array( 
						"sunday"=>($schItem[$iSch]['schl_sunday'])?"1":"0",
						"monday"=>($schItem[$iSch]['schl_monday'])?"1":"0",
						"tuesday"=>($schItem[$iSch]['schl_tuesday'])?"1":"0",
						"wednesday"=>($schItem[$iSch]['schl_wednesday'])?"1":"0",
						"thursday"=>($schItem[$iSch]['schl_thursday'])?"1":"0",
						"friday"=>($schItem[$iSch]['schl_friday'])?"1":"0",
						"saturday"=>($schItem[$iSch]['schl_saturday'])?"1":"0"	),
					"mpanel"=>$MPanel,
					"panel"=>$Panel	);
		}

		/*Create Schedule Array*/
		$Schedule=NULL;
		if (count($schInfo) > 0) {
			$Schedule=array(
				"id"=>$schInfo[0]['schh_id'],
				"title"=>$schInfo[0]['schh_name'],
				"showtime"=>$ShowTime,
				"template"=>$Template );
		}
	}
	$Data=array("schedule"=>$Schedule);
	echo json_encode($Data);

?>
	